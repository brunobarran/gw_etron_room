// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#define OCULUSHMD_OPTITRACK

namespace UnrealBuildTool.Rules
{
	public class OptitrackOculusInput : ModuleRules
	{
		public OptitrackOculusInput(ReadOnlyTargetRules Target) : base(Target)
		{
#if OCULUSHMD_OPTITRACK
			PublicDefinitions.Add( "OCULUSHMD_OPTITRACK" );
#endif // #if OCULUSHMD_OPTITRACK

            PrivateIncludePathModuleNames.AddRange(
				new string[]
				{
					"InputDevice",			// For IInputDevice.h
					"HeadMountedDisplay",	// For IMotionController.h
					"ImageWrapper"
				});

			PrivateDependencyModuleNames.AddRange(
				new string[]
				{
					"Core",
					"CoreUObject",
					"ApplicationCore",
					"Engine",
					"InputCore",
					"HeadMountedDisplay",
					"OptitrackOculusHMD",
					"OVRPlugin",

#if OCULUSHMD_OPTITRACK
					"OptitrackNatnet",
                    "NaturalPointHmdFusion",
#endif // #if OCULUSHMD_OPTITRACK
				});

			PrivateIncludePaths.AddRange(
				new string[] {
					// Relative to Engine\Plugins\Runtime\Oculus\OculusVR\Source
					"OptitrackOculusHMD/Private",
					EngineDirectory + "/Source/Runtime/Renderer/Private",
					EngineDirectory + "/Source/Runtime/Engine/Classes/Components",
				});

            if (Target.Platform == UnrealTargetPlatform.Win32 || Target.Platform == UnrealTargetPlatform.Win64)
            {
				PublicDelayLoadDLLs.Add("OVRPlugin.dll");
				RuntimeDependencies.Add("$(EngineDir)/Binaries/ThirdParty/Oculus/OVRPlugin/OVRPlugin/" + Target.Platform.ToString() + "/OVRPlugin.dll");			
			}
		}
	}
}
