// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class OptitrackOculusEditor : ModuleRules
{
	public OptitrackOculusEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PrivateIncludePaths.Add("OptitrackOculusEditor/Private");
        PrivateDependencyModuleNames.AddRange(
			new string[] {
				"Projects",
				"InputCore",
				"UnrealEd",
				"LevelEditor",
				"CoreUObject",
				"Engine",
				"EngineSettings",
				"Slate",
				"SlateCore",
				"EditorStyle",
				"Core",
				"CoreUObject",
				"OptitrackOculusHMD",
				"OVRPlugin",
			}
			);

		PrivateIncludePaths.AddRange(
				new string[] {
					// Relative to Engine\Plugins\Runtime\Oculus\OculusVR\Source
					"OptitrackOculusEditor/Private",
					"OptitrackOculusHMD/Private",
				});

		PrivateIncludePathModuleNames.AddRange(
			new string[] {
				"Settings",
            }
            );
	}
}
