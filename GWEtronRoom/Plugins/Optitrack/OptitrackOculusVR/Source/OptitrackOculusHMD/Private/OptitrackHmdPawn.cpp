/*
Copyright 2018 NaturalPoint

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "OptitrackHmdPawn.h"
#include "OptitrackClientOrigin.h"
#include "OculusFunctionLibrary.h"


AOptitrackHmdPawn::AOptitrackHmdPawn(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    RigidBodyComponent = CreateDefaultSubobject<UOptitrackRigidBodyComponent>(TEXT("RigidBodyComponent"));

    // Make the scene component the root component
    RootComponent = RigidBodyComponent;

    BaseEyeHeight = 0.0f;
    SetTickGroup(TG_PrePhysics);

    CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
    
    CameraComponent->SetupAttachment(RigidBodyComponent);

    // Initialize actor position/rotation in Optitrack/Oculus function library.
    UOptitrackOculusFunctionLibrary::SetBaseRotationAndBaseOffsetInMeters(GetActorRotation(), GetActorLocation(), EOrientPositionSelector::OrientationAndPosition);
}

void AOptitrackHmdPawn::Tick(float DeltaSeconds)
{
    if (TrackingOrigin == nullptr)
    {
        TrackingOrigin = AOptitrackClientOrigin::FindDefaultClientOrigin(GetWorld());
    }
    else
    {
        if (MovePawn)
        {
            RigidBodyComponent->TrackingId = TrackingOrigin->HmdRigidBodyId;
        }
        else
        {
            RigidBodyComponent->TrackingId = 9999;
        }
    }
    UOptitrackOculusFunctionLibrary::SetBaseRotationAndBaseOffsetInMeters(GetActorRotation(), GetActorLocation(), EOrientPositionSelector::OrientationAndPosition);
    Super::Tick(DeltaSeconds);
}
