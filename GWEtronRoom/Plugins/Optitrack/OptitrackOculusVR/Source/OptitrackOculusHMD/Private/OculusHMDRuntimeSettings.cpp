// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "OculusHMDRuntimeSettings.h"

//////////////////////////////////////////////////////////////////////////
// UOptitrackOculusHMDRuntimeSettings


UOptitrackOculusHMDRuntimeSettings::UOptitrackOculusHMDRuntimeSettings(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
, bAutoEnabled(true)
{
}
