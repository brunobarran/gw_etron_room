// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#define OCULUSHMD_OPTITRACK

using System;
using System.IO;

namespace UnrealBuildTool.Rules
{
	public class OptitrackOculusHMD : ModuleRules
	{

		public OptitrackOculusHMD(ReadOnlyTargetRules Target) : base(Target)
		{
#if OCULUSHMD_OPTITRACK
			PublicDefinitions.Add( "OCULUSHMD_OPTITRACK" );
			PublicDefinitions.Add( "OCULUSHMD_API=OPTITRACKOCULUSHMD_API" );
#endif // #if OCULUSHMD_OPTITRACK

            PrivateIncludePaths.AddRange(
				new string[] {
					// Relative to Engine\Plugins\Runtime\Oculus\OculusVR\Source
					EngineDirectory + "/Source/Runtime/Renderer/Private",
                    EngineDirectory + "/Source/Runtime/OpenGLDrv/Private",
                    EngineDirectory + "/Source/Runtime/VulkanRHI/Private",
                    EngineDirectory + "/Source/Runtime/Engine/Classes/Components",
                } );

			PublicIncludePathModuleNames.AddRange(
				new string[] {
					"Launch",
					"ProceduralMeshComponent",
				});			

			if (Target.Platform == UnrealTargetPlatform.Win32 || Target.Platform == UnrealTargetPlatform.Win64)
			{
                PrivateIncludePaths.Add(EngineDirectory + "/Source/Runtime/VulkanRHI/Private/Windows");
			}
			else
			{
                PrivateIncludePaths.Add(EngineDirectory + "/Source/Runtime/VulkanRHI/Private/" + Target.Platform);
			}


			PrivateDependencyModuleNames.AddRange(
				new string[]
				{
					"Core",
					"CoreUObject",
					"Engine",
					"InputCore",
					"RHI",
					"RenderCore",
					"Renderer",
					"ShaderCore",
					"HeadMountedDisplay",
					"Slate",
					"SlateCore",
					"ImageWrapper",
					"MediaAssets",
					"Analytics",
					"UtilityShaders",
					"OpenGLDrv",
					"VulkanRHI",
					"OVRPlugin",
					"ProceduralMeshComponent",
                    "Projects",
					"OpenGL",
				} );
#if OCULUSHMD_OPTITRACK
            PublicDependencyModuleNames.AddRange(
                new string[]
                {
                    "NaturalPointHmdFusion",
                    "OptitrackNatnet",
                } );
#endif // #if OCULUSHMD_OPTITRACK

#if OCULUSHMD_OPTITRACK
            if (Target.Platform == UnrealTargetPlatform.Win32 || Target.Platform == UnrealTargetPlatform.Win64)
            {
                PrivateDependencyModuleNames.AddRange(
                    new string[]
                    {
                        "DX11",
                        "DX12",
                        "NVAPI",
                        "DX11Audio",
                        "DirectSound",
                        "NVAftermath",
                    } );
            }
#endif // #if OCULUSHMD_OPTITRACK

            if (Target.bBuildEditor == true)
			{
				PrivateDependencyModuleNames.Add("UnrealEd");
			}

#if !OCULUSHMD_OPTITRACK
            AddEngineThirdPartyPrivateStaticDependencies(Target, "OpenGL");
#endif // #if OCULUSHMD_OPTITRACK

            if (Target.Platform == UnrealTargetPlatform.Win32 || Target.Platform == UnrealTargetPlatform.Win64)
			{
				// D3D
				{
					PrivateDependencyModuleNames.AddRange(
                        new string[]
                        {
                            "D3D11RHI",
                            "D3D12RHI",
                        } );

                    PrivateIncludePaths.AddRange(
                        new string[]
                        {
                            EngineDirectory + "/Source/Runtime/Windows/D3D11RHI/Private",
                            EngineDirectory + "/Source/Runtime/Windows/D3D11RHI/Private/Windows",
                            EngineDirectory + "/Source/Runtime/D3D12RHI/Private",
                            EngineDirectory + "/Source/Runtime/D3D12RHI/Private/Windows",
                        } );

                    AddEngineThirdPartyPrivateStaticDependencies( Target, "DX11" );
                    AddEngineThirdPartyPrivateStaticDependencies( Target, "DX12" );
                    AddEngineThirdPartyPrivateStaticDependencies( Target, "NVAPI" );
                    AddEngineThirdPartyPrivateStaticDependencies( Target, "DX11Audio" );
                    AddEngineThirdPartyPrivateStaticDependencies( Target, "DirectSound" );
                    AddEngineThirdPartyPrivateStaticDependencies( Target, "NVAftermath" );
                    AddEngineThirdPartyPrivateStaticDependencies( Target, "IntelMetricsDiscovery");

					// Vulkan
					{
						AddEngineThirdPartyPrivateStaticDependencies(Target, "Vulkan");
					}

					// OVRPlugin
					{
						PublicDelayLoadDLLs.Add("OVRPlugin.dll");
						RuntimeDependencies.Add("$(EngineDir)/Binaries/ThirdParty/Oculus/OVRPlugin/OVRPlugin/" + Target.Platform.ToString() + "/OVRPlugin.dll");
					}
                
                }
			}
			else if (Target.Platform == UnrealTargetPlatform.Android)
			{
				// Vulkan
				{
                    AddEngineThirdPartyPrivateStaticDependencies(Target, "Vulkan");
                }

                // AndroidPlugin
                {
					string PluginPath = Utils.MakePathRelativeTo(ModuleDirectory, Target.RelativeEnginePath);
					AdditionalPropertiesForReceipt.Add("AndroidPlugin", Path.Combine(PluginPath, "GearVR_APL.xml"));
				}
			}
		}
	}
}
