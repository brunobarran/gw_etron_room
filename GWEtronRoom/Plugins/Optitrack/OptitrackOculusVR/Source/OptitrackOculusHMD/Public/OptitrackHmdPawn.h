/*
Copyright 2018 NaturalPoint

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <GameFramework/Pawn.h>
#include <Camera/CameraComponent.h>
#include <OptitrackRigidBodyComponent.h>

#include "OptitrackHmdPawn.generated.h"

UCLASS( )
class OPTITRACKOCULUSHMD_API AOptitrackHmdPawn : public APawn
{
	GENERATED_UCLASS_BODY()

    virtual void Tick(float DeltaSeconds);

    UPROPERTY(EditAnywhere, AdvancedDisplay, BlueprintReadWrite, Category = "Optitrack")
    class AOptitrackClientOrigin* TrackingOrigin;

    /*
    *Move pawn along with tracked HMD.
    */
    UPROPERTY(EditAnywhere, AdvancedDisplay, BlueprintReadWrite, Category = "Optitrack", meta = (AllowPrivateAccess = "true"))
    bool MovePawn = false;

private:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
    UCameraComponent* CameraComponent;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Rigid Body", meta = (AllowPrivateAccess = "true"))
    UOptitrackRigidBodyComponent* RigidBodyComponent;
};
