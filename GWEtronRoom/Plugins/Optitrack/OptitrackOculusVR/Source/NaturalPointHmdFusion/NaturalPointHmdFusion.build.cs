using System.IO;
using UnrealBuildTool;


public class NaturalPointHmdFusion : ModuleRules
{
	public NaturalPointHmdFusion( ReadOnlyTargetRules Target ) : base( Target )
	{
		Type = ModuleType.External;

		PublicDefinitions.Add( "NATURALPOINTHMDFUSIONIMPORTS" );

        PublicIncludePaths.Add( Path.Combine( ModuleDirectory, "Include" ) );        

        if ( Target.Platform == UnrealTargetPlatform.Win64 )
		{
			PublicLibraryPaths.Add( Path.Combine( ModuleDirectory, "Lib/Win64/" ) );
			PublicAdditionalLibraries.Add( "NaturalPointHmdFusion.lib" );
			PublicDelayLoadDLLs.Add( "NaturalPointHmdFusion.dll" );
			RuntimeDependencies.Add( Path.Combine( ModuleDirectory, "Dist/Win64/NaturalPointHmdFusion.dll") );
		}
		else if (Target.Platform == UnrealTargetPlatform.Win32 )
		{
			PublicLibraryPaths.Add( Path.Combine( ModuleDirectory, "Lib/Win32/" ) );
			PublicAdditionalLibraries.Add( "NaturalPointHmdFusion.lib" );
			PublicDelayLoadDLLs.Add( "NaturalPointHmdFusion.dll" );
			RuntimeDependencies.Add( Path.Combine( ModuleDirectory, "Dist/Win32/NaturalPointHmdFusion.dll") );
		}
		else if ( Target.Platform == UnrealTargetPlatform.Android )
		{
			PublicLibraryPaths.Add( Path.Combine( ModuleDirectory, "Lib/Android/armeabi-v7a/" ) );
			PublicAdditionalLibraries.Add( "NaturalPointHmdFusion" );
		}
	}
}