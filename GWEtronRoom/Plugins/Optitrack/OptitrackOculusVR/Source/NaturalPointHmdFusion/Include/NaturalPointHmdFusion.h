//======================================================================================================
// Copyright 2016, NaturalPoint Inc.
//======================================================================================================

#pragma once


#if defined( _WIN32 )
#   define NATURALPOINTHMDFUSION_CALLCONV __cdecl
#else
#   define NATURALPOINTHMDFUSION_CALLCONV
#endif


#if defined( _WIN32 )
#   if defined( NATURALPOINTHMDFUSION_EXPORTS )
#       define NATURALPOINTHMDFUSION_API              __declspec(dllexport)
#   elif defined( NATURALPOINTHMDFUSION_IMPORTS )
#       define NATURALPOINTHMDFUSION_API              __declspec(dllimport)
#   else
#       define NATURALPOINTHMDFUSION_API
#   endif
#else
#   if defined( NATURALPOINTHMDFUSION_EXPORTS )
#       define NATURALPOINTHMDFUSION_API              __attribute((visibility("default")))
#   elif defined( NATURALPOINTHMDFUSION_IMPORTS )
#       define NATURALPOINTHMDFUSION_API
#   else
#       define NATURALPOINTHMDFUSION_API
#   endif
#endif

#define NATURALPOINTHMDFUSION_CHALLENGETOKENLENGTH 8
#define NATURALPOINTHMDFUSION_AUTHENTICATIONTOKENLENGTH 99


#if defined( __cplusplus )
extern "C" {
#endif


typedef struct NaturalPointHmdHandle_* NaturalPointHmdHandle;
typedef double NaturalPointHmdTimeSeconds;


typedef struct NaturalPointHmdVector3
{
    double x;
    double y;
    double z;

#if defined( __cplusplus )
    NaturalPointHmdVector3()
        : x( 0.0 ), y( 0.0 ), z( 0.0 )
    {
    }

    NaturalPointHmdVector3( double x_, double y_, double z_ )
        : x( x_ ), y( y_ ), z( z_ )
    {
    }
#endif // #if defined( __cplusplus )
} NaturalPointHmdVector3;


typedef struct NaturalPointHmdQuaternion
{
    double x;
    double y;
    double z;
    double w;

#if defined( __cplusplus )
    NaturalPointHmdQuaternion()
        : x( 0.0 ), y( 0.0 ), z( 0.0 ), w( 1.0 )
    {
    }

    NaturalPointHmdQuaternion( double x_, double y_, double z_, double w_ )
        : x( x_ ), y( y_ ), z( z_ ), w( w_ )
    {
    }
#endif // #if defined( __cplusplus )
} NaturalPointHmdQuaternion;


enum NaturalPointHmdResult
{
    NaturalPointHmdResult_OK = 0,
    NaturalPointHmdResult_Failed,
    NaturalPointHmdResult_InvalidArgument,
};



NATURALPOINTHMDFUSION_API NaturalPointHmdTimeSeconds NATURALPOINTHMDFUSION_CALLCONV NaturalPointHmd_TimeNow();

NATURALPOINTHMDFUSION_API NaturalPointHmdResult NATURALPOINTHMDFUSION_CALLCONV NaturalPointHmd_Create( NaturalPointHmdHandle* pOutHmd );
NATURALPOINTHMDFUSION_API NaturalPointHmdResult NATURALPOINTHMDFUSION_CALLCONV NaturalPointHmd_Destroy( NaturalPointHmdHandle hmd );

NATURALPOINTHMDFUSION_API NaturalPointHmdResult NATURALPOINTHMDFUSION_CALLCONV NaturalPointHmd_ResetTimestamps(NaturalPointHmdHandle hmd);
NATURALPOINTHMDFUSION_API NaturalPointHmdResult NATURALPOINTHMDFUSION_CALLCONV NaturalPointHmd_ValidateAuthenticationToken(char* authToken);
NATURALPOINTHMDFUSION_API void NATURALPOINTHMDFUSION_CALLCONV NaturalPointHmd_AuthenticationToken(char* challengeToken);

NATURALPOINTHMDFUSION_API NaturalPointHmdResult NATURALPOINTHMDFUSION_CALLCONV NaturalPointHmd_MeasurementUpdate_Optical(
    NaturalPointHmdHandle hmd,
    const NaturalPointHmdVector3* pPosition,
    const NaturalPointHmdQuaternion* pOrientation,
    NaturalPointHmdTimeSeconds timestamp
);

NATURALPOINTHMDFUSION_API NaturalPointHmdResult NATURALPOINTHMDFUSION_CALLCONV NaturalPointHmd_MeasurementUpdate_Inertial(
    NaturalPointHmdHandle hmd,
    const NaturalPointHmdVector3* pLinearAcceleration,
    const NaturalPointHmdVector3* pAngularAcceleration,
    const NaturalPointHmdVector3* pLinearVelocity,
    const NaturalPointHmdVector3* pAngularVelocity,
    const NaturalPointHmdVector3* pPosition,
    const NaturalPointHmdQuaternion* pOrientation,
    NaturalPointHmdTimeSeconds timestamp
);

NATURALPOINTHMDFUSION_API NaturalPointHmdResult NATURALPOINTHMDFUSION_CALLCONV NaturalPointHmd_GetPosition(
    NaturalPointHmdHandle hmd,
    NaturalPointHmdVector3* pOutPosition
);

NATURALPOINTHMDFUSION_API NaturalPointHmdResult NATURALPOINTHMDFUSION_CALLCONV NaturalPointHmd_GetOrientation(
    NaturalPointHmdHandle hmd,
    NaturalPointHmdQuaternion* pOutOrientation
);

#if defined( __cplusplus )
} // extern "C"
#endif
