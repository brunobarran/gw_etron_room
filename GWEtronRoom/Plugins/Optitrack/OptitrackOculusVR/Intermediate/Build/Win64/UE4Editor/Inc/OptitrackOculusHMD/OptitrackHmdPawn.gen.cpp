// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OptitrackOculusHMD/Public/OptitrackHmdPawn.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOptitrackHmdPawn() {}
// Cross Module References
	OPTITRACKOCULUSHMD_API UClass* Z_Construct_UClass_AOptitrackHmdPawn_NoRegister();
	OPTITRACKOCULUSHMD_API UClass* Z_Construct_UClass_AOptitrackHmdPawn();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_OptitrackOculusHMD();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_UOptitrackRigidBodyComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_AOptitrackClientOrigin_NoRegister();
// End Cross Module References
	void AOptitrackHmdPawn::StaticRegisterNativesAOptitrackHmdPawn()
	{
	}
	UClass* Z_Construct_UClass_AOptitrackHmdPawn_NoRegister()
	{
		return AOptitrackHmdPawn::StaticClass();
	}
	struct Z_Construct_UClass_AOptitrackHmdPawn_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RigidBodyComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RigidBodyComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MovePawn_MetaData[];
#endif
		static void NewProp_MovePawn_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_MovePawn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TrackingOrigin;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AOptitrackHmdPawn_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackOculusHMD,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackHmdPawn_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "OptitrackHmdPawn.h" },
		{ "ModuleRelativePath", "Public/OptitrackHmdPawn.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_RigidBodyComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Rigid Body" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/OptitrackHmdPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_RigidBodyComponent = { UE4CodeGen_Private::EPropertyClass::Object, "RigidBodyComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(AOptitrackHmdPawn, RigidBodyComponent), Z_Construct_UClass_UOptitrackRigidBodyComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_RigidBodyComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_RigidBodyComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_CameraComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/OptitrackHmdPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_CameraComponent = { UE4CodeGen_Private::EPropertyClass::Object, "CameraComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(AOptitrackHmdPawn, CameraComponent), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_CameraComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_CameraComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_MovePawn_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackHmdPawn.h" },
		{ "ToolTip", "*Move pawn along with tracked HMD." },
	};
#endif
	void Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_MovePawn_SetBit(void* Obj)
	{
		((AOptitrackHmdPawn*)Obj)->MovePawn = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_MovePawn = { UE4CodeGen_Private::EPropertyClass::Bool, "MovePawn", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AOptitrackHmdPawn), &Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_MovePawn_SetBit, METADATA_PARAMS(Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_MovePawn_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_MovePawn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_TrackingOrigin_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackHmdPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_TrackingOrigin = { UE4CodeGen_Private::EPropertyClass::Object, "TrackingOrigin", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000005, 1, nullptr, STRUCT_OFFSET(AOptitrackHmdPawn, TrackingOrigin), Z_Construct_UClass_AOptitrackClientOrigin_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_TrackingOrigin_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_TrackingOrigin_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AOptitrackHmdPawn_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_RigidBodyComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_CameraComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_MovePawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackHmdPawn_Statics::NewProp_TrackingOrigin,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AOptitrackHmdPawn_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AOptitrackHmdPawn>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AOptitrackHmdPawn_Statics::ClassParams = {
		&AOptitrackHmdPawn::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		Z_Construct_UClass_AOptitrackHmdPawn_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AOptitrackHmdPawn_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AOptitrackHmdPawn_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AOptitrackHmdPawn_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AOptitrackHmdPawn()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AOptitrackHmdPawn_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AOptitrackHmdPawn, 3403516117);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AOptitrackHmdPawn(Z_Construct_UClass_AOptitrackHmdPawn, &AOptitrackHmdPawn::StaticClass, TEXT("/Script/OptitrackOculusHMD"), TEXT("AOptitrackHmdPawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AOptitrackHmdPawn);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
