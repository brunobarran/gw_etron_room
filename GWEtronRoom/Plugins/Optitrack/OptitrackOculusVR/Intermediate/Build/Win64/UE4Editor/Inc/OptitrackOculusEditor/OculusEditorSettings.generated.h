// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPTITRACKOCULUSEDITOR_OculusEditorSettings_generated_h
#error "OculusEditorSettings.generated.h already included, missing '#pragma once' in OculusEditorSettings.h"
#endif
#define OPTITRACKOCULUSEDITOR_OculusEditorSettings_generated_h

#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_RPC_WRAPPERS
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOptitrackOculusEditorSettings(); \
	friend struct Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UOptitrackOculusEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OptitrackOculusEditor"), NO_API) \
	DECLARE_SERIALIZER(UOptitrackOculusEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUOptitrackOculusEditorSettings(); \
	friend struct Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UOptitrackOculusEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OptitrackOculusEditor"), NO_API) \
	DECLARE_SERIALIZER(UOptitrackOculusEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOptitrackOculusEditorSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOptitrackOculusEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOptitrackOculusEditorSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOptitrackOculusEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOptitrackOculusEditorSettings(UOptitrackOculusEditorSettings&&); \
	NO_API UOptitrackOculusEditorSettings(const UOptitrackOculusEditorSettings&); \
public:


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOptitrackOculusEditorSettings(UOptitrackOculusEditorSettings&&); \
	NO_API UOptitrackOculusEditorSettings(const UOptitrackOculusEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOptitrackOculusEditorSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOptitrackOculusEditorSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UOptitrackOculusEditorSettings)


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_PRIVATE_PROPERTY_OFFSET
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_21_PROLOG
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_RPC_WRAPPERS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_INCLASS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_INCLASS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusEditor_Public_OculusEditorSettings_h


#define FOREACH_ENUM_EOCULUSPLATFORM(op) \
	op(EOculusPlatform::PC) \
	op(EOculusPlatform::Mobile) \
	op(EOculusPlatform::Length) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
