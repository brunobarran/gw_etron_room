// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OptitrackOculusEditor/Public/OculusEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusEditorSettings() {}
// Cross Module References
	OPTITRACKOCULUSEDITOR_API UEnum* Z_Construct_UEnum_OptitrackOculusEditor_EOculusPlatform();
	UPackage* Z_Construct_UPackage__Script_OptitrackOculusEditor();
	OPTITRACKOCULUSEDITOR_API UClass* Z_Construct_UClass_UOptitrackOculusEditorSettings_NoRegister();
	OPTITRACKOCULUSEDITOR_API UClass* Z_Construct_UClass_UOptitrackOculusEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	static UEnum* EOculusPlatform_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OptitrackOculusEditor_EOculusPlatform, Z_Construct_UPackage__Script_OptitrackOculusEditor(), TEXT("EOculusPlatform"));
		}
		return Singleton;
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOculusPlatform(EOculusPlatform_StaticEnum, TEXT("/Script/OptitrackOculusEditor"), TEXT("EOculusPlatform"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OptitrackOculusEditor_EOculusPlatform_CRC() { return 1889645618U; }
	UEnum* Z_Construct_UEnum_OptitrackOculusEditor_EOculusPlatform()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackOculusEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOculusPlatform"), 0, Get_Z_Construct_UEnum_OptitrackOculusEditor_EOculusPlatform_CRC(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOculusPlatform::PC", (int64)EOculusPlatform::PC },
				{ "EOculusPlatform::Mobile", (int64)EOculusPlatform::Mobile },
				{ "EOculusPlatform::Length", (int64)EOculusPlatform::Length },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Length.DisplayName", "Invalid" },
				{ "Mobile.DisplayName", "Mobile" },
				{ "ModuleRelativePath", "Public/OculusEditorSettings.h" },
				{ "PC.DisplayName", "PC" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OptitrackOculusEditor,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				"EOculusPlatform",
				RF_Public|RF_Transient|RF_MarkAsNative,
				nullptr,
				(uint8)UEnum::ECppForm::EnumClass,
				"EOculusPlatform",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UOptitrackOculusEditorSettings::StaticRegisterNativesUOptitrackOculusEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UOptitrackOculusEditorSettings_NoRegister()
	{
		return UOptitrackOculusEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PerfToolTargetPlatform_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PerfToolTargetPlatform;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PerfToolTargetPlatform_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PerfToolIgnoreList_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_PerfToolIgnoreList;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PerfToolIgnoreList_Key_KeyProp;
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_PerfToolIgnoreList_ValueProp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackOculusEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "OculusEditorSettings.h" },
		{ "ModuleRelativePath", "Public/OculusEditorSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform = { UE4CodeGen_Private::EPropertyClass::Enum, "PerfToolTargetPlatform", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000004001, 1, nullptr, STRUCT_OFFSET(UOptitrackOculusEditorSettings, PerfToolTargetPlatform), Z_Construct_UEnum_OptitrackOculusEditor_EOculusPlatform, METADATA_PARAMS(Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList = { UE4CodeGen_Private::EPropertyClass::Map, "PerfToolIgnoreList", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000004001, 1, nullptr, STRUCT_OFFSET(UOptitrackOculusEditorSettings, PerfToolIgnoreList), METADATA_PARAMS(Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_Key_KeyProp = { UE4CodeGen_Private::EPropertyClass::Name, "PerfToolIgnoreList_Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000004001, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_ValueProp = { UE4CodeGen_Private::EPropertyClass::Bool, "PerfToolIgnoreList", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000004001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_ValueProp,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOptitrackOculusEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::ClassParams = {
		&UOptitrackOculusEditorSettings::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x001000A4u,
		nullptr, 0,
		Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::PropPointers),
		"Editor",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOptitrackOculusEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOptitrackOculusEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOptitrackOculusEditorSettings, 892665322);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOptitrackOculusEditorSettings(Z_Construct_UClass_UOptitrackOculusEditorSettings, &UOptitrackOculusEditorSettings::StaticClass, TEXT("/Script/OptitrackOculusEditor"), TEXT("UOptitrackOculusEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOptitrackOculusEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
