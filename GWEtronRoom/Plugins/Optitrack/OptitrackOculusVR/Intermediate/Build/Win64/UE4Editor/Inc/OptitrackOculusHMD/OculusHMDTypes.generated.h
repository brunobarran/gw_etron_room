// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPTITRACKOCULUSHMD_OculusHMDTypes_generated_h
#error "OculusHMDTypes.generated.h already included, missing '#pragma once' in OculusHMDTypes.h"
#endif
#define OPTITRACKOCULUSHMD_OculusHMDTypes_generated_h

#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDTypes_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOculusSplashDesc_Statics; \
	OPTITRACKOCULUSHMD_API static class UScriptStruct* StaticStruct();


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
