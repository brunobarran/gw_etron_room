// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OptitrackOculusHMD/Public/OculusHMDRuntimeSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusHMDRuntimeSettings() {}
// Cross Module References
	OPTITRACKOCULUSHMD_API UClass* Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_NoRegister();
	OPTITRACKOCULUSHMD_API UClass* Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_OptitrackOculusHMD();
	OPTITRACKOCULUSHMD_API UScriptStruct* Z_Construct_UScriptStruct_FOculusSplashDesc();
// End Cross Module References
	void UOptitrackOculusHMDRuntimeSettings::StaticRegisterNativesUOptitrackOculusHMDRuntimeSettings()
	{
	}
	UClass* Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_NoRegister()
	{
		return UOptitrackOculusHMDRuntimeSettings::StaticClass();
	}
	struct Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SplashDescs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SplashDescs;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SplashDescs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoEnabled_MetaData[];
#endif
		static void NewProp_bAutoEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackOculusHMD,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "OculusHMDRuntimeSettings.h" },
		{ "ModuleRelativePath", "Public/OculusHMDRuntimeSettings.h" },
		{ "ToolTip", "Implements the settings for the OculusVR plugin." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_SplashDescs_MetaData[] = {
		{ "Category", "SplashScreen" },
		{ "ModuleRelativePath", "Public/OculusHMDRuntimeSettings.h" },
		{ "ToolTip", "An array of splash screen descriptors listing textures to show and their positions." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_SplashDescs = { UE4CodeGen_Private::EPropertyClass::Array, "SplashDescs", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000004001, 1, nullptr, STRUCT_OFFSET(UOptitrackOculusHMDRuntimeSettings, SplashDescs), METADATA_PARAMS(Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_SplashDescs_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_SplashDescs_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_SplashDescs_Inner = { UE4CodeGen_Private::EPropertyClass::Struct, "SplashDescs", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000004000, 1, nullptr, 0, Z_Construct_UScriptStruct_FOculusSplashDesc, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_bAutoEnabled_MetaData[] = {
		{ "Category", "SplashScreen" },
		{ "ModuleRelativePath", "Public/OculusHMDRuntimeSettings.h" },
		{ "ToolTip", "Whether the Splash screen is enabled." },
	};
#endif
	void Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_bAutoEnabled_SetBit(void* Obj)
	{
		((UOptitrackOculusHMDRuntimeSettings*)Obj)->bAutoEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_bAutoEnabled = { UE4CodeGen_Private::EPropertyClass::Bool, "bAutoEnabled", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000004001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UOptitrackOculusHMDRuntimeSettings), &Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_bAutoEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_bAutoEnabled_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_bAutoEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_SplashDescs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_SplashDescs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::NewProp_bAutoEnabled,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOptitrackOculusHMDRuntimeSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::ClassParams = {
		&UOptitrackOculusHMDRuntimeSettings::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x001000A6u,
		nullptr, 0,
		Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOptitrackOculusHMDRuntimeSettings, 1165925636);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOptitrackOculusHMDRuntimeSettings(Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings, &UOptitrackOculusHMDRuntimeSettings::StaticClass, TEXT("/Script/OptitrackOculusHMD"), TEXT("UOptitrackOculusHMDRuntimeSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOptitrackOculusHMDRuntimeSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
