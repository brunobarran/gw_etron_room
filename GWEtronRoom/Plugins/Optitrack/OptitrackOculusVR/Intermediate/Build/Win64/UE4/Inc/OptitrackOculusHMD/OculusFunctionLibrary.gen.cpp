// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OptitrackOculusHMD/Public/OculusFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusFunctionLibrary() {}
// Cross Module References
	OPTITRACKOCULUSHMD_API UEnum* Z_Construct_UEnum_OptitrackOculusHMD_EBoundaryType();
	UPackage* Z_Construct_UPackage__Script_OptitrackOculusHMD();
	OPTITRACKOCULUSHMD_API UEnum* Z_Construct_UEnum_OptitrackOculusHMD_ETiledMultiResLevel();
	OPTITRACKOCULUSHMD_API UEnum* Z_Construct_UEnum_OptitrackOculusHMD_ETrackedDeviceType();
	OPTITRACKOCULUSHMD_API UScriptStruct* Z_Construct_UScriptStruct_FGuardianTestResult();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	OPTITRACKOCULUSHMD_API UScriptStruct* Z_Construct_UScriptStruct_FHmdUserProfile();
	OPTITRACKOCULUSHMD_API UScriptStruct* Z_Construct_UScriptStruct_FHmdUserProfileField();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	OPTITRACKOCULUSHMD_API UClass* Z_Construct_UClass_UOptitrackOculusFunctionLibrary_NoRegister();
	OPTITRACKOCULUSHMD_API UClass* Z_Construct_UClass_UOptitrackOculusFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ClearLoadingSplashScreens();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingIcon();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters();
	HEADMOUNTEDDISPLAY_API UEnum* Z_Construct_UEnum_HeadMountedDisplay_EOrientPositionSelector();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon();
	OPTITRACKOCULUSHMD_API UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingSplashScreen();
// End Cross Module References
	static UEnum* EBoundaryType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OptitrackOculusHMD_EBoundaryType, Z_Construct_UPackage__Script_OptitrackOculusHMD(), TEXT("EBoundaryType"));
		}
		return Singleton;
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBoundaryType(EBoundaryType_StaticEnum, TEXT("/Script/OptitrackOculusHMD"), TEXT("EBoundaryType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OptitrackOculusHMD_EBoundaryType_CRC() { return 930197026U; }
	UEnum* Z_Construct_UEnum_OptitrackOculusHMD_EBoundaryType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackOculusHMD();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBoundaryType"), 0, Get_Z_Construct_UEnum_OptitrackOculusHMD_EBoundaryType_CRC(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBoundaryType::Boundary_Outer", (int64)EBoundaryType::Boundary_Outer },
				{ "EBoundaryType::Boundary_PlayArea", (int64)EBoundaryType::Boundary_PlayArea },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Boundary_Outer.DisplayName", "Outer Boundary" },
				{ "Boundary_PlayArea.DisplayName", "Play Area" },
				{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
				{ "ToolTip", "Guardian boundary types" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OptitrackOculusHMD,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				"EBoundaryType",
				RF_Public|RF_Transient|RF_MarkAsNative,
				nullptr,
				(uint8)UEnum::ECppForm::EnumClass,
				"EBoundaryType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETiledMultiResLevel_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OptitrackOculusHMD_ETiledMultiResLevel, Z_Construct_UPackage__Script_OptitrackOculusHMD(), TEXT("ETiledMultiResLevel"));
		}
		return Singleton;
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETiledMultiResLevel(ETiledMultiResLevel_StaticEnum, TEXT("/Script/OptitrackOculusHMD"), TEXT("ETiledMultiResLevel"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OptitrackOculusHMD_ETiledMultiResLevel_CRC() { return 4150469999U; }
	UEnum* Z_Construct_UEnum_OptitrackOculusHMD_ETiledMultiResLevel()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackOculusHMD();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETiledMultiResLevel"), 0, Get_Z_Construct_UEnum_OptitrackOculusHMD_ETiledMultiResLevel_CRC(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETiledMultiResLevel::ETiledMultiResLevel_Off", (int64)ETiledMultiResLevel::ETiledMultiResLevel_Off },
				{ "ETiledMultiResLevel::ETiledMultiResLevel_LMSLow", (int64)ETiledMultiResLevel::ETiledMultiResLevel_LMSLow },
				{ "ETiledMultiResLevel::ETiledMultiResLevel_LMSMedium", (int64)ETiledMultiResLevel::ETiledMultiResLevel_LMSMedium },
				{ "ETiledMultiResLevel::ETiledMultiResLevel_LMSHigh", (int64)ETiledMultiResLevel::ETiledMultiResLevel_LMSHigh },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OptitrackOculusHMD,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				"ETiledMultiResLevel",
				RF_Public|RF_Transient|RF_MarkAsNative,
				nullptr,
				(uint8)UEnum::ECppForm::EnumClass,
				"ETiledMultiResLevel",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETrackedDeviceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OptitrackOculusHMD_ETrackedDeviceType, Z_Construct_UPackage__Script_OptitrackOculusHMD(), TEXT("ETrackedDeviceType"));
		}
		return Singleton;
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETrackedDeviceType(ETrackedDeviceType_StaticEnum, TEXT("/Script/OptitrackOculusHMD"), TEXT("ETrackedDeviceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OptitrackOculusHMD_ETrackedDeviceType_CRC() { return 247362528U; }
	UEnum* Z_Construct_UEnum_OptitrackOculusHMD_ETrackedDeviceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackOculusHMD();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETrackedDeviceType"), 0, Get_Z_Construct_UEnum_OptitrackOculusHMD_ETrackedDeviceType_CRC(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETrackedDeviceType::None", (int64)ETrackedDeviceType::None },
				{ "ETrackedDeviceType::HMD", (int64)ETrackedDeviceType::HMD },
				{ "ETrackedDeviceType::LTouch", (int64)ETrackedDeviceType::LTouch },
				{ "ETrackedDeviceType::RTouch", (int64)ETrackedDeviceType::RTouch },
				{ "ETrackedDeviceType::Touch", (int64)ETrackedDeviceType::Touch },
				{ "ETrackedDeviceType::DeviceObjectZero", (int64)ETrackedDeviceType::DeviceObjectZero },
				{ "ETrackedDeviceType::All", (int64)ETrackedDeviceType::All },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "All.DisplayName", "All Devices" },
				{ "BlueprintType", "true" },
				{ "DeviceObjectZero.DisplayName", "DeviceObject Zero" },
				{ "HMD.DisplayName", "HMD" },
				{ "LTouch.DisplayName", "Left Hand" },
				{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
				{ "None.DisplayName", "No Devices" },
				{ "RTouch.DisplayName", "Right Hand" },
				{ "ToolTip", "Tracked device types corresponding to ovrTrackedDeviceType enum" },
				{ "Touch.DisplayName", "All Hands" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OptitrackOculusHMD,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				"ETrackedDeviceType",
				RF_Public|RF_Transient|RF_MarkAsNative,
				nullptr,
				(uint8)UEnum::ECppForm::EnumClass,
				"ETrackedDeviceType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FGuardianTestResult::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPTITRACKOCULUSHMD_API uint32 Get_Z_Construct_UScriptStruct_FGuardianTestResult_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGuardianTestResult, Z_Construct_UPackage__Script_OptitrackOculusHMD(), TEXT("GuardianTestResult"), sizeof(FGuardianTestResult), Get_Z_Construct_UScriptStruct_FGuardianTestResult_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGuardianTestResult(FGuardianTestResult::StaticStruct, TEXT("/Script/OptitrackOculusHMD"), TEXT("GuardianTestResult"), false, nullptr, nullptr);
static struct FScriptStruct_OptitrackOculusHMD_StaticRegisterNativesFGuardianTestResult
{
	FScriptStruct_OptitrackOculusHMD_StaticRegisterNativesFGuardianTestResult()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("GuardianTestResult")),new UScriptStruct::TCppStructOps<FGuardianTestResult>);
	}
} ScriptStruct_OptitrackOculusHMD_StaticRegisterNativesFGuardianTestResult;
	struct Z_Construct_UScriptStruct_FGuardianTestResult_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClosestPointNormal_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClosestPointNormal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClosestPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClosestPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClosestDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClosestDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DeviceType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsTriggering_MetaData[];
#endif
		static void NewProp_IsTriggering_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsTriggering;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGuardianTestResult_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "* Information about relationships between a triggered boundary (EBoundaryType::Boundary_Outer or\n* EBoundaryType::Boundary_PlayArea) and a device or point in the world.\n* All dimensions, points, and vectors are returned in Unreal world coordinate space." },
	};
#endif
	void* Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGuardianTestResult>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestPointNormal_MetaData[] = {
		{ "Category", "Boundary Test Result" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Normal of closest point" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestPointNormal = { UE4CodeGen_Private::EPropertyClass::Struct, "ClosestPointNormal", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000015, 1, nullptr, STRUCT_OFFSET(FGuardianTestResult, ClosestPointNormal), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestPointNormal_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestPointNormal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestPoint_MetaData[] = {
		{ "Category", "Boundary Test Result" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Closest point on surface corresponding to specified boundary" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestPoint = { UE4CodeGen_Private::EPropertyClass::Struct, "ClosestPoint", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000015, 1, nullptr, STRUCT_OFFSET(FGuardianTestResult, ClosestPoint), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestPoint_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestDistance_MetaData[] = {
		{ "Category", "Boundary Test Result" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Distance of device/point to surface of boundary specified by BoundaryType" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestDistance = { UE4CodeGen_Private::EPropertyClass::Float, "ClosestDistance", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000015, 1, nullptr, STRUCT_OFFSET(FGuardianTestResult, ClosestDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestDistance_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_DeviceType_MetaData[] = {
		{ "Category", "Boundary Test Result" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Device type triggering boundary (ETrackedDeviceType::None if BoundaryTestResult corresponds to a point rather than a device)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_DeviceType = { UE4CodeGen_Private::EPropertyClass::Enum, "DeviceType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000015, 1, nullptr, STRUCT_OFFSET(FGuardianTestResult, DeviceType), Z_Construct_UEnum_OptitrackOculusHMD_ETrackedDeviceType, METADATA_PARAMS(Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_DeviceType_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_DeviceType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_DeviceType_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_IsTriggering_MetaData[] = {
		{ "Category", "Boundary Test Result" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Is there a triggering interaction between the device/point and specified boundary?" },
	};
#endif
	void Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_IsTriggering_SetBit(void* Obj)
	{
		((FGuardianTestResult*)Obj)->IsTriggering = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_IsTriggering = { UE4CodeGen_Private::EPropertyClass::Bool, "IsTriggering", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000015, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(FGuardianTestResult), &Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_IsTriggering_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_IsTriggering_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_IsTriggering_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGuardianTestResult_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestPointNormal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_ClosestDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_DeviceType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_DeviceType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGuardianTestResult_Statics::NewProp_IsTriggering,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGuardianTestResult_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackOculusHMD,
		nullptr,
		&NewStructOps,
		"GuardianTestResult",
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		sizeof(FGuardianTestResult),
		alignof(FGuardianTestResult),
		Z_Construct_UScriptStruct_FGuardianTestResult_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UScriptStruct_FGuardianTestResult_Statics::PropPointers),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGuardianTestResult_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FGuardianTestResult_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGuardianTestResult()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGuardianTestResult_CRC();
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackOculusHMD();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GuardianTestResult"), sizeof(FGuardianTestResult), Get_Z_Construct_UScriptStruct_FGuardianTestResult_CRC(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGuardianTestResult_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGuardianTestResult_CRC() { return 4137458273U; }
class UScriptStruct* FHmdUserProfile::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPTITRACKOCULUSHMD_API uint32 Get_Z_Construct_UScriptStruct_FHmdUserProfile_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHmdUserProfile, Z_Construct_UPackage__Script_OptitrackOculusHMD(), TEXT("HmdUserProfile"), sizeof(FHmdUserProfile), Get_Z_Construct_UScriptStruct_FHmdUserProfile_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHmdUserProfile(FHmdUserProfile::StaticStruct, TEXT("/Script/OptitrackOculusHMD"), TEXT("HmdUserProfile"), false, nullptr, nullptr);
static struct FScriptStruct_OptitrackOculusHMD_StaticRegisterNativesFHmdUserProfile
{
	FScriptStruct_OptitrackOculusHMD_StaticRegisterNativesFHmdUserProfile()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("HmdUserProfile")),new UScriptStruct::TCppStructOps<FHmdUserProfile>);
	}
} ScriptStruct_OptitrackOculusHMD_StaticRegisterNativesFHmdUserProfile;
	struct Z_Construct_UScriptStruct_FHmdUserProfile_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExtraFields_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExtraFields;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExtraFields_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NeckToEyeDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NeckToEyeDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IPD_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_IPD;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EyeHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EyeHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PlayerHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Gender_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Gender;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHmdUserProfile_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "HMD User Profile Data" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHmdUserProfile>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_ExtraFields_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_ExtraFields = { UE4CodeGen_Private::EPropertyClass::Array, "ExtraFields", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(FHmdUserProfile, ExtraFields), METADATA_PARAMS(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_ExtraFields_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_ExtraFields_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_ExtraFields_Inner = { UE4CodeGen_Private::EPropertyClass::Struct, "ExtraFields", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UScriptStruct_FHmdUserProfileField, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_NeckToEyeDistance_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Neck-to-eye distance, in meters. X - horizontal, Y - vertical." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_NeckToEyeDistance = { UE4CodeGen_Private::EPropertyClass::Struct, "NeckToEyeDistance", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(FHmdUserProfile, NeckToEyeDistance), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_NeckToEyeDistance_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_NeckToEyeDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_IPD_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Interpupillary distance of the player, in meters" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_IPD = { UE4CodeGen_Private::EPropertyClass::Float, "IPD", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(FHmdUserProfile, IPD), METADATA_PARAMS(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_IPD_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_IPD_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_EyeHeight_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Height of the player, in meters" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_EyeHeight = { UE4CodeGen_Private::EPropertyClass::Float, "EyeHeight", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(FHmdUserProfile, EyeHeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_EyeHeight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_EyeHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_PlayerHeight_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Height of the player, in meters" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_PlayerHeight = { UE4CodeGen_Private::EPropertyClass::Float, "PlayerHeight", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(FHmdUserProfile, PlayerHeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_PlayerHeight_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_PlayerHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_Gender_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Gender of the user (\"male\", \"female\", etc)." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_Gender = { UE4CodeGen_Private::EPropertyClass::Str, "Gender", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(FHmdUserProfile, Gender), METADATA_PARAMS(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_Gender_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_Gender_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Name of the user's profile." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_Name = { UE4CodeGen_Private::EPropertyClass::Str, "Name", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(FHmdUserProfile, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_Name_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHmdUserProfile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_ExtraFields,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_ExtraFields_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_NeckToEyeDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_IPD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_EyeHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_PlayerHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_Gender,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHmdUserProfile_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHmdUserProfile_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackOculusHMD,
		nullptr,
		&NewStructOps,
		"HmdUserProfile",
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		sizeof(FHmdUserProfile),
		alignof(FHmdUserProfile),
		Z_Construct_UScriptStruct_FHmdUserProfile_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::PropPointers),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FHmdUserProfile_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHmdUserProfile()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHmdUserProfile_CRC();
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackOculusHMD();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HmdUserProfile"), sizeof(FHmdUserProfile), Get_Z_Construct_UScriptStruct_FHmdUserProfile_CRC(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHmdUserProfile_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHmdUserProfile_CRC() { return 2032135079U; }
class UScriptStruct* FHmdUserProfileField::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPTITRACKOCULUSHMD_API uint32 Get_Z_Construct_UScriptStruct_FHmdUserProfileField_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FHmdUserProfileField, Z_Construct_UPackage__Script_OptitrackOculusHMD(), TEXT("HmdUserProfileField"), sizeof(FHmdUserProfileField), Get_Z_Construct_UScriptStruct_FHmdUserProfileField_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FHmdUserProfileField(FHmdUserProfileField::StaticStruct, TEXT("/Script/OptitrackOculusHMD"), TEXT("HmdUserProfileField"), false, nullptr, nullptr);
static struct FScriptStruct_OptitrackOculusHMD_StaticRegisterNativesFHmdUserProfileField
{
	FScriptStruct_OptitrackOculusHMD_StaticRegisterNativesFHmdUserProfileField()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("HmdUserProfileField")),new UScriptStruct::TCppStructOps<FHmdUserProfileField>);
	}
} ScriptStruct_OptitrackOculusHMD_StaticRegisterNativesFHmdUserProfileField;
	struct Z_Construct_UScriptStruct_FHmdUserProfileField_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FieldValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FieldName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "HMD User Profile Data Field" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FHmdUserProfileField>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::NewProp_FieldValue_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::NewProp_FieldValue = { UE4CodeGen_Private::EPropertyClass::Str, "FieldValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(FHmdUserProfileField, FieldValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::NewProp_FieldValue_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::NewProp_FieldValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::NewProp_FieldName_MetaData[] = {
		{ "Category", "Input|HeadMountedDisplay" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::NewProp_FieldName = { UE4CodeGen_Private::EPropertyClass::Str, "FieldName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000004, 1, nullptr, STRUCT_OFFSET(FHmdUserProfileField, FieldName), METADATA_PARAMS(Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::NewProp_FieldName_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::NewProp_FieldName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::NewProp_FieldValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::NewProp_FieldName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackOculusHMD,
		nullptr,
		&NewStructOps,
		"HmdUserProfileField",
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		sizeof(FHmdUserProfileField),
		alignof(FHmdUserProfileField),
		Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::PropPointers),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FHmdUserProfileField()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FHmdUserProfileField_CRC();
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackOculusHMD();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("HmdUserProfileField"), sizeof(FHmdUserProfileField), Get_Z_Construct_UScriptStruct_FHmdUserProfileField_CRC(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FHmdUserProfileField_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FHmdUserProfileField_CRC() { return 1107622573U; }
	void UOptitrackOculusFunctionLibrary::StaticRegisterNativesUOptitrackOculusFunctionLibrary()
	{
		UClass* Class = UOptitrackOculusFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddLoadingSplashScreen", &UOptitrackOculusFunctionLibrary::execAddLoadingSplashScreen },
			{ "ClearLoadingSplashScreens", &UOptitrackOculusFunctionLibrary::execClearLoadingSplashScreens },
			{ "EnableAutoLoadingSplashScreen", &UOptitrackOculusFunctionLibrary::execEnableAutoLoadingSplashScreen },
			{ "EnableOrientationTracking", &UOptitrackOculusFunctionLibrary::execEnableOrientationTracking },
			{ "EnablePositionTracking", &UOptitrackOculusFunctionLibrary::execEnablePositionTracking },
			{ "GetAvailableDisplayFrequencies", &UOptitrackOculusFunctionLibrary::execGetAvailableDisplayFrequencies },
			{ "GetBaseRotationAndBaseOffsetInMeters", &UOptitrackOculusFunctionLibrary::execGetBaseRotationAndBaseOffsetInMeters },
			{ "GetBaseRotationAndPositionOffset", &UOptitrackOculusFunctionLibrary::execGetBaseRotationAndPositionOffset },
			{ "GetCurrentDisplayFrequency", &UOptitrackOculusFunctionLibrary::execGetCurrentDisplayFrequency },
			{ "GetDeviceName", &UOptitrackOculusFunctionLibrary::execGetDeviceName },
			{ "GetGPUFrameTime", &UOptitrackOculusFunctionLibrary::execGetGPUFrameTime },
			{ "GetGPUUtilization", &UOptitrackOculusFunctionLibrary::execGetGPUUtilization },
			{ "GetGuardianDimensions", &UOptitrackOculusFunctionLibrary::execGetGuardianDimensions },
			{ "GetGuardianPoints", &UOptitrackOculusFunctionLibrary::execGetGuardianPoints },
			{ "GetLoadingSplashParams", &UOptitrackOculusFunctionLibrary::execGetLoadingSplashParams },
			{ "GetNodeGuardianIntersection", &UOptitrackOculusFunctionLibrary::execGetNodeGuardianIntersection },
			{ "GetPlayAreaTransform", &UOptitrackOculusFunctionLibrary::execGetPlayAreaTransform },
			{ "GetPointGuardianIntersection", &UOptitrackOculusFunctionLibrary::execGetPointGuardianIntersection },
			{ "GetPose", &UOptitrackOculusFunctionLibrary::execGetPose },
			{ "GetRawSensorData", &UOptitrackOculusFunctionLibrary::execGetRawSensorData },
			{ "GetTiledMultiresLevel", &UOptitrackOculusFunctionLibrary::execGetTiledMultiresLevel },
			{ "GetUserProfile", &UOptitrackOculusFunctionLibrary::execGetUserProfile },
			{ "HasInputFocus", &UOptitrackOculusFunctionLibrary::execHasInputFocus },
			{ "HasSystemOverlayPresent", &UOptitrackOculusFunctionLibrary::execHasSystemOverlayPresent },
			{ "HideLoadingIcon", &UOptitrackOculusFunctionLibrary::execHideLoadingIcon },
			{ "HideLoadingSplashScreen", &UOptitrackOculusFunctionLibrary::execHideLoadingSplashScreen },
			{ "IsAutoLoadingSplashScreenEnabled", &UOptitrackOculusFunctionLibrary::execIsAutoLoadingSplashScreenEnabled },
			{ "IsDeviceTracked", &UOptitrackOculusFunctionLibrary::execIsDeviceTracked },
			{ "IsGuardianDisplayed", &UOptitrackOculusFunctionLibrary::execIsGuardianDisplayed },
			{ "IsLoadingIconEnabled", &UOptitrackOculusFunctionLibrary::execIsLoadingIconEnabled },
			{ "SetBaseRotationAndBaseOffsetInMeters", &UOptitrackOculusFunctionLibrary::execSetBaseRotationAndBaseOffsetInMeters },
			{ "SetBaseRotationAndPositionOffset", &UOptitrackOculusFunctionLibrary::execSetBaseRotationAndPositionOffset },
			{ "SetCPUAndGPULevels", &UOptitrackOculusFunctionLibrary::execSetCPUAndGPULevels },
			{ "SetDisplayFrequency", &UOptitrackOculusFunctionLibrary::execSetDisplayFrequency },
			{ "SetGuardianVisibility", &UOptitrackOculusFunctionLibrary::execSetGuardianVisibility },
			{ "SetLoadingSplashParams", &UOptitrackOculusFunctionLibrary::execSetLoadingSplashParams },
			{ "SetPositionScale3D", &UOptitrackOculusFunctionLibrary::execSetPositionScale3D },
			{ "SetReorientHMDOnControllerRecenter", &UOptitrackOculusFunctionLibrary::execSetReorientHMDOnControllerRecenter },
			{ "SetTiledMultiresLevel", &UOptitrackOculusFunctionLibrary::execSetTiledMultiresLevel },
			{ "ShowLoadingIcon", &UOptitrackOculusFunctionLibrary::execShowLoadingIcon },
			{ "ShowLoadingSplashScreen", &UOptitrackOculusFunctionLibrary::execShowLoadingSplashScreen },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventAddLoadingSplashScreen_Parms
		{
			UTexture2D* Texture;
			FVector TranslationInMeters;
			FRotator Rotation;
			FVector2D SizeInMeters;
			FRotator DeltaRotation;
			bool bClearBeforeAdd;
		};
		static void NewProp_bClearBeforeAdd_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClearBeforeAdd;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DeltaRotation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SizeInMeters;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TranslationInMeters;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Texture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_bClearBeforeAdd_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventAddLoadingSplashScreen_Parms*)Obj)->bClearBeforeAdd = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_bClearBeforeAdd = { UE4CodeGen_Private::EPropertyClass::Bool, "bClearBeforeAdd", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventAddLoadingSplashScreen_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_bClearBeforeAdd_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_DeltaRotation = { UE4CodeGen_Private::EPropertyClass::Struct, "DeltaRotation", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventAddLoadingSplashScreen_Parms, DeltaRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_SizeInMeters = { UE4CodeGen_Private::EPropertyClass::Struct, "SizeInMeters", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventAddLoadingSplashScreen_Parms, SizeInMeters), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_Rotation = { UE4CodeGen_Private::EPropertyClass::Struct, "Rotation", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventAddLoadingSplashScreen_Parms, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_TranslationInMeters = { UE4CodeGen_Private::EPropertyClass::Struct, "TranslationInMeters", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventAddLoadingSplashScreen_Parms, TranslationInMeters), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_Texture = { UE4CodeGen_Private::EPropertyClass::Object, "Texture", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventAddLoadingSplashScreen_Parms, Texture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_bClearBeforeAdd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_DeltaRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_SizeInMeters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_TranslationInMeters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::NewProp_Texture,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "CPP_Default_bClearBeforeAdd", "false" },
		{ "CPP_Default_DeltaRotation", "" },
		{ "CPP_Default_SizeInMeters", "(X=1.000,Y=1.000)" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Adds loading splash screen with parameters\n\n@param Texture                       (in) A texture asset to be used for the splash. Gear VR uses it as a path for loading icon; all other params are currently ignored by Gear VR.\n@param TranslationInMeters (in) Initial translation of the center of the splash screen (in meters).\n@param Rotation                      (in) Initial rotation of the splash screen, with the origin at the center of the splash screen.\n@param SizeInMeters          (in) Size, in meters, of the quad with the splash screen.\n@param DeltaRotation         (in) Incremental rotation, that is added each 2nd frame to the quad transform. The quad is rotated around the center of the quad.\n@param bClearBeforeAdd       (in) If true, clears splashes before adding a new one." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "AddLoadingSplashScreen", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(OptitrackOculusFunctionLibrary_eventAddLoadingSplashScreen_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ClearLoadingSplashScreens_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ClearLoadingSplashScreens_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Removes all the splash screens." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ClearLoadingSplashScreens_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "ClearLoadingSplashScreens", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ClearLoadingSplashScreens_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ClearLoadingSplashScreens_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ClearLoadingSplashScreens()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ClearLoadingSplashScreens_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventEnableAutoLoadingSplashScreen_Parms
		{
			bool bAutoShowEnabled;
		};
		static void NewProp_bAutoShowEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoShowEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen_Statics::NewProp_bAutoShowEnabled_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventEnableAutoLoadingSplashScreen_Parms*)Obj)->bAutoShowEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen_Statics::NewProp_bAutoShowEnabled = { UE4CodeGen_Private::EPropertyClass::Bool, "bAutoShowEnabled", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventEnableAutoLoadingSplashScreen_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen_Statics::NewProp_bAutoShowEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen_Statics::NewProp_bAutoShowEnabled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Enables/disables splash screen to be automatically shown when LoadMap is called.\n\n@param       bAutoShowEnabled        (in)    True, if automatic showing of splash screens is enabled when map is being loaded." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "EnableAutoLoadingSplashScreen", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OptitrackOculusFunctionLibrary_eventEnableAutoLoadingSplashScreen_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventEnableOrientationTracking_Parms
		{
			bool bOrientationTracking;
		};
		static void NewProp_bOrientationTracking_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOrientationTracking;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking_Statics::NewProp_bOrientationTracking_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventEnableOrientationTracking_Parms*)Obj)->bOrientationTracking = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking_Statics::NewProp_bOrientationTracking = { UE4CodeGen_Private::EPropertyClass::Bool, "bOrientationTracking", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventEnableOrientationTracking_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking_Statics::NewProp_bOrientationTracking_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking_Statics::NewProp_bOrientationTracking,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Enables/disables orientation tracking on devices that support it." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "EnableOrientationTracking", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OptitrackOculusFunctionLibrary_eventEnableOrientationTracking_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventEnablePositionTracking_Parms
		{
			bool bPositionTracking;
		};
		static void NewProp_bPositionTracking_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPositionTracking;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking_Statics::NewProp_bPositionTracking_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventEnablePositionTracking_Parms*)Obj)->bPositionTracking = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking_Statics::NewProp_bPositionTracking = { UE4CodeGen_Private::EPropertyClass::Bool, "bPositionTracking", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventEnablePositionTracking_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking_Statics::NewProp_bPositionTracking_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking_Statics::NewProp_bPositionTracking,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Enables/disables positional tracking on devices that support it." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "EnablePositionTracking", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OptitrackOculusFunctionLibrary_eventEnablePositionTracking_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetAvailableDisplayFrequencies_Parms
		{
			TArray<float> ReturnValue;
		};
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Array, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetAvailableDisplayFrequencies_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies_Statics::NewProp_ReturnValue_Inner = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies_Statics::NewProp_ReturnValue_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns the current available frequencies" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetAvailableDisplayFrequencies", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OptitrackOculusFunctionLibrary_eventGetAvailableDisplayFrequencies_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetBaseRotationAndBaseOffsetInMeters_Parms
		{
			FRotator OutRotation;
			FVector OutBaseOffsetInMeters;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutBaseOffsetInMeters;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutRotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_OutBaseOffsetInMeters = { UE4CodeGen_Private::EPropertyClass::Struct, "OutBaseOffsetInMeters", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetBaseRotationAndBaseOffsetInMeters_Parms, OutBaseOffsetInMeters), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_OutRotation = { UE4CodeGen_Private::EPropertyClass::Struct, "OutRotation", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetBaseRotationAndBaseOffsetInMeters_Parms, OutRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_OutBaseOffsetInMeters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_OutRotation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns current base rotation and base offset.\nThe base offset is currently used base position offset, previously set by the\nResetPosition or SetBasePositionOffset calls. It represents a vector that translates the HMD's position\ninto (0,0,0) point, in meters.\nThe axis of the vector are the same as in Unreal: X - forward, Y - right, Z - up.\n\n@param OutRotation                    (out) Rotator object with base rotation\n@param OutBaseOffsetInMeters  (out) base position offset, vector, in meters." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetBaseRotationAndBaseOffsetInMeters", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14C22401, sizeof(OptitrackOculusFunctionLibrary_eventGetBaseRotationAndBaseOffsetInMeters_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetBaseRotationAndPositionOffset_Parms
		{
			FRotator OutRot;
			FVector OutPosOffset;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutPosOffset;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutRot;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::NewProp_OutPosOffset = { UE4CodeGen_Private::EPropertyClass::Struct, "OutPosOffset", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetBaseRotationAndPositionOffset_Parms, OutPosOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::NewProp_OutRot = { UE4CodeGen_Private::EPropertyClass::Struct, "OutRot", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetBaseRotationAndPositionOffset_Parms, OutRot), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::NewProp_OutPosOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::NewProp_OutRot,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "A hack, proper camera positioning should be used" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns current base rotation and position offset.\n\n@param OutRot                        (out) Rotator object with base rotation\n@param OutPosOffset          (out) the vector with previously set position offset." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetBaseRotationAndPositionOffset", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(OptitrackOculusFunctionLibrary_eventGetBaseRotationAndPositionOffset_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetCurrentDisplayFrequency_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetCurrentDisplayFrequency_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns the current display frequency" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetCurrentDisplayFrequency", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OptitrackOculusFunctionLibrary_eventGetCurrentDisplayFrequency_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetDeviceName_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetDeviceName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns the current device's name" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetDeviceName", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OptitrackOculusFunctionLibrary_eventGetDeviceName_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetGPUFrameTime_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetGPUFrameTime_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns the GPU frame time on supported mobile platforms (Go for now)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetGPUFrameTime", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OptitrackOculusFunctionLibrary_eventGetGPUFrameTime_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetGPUUtilization_Parms
		{
			bool IsGPUAvailable;
			float GPUUtilization;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GPUUtilization;
		static void NewProp_IsGPUAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsGPUAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::NewProp_GPUUtilization = { UE4CodeGen_Private::EPropertyClass::Float, "GPUUtilization", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetGPUUtilization_Parms, GPUUtilization), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::NewProp_IsGPUAvailable_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventGetGPUUtilization_Parms*)Obj)->IsGPUAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::NewProp_IsGPUAvailable = { UE4CodeGen_Private::EPropertyClass::Bool, "IsGPUAvailable", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventGetGPUUtilization_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::NewProp_IsGPUAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::NewProp_GPUUtilization,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::NewProp_IsGPUAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns the GPU utilization availability and value" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetGPUUtilization", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(OptitrackOculusFunctionLibrary_eventGetGPUUtilization_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetGuardianDimensions_Parms
		{
			EBoundaryType BoundaryType;
			FVector ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BoundaryType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BoundaryType_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Struct, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetGuardianDimensions_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::NewProp_BoundaryType = { UE4CodeGen_Private::EPropertyClass::Enum, "BoundaryType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetGuardianDimensions_Parms, BoundaryType), Z_Construct_UEnum_OptitrackOculusHMD_EBoundaryType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::NewProp_BoundaryType_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::NewProp_BoundaryType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::NewProp_BoundaryType_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns the dimensions in UE world space of the requested Boundary Type\n@param BoundaryType                   (in) An enum representing the boundary type requested, either Outer Boundary (exact guardian bounds) or PlayArea (rectangle inside the Outer Boundary)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetGuardianDimensions", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14822401, sizeof(OptitrackOculusFunctionLibrary_eventGetGuardianDimensions_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetGuardianPoints_Parms
		{
			EBoundaryType BoundaryType;
			TArray<FVector> ReturnValue;
		};
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BoundaryType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BoundaryType_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Array, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetGuardianPoints_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::NewProp_ReturnValue_Inner = { UE4CodeGen_Private::EPropertyClass::Struct, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::NewProp_BoundaryType = { UE4CodeGen_Private::EPropertyClass::Enum, "BoundaryType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetGuardianPoints_Parms, BoundaryType), Z_Construct_UEnum_OptitrackOculusHMD_EBoundaryType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::NewProp_BoundaryType_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::NewProp_BoundaryType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::NewProp_BoundaryType_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns the list of points in UE world space of the requested Boundary Type\n@param BoundaryType                   (in) An enum representing the boundary type requested, either Outer Boundary (exact guardian bounds) or PlayArea (rectangle inside the Outer Boundary)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetGuardianPoints", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OptitrackOculusFunctionLibrary_eventGetGuardianPoints_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetLoadingSplashParams_Parms
		{
			FString TexturePath;
			FVector DistanceInMeters;
			FVector2D SizeInMeters;
			FVector RotationAxis;
			float RotationDeltaInDeg;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RotationDeltaInDeg;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotationAxis;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SizeInMeters;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistanceInMeters;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TexturePath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::NewProp_RotationDeltaInDeg = { UE4CodeGen_Private::EPropertyClass::Float, "RotationDeltaInDeg", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetLoadingSplashParams_Parms, RotationDeltaInDeg), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::NewProp_RotationAxis = { UE4CodeGen_Private::EPropertyClass::Struct, "RotationAxis", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetLoadingSplashParams_Parms, RotationAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::NewProp_SizeInMeters = { UE4CodeGen_Private::EPropertyClass::Struct, "SizeInMeters", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetLoadingSplashParams_Parms, SizeInMeters), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::NewProp_DistanceInMeters = { UE4CodeGen_Private::EPropertyClass::Struct, "DistanceInMeters", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetLoadingSplashParams_Parms, DistanceInMeters), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::NewProp_TexturePath = { UE4CodeGen_Private::EPropertyClass::Str, "TexturePath", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetLoadingSplashParams_Parms, TexturePath), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::NewProp_RotationDeltaInDeg,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::NewProp_RotationAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::NewProp_SizeInMeters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::NewProp_DistanceInMeters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::NewProp_TexturePath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "use AddLoadingSplashScreen / ClearLoadingSplashScreens" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns loading splash screen parameters.\n\n@param TexturePath           (out) A path to the texture asset to be used for the splash. Gear VR uses it as a path for loading icon; all other params are currently ignored by Gear VR.\n@param DistanceInMeters      (out) Distance, in meters, to the center of the splash screen.\n@param SizeInMeters          (out) Size, in meters, of the quad with the splash screen.\n@param RotationAxes          (out) A vector that specifies the axis of the splash screen rotation (if RotationDelta is specified).\n@param RotationDeltaInDeg (out) Rotation delta, in degrees, that is added each 2nd frame to the quad transform. The quad is rotated around the vector \"RotationAxes\"." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetLoadingSplashParams", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14C22401, sizeof(OptitrackOculusFunctionLibrary_eventGetLoadingSplashParams_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetNodeGuardianIntersection_Parms
		{
			ETrackedDeviceType DeviceType;
			EBoundaryType BoundaryType;
			FGuardianTestResult ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BoundaryType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BoundaryType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DeviceType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceType_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Struct, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetNodeGuardianIntersection_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuardianTestResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_BoundaryType = { UE4CodeGen_Private::EPropertyClass::Enum, "BoundaryType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetNodeGuardianIntersection_Parms, BoundaryType), Z_Construct_UEnum_OptitrackOculusHMD_EBoundaryType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_BoundaryType_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_DeviceType = { UE4CodeGen_Private::EPropertyClass::Enum, "DeviceType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetNodeGuardianIntersection_Parms, DeviceType), Z_Construct_UEnum_OptitrackOculusHMD_ETrackedDeviceType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_DeviceType_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_BoundaryType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_BoundaryType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_DeviceType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::NewProp_DeviceType_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Get the intersection result between a tracked device (HMD or controllers) and a guardian boundary\n@param DeviceType             (in) Tracked Device type to test against guardian boundaries\n@param BoundaryType                   (in) An enum representing the boundary type requested, either Outer Boundary (exact guardian bounds) or PlayArea (rectangle inside the Outer Boundary)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetNodeGuardianIntersection", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OptitrackOculusFunctionLibrary_eventGetNodeGuardianIntersection_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetPlayAreaTransform_Parms
		{
			FTransform ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Struct, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetPlayAreaTransform_Parms, ReturnValue), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns the transform of the play area rectangle, defining its position, rotation and scale to apply to a unit cube to match it with the play area." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetPlayAreaTransform", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14822401, sizeof(OptitrackOculusFunctionLibrary_eventGetPlayAreaTransform_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetPointGuardianIntersection_Parms
		{
			FVector Point;
			EBoundaryType BoundaryType;
			FGuardianTestResult ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BoundaryType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BoundaryType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Point_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Point;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Struct, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetPointGuardianIntersection_Parms, ReturnValue), Z_Construct_UScriptStruct_FGuardianTestResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_BoundaryType = { UE4CodeGen_Private::EPropertyClass::Enum, "BoundaryType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetPointGuardianIntersection_Parms, BoundaryType), Z_Construct_UEnum_OptitrackOculusHMD_EBoundaryType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_BoundaryType_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_Point_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_Point = { UE4CodeGen_Private::EPropertyClass::Struct, "Point", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetPointGuardianIntersection_Parms, Point), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_Point_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_Point_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_BoundaryType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_BoundaryType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::NewProp_Point,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Get the intersection result between a UE4 coordinate and a guardian boundary\n@param Point                                  (in) Point in UE space to test against guardian boundaries\n@param BoundaryType                   (in) An enum representing the boundary type requested, either Outer Boundary (exact guardian bounds) or PlayArea (rectangle inside the Outer Boundary)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetPointGuardianIntersection", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(OptitrackOculusFunctionLibrary_eventGetPointGuardianIntersection_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetPose_Parms
		{
			FRotator DeviceRotation;
			FVector DevicePosition;
			FVector NeckPosition;
			bool bUseOrienationForPlayerCamera;
			bool bUsePositionForPlayerCamera;
			FVector PositionScale;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PositionScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PositionScale;
		static void NewProp_bUsePositionForPlayerCamera_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUsePositionForPlayerCamera;
		static void NewProp_bUseOrienationForPlayerCamera_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseOrienationForPlayerCamera;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NeckPosition;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DevicePosition;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DeviceRotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_PositionScale_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_PositionScale = { UE4CodeGen_Private::EPropertyClass::Struct, "PositionScale", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetPose_Parms, PositionScale), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_PositionScale_MetaData, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_PositionScale_MetaData)) };
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_bUsePositionForPlayerCamera_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventGetPose_Parms*)Obj)->bUsePositionForPlayerCamera = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_bUsePositionForPlayerCamera = { UE4CodeGen_Private::EPropertyClass::Bool, "bUsePositionForPlayerCamera", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventGetPose_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_bUsePositionForPlayerCamera_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_bUseOrienationForPlayerCamera_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventGetPose_Parms*)Obj)->bUseOrienationForPlayerCamera = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_bUseOrienationForPlayerCamera = { UE4CodeGen_Private::EPropertyClass::Bool, "bUseOrienationForPlayerCamera", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventGetPose_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_bUseOrienationForPlayerCamera_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_NeckPosition = { UE4CodeGen_Private::EPropertyClass::Struct, "NeckPosition", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetPose_Parms, NeckPosition), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_DevicePosition = { UE4CodeGen_Private::EPropertyClass::Struct, "DevicePosition", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetPose_Parms, DevicePosition), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_DeviceRotation = { UE4CodeGen_Private::EPropertyClass::Struct, "DeviceRotation", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetPose_Parms, DeviceRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_PositionScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_bUsePositionForPlayerCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_bUseOrienationForPlayerCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_NeckPosition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_DevicePosition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::NewProp_DeviceRotation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "CPP_Default_bUseOrienationForPlayerCamera", "false" },
		{ "CPP_Default_bUsePositionForPlayerCamera", "false" },
		{ "CPP_Default_PositionScale", "" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Grabs the current orientation and position for the HMD.  If positional tracking is not available, DevicePosition will be a zero vector\n\n@param DeviceRotation        (out) The device's current rotation\n@param DevicePosition        (out) The device's current position, in its own tracking space\n@param NeckPosition          (out) The estimated neck position, calculated using NeckToEye vector from User Profile. Same coordinate space as DevicePosition.\n@param bUseOrienationForPlayerCamera (in) Should be set to 'true' if the orientation is going to be used to update orientation of the camera manually.\n@param bUsePositionForPlayerCamera   (in) Should be set to 'true' if the position is going to be used to update position of the camera manually.\n@param PositionScale         (in) The 3D scale that will be applied to position." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetPose", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14C22401, sizeof(OptitrackOculusFunctionLibrary_eventGetPose_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetRawSensorData_Parms
		{
			FVector AngularAcceleration;
			FVector LinearAcceleration;
			FVector AngularVelocity;
			FVector LinearVelocity;
			float TimeInSeconds;
			ETrackedDeviceType DeviceType;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DeviceType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceType_Underlying;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeInSeconds;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LinearVelocity;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AngularVelocity;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LinearAcceleration;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AngularAcceleration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_DeviceType = { UE4CodeGen_Private::EPropertyClass::Enum, "DeviceType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetRawSensorData_Parms, DeviceType), Z_Construct_UEnum_OptitrackOculusHMD_ETrackedDeviceType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_DeviceType_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_TimeInSeconds = { UE4CodeGen_Private::EPropertyClass::Float, "TimeInSeconds", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetRawSensorData_Parms, TimeInSeconds), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_LinearVelocity = { UE4CodeGen_Private::EPropertyClass::Struct, "LinearVelocity", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetRawSensorData_Parms, LinearVelocity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_AngularVelocity = { UE4CodeGen_Private::EPropertyClass::Struct, "AngularVelocity", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetRawSensorData_Parms, AngularVelocity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_LinearAcceleration = { UE4CodeGen_Private::EPropertyClass::Struct, "LinearAcceleration", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetRawSensorData_Parms, LinearAcceleration), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_AngularAcceleration = { UE4CodeGen_Private::EPropertyClass::Struct, "AngularAcceleration", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetRawSensorData_Parms, AngularAcceleration), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_DeviceType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_DeviceType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_TimeInSeconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_LinearVelocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_AngularVelocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_LinearAcceleration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::NewProp_AngularAcceleration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "CPP_Default_DeviceType", "HMD" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Reports raw sensor data. If HMD doesn't support any of the parameters then it will be set to zero.\n\n@param AngularAcceleration    (out) Angular acceleration in radians per second per second.\n@param LinearAcceleration             (out) Acceleration in meters per second per second.\n@param AngularVelocity                (out) Angular velocity in radians per second.\n@param LinearVelocity                 (out) Velocity in meters per second.\n@param TimeInSeconds                  (out) Time when the reported IMU reading took place, in seconds." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetRawSensorData", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14C22401, sizeof(OptitrackOculusFunctionLibrary_eventGetRawSensorData_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetTiledMultiresLevel_Parms
		{
			ETiledMultiResLevel ReturnValue;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Enum, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetTiledMultiresLevel_Parms, ReturnValue), Z_Construct_UEnum_OptitrackOculusHMD_ETiledMultiResLevel, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel_Statics::NewProp_ReturnValue_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel_Statics::NewProp_ReturnValue_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns the current multiresolution level" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetTiledMultiresLevel", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OptitrackOculusFunctionLibrary_eventGetTiledMultiresLevel_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventGetUserProfile_Parms
		{
			FHmdUserProfile Profile;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Profile;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventGetUserProfile_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventGetUserProfile_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::NewProp_Profile = { UE4CodeGen_Private::EPropertyClass::Struct, "Profile", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventGetUserProfile_Parms, Profile), Z_Construct_UScriptStruct_FHmdUserProfile, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::NewProp_Profile,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns current user profile.\n\n@param Profile                (out) Structure to hold current user profile.\n@return (boolean)     True, if user profile was acquired." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "GetUserProfile", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(OptitrackOculusFunctionLibrary_eventGetUserProfile_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventHasInputFocus_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventHasInputFocus_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventHasInputFocus_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns true, if the app has input focus." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "HasInputFocus", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OptitrackOculusFunctionLibrary_eventHasInputFocus_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventHasSystemOverlayPresent_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventHasSystemOverlayPresent_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventHasSystemOverlayPresent_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns true, if the system overlay is present." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "HasSystemOverlayPresent", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OptitrackOculusFunctionLibrary_eventHasSystemOverlayPresent_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingIcon_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingIcon_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Clears the loading icon. This call will clear all the splashes." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingIcon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "HideLoadingIcon", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingIcon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingIcon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingIcon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingIcon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventHideLoadingSplashScreen_Parms
		{
			bool bClear;
		};
		static void NewProp_bClear_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClear;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen_Statics::NewProp_bClear_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventHideLoadingSplashScreen_Parms*)Obj)->bClear = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen_Statics::NewProp_bClear = { UE4CodeGen_Private::EPropertyClass::Bool, "bClear", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventHideLoadingSplashScreen_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen_Statics::NewProp_bClear_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen_Statics::NewProp_bClear,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "CPP_Default_bClear", "false" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Hides loading splash screen.\n\n@param       bClear  (in) Clear all splash screens after hide." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "HideLoadingSplashScreen", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OptitrackOculusFunctionLibrary_eventHideLoadingSplashScreen_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventIsAutoLoadingSplashScreenEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventIsAutoLoadingSplashScreenEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventIsAutoLoadingSplashScreenEnabled_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns true, if the splash screen is automatically shown when LoadMap is called." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "IsAutoLoadingSplashScreenEnabled", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OptitrackOculusFunctionLibrary_eventIsAutoLoadingSplashScreenEnabled_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventIsDeviceTracked_Parms
		{
			ETrackedDeviceType DeviceType;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DeviceType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DeviceType_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventIsDeviceTracked_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventIsDeviceTracked_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::NewProp_DeviceType = { UE4CodeGen_Private::EPropertyClass::Enum, "DeviceType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventIsDeviceTracked_Parms, DeviceType), Z_Construct_UEnum_OptitrackOculusHMD_ETrackedDeviceType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::NewProp_DeviceType_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::NewProp_DeviceType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::NewProp_DeviceType_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns if the device is currently tracked by the runtime or not." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "IsDeviceTracked", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OptitrackOculusFunctionLibrary_eventIsDeviceTracked_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventIsGuardianDisplayed_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventIsGuardianDisplayed_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventIsGuardianDisplayed_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns true if the Guardian Outer Boundary is being displayed" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "IsGuardianDisplayed", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OptitrackOculusFunctionLibrary_eventIsGuardianDisplayed_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventIsLoadingIconEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventIsLoadingIconEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventIsLoadingIconEnabled_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns true, if the splash screen is in loading icon mode." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "IsLoadingIconEnabled", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(OptitrackOculusFunctionLibrary_eventIsLoadingIconEnabled_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventSetBaseRotationAndBaseOffsetInMeters_Parms
		{
			FRotator Rotation;
			FVector BaseOffsetInMeters;
			TEnumAsByte<EOrientPositionSelector::Type> Options;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Options;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BaseOffsetInMeters;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_Options = { UE4CodeGen_Private::EPropertyClass::Byte, "Options", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetBaseRotationAndBaseOffsetInMeters_Parms, Options), Z_Construct_UEnum_HeadMountedDisplay_EOrientPositionSelector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_BaseOffsetInMeters = { UE4CodeGen_Private::EPropertyClass::Struct, "BaseOffsetInMeters", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetBaseRotationAndBaseOffsetInMeters_Parms, BaseOffsetInMeters), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_Rotation = { UE4CodeGen_Private::EPropertyClass::Struct, "Rotation", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetBaseRotationAndBaseOffsetInMeters_Parms, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_Options,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_BaseOffsetInMeters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::NewProp_Rotation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Sets 'base rotation' - the rotation that will be subtracted from\nthe actual HMD orientation.\nSets base position offset (in meters). The base position offset is the distance from the physical (0, 0, 0) position\nto current HMD position (bringing the (0, 0, 0) point to the current HMD position)\nNote, this vector is set by ResetPosition call; use this method with care.\nThe axis of the vector are the same as in Unreal: X - forward, Y - right, Z - up.\n\n@param Rotation                       (in) Rotator object with base rotation\n@param BaseOffsetInMeters (in) the vector to be set as base offset, in meters.\n@param Options                        (in) specifies either position, orientation or both should be set." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "SetBaseRotationAndBaseOffsetInMeters", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(OptitrackOculusFunctionLibrary_eventSetBaseRotationAndBaseOffsetInMeters_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventSetBaseRotationAndPositionOffset_Parms
		{
			FRotator BaseRot;
			FVector PosOffset;
			TEnumAsByte<EOrientPositionSelector::Type> Options;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Options;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PosOffset;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BaseRot;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::NewProp_Options = { UE4CodeGen_Private::EPropertyClass::Byte, "Options", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetBaseRotationAndPositionOffset_Parms, Options), Z_Construct_UEnum_HeadMountedDisplay_EOrientPositionSelector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::NewProp_PosOffset = { UE4CodeGen_Private::EPropertyClass::Struct, "PosOffset", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetBaseRotationAndPositionOffset_Parms, PosOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::NewProp_BaseRot = { UE4CodeGen_Private::EPropertyClass::Struct, "BaseRot", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetBaseRotationAndPositionOffset_Parms, BaseRot), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::NewProp_Options,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::NewProp_PosOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::NewProp_BaseRot,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "A hack, proper camera positioning should be used" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Sets 'base rotation' - the rotation that will be subtracted from\nthe actual HMD orientation.\nThe position offset might be added to current HMD position,\neffectively moving the virtual camera by the specified offset. The addition\noccurs after the HMD orientation and position are applied.\n\n@param BaseRot                       (in) Rotator object with base rotation\n@param PosOffset                     (in) the vector to be added to HMD position.\n@param Options                       (in) specifies either position, orientation or both should be set." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "SetBaseRotationAndPositionOffset", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(OptitrackOculusFunctionLibrary_eventSetBaseRotationAndPositionOffset_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventSetCPUAndGPULevels_Parms
		{
			int32 CPULevel;
			int32 GPULevel;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_GPULevel;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CPULevel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels_Statics::NewProp_GPULevel = { UE4CodeGen_Private::EPropertyClass::Int, "GPULevel", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetCPUAndGPULevels_Parms, GPULevel), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels_Statics::NewProp_CPULevel = { UE4CodeGen_Private::EPropertyClass::Int, "CPULevel", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetCPUAndGPULevels_Parms, CPULevel), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels_Statics::NewProp_GPULevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels_Statics::NewProp_CPULevel,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Returns if the device is currently tracked by the runtime or not." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "SetCPUAndGPULevels", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OptitrackOculusFunctionLibrary_eventSetCPUAndGPULevels_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventSetDisplayFrequency_Parms
		{
			float RequestedFrequency;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RequestedFrequency;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency_Statics::NewProp_RequestedFrequency = { UE4CodeGen_Private::EPropertyClass::Float, "RequestedFrequency", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetDisplayFrequency_Parms, RequestedFrequency), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency_Statics::NewProp_RequestedFrequency,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Sets the requested display frequency" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "SetDisplayFrequency", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OptitrackOculusFunctionLibrary_eventSetDisplayFrequency_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventSetGuardianVisibility_Parms
		{
			bool GuardianVisible;
		};
		static void NewProp_GuardianVisible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_GuardianVisible;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility_Statics::NewProp_GuardianVisible_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventSetGuardianVisibility_Parms*)Obj)->GuardianVisible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility_Statics::NewProp_GuardianVisible = { UE4CodeGen_Private::EPropertyClass::Bool, "GuardianVisible", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventSetGuardianVisibility_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility_Statics::NewProp_GuardianVisible_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility_Statics::NewProp_GuardianVisible,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary|Guardian" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Forces the runtime to render guardian at all times or not\n@param GuardianVisible                        (in) True will display guardian, False will hide it" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "SetGuardianVisibility", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OptitrackOculusFunctionLibrary_eventSetGuardianVisibility_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventSetLoadingSplashParams_Parms
		{
			FString TexturePath;
			FVector DistanceInMeters;
			FVector2D SizeInMeters;
			FVector RotationAxis;
			float RotationDeltaInDeg;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RotationDeltaInDeg;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotationAxis;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SizeInMeters;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistanceInMeters;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TexturePath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::NewProp_RotationDeltaInDeg = { UE4CodeGen_Private::EPropertyClass::Float, "RotationDeltaInDeg", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetLoadingSplashParams_Parms, RotationDeltaInDeg), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::NewProp_RotationAxis = { UE4CodeGen_Private::EPropertyClass::Struct, "RotationAxis", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetLoadingSplashParams_Parms, RotationAxis), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::NewProp_SizeInMeters = { UE4CodeGen_Private::EPropertyClass::Struct, "SizeInMeters", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetLoadingSplashParams_Parms, SizeInMeters), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::NewProp_DistanceInMeters = { UE4CodeGen_Private::EPropertyClass::Struct, "DistanceInMeters", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetLoadingSplashParams_Parms, DistanceInMeters), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::NewProp_TexturePath = { UE4CodeGen_Private::EPropertyClass::Str, "TexturePath", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetLoadingSplashParams_Parms, TexturePath), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::NewProp_RotationDeltaInDeg,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::NewProp_RotationAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::NewProp_SizeInMeters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::NewProp_DistanceInMeters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::NewProp_TexturePath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "use AddLoadingSplashScreen / ClearLoadingSplashScreens" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Sets loading splash screen parameters.\n\n@param TexturePath           (in) A path to the texture asset to be used for the splash. Gear VR uses it as a path for loading icon; all other params are currently ignored by Gear VR.\n@param DistanceInMeters      (in) Distance, in meters, to the center of the splash screen.\n@param SizeInMeters          (in) Size, in meters, of the quad with the splash screen.\n@param RotationAxes          (in) A vector that specifies the axis of the splash screen rotation (if RotationDelta is specified).\n@param RotationDeltaInDeg (in) Rotation delta, in degrees, that is added each 2nd frame to the quad transform. The quad is rotated around the vector \"RotationAxes\"." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "SetLoadingSplashParams", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(OptitrackOculusFunctionLibrary_eventSetLoadingSplashParams_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventSetPositionScale3D_Parms
		{
			FVector PosScale3D;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PosScale3D;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D_Statics::NewProp_PosScale3D = { UE4CodeGen_Private::EPropertyClass::Struct, "PosScale3D", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetPositionScale3D_Parms, PosScale3D), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D_Statics::NewProp_PosScale3D,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "This feature is no longer supported." },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Scales the HMD position that gets added to the virtual camera position.\n\n@param PosScale3D    (in) the scale to apply to the HMD position." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "SetPositionScale3D", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(OptitrackOculusFunctionLibrary_eventSetPositionScale3D_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventSetReorientHMDOnControllerRecenter_Parms
		{
			bool recenterMode;
		};
		static void NewProp_recenterMode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_recenterMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter_Statics::NewProp_recenterMode_SetBit(void* Obj)
	{
		((OptitrackOculusFunctionLibrary_eventSetReorientHMDOnControllerRecenter_Parms*)Obj)->recenterMode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter_Statics::NewProp_recenterMode = { UE4CodeGen_Private::EPropertyClass::Bool, "recenterMode", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackOculusFunctionLibrary_eventSetReorientHMDOnControllerRecenter_Parms), &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter_Statics::NewProp_recenterMode_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter_Statics::NewProp_recenterMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input|OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Sets the HMD recenter behavior to a mode that specifies HMD recentering behavior when a\ncontroller recenter is performed. If the recenterMode specified is 1, the HMD will recenter\non controller recenter; if it's 0, only the controller will recenter. Returns false if not\nsupported." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "SetReorientHMDOnControllerRecenter", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OptitrackOculusFunctionLibrary_eventSetReorientHMDOnControllerRecenter_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventSetTiledMultiresLevel_Parms
		{
			ETiledMultiResLevel level;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_level;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_level_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel_Statics::NewProp_level = { UE4CodeGen_Private::EPropertyClass::Enum, "level", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventSetTiledMultiresLevel_Parms, level), Z_Construct_UEnum_OptitrackOculusHMD_ETiledMultiResLevel, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel_Statics::NewProp_level_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel_Statics::NewProp_level,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel_Statics::NewProp_level_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Set the requested multiresolution level for the next frame" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "SetTiledMultiresLevel", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OptitrackOculusFunctionLibrary_eventSetTiledMultiresLevel_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon_Statics
	{
		struct OptitrackOculusFunctionLibrary_eventShowLoadingIcon_Parms
		{
			UTexture2D* Texture;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Texture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon_Statics::NewProp_Texture = { UE4CodeGen_Private::EPropertyClass::Object, "Texture", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackOculusFunctionLibrary_eventShowLoadingIcon_Parms, Texture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon_Statics::NewProp_Texture,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Sets a texture for loading icon mode and shows it. This call will clear all the splashes." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "ShowLoadingIcon", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OptitrackOculusFunctionLibrary_eventShowLoadingIcon_Parms), Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingSplashScreen_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingSplashScreen_Statics::Function_MetaDataParams[] = {
		{ "Category", "OculusLibrary" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
		{ "ToolTip", "Shows loading splash screen." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingSplashScreen_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOptitrackOculusFunctionLibrary, "ShowLoadingSplashScreen", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingSplashScreen_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingSplashScreen_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingSplashScreen()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingSplashScreen_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UOptitrackOculusFunctionLibrary_NoRegister()
	{
		return UOptitrackOculusFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UOptitrackOculusFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOptitrackOculusFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackOculusHMD,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UOptitrackOculusFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_AddLoadingSplashScreen, "AddLoadingSplashScreen" }, // 3911777440
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ClearLoadingSplashScreens, "ClearLoadingSplashScreens" }, // 1411629788
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableAutoLoadingSplashScreen, "EnableAutoLoadingSplashScreen" }, // 3632039064
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnableOrientationTracking, "EnableOrientationTracking" }, // 1354173209
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_EnablePositionTracking, "EnablePositionTracking" }, // 2096323700
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetAvailableDisplayFrequencies, "GetAvailableDisplayFrequencies" }, // 2931169694
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndBaseOffsetInMeters, "GetBaseRotationAndBaseOffsetInMeters" }, // 2170208872
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetBaseRotationAndPositionOffset, "GetBaseRotationAndPositionOffset" }, // 942944421
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetCurrentDisplayFrequency, "GetCurrentDisplayFrequency" }, // 3164329767
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetDeviceName, "GetDeviceName" }, // 297808712
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUFrameTime, "GetGPUFrameTime" }, // 911934979
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGPUUtilization, "GetGPUUtilization" }, // 525182438
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianDimensions, "GetGuardianDimensions" }, // 3840991505
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetGuardianPoints, "GetGuardianPoints" }, // 562247225
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetLoadingSplashParams, "GetLoadingSplashParams" }, // 2618255165
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetNodeGuardianIntersection, "GetNodeGuardianIntersection" }, // 2025319206
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPlayAreaTransform, "GetPlayAreaTransform" }, // 3352937856
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPointGuardianIntersection, "GetPointGuardianIntersection" }, // 1688171400
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetPose, "GetPose" }, // 3579289822
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetRawSensorData, "GetRawSensorData" }, // 133372649
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetTiledMultiresLevel, "GetTiledMultiresLevel" }, // 127123280
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_GetUserProfile, "GetUserProfile" }, // 3732450657
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasInputFocus, "HasInputFocus" }, // 1475278877
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HasSystemOverlayPresent, "HasSystemOverlayPresent" }, // 3462630624
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingIcon, "HideLoadingIcon" }, // 2724067990
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_HideLoadingSplashScreen, "HideLoadingSplashScreen" }, // 4010257910
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsAutoLoadingSplashScreenEnabled, "IsAutoLoadingSplashScreenEnabled" }, // 3745428026
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsDeviceTracked, "IsDeviceTracked" }, // 2801871460
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsGuardianDisplayed, "IsGuardianDisplayed" }, // 2432984819
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_IsLoadingIconEnabled, "IsLoadingIconEnabled" }, // 1349341600
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndBaseOffsetInMeters, "SetBaseRotationAndBaseOffsetInMeters" }, // 2860332422
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetBaseRotationAndPositionOffset, "SetBaseRotationAndPositionOffset" }, // 2756958150
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetCPUAndGPULevels, "SetCPUAndGPULevels" }, // 1866905031
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetDisplayFrequency, "SetDisplayFrequency" }, // 3479834933
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetGuardianVisibility, "SetGuardianVisibility" }, // 2607846065
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetLoadingSplashParams, "SetLoadingSplashParams" }, // 1583738915
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetPositionScale3D, "SetPositionScale3D" }, // 3624643087
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetReorientHMDOnControllerRecenter, "SetReorientHMDOnControllerRecenter" }, // 2273974208
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_SetTiledMultiresLevel, "SetTiledMultiresLevel" }, // 2806482410
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingIcon, "ShowLoadingIcon" }, // 2195702947
		{ &Z_Construct_UFunction_UOptitrackOculusFunctionLibrary_ShowLoadingSplashScreen, "ShowLoadingSplashScreen" }, // 2119433986
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackOculusFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "OculusFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/OculusFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOptitrackOculusFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOptitrackOculusFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOptitrackOculusFunctionLibrary_Statics::ClassParams = {
		&UOptitrackOculusFunctionLibrary::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x001000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UOptitrackOculusFunctionLibrary_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UOptitrackOculusFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOptitrackOculusFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOptitrackOculusFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOptitrackOculusFunctionLibrary, 48501942);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOptitrackOculusFunctionLibrary(Z_Construct_UClass_UOptitrackOculusFunctionLibrary, &UOptitrackOculusFunctionLibrary::StaticClass, TEXT("/Script/OptitrackOculusHMD"), TEXT("UOptitrackOculusFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOptitrackOculusFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
