// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPTITRACKOCULUSHMD_OptitrackHmdPawn_generated_h
#error "OptitrackHmdPawn.generated.h already included, missing '#pragma once' in OptitrackHmdPawn.h"
#endif
#define OPTITRACKOCULUSHMD_OptitrackHmdPawn_generated_h

#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_RPC_WRAPPERS
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOptitrackHmdPawn(); \
	friend struct Z_Construct_UClass_AOptitrackHmdPawn_Statics; \
public: \
	DECLARE_CLASS(AOptitrackHmdPawn, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OptitrackOculusHMD"), NO_API) \
	DECLARE_SERIALIZER(AOptitrackHmdPawn)


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_INCLASS \
private: \
	static void StaticRegisterNativesAOptitrackHmdPawn(); \
	friend struct Z_Construct_UClass_AOptitrackHmdPawn_Statics; \
public: \
	DECLARE_CLASS(AOptitrackHmdPawn, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OptitrackOculusHMD"), NO_API) \
	DECLARE_SERIALIZER(AOptitrackHmdPawn)


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOptitrackHmdPawn(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOptitrackHmdPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOptitrackHmdPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOptitrackHmdPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOptitrackHmdPawn(AOptitrackHmdPawn&&); \
	NO_API AOptitrackHmdPawn(const AOptitrackHmdPawn&); \
public:


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOptitrackHmdPawn(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOptitrackHmdPawn(AOptitrackHmdPawn&&); \
	NO_API AOptitrackHmdPawn(const AOptitrackHmdPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOptitrackHmdPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOptitrackHmdPawn); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOptitrackHmdPawn)


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraComponent() { return STRUCT_OFFSET(AOptitrackHmdPawn, CameraComponent); } \
	FORCEINLINE static uint32 __PPO__RigidBodyComponent() { return STRUCT_OFFSET(AOptitrackHmdPawn, RigidBodyComponent); }


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_24_PROLOG
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_RPC_WRAPPERS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_INCLASS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_INCLASS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h_27_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OptitrackHmdPawn."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OptitrackHmdPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
