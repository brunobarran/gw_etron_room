// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPTITRACKOCULUSHMD_OculusHMDRuntimeSettings_generated_h
#error "OculusHMDRuntimeSettings.generated.h already included, missing '#pragma once' in OculusHMDRuntimeSettings.h"
#endif
#define OPTITRACKOCULUSHMD_OculusHMDRuntimeSettings_generated_h

#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_RPC_WRAPPERS
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOptitrackOculusHMDRuntimeSettings(); \
	friend struct Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics; \
public: \
	DECLARE_CLASS(UOptitrackOculusHMDRuntimeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/OptitrackOculusHMD"), NO_API) \
	DECLARE_SERIALIZER(UOptitrackOculusHMDRuntimeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUOptitrackOculusHMDRuntimeSettings(); \
	friend struct Z_Construct_UClass_UOptitrackOculusHMDRuntimeSettings_Statics; \
public: \
	DECLARE_CLASS(UOptitrackOculusHMDRuntimeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/OptitrackOculusHMD"), NO_API) \
	DECLARE_SERIALIZER(UOptitrackOculusHMDRuntimeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOptitrackOculusHMDRuntimeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOptitrackOculusHMDRuntimeSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOptitrackOculusHMDRuntimeSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOptitrackOculusHMDRuntimeSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOptitrackOculusHMDRuntimeSettings(UOptitrackOculusHMDRuntimeSettings&&); \
	NO_API UOptitrackOculusHMDRuntimeSettings(const UOptitrackOculusHMDRuntimeSettings&); \
public:


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOptitrackOculusHMDRuntimeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOptitrackOculusHMDRuntimeSettings(UOptitrackOculusHMDRuntimeSettings&&); \
	NO_API UOptitrackOculusHMDRuntimeSettings(const UOptitrackOculusHMDRuntimeSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOptitrackOculusHMDRuntimeSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOptitrackOculusHMDRuntimeSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOptitrackOculusHMDRuntimeSettings)


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_PRIVATE_PROPERTY_OFFSET
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_13_PROLOG
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_RPC_WRAPPERS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_INCLASS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_INCLASS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OptitrackOculusHMDRuntimeSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Public_OculusHMDRuntimeSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
