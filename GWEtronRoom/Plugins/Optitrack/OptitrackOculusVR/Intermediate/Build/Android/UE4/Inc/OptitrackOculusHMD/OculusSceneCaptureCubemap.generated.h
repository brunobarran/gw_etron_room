// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPTITRACKOCULUSHMD_OculusSceneCaptureCubemap_generated_h
#error "OculusSceneCaptureCubemap.generated.h already included, missing '#pragma once' in OculusSceneCaptureCubemap.h"
#endif
#define OPTITRACKOCULUSHMD_OculusSceneCaptureCubemap_generated_h

#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_RPC_WRAPPERS
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOptitrackOculusSceneCaptureCubemap(); \
	friend struct Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics; \
public: \
	DECLARE_CLASS(UOptitrackOculusSceneCaptureCubemap, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OptitrackOculusHMD"), NO_API) \
	DECLARE_SERIALIZER(UOptitrackOculusSceneCaptureCubemap)


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUOptitrackOculusSceneCaptureCubemap(); \
	friend struct Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics; \
public: \
	DECLARE_CLASS(UOptitrackOculusSceneCaptureCubemap, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OptitrackOculusHMD"), NO_API) \
	DECLARE_SERIALIZER(UOptitrackOculusSceneCaptureCubemap)


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOptitrackOculusSceneCaptureCubemap(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOptitrackOculusSceneCaptureCubemap) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOptitrackOculusSceneCaptureCubemap); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOptitrackOculusSceneCaptureCubemap); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOptitrackOculusSceneCaptureCubemap(UOptitrackOculusSceneCaptureCubemap&&); \
	NO_API UOptitrackOculusSceneCaptureCubemap(const UOptitrackOculusSceneCaptureCubemap&); \
public:


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOptitrackOculusSceneCaptureCubemap(UOptitrackOculusSceneCaptureCubemap&&); \
	NO_API UOptitrackOculusSceneCaptureCubemap(const UOptitrackOculusSceneCaptureCubemap&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOptitrackOculusSceneCaptureCubemap); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOptitrackOculusSceneCaptureCubemap); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UOptitrackOculusSceneCaptureCubemap)


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CaptureComponents() { return STRUCT_OFFSET(UOptitrackOculusSceneCaptureCubemap, CaptureComponents); }


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_16_PROLOG
#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_RPC_WRAPPERS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_INCLASS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_INCLASS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID C__DELME_ue421_package_OptitrackOculusVR_HostProject_Plugins_OptitrackOculusVR_Source_OptitrackOculusHMD_Private_OculusSceneCaptureCubemap_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
