// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OptitrackOculusHMD/Private/OculusSceneCaptureCubemap.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusSceneCaptureCubemap() {}
// Cross Module References
	OPTITRACKOCULUSHMD_API UClass* Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_NoRegister();
	OPTITRACKOCULUSHMD_API UClass* Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_OptitrackOculusHMD();
	ENGINE_API UClass* Z_Construct_UClass_USceneCaptureComponent2D_NoRegister();
// End Cross Module References
	void UOptitrackOculusSceneCaptureCubemap::StaticRegisterNativesUOptitrackOculusSceneCaptureCubemap()
	{
	}
	UClass* Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_NoRegister()
	{
		return UOptitrackOculusSceneCaptureCubemap::StaticClass();
	}
	struct Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CaptureComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CaptureComponents;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CaptureComponents_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackOculusHMD,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "OculusSceneCaptureCubemap.h" },
		{ "ModuleRelativePath", "Private/OculusSceneCaptureCubemap.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::NewProp_CaptureComponents_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/OculusSceneCaptureCubemap.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::NewProp_CaptureComponents = { UE4CodeGen_Private::EPropertyClass::Array, "CaptureComponents", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040008000000008, 1, nullptr, STRUCT_OFFSET(UOptitrackOculusSceneCaptureCubemap, CaptureComponents), METADATA_PARAMS(Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::NewProp_CaptureComponents_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::NewProp_CaptureComponents_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::NewProp_CaptureComponents_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "CaptureComponents", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000080008, 1, nullptr, 0, Z_Construct_UClass_USceneCaptureComponent2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::NewProp_CaptureComponents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::NewProp_CaptureComponents_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOptitrackOculusSceneCaptureCubemap>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::ClassParams = {
		&UOptitrackOculusSceneCaptureCubemap::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008000A0u,
		nullptr, 0,
		Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOptitrackOculusSceneCaptureCubemap, 354014502);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOptitrackOculusSceneCaptureCubemap(Z_Construct_UClass_UOptitrackOculusSceneCaptureCubemap, &UOptitrackOculusSceneCaptureCubemap::StaticClass, TEXT("/Script/OptitrackOculusHMD"), TEXT("UOptitrackOculusSceneCaptureCubemap"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOptitrackOculusSceneCaptureCubemap);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
