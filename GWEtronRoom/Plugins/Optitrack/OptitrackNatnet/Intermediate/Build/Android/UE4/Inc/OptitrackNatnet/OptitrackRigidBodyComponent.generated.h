// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPTITRACKNATNET_OptitrackRigidBodyComponent_generated_h
#error "OptitrackRigidBodyComponent.generated.h already included, missing '#pragma once' in OptitrackRigidBodyComponent.h"
#endif
#define OPTITRACKNATNET_OptitrackRigidBodyComponent_generated_h

#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_RPC_WRAPPERS
#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOptitrackRigidBodyComponent(); \
	friend struct Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics; \
public: \
	DECLARE_CLASS(UOptitrackRigidBodyComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OptitrackNatnet"), NO_API) \
	DECLARE_SERIALIZER(UOptitrackRigidBodyComponent)


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUOptitrackRigidBodyComponent(); \
	friend struct Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics; \
public: \
	DECLARE_CLASS(UOptitrackRigidBodyComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OptitrackNatnet"), NO_API) \
	DECLARE_SERIALIZER(UOptitrackRigidBodyComponent)


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOptitrackRigidBodyComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOptitrackRigidBodyComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOptitrackRigidBodyComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOptitrackRigidBodyComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOptitrackRigidBodyComponent(UOptitrackRigidBodyComponent&&); \
	NO_API UOptitrackRigidBodyComponent(const UOptitrackRigidBodyComponent&); \
public:


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOptitrackRigidBodyComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOptitrackRigidBodyComponent(UOptitrackRigidBodyComponent&&); \
	NO_API UOptitrackRigidBodyComponent(const UOptitrackRigidBodyComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOptitrackRigidBodyComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOptitrackRigidBodyComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOptitrackRigidBodyComponent)


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_PRIVATE_PROPERTY_OFFSET
#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_29_PROLOG
#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_RPC_WRAPPERS \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_INCLASS \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_INCLASS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h_32_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OptitrackRigidBodyComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
