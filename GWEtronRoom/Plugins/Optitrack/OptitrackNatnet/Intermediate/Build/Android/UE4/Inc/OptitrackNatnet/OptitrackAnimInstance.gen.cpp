// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OptitrackNatnet/Classes/OptitrackAnimInstance.h"
#include "Engine/Classes/Components/SkeletalMeshComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOptitrackAnimInstance() {}
// Cross Module References
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_UOptitrackAnimInstance_NoRegister();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_UOptitrackAnimInstance();
	ENGINE_API UClass* Z_Construct_UClass_UAnimInstance();
	UPackage* Z_Construct_UPackage__Script_OptitrackNatnet();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_AOptitrackClientOrigin_NoRegister();
// End Cross Module References
	void UOptitrackAnimInstance::StaticRegisterNativesUOptitrackAnimInstance()
	{
	}
	UClass* Z_Construct_UClass_UOptitrackAnimInstance_NoRegister()
	{
		return UOptitrackAnimInstance::StaticClass();
	}
	struct Z_Construct_UClass_UOptitrackAnimInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StreamingClientOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StreamingClientOrigin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceSkeletonAssetName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SourceSkeletonAssetName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOptitrackAnimInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnimInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnet,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackAnimInstance_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "AnimInstance" },
		{ "IncludePath", "OptitrackAnimInstance.h" },
		{ "ModuleRelativePath", "Classes/OptitrackAnimInstance.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackAnimInstance_Statics::NewProp_StreamingClientOrigin_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOptitrackAnimInstance_Statics::NewProp_StreamingClientOrigin = { UE4CodeGen_Private::EPropertyClass::Object, "StreamingClientOrigin", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(UOptitrackAnimInstance, StreamingClientOrigin), Z_Construct_UClass_AOptitrackClientOrigin_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UOptitrackAnimInstance_Statics::NewProp_StreamingClientOrigin_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackAnimInstance_Statics::NewProp_StreamingClientOrigin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackAnimInstance_Statics::NewProp_SourceSkeletonAssetName_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UOptitrackAnimInstance_Statics::NewProp_SourceSkeletonAssetName = { UE4CodeGen_Private::EPropertyClass::Name, "SourceSkeletonAssetName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(UOptitrackAnimInstance, SourceSkeletonAssetName), METADATA_PARAMS(Z_Construct_UClass_UOptitrackAnimInstance_Statics::NewProp_SourceSkeletonAssetName_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackAnimInstance_Statics::NewProp_SourceSkeletonAssetName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOptitrackAnimInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackAnimInstance_Statics::NewProp_StreamingClientOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackAnimInstance_Statics::NewProp_SourceSkeletonAssetName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOptitrackAnimInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOptitrackAnimInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOptitrackAnimInstance_Statics::ClassParams = {
		&UOptitrackAnimInstance::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A8u,
		nullptr, 0,
		Z_Construct_UClass_UOptitrackAnimInstance_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UOptitrackAnimInstance_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UOptitrackAnimInstance_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UOptitrackAnimInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOptitrackAnimInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOptitrackAnimInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOptitrackAnimInstance, 2248430180);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOptitrackAnimInstance(Z_Construct_UClass_UOptitrackAnimInstance, &UOptitrackAnimInstance::StaticClass, TEXT("/Script/OptitrackNatnet"), TEXT("UOptitrackAnimInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOptitrackAnimInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
