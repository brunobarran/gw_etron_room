// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPTITRACKNATNET_AnimNode_OptitrackSkeleton_generated_h
#error "AnimNode_OptitrackSkeleton.generated.h already included, missing '#pragma once' in AnimNode_OptitrackSkeleton.h"
#endif
#define OPTITRACKNATNET_AnimNode_OptitrackSkeleton_generated_h

#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Public_AnimNode_OptitrackSkeleton_h_40_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__RetargetBasePose() { return STRUCT_OFFSET(FAnimNode_OptitrackSkeleton, RetargetBasePose); } \
	typedef FAnimNode_Base Super;


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Public_AnimNode_OptitrackSkeleton_h_31_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics; \
	OPTITRACKNATNET_API static class UScriptStruct* StaticStruct();


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Public_AnimNode_OptitrackSkeleton_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
