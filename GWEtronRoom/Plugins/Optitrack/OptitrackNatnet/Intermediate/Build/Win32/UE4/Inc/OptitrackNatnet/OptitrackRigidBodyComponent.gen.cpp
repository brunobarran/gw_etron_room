// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OptitrackNatnet/Classes/OptitrackRigidBodyComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOptitrackRigidBodyComponent() {}
// Cross Module References
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_UOptitrackRigidBodyComponent_NoRegister();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_UOptitrackRigidBodyComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	UPackage* Z_Construct_UPackage__Script_OptitrackNatnet();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_AOptitrackClientOrigin_NoRegister();
// End Cross Module References
	void UOptitrackRigidBodyComponent::StaticRegisterNativesUOptitrackRigidBodyComponent()
	{
	}
	UClass* Z_Construct_UClass_UOptitrackRigidBodyComponent_NoRegister()
	{
		return UOptitrackRigidBodyComponent::StaticClass();
	}
	struct Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RespectParentTransform_MetaData[];
#endif
		static void NewProp_RespectParentTransform_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_RespectParentTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TrackingOrigin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisableLowLatencyUpdate_MetaData[];
#endif
		static void NewProp_bDisableLowLatencyUpdate_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisableLowLatencyUpdate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingId_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TrackingId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnet,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "OptiTrack" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "OptitrackRigidBodyComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Classes/OptitrackRigidBodyComponent.h" },
		{ "ToolTip", "A component whose position and orientation is driven by the pose of the\nspecified tracked rigid body. Supports render thread \"late updates\" for\nreduced tracking latency." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_RespectParentTransform_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackRigidBodyComponent.h" },
		{ "ToolTip", "If true, rigid body transform from Motive will be applied with respect to parent transform instead of the client origin." },
	};
#endif
	void Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_RespectParentTransform_SetBit(void* Obj)
	{
		((UOptitrackRigidBodyComponent*)Obj)->RespectParentTransform = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_RespectParentTransform = { UE4CodeGen_Private::EPropertyClass::Bool, "RespectParentTransform", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UOptitrackRigidBodyComponent), &Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_RespectParentTransform_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_RespectParentTransform_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_RespectParentTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_TrackingOrigin_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackRigidBodyComponent.h" },
		{ "ToolTip", "If your scene contains multiple client origin objects, you can specify\nwhich one to use. If unset, this defaults to the first client origin\nthat's found in the world." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_TrackingOrigin = { UE4CodeGen_Private::EPropertyClass::Object, "TrackingOrigin", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000005, 1, nullptr, STRUCT_OFFSET(UOptitrackRigidBodyComponent, TrackingOrigin), Z_Construct_UClass_AOptitrackClientOrigin_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_TrackingOrigin_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_TrackingOrigin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_bDisableLowLatencyUpdate_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackRigidBodyComponent.h" },
		{ "ToolTip", "If false, render transforms within the rigid body hierarchy will be updated a second time immediately before rendering." },
	};
#endif
	void Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_bDisableLowLatencyUpdate_SetBit(void* Obj)
	{
		((UOptitrackRigidBodyComponent*)Obj)->bDisableLowLatencyUpdate = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_bDisableLowLatencyUpdate = { UE4CodeGen_Private::EPropertyClass::Bool, "bDisableLowLatencyUpdate", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000005, 1, nullptr, sizeof(uint8), UE4CodeGen_Private::ENativeBool::NotNative, sizeof(UOptitrackRigidBodyComponent), &Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_bDisableLowLatencyUpdate_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_bDisableLowLatencyUpdate_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_bDisableLowLatencyUpdate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_TrackingId_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackRigidBodyComponent.h" },
		{ "ToolTip", "ID of the rigid body used to drive this component's transform." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_TrackingId = { UE4CodeGen_Private::EPropertyClass::Int, "TrackingId", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(UOptitrackRigidBodyComponent, TrackingId), METADATA_PARAMS(Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_TrackingId_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_TrackingId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_RespectParentTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_TrackingOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_bDisableLowLatencyUpdate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::NewProp_TrackingId,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOptitrackRigidBodyComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::ClassParams = {
		&UOptitrackRigidBodyComponent::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x00B000A4u,
		nullptr, 0,
		Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOptitrackRigidBodyComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOptitrackRigidBodyComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOptitrackRigidBodyComponent, 3447249004);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOptitrackRigidBodyComponent(Z_Construct_UClass_UOptitrackRigidBodyComponent, &UOptitrackRigidBodyComponent::StaticClass, TEXT("/Script/OptitrackNatnet"), TEXT("UOptitrackRigidBodyComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOptitrackRigidBodyComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
