// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPTITRACKNATNET_OptitrackRigidBodyActor_generated_h
#error "OptitrackRigidBodyActor.generated.h already included, missing '#pragma once' in OptitrackRigidBodyActor.h"
#endif
#define OPTITRACKNATNET_OptitrackRigidBodyActor_generated_h

#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_RPC_WRAPPERS
#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOptitrackRigidBodyActor(); \
	friend struct Z_Construct_UClass_AOptitrackRigidBodyActor_Statics; \
public: \
	DECLARE_CLASS(AOptitrackRigidBodyActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OptitrackNatnet"), NO_API) \
	DECLARE_SERIALIZER(AOptitrackRigidBodyActor)


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_INCLASS \
private: \
	static void StaticRegisterNativesAOptitrackRigidBodyActor(); \
	friend struct Z_Construct_UClass_AOptitrackRigidBodyActor_Statics; \
public: \
	DECLARE_CLASS(AOptitrackRigidBodyActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OptitrackNatnet"), NO_API) \
	DECLARE_SERIALIZER(AOptitrackRigidBodyActor)


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOptitrackRigidBodyActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOptitrackRigidBodyActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOptitrackRigidBodyActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOptitrackRigidBodyActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOptitrackRigidBodyActor(AOptitrackRigidBodyActor&&); \
	NO_API AOptitrackRigidBodyActor(const AOptitrackRigidBodyActor&); \
public:


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOptitrackRigidBodyActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOptitrackRigidBodyActor(AOptitrackRigidBodyActor&&); \
	NO_API AOptitrackRigidBodyActor(const AOptitrackRigidBodyActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOptitrackRigidBodyActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOptitrackRigidBodyActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOptitrackRigidBodyActor)


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RigidBodyComponent() { return STRUCT_OFFSET(AOptitrackRigidBodyActor, RigidBodyComponent); }


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_26_PROLOG
#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_RPC_WRAPPERS \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_INCLASS \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_INCLASS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h_29_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OptitrackRigidBodyActor."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackRigidBodyActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
