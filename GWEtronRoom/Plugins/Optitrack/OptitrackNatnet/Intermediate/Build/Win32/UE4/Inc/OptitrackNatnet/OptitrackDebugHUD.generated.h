// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPTITRACKNATNET_OptitrackDebugHUD_generated_h
#error "OptitrackDebugHUD.generated.h already included, missing '#pragma once' in OptitrackDebugHUD.h"
#endif
#define OPTITRACKNATNET_OptitrackDebugHUD_generated_h

#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_RPC_WRAPPERS
#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOptitrackDebugHUD(); \
	friend struct Z_Construct_UClass_AOptitrackDebugHUD_Statics; \
public: \
	DECLARE_CLASS(AOptitrackDebugHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OptitrackNatnet"), NO_API) \
	DECLARE_SERIALIZER(AOptitrackDebugHUD)


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_INCLASS \
private: \
	static void StaticRegisterNativesAOptitrackDebugHUD(); \
	friend struct Z_Construct_UClass_AOptitrackDebugHUD_Statics; \
public: \
	DECLARE_CLASS(AOptitrackDebugHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/OptitrackNatnet"), NO_API) \
	DECLARE_SERIALIZER(AOptitrackDebugHUD)


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOptitrackDebugHUD(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOptitrackDebugHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOptitrackDebugHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOptitrackDebugHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOptitrackDebugHUD(AOptitrackDebugHUD&&); \
	NO_API AOptitrackDebugHUD(const AOptitrackDebugHUD&); \
public:


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOptitrackDebugHUD(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOptitrackDebugHUD(AOptitrackDebugHUD&&); \
	NO_API AOptitrackDebugHUD(const AOptitrackDebugHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOptitrackDebugHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOptitrackDebugHUD); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOptitrackDebugHUD)


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_PRIVATE_PROPERTY_OFFSET
#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_26_PROLOG
#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_RPC_WRAPPERS \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_INCLASS \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_PRIVATE_PROPERTY_OFFSET \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_INCLASS_NO_PURE_DECLS \
	C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID C__DELME_ue421_package_OptitrackNatnet_HostProject_Plugins_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackDebugHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
