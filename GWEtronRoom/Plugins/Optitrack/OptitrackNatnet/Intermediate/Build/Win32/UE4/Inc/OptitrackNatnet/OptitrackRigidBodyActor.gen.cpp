// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OptitrackNatnet/Classes/OptitrackRigidBodyActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOptitrackRigidBodyActor() {}
// Cross Module References
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_AOptitrackRigidBodyActor_NoRegister();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_AOptitrackRigidBodyActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_OptitrackNatnet();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_UOptitrackRigidBodyComponent_NoRegister();
// End Cross Module References
	void AOptitrackRigidBodyActor::StaticRegisterNativesAOptitrackRigidBodyActor()
	{
	}
	UClass* Z_Construct_UClass_AOptitrackRigidBodyActor_NoRegister()
	{
		return AOptitrackRigidBodyActor::StaticClass();
	}
	struct Z_Construct_UClass_AOptitrackRigidBodyActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RigidBodyComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RigidBodyComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHideOnInvalidDefinition_MetaData[];
#endif
		static void NewProp_bHideOnInvalidDefinition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHideOnInvalidDefinition;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnet,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::Class_MetaDataParams[] = {
		{ "IgnoreCategoryKeywordsInSubclasses", "true" },
		{ "IncludePath", "OptitrackRigidBodyActor.h" },
		{ "ModuleRelativePath", "Classes/OptitrackRigidBodyActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::NewProp_RigidBodyComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Rigid Body Actor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Classes/OptitrackRigidBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::NewProp_RigidBodyComponent = { UE4CodeGen_Private::EPropertyClass::Object, "RigidBodyComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(AOptitrackRigidBodyActor, RigidBodyComponent), Z_Construct_UClass_UOptitrackRigidBodyComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::NewProp_RigidBodyComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::NewProp_RigidBodyComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::NewProp_bHideOnInvalidDefinition_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackRigidBodyActor.h" },
		{ "ToolTip", "If true, rigid body actor will hide if no associated asset is being streamed from Motive." },
	};
#endif
	void Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::NewProp_bHideOnInvalidDefinition_SetBit(void* Obj)
	{
		((AOptitrackRigidBodyActor*)Obj)->bHideOnInvalidDefinition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::NewProp_bHideOnInvalidDefinition = { UE4CodeGen_Private::EPropertyClass::Bool, "bHideOnInvalidDefinition", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AOptitrackRigidBodyActor), &Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::NewProp_bHideOnInvalidDefinition_SetBit, METADATA_PARAMS(Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::NewProp_bHideOnInvalidDefinition_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::NewProp_bHideOnInvalidDefinition_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::NewProp_RigidBodyComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::NewProp_bHideOnInvalidDefinition,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AOptitrackRigidBodyActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::ClassParams = {
		&AOptitrackRigidBodyActor::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AOptitrackRigidBodyActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AOptitrackRigidBodyActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AOptitrackRigidBodyActor, 3794121347);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AOptitrackRigidBodyActor(Z_Construct_UClass_AOptitrackRigidBodyActor, &AOptitrackRigidBodyActor::StaticClass, TEXT("/Script/OptitrackNatnet"), TEXT("AOptitrackRigidBodyActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AOptitrackRigidBodyActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
