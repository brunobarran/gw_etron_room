// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OptitrackNatnet/Public/OptitrackSkeletonTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOptitrackSkeletonTypes() {}
// Cross Module References
	OPTITRACKNATNET_API UEnum* Z_Construct_UEnum_OptitrackNatnet_EOptitrackBone();
	UPackage* Z_Construct_UPackage__Script_OptitrackNatnet();
	OPTITRACKNATNET_API UEnum* Z_Construct_UEnum_OptitrackNatnet_EOptitrackBoneNamingConvention();
	OPTITRACKNATNET_API UScriptStruct* Z_Construct_UScriptStruct_FOptitrackSkeletonState();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	OPTITRACKNATNET_API UScriptStruct* Z_Construct_UScriptStruct_FOptitrackPose();
	OPTITRACKNATNET_API UScriptStruct* Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition();
	OPTITRACKNATNET_API UScriptStruct* Z_Construct_UScriptStruct_FOptitrackBoneDefinition();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
// End Cross Module References
	static UEnum* EOptitrackBone_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OptitrackNatnet_EOptitrackBone, Z_Construct_UPackage__Script_OptitrackNatnet(), TEXT("EOptitrackBone"));
		}
		return Singleton;
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOptitrackBone(EOptitrackBone_StaticEnum, TEXT("/Script/OptitrackNatnet"), TEXT("EOptitrackBone"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OptitrackNatnet_EOptitrackBone_CRC() { return 3176546760U; }
	UEnum* Z_Construct_UEnum_OptitrackNatnet_EOptitrackBone()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackNatnet();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOptitrackBone"), 0, Get_Z_Construct_UEnum_OptitrackNatnet_EOptitrackBone_CRC(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOptitrackBone::Root", (int64)EOptitrackBone::Root },
				{ "EOptitrackBone::Hip", (int64)EOptitrackBone::Hip },
				{ "EOptitrackBone::Ab", (int64)EOptitrackBone::Ab },
				{ "EOptitrackBone::Chest", (int64)EOptitrackBone::Chest },
				{ "EOptitrackBone::Neck", (int64)EOptitrackBone::Neck },
				{ "EOptitrackBone::Head", (int64)EOptitrackBone::Head },
				{ "EOptitrackBone::LShoulder", (int64)EOptitrackBone::LShoulder },
				{ "EOptitrackBone::LUArm", (int64)EOptitrackBone::LUArm },
				{ "EOptitrackBone::LFArm", (int64)EOptitrackBone::LFArm },
				{ "EOptitrackBone::LHand", (int64)EOptitrackBone::LHand },
				{ "EOptitrackBone::RShoulder", (int64)EOptitrackBone::RShoulder },
				{ "EOptitrackBone::RUArm", (int64)EOptitrackBone::RUArm },
				{ "EOptitrackBone::RFArm", (int64)EOptitrackBone::RFArm },
				{ "EOptitrackBone::RHand", (int64)EOptitrackBone::RHand },
				{ "EOptitrackBone::LThigh", (int64)EOptitrackBone::LThigh },
				{ "EOptitrackBone::LShin", (int64)EOptitrackBone::LShin },
				{ "EOptitrackBone::LFoot", (int64)EOptitrackBone::LFoot },
				{ "EOptitrackBone::LToe", (int64)EOptitrackBone::LToe },
				{ "EOptitrackBone::RThigh", (int64)EOptitrackBone::RThigh },
				{ "EOptitrackBone::RShin", (int64)EOptitrackBone::RShin },
				{ "EOptitrackBone::RFoot", (int64)EOptitrackBone::RFoot },
				{ "EOptitrackBone::RToe", (int64)EOptitrackBone::RToe },
				{ "EOptitrackBone::LThumb1", (int64)EOptitrackBone::LThumb1 },
				{ "EOptitrackBone::LThumb2", (int64)EOptitrackBone::LThumb2 },
				{ "EOptitrackBone::LThumb3", (int64)EOptitrackBone::LThumb3 },
				{ "EOptitrackBone::LIndex1", (int64)EOptitrackBone::LIndex1 },
				{ "EOptitrackBone::LIndex2", (int64)EOptitrackBone::LIndex2 },
				{ "EOptitrackBone::LIndex3", (int64)EOptitrackBone::LIndex3 },
				{ "EOptitrackBone::LMiddle1", (int64)EOptitrackBone::LMiddle1 },
				{ "EOptitrackBone::LMiddle2", (int64)EOptitrackBone::LMiddle2 },
				{ "EOptitrackBone::LMiddle3", (int64)EOptitrackBone::LMiddle3 },
				{ "EOptitrackBone::LRing1", (int64)EOptitrackBone::LRing1 },
				{ "EOptitrackBone::LRing2", (int64)EOptitrackBone::LRing2 },
				{ "EOptitrackBone::LRing3", (int64)EOptitrackBone::LRing3 },
				{ "EOptitrackBone::LPinky1", (int64)EOptitrackBone::LPinky1 },
				{ "EOptitrackBone::LPinky2", (int64)EOptitrackBone::LPinky2 },
				{ "EOptitrackBone::LPinky3", (int64)EOptitrackBone::LPinky3 },
				{ "EOptitrackBone::RThumb1", (int64)EOptitrackBone::RThumb1 },
				{ "EOptitrackBone::RThumb2", (int64)EOptitrackBone::RThumb2 },
				{ "EOptitrackBone::RThumb3", (int64)EOptitrackBone::RThumb3 },
				{ "EOptitrackBone::RIndex1", (int64)EOptitrackBone::RIndex1 },
				{ "EOptitrackBone::RIndex2", (int64)EOptitrackBone::RIndex2 },
				{ "EOptitrackBone::RIndex3", (int64)EOptitrackBone::RIndex3 },
				{ "EOptitrackBone::RMiddle1", (int64)EOptitrackBone::RMiddle1 },
				{ "EOptitrackBone::RMiddle2", (int64)EOptitrackBone::RMiddle2 },
				{ "EOptitrackBone::RMiddle3", (int64)EOptitrackBone::RMiddle3 },
				{ "EOptitrackBone::RRing1", (int64)EOptitrackBone::RRing1 },
				{ "EOptitrackBone::RRing2", (int64)EOptitrackBone::RRing2 },
				{ "EOptitrackBone::RRing3", (int64)EOptitrackBone::RRing3 },
				{ "EOptitrackBone::RPinky1", (int64)EOptitrackBone::RPinky1 },
				{ "EOptitrackBone::RPinky2", (int64)EOptitrackBone::RPinky2 },
				{ "EOptitrackBone::RPinky3", (int64)EOptitrackBone::RPinky3 },
				{ "EOptitrackBone::NumBones", (int64)EOptitrackBone::NumBones },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OptitrackNatnet,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				"EOptitrackBone",
				RF_Public|RF_Transient|RF_MarkAsNative,
				nullptr,
				(uint8)UEnum::ECppForm::EnumClass,
				"EOptitrackBone",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EOptitrackBoneNamingConvention_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OptitrackNatnet_EOptitrackBoneNamingConvention, Z_Construct_UPackage__Script_OptitrackNatnet(), TEXT("EOptitrackBoneNamingConvention"));
		}
		return Singleton;
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOptitrackBoneNamingConvention(EOptitrackBoneNamingConvention_StaticEnum, TEXT("/Script/OptitrackNatnet"), TEXT("EOptitrackBoneNamingConvention"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OptitrackNatnet_EOptitrackBoneNamingConvention_CRC() { return 4212730314U; }
	UEnum* Z_Construct_UEnum_OptitrackNatnet_EOptitrackBoneNamingConvention()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackNatnet();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOptitrackBoneNamingConvention"), 0, Get_Z_Construct_UEnum_OptitrackNatnet_EOptitrackBoneNamingConvention_CRC(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOptitrackBoneNamingConvention::Motive", (int64)EOptitrackBoneNamingConvention::Motive },
				{ "EOptitrackBoneNamingConvention::FBX", (int64)EOptitrackBoneNamingConvention::FBX },
				{ "EOptitrackBoneNamingConvention::BVH", (int64)EOptitrackBoneNamingConvention::BVH },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OptitrackNatnet,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				"EOptitrackBoneNamingConvention",
				RF_Public|RF_Transient|RF_MarkAsNative,
				nullptr,
				(uint8)UEnum::ECppForm::EnumClass,
				"EOptitrackBoneNamingConvention",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FOptitrackSkeletonState::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPTITRACKNATNET_API uint32 Get_Z_Construct_UScriptStruct_FOptitrackSkeletonState_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOptitrackSkeletonState, Z_Construct_UPackage__Script_OptitrackNatnet(), TEXT("OptitrackSkeletonState"), sizeof(FOptitrackSkeletonState), Get_Z_Construct_UScriptStruct_FOptitrackSkeletonState_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOptitrackSkeletonState(FOptitrackSkeletonState::StaticStruct, TEXT("/Script/OptitrackNatnet"), TEXT("OptitrackSkeletonState"), false, nullptr, nullptr);
static struct FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackSkeletonState
{
	FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackSkeletonState()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("OptitrackSkeletonState")),new UScriptStruct::TCppStructOps<FOptitrackSkeletonState>);
	}
} ScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackSkeletonState;
	struct Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FullSkeleton_MetaData[];
#endif
		static void NewProp_FullSkeleton_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_FullSkeleton;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldSpaceBoneTransforms_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_WorldSpaceBoneTransforms;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_WorldSpaceBoneTransforms_Key_KeyProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorldSpaceBoneTransforms_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonePoses_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_BonePoses;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_BonePoses_Key_KeyProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BonePoses_ValueProp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOptitrackSkeletonState>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_FullSkeleton_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_FullSkeleton_SetBit(void* Obj)
	{
		((FOptitrackSkeletonState*)Obj)->FullSkeleton = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_FullSkeleton = { UE4CodeGen_Private::EPropertyClass::Bool, "FullSkeleton", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(FOptitrackSkeletonState), &Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_FullSkeleton_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_FullSkeleton_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_FullSkeleton_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_WorldSpaceBoneTransforms_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_WorldSpaceBoneTransforms = { UE4CodeGen_Private::EPropertyClass::Map, "WorldSpaceBoneTransforms", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackSkeletonState, WorldSpaceBoneTransforms), METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_WorldSpaceBoneTransforms_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_WorldSpaceBoneTransforms_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_WorldSpaceBoneTransforms_Key_KeyProp = { UE4CodeGen_Private::EPropertyClass::Int, "WorldSpaceBoneTransforms_Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_WorldSpaceBoneTransforms_ValueProp = { UE4CodeGen_Private::EPropertyClass::Struct, "WorldSpaceBoneTransforms", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 1, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_BonePoses_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_BonePoses = { UE4CodeGen_Private::EPropertyClass::Map, "BonePoses", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackSkeletonState, BonePoses), METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_BonePoses_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_BonePoses_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_BonePoses_Key_KeyProp = { UE4CodeGen_Private::EPropertyClass::Int, "BonePoses_Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_BonePoses_ValueProp = { UE4CodeGen_Private::EPropertyClass::Struct, "BonePoses", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 1, Z_Construct_UScriptStruct_FOptitrackPose, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_FullSkeleton,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_WorldSpaceBoneTransforms,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_WorldSpaceBoneTransforms_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_WorldSpaceBoneTransforms_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_BonePoses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_BonePoses_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::NewProp_BonePoses_ValueProp,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnet,
		nullptr,
		&NewStructOps,
		"OptitrackSkeletonState",
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		sizeof(FOptitrackSkeletonState),
		alignof(FOptitrackSkeletonState),
		Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::PropPointers),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOptitrackSkeletonState()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOptitrackSkeletonState_CRC();
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackNatnet();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OptitrackSkeletonState"), sizeof(FOptitrackSkeletonState), Get_Z_Construct_UScriptStruct_FOptitrackSkeletonState_CRC(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOptitrackSkeletonState_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOptitrackSkeletonState_CRC() { return 2676983692U; }
class UScriptStruct* FOptitrackSkeletonDefinition::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPTITRACKNATNET_API uint32 Get_Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition, Z_Construct_UPackage__Script_OptitrackNatnet(), TEXT("OptitrackSkeletonDefinition"), sizeof(FOptitrackSkeletonDefinition), Get_Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOptitrackSkeletonDefinition(FOptitrackSkeletonDefinition::StaticStruct, TEXT("/Script/OptitrackNatnet"), TEXT("OptitrackSkeletonDefinition"), false, nullptr, nullptr);
static struct FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackSkeletonDefinition
{
	FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackSkeletonDefinition()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("OptitrackSkeletonDefinition")),new UScriptStruct::TCppStructOps<FOptitrackSkeletonDefinition>);
	}
} ScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackSkeletonDefinition;
	struct Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bones_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Bones;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Bones_Key_KeyProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Bones_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Id;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOptitrackSkeletonDefinition>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Bones_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Bones = { UE4CodeGen_Private::EPropertyClass::Map, "Bones", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackSkeletonDefinition, Bones), METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Bones_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Bones_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Bones_Key_KeyProp = { UE4CodeGen_Private::EPropertyClass::Int, "Bones_Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Bones_ValueProp = { UE4CodeGen_Private::EPropertyClass::Struct, "Bones", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 1, Z_Construct_UScriptStruct_FOptitrackBoneDefinition, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Name = { UE4CodeGen_Private::EPropertyClass::Name, "Name", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackSkeletonDefinition, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Name_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Id_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Id = { UE4CodeGen_Private::EPropertyClass::Int, "Id", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackSkeletonDefinition, Id), METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Id_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Id_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Bones,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Bones_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Bones_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::NewProp_Id,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnet,
		nullptr,
		&NewStructOps,
		"OptitrackSkeletonDefinition",
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		sizeof(FOptitrackSkeletonDefinition),
		alignof(FOptitrackSkeletonDefinition),
		Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::PropPointers),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_CRC();
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackNatnet();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OptitrackSkeletonDefinition"), sizeof(FOptitrackSkeletonDefinition), Get_Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_CRC(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition_CRC() { return 3058073537U; }
class UScriptStruct* FOptitrackBoneDefinition::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPTITRACKNATNET_API uint32 Get_Z_Construct_UScriptStruct_FOptitrackBoneDefinition_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOptitrackBoneDefinition, Z_Construct_UPackage__Script_OptitrackNatnet(), TEXT("OptitrackBoneDefinition"), sizeof(FOptitrackBoneDefinition), Get_Z_Construct_UScriptStruct_FOptitrackBoneDefinition_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOptitrackBoneDefinition(FOptitrackBoneDefinition::StaticStruct, TEXT("/Script/OptitrackNatnet"), TEXT("OptitrackBoneDefinition"), false, nullptr, nullptr);
static struct FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackBoneDefinition
{
	FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackBoneDefinition()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("OptitrackBoneDefinition")),new UScriptStruct::TCppStructOps<FOptitrackBoneDefinition>);
	}
} ScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackBoneDefinition;
	struct Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocalOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentId_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ParentId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Id;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
		{ "ToolTip", "The streaming skeleton begins at the hip bone; the root bone is implicit, and not streamed." },
	};
#endif
	void* Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOptitrackBoneDefinition>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_LocalOffset_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_LocalOffset = { UE4CodeGen_Private::EPropertyClass::Struct, "LocalOffset", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackBoneDefinition, LocalOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_LocalOffset_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_LocalOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_Name = { UE4CodeGen_Private::EPropertyClass::Name, "Name", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackBoneDefinition, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_Name_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_ParentId_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_ParentId = { UE4CodeGen_Private::EPropertyClass::Int, "ParentId", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackBoneDefinition, ParentId), METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_ParentId_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_ParentId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_Id_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_Id = { UE4CodeGen_Private::EPropertyClass::Int, "Id", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackBoneDefinition, Id), METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_Id_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_Id_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_LocalOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_ParentId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::NewProp_Id,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnet,
		nullptr,
		&NewStructOps,
		"OptitrackBoneDefinition",
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		sizeof(FOptitrackBoneDefinition),
		alignof(FOptitrackBoneDefinition),
		Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::PropPointers),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOptitrackBoneDefinition()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOptitrackBoneDefinition_CRC();
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackNatnet();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OptitrackBoneDefinition"), sizeof(FOptitrackBoneDefinition), Get_Z_Construct_UScriptStruct_FOptitrackBoneDefinition_CRC(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOptitrackBoneDefinition_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOptitrackBoneDefinition_CRC() { return 976677972U; }
class UScriptStruct* FOptitrackPose::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPTITRACKNATNET_API uint32 Get_Z_Construct_UScriptStruct_FOptitrackPose_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOptitrackPose, Z_Construct_UPackage__Script_OptitrackNatnet(), TEXT("OptitrackPose"), sizeof(FOptitrackPose), Get_Z_Construct_UScriptStruct_FOptitrackPose_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOptitrackPose(FOptitrackPose::StaticStruct, TEXT("/Script/OptitrackNatnet"), TEXT("OptitrackPose"), false, nullptr, nullptr);
static struct FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackPose
{
	FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackPose()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("OptitrackPose")),new UScriptStruct::TCppStructOps<FOptitrackPose>);
	}
} ScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackPose;
	struct Z_Construct_UScriptStruct_FOptitrackPose_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Orientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Orientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackPose_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOptitrackPose_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOptitrackPose>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackPose_Statics::NewProp_Orientation_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOptitrackPose_Statics::NewProp_Orientation = { UE4CodeGen_Private::EPropertyClass::Struct, "Orientation", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackPose, Orientation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackPose_Statics::NewProp_Orientation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackPose_Statics::NewProp_Orientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackPose_Statics::NewProp_Position_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Public/OptitrackSkeletonTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOptitrackPose_Statics::NewProp_Position = { UE4CodeGen_Private::EPropertyClass::Struct, "Position", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackPose, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackPose_Statics::NewProp_Position_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackPose_Statics::NewProp_Position_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOptitrackPose_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackPose_Statics::NewProp_Orientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackPose_Statics::NewProp_Position,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOptitrackPose_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnet,
		nullptr,
		&NewStructOps,
		"OptitrackPose",
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		sizeof(FOptitrackPose),
		alignof(FOptitrackPose),
		Z_Construct_UScriptStruct_FOptitrackPose_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackPose_Statics::PropPointers),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackPose_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackPose_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOptitrackPose()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOptitrackPose_CRC();
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackNatnet();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OptitrackPose"), sizeof(FOptitrackPose), Get_Z_Construct_UScriptStruct_FOptitrackPose_CRC(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOptitrackPose_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOptitrackPose_CRC() { return 1213200966U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
