// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OptitrackNatnet/Classes/OptitrackSkeletonComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOptitrackSkeletonComponent() {}
// Cross Module References
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_UOptitrackSkeletonComponent_NoRegister();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_UOptitrackSkeletonComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_OptitrackNatnet();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_UOptitrackAnimInstance_NoRegister();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_AOptitrackClientOrigin_NoRegister();
// End Cross Module References
	void UOptitrackSkeletonComponent::StaticRegisterNativesUOptitrackSkeletonComponent()
	{
	}
	UClass* Z_Construct_UClass_UOptitrackSkeletonComponent_NoRegister()
	{
		return UOptitrackSkeletonComponent::StaticClass();
	}
	struct Z_Construct_UClass_UOptitrackSkeletonComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OptitrackAnimInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OptitrackAnimInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HideOnInvalidDefinition_MetaData[];
#endif
		static void NewProp_HideOnInvalidDefinition_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_HideOnInvalidDefinition;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StreamingClientOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StreamingClientOrigin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceSkeletonAssetName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SourceSkeletonAssetName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnet,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "OptiTrack" },
		{ "IncludePath", "OptitrackSkeletonComponent.h" },
		{ "ModuleRelativePath", "Classes/OptitrackSkeletonComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "A component that drives the position and rotation of its actor's root\ncomponent based on the extracted XY planar movement of a tracked skeleton's\nroot motion / hip movement. Designed to work with FAnimNode_OptitrackSkeleton." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_OptitrackAnimInstance_MetaData[] = {
		{ "ModuleRelativePath", "Classes/OptitrackSkeletonComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_OptitrackAnimInstance = { UE4CodeGen_Private::EPropertyClass::Object, "OptitrackAnimInstance", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0020080000000000, 1, nullptr, STRUCT_OFFSET(UOptitrackSkeletonComponent, OptitrackAnimInstance), Z_Construct_UClass_UOptitrackAnimInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_OptitrackAnimInstance_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_OptitrackAnimInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_HideOnInvalidDefinition_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackSkeletonComponent.h" },
		{ "ToolTip", "If asset isn't being streamed from Motive, hide skeleton.\nDefaults to true." },
	};
#endif
	void Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_HideOnInvalidDefinition_SetBit(void* Obj)
	{
		((UOptitrackSkeletonComponent*)Obj)->HideOnInvalidDefinition = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_HideOnInvalidDefinition = { UE4CodeGen_Private::EPropertyClass::Bool, "HideOnInvalidDefinition", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UOptitrackSkeletonComponent), &Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_HideOnInvalidDefinition_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_HideOnInvalidDefinition_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_HideOnInvalidDefinition_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_StreamingClientOrigin_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackSkeletonComponent.h" },
		{ "ToolTip", "If your scene contains multiple client origin objects, you can specify\nwhich one to use. If unset, this defaults to the first client origin\nthat's found in the world." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_StreamingClientOrigin = { UE4CodeGen_Private::EPropertyClass::Object, "StreamingClientOrigin", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000005, 1, nullptr, STRUCT_OFFSET(UOptitrackSkeletonComponent, StreamingClientOrigin), Z_Construct_UClass_AOptitrackClientOrigin_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_StreamingClientOrigin_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_StreamingClientOrigin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_SourceSkeletonAssetName_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackSkeletonComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_SourceSkeletonAssetName = { UE4CodeGen_Private::EPropertyClass::Name, "SourceSkeletonAssetName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000025, 1, nullptr, STRUCT_OFFSET(UOptitrackSkeletonComponent, SourceSkeletonAssetName), METADATA_PARAMS(Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_SourceSkeletonAssetName_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_SourceSkeletonAssetName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_OptitrackAnimInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_HideOnInvalidDefinition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_StreamingClientOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::NewProp_SourceSkeletonAssetName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOptitrackSkeletonComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::ClassParams = {
		&UOptitrackSkeletonComponent::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x00B000A4u,
		nullptr, 0,
		Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::PropPointers),
		"Engine",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOptitrackSkeletonComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOptitrackSkeletonComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOptitrackSkeletonComponent, 575329434);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOptitrackSkeletonComponent(Z_Construct_UClass_UOptitrackSkeletonComponent, &UOptitrackSkeletonComponent::StaticClass, TEXT("/Script/OptitrackNatnet"), TEXT("UOptitrackSkeletonComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOptitrackSkeletonComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
