// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OptitrackNatnet/Classes/OptitrackClientOrigin.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOptitrackClientOrigin() {}
// Cross Module References
	OPTITRACKNATNET_API UEnum* Z_Construct_UEnum_OptitrackNatnet_EOptitrackForwardAxisType();
	UPackage* Z_Construct_UPackage__Script_OptitrackNatnet();
	OPTITRACKNATNET_API UEnum* Z_Construct_UEnum_OptitrackNatnet_EOptitrackClientConnectionType();
	OPTITRACKNATNET_API UEnum* Z_Construct_UEnum_OptitrackNatnet_EOptitrackTimestampType();
	OPTITRACKNATNET_API UScriptStruct* Z_Construct_UScriptStruct_FOptitrackMarkerState();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	OPTITRACKNATNET_API UScriptStruct* Z_Construct_UScriptStruct_FOptitrackRigidBodyState();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_AOptitrackClientOrigin_NoRegister();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_AOptitrackClientOrigin();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition();
	OPTITRACKNATNET_API UScriptStruct* Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState();
	OPTITRACKNATNET_API UScriptStruct* Z_Construct_UScriptStruct_FOptitrackSkeletonState();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized();
	OPTITRACKNATNET_API UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient();
	OPTITRACKNATNET_API UEnum* Z_Construct_UEnum_OptitrackNatnet_EOptitrackBoneNamingConvention();
// End Cross Module References
	static UEnum* EOptitrackForwardAxisType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OptitrackNatnet_EOptitrackForwardAxisType, Z_Construct_UPackage__Script_OptitrackNatnet(), TEXT("EOptitrackForwardAxisType"));
		}
		return Singleton;
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOptitrackForwardAxisType(EOptitrackForwardAxisType_StaticEnum, TEXT("/Script/OptitrackNatnet"), TEXT("EOptitrackForwardAxisType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OptitrackNatnet_EOptitrackForwardAxisType_CRC() { return 2652567993U; }
	UEnum* Z_Construct_UEnum_OptitrackNatnet_EOptitrackForwardAxisType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackNatnet();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOptitrackForwardAxisType"), 0, Get_Z_Construct_UEnum_OptitrackNatnet_EOptitrackForwardAxisType_CRC(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOptitrackForwardAxisType::Z_Positive", (int64)EOptitrackForwardAxisType::Z_Positive },
				{ "EOptitrackForwardAxisType::X_Positive", (int64)EOptitrackForwardAxisType::X_Positive },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
				{ "X_Positive.DisplayName", "+X Forward" },
				{ "Z_Positive.DisplayName", "+Z Forward" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OptitrackNatnet,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				"EOptitrackForwardAxisType",
				RF_Public|RF_Transient|RF_MarkAsNative,
				nullptr,
				(uint8)UEnum::ECppForm::EnumClass,
				"EOptitrackForwardAxisType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EOptitrackClientConnectionType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OptitrackNatnet_EOptitrackClientConnectionType, Z_Construct_UPackage__Script_OptitrackNatnet(), TEXT("EOptitrackClientConnectionType"));
		}
		return Singleton;
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOptitrackClientConnectionType(EOptitrackClientConnectionType_StaticEnum, TEXT("/Script/OptitrackNatnet"), TEXT("EOptitrackClientConnectionType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OptitrackNatnet_EOptitrackClientConnectionType_CRC() { return 470689308U; }
	UEnum* Z_Construct_UEnum_OptitrackNatnet_EOptitrackClientConnectionType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackNatnet();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOptitrackClientConnectionType"), 0, Get_Z_Construct_UEnum_OptitrackNatnet_EOptitrackClientConnectionType_CRC(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOptitrackClientConnectionType::Multicast", (int64)EOptitrackClientConnectionType::Multicast },
				{ "EOptitrackClientConnectionType::Unicast", (int64)EOptitrackClientConnectionType::Unicast },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OptitrackNatnet,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				"EOptitrackClientConnectionType",
				RF_Public|RF_Transient|RF_MarkAsNative,
				nullptr,
				(uint8)UEnum::ECppForm::EnumClass,
				"EOptitrackClientConnectionType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EOptitrackTimestampType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OptitrackNatnet_EOptitrackTimestampType, Z_Construct_UPackage__Script_OptitrackNatnet(), TEXT("EOptitrackTimestampType"));
		}
		return Singleton;
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOptitrackTimestampType(EOptitrackTimestampType_StaticEnum, TEXT("/Script/OptitrackNatnet"), TEXT("EOptitrackTimestampType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OptitrackNatnet_EOptitrackTimestampType_CRC() { return 1145612642U; }
	UEnum* Z_Construct_UEnum_OptitrackNatnet_EOptitrackTimestampType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackNatnet();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOptitrackTimestampType"), 0, Get_Z_Construct_UEnum_OptitrackNatnet_EOptitrackTimestampType_CRC(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOptitrackTimestampType::Unknown", (int64)EOptitrackTimestampType::Unknown },
				{ "EOptitrackTimestampType::LocalArrivalTime", (int64)EOptitrackTimestampType::LocalArrivalTime },
				{ "EOptitrackTimestampType::ServerAggregationTime", (int64)EOptitrackTimestampType::ServerAggregationTime },
				{ "EOptitrackTimestampType::ServerExposureTime", (int64)EOptitrackTimestampType::ServerExposureTime },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "LocalArrivalTime.ToolTip", "When the NatNet frame callback occurred. Fallback when precise metrics are unavailable." },
				{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
				{ "ServerAggregationTime.ToolTip", "Clock synchronized estimate of when the camera data became available on the server." },
				{ "ServerExposureTime.ToolTip", "Clock synchronized estimate of when the camera exposure occurred." },
				{ "Unknown.ToolTip", "We don't know what the server supports until after we've received at least one frame." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OptitrackNatnet,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				"EOptitrackTimestampType",
				RF_Public|RF_Transient|RF_MarkAsNative,
				nullptr,
				(uint8)UEnum::ECppForm::EnumClass,
				"EOptitrackTimestampType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FOptitrackMarkerState::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPTITRACKNATNET_API uint32 Get_Z_Construct_UScriptStruct_FOptitrackMarkerState_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOptitrackMarkerState, Z_Construct_UPackage__Script_OptitrackNatnet(), TEXT("OptitrackMarkerState"), sizeof(FOptitrackMarkerState), Get_Z_Construct_UScriptStruct_FOptitrackMarkerState_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOptitrackMarkerState(FOptitrackMarkerState::StaticStruct, TEXT("/Script/OptitrackNatnet"), TEXT("OptitrackMarkerState"), false, nullptr, nullptr);
static struct FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackMarkerState
{
	FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackMarkerState()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("OptitrackMarkerState")),new UScriptStruct::TCppStructOps<FOptitrackMarkerState>);
	}
} ScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackMarkerState;
	struct Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimestampPlatformSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_TimestampPlatformSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Represents a tracked marker." },
	};
#endif
	void* Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOptitrackMarkerState>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_Id_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Marker Id in Motive." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_Id = { UE4CodeGen_Private::EPropertyClass::Int, "Id", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackMarkerState, Id), METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_Id_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_Id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_Position_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Position in tracking space." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_Position = { UE4CodeGen_Private::EPropertyClass::Struct, "Position", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackMarkerState, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_Position_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_TimestampPlatformSeconds_MetaData[] = {
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Timestamp corresponding to this marker. Exact semantics can vary depending on\nserver support; see EOptitrackTimestampType and AOptitrackClientOrigin::GetTimestampType.\n\nCan be directly compared with values returned by FPlatformTime::Seconds()." },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_TimestampPlatformSeconds = { UE4CodeGen_Private::EPropertyClass::Double, "TimestampPlatformSeconds", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000000, 1, nullptr, STRUCT_OFFSET(FOptitrackMarkerState, TimestampPlatformSeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_TimestampPlatformSeconds_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_TimestampPlatformSeconds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_Id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::NewProp_TimestampPlatformSeconds,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnet,
		nullptr,
		&NewStructOps,
		"OptitrackMarkerState",
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		sizeof(FOptitrackMarkerState),
		alignof(FOptitrackMarkerState),
		Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::PropPointers),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOptitrackMarkerState()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOptitrackMarkerState_CRC();
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackNatnet();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OptitrackMarkerState"), sizeof(FOptitrackMarkerState), Get_Z_Construct_UScriptStruct_FOptitrackMarkerState_CRC(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOptitrackMarkerState_CRC() { return 312875079U; }
class UScriptStruct* FOptitrackRigidBodyState::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPTITRACKNATNET_API uint32 Get_Z_Construct_UScriptStruct_FOptitrackRigidBodyState_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOptitrackRigidBodyState, Z_Construct_UPackage__Script_OptitrackNatnet(), TEXT("OptitrackRigidBodyState"), sizeof(FOptitrackRigidBodyState), Get_Z_Construct_UScriptStruct_FOptitrackRigidBodyState_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOptitrackRigidBodyState(FOptitrackRigidBodyState::StaticStruct, TEXT("/Script/OptitrackNatnet"), TEXT("OptitrackRigidBodyState"), false, nullptr, nullptr);
static struct FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackRigidBodyState
{
	FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackRigidBodyState()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("OptitrackRigidBodyState")),new UScriptStruct::TCppStructOps<FOptitrackRigidBodyState>);
	}
} ScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackRigidBodyState;
	struct Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Orientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Orientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimestampPlatformSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_TimestampPlatformSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Represents a pose for a tracked rigid body." },
	};
#endif
	void* Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOptitrackRigidBodyState>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_Orientation_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Orientation in tracking space." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_Orientation = { UE4CodeGen_Private::EPropertyClass::Struct, "Orientation", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackRigidBodyState, Orientation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_Orientation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_Orientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_Position_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Position in tracking space." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_Position = { UE4CodeGen_Private::EPropertyClass::Struct, "Position", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000014, 1, nullptr, STRUCT_OFFSET(FOptitrackRigidBodyState, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_Position_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_TimestampPlatformSeconds_MetaData[] = {
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Timestamp corresponding to this rigid body pose. Exact semantics can vary depending on\nserver support; see EOptitrackTimestampType and AOptitrackClientOrigin::GetTimestampType.\n\nCan be directly compared with values returned by FPlatformTime::Seconds()." },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_TimestampPlatformSeconds = { UE4CodeGen_Private::EPropertyClass::Double, "TimestampPlatformSeconds", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000000, 1, nullptr, STRUCT_OFFSET(FOptitrackRigidBodyState, TimestampPlatformSeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_TimestampPlatformSeconds_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_TimestampPlatformSeconds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_Orientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::NewProp_TimestampPlatformSeconds,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnet,
		nullptr,
		&NewStructOps,
		"OptitrackRigidBodyState",
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		sizeof(FOptitrackRigidBodyState),
		alignof(FOptitrackRigidBodyState),
		Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::PropPointers),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOptitrackRigidBodyState()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOptitrackRigidBodyState_CRC();
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackNatnet();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OptitrackRigidBodyState"), sizeof(FOptitrackRigidBodyState), Get_Z_Construct_UScriptStruct_FOptitrackRigidBodyState_CRC(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOptitrackRigidBodyState_CRC() { return 2776389285U; }
	void AOptitrackClientOrigin::StaticRegisterNativesAOptitrackClientOrigin()
	{
		UClass* Class = AOptitrackClientOrigin::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "FindDefaultClientOrigin", &AOptitrackClientOrigin::execFindDefaultClientOrigin },
			{ "FindHmdClientOrigin", &AOptitrackClientOrigin::execFindHmdClientOrigin },
			{ "FindSkeletonDefinition", &AOptitrackClientOrigin::execFindSkeletonDefinition },
			{ "GetDataDescription", &AOptitrackClientOrigin::execGetDataDescription },
			{ "GetLatestMarkerState", &AOptitrackClientOrigin::execGetLatestMarkerState },
			{ "GetLatestMarkerStateUntransformed", &AOptitrackClientOrigin::execGetLatestMarkerStateUntransformed },
			{ "GetLatestRigidBodyState", &AOptitrackClientOrigin::execGetLatestRigidBodyState },
			{ "GetLatestRigidBodyStateUntransformed", &AOptitrackClientOrigin::execGetLatestRigidBodyStateUntransformed },
			{ "GetLatestSkeletonState", &AOptitrackClientOrigin::execGetLatestSkeletonState },
			{ "GetTimestampType", &AOptitrackClientOrigin::execGetTimestampType },
			{ "InitializeClient", &AOptitrackClientOrigin::execInitializeClient },
			{ "IsClientConnected", &AOptitrackClientOrigin::execIsClientConnected },
			{ "IsClientInitialized", &AOptitrackClientOrigin::execIsClientInitialized },
			{ "ShutdownClient", &AOptitrackClientOrigin::execShutdownClient },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin_Statics
	{
		struct OptitrackClientOrigin_eventFindDefaultClientOrigin_Parms
		{
			UWorld* World;
			AOptitrackClientOrigin* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_World;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventFindDefaultClientOrigin_Parms, ReturnValue), Z_Construct_UClass_AOptitrackClientOrigin_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin_Statics::NewProp_World = { UE4CodeGen_Private::EPropertyClass::Object, "World", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventFindDefaultClientOrigin_Parms, World), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin_Statics::NewProp_World,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Helper for the common case where only a single client origin is present.\nUsed to provide a default for any UOptitrackRigidBody components which\ndon't otherwise specify a particular origin to use.\nNote: This is potentially expensive and the result should be cached.\n@return The first AOptitrackClientOrigin actor found in @World." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "FindDefaultClientOrigin", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OptitrackClientOrigin_eventFindDefaultClientOrigin_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin_Statics
	{
		struct OptitrackClientOrigin_eventFindHmdClientOrigin_Parms
		{
			UWorld* World;
			AOptitrackClientOrigin* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_World;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventFindHmdClientOrigin_Parms, ReturnValue), Z_Construct_UClass_AOptitrackClientOrigin_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin_Statics::NewProp_World = { UE4CodeGen_Private::EPropertyClass::Object, "World", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventFindHmdClientOrigin_Parms, World), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin_Statics::NewProp_World,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Finds the first AOptitrackClientOrigin specifying an HMD rigid body ID.\nNote: This is potentially expensive and the result should be cached.\n@return The first suitable AOptitrackClientOrigin actor found in @World." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "FindHmdClientOrigin", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(OptitrackClientOrigin_eventFindHmdClientOrigin_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics
	{
		struct OptitrackClientOrigin_eventFindSkeletonDefinition_Parms
		{
			FName SkeletonName;
			FOptitrackSkeletonDefinition OutSkelDef;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutSkelDef;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletonName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SkeletonName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackClientOrigin_eventFindSkeletonDefinition_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackClientOrigin_eventFindSkeletonDefinition_Parms), &Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::NewProp_OutSkelDef = { UE4CodeGen_Private::EPropertyClass::Struct, "OutSkelDef", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventFindSkeletonDefinition_Parms, OutSkelDef), Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::NewProp_SkeletonName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::NewProp_SkeletonName = { UE4CodeGen_Private::EPropertyClass::Name, "SkeletonName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventFindSkeletonDefinition_Parms, SkeletonName), METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::NewProp_SkeletonName_MetaData, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::NewProp_SkeletonName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::NewProp_OutSkelDef,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::NewProp_SkeletonName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack|Advanced" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "FindSkeletonDefinition", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04420401, sizeof(OptitrackClientOrigin_eventFindSkeletonDefinition_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription_Statics
	{
		struct OptitrackClientOrigin_eventGetDataDescription_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackClientOrigin_eventGetDataDescription_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackClientOrigin_eventGetDataDescription_Parms), &Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Request NatNet data description from server.\nInvalid to call if @IsClientInitialized() returns false.\n@return True on success, false on failure." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "GetDataDescription", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(OptitrackClientOrigin_eventGetDataDescription_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics
	{
		struct OptitrackClientOrigin_eventGetLatestMarkerState_Parms
		{
			int32 markerId;
			FOptitrackMarkerState OutMarkerState;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutMarkerState;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_markerId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackClientOrigin_eventGetLatestMarkerState_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackClientOrigin_eventGetLatestMarkerState_Parms), &Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::NewProp_OutMarkerState = { UE4CodeGen_Private::EPropertyClass::Struct, "OutMarkerState", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventGetLatestMarkerState_Parms, OutMarkerState), Z_Construct_UScriptStruct_FOptitrackMarkerState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::NewProp_markerId = { UE4CodeGen_Private::EPropertyClass::Int, "markerId", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventGetLatestMarkerState_Parms, markerId), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::NewProp_OutMarkerState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::NewProp_markerId,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Retrieves latest available FOptitrackMarkerState for the marker identified by the\n@markerId parameter. Note that this data can be stale/non-existent if the marker\nhasn't streamed any new data recently.\n\nThe tracking space pose is transformed by the offset/rotation/scale of this actor,\nsuch that the placement of this actor corresponds to the tracking space origin.\n\n@param OutMarkerState Receives latest available marker state (if any).\n@return True if any marker state was available for the specified ID,\n        otherwise false." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "GetLatestMarkerState", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04420401, sizeof(OptitrackClientOrigin_eventGetLatestMarkerState_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics
	{
		struct OptitrackClientOrigin_eventGetLatestMarkerStateUntransformed_Parms
		{
			int32 markerId;
			FOptitrackMarkerState OutMarkerState;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutMarkerState;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_markerId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackClientOrigin_eventGetLatestMarkerStateUntransformed_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackClientOrigin_eventGetLatestMarkerStateUntransformed_Parms), &Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::NewProp_OutMarkerState = { UE4CodeGen_Private::EPropertyClass::Struct, "OutMarkerState", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventGetLatestMarkerStateUntransformed_Parms, OutMarkerState), Z_Construct_UScriptStruct_FOptitrackMarkerState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::NewProp_markerId = { UE4CodeGen_Private::EPropertyClass::Int, "markerId", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventGetLatestMarkerStateUntransformed_Parms, markerId), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::NewProp_OutMarkerState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::NewProp_markerId,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack|Advanced" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Retrieves latest available FOptitrackMarkerState for the marker identified by the\n@markerId parameter. Note that this data can be stale/non-existent if the marker\nhasn't streamed any new data recently.\n\nThis \"Untransformed\" version returns the tracking space pose, applying only the global\nWorldToMeters scale. It does not take into account the transform of this actor.\n\n@param OutMarkerState Receives latest available marker state (if any).\n@return True if any marker state was available for the specified ID,\n        otherwise false." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "GetLatestMarkerStateUntransformed", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04420401, sizeof(OptitrackClientOrigin_eventGetLatestMarkerStateUntransformed_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics
	{
		struct OptitrackClientOrigin_eventGetLatestRigidBodyState_Parms
		{
			int32 RbId;
			FOptitrackRigidBodyState OutRbState;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutRbState;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RbId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackClientOrigin_eventGetLatestRigidBodyState_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackClientOrigin_eventGetLatestRigidBodyState_Parms), &Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::NewProp_OutRbState = { UE4CodeGen_Private::EPropertyClass::Struct, "OutRbState", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventGetLatestRigidBodyState_Parms, OutRbState), Z_Construct_UScriptStruct_FOptitrackRigidBodyState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::NewProp_RbId = { UE4CodeGen_Private::EPropertyClass::Int, "RbId", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventGetLatestRigidBodyState_Parms, RbId), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::NewProp_OutRbState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::NewProp_RbId,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Retrieves latest available FOptitrackRigidBodyState for the rigid body identified by the\n@RbId parameter. Note that this data can be stale if the rigid body in question hasn't\nstreamed any new data recently.\n\nThe tracking space pose is transformed by the offset/rotation/scale of this actor,\nsuch that the placement of this actor corresponds to the tracking space origin.\n\n@param OutRbState Receives latest available rigid body state (if any).\n@return True if any rigid body state was available for the specified ID,\n        otherwise false." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "GetLatestRigidBodyState", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04420401, sizeof(OptitrackClientOrigin_eventGetLatestRigidBodyState_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics
	{
		struct OptitrackClientOrigin_eventGetLatestRigidBodyStateUntransformed_Parms
		{
			int32 RbId;
			FOptitrackRigidBodyState OutRbState;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutRbState;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RbId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackClientOrigin_eventGetLatestRigidBodyStateUntransformed_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackClientOrigin_eventGetLatestRigidBodyStateUntransformed_Parms), &Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::NewProp_OutRbState = { UE4CodeGen_Private::EPropertyClass::Struct, "OutRbState", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventGetLatestRigidBodyStateUntransformed_Parms, OutRbState), Z_Construct_UScriptStruct_FOptitrackRigidBodyState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::NewProp_RbId = { UE4CodeGen_Private::EPropertyClass::Int, "RbId", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventGetLatestRigidBodyStateUntransformed_Parms, RbId), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::NewProp_OutRbState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::NewProp_RbId,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack|Advanced" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Retrieves latest available FOptitrackRigidBodyState for the rigid body identified by the\n@RbId parameter. Note that this data can be stale if the rigid body in question hasn't\nstreamed any new data recently.\n\nThis \"Untransformed\" version returns the tracking space pose, applying only the global\nWorldToMeters scale. It does not take into account the transform of this actor.\n\n@param OutRbState Receives latest available rigid body state (if any).\n@return True if any rigid body state was available for the specified ID,\n        otherwise false." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "GetLatestRigidBodyStateUntransformed", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04420401, sizeof(OptitrackClientOrigin_eventGetLatestRigidBodyStateUntransformed_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics
	{
		struct OptitrackClientOrigin_eventGetLatestSkeletonState_Parms
		{
			int32 SkelId;
			FOptitrackSkeletonState OutSkelState;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutSkelState;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SkelId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackClientOrigin_eventGetLatestSkeletonState_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackClientOrigin_eventGetLatestSkeletonState_Parms), &Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::NewProp_OutSkelState = { UE4CodeGen_Private::EPropertyClass::Struct, "OutSkelState", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventGetLatestSkeletonState_Parms, OutSkelState), Z_Construct_UScriptStruct_FOptitrackSkeletonState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::NewProp_SkelId = { UE4CodeGen_Private::EPropertyClass::Int, "SkelId", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventGetLatestSkeletonState_Parms, SkelId), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::NewProp_OutSkelState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::NewProp_SkelId,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack|Advanced" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "TODO\n\n@param OutSkelState\n@return True if any skeleton state was available for the specified ID,\n        otherwise false." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "GetLatestSkeletonState", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04420401, sizeof(OptitrackClientOrigin_eventGetLatestSkeletonState_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType_Statics
	{
		struct OptitrackClientOrigin_eventGetTimestampType_Parms
		{
			EOptitrackTimestampType ReturnValue;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Enum, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(OptitrackClientOrigin_eventGetTimestampType_Parms, ReturnValue), Z_Construct_UEnum_OptitrackNatnet_EOptitrackTimestampType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType_Statics::NewProp_ReturnValue_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType_Statics::NewProp_ReturnValue_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Describes the semantics of @FOptitrackRigidBodyState::TimestampPlatformSeconds values." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "GetTimestampType", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x54020401, sizeof(OptitrackClientOrigin_eventGetTimestampType_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient_Statics
	{
		struct OptitrackClientOrigin_eventInitializeClient_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackClientOrigin_eventInitializeClient_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackClientOrigin_eventInitializeClient_Parms), &Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Attempt to initialize NatNet and connect to the server.\nInvalid to call if @IsClientInitialized() returns true.\n@return True on success, false on failure." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "InitializeClient", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(OptitrackClientOrigin_eventInitializeClient_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected_Statics
	{
		struct OptitrackClientOrigin_eventIsClientConnected_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackClientOrigin_eventIsClientConnected_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackClientOrigin_eventIsClientConnected_Parms), &Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Returns true if the Client has successfully connected to Motive" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "IsClientConnected", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x54020401, sizeof(OptitrackClientOrigin_eventIsClientConnected_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized_Statics
	{
		struct OptitrackClientOrigin_eventIsClientInitialized_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackClientOrigin_eventIsClientInitialized_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackClientOrigin_eventIsClientInitialized_Parms), &Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Returns true if the last call to InitializeClient succeeded,\nand ShutdownClient has not been called subsequently." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "IsClientInitialized", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x54020401, sizeof(OptitrackClientOrigin_eventIsClientInitialized_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient_Statics
	{
		struct OptitrackClientOrigin_eventShutdownClient_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OptitrackClientOrigin_eventShutdownClient_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(OptitrackClientOrigin_eventShutdownClient_Parms), &Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient_Statics::Function_MetaDataParams[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Attempt to shut down any existing NatNet client.\nInvalid to call if @IsClientInitialized() returns false.\n@return True on success, false on failure." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AOptitrackClientOrigin, "ShutdownClient", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(OptitrackClientOrigin_eventShutdownClient_Parms), Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AOptitrackClientOrigin_NoRegister()
	{
		return AOptitrackClientOrigin::StaticClass();
	}
	struct Z_Construct_UClass_AOptitrackClientOrigin_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LatestMarkerStates_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_LatestMarkerStates;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LatestMarkerStates_Key_KeyProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LatestMarkerStates_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LatestSkeletonStates_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_LatestSkeletonStates;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LatestSkeletonStates_Key_KeyProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LatestSkeletonStates_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LatestRigidBodyStates_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_LatestRigidBodyStates;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LatestRigidBodyStates_Key_KeyProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LatestRigidBodyStates_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletonDefinitions_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_SkeletonDefinitions;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SkeletonDefinitions_Key_KeyProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SkeletonDefinitions_ValueProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HmdOrientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_HmdOrientation;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_HmdOrientation_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawMarkers_MetaData[];
#endif
		static void NewProp_bDrawMarkers_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawMarkers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawDebugSkeletons_MetaData[];
#endif
		static void NewProp_bDrawDebugSkeletons_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawDebugSkeletons;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoInitialize_MetaData[];
#endif
		static void NewProp_bAutoInitialize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoInitialize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HmdRigidBodyId_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HmdRigidBodyId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneNamingConvention_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BoneNamingConvention;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BoneNamingConvention_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectionType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ConnectionType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ConnectionType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ClientAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ServerAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ServerAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoConnect_MetaData[];
#endif
		static void NewProp_bAutoConnect_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoConnect;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AOptitrackClientOrigin_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnet,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AOptitrackClientOrigin_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_FindDefaultClientOrigin, "FindDefaultClientOrigin" }, // 71760222
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_FindHmdClientOrigin, "FindHmdClientOrigin" }, // 2969556644
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_FindSkeletonDefinition, "FindSkeletonDefinition" }, // 3430434033
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_GetDataDescription, "GetDataDescription" }, // 1128907910
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerState, "GetLatestMarkerState" }, // 518977440
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestMarkerStateUntransformed, "GetLatestMarkerStateUntransformed" }, // 2524307236
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyState, "GetLatestRigidBodyState" }, // 528651968
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestRigidBodyStateUntransformed, "GetLatestRigidBodyStateUntransformed" }, // 646053643
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_GetLatestSkeletonState, "GetLatestSkeletonState" }, // 4190772772
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_GetTimestampType, "GetTimestampType" }, // 3281303435
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_InitializeClient, "InitializeClient" }, // 2652688501
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_IsClientConnected, "IsClientConnected" }, // 1368206258
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_IsClientInitialized, "IsClientInitialized" }, // 2138651584
		{ &Z_Construct_UFunction_AOptitrackClientOrigin_ShutdownClient, "ShutdownClient" }, // 1956109851
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "OptitrackClientOrigin.h" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestMarkerStates_MetaData[] = {
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Key is marker ID, value is latest position of marker available." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestMarkerStates = { UE4CodeGen_Private::EPropertyClass::Map, "LatestMarkerStates", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000000, 1, nullptr, STRUCT_OFFSET(AOptitrackClientOrigin, LatestMarkerStates), METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestMarkerStates_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestMarkerStates_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestMarkerStates_Key_KeyProp = { UE4CodeGen_Private::EPropertyClass::Int, "LatestMarkerStates_Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestMarkerStates_ValueProp = { UE4CodeGen_Private::EPropertyClass::Struct, "LatestMarkerStates", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 1, Z_Construct_UScriptStruct_FOptitrackMarkerState, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestSkeletonStates_MetaData[] = {
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Key is skeleton ID, value is latest set of bone poses available for that skeleton." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestSkeletonStates = { UE4CodeGen_Private::EPropertyClass::Map, "LatestSkeletonStates", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000000, 1, nullptr, STRUCT_OFFSET(AOptitrackClientOrigin, LatestSkeletonStates), METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestSkeletonStates_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestSkeletonStates_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestSkeletonStates_Key_KeyProp = { UE4CodeGen_Private::EPropertyClass::Int, "LatestSkeletonStates_Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestSkeletonStates_ValueProp = { UE4CodeGen_Private::EPropertyClass::Struct, "LatestSkeletonStates", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 1, Z_Construct_UScriptStruct_FOptitrackSkeletonState, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestRigidBodyStates_MetaData[] = {
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Key is rigid body ID, value is latest pose available for that rigid body." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestRigidBodyStates = { UE4CodeGen_Private::EPropertyClass::Map, "LatestRigidBodyStates", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000000, 1, nullptr, STRUCT_OFFSET(AOptitrackClientOrigin, LatestRigidBodyStates), METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestRigidBodyStates_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestRigidBodyStates_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestRigidBodyStates_Key_KeyProp = { UE4CodeGen_Private::EPropertyClass::Int, "LatestRigidBodyStates_Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestRigidBodyStates_ValueProp = { UE4CodeGen_Private::EPropertyClass::Struct, "LatestRigidBodyStates", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 1, Z_Construct_UScriptStruct_FOptitrackRigidBodyState, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_SkeletonDefinitions_MetaData[] = {
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_SkeletonDefinitions = { UE4CodeGen_Private::EPropertyClass::Map, "SkeletonDefinitions", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000000, 1, nullptr, STRUCT_OFFSET(AOptitrackClientOrigin, SkeletonDefinitions), METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_SkeletonDefinitions_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_SkeletonDefinitions_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_SkeletonDefinitions_Key_KeyProp = { UE4CodeGen_Private::EPropertyClass::Int, "SkeletonDefinitions_Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_SkeletonDefinitions_ValueProp = { UE4CodeGen_Private::EPropertyClass::Struct, "SkeletonDefinitions", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 1, Z_Construct_UScriptStruct_FOptitrackSkeletonDefinition, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_HmdOrientation_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "Orientation of HMD in tracking volume" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_HmdOrientation = { UE4CodeGen_Private::EPropertyClass::Enum, "HmdOrientation", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AOptitrackClientOrigin, HmdOrientation), Z_Construct_UEnum_OptitrackNatnet_EOptitrackForwardAxisType, METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_HmdOrientation_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_HmdOrientation_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_HmdOrientation_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawMarkers_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "If true, draw Motive's labeled marker locations (blue)" },
	};
#endif
	void Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawMarkers_SetBit(void* Obj)
	{
		((AOptitrackClientOrigin*)Obj)->bDrawMarkers = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawMarkers = { UE4CodeGen_Private::EPropertyClass::Bool, "bDrawMarkers", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AOptitrackClientOrigin), &Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawMarkers_SetBit, METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawMarkers_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawMarkers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawDebugSkeletons_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "If true, draw Motive and UE bone locations (white and green respectively)" },
	};
#endif
	void Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawDebugSkeletons_SetBit(void* Obj)
	{
		((AOptitrackClientOrigin*)Obj)->bDrawDebugSkeletons = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawDebugSkeletons = { UE4CodeGen_Private::EPropertyClass::Bool, "bDrawDebugSkeletons", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000005, 1, nullptr, sizeof(uint8), UE4CodeGen_Private::ENativeBool::NotNative, sizeof(AOptitrackClientOrigin), &Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawDebugSkeletons_SetBit, METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawDebugSkeletons_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawDebugSkeletons_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoInitialize_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "If true, call InitializeClient and connect automatically during PreInitializeComponents." },
	};
#endif
	void Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoInitialize_SetBit(void* Obj)
	{
		((AOptitrackClientOrigin*)Obj)->bAutoInitialize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoInitialize = { UE4CodeGen_Private::EPropertyClass::Bool, "bAutoInitialize", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AOptitrackClientOrigin), &Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoInitialize_SetBit, METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoInitialize_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoInitialize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_HmdRigidBodyId_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "If non-zero, the rigid body with the specified ID is used to provide input to compatible HMD implementations." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_HmdRigidBodyId = { UE4CodeGen_Private::EPropertyClass::Int, "HmdRigidBodyId", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AOptitrackClientOrigin, HmdRigidBodyId), METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_HmdRigidBodyId_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_HmdRigidBodyId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_BoneNamingConvention_MetaData[] = {
		{ "Category", "Optitrack Connection Settings" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_BoneNamingConvention = { UE4CodeGen_Private::EPropertyClass::Enum, "BoneNamingConvention", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AOptitrackClientOrigin, BoneNamingConvention), Z_Construct_UEnum_OptitrackNatnet_EOptitrackBoneNamingConvention, METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_BoneNamingConvention_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_BoneNamingConvention_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_BoneNamingConvention_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ConnectionType_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "EditCondition", "!bAutoConnect" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "The NatNet client type to instantiate. Must match the setting of the NatNet server." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ConnectionType = { UE4CodeGen_Private::EPropertyClass::Enum, "ConnectionType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AOptitrackClientOrigin, ConnectionType), Z_Construct_UEnum_OptitrackNatnet_EOptitrackClientConnectionType, METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ConnectionType_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ConnectionType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ConnectionType_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ClientAddress_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "EditCondition", "!bAutoConnect" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "The address of the local network interface to use." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ClientAddress = { UE4CodeGen_Private::EPropertyClass::Str, "ClientAddress", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AOptitrackClientOrigin, ClientAddress), METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ClientAddress_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ClientAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ServerAddress_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "EditCondition", "!bAutoConnect" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "The network address of the NatNet server to connect to." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ServerAddress = { UE4CodeGen_Private::EPropertyClass::Str, "ServerAddress", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AOptitrackClientOrigin, ServerAddress), METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ServerAddress_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ServerAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoConnect_MetaData[] = {
		{ "Category", "Optitrack" },
		{ "ModuleRelativePath", "Classes/OptitrackClientOrigin.h" },
		{ "ToolTip", "If true, the NatNet client will search for and connect to Motive automatically ." },
	};
#endif
	void Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoConnect_SetBit(void* Obj)
	{
		((AOptitrackClientOrigin*)Obj)->bAutoConnect = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoConnect = { UE4CodeGen_Private::EPropertyClass::Bool, "bAutoConnect", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AOptitrackClientOrigin), &Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoConnect_SetBit, METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoConnect_MetaData, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoConnect_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AOptitrackClientOrigin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestMarkerStates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestMarkerStates_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestMarkerStates_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestSkeletonStates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestSkeletonStates_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestSkeletonStates_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestRigidBodyStates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestRigidBodyStates_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_LatestRigidBodyStates_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_SkeletonDefinitions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_SkeletonDefinitions_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_SkeletonDefinitions_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_HmdOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_HmdOrientation_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawMarkers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bDrawDebugSkeletons,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoInitialize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_HmdRigidBodyId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_BoneNamingConvention,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_BoneNamingConvention_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ConnectionType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ConnectionType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ClientAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_ServerAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOptitrackClientOrigin_Statics::NewProp_bAutoConnect,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AOptitrackClientOrigin_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AOptitrackClientOrigin>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AOptitrackClientOrigin_Statics::ClassParams = {
		&AOptitrackClientOrigin::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_AOptitrackClientOrigin_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AOptitrackClientOrigin_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AOptitrackClientOrigin_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AOptitrackClientOrigin()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AOptitrackClientOrigin_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AOptitrackClientOrigin, 1042585305);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AOptitrackClientOrigin(Z_Construct_UClass_AOptitrackClientOrigin, &AOptitrackClientOrigin::StaticClass, TEXT("/Script/OptitrackNatnet"), TEXT("AOptitrackClientOrigin"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AOptitrackClientOrigin);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
