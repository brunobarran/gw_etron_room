// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UWorld;
class AOptitrackClientOrigin;
enum class EOptitrackTimestampType : uint8;
struct FOptitrackMarkerState;
struct FOptitrackRigidBodyState;
struct FOptitrackSkeletonDefinition;
struct FOptitrackSkeletonState;
#ifdef OPTITRACKNATNET_OptitrackClientOrigin_generated_h
#error "OptitrackClientOrigin.generated.h already included, missing '#pragma once' in OptitrackClientOrigin.h"
#endif
#define OPTITRACKNATNET_OptitrackClientOrigin_generated_h

#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_82_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOptitrackMarkerState_Statics; \
	OPTITRACKNATNET_API static class UScriptStruct* StaticStruct();


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_56_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOptitrackRigidBodyState_Statics; \
	OPTITRACKNATNET_API static class UScriptStruct* StaticStruct();


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execShutdownClient) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ShutdownClient(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetDataDescription) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetDataDescription(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitializeClient) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->InitializeClient(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsClientConnected) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsClientConnected(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsClientInitialized) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsClientInitialized(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFindHmdClientOrigin) \
	{ \
		P_GET_OBJECT(UWorld,Z_Param_World); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(AOptitrackClientOrigin**)Z_Param__Result=AOptitrackClientOrigin::FindHmdClientOrigin(Z_Param_World); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFindDefaultClientOrigin) \
	{ \
		P_GET_OBJECT(UWorld,Z_Param_World); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(AOptitrackClientOrigin**)Z_Param__Result=AOptitrackClientOrigin::FindDefaultClientOrigin(Z_Param_World); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTimestampType) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EOptitrackTimestampType*)Z_Param__Result=P_THIS->GetTimestampType(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLatestMarkerStateUntransformed) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_markerId); \
		P_GET_STRUCT_REF(FOptitrackMarkerState,Z_Param_Out_OutMarkerState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetLatestMarkerStateUntransformed(Z_Param_markerId,Z_Param_Out_OutMarkerState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLatestRigidBodyStateUntransformed) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_RbId); \
		P_GET_STRUCT_REF(FOptitrackRigidBodyState,Z_Param_Out_OutRbState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetLatestRigidBodyStateUntransformed(Z_Param_RbId,Z_Param_Out_OutRbState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFindSkeletonDefinition) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_SkeletonName); \
		P_GET_STRUCT_REF(FOptitrackSkeletonDefinition,Z_Param_Out_OutSkelDef); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->FindSkeletonDefinition(Z_Param_Out_SkeletonName,Z_Param_Out_OutSkelDef); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLatestSkeletonState) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_SkelId); \
		P_GET_STRUCT_REF(FOptitrackSkeletonState,Z_Param_Out_OutSkelState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetLatestSkeletonState(Z_Param_SkelId,Z_Param_Out_OutSkelState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLatestMarkerState) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_markerId); \
		P_GET_STRUCT_REF(FOptitrackMarkerState,Z_Param_Out_OutMarkerState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetLatestMarkerState(Z_Param_markerId,Z_Param_Out_OutMarkerState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLatestRigidBodyState) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_RbId); \
		P_GET_STRUCT_REF(FOptitrackRigidBodyState,Z_Param_Out_OutRbState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetLatestRigidBodyState(Z_Param_RbId,Z_Param_Out_OutRbState); \
		P_NATIVE_END; \
	}


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execShutdownClient) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ShutdownClient(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetDataDescription) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetDataDescription(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInitializeClient) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->InitializeClient(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsClientConnected) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsClientConnected(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsClientInitialized) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->IsClientInitialized(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFindHmdClientOrigin) \
	{ \
		P_GET_OBJECT(UWorld,Z_Param_World); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(AOptitrackClientOrigin**)Z_Param__Result=AOptitrackClientOrigin::FindHmdClientOrigin(Z_Param_World); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFindDefaultClientOrigin) \
	{ \
		P_GET_OBJECT(UWorld,Z_Param_World); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(AOptitrackClientOrigin**)Z_Param__Result=AOptitrackClientOrigin::FindDefaultClientOrigin(Z_Param_World); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTimestampType) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EOptitrackTimestampType*)Z_Param__Result=P_THIS->GetTimestampType(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLatestMarkerStateUntransformed) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_markerId); \
		P_GET_STRUCT_REF(FOptitrackMarkerState,Z_Param_Out_OutMarkerState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetLatestMarkerStateUntransformed(Z_Param_markerId,Z_Param_Out_OutMarkerState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLatestRigidBodyStateUntransformed) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_RbId); \
		P_GET_STRUCT_REF(FOptitrackRigidBodyState,Z_Param_Out_OutRbState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetLatestRigidBodyStateUntransformed(Z_Param_RbId,Z_Param_Out_OutRbState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execFindSkeletonDefinition) \
	{ \
		P_GET_PROPERTY_REF(UNameProperty,Z_Param_Out_SkeletonName); \
		P_GET_STRUCT_REF(FOptitrackSkeletonDefinition,Z_Param_Out_OutSkelDef); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->FindSkeletonDefinition(Z_Param_Out_SkeletonName,Z_Param_Out_OutSkelDef); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLatestSkeletonState) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_SkelId); \
		P_GET_STRUCT_REF(FOptitrackSkeletonState,Z_Param_Out_OutSkelState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetLatestSkeletonState(Z_Param_SkelId,Z_Param_Out_OutSkelState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLatestMarkerState) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_markerId); \
		P_GET_STRUCT_REF(FOptitrackMarkerState,Z_Param_Out_OutMarkerState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetLatestMarkerState(Z_Param_markerId,Z_Param_Out_OutMarkerState); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLatestRigidBodyState) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_RbId); \
		P_GET_STRUCT_REF(FOptitrackRigidBodyState,Z_Param_Out_OutRbState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->GetLatestRigidBodyState(Z_Param_RbId,Z_Param_Out_OutRbState); \
		P_NATIVE_END; \
	}


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOptitrackClientOrigin(); \
	friend struct Z_Construct_UClass_AOptitrackClientOrigin_Statics; \
public: \
	DECLARE_CLASS(AOptitrackClientOrigin, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OptitrackNatnet"), NO_API) \
	DECLARE_SERIALIZER(AOptitrackClientOrigin)


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_INCLASS \
private: \
	static void StaticRegisterNativesAOptitrackClientOrigin(); \
	friend struct Z_Construct_UClass_AOptitrackClientOrigin_Statics; \
public: \
	DECLARE_CLASS(AOptitrackClientOrigin, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OptitrackNatnet"), NO_API) \
	DECLARE_SERIALIZER(AOptitrackClientOrigin)


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOptitrackClientOrigin(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOptitrackClientOrigin) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOptitrackClientOrigin); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOptitrackClientOrigin); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOptitrackClientOrigin(AOptitrackClientOrigin&&); \
	NO_API AOptitrackClientOrigin(const AOptitrackClientOrigin&); \
public:


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOptitrackClientOrigin(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOptitrackClientOrigin(AOptitrackClientOrigin&&); \
	NO_API AOptitrackClientOrigin(const AOptitrackClientOrigin&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOptitrackClientOrigin); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOptitrackClientOrigin); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOptitrackClientOrigin)


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SkeletonDefinitions() { return STRUCT_OFFSET(AOptitrackClientOrigin, SkeletonDefinitions); } \
	FORCEINLINE static uint32 __PPO__LatestRigidBodyStates() { return STRUCT_OFFSET(AOptitrackClientOrigin, LatestRigidBodyStates); } \
	FORCEINLINE static uint32 __PPO__LatestSkeletonStates() { return STRUCT_OFFSET(AOptitrackClientOrigin, LatestSkeletonStates); } \
	FORCEINLINE static uint32 __PPO__LatestMarkerStates() { return STRUCT_OFFSET(AOptitrackClientOrigin, LatestMarkerStates); }


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_119_PROLOG
#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_PRIVATE_PROPERTY_OFFSET \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_RPC_WRAPPERS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_INCLASS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_PRIVATE_PROPERTY_OFFSET \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_RPC_WRAPPERS_NO_PURE_DECLS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_INCLASS_NO_PURE_DECLS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h_122_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OptitrackClientOrigin."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackClientOrigin_h


#define FOREACH_ENUM_EOPTITRACKFORWARDAXISTYPE(op) \
	op(EOptitrackForwardAxisType::Z_Positive) \
	op(EOptitrackForwardAxisType::X_Positive) 
#define FOREACH_ENUM_EOPTITRACKCLIENTCONNECTIONTYPE(op) \
	op(EOptitrackClientConnectionType::Multicast) \
	op(EOptitrackClientConnectionType::Unicast) 
#define FOREACH_ENUM_EOPTITRACKTIMESTAMPTYPE(op) \
	op(EOptitrackTimestampType::Unknown) \
	op(EOptitrackTimestampType::LocalArrivalTime) \
	op(EOptitrackTimestampType::ServerAggregationTime) \
	op(EOptitrackTimestampType::ServerExposureTime) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
