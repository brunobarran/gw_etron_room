// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OptitrackNatnet/Public/AnimNode_OptitrackSkeleton.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAnimNode_OptitrackSkeleton() {}
// Cross Module References
	OPTITRACKNATNET_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton();
	UPackage* Z_Construct_UPackage__Script_OptitrackNatnet();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_Base();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	OPTITRACKNATNET_API UClass* Z_Construct_UClass_AOptitrackClientOrigin_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FBoneReference();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMesh_NoRegister();
	OPTITRACKNATNET_API UScriptStruct* Z_Construct_UScriptStruct_FOptitrackRetargetMapping();
// End Cross Module References
class UScriptStruct* FAnimNode_OptitrackSkeleton::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPTITRACKNATNET_API uint32 Get_Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton, Z_Construct_UPackage__Script_OptitrackNatnet(), TEXT("AnimNode_OptitrackSkeleton"), sizeof(FAnimNode_OptitrackSkeleton), Get_Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAnimNode_OptitrackSkeleton(FAnimNode_OptitrackSkeleton::StaticStruct, TEXT("/Script/OptitrackNatnet"), TEXT("AnimNode_OptitrackSkeleton"), false, nullptr, nullptr);
static struct FScriptStruct_OptitrackNatnet_StaticRegisterNativesFAnimNode_OptitrackSkeleton
{
	FScriptStruct_OptitrackNatnet_StaticRegisterNativesFAnimNode_OptitrackSkeleton()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("AnimNode_OptitrackSkeleton")),new UScriptStruct::TCppStructOps<FAnimNode_OptitrackSkeleton>);
	}
} ScriptStruct_OptitrackNatnet_StaticRegisterNativesFAnimNode_OptitrackSkeleton;
	struct Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RetargetBasePose_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RetargetBasePose;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RetargetBasePose_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThumbBaseRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ThumbBaseRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SkeletonBaseRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SkeletonBaseRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FingerRotationOnly_MetaData[];
#endif
		static void NewProp_FingerRotationOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_FingerRotationOnly;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollBoneBlending_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RollBoneBlending;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollBoneInterpolation_MetaData[];
#endif
		static void NewProp_RollBoneInterpolation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_RollBoneInterpolation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PoseCorrection_MetaData[];
#endif
		static void NewProp_PoseCorrection_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_PoseCorrection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScaleBones_MetaData[];
#endif
		static void NewProp_ScaleBones_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ScaleBones;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StreamingClientOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StreamingClientOrigin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceSkeletonAssetName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SourceSkeletonAssetName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneMappings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BoneMappings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BoneMappings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RetargetBaseMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RetargetBaseMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAnimNode_OptitrackSkeleton>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RetargetBasePose_MetaData[] = {
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RetargetBasePose = { UE4CodeGen_Private::EPropertyClass::Array, "RetargetBasePose", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000000, 1, nullptr, STRUCT_OFFSET(FAnimNode_OptitrackSkeleton, RetargetBasePose), METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RetargetBasePose_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RetargetBasePose_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RetargetBasePose_Inner = { UE4CodeGen_Private::EPropertyClass::Struct, "RetargetBasePose", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_ThumbBaseRotation_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
		{ "PinHiddenByDefault", "" },
		{ "ToolTip", "Base Thumb Rotation." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_ThumbBaseRotation = { UE4CodeGen_Private::EPropertyClass::Float, "ThumbBaseRotation", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(FAnimNode_OptitrackSkeleton, ThumbBaseRotation), METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_ThumbBaseRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_ThumbBaseRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_SkeletonBaseRotation_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
		{ "PinHiddenByDefault", "" },
		{ "ToolTip", "Base Skeleton Rotation. Automatically inferred for full skeletons." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_SkeletonBaseRotation = { UE4CodeGen_Private::EPropertyClass::Struct, "SkeletonBaseRotation", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(FAnimNode_OptitrackSkeleton, SkeletonBaseRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_SkeletonBaseRotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_SkeletonBaseRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_FingerRotationOnly_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
		{ "PinShownByDefault", "" },
		{ "ToolTip", "Enable to ignore finger positions and only use orientation information from Motive when mapping hands." },
	};
#endif
	void Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_FingerRotationOnly_SetBit(void* Obj)
	{
		((FAnimNode_OptitrackSkeleton*)Obj)->FingerRotationOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_FingerRotationOnly = { UE4CodeGen_Private::EPropertyClass::Bool, "FingerRotationOnly", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(FAnimNode_OptitrackSkeleton), &Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_FingerRotationOnly_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_FingerRotationOnly_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_FingerRotationOnly_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RollBoneBlending_MetaData[] = {
		{ "Category", "Settings" },
		{ "EditCondition", "RollBoneInterpolation" },
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
		{ "PinShownByDefault", "" },
		{ "ToolTip", "Blending parameter for rollbone interpolation" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RollBoneBlending = { UE4CodeGen_Private::EPropertyClass::Float, "RollBoneBlending", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(FAnimNode_OptitrackSkeleton, RollBoneBlending), METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RollBoneBlending_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RollBoneBlending_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RollBoneInterpolation_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
		{ "PinShownByDefault", "" },
		{ "ToolTip", "Infer appropriate roll bone orientation. Only available for arms." },
	};
#endif
	void Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RollBoneInterpolation_SetBit(void* Obj)
	{
		((FAnimNode_OptitrackSkeleton*)Obj)->RollBoneInterpolation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RollBoneInterpolation = { UE4CodeGen_Private::EPropertyClass::Bool, "RollBoneInterpolation", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(FAnimNode_OptitrackSkeleton), &Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RollBoneInterpolation_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RollBoneInterpolation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RollBoneInterpolation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_PoseCorrection_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
		{ "PinShownByDefault", "" },
		{ "ToolTip", "Infer rotation of reference bone poses to match Motive T-Pose" },
	};
#endif
	void Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_PoseCorrection_SetBit(void* Obj)
	{
		((FAnimNode_OptitrackSkeleton*)Obj)->PoseCorrection = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_PoseCorrection = { UE4CodeGen_Private::EPropertyClass::Bool, "PoseCorrection", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(FAnimNode_OptitrackSkeleton), &Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_PoseCorrection_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_PoseCorrection_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_PoseCorrection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_ScaleBones_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
		{ "PinShownByDefault", "" },
		{ "ToolTip", "Scale UE bones to match tracked skeleton" },
	};
#endif
	void Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_ScaleBones_SetBit(void* Obj)
	{
		((FAnimNode_OptitrackSkeleton*)Obj)->ScaleBones = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_ScaleBones = { UE4CodeGen_Private::EPropertyClass::Bool, "ScaleBones", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(FAnimNode_OptitrackSkeleton), &Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_ScaleBones_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_ScaleBones_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_ScaleBones_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_StreamingClientOrigin_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
		{ "PinShownByDefault", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_StreamingClientOrigin = { UE4CodeGen_Private::EPropertyClass::Object, "StreamingClientOrigin", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(FAnimNode_OptitrackSkeleton, StreamingClientOrigin), Z_Construct_UClass_AOptitrackClientOrigin_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_StreamingClientOrigin_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_StreamingClientOrigin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_SourceSkeletonAssetName_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
		{ "PinShownByDefault", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_SourceSkeletonAssetName = { UE4CodeGen_Private::EPropertyClass::Name, "SourceSkeletonAssetName", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(FAnimNode_OptitrackSkeleton, SourceSkeletonAssetName), METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_SourceSkeletonAssetName_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_SourceSkeletonAssetName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_BoneMappings_MetaData[] = {
		{ "Category", "Retargeting" },
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_BoneMappings = { UE4CodeGen_Private::EPropertyClass::Array, "BoneMappings", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000041, 1, nullptr, STRUCT_OFFSET(FAnimNode_OptitrackSkeleton, BoneMappings), METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_BoneMappings_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_BoneMappings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_BoneMappings_Inner = { UE4CodeGen_Private::EPropertyClass::Struct, "BoneMappings", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UScriptStruct_FBoneReference, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RetargetBaseMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
		{ "ToolTip", "( EditAnywhere, BlueprintReadWrite, Category=\"Retargeting\" )" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RetargetBaseMesh = { UE4CodeGen_Private::EPropertyClass::Object, "RetargetBaseMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000000, 1, nullptr, STRUCT_OFFSET(FAnimNode_OptitrackSkeleton, RetargetBaseMesh), Z_Construct_UClass_USkeletalMesh_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RetargetBaseMesh_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RetargetBaseMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RetargetBasePose,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RetargetBasePose_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_ThumbBaseRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_SkeletonBaseRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_FingerRotationOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RollBoneBlending,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RollBoneInterpolation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_PoseCorrection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_ScaleBones,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_StreamingClientOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_SourceSkeletonAssetName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_BoneMappings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_BoneMappings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::NewProp_RetargetBaseMesh,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnet,
		Z_Construct_UScriptStruct_FAnimNode_Base,
		&NewStructOps,
		"AnimNode_OptitrackSkeleton",
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		sizeof(FAnimNode_OptitrackSkeleton),
		alignof(FAnimNode_OptitrackSkeleton),
		Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::PropPointers),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_CRC();
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackNatnet();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AnimNode_OptitrackSkeleton"), sizeof(FAnimNode_OptitrackSkeleton), Get_Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_CRC(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton_CRC() { return 2735851502U; }
class UScriptStruct* FOptitrackRetargetMapping::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPTITRACKNATNET_API uint32 Get_Z_Construct_UScriptStruct_FOptitrackRetargetMapping_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOptitrackRetargetMapping, Z_Construct_UPackage__Script_OptitrackNatnet(), TEXT("OptitrackRetargetMapping"), sizeof(FOptitrackRetargetMapping), Get_Z_Construct_UScriptStruct_FOptitrackRetargetMapping_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOptitrackRetargetMapping(FOptitrackRetargetMapping::StaticStruct, TEXT("/Script/OptitrackNatnet"), TEXT("OptitrackRetargetMapping"), false, nullptr, nullptr);
static struct FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackRetargetMapping
{
	FScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackRetargetMapping()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("OptitrackRetargetMapping")),new UScriptStruct::TCppStructOps<FOptitrackRetargetMapping>);
	}
} ScriptStruct_OptitrackNatnet_StaticRegisterNativesFOptitrackRetargetMapping;
	struct Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetBone_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TargetBone;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOptitrackRetargetMapping>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::NewProp_TargetBone_MetaData[] = {
		{ "Category", "Retargeting" },
		{ "ModuleRelativePath", "Public/AnimNode_OptitrackSkeleton.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::NewProp_TargetBone = { UE4CodeGen_Private::EPropertyClass::Struct, "TargetBone", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(FOptitrackRetargetMapping, TargetBone), Z_Construct_UScriptStruct_FBoneReference, METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::NewProp_TargetBone_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::NewProp_TargetBone_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::NewProp_TargetBone,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnet,
		nullptr,
		&NewStructOps,
		"OptitrackRetargetMapping",
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		sizeof(FOptitrackRetargetMapping),
		alignof(FOptitrackRetargetMapping),
		Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::PropPointers),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOptitrackRetargetMapping()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOptitrackRetargetMapping_CRC();
		UPackage* Outer = Z_Construct_UPackage__Script_OptitrackNatnet();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OptitrackRetargetMapping"), sizeof(FOptitrackRetargetMapping), Get_Z_Construct_UScriptStruct_FOptitrackRetargetMapping_CRC(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOptitrackRetargetMapping_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOptitrackRetargetMapping_CRC() { return 4270312139U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
