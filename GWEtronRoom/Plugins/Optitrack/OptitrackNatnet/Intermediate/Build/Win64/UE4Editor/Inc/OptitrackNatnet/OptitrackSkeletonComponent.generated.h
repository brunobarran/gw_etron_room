// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPTITRACKNATNET_OptitrackSkeletonComponent_generated_h
#error "OptitrackSkeletonComponent.generated.h already included, missing '#pragma once' in OptitrackSkeletonComponent.h"
#endif
#define OPTITRACKNATNET_OptitrackSkeletonComponent_generated_h

#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_RPC_WRAPPERS
#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOptitrackSkeletonComponent(); \
	friend struct Z_Construct_UClass_UOptitrackSkeletonComponent_Statics; \
public: \
	DECLARE_CLASS(UOptitrackSkeletonComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OptitrackNatnet"), NO_API) \
	DECLARE_SERIALIZER(UOptitrackSkeletonComponent)


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUOptitrackSkeletonComponent(); \
	friend struct Z_Construct_UClass_UOptitrackSkeletonComponent_Statics; \
public: \
	DECLARE_CLASS(UOptitrackSkeletonComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OptitrackNatnet"), NO_API) \
	DECLARE_SERIALIZER(UOptitrackSkeletonComponent)


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOptitrackSkeletonComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOptitrackSkeletonComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOptitrackSkeletonComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOptitrackSkeletonComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOptitrackSkeletonComponent(UOptitrackSkeletonComponent&&); \
	NO_API UOptitrackSkeletonComponent(const UOptitrackSkeletonComponent&); \
public:


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOptitrackSkeletonComponent(UOptitrackSkeletonComponent&&); \
	NO_API UOptitrackSkeletonComponent(const UOptitrackSkeletonComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOptitrackSkeletonComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOptitrackSkeletonComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOptitrackSkeletonComponent)


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OptitrackAnimInstance() { return STRUCT_OFFSET(UOptitrackSkeletonComponent, OptitrackAnimInstance); }


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_28_PROLOG
#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_PRIVATE_PROPERTY_OFFSET \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_RPC_WRAPPERS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_INCLASS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_PRIVATE_PROPERTY_OFFSET \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_INCLASS_NO_PURE_DECLS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h_31_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackSkeletonComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
