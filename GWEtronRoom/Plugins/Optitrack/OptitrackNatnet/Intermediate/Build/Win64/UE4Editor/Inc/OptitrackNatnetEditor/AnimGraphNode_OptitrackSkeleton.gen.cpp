// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OptitrackNatnetEditor/Public/AnimGraphNode_OptitrackSkeleton.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAnimGraphNode_OptitrackSkeleton() {}
// Cross Module References
	OPTITRACKNATNETEDITOR_API UClass* Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_NoRegister();
	OPTITRACKNATNETEDITOR_API UClass* Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton();
	ANIMGRAPH_API UClass* Z_Construct_UClass_UAnimGraphNode_Base();
	UPackage* Z_Construct_UPackage__Script_OptitrackNatnetEditor();
	OPTITRACKNATNET_API UScriptStruct* Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton();
// End Cross Module References
	void UAnimGraphNode_OptitrackSkeleton::StaticRegisterNativesUAnimGraphNode_OptitrackSkeleton()
	{
	}
	UClass* Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_NoRegister()
	{
		return UAnimGraphNode_OptitrackSkeleton::StaticClass();
	}
	struct Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Node_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Node;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnimGraphNode_Base,
		(UObject* (*)())Z_Construct_UPackage__Script_OptitrackNatnetEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "AnimGraphNode_OptitrackSkeleton.h" },
		{ "ModuleRelativePath", "Public/AnimGraphNode_OptitrackSkeleton.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::NewProp_Node_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/AnimGraphNode_OptitrackSkeleton.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::NewProp_Node = { UE4CodeGen_Private::EPropertyClass::Struct, "Node", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000001, 1, nullptr, STRUCT_OFFSET(UAnimGraphNode_OptitrackSkeleton, Node), Z_Construct_UScriptStruct_FAnimNode_OptitrackSkeleton, METADATA_PARAMS(Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::NewProp_Node_MetaData, ARRAY_COUNT(Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::NewProp_Node_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::NewProp_Node,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAnimGraphNode_OptitrackSkeleton>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::ClassParams = {
		&UAnimGraphNode_OptitrackSkeleton::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x001000A0u,
		nullptr, 0,
		Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAnimGraphNode_OptitrackSkeleton, 601527559);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAnimGraphNode_OptitrackSkeleton(Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton, &UAnimGraphNode_OptitrackSkeleton::StaticClass, TEXT("/Script/OptitrackNatnetEditor"), TEXT("UAnimGraphNode_OptitrackSkeleton"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAnimGraphNode_OptitrackSkeleton);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
