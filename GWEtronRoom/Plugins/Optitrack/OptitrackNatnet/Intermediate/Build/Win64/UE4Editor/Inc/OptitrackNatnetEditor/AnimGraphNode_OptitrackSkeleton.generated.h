// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPTITRACKNATNETEDITOR_AnimGraphNode_OptitrackSkeleton_generated_h
#error "AnimGraphNode_OptitrackSkeleton.generated.h already included, missing '#pragma once' in AnimGraphNode_OptitrackSkeleton.h"
#endif
#define OPTITRACKNATNETEDITOR_AnimGraphNode_OptitrackSkeleton_generated_h

#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_RPC_WRAPPERS
#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAnimGraphNode_OptitrackSkeleton(); \
	friend struct Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics; \
public: \
	DECLARE_CLASS(UAnimGraphNode_OptitrackSkeleton, UAnimGraphNode_Base, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OptitrackNatnetEditor"), NO_API) \
	DECLARE_SERIALIZER(UAnimGraphNode_OptitrackSkeleton)


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUAnimGraphNode_OptitrackSkeleton(); \
	friend struct Z_Construct_UClass_UAnimGraphNode_OptitrackSkeleton_Statics; \
public: \
	DECLARE_CLASS(UAnimGraphNode_OptitrackSkeleton, UAnimGraphNode_Base, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OptitrackNatnetEditor"), NO_API) \
	DECLARE_SERIALIZER(UAnimGraphNode_OptitrackSkeleton)


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnimGraphNode_OptitrackSkeleton(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimGraphNode_OptitrackSkeleton) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnimGraphNode_OptitrackSkeleton); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimGraphNode_OptitrackSkeleton); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnimGraphNode_OptitrackSkeleton(UAnimGraphNode_OptitrackSkeleton&&); \
	NO_API UAnimGraphNode_OptitrackSkeleton(const UAnimGraphNode_OptitrackSkeleton&); \
public:


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnimGraphNode_OptitrackSkeleton(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnimGraphNode_OptitrackSkeleton(UAnimGraphNode_OptitrackSkeleton&&); \
	NO_API UAnimGraphNode_OptitrackSkeleton(const UAnimGraphNode_OptitrackSkeleton&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnimGraphNode_OptitrackSkeleton); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimGraphNode_OptitrackSkeleton); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimGraphNode_OptitrackSkeleton)


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_PRIVATE_PROPERTY_OFFSET
#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_24_PROLOG
#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_PRIVATE_PROPERTY_OFFSET \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_RPC_WRAPPERS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_INCLASS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_PRIVATE_PROPERTY_OFFSET \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_INCLASS_NO_PURE_DECLS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h_27_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AnimGraphNode_OptitrackSkeleton."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnetEditor_Public_AnimGraphNode_OptitrackSkeleton_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
