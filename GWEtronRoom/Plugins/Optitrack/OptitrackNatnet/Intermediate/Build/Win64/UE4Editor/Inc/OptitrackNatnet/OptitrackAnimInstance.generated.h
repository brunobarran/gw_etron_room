// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPTITRACKNATNET_OptitrackAnimInstance_generated_h
#error "OptitrackAnimInstance.generated.h already included, missing '#pragma once' in OptitrackAnimInstance.h"
#endif
#define OPTITRACKNATNET_OptitrackAnimInstance_generated_h

#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_RPC_WRAPPERS
#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOptitrackAnimInstance(); \
	friend struct Z_Construct_UClass_UOptitrackAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UOptitrackAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/OptitrackNatnet"), NO_API) \
	DECLARE_SERIALIZER(UOptitrackAnimInstance)


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUOptitrackAnimInstance(); \
	friend struct Z_Construct_UClass_UOptitrackAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UOptitrackAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/OptitrackNatnet"), NO_API) \
	DECLARE_SERIALIZER(UOptitrackAnimInstance)


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOptitrackAnimInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOptitrackAnimInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOptitrackAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOptitrackAnimInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOptitrackAnimInstance(UOptitrackAnimInstance&&); \
	NO_API UOptitrackAnimInstance(const UOptitrackAnimInstance&); \
public:


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOptitrackAnimInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOptitrackAnimInstance(UOptitrackAnimInstance&&); \
	NO_API UOptitrackAnimInstance(const UOptitrackAnimInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOptitrackAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOptitrackAnimInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOptitrackAnimInstance)


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_PRIVATE_PROPERTY_OFFSET
#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_23_PROLOG
#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_PRIVATE_PROPERTY_OFFSET \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_RPC_WRAPPERS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_INCLASS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_PRIVATE_PROPERTY_OFFSET \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_INCLASS_NO_PURE_DECLS \
	GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h_26_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OptitrackAnimInstance."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GWEtronRoom_Plugins_Optitrack_OptitrackNatnet_Source_OptitrackNatnet_Classes_OptitrackAnimInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
