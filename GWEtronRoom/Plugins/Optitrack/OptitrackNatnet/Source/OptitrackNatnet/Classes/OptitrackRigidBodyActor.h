/*
Copyright 2018 NaturalPoint

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <GameFramework/Actor.h>

#include "OptitrackRigidBodyActor.generated.h"


class UOptitrackRigidBodyComponent;


UCLASS( ComponentWrapperClass )
class OPTITRACKNATNET_API AOptitrackRigidBodyActor : public AActor
{
	GENERATED_UCLASS_BODY()

	UOptitrackRigidBodyComponent* GetRigidBodyComponent() { return RigidBodyComponent; }

	/**
	* If true, rigid body actor will hide if no associated asset is being streamed from Motive.
	*/
	UPROPERTY( EditAnywhere, AdvancedDisplay, BlueprintReadWrite, Category = "Optitrack" )
	bool bHideOnInvalidDefinition = true;

protected:
	virtual void TickActor( float DeltaTime, ELevelTick TickType, FActorTickFunction& ThisTickFunction ) override;

private:
	UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category="Rigid Body Actor", meta=(AllowPrivateAccess="true") )
	UOptitrackRigidBodyComponent* RigidBodyComponent;
};
