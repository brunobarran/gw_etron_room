/*
Copyright 2018 NaturalPoint

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <GameFramework/Actor.h>

#include "OptitrackSkeletonTypes.h"

#pragma pack(push, 8)
#	include <NatNetCAPI.h>
#	include <NatNetClient.h>
#	include <NatNetTypes.h>
#pragma pack(pop)

#include <thread>

#include "OptitrackClientOrigin.generated.h"


UENUM( BlueprintType )
enum class EOptitrackTimestampType : uint8
{
	/** We don't know what the server supports until after we've received at least one frame. */
	Unknown = 0,

	/** When the NatNet frame callback occurred. Fallback when precise metrics are unavailable. */
	LocalArrivalTime,

	/** Clock synchronized estimate of when the camera data became available on the server. */
	ServerAggregationTime,

	/** Clock synchronized estimate of when the camera exposure occurred. */
	ServerExposureTime,
};


/**
* Represents a pose for a tracked rigid body.
*/
USTRUCT( BlueprintType )
struct FOptitrackRigidBodyState
{
	GENERATED_USTRUCT_BODY()

	/**
	 * Timestamp corresponding to this rigid body pose. Exact semantics can vary depending on
	 * server support; see EOptitrackTimestampType and AOptitrackClientOrigin::GetTimestampType.
	 *
	 * Can be directly compared with values returned by FPlatformTime::Seconds().
	 */
	UPROPERTY()
	double TimestampPlatformSeconds;

	/** Position in tracking space. */
	UPROPERTY( BlueprintReadOnly, Category="Optitrack" )
	FVector Position;

	/** Orientation in tracking space. */
	UPROPERTY( BlueprintReadOnly, Category="Optitrack" )
	FQuat Orientation;
};

/** 
* Represents a tracked marker.
*/
USTRUCT(BlueprintType)
struct FOptitrackMarkerState
{
    GENERATED_USTRUCT_BODY()

    /**
    * Timestamp corresponding to this marker. Exact semantics can vary depending on
    * server support; see EOptitrackTimestampType and AOptitrackClientOrigin::GetTimestampType.
    *
    * Can be directly compared with values returned by FPlatformTime::Seconds().
    */
    UPROPERTY()
    double TimestampPlatformSeconds;

    /** Position in tracking space. */
    UPROPERTY(BlueprintReadOnly, Category = "Optitrack")
    FVector Position;

    /** Marker Id in Motive. */
    UPROPERTY(BlueprintReadOnly, Category = "Optitrack")
    int32 Id;
};

UENUM( BlueprintType )
enum class EOptitrackClientConnectionType : uint8
{
	Multicast = 0,
	Unicast,
};

UENUM( BlueprintType )
enum class EOptitrackForwardAxisType : uint8
{
    Z_Positive = 0 UMETA(DisplayName = "+Z Forward"),
    X_Positive     UMETA(DisplayName = "+X Forward"),
};

/**
*
*/
UCLASS()
class OPTITRACKNATNET_API AOptitrackClientOrigin : public AActor
{
	GENERATED_UCLASS_BODY()

    /** If true, the NatNet client will search for and connect to Motive automatically . */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Optitrack")
    bool bAutoConnect = true;

	/** The network address of the NatNet server to connect to. */
	UPROPERTY( EditAnywhere, BlueprintReadWrite, meta=(EditCondition="!bAutoConnect"), Category="Optitrack" )
	FString ServerAddress;

	/** The address of the local network interface to use. */
	UPROPERTY( EditAnywhere, BlueprintReadWrite, meta=(EditCondition="!bAutoConnect"), Category="Optitrack" )
	FString ClientAddress;

	/** The NatNet client type to instantiate. Must match the setting of the NatNet server. */
	UPROPERTY( EditAnywhere, BlueprintReadWrite, meta=(EditCondition="!bAutoConnect"), Category="Optitrack" )
	EOptitrackClientConnectionType ConnectionType;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category="Optitrack Connection Settings" )
	EOptitrackBoneNamingConvention BoneNamingConvention;

	/** If non-zero, the rigid body with the specified ID is used to provide input to compatible HMD implementations. */
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category="Optitrack" )
	int32 HmdRigidBodyId;

    /** If true, call InitializeClient and connect automatically during PreInitializeComponents. */
    UPROPERTY( EditAnywhere, BlueprintReadWrite, AdvancedDisplay, Category="Optitrack" )
    bool bAutoInitialize = true;

	/** If true, draw Motive and UE bone locations (white and green respectively)*/
	UPROPERTY( EditAnywhere, BlueprintReadWrite, AdvancedDisplay, Category="Optitrack" )
	uint8 bDrawDebugSkeletons : 1;

	/** If true, draw Motive's labeled marker locations (blue)*/
	UPROPERTY( EditAnywhere, BlueprintReadWrite, AdvancedDisplay, Category = "Optitrack" )
	bool bDrawMarkers = false;

    /** Orientation of HMD in tracking volume */
    UPROPERTY( EditAnywhere, BlueprintReadWrite, Category="Optitrack" )
    EOptitrackForwardAxisType HmdOrientation = EOptitrackForwardAxisType::Z_Positive;

    DECLARE_EVENT_TwoParams( AOptitrackClientOrigin, FOnNatNetFrameReceivedEvent, struct sFrameOfMocapData*, double );
    FOnNatNetFrameReceivedEvent& OnNatNetFrameReceived() { return NatNetFrameReceivedEvent; }

	/**
	 * Retrieves latest available FOptitrackRigidBodyState for the rigid body identified by the
	 * @RbId parameter. Note that this data can be stale if the rigid body in question hasn't
	 * streamed any new data recently.
	 *
	 * The tracking space pose is transformed by the offset/rotation/scale of this actor,
	 * such that the placement of this actor corresponds to the tracking space origin.
	 *
	 * @param OutRbState Receives latest available rigid body state (if any).
	 * @return True if any rigid body state was available for the specified ID,
	 *         otherwise false.
	 */
	UFUNCTION( BlueprintCallable, Category="Optitrack" )
	bool GetLatestRigidBodyState( int32 RbId, FOptitrackRigidBodyState& OutRbState );

    /**
    * Retrieves latest available FOptitrackMarkerState for the marker identified by the
    * @markerId parameter. Note that this data can be stale/non-existent if the marker
    * hasn't streamed any new data recently.
    *
    * The tracking space pose is transformed by the offset/rotation/scale of this actor,
    * such that the placement of this actor corresponds to the tracking space origin.
    *
    * @param OutMarkerState Receives latest available marker state (if any).
    * @return True if any marker state was available for the specified ID,
    *         otherwise false.
    */
    UFUNCTION(BlueprintCallable, Category = "Optitrack")
    bool GetLatestMarkerState(int32 markerId, FOptitrackMarkerState& OutMarkerState);

    /**
    * TODO
    *
    * @param OutSkelState
    * @return True if any skeleton state was available for the specified ID,
    *         otherwise false.
    */
    UFUNCTION( BlueprintCallable, Category="Optitrack|Advanced" )
    bool GetLatestSkeletonState( int32 SkelId, FOptitrackSkeletonState& OutSkelState );

    UFUNCTION( BlueprintCallable, Category="Optitrack|Advanced" )
    bool FindSkeletonDefinition( const FName& SkeletonName, FOptitrackSkeletonDefinition& OutSkelDef );

	/**
	 * Retrieves latest available FOptitrackRigidBodyState for the rigid body identified by the
	 * @RbId parameter. Note that this data can be stale if the rigid body in question hasn't
	 * streamed any new data recently.
	 *
	 * This "Untransformed" version returns the tracking space pose, applying only the global
	 * WorldToMeters scale. It does not take into account the transform of this actor.
	 *
	 * @param OutRbState Receives latest available rigid body state (if any).
	 * @return True if any rigid body state was available for the specified ID,
	 *         otherwise false.
	 */
	UFUNCTION( BlueprintCallable, Category="Optitrack|Advanced" )
	bool GetLatestRigidBodyStateUntransformed( int32 RbId, FOptitrackRigidBodyState& OutRbState );

    /**
    * Retrieves latest available FOptitrackMarkerState for the marker identified by the
    * @markerId parameter. Note that this data can be stale/non-existent if the marker
    * hasn't streamed any new data recently.
    *
    * This "Untransformed" version returns the tracking space pose, applying only the global
    * WorldToMeters scale. It does not take into account the transform of this actor.
    *
    * @param OutMarkerState Receives latest available marker state (if any).
    * @return True if any marker state was available for the specified ID,
    *         otherwise false.
    */
    UFUNCTION(BlueprintCallable, Category = "Optitrack|Advanced")
    bool GetLatestMarkerStateUntransformed(int32 markerId, FOptitrackMarkerState& OutMarkerState);

	/** Describes the semantics of @FOptitrackRigidBodyState::TimestampPlatformSeconds values. */
	UFUNCTION( BlueprintCallable, Category="Optitrack" )
	EOptitrackTimestampType GetTimestampType() const { return TimestampType; }

	/**
	 * Helper for the common case where only a single client origin is present.
	 * Used to provide a default for any UOptitrackRigidBody components which
	 * don't otherwise specify a particular origin to use.
	 * Note: This is potentially expensive and the result should be cached.
	 * @return The first AOptitrackClientOrigin actor found in @World.
	 */
	UFUNCTION( BlueprintCallable, Category="Optitrack" )
	static AOptitrackClientOrigin* FindDefaultClientOrigin( UWorld* World );

	/**
	 * Finds the first AOptitrackClientOrigin specifying an HMD rigid body ID.
	 * Note: This is potentially expensive and the result should be cached.
	 * @return The first suitable AOptitrackClientOrigin actor found in @World.
	 */
	UFUNCTION( BlueprintCallable, Category="Optitrack" )
	static AOptitrackClientOrigin* FindHmdClientOrigin( UWorld* World );

	/**
	 * Returns true if the last call to InitializeClient succeeded,
	 * and ShutdownClient has not been called subsequently.
	 */
	UFUNCTION( BlueprintCallable, Category="Optitrack" )
	bool IsClientInitialized() const { return Client != nullptr; }

    /**
    * Returns true if the Client has successfully connected to Motive
    */
    UFUNCTION(BlueprintCallable, Category = "Optitrack")
    bool IsClientConnected() const { return mClientConnected; }

	/**
	 * Attempt to initialize NatNet and connect to the server.
	 * Invalid to call if @IsClientInitialized() returns true.
	 * @return True on success, false on failure.
	 */
	UFUNCTION( BlueprintCallable, Category="Optitrack" )
	bool InitializeClient();

    /**
    * Request NatNet data description from server.
    * Invalid to call if @IsClientInitialized() returns false.
    * @return True on success, false on failure.
    */
    UFUNCTION(BlueprintCallable, Category = "Optitrack")
    bool GetDataDescription();

	/**
	 * Attempt to shut down any existing NatNet client.
	 * Invalid to call if @IsClientInitialized() returns false.
	 * @return True on success, false on failure.
	 */
	UFUNCTION( BlueprintCallable, Category="Optitrack" )
	bool ShutdownClient();

public:
	void NatNetFrameReceivedCallback_Internal( struct sFrameOfMocapData* NewFrame );
    void ServerDiscoveredCallback(const sNatNetDiscoveredServer* pDiscoveredServer);
    void ValidateAuthenticationToken(const char* challengeToken, char* authToken);

protected:
	//~ Begin AActor Interface
	virtual void PreInitializeComponents() override;
    virtual void BeginPlay();
	virtual void EndPlay( const EEndPlayReason::Type EndPlayReason ) override;
    virtual void Tick(float DeltaSeconds) override;

private:
    bool SetDefaultSkeletonSettings();
    bool NeedUpdateDataDescription;

	class NatNetClient* Client = nullptr;
    void ThreadProc_DetectDisconnection();
    bool ConnectNatNet(const sNatNetClientConnectParams& params);

    /** Auto discovery and disconnection detection parameters*/
    bool mDetectDisconnectionExitFlag = false;
    bool mAsynchronousDiscoveryActiveFlag = false;
    bool mClientConnected = false;
    bool mTriggerReconnectFlag = false;
    NatNetDiscoveryHandle mNatNetDiscoveryHandle = nullptr;
    uint64_t mNatNetTimestampEpoch = 0;
    uint64_t mNatNetLastRecievedTimestamp = UINT64_MAX;
    std::thread mDetectDisconnectionThread;

	/** Controls access to @LatestRigidBodyStates map. */
	FCriticalSection FrameUpdateLock;

	/** Copied from AWorldSettings::WorldToMeters for use in the NatNet callback (which happens on another thread). */
	float CachedWorldToMeters;

    UPROPERTY()
    TMap< int32, FOptitrackSkeletonDefinition > SkeletonDefinitions;

    /** Key is rigid body ID, value is latest pose available for that rigid body. */
    UPROPERTY()
    TMap< int32, FOptitrackRigidBodyState > LatestRigidBodyStates;

    /** Key is skeleton ID, value is latest set of bone poses available for that skeleton. */
    UPROPERTY()
    TMap< int32, FOptitrackSkeletonState > LatestSkeletonStates;

    /** Key is marker ID, value is latest position of marker available. */
    UPROPERTY()
    TMap< int32, FOptitrackMarkerState> LatestMarkerStates;

	EOptitrackTimestampType TimestampType = EOptitrackTimestampType::Unknown;

	FOnNatNetFrameReceivedEvent NatNetFrameReceivedEvent;

#if ! UE_BUILD_SHIPPING
private:
	friend class AOptitrackDebugHUD;

	struct FTimingStats
	{
		static const uint32 kSampleCount = 400;

		int32 CurrentIndex;
		TArray< float > TotalLatencySamples;
		TArray< float > NetworkLatencySamples;
		TArray< float > ArrivalPeriodSamples;

		FTimingStats()
			: CurrentIndex( 0 )
		{
			TotalLatencySamples.AddZeroed( kSampleCount );
			NetworkLatencySamples.AddZeroed( kSampleCount );
			ArrivalPeriodSamples.AddZeroed( kSampleCount );
		}

		void RecordSamples( float totalLatency, float networkLatency, float arrivalPeriod )
		{
			TotalLatencySamples[CurrentIndex] = totalLatency;
			NetworkLatencySamples[CurrentIndex] = networkLatency;
			ArrivalPeriodSamples[CurrentIndex] = arrivalPeriod;

			if ( ++CurrentIndex == kSampleCount )
			{
				CurrentIndex = 0;
			}
		}
	};

	double LastArrivalTimestamp = 0.0;
	FTimingStats TimingStats;
#endif // #if ! UE_BUILD_SHIPPING
};
