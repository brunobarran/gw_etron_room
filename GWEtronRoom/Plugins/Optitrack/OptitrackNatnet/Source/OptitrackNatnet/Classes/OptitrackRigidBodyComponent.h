/*
Copyright 2018 NaturalPoint

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <SceneViewExtension.h>
#include <LateUpdateManager.h>
#include <Components/SceneComponent.h>
#include "OptitrackRigidBodyComponent.generated.h"


/**
  * A component whose position and orientation is driven by the pose of the
  * specified tracked rigid body. Supports render thread "late updates" for
  * reduced tracking latency.
  */
UCLASS( Blueprintable, ClassGroup="OptiTrack", meta=(BlueprintSpawnableComponent) )
class OPTITRACKNATNET_API UOptitrackRigidBodyComponent : public USceneComponent
{
	GENERATED_UCLASS_BODY()

	virtual ~UOptitrackRigidBodyComponent();

	/** ID of the rigid body used to drive this component's transform. */
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category="Optitrack" )
	int32 TrackingId;

	/** If false, render transforms within the rigid body hierarchy will be updated a second time immediately before rendering. */
	UPROPERTY( EditAnywhere, AdvancedDisplay, BlueprintReadWrite, Category="Optitrack" )
	uint32 bDisableLowLatencyUpdate : 1;

	/**
	* If your scene contains multiple client origin objects, you can specify
	* which one to use. If unset, this defaults to the first client origin
	* that's found in the world.
	*/
	UPROPERTY( EditAnywhere, AdvancedDisplay, BlueprintReadWrite, Category="Optitrack" )
	class AOptitrackClientOrigin* TrackingOrigin;

    /**
    * If true, rigid body transform from Motive will be applied with respect to parent transform instead of the client origin.
    */
    UPROPERTY(EditAnywhere, AdvancedDisplay, BlueprintReadWrite, Category = "Optitrack")
    bool RespectParentTransform = false;

protected:
	//~ Begin UActorComponent Interface
	virtual void BeginPlay() override;
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;
	virtual void SendRenderTransform_Concurrent() override;
	//~ End UActorComponent Interface

private:
	/** View extension object that can persist on the render thread without the rigid body component. */
	class FViewExtension : public FSceneViewExtensionBase
	{
	public:
		FViewExtension( const FAutoRegister& AutoRegister, UOptitrackRigidBodyComponent* InRigidBodyComponent )
			: FSceneViewExtensionBase( AutoRegister )
			, RigidBodyComponent( InRigidBodyComponent )
		{}
		virtual ~FViewExtension() {}

		//~ Begin ISceneViewExtension Interface
		virtual void SetupViewFamily( FSceneViewFamily& InViewFamily ) override {}
		virtual void SetupView( FSceneViewFamily& InViewFamily, FSceneView& InView ) override {}
		virtual void BeginRenderViewFamily( FSceneViewFamily& InViewFamily ) override;
		virtual void PreRenderView_RenderThread( FRHICommandListImmediate& RHICmdList, FSceneView& InView ) override {}
		virtual void PreRenderViewFamily_RenderThread( FRHICommandListImmediate& RHICmdList, FSceneViewFamily& InViewFamily ) override;
		virtual int32 GetPriority() const override { return -10; }
		virtual bool IsActiveThisFrame( class FViewport* InViewport ) const override;
		//~ End ISceneViewExtension Interface

	private:
		friend class UOptitrackRigidBodyComponent;

		/** Rigid body component associated with this view extension */
		UOptitrackRigidBodyComponent* RigidBodyComponent;
		FLateUpdateManager LateUpdate;
	};

	TSharedPtr< FViewExtension, ESPMode::ThreadSafe > ViewExtension;
	FTransform RenderThreadRelativeTransform;
	FVector RenderThreadComponentScale;
	double RenderThreadPoseTimestamp;
};
