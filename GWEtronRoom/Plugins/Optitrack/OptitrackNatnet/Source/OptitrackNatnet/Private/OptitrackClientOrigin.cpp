/*
Copyright 2018 NaturalPoint

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "OptitrackClientOrigin.h"
#include "OptitrackNatnet.h"

#include <EngineUtils.h>
#include <DrawDebugHelpers.h>
#include <HAL/RunnableThread.h>

#include <string>


AOptitrackClientOrigin::AOptitrackClientOrigin( const FObjectInitializer& ObjectInitializer )
	: Super( ObjectInitializer )
{
	ServerAddress = "0.0.0.0";
	ClientAddress = "0.0.0.0";
	ConnectionType = EOptitrackClientConnectionType::Multicast;
    PrimaryActorTick.bCanEverTick = true;
	RootComponent = ObjectInitializer.CreateDefaultSubobject<USceneComponent>( this, TEXT( "RootSceneComponent" ) );

#if WITH_EDITORONLY_DATA
	RootComponent->bVisualizeComponent = true;
#endif
}


bool AOptitrackClientOrigin::GetLatestRigidBodyState( int32 RbId, FOptitrackRigidBodyState& OutRbState )
{
	if ( GetLatestRigidBodyStateUntransformed( RbId, OutRbState ) )
	{
		const FTransform optitrackRbTransform( OutRbState.Orientation, OutRbState.Position );

		const FTransform adjustedRbTransform = optitrackRbTransform * GetTransform();
		OutRbState.Position = adjustedRbTransform.GetLocation();
		OutRbState.Orientation = adjustedRbTransform.GetRotation();

		return true;
	}
	else
	{
		return false;
	}
}

bool AOptitrackClientOrigin::GetLatestMarkerState(int32 markerId, FOptitrackMarkerState& OutMarkerState)
{
    if (GetLatestMarkerStateUntransformed(markerId, OutMarkerState))
    {
        const FTransform optitrackMarkerTransform(FQuat::Identity, OutMarkerState.Position);

        const FTransform adjustedMarkerTransform = optitrackMarkerTransform * GetTransform();
        OutMarkerState.Position = adjustedMarkerTransform.GetLocation();

        return true;
    }
    else
    {
        return false;
    }
}

bool AOptitrackClientOrigin::GetLatestSkeletonState(int32 SkelId, FOptitrackSkeletonState& OutSkelState)
{
    bool bResult = false;

    // NOTE: This lock blocks NatNet frame delivery for the duration.
    // See comment in NatNetDataCallback.
    FrameUpdateLock.Lock();

    if (const FOptitrackSkeletonState* pState = LatestSkeletonStates.Find(SkelId))
    {
        OutSkelState = *pState;
        bResult = true;
    }

    FrameUpdateLock.Unlock();

    return bResult;
}

bool AOptitrackClientOrigin::FindSkeletonDefinition(const FName& SkeletonName, FOptitrackSkeletonDefinition& OutSkelDef)
{
    for (const TPair< int32, FOptitrackSkeletonDefinition >& SkelDef : SkeletonDefinitions)
    {
        if (SkelDef.Value.Name == SkeletonName)
        {
            OutSkelDef = SkelDef.Value;
            return true;
        }
    }

    return false;
}


bool AOptitrackClientOrigin::GetLatestRigidBodyStateUntransformed( int32 RbId, FOptitrackRigidBodyState& OutRbState )
{
	bool bResult = false;

	// NOTE: This lock blocks NatNet frame delivery for the duration.
	// See comment in NatNetDataCallback.
	FrameUpdateLock.Lock();

	if ( FOptitrackRigidBodyState* pState = LatestRigidBodyStates.Find( RbId ) )
	{
		OutRbState = *pState;
		bResult = true;
	}

	FrameUpdateLock.Unlock();

	return bResult;
}

bool AOptitrackClientOrigin::GetLatestMarkerStateUntransformed(int32 markerId, FOptitrackMarkerState& OutMarkerState)
{
    bool bResult = false;

    // NOTE: This lock blocks NatNet frame delivery for the duration.
    // See comment in NatNetDataCallback.
    FrameUpdateLock.Lock();

    if (FOptitrackMarkerState* pState = LatestMarkerStates.Find(markerId))
    {
        OutMarkerState = *pState;
        bResult = true;
    }

    FrameUpdateLock.Unlock();

    return bResult;
}



//static
AOptitrackClientOrigin* AOptitrackClientOrigin::FindDefaultClientOrigin( UWorld* World )
{
	AOptitrackClientOrigin* returnClientOrigin = nullptr;

	int numClientOrigins = 0;
	for ( TActorIterator<AOptitrackClientOrigin> ClientOriginIter( World ); ClientOriginIter; ++ClientOriginIter )
	{
		++numClientOrigins;

		if ( numClientOrigins == 1 )
		{
			returnClientOrigin = *ClientOriginIter;
		}
	}

	if ( numClientOrigins == 0 )
	{
		UE_LOG( LogOptitrack, Warning, TEXT( "AOptitrackClientOrigin::FindDefaultClientOrigin: Unable to locate any client origin actors!" ) );
	}
	else if ( numClientOrigins > 1 )
	{
		UE_LOG( LogOptitrack, Warning, TEXT( "AOptitrackClientOrigin::FindDefaultClientOrigin: Multiple client origin actors; using first (%s)" ), *returnClientOrigin->GetName() );
	}

	return returnClientOrigin;
}


//static
AOptitrackClientOrigin* AOptitrackClientOrigin::FindHmdClientOrigin( UWorld* World )
{
	AOptitrackClientOrigin* returnClientOrigin = nullptr;

	int numEligibleClientOrigins = 0;
	for ( TActorIterator<AOptitrackClientOrigin> ClientOriginIter( World ); ClientOriginIter; ++ClientOriginIter )
	{
        ++numEligibleClientOrigins;
        if (numEligibleClientOrigins == 1)
        {
            returnClientOrigin = *ClientOriginIter;
        }
	}

	if ( numEligibleClientOrigins == 0 )
	{
		UE_LOG( LogOptitrack, Warning, TEXT( "AOptitrackClientOrigin::FindHmdClientOrigin: Unable to locate any client origin actors that specify the HMD rigid body ID!" ) );
	}
	else if ( numEligibleClientOrigins > 1 )
	{
		UE_LOG( LogOptitrack, Warning, TEXT( "AOptitrackClientOrigin::FindHmdClientOrigin: Multiple client origin actors specify an HMD rigid body ID; using first (%s)" ), *returnClientOrigin->GetName() );
	}

	return returnClientOrigin;
}


void AOptitrackClientOrigin::PreInitializeComponents()
{
	// TODO: Do we need to support this changing at runtime?
	CachedWorldToMeters = GetWorld()->GetWorldSettings()->WorldToMeters;

	Super::PreInitializeComponents();
}

void AOptitrackClientOrigin::BeginPlay()
{
    Super::BeginPlay();

    if (bAutoInitialize)
    {
        InitializeClient();
    }
}


void AOptitrackClientOrigin::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    Super::EndPlay(EndPlayReason);

    if (Client)
    {
        mAsynchronousDiscoveryActiveFlag = false;
        if (mDetectDisconnectionThread.joinable())
        {
            mDetectDisconnectionExitFlag = true;
            mDetectDisconnectionThread.join();
        }
        if (mNatNetDiscoveryHandle != nullptr)
        {
            NatNet_FreeAsyncServerDiscovery(mNatNetDiscoveryHandle);
            mNatNetDiscoveryHandle = nullptr;
        }
        ShutdownClient();
    }
}

static void NATNET_CALLCONV NatNetFrameReceivedThunk(sFrameOfMocapData* NewFrame, void* UserData)
{
    AOptitrackClientOrigin* This = reinterpret_cast<AOptitrackClientOrigin*>(UserData);

    if (ensure(This))
    {
        This->NatNetFrameReceivedCallback_Internal(NewFrame);
    }
}

static void NATNET_CALLCONV ServerDiscoveredCallbackThunk(const sNatNetDiscoveredServer* pDiscoveredServer, void* pUserContext)
{
    AOptitrackClientOrigin* This = reinterpret_cast<AOptitrackClientOrigin*>(pUserContext);

    if (ensure(This))
    {
        This->ServerDiscoveredCallback(pDiscoveredServer);
    }
}

void AOptitrackClientOrigin::Tick(float DeltaSeconds)
{
	Super::Tick( DeltaSeconds );

	FrameUpdateLock.Lock();

	if ( bDrawDebugSkeletons )
	{
		for ( const TPair< int32, FOptitrackSkeletonDefinition >& skelDefEntry : SkeletonDefinitions )
		{
			const int32 skelId = skelDefEntry.Key;
			const FOptitrackSkeletonDefinition& skelDef = skelDefEntry.Value;

			if ( ! LatestSkeletonStates.Contains( skelId ) )
			{
				continue;
			}

			const FOptitrackSkeletonState& skelState = LatestSkeletonStates[skelId];

			for ( const TPair<int, FOptitrackBoneDefinition>&pair : skelDef.Bones )
			{
                int boneId = pair.Key; 
                if (!skelDef.Bones.Contains(boneId))
                {
                    // BoneId not in the skeleton definition, could happen for non-full skeletons, e.g. hand only models
                    continue;
                }
				const FOptitrackBoneDefinition& boneDef = skelDef.Bones[boneId];
				const int32 parentBoneId = boneDef.ParentId;
				const FTransform& boneTransform = skelState.WorldSpaceBoneTransforms[boneId];

				FVector Start, End;
				FColor LineColor;

				End = boneTransform.GetLocation();

				if ( parentBoneId >= 0 )
				{
					const FTransform& parentBoneTransform = skelState.WorldSpaceBoneTransforms[parentBoneId];
					Start = parentBoneTransform.GetLocation();
					LineColor = FColor::White;
				}
				else
				{
					Start = GetTransform().GetLocation();
					LineColor = FColor::Red;
				}

				static const float SphereRadius = 2.0f;

				FVector EndToStart = (Start - End);
				float ConeLength = EndToStart.Size();
				float Angle = FMath::RadiansToDegrees( FMath::Atan( SphereRadius / ConeLength ) ) * 0.02f;

				DrawDebugSphere( GetWorld(), End, SphereRadius, 10, LineColor );
				DrawDebugCone( GetWorld(), End, EndToStart, ConeLength, Angle, Angle, 4, LineColor );
				//DrawDebugCoordinateSystem( GetWorld(), End, boneTransform.Rotator(), SphereRadius * 4.0f, false, -1.f, 0.1f );
				//DrawDebugString( GetWorld(), End, boneDef.Name.ToString(), nullptr, FColor::White, 0.f, true );
			}
		}
	}

	if( bDrawMarkers )
	{
		for( const TPair<int32, FOptitrackMarkerState>&pair : LatestMarkerStates )
		{
			FOptitrackMarkerState markerState;
			GetLatestMarkerState( pair.Key, markerState );
			FVector MarkerPos = markerState.Position;
			static const float SphereRadius = 1.0f;
			FColor LineColor = FColor::Blue;
			DrawDebugSphere( GetWorld(), MarkerPos, SphereRadius, 10, LineColor );
		}
	}

    if (NeedUpdateDataDescription)
    {
        // Update Data Description
        GetDataDescription();
    }

    if (mTriggerReconnectFlag)
    {
        UE_LOG(LogOptitrack, Log, TEXT("AOptitrackClientOrigin: Attempting server discovery."));
        ShutdownClient();
        Client = IOptitrackNatnetPlugin::Get().CreateClient();

        if (Client == nullptr)
        {
            UE_LOG(LogOptitrack, Error, TEXT("AOptitrackClientOrigin: IOptitrackNatnetPlugin::CreateClient failed"));
            return;
        }

        mClientConnected = false;
        // activate asynchronous discovery
        mAsynchronousDiscoveryActiveFlag = true;
        if (mNatNetDiscoveryHandle != nullptr)
        {
            NatNet_FreeAsyncServerDiscovery(mNatNetDiscoveryHandle);
            mNatNetDiscoveryHandle = nullptr;
        }
        NatNet_CreateAsyncServerDiscovery(&mNatNetDiscoveryHandle, ServerDiscoveredCallbackThunk, this);
        mTriggerReconnectFlag = false;
    }

	FrameUpdateLock.Unlock();
}

bool AOptitrackClientOrigin::InitializeClient()
{
	check( IOptitrackNatnetPlugin::IsAvailable() );

	if ( Client != nullptr )
	{
		UE_LOG( LogOptitrack, Error, TEXT( "AOptitrackClientOrigin: Not valid to call InitializeClient when already initialized" ) );
		return false;
	}

	Client = IOptitrackNatnetPlugin::Get().CreateClient();

	if ( Client == nullptr )
	{
		UE_LOG( LogOptitrack, Error, TEXT( "AOptitrackClientOrigin: IOptitrackNatnetPlugin::CreateClient failed" ) );
		return false;
	}
    
    mClientConnected = false;

    if (bAutoConnect)
    {
        // Initialize NatNet asynchronous discovery.
        mAsynchronousDiscoveryActiveFlag = true;
        NatNet_CreateAsyncServerDiscovery(&mNatNetDiscoveryHandle, ServerDiscoveredCallbackThunk, this);

        // Start disconnection detection thread.
        mDetectDisconnectionThread = std::thread(&AOptitrackClientOrigin::ThreadProc_DetectDisconnection, this);
    }
    else
    {
        // connect normally
        const std::string strClientAddr(TCHAR_TO_ANSI(*ClientAddress));
        const std::string strServerAddr(TCHAR_TO_ANSI(*ServerAddress));
        sNatNetClientConnectParams connectParams;
        connectParams.connectionType = (ConnectionType == EOptitrackClientConnectionType::Unicast) ? ConnectionType_Unicast : ConnectionType_Multicast;
        connectParams.localAddress = strClientAddr.c_str();
        connectParams.serverAddress = strServerAddr.c_str();
        ConnectNatNet(connectParams);
    }

    return true;
}

bool AOptitrackClientOrigin::GetDataDescription()
{
    if (Client == nullptr)
    {
        UE_LOG(LogOptitrack, Error, TEXT("AOptitrackClientOrigin: Data Description requested without valid connected client."));
        return false;
    }

    SkeletonDefinitions.Empty();
    LatestSkeletonStates.Empty();
	LatestRigidBodyStates.Empty();
    sDataDescriptions* DataDescriptions = nullptr;
    const ErrorCode GetDataDescsResult = Client->GetDataDescriptionList(&DataDescriptions);
    if (!ensureMsgf(GetDataDescsResult == ErrorCode_OK && DataDescriptions != nullptr, TEXT("NatNetClient::GetDataDescriptionList failed with return code %d"), GetDataDescsResult))
    {
        ShutdownClient();
        return false;
    }
    // Parse data descriptions for skeleton definitions.
    const float coordScalingFactor = CachedWorldToMeters;
    for (int32 descIdx = 0; descIdx < DataDescriptions->nDataDescriptions; ++descIdx)
    {
        if (DataDescriptions->arrDataDescriptions[descIdx].type == Descriptor_Skeleton)
        {
            const sSkeletonDescription& skelDesc =
                *DataDescriptions->arrDataDescriptions[descIdx].Data.SkeletonDescription;

            FOptitrackSkeletonDefinition skelDef;
            skelDef.Id = skelDesc.skeletonID;
            skelDef.Name = skelDesc.szName;

            skelDef.Bones.Empty(skelDesc.nRigidBodies + 1);
            FOptitrackBoneDefinition rootBoneDef;
            rootBoneDef.Id = 0;
            rootBoneDef.LocalOffset = FVector::ZeroVector;
            rootBoneDef.Name = "Root";
            rootBoneDef.ParentId = -1;
            skelDef.Bones.Emplace(0, rootBoneDef);

            for (int32 boneIdx = 0; boneIdx < skelDesc.nRigidBodies; ++boneIdx)
            {
                const sRigidBodyDescription& boneDesc = skelDesc.RigidBodies[boneIdx];
                checkSlow(boneDesc.ID > 0);

                //FOptitrackBoneDefinition& boneDef = skelDef.Bones[boneDesc.ID];
                FOptitrackBoneDefinition boneDef;
                boneDef.Name = boneDesc.szName;
                boneDef.Id = boneDesc.ID;
                boneDef.ParentId = boneDesc.parentID;
#if 0
                boneDef.LocalOffset = FVector( boneDesc.offsetx * coordScalingFactor, boneDesc.offsetz * coordScalingFactor, boneDesc.offsety * coordScalingFactor );
#else
                boneDef.LocalOffset = FVector( boneDesc.offsetz * coordScalingFactor, -boneDesc.offsetx * coordScalingFactor, boneDesc.offsety * coordScalingFactor );
#endif
                skelDef.Bones.Emplace(boneDesc.ID, boneDef);
            }

            SkeletonDefinitions.Emplace(skelDef.Id, MoveTemp(skelDef));
        }
    }
    Client->SetFrameReceivedCallback(&NatNetFrameReceivedThunk, this);
    sDataDescriptions* pDataDefs = NULL;
    Client->GetDataDescriptionList(&pDataDefs);

    NeedUpdateDataDescription = false;

    return true;
}


bool AOptitrackClientOrigin::ShutdownClient()
{
	if ( Client != nullptr )
	{
		FrameUpdateLock.Lock();
		Client->Disconnect();
		check( IOptitrackNatnetPlugin::IsAvailable() );
		IOptitrackNatnetPlugin::Get().DestroyClient( Client );
		Client = nullptr;
		TimestampType = EOptitrackTimestampType::Unknown;

		FrameUpdateLock.Unlock();

		return true;
	}
	else
	{
		UE_LOG( LogOptitrack, Error, TEXT( "AOptitrackClientOrigin: Not valid to call ShutdownClient when not initialized" ) );
		return false;
	}
	SkeletonDefinitions.Reset();
}

bool AOptitrackClientOrigin::SetDefaultSkeletonSettings()
{
    if (IsClientInitialized())
    {
        // Get bone naming convention and match 
        void* pResult;
        int ret = 0;
        int nBytes = 0;
        ret = Client->SendMessageAndWait("GetProperty,,Bone Naming Convention", &pResult, &nBytes);
        if (ret == ErrorCode_OK)
        {
            char* BoneConvention((char*) pResult);
            if (BoneConvention[0] == '0')
            {
                BoneNamingConvention = EOptitrackBoneNamingConvention::Motive;
            }
            else if (BoneConvention[0] == '1')
            {
                BoneNamingConvention = EOptitrackBoneNamingConvention::FBX;
            }
            else if (BoneConvention[0] == '2')
            {
                BoneNamingConvention = EOptitrackBoneNamingConvention::BVH;
            }
        }
        else
        {
            UE_LOG(LogOptitrack, Error, TEXT("AOptitrackClientOrigin: Unable to match Motive bone naming convention. Check Motive streaming settings."));
        }

        // Ensure skeletons are streamed with local coordinates (and not global)
        ret = Client->SendMessageAndWait("SetProperty,,Skeleton Coordinates,true", &pResult, &nBytes);
        if (ret != ErrorCode_OK)
        {
            UE_LOG(LogOptitrack, Error, TEXT("AOptitrackClientOrigin: Unable to set skeleton coordinates to local. Check Motive streaming settings."));
        }

    }
    return true;
}


void AOptitrackClientOrigin::NatNetFrameReceivedCallback_Internal( struct sFrameOfMocapData* NewFrame )
{
	// Compute a timestamp for the rigid body states. See also ServerSupportsExposureTimestamp().
	const double arrivalTimestamp = FPlatformTime::Seconds();

    // Use best available timestamp (USB cameras don't support mid-exposure).
    const uint64_t natNetTimestamp =
        NewFrame->CameraMidExposureTimestamp == 0
        ? NewFrame->CameraDataReceivedTimestamp
        : NewFrame->CameraMidExposureTimestamp;
    mNatNetLastRecievedTimestamp = natNetTimestamp;
    if (mNatNetTimestampEpoch == 0)
    {
        mNatNetTimestampEpoch = natNetTimestamp;
    }

	double totalLatency;
	if ( NewFrame->CameraMidExposureTimestamp != 0 )
	{
		TimestampType = EOptitrackTimestampType::ServerExposureTime;
		totalLatency = Client->SecondsSinceHostTimestamp( NewFrame->CameraMidExposureTimestamp );
	}
	else if ( NewFrame->CameraDataReceivedTimestamp != 0 )
	{
		// In this case, the pipeline is only partially instrumented, and latency is underestimated slightly.
		// Could be the case with a USB camera system, for example.
		TimestampType = EOptitrackTimestampType::ServerAggregationTime;
		totalLatency = Client->SecondsSinceHostTimestamp( NewFrame->CameraDataReceivedTimestamp );
	}
	else
	{
		// Latency metric is unavailable/unknown.
		TimestampType = EOptitrackTimestampType::LocalArrivalTime;
		totalLatency = 0.0;
	}

	const double dataTimestamp = arrivalTimestamp - totalLatency;

    NatNetFrameReceivedEvent.Broadcast( NewFrame, dataTimestamp );

#if ! UE_BUILD_SHIPPING
	const double networkLatency = Client->SecondsSinceHostTimestamp( NewFrame->TransmitTimestamp );
	const double arrivalPeriod = arrivalTimestamp - LastArrivalTimestamp;

	TimingStats.RecordSamples( totalLatency, networkLatency, arrivalPeriod );
	LastArrivalTimestamp = arrivalTimestamp;
#endif // #if ! UE_BUILD_SHIPPING

	// TODO: In the event of FrameUpdateLock contention, rather than blocking,
	// the frame NatNet is attempting to deliver is dropped. In practice,
	// contention should be infrequent, and the NatNet stream's frame rate is
	// generally much higher than Unreal.
	// A more sophisticated locking (or lock-free) scheme would be preferable.
    if (FrameUpdateLock.TryLock())
    {
        const float coordScalingFactor = CachedWorldToMeters;

        for (int i = 0; i < NewFrame->nRigidBodies; ++i)
        {
            const sRigidBodyData& rbData = NewFrame->RigidBodies[i];

            const bool bTrackedThisFrame = (rbData.params & 0x01) != 0;
            if (bTrackedThisFrame == false)
            {
                continue;
            }

#if 0
            // Relative to OptiTrack volume origin, with conversions for: scale, Y-up to Z-up, right-handed to left-handed. (X basis is preserved.)
            const FVector position(rbData.x * coordScalingFactor, rbData.z * coordScalingFactor, rbData.y * coordScalingFactor);
            const FQuat orientation(rbData.qx, rbData.qz, rbData.qy, -rbData.qw);
#else
            const FVector position(rbData.z * coordScalingFactor, -rbData.x * coordScalingFactor, rbData.y * coordScalingFactor);
            const FQuat orientation(rbData.qz, -rbData.qx, rbData.qy, -rbData.qw);
#endif

            FOptitrackRigidBodyState rbState;
            rbState.TimestampPlatformSeconds = dataTimestamp;
            rbState.Position = position;
            rbState.Orientation = orientation;

            LatestRigidBodyStates.Emplace(rbData.ID, rbState);
        }

        // unpack labeled markers
        LatestMarkerStates.Empty(NewFrame->nLabeledMarkers);
        for (int i = 0; i < NewFrame->nLabeledMarkers; ++i)
        {
            const sMarker& markerData = NewFrame->LabeledMarkers[i];
            const FVector position(markerData.z * coordScalingFactor, -markerData.x * coordScalingFactor, markerData.y * coordScalingFactor);
            FOptitrackMarkerState markerState;
            markerState.TimestampPlatformSeconds = dataTimestamp;
            markerState.Position = position;
            markerState.Id = markerData.ID;

            LatestMarkerStates.Emplace(markerData.ID, markerState);
        }


        // Check if data descriptions have changed
        if ((NewFrame->params & 0x02) != 0)
        {
            NeedUpdateDataDescription = true;
        }

        LatestSkeletonStates.Reserve(NewFrame->nSkeletons);

        for (int skelIdx = 0; skelIdx < NewFrame->nSkeletons; ++skelIdx)
        {
            const sSkeletonData& skelData = NewFrame->Skeletons[skelIdx];

            // TODO: Reject any incoming skeleton state that's invalid or doesn't match our definition,
            // that way downstream client code doesn't need to do redundant checking.
            if (!SkeletonDefinitions.Contains(skelData.skeletonID))
            {
                // TODO: Can't continue without knowledge of the hierarchy.
                // Maybe this condition should flag for a re-request of updated NatNet data descriptions?
                continue;
            }

            const FOptitrackSkeletonDefinition& skelDef = SkeletonDefinitions[skelData.skeletonID];

            FOptitrackSkeletonState& skelState = LatestSkeletonStates.FindOrAdd(skelData.skeletonID);
            skelState.BonePoses.Empty(skelData.nRigidBodies + 1);

            bool bFullSkeletonModel = true;
            if (skelData.nRigidBodies >= 2 && skelData.RigidBodyData[2].ID != 2)
            {
                // 3rd bone is not the spine
                // Skeleton definition is not the entire skeleton ( could be a hand only model )
                bFullSkeletonModel = false;
            }

            skelState.FullSkeleton = bFullSkeletonModel;

            if (bFullSkeletonModel)
            {
                const sRigidBodyData& hipData = skelData.RigidBodyData[0];

#if 0
                const FVector hipPosition = FVector(hipData.x * coordScalingFactor, hipData.z * coordScalingFactor, hipData.y * coordScalingFactor);
                const FQuat hipOrientation = FQuat(hipData.qx, hipData.qz, hipData.qy, -hipData.qw).GetNormalized();
#else
                const FVector hipPosition = FVector(hipData.z * coordScalingFactor, -hipData.x * coordScalingFactor, hipData.y * coordScalingFactor);
                const FQuat hipOrientation = FQuat(hipData.qz, -hipData.qx, hipData.qy, -hipData.qw).GetNormalized();
#endif

                FQuat hipYaw, hipTilt;
                hipOrientation.ToSwingTwist(FVector::UpVector, hipTilt, hipYaw);

                // Extract ground plane component of hip motion to root bone, as well as yaw component of orientation
                //skelState.BonePoses[0].Position = FVector(hipPosition.X, hipPosition.Y, 0.0f);
                //skelState.BonePoses[0].Orientation = hipYaw;
                FOptitrackPose hipPose;
                hipPose.Position = FVector(hipPosition.X, hipPosition.Y, 0.0f);
                hipPose.Orientation = hipYaw;
                skelState.BonePoses.Emplace(0, hipPose);


                const FTransform rootBoneTransform = FTransform(skelState.BonePoses[0].Orientation, skelState.BonePoses[0].Position);

                // Assign just vertical axis translation and tilt orientation to hip bone
                FOptitrackPose hipTiltPose;
                hipTiltPose.Position = FVector(0.0f, 0.0f, hipPosition.Z);
                hipTiltPose.Orientation = hipYaw.Inverse() * hipTilt * hipYaw;
                skelState.BonePoses.Emplace(1, hipTiltPose);
            }
            else
            {
                // Not a full skeleton (could be a hand only model)
                FOptitrackPose rootPose;
                rootPose.Position = FVector::ZeroVector;
                rootPose.Orientation = FQuat::Identity;
                skelState.BonePoses.Emplace(0, rootPose);

                const sRigidBodyData& hipData = skelData.RigidBodyData[0];
                const FVector hipPosition = FVector(hipData.z * coordScalingFactor, -hipData.x * coordScalingFactor, hipData.y * coordScalingFactor);
                const FQuat hipOrientation = FQuat(hipData.qz, -hipData.qx, hipData.qy, -hipData.qw).GetNormalized();
                FOptitrackPose hipPose;
                hipPose.Position = hipPosition;
                hipPose.Orientation = hipOrientation;
                skelState.BonePoses.Emplace(1, hipPose);
            }

            // Streaming bone index is one lower since there's no root bone in the streaming data.
            for (int boneIdx = 1; boneIdx < skelData.nRigidBodies; ++boneIdx)
            {
                const sRigidBodyData& boneData = skelData.RigidBodyData[boneIdx];

                int32 boneSkelId, boneId;
                NatNet_DecodeID(boneData.ID, &boneSkelId, &boneId);
                checkSlow(boneSkelId == skelData.skeletonID);
                checkSlow( boneId > 0 );

                //FOptitrackPose& bonePose = skelState.BonePoses[boneId];
                FOptitrackPose bonePose;
#if 0
                bonePose.Position = FVector(boneData.x * coordScalingFactor, boneData.z * coordScalingFactor, boneData.y * coordScalingFactor);
                bonePose.Orientation = FQuat(boneData.qx, boneData.qz, boneData.qy, -boneData.qw).GetNormalized();
#else
                bonePose.Position = FVector( boneData.z * coordScalingFactor, -boneData.x * coordScalingFactor, boneData.y * coordScalingFactor );
                bonePose.Orientation = FQuat( boneData.qz, -boneData.qx, boneData.qy, -boneData.qw ).GetNormalized();
#endif
                skelState.BonePoses.Emplace(boneId, bonePose);
            }

            // Compute world space bones
            skelState.WorldSpaceBoneTransforms.Empty(skelData.nRigidBodies + 1);

            //for (int boneId = 0; boneId < skelDef.Bones.Num(); ++boneId)
            for (const TPair<int, FOptitrackBoneDefinition>& pair : skelDef.Bones )
            {
                int boneId = pair.Key;
                const FOptitrackBoneDefinition& boneDef = skelDef.Bones[boneId];
                const int32 parentBoneId = boneDef.ParentId;
                checkSlow(parentBoneId < boneId); // Topological ordering
                checkSlow(skelState.BonePoses.Contains(boneId));

                const FOptitrackPose& srcBonePose = skelState.BonePoses[boneId];
                const FTransform& parentBoneTrackingSpace = boneId > 0
                    ? skelState.WorldSpaceBoneTransforms[parentBoneId]
                    : GetTransform();

                const FTransform boneSpaceTransform = FTransform(srcBonePose.Orientation, srcBonePose.Position);

                skelState.WorldSpaceBoneTransforms.Emplace(boneId, boneSpaceTransform * parentBoneTrackingSpace);
            }
        }

		FrameUpdateLock.Unlock();
	}
}

void AOptitrackClientOrigin::ValidateAuthenticationToken(const char* challengeToken, char* authToken)
{
    if (Client)
    {
        Client->ValidateAuthenticationToken(challengeToken, authToken);
    }
}


void AOptitrackClientOrigin::ThreadProc_DetectDisconnection()
{
    // Start the disconnection detection loop
    mDetectDisconnectionExitFlag = false;
    while (mDetectDisconnectionExitFlag == false)
    {
        if (Client == nullptr && mAsynchronousDiscoveryActiveFlag == false)
        {
            // shouldn't happen, but just in case
            UE_LOG(LogOptitrack, Log, TEXT("AOptitrackClientOrigin: NatNet attempting server discovery."));
            mTriggerReconnectFlag = true;
        }

        if (bAutoConnect && mNatNetTimestampEpoch != 0 && mAsynchronousDiscoveryActiveFlag == false)
        {
            const double secondsSinceLastTimestamp =
                Client ->SecondsSinceHostTimestamp(mNatNetLastRecievedTimestamp);

            if (Client != nullptr && secondsSinceLastTimestamp > 1.0)
            {
                // valid timestamp and not actively seeking motive server, receive frame hasn't been called in a second
                // ping Motive to see if it's still there
                void* pResult;
                int ret = 0;
                int nBytes = 0;
                ret = Client->SendMessageAndWait("FrameRate", &pResult, &nBytes);
                if (ret != 0)
                {
                    // unable to query Motive, assume it's gone and trigger re-discovery
                    mTriggerReconnectFlag = true;
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
}

void AOptitrackClientOrigin::ServerDiscoveredCallback(const sNatNetDiscoveredServer* pDiscoveredServer)
{
    if (mClientConnected)
    {
        // already connected...
        return;
    }

    // server discovered, unpack connection info
    if (ServerAddress != "0.0.0.0" && ServerAddress != pDiscoveredServer->serverAddress)
    {
        // newly found server does not match specified server address config, don't connect
        return;
    }

    if (ClientAddress != "0.0.0.0" && ClientAddress != pDiscoveredServer->localAddress)
    {
        // newly found server does not match specified local address config, don't connect
        return;
    }

    UE_LOG(LogOptitrack, Log, TEXT("Motive server discovered, connecting...\n"));

    // Initialize NatNet for discovered server
    sNatNetClientConnectParams connectParams;
    connectParams.connectionType = pDiscoveredServer->serverDescription.ConnectionMulticast ? ConnectionType_Multicast : ConnectionType_Unicast;
    connectParams.serverAddress = pDiscoveredServer->serverAddress;
    connectParams.localAddress = pDiscoveredServer->localAddress;
    ConnectNatNet(connectParams);

    mAsynchronousDiscoveryActiveFlag = false;
}


bool AOptitrackClientOrigin::ConnectNatNet(const sNatNetClientConnectParams& connectParams)
{
    const ErrorCode ConnectResult = Client->Connect(connectParams);
    if (ConnectResult != ErrorCode_OK)
    {
        UE_LOG(LogOptitrack, Error, TEXT("AOptitrackClientOrigin: NatNetClient::Connect failed with return code %d"), static_cast<int32>(ConnectResult));
        ShutdownClient();
        return false;
    }

    if (!GetDataDescription())
    {
        return false;
    }

    mClientConnected = true;

    // Set local/global streaming setting as well as match bone naming convention
    SetDefaultSkeletonSettings();

    return true;
}