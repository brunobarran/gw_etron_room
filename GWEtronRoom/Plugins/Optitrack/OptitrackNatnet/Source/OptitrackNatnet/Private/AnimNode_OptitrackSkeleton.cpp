/*
Copyright 2018 NaturalPoint

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "AnimNode_OptitrackSkeleton.h"
#include "OptitrackClientOrigin.h"
#include <Internationalization/Regex.h>
#include <Animation/AnimInstanceProxy.h>

#if OPTITRACK_SKELNODE_DEBUGDRAW
#	include <DrawDebugHelpers.h>
#endif

FAnimNode_OptitrackSkeleton::FAnimNode_OptitrackSkeleton():
#if 0
    mOptitrackSkeletonQuatBasisChange(FRotator(0, 90, 0)),
    mOptitrackSkeletonQuatBasisChangeInv(FRotator(0, -90, 0))
#else
    mOptitrackSkeletonQuatBasisChange( FRotator( 0, 0, 0 ) ),
    mOptitrackSkeletonQuatBasisChangeInv( FRotator( 0, 0, 0 ) )
#endif
{
    const uint8 kNumBones = static_cast<uint8>(EOptitrackBone::NumBones);
	BoneMappings.SetNum( kNumBones );
	MarkSkeletonDefinitionInvalid();
}


void FAnimNode_OptitrackSkeleton::Initialize_AnyThread( const FAnimationInitializeContext& Context )
{
	Super::Initialize_AnyThread( Context );

	MarkSkeletonDefinitionInvalid();
}

void FAnimNode_OptitrackSkeleton::CacheBones_AnyThread( const FAnimationCacheBonesContext& Context )
{
	const FBoneContainer& RequiredBones = Context.AnimInstanceProxy->GetRequiredBones();

	for ( FBoneReference& BoneRef : BoneMappings )
	{
        BoneRef.Initialize( RequiredBones );
	}

    // Need this for FCompactPose.
    FMemMark Mark( FMemStack::Get() );

    FCompactPose RefPose;
    
	RefPose.SetBoneContainer( &RequiredBones );
	RefPose.ResetToRefPose();

    // compute and cache reference world positions
    ComputePoseCorrectionTransforms(RefPose);

	FCSPose<FCompactPose> CsRefPose;
	CsRefPose.InitPose( RefPose );
    
	// Composite measurements
	const FVector HipPos = GetCsBoneLocation( CsRefPose, EOptitrackBone::Hip );
	const FVector HeadPos = GetCsBoneLocation( CsRefPose, EOptitrackBone::Head );
	const FVector LFootPos = GetCsBoneLocation( CsRefPose, EOptitrackBone::LFoot );
	const FVector RFootPos = GetCsBoneLocation( CsRefPose, EOptitrackBone::RFoot );
	TargetScaling.TorsoLength = (HeadPos - HipPos).Size();
	TargetScaling.AvgLegLength = ((LFootPos - HipPos).Size() + (RFootPos - HipPos).Size()) / 2.0f;

	const FVector LHandPos = GetCsBoneLocation( CsRefPose, EOptitrackBone::LHand );
	const FVector RHandPos = GetCsBoneLocation( CsRefPose, EOptitrackBone::RHand );
	const FVector LShoulderPos = GetCsBoneLocation( CsRefPose, EOptitrackBone::LShoulder );
	const FVector RShoulderPos = GetCsBoneLocation( CsRefPose, EOptitrackBone::RShoulder );
	const float IntraShoulderDist = (LShoulderPos - RShoulderPos).Size();
	const float IntraHandDist = (LHandPos - RHandPos).Size();
	TargetScaling.AvgArmLength = (IntraHandDist - IntraShoulderDist) / 2.0f;

    // initialize ignore bone array
    const TArray<int32> PoseIndexArray = RequiredBones.GetSkeletonToPoseBoneIndexArray();
    IgnoreBoneIndices.Reset(kMaxSourceBones);
    // Loop through bones and find all ignore bones (which include roll bones)
    for (int32 BoneIndex : PoseIndexArray)
    {
        bool BoneIndexInBoneMappings = false;
        for (FBoneReference& BoneRef : BoneMappings)
        {
            if (BoneRef.BoneIndex == BoneIndex)
            {
                BoneIndexInBoneMappings = true;
                break;
            }
        }
        if (!BoneIndexInBoneMappings)
        {
            // add to ignore bone list
            IgnoreBoneIndices.Push(BoneIndex);
            // check if also a roll bone
            FName BoneName = RequiredBones.GetReferenceSkeleton().GetBoneName(BoneIndex);
            TArray<FRegexPattern> RollBoneCandidates;
            RollBoneCandidates.Push(FRegexPattern(TEXT(".*roll.*")));
            RollBoneCandidates.Push(FRegexPattern(TEXT(".*twist.*")));
            for (FRegexPattern& Pattern : RollBoneCandidates)
            {
                FRegexMatcher Matcher(Pattern, BoneName.ToString());
                if (Matcher.FindNext())
                {
                    // Bone is a roll bone
                    RollBoneIndices.Push(BoneIndex);
                    RollBoneParentIndices.Push(RequiredBones.GetParentBoneIndex(BoneIndex));
                    break;
                }
            }
        }
    }

    // Exact segment lengths - skip the root bone.
    TargetScaling.BoneDistanceToParent[0] = 0.0f;
    for (uint8 BoneId = 1; BoneId < static_cast<uint8>(EOptitrackBone::NumBones); ++BoneId)
    {
        EOptitrackBone Bone = static_cast<EOptitrackBone>(BoneId);
        float BoneLength = GetCsBoneDistanceToParent(CsRefPose, Bone);
        if (BoneLength == 0.0f)
        {
            TargetScaling.BoneDistanceToParent[BoneId] = 1.0f;
        }
        else
        {
            TargetScaling.BoneDistanceToParent[BoneId] = BoneLength;
        }
    }
}


void FAnimNode_OptitrackSkeleton::Update_AnyThread( const FAnimationUpdateContext& Context )
{
	EvaluateGraphExposedInputs.Execute( Context );

	if ( StreamingClientOrigin == nullptr )
	{
		return;
	}

	if ( SourceSkeletonDefinitionValid == false )
	{
		if ( ! TryUpdateSkeletonDefinition() )
		{
			return;
		}
	}

	const bool DataAvailable = StreamingClientOrigin->GetLatestSkeletonState( SourceSkeletonDefinition.Id, LatestSkeletonState );
    
	if ( DataAvailable == false )
	{
		MarkSkeletonDefinitionInvalid();
		return;
	}
    FullSkeletonModel = LatestSkeletonState.FullSkeleton;
	ExtractSourceScalingFromLatestState();
	UpdateTargetScaleRatios();
}


void FAnimNode_OptitrackSkeleton::Evaluate_AnyThread( FPoseContext& Output )
{
	Output.Pose.ResetToRefPose();

#if 0
	if ( RetargetBaseMesh != Output.AnimInstanceProxy->GetSkelMeshComponent()->SkeletalMesh )
	{
		return;
	}
#endif

	for ( int BoneId = 0; BoneId < static_cast<int>(EOptitrackBone::NumBones); ++BoneId )
	{
		PerformStreamingRetargeting( Output, BoneId );
	}

	for ( const FCompactPoseBoneIndex fcpIdx : Output.Pose.ForEachBoneIndex() )
	{
		Output.Pose[fcpIdx].DiagnosticCheck_IsValid();
	}

//#if OPTITRACK_SKELNODE_DEBUGDRAW
    if (StreamingClientOrigin && StreamingClientOrigin->bDrawDebugSkeletons)
	{
		FCSPose<FCompactPose> CsPose;
		CsPose.InitPose( Output.Pose );
		DrawPose( Output.AnimInstanceProxy, CsPose, FColor::Green );
	}
//#endif
}


#if WITH_EDITORONLY_DATA
bool FAnimNode_OptitrackSkeleton::UpdateRetargetBasePose()
{
	if ( RetargetBaseMesh )
	{
		RetargetBasePose = RetargetBaseMesh->RetargetBasePose;

		for ( FTransform& RetargetBaseTransform : RetargetBasePose )
		{
			RetargetBaseTransform.NormalizeRotation();
		}

		return true;
	}

	return false;
}

#endif // #if WITH_EDITORONLY_DATA


void FAnimNode_OptitrackSkeleton::CacheStreamingBoneIds( EOptitrackBoneNamingConvention BoneNamingConvention )
{
	for ( int BoneIdx = 0; BoneIdx < kMaxSourceBones; ++BoneIdx )
	{
		BoneStreamingIds[BoneIdx] = FindBoneId( GetStreamingBoneName( static_cast<EOptitrackBone>(BoneIdx), BoneNamingConvention ) );
	}
}

void FAnimNode_OptitrackSkeleton::ComputePoseCorrectionTransforms(const FCompactPose& Pose)
{
    // get base rotation (Y forward vs X forward)
    if (BoneMappings[static_cast<uint8>(EOptitrackBone::LFoot)].IsValidToEvaluate() && BoneMappings[static_cast<uint8>(EOptitrackBone::RFoot)].IsValidToEvaluate())
    {
        // use world translation between feet to infer what is left/right versus forward/backward. Assumes Z is up
        const FBoneReference& LFootBone = BoneMappings[static_cast<uint8>(EOptitrackBone::LFoot)];
        const FBoneReference& RFootBone = BoneMappings[static_cast<uint8>(EOptitrackBone::RFoot)];
        const FVector LFootLocation = GetRefPoseIndexTransform(Pose, LFootBone.GetCompactPoseIndex(Pose.GetBoneContainer()), false, false).GetLocation();
        const FVector RFootLocation = GetRefPoseIndexTransform(Pose, RFootBone.GetCompactPoseIndex(Pose.GetBoneContainer()), false, false).GetLocation();
        const FVector FootDiff = (LFootLocation - RFootLocation).GetSafeNormal();

        const double tol = 0.1;
        if (FVector::DotProduct(FVector(1, 0, 0), FootDiff) > 1 - tol)
        {
            // X+ takes you from right to left => Y forward
            SkeletonBaseRotation = FRotator(0, 90, 0);
        }
    }

    // loop through bones and compute necessary rotation to make UE ref pose into a perfect T-pose
    for (int BoneId = 0; BoneId < static_cast<int>(EOptitrackBone::NumBones); ++BoneId)
    {
        const FBoneReference& BoneRef = BoneMappings[BoneId];
        const EOptitrackBone Bone = static_cast<EOptitrackBone>(BoneId);
        const EOptitrackBone ChildBone = GetBoneSegmentChild(Bone);
        // Reset local pose correction
        LocalPoseCorrectionTransforms[BoneId] = FRotator(0, 0, 0);
        if (ChildBone != EOptitrackBone::Root)
        {
            FBoneReference& ChildBoneRef = BoneMappings[static_cast<uint8>(ChildBone)];
            if (ChildBoneRef.IsValidToEvaluate() && BoneRef.IsValidToEvaluate())
            {
                // Get direction of bone is pointing in world coordinates by looking at child bone
                // Compute needed world rotation to move UE ref pose into T Pose

                // Calculate the unit vector that points along the bone of UE ref pose
                const FVector RefSkeletonLocalAxis = (GetRefPoseIndexTransform(Pose, BoneRef.CachedCompactPoseIndex).GetLocation() 
                    - GetRefPoseIndexTransform(Pose, ChildBoneRef.CachedCompactPoseIndex).GetLocation()).GetSafeNormal();
                
                // Compare vector that points along bone in Motive to the UE Ref pose vector (RefSkeletonLocalAxis)
                const FVector OptiLocalAxis = GetLocalBoneAxis(Bone);
                const FVector UnitDiff = (RefSkeletonLocalAxis - OptiLocalAxis).GetSafeNormal();

                // Calculate rotation needed to rotate RefSkeletonLocalAxis to OptiLocalAxis
                // axis of rotation (world space) given by cross product of the two vectors
                const FVector CrossVector = FVector::CrossProduct(RefSkeletonLocalAxis, OptiLocalAxis).GetSafeNormal();
                // angle of rotation given by dot product between the two vectors
                float theta = FMath::Acos(FVector::DotProduct(OptiLocalAxis, RefSkeletonLocalAxis));
                // Change axis of rotation from world space to local space
                const FVector LocalRotationAxis = GetRefPoseIndexTransform(Pose, BoneRef.CachedCompactPoseIndex).GetRotation().Inverse().RotateVector(CrossVector);
                // Build and store rotator via axis/angle
                LocalPoseCorrectionTransforms[BoneId] = FQuat(LocalRotationAxis, theta).Rotator();
            }
        }
        else if (Bone == EOptitrackBone::Head)
        {
            // The pose correction calculations apply rotations to bones which will affect bones later in hierarchy that may not need it
            // Assume original orientation of Head is fine. Calculate appropriate transform to 'undo' the effect of pose correction earlier in hierarchy.
            FTransform BoneTransformCorrectedRefPose = GetRefPoseIndexTransform(Pose, BoneMappings[BoneId].CachedCompactPoseIndex, true, false);
            FTransform BoneTransformOriginalRefPose = GetRefPoseIndexTransform(Pose, BoneMappings[BoneId].CachedCompactPoseIndex, false, false);
            FQuat BoneCorrection = FQuat(BoneTransformCorrectedRefPose.Rotator().GetInverse())*BoneTransformOriginalRefPose.GetRotation();
            LocalPoseCorrectionTransforms[BoneId] = FRotator(BoneCorrection);
        }
    }
    for (int BoneId = 0; BoneId < static_cast<int>(EOptitrackBone::NumBones); ++BoneId)
    {
        const FBoneReference& BoneRef = BoneMappings[BoneId];
        const EOptitrackBone Bone = static_cast<EOptitrackBone>(BoneId);
        const EOptitrackBone ChildBone = GetBoneSegmentChild(Bone);
        LocalRefPoseBoneAxis[BoneId] = FVector(0, 0, 0);
        if (ChildBone != EOptitrackBone::Root)
        {
            FBoneReference& ChildBoneRef = BoneMappings[static_cast<uint8>(ChildBone)];
            if (ChildBoneRef.IsValidToEvaluate() && BoneRef.IsValidToEvaluate())
            {
                // Get world vector pointing in direction along bone
                const FVector RefSkeletonBoneAxis = (GetRefPoseIndexTransform(Pose, BoneRef.CachedCompactPoseIndex).GetLocation()
                    - GetRefPoseIndexTransform(Pose, ChildBoneRef.CachedCompactPoseIndex).GetLocation()).GetSafeNormal();
                // Store a vector pointing in the direction along the bone in the local bone space
                LocalRefPoseBoneAxis[BoneId] = GetRefPoseIndexTransform(Pose, BoneRef.CachedCompactPoseIndex).Rotator().GetInverse().RotateVector(RefSkeletonBoneAxis);
            }
        }
    }

}

FTransform FAnimNode_OptitrackSkeleton::GetRefPoseIndexTransform(const FCompactPose& Pose, const FCompactPoseBoneIndex& fcpIdx, bool UsePoseCorrection /*=true*/, bool UseSkeletonBaseRotation /*=true*/) const
{
    // Return the reference bone transform of the bone with index fcpIdx
    if (fcpIdx.GetInt() == -1)
    {
        return FTransform(FRotator(0, 0, 0));
    }

    FCompactPoseBoneIndex ParentPoseIndex = Pose.GetParentBoneIndex(fcpIdx);
    if (ParentPoseIndex == INDEX_NONE)
    {
        // Parent is invalid => bone index (fcpIdx) is the root
        if (UseSkeletonBaseRotation)
        {
            return Pose.GetRefPose(fcpIdx) * FQuat(SkeletonBaseRotation.GetInverse());
        }
        else
        {
            return Pose.GetRefPose(fcpIdx);
        }
    }

    // Get pose correction rotation for the current bone
    FTransform PoseCorrectionRotation(FRotator(0, 0, 0));
    if (PoseCorrection && UsePoseCorrection)
    {
        // loop through opti bones to find corresponding mapped bone
        // pose correction transforms are only applied to mapped bones
        for (uint8 BoneId = 1; BoneId < static_cast<uint8>(EOptitrackBone::NumBones); ++BoneId)
        {
            const FBoneReference& BoneRef = BoneMappings[BoneId];
            if (BoneRef.IsValidToEvaluate() && fcpIdx == BoneRef.CachedCompactPoseIndex)
            {
                PoseCorrectionRotation = FTransform(LocalPoseCorrectionTransforms[BoneId]);
            }
        }
    }

    // return local reference bone transform * local to world transform
    return PoseCorrectionRotation * Pose.GetRefPose(fcpIdx) * GetRefPoseIndexTransform(Pose, ParentPoseIndex, UsePoseCorrection, UseSkeletonBaseRotation);
}


int32 FAnimNode_OptitrackSkeleton::FindBoneId( const FName& BoneName )
{
	for ( const TPair<int, FOptitrackBoneDefinition>& pair : SourceSkeletonDefinition.Bones )
	{
		if ( pair.Value.Name.ToString().EndsWith( BoneName.ToString() ) )
		{
			return pair.Value.Id;
		}
	}

	return -1;
}


void FAnimNode_OptitrackSkeleton::PerformStreamingRetargeting( FPoseContext& PoseContext, uint8 BoneId )
{
	if ( BoneId >= BoneMappings.Num() || BoneId >= kMaxSourceBones )
	{
		return;
	}
    
	FBoneReference& BoneRef = BoneMappings[BoneId];
	const int32 BoneStreamingId = BoneStreamingIds[BoneId];

    if ( BoneStreamingId < 0 )
	{
		return;
	}
    
	const FBoneContainer& RequiredBones = PoseContext.Pose.GetBoneContainer();
	if ( BoneRef.IsValidToEvaluate( RequiredBones ) )
	{
		const FCompactPoseBoneIndex fcpIdx = BoneRef.GetCompactPoseIndex( RequiredBones );

        // Get latest Optitrack data for bone
		FTransform& StreamingTransform = LatestSkeletonState.WorldSpaceBoneTransforms[BoneStreamingId];

		const EOptitrackBone ParentBone = GetParentBone( static_cast<EOptitrackBone>(BoneId) );
		const uint8 ParentBoneId = static_cast<uint8>(ParentBone);

		// For every bone, we cancel out the local scale of the parent bone and apply only our
		// local ratio vs. the nominal/reference scale.
		//
		// The exception is the root bone scale, which is allowed to propagate to all bones.
		const FVector ParentScaleToNegate = ( ParentBone == EOptitrackBone::Root  ) ? FVector( 1.f ) : TargetBoneLocalScaleRatios[ParentBoneId];
        const FVector LocalScale = TargetBoneLocalScaleRatios[BoneId] / ParentScaleToNegate;

        if (BoneId == static_cast<uint8>(EOptitrackBone::Root))
        {
            FTransform SkeletonTransform = PoseContext.AnimInstanceProxy->GetSkelMeshComponent()->GetComponentToWorld();
            FTransform RootTransform = StreamingTransform * SkeletonTransform.Inverse();
            PoseContext.Pose[fcpIdx].SetTranslation(RootTransform.GetTranslation());
            FVector RootScale = TargetBoneLocalScaleRatios[static_cast<uint8>(EOptitrackBone::Root)];
            if (ScaleBones && !RootScale.ContainsNaN())
            {
                PoseContext.Pose[fcpIdx].SetScale3D(RootScale);
            }
        }

        if (BoneId != static_cast<uint8>(EOptitrackBone::Root))
        {
            check(ParentBoneId < BoneId); // Topological sort; we must already have solved the parent.
            const FTransform LocalToComponent = GetBoneToComponent(PoseContext.Pose, PoseContext.Pose.GetParentBoneIndex(fcpIdx));
            const FTransform LocalToWorld = LocalToComponent * PoseContext.AnimInstanceProxy->GetSkelMeshComponent()->GetComponentToWorld();
            const FTransform WorldToLocal = LocalToWorld.Inverse();
            FTransform LocalStreamingTransform = StreamingTransform * WorldToLocal;
            
             // Set Pose Rotation/Translation
            const FQuat RefRotation = GetRefPoseIndexTransform(PoseContext.Pose, fcpIdx).GetRotation();
            const FQuat PoseRotation = LocalStreamingTransform.GetRotation() * mOptitrackSkeletonQuatBasisChange * RefRotation;
            PoseContext.Pose[fcpIdx].SetRotation(PoseRotation);

            EOptitrackBone Bone = static_cast<EOptitrackBone>(BoneId);
            if (!FingerBones.Contains(Bone) || !FingerRotationOnly)
            {
                PoseContext.Pose[fcpIdx].SetTranslation(LocalStreamingTransform.GetTranslation());
            }

            // Check if current bone is the parent of a rollbone
            if (RollBoneInterpolation && RollBoneParentIndices.Contains(fcpIdx.GetInt()) && SupportedRollBones.Contains(Bone))
            {
                // Get bone index of roll bone
                int32 ArrayIndex = 0;
                RollBoneParentIndices.Find(fcpIdx.GetInt(), ArrayIndex);
                const int32 RollBoneIndex = RollBoneIndices[ArrayIndex];

                // Decompose Optitrack pose rotation into UE local bone swing/twist.
                const FVector BoneAxis = LocalRefPoseBoneAxis[BoneId];
                FQuat Swing; FQuat Twist;
                PoseRotation.ToSwingTwist(BoneAxis, Swing, Twist);
                // Apply roll bone blend and create roll bone rotation
                const FQuat RollBoneRotation = FQuat::Slerp(FQuat(0, 0, 0, 1), Twist, RollBoneBlending).Inverse();
                
                // check if roll bone is part of hierarchy or outside it
                // If rollbone is within hierarchy, then the next OptiTrack mapped bone will be a child of the rollbone
                const EOptitrackBone ChildBone = GetBoneSegmentChild(Bone);
                const FCompactPoseBoneIndex ChildIdx = BoneMappings[static_cast<uint8>(ChildBone)].CachedCompactPoseIndex;
                const FCompactPoseBoneIndex RollIdx = FCompactPoseBoneIndex(RollBoneIndex);
                const bool RollBoneInHierarchy = PoseContext.Pose.GetParentBoneIndex(ChildIdx).GetInt() == RollBoneIndex;

                // Get local roll bone rotation (Local UE space)
                const FQuat LocalRollBoneRefRotation = PoseContext.Pose.GetRefPose(RollIdx).GetRotation();

                if (RollBoneInHierarchy)
                {
                    // mapped child bone is a child of roll bone => roll bone is part of heirarchy
                    PoseContext.Pose[fcpIdx].SetRotation(PoseRotation * RollBoneRotation);
                    PoseContext.Pose[RollIdx].SetRotation(RollBoneRotation * LocalRollBoneRefRotation);
                }
                else
                {
                    // rollbone is outside heirarchy (no children)
                    PoseContext.Pose[fcpIdx].SetRotation(PoseRotation);
                    PoseContext.Pose[RollIdx].SetRotation(RollBoneRotation * LocalRollBoneRefRotation );
                }
            }

            // Scale Bones if necessary
            if (ScaleBones && !LocalScale.ContainsNaN())
            {
                 PoseContext.Pose[fcpIdx].SetScale3D(LocalScale);
            }

        }

    PoseContext.Pose[fcpIdx].DiagnosticCheck_IsValid();
    } 
}

FTransform FAnimNode_OptitrackSkeleton::GetBoneToComponent(const FCompactPose& Pose, FCompactPoseBoneIndex PoseIndex) const
{
    if (PoseIndex == INDEX_NONE)
    {
        // invalid bone index
        return FTransform();
    }
    const FTransform& BoneTransform = Pose[PoseIndex];
    const FCompactPoseBoneIndex ParentBoneIdx = Pose.GetParentBoneIndex(PoseIndex);
    return BoneTransform * GetBoneToComponent(Pose, ParentBoneIdx);
}

void FAnimNode_OptitrackSkeleton::MarkSkeletonDefinitionInvalid()
{
	SourceSkeletonDefinitionValid = false;

	for ( int32& BoneStreamingId : BoneStreamingIds )
	{
		BoneStreamingId = kInvalidBoneStreamingId;
	}

	ResetTargetScaleRatios();
}

bool FAnimNode_OptitrackSkeleton::TryUpdateSkeletonDefinition()
{
	if ( StreamingClientOrigin->FindSkeletonDefinition( SourceSkeletonAssetName, SourceSkeletonDefinition ) )
	{
		SourceSkeletonDefinitionValid = true;

		CacheStreamingBoneIds( StreamingClientOrigin->BoneNamingConvention );

		// Compute segment length adjustments as anisotropic bone scale factors.
		ExtractSourceScalingFromDefinition();
		UpdateTargetScaleRatios();

		return true;
	}
	else
	{
		return false;
	}
}

// The returned vector should be either unit length or zero length (if the segment should not be scaled).
FVector FAnimNode_OptitrackSkeleton::GetLocalBoneScaleAxis( EOptitrackBone Bone ) const
{
	switch ( Bone )
	{
	//case EOptitrackBone::Hip:
	case EOptitrackBone::Ab:
	case EOptitrackBone::Chest:
    case EOptitrackBone::Head:
    case EOptitrackBone::Neck:
        return FVector(0.0f,0.0f,0.0f);
    case EOptitrackBone::LShoulder:
    case EOptitrackBone::RShoulder:
    case EOptitrackBone::LUArm:
    case EOptitrackBone::RUArm:
        return FVector(0, 0, 0);
	case EOptitrackBone::LFArm:
	case EOptitrackBone::RFArm:
        return FVector(1, 1, 1);
	case EOptitrackBone::LThigh:
    case EOptitrackBone::RThigh:
        return FVector(1, 1, 1);
	case EOptitrackBone::LShin:
	case EOptitrackBone::RShin:
        return FVector(1, 1, 1);
	}

	return FVector::ZeroVector;
}

// The returned vector should be either unit length or zero length (if the segment should not be scaled).
FVector FAnimNode_OptitrackSkeleton::GetLocalBoneAxis(EOptitrackBone Bone) const
{
    switch (Bone)
    {
    case EOptitrackBone::Root:
    case EOptitrackBone::Hip:
    case EOptitrackBone::Ab:
    case EOptitrackBone::Chest:
    case EOptitrackBone::Head:
    case EOptitrackBone::Neck:
        return FVector(0.0f, 0.0f, -1.0f);
    case EOptitrackBone::LShoulder:
    case EOptitrackBone::LUArm:
    case EOptitrackBone::LFArm:
    case EOptitrackBone::LHand:
    case EOptitrackBone::LIndex1:
    case EOptitrackBone::LIndex2:
    case EOptitrackBone::LIndex3:
    case EOptitrackBone::LMiddle1:
    case EOptitrackBone::LMiddle2:
    case EOptitrackBone::LMiddle3:
    case EOptitrackBone::LRing1:
    case EOptitrackBone::LRing2:
    case EOptitrackBone::LRing3:
    case EOptitrackBone::LPinky1:
    case EOptitrackBone::LPinky2:
    case EOptitrackBone::LPinky3:
        return FVector(0.0f, 1.0f, 0.0f);
    case EOptitrackBone::LThumb1:
    case EOptitrackBone::LThumb2:
    case EOptitrackBone::LThumb3:
    {
        return FVector(-sin(ThumbBaseRotation), cos(ThumbBaseRotation), 0.0f);
    }
    case EOptitrackBone::RShoulder:
    case EOptitrackBone::RUArm:
    case EOptitrackBone::RFArm:
    case EOptitrackBone::RHand:
    case EOptitrackBone::RIndex1:
    case EOptitrackBone::RIndex2:
    case EOptitrackBone::RIndex3:
    case EOptitrackBone::RMiddle1:
    case EOptitrackBone::RMiddle2:
    case EOptitrackBone::RMiddle3:
    case EOptitrackBone::RRing1:
    case EOptitrackBone::RRing2:
    case EOptitrackBone::RRing3:
    case EOptitrackBone::RPinky1:
    case EOptitrackBone::RPinky2:
    case EOptitrackBone::RPinky3:
        return FVector(0.0f, -1.0f, 0.0f);
    case EOptitrackBone::RThumb1:
    case EOptitrackBone::RThumb2:
    case EOptitrackBone::RThumb3:
    {   
        return FVector(-sin(ThumbBaseRotation), -cos(ThumbBaseRotation), 0.0f);
    }
    case EOptitrackBone::LThigh:
    case EOptitrackBone::LShin:
    case EOptitrackBone::RThigh:
    case EOptitrackBone::RShin:
        return FVector( 0.0f, 0.0f, 1.0f );
    }

    return FVector::ZeroVector;
}

EOptitrackBone FAnimNode_OptitrackSkeleton::GetBoneSegmentChild( EOptitrackBone Bone ) const
{
	switch ( Bone )
	{
    case EOptitrackBone::Root: return EOptitrackBone::Hip;
    case EOptitrackBone::Hip: return EOptitrackBone::Ab;
	case EOptitrackBone::Ab: return EOptitrackBone::Chest;
	case EOptitrackBone::Chest: return EOptitrackBone::Neck;
	case EOptitrackBone::Neck: return EOptitrackBone::Head;
	case EOptitrackBone::LShoulder: return EOptitrackBone::LUArm;
	case EOptitrackBone::LUArm: return EOptitrackBone::LFArm;
	case EOptitrackBone::LFArm: return EOptitrackBone::LHand;
	case EOptitrackBone::RShoulder: return EOptitrackBone::RUArm;
	case EOptitrackBone::RUArm: return EOptitrackBone::RFArm;
	case EOptitrackBone::RFArm: return EOptitrackBone::RHand;
	case EOptitrackBone::LThigh: return EOptitrackBone::LShin;
	case EOptitrackBone::LShin: return EOptitrackBone::LFoot;
    case EOptitrackBone::LFoot: return EOptitrackBone::LToe;
	case EOptitrackBone::RThigh: return EOptitrackBone::RShin;
	case EOptitrackBone::RShin: return EOptitrackBone::RFoot;
    case EOptitrackBone::RFoot: return EOptitrackBone::RToe;
    case EOptitrackBone::LHand: return EOptitrackBone::LMiddle1;
    case EOptitrackBone::RHand: return EOptitrackBone::RMiddle1;
    case EOptitrackBone::LThumb1: return EOptitrackBone::LThumb2;
    case EOptitrackBone::LThumb2:   return EOptitrackBone::LThumb2;
    case EOptitrackBone::LIndex1:return EOptitrackBone::LIndex2;
    case EOptitrackBone::LIndex2:return EOptitrackBone::LIndex3;
    case EOptitrackBone::LMiddle1:return EOptitrackBone::LMiddle2;
    case EOptitrackBone::LMiddle2:return EOptitrackBone::LMiddle3;
    case EOptitrackBone::LRing1:return EOptitrackBone::LRing2;
    case EOptitrackBone::LRing2:return EOptitrackBone::LRing3;
    case EOptitrackBone::LPinky1:return EOptitrackBone::LPinky2;
    case EOptitrackBone::LPinky2: return EOptitrackBone::LPinky3;
    case EOptitrackBone::RThumb1: return EOptitrackBone::RThumb2;
    case EOptitrackBone::RThumb2:   return EOptitrackBone::RThumb2;
    case EOptitrackBone::RIndex1:return EOptitrackBone::RIndex2;
    case EOptitrackBone::RIndex2:return EOptitrackBone::RIndex3;
    case EOptitrackBone::RMiddle1:return EOptitrackBone::RMiddle2;
    case EOptitrackBone::RMiddle2:return EOptitrackBone::RMiddle3;
    case EOptitrackBone::RRing1:return EOptitrackBone::RRing2;
    case EOptitrackBone::RRing2:return EOptitrackBone::RRing3;
    case EOptitrackBone::RPinky1:return EOptitrackBone::RPinky2;
    case EOptitrackBone::RPinky2: return EOptitrackBone::RPinky3;
	}

	return EOptitrackBone::Root;
}


void FAnimNode_OptitrackSkeleton::ExtractSourceScalingFromDefinition()
{
	const FVector HipPos = GetStreamDefBoneLocation( GetStreamingId( EOptitrackBone::Hip ) );
	const FVector HeadPos = GetStreamDefBoneLocation( GetStreamingId( EOptitrackBone::Head ) );
	const FVector LFootPos = GetStreamDefBoneLocation( GetStreamingId( EOptitrackBone::LFoot ) );
	const FVector RFootPos = GetStreamDefBoneLocation( GetStreamingId( EOptitrackBone::RFoot ) );
	SourceScaling.TorsoLength = (HeadPos - HipPos).Size();
	SourceScaling.AvgLegLength = ((LFootPos - HipPos).Size() + (RFootPos - HipPos).Size()) / 2.0f;

	// Exact segment lengths - skip the root bone.
	SourceScaling.BoneDistanceToParent[0] = 0.0f;
	for ( uint8 BoneId = 1; BoneId < static_cast<uint8>(EOptitrackBone::NumBones); ++BoneId )
	{
		EOptitrackBone Bone = static_cast<EOptitrackBone>(BoneId);
		SourceScaling.BoneDistanceToParent[BoneId] = GetStreamDefBoneDistanceToParent( Bone );
	}
}


void FAnimNode_OptitrackSkeleton::ExtractSourceScalingFromLatestState()
{
	// Exact segment lengths - skip the root bone.
	SourceScaling.BoneDistanceToParent[0] = 0.0f;
	for ( uint8 BoneId = 1; BoneId < static_cast<uint8>(EOptitrackBone::NumBones); ++BoneId )
	{
		EOptitrackBone Bone = static_cast<EOptitrackBone>(BoneId);
		SourceScaling.BoneDistanceToParent[BoneId] = GetStreamDefBoneDistanceToParent( Bone );
	}
}


void FAnimNode_OptitrackSkeleton::UpdateTargetScaleRatios()
{
	const float SourceHeadToAnkle = SourceScaling.TorsoLength + SourceScaling.AvgLegLength;
	const float TargetHeadToAnkle = TargetScaling.TorsoLength + TargetScaling.AvgLegLength;
	const float TargetRootScaleRatio = SourceHeadToAnkle / TargetHeadToAnkle;

    if (FullSkeletonModel)
    {
        TargetBoneLocalScaleRatios[static_cast<uint8>(EOptitrackBone::Root)] = FVector(TargetRootScaleRatio);
    }
    else
    {
        TargetBoneLocalScaleRatios[static_cast<uint8>(EOptitrackBone::Root)] = FVector(1.0f);
    }

	// Segment length ratios - skip the root and hips.
	static_assert(0 == static_cast<uint8>(EOptitrackBone::Root), "Expecting to skip root by starting at 2");
	static_assert(1 == static_cast<uint8>(EOptitrackBone::Hip), "Expecting to skip hips by starting at 2");
	for ( uint8 BoneId = 2; BoneId < static_cast<uint8>(EOptitrackBone::NumBones); ++BoneId )
	{
		const EOptitrackBone Bone = static_cast<EOptitrackBone>(BoneId);
		const uint8 SegmentChildId = static_cast<uint8>(GetBoneSegmentChild( Bone ));
		if ( SegmentChildId != 0 && Bone != EOptitrackBone::LFoot && Bone != EOptitrackBone::RFoot)
		{
            if (TargetScaling.BoneDistanceToParent[SegmentChildId] != 0.0f)
            {
                FVector boneScale = FVector(1.f) -
                    (FVector(1.f) * GetLocalBoneScaleAxis(Bone)) +
                    (GetLocalBoneScaleAxis(Bone) *
                    (SourceScaling.BoneDistanceToParent[SegmentChildId] /
                        TargetScaling.BoneDistanceToParent[SegmentChildId] /
                        TargetRootScaleRatio));
                if (boneScale != FVector::ZeroVector)
                {
                    TargetBoneLocalScaleRatios[BoneId] = boneScale;
                }
                else
                {
                    TargetBoneLocalScaleRatios[BoneId] = FVector(1.0f);
                }
            }
            else
            {
                TargetBoneLocalScaleRatios[BoneId] = FVector(1.f);
            }
		}
		else
		{
			TargetBoneLocalScaleRatios[BoneId] = FVector( 1.f );
		}
	}
}

void FAnimNode_OptitrackSkeleton::ResetTargetScaleRatios()
{
	for ( FVector& BoneScaleRatio : TargetBoneLocalScaleRatios )
	{
		BoneScaleRatio = FVector( 1.0f );
	}
}


int32 FAnimNode_OptitrackSkeleton::GetStreamingId( EOptitrackBone Bone ) const
{
	const uint8 BoneId = static_cast<uint8>(Bone);
	return BoneStreamingIds[BoneId];
}


FTransform& FAnimNode_OptitrackSkeleton::PoseTransform( FCompactPose& Pose, EOptitrackBone Bone )
{
	const uint8 BoneId = static_cast<uint8>(Bone);
	FBoneReference& BoneRef = BoneMappings[BoneId];
	const FCompactPoseBoneIndex PoseBoneIdx = BoneRef.GetCompactPoseIndex( Pose.GetBoneContainer() );
	return Pose[PoseBoneIdx];
}


FVector FAnimNode_OptitrackSkeleton::GetCsBoneLocation( /*const*/ FCSPose<FCompactPose>& CsPose, EOptitrackBone Bone ) const
{
	const FBoneContainer& RequiredBones = CsPose.GetPose().GetBoneContainer();
	const uint8 BoneId = static_cast<uint8>(Bone);
	const FBoneReference& BoneRef = BoneMappings[BoneId];
	if ( BoneRef.IsValidToEvaluate( RequiredBones ) )
	{
		const FCompactPoseBoneIndex BoneIdx = BoneRef.GetCompactPoseIndex( RequiredBones );
		return CsPose.GetComponentSpaceTransform( BoneIdx ).GetLocation();
	}

	return FVector::ZeroVector;
}


float FAnimNode_OptitrackSkeleton::GetCsBoneDistanceToParent( /*const*/ FCSPose<FCompactPose>& CsPose, EOptitrackBone Bone ) const
{
	const FBoneContainer& RequiredBones = CsPose.GetPose().GetBoneContainer();
	const uint8 BoneId = static_cast<uint8>(Bone);
	const FBoneReference& BoneRef = BoneMappings[BoneId];
	if ( BoneRef.IsValidToEvaluate( RequiredBones ) )
	{
		const FCompactPoseBoneIndex BoneIdx = BoneRef.GetCompactPoseIndex( RequiredBones );
        const FCompactPoseBoneIndex ParentBoneIdx = GetMappedParentPoseIndex(CsPose.GetPose(), BoneIdx);
        check(ParentBoneIdx != BoneIdx);

		if ( CsPose.GetPose().IsValidIndex( ParentBoneIdx ) )
		{
			const FVector BoneCsLocation = CsPose.GetComponentSpaceTransform( BoneIdx ).GetLocation();
			const FVector ParentCsLocation = CsPose.GetComponentSpaceTransform( ParentBoneIdx ).GetLocation();
			return (BoneCsLocation - ParentCsLocation).Size();
		}
	}

	return 0.f;
}

FCompactPoseBoneIndex FAnimNode_OptitrackSkeleton::GetMappedParentPoseIndex(const FCompactPose& Pose, const FCompactPoseBoneIndex BoneIdx) const
{
    check(Pose.IsValidIndex(BoneIdx))
    if (!Pose.IsValidIndex(BoneIdx))
    {
        return BoneIdx;
    }
    // find parent compact pose bone index that is mapped (not an ignore bone)
    const FCompactPoseBoneIndex ParentBoneIdx = Pose.GetBoneContainer().GetParentBoneIndex(BoneIdx);
    check(Pose.IsValidIndex(ParentBoneIdx));
    if (!IgnoreBoneIndices.Contains(ParentBoneIdx.GetInt()))
    {
        // Parent is not an ignore bone (i.e. mapped). Return parent index;
        return ParentBoneIdx;
    }
    else
    {
        return GetMappedParentPoseIndex(Pose, ParentBoneIdx);
    }
}

FVector FAnimNode_OptitrackSkeleton::GetStreamDefBoneLocation( int32 StreamingId ) const
{
    if (StreamingId == -1)
    {
        return FVector(0, 0, 0);
    }
	const FOptitrackBoneDefinition& BoneDef = SourceSkeletonDefinition.Bones[StreamingId];
	const FVector BoneLocalPos = BoneDef.LocalOffset;
	if ( BoneDef.ParentId != kInvalidBoneStreamingId )
	{
		return BoneLocalPos + GetStreamDefBoneLocation( BoneDef.ParentId );
	}
	else
	{
		return BoneLocalPos;
	}
}


float FAnimNode_OptitrackSkeleton::GetStreamDefBoneDistanceToParent( EOptitrackBone Bone ) const
{
	const int32 StreamingId = GetStreamingId( Bone );
	if ( StreamingId != kInvalidBoneStreamingId )
	{
		return SourceSkeletonDefinition.Bones[StreamingId].LocalOffset.Size();
	}
	else
	{
		return 0.f;
	}
}


float FAnimNode_OptitrackSkeleton::GetLatestStateBoneDistanceToParent( EOptitrackBone Bone ) const
{
	const int32 StreamingId = GetStreamingId( Bone );
	if ( StreamingId != kInvalidBoneStreamingId )
	{
		return LatestSkeletonState.BonePoses[StreamingId].Position.Size();
	}
	else
	{
		return 0.f;
	}
}


//#if OPTITRACK_SKELNODE_DEBUGDRAW
template< typename PoseT >
void FAnimNode_OptitrackSkeleton::DrawPose( FAnimInstanceProxy* DrawProxy, FCSPose< PoseT >& CsPose, const FColor& DrawColor )
{
	if ( DrawProxy == nullptr )
	{
		return;
	}

	const FTransform ComponentToWorld = DrawProxy->GetSkelMeshComponent()
		? DrawProxy->GetSkelMeshComponent()->GetComponentToWorld()
		: FTransform::Identity;

	for ( const FCompactPoseBoneIndex fcpIdx : CsPose.GetPose().ForEachBoneIndex() )
	{
		FTransform BoneTM = CsPose.GetComponentSpaceTransform( fcpIdx );
		BoneTM.NormalizeRotation();

		FVector Start, End;
		FColor LineColor = DrawColor;

		End = BoneTM.GetLocation();

		const FCompactPoseBoneIndex fcpParentIdx = CsPose.GetPose().GetParentBoneIndex( fcpIdx );
		if ( fcpParentIdx >= 0 )
		{
			FTransform ParentTM = CsPose.GetComponentSpaceTransform( fcpParentIdx );
			ParentTM.NormalizeRotation();
			Start = ParentTM.GetLocation();
		}
		else
		{
			Start = FVector::ZeroVector;//ComponentToWorld.GetLocation();
			LineColor = FColor::Red;
		}

		static const float SphereRadius = 1.0f;

		// Calc cone size
#if 0
		FVector EndToStart = (Start - End);
		float ConeLength = EndToStart.Size();
		float Angle = FMath::RadiansToDegrees( FMath::Atan( SphereRadius / ConeLength ) ) * 0.02f;
		DrawDebugCone( World, End, EndToStart, ConeLength, Angle, Angle, 4, LineColor );
		DrawDebugSphere( World, End, SphereRadius, 10, LineColor );
		DrawDebugCoordinateSystem( World, End, BoneTM.Rotator(), SphereRadius );

		const FMeshPoseBoneIndex MeshIdx =
			CsPose.GetPose().GetBoneContainer().MakeMeshPoseIndex( fcpIdx );
		const FName& BoneName =
			SkelMeshComp->SkeletalMesh->RefSkeleton.GetRefBoneInfo()[ MeshIdx.GetInt() ].Name;

		DrawDebugString( World, End, BoneName.ToString(), nullptr, FColor::White, 0.0f );
#else
		const FVector WorldStart = ComponentToWorld.TransformPosition( Start );
		const FVector WorldEnd = ComponentToWorld.TransformPosition( End );
		DrawProxy->AnimDrawDebugLine( WorldStart, WorldEnd, LineColor );
		DrawProxy->AnimDrawDebugSphere( WorldEnd, SphereRadius, 10, LineColor );
        //DrawProxy->AnimDrawDebugCoordinateSystem(WorldEnd, BoneTM.Rotator(), SphereRadius * 4.0f, false, -1.f, 0.15f);
#endif
	}
}
//#endif // #if OPTITRACK_SKELNODE_DEBUGDRAW
