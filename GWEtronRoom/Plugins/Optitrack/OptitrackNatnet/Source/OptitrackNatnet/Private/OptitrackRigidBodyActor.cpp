/*
Copyright 2018 NaturalPoint

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "OptitrackRigidBodyActor.h"
#include "OptitrackRigidBodyComponent.h"
#include "OptitrackClientOrigin.h"


AOptitrackRigidBodyActor::AOptitrackRigidBodyActor( const FObjectInitializer& ObjectInitializer )
	: Super( ObjectInitializer )
{
	RigidBodyComponent = CreateDefaultSubobject<UOptitrackRigidBodyComponent>( TEXT( "RigidBodyComponent" ) );

	RootComponent = RigidBodyComponent;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	PrimaryActorTick.bTickEvenWhenPaused = true;
}


void AOptitrackRigidBodyActor::TickActor( float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction )
{
	Super::TickActor( DeltaTime, TickType, ThisTickFunction );

	FOptitrackRigidBodyState rbState;
	if( RigidBodyComponent->TrackingOrigin && bHideOnInvalidDefinition )
	{
		if( RigidBodyComponent->TrackingOrigin->GetLatestRigidBodyState( RigidBodyComponent->TrackingId, rbState ) )
		{
			SetActorHiddenInGame( false );
		}
		else
		{
			SetActorHiddenInGame( true );
		}
		
	}
}

