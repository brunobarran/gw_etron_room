/*
Copyright 2018 NaturalPoint

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include "AnimGraphNode_OptitrackSkeleton.h"


#define LOCTEXT_NAMESPACE "OptitrackNatnet"


UAnimGraphNode_OptitrackSkeleton::UAnimGraphNode_OptitrackSkeleton( const FObjectInitializer& ObjectInitializer )
    : Super( ObjectInitializer )
{
}


FText UAnimGraphNode_OptitrackSkeleton::GetNodeTitle( ENodeTitleType::Type TitleType ) const
{
    return LOCTEXT( "OptitrackSkeleton_NodeTitle", "OptiTrack Skeleton" );
}


FText UAnimGraphNode_OptitrackSkeleton::GetTooltipText() const
{
    return LOCTEXT( "OptitrackSkeleton_Tooltip", "Drive skeleton based on streamed motion capture data from an OptiTrack NatNet server." );
}


FText UAnimGraphNode_OptitrackSkeleton::GetMenuCategory() const
{
    return LOCTEXT( "OptitrackSkeleton_MenuCategory", "Optitrack" );
}


#undef LOCTEXT_NAMESPACE
