// Copyright (C) Microsoft. All rights reserved.

using UnrealBuildTool;

public class JsonUtils : ModuleRules
{
	public JsonUtils(ReadOnlyTargetRules Target) : base(Target)
    {
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
        bEnforceIWYU = false;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine"});
        PrivateDependencyModuleNames.AddRange(new string[] { "MessageLog", "Json", "Core" });
	}
}
