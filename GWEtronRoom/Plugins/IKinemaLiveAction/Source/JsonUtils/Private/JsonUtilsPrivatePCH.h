// Copyright (C) Microsoft. All rights reserved.

#pragma once

#include "Core.h"
#include "Engine.h"
//Removing monolithic headers replacing with IWYU
//#include "UObject/Class.h"
//#include "UObject/UnrealType.h"
//#include "UObject/PropertyPortFlags.h"
//#include "Modules/ModuleManager.h"
//#include "UObject/ObjectMacros.h"
//#include "Dom/JsonValue.h"
//#include "Dom/JsonObject.h"
//#include "Serialization/JsonSerializer.h"
//#include "Serialization/JsonTypes.h"

DECLARE_LOG_CATEGORY_EXTERN(LogJsonUtils, Warning, All);

#include "JsonUtilsModule.h"
#include "JsonUtils.h"
