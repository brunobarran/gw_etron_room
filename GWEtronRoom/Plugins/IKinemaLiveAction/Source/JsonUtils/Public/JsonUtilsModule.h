// Copyright (C) Microsoft. All rights reserved.

#pragma once

#include "Modules/ModuleInterface.h"

class FJsonUtilsModule : public IModuleInterface
{
public:
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};
