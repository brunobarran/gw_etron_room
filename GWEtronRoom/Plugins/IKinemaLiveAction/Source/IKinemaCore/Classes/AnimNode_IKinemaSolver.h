/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include "Animation/AnimNodeBase.h"
#include "IKinemaRig.h"
#include "UObject/LinkerLoad.h"
#include "AnimNode_IKinemaSolver.generated.h"

USTRUCT(BlueprintType)
struct IKINEMACORE_API FAnimNode_IKinemaSolver : public FAnimNode_Base
{
	GENERATED_USTRUCT_BODY()

	// The input pose is segmented into two:
	// - The FK input pose that serves as a bias for the solver.
	// - The task targets appended at the end.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Links)
	FPoseLink InPose;

	// This is set by the anim graph node, so don't allow further edits.
	UPROPERTY()
	class UIKinemaRig* IKinemaRig;

	// The targets for the IKinema solver to try and achieve.
	UPROPERTY(EditFixedSize, EditAnywhere, BlueprintReadWrite, Category=Links, meta=(PinShownByDefault))
	TArray<FIKinemaSolverTask> Tasks;

	//UPROPERTY(EditFixedSize, EditAnywhere, Category=Links, meta(PinShownByDefault))
	//TArray<bool> overrideTaskProperties;

	UPROPERTY(EditFixedSize, EditAnywhere, BlueprintReadWrite, Category=Links, meta=(PinHiddenByDefault))
	TArray<FIKinemaTaskProperties> TaskProperties;

	//UPROPERTY(EditFixedSize, EditAnywhere, Category=Links)
	//TArray<bool> overrideTaskProperties;

	UPROPERTY(EditFixedSize, EditAnywhere, Category=Links)
	TArray<FIKinemaTaskOverride> PinTaskProperties;

	UPROPERTY(EditFixedSize, EditAnywhere, BlueprintReadWrite, Category=Links, meta=(PinHiddenByDefault))
	TArray<FIKinemaSegmentProperties> SegmentProperties;

	//UPROPERTY(EditFixedSize, EditAnywhere, Category=Links)
	//TArray<bool> overrideSegmentProperties;

	UPROPERTY(EditFixedSize, EditAnywhere, Category=Links)
	TArray<FIKinemaSegmentOverride> PinSegmentProperties;

protected:

	// Keep the binding here.
	FIKinemaRigToSkeletonBinding mSkeletonBinding;

	// The IKinema solver instance data (solver and tasks).
	FIKinemaSolverInstance mSolverInstance;

	int32	UpdateVersion;

	// Ensures the specified object is preloaded.  ReferencedObject can be NULL.
	void PreloadObject(UObject* ReferencedObject)
	{
		if ((ReferencedObject != NULL) && ReferencedObject->HasAnyFlags(RF_NeedLoad))
		{
			ReferencedObject->GetLinker()->Preload(ReferencedObject);
		}
	}

public:	

	FAnimNode_IKinemaSolver();
	~FAnimNode_IKinemaSolver();

	void Dump(const FString& Folder);

	// Set the rig on this.
	virtual void SetAsset(class UIKinemaRig* rig);
	virtual void UpdateSolverSettings(class UIKinemaRig* rig);

	// FAnimNode_Base interface
	virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	virtual void Update_AnyThread(const FAnimationUpdateContext& Context) override;
	virtual void Evaluate_AnyThread(FPoseContext& Output) override;
	virtual void CacheBones_AnyThread(const FAnimationCacheBonesContext & Context) override;
	// End of FAnimNode_Base interface
};
