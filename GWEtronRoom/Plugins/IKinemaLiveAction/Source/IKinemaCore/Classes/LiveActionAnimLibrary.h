#pragma once


#include "Animation/AnimNodeBase.h"
#include "IKinemaRig.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "LiveActionAnimLibrary.generated.h"

UCLASS()
class IKINEMACORE_API ULAUtilities : public UBlueprintFunctionLibrary {

	GENERATED_BODY()

		UFUNCTION(BlueprintPure, Category = "IKINEMA", DisplayName = "Get IKINEMA Rig Scale")
		static void GetRigScale(UIKinemaRig* Rig, float& Scale) {
		Scale = Rig->Actor->ImportScale;
	}
};
