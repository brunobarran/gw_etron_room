/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "IKinemaRig.h"
#include "AnimNode_IKinemaSolver.h"
#include "AnimNode_IKinemaMarkerSolving.generated.h"

struct rigidBody
{
	TArray<int32> markerIndex;
	TArray<FVector> relativeDistance;
};

USTRUCT(BlueprintType)
struct IKINEMACORE_API FAnimNode_IKinemaMarkerSolving : public FAnimNode_IKinemaSolver
{
	GENERATED_USTRUCT_BODY()


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinShownByDefault))
	bool DebugDraw;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SolverTuning, meta = (PinHiddenByDefault))
	bool AutoTune;


	virtual void SetAsset(class UIKinemaRig* rig) override;

public:
	FAnimNode_IKinemaMarkerSolving();
	~FAnimNode_IKinemaMarkerSolving();
	virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	virtual void Update_AnyThread(const FAnimationUpdateContext& Context) override;
	virtual void Evaluate_AnyThread(FPoseContext& Output) override;
private:
	TMap<int32,rigidBody> rigidBodies;
};
