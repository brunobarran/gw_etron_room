/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "IKinemaRig.h"
#include "Animation/AnimNodeBase.h"
#include "UObject/LinkerLoad.h"
#include "RootFilter.h"
#include "AnimNode_IKinemaRetargeting.generated.h"

USTRUCT(BlueprintType)
struct IKINEMACORE_API FAnimNode_IKinemaRetargeting : public FAnimNode_Base
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
	FPoseLink InPose;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SolverTuning, meta = (PinHiddenByDefault))
	bool AutoTune;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SolverTuning, meta = (PinHiddenByDefault, DisplayName = "Pull Gain"))
	float pCoeff;

	// This is set by the anim graph node, so don't allow further edits.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Rig, meta = (DisplayName = "IKINEMA Rig"))
	class UIKinemaRig* IKinemaRig;

	// The targets for the IKinema solver to try and achieve.
	UPROPERTY(EditFixedSize, EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinHiddenByDefault))
	TArray<FIKinemaSolverTask> Tasks;


	UPROPERTY(EditFixedSize, EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinHiddenByDefault))
	TArray<FIKinemaTaskProperties> TaskProperties;

	
	UPROPERTY(EditFixedSize, EditAnywhere, Category = Links)
	TArray<FIKinemaTaskOverride> PinTaskProperties;

	UPROPERTY(EditFixedSize, EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinHiddenByDefault))
	TArray<FIKinemaSegmentProperties> SegmentProperties;


	UPROPERTY(EditFixedSize, EditAnywhere, Category = Links)
	TArray<FIKinemaSegmentOverride> PinSegmentProperties;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinHiddenByDefault, DisplayName = "Prop", ToolTip = "Is a prop using less than 3 tasks"))
	bool isProp = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinShownByDefault))
	bool DebugDraw;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RootMotion, meta = (PinShownByDefault, DisplayName = "Extract Root Motion", ToolTip = "Extract root motion from in-place live animation, and apply to scene component"))
	bool ExtractRootMotion;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RootMotion, meta = (PinShownByDefault, DisplayName = "Extraction Alpha", ToolTip = "How harshly do we want to filter the motion [0.01 to 0.99]", UIMin = 0.01f, UIMax = 0.999f, ClampMin = 0.01f, ClampMax = 0.999f))
	float ExtractionAlpha;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RootMotion, meta = (PinHiddenByDefault, DisplayName = "Hip bone ID", ToolTip = "Specify a specific hip bone ID (Default 0)"))
	int RootBoneID;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RootMotion, meta = (PinHiddenByDefault, DisplayName = "Inverse Bone Axis", ToolTip = "Is root Y-up bonespace?"))
	bool InverseBoneAxis = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RootMotion, meta = (PinHiddenByDefault, DisplayName = "Position Offset", ToolTip = "Position the character in the scene"))
	FVector PositionOffset = FVector(0.f, 0.f, 0.f);

public:
	FAnimNode_IKinemaRetargeting();
	~FAnimNode_IKinemaRetargeting();

	virtual void SetAsset(class UIKinemaRig* rig);
	virtual void UpdateSolverSettings(class UIKinemaRig* rig);
	virtual void BindSourceToTarget(FAnimInstanceProxy * vim);

	virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	virtual void Update_AnyThread(const FAnimationUpdateContext& Context) override;
	virtual void Evaluate_AnyThread(FPoseContext& Output) override;

protected:

	// Keep the binding here.
	FIKinemaRigToSkeletonBinding mSkeletonBinding;

	// The IKinema solver instance data (solver and tasks).
	FIKinemaSolverInstance mSolverInstance;

	int32	UpdateVersion;
	bool IsRigBound;
	
	USceneComponent* characterSceneComponent;

	RootFilter filterObjectX;
	RootFilter filterObjectY;

	FVector filteredMotion = FVector(0.f, 0.f, 0.f);
	FVector resetHipVector = FVector(0.f, 0.f, 0.f);
	FVector transferedMotion = FVector(0.f, 0.f, 0.f);
	FVector hipBoneWorldPos = FVector(0.f, 0.f, 0.f);
	FVector axisChanged = FVector(0.f, 0.f, 0.f);
	
	// Ensures the specified object is preloaded.  ReferencedObject can be NULL.
	void PreloadObject(UObject* ReferencedObject)
	{
		if ((ReferencedObject != NULL) && ReferencedObject->HasAnyFlags(RF_NeedLoad))
		{
			ReferencedObject->GetLinker()->Preload(ReferencedObject);
		}
	}
};
