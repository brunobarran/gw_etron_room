/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include "Animation/AnimNodeBase.h"
#include "IKSolver.h"
#include "IKTask.h"
#include "MocapActorSource.h"
#include "ACP/ACPRig.h"
//#include "LiveLinkRetargetAsset.h"
#include "IKinemaRig.generated.h"


namespace FIK
{
	template<typename T> class AnimNode;

}

// Convert between Maya-like space (left-handed, Y-down) and Maya space (right-handed, Y-up).
static void ConvertTransformToFromMaya(FTransform& Transform, bool convertTranslation);

// Perform AutoTune of the Solver and return the new PCoef
void AutoTuneSolver(FIK::IKSolver * Solver, int32 MaxIter, float& PCoef);

// DebugDraw IKinema Skeleton in the specified color,default is Black
void DebugDrawIKinema(FIK::IKSolver * Solver, FTransform const& mSolverToWorldSpace, FColor const& color = FColorList::Black, UWorld* World = nullptr);

// Free function to update ACPNode, solve and update the Output pose with the results. 
// Moved here to abstract into a single place
void SolveAndUpdateACPPose(FIK::AnimNode<FIK::ACP::ACPRig>* acpNode, const TArray<int32>& ACPToUE4, FPoseContext& Output);

UENUM()
enum EIKinemaBoneAxisType
{
	EIKBAT_X,
	EIKBAT_Y,
	EIKBAT_Z,
	EIKBAT_NEGX,
	EIKBAT_NEGY,
	EIKBAT_NEGZ
};

UENUM()
enum EIKinemaCollisionShape
{
	EIKNONE		UMETA(DisplayName = "No Collision Primitive", ToolTip = "Disable this bone from affecting any self penetration constraint"),
	EIKSPHERE	UMETA(DisplayName = "Sphere", ToolTip = "Spherical collision primitive"),
	EIKCAPSULE	UMETA(DisplayName = "Capsule", ToolTip = "Capsule collision primitive"),
	EIKBOX		UMETA(DisplayName = "Box", ToolTip = "Capsule collision primitive"),
	EIKNOTSET
};


UENUM()
enum EIKinemaCharacterDefinition
{
	ENONE UMETA(DisplayName = "No Definition"),
	EHIPS UMETA(DisplayName = "Hips Bone"),
	ECHEST UMETA(DisplayName = "Chest Bone"),
	EHEAD UMETA(DisplayName = "Head Bone"),
	ESHOULDER UMETA(DisplayName = "Shoulder Bone"),
	EELBOW UMETA(DisplayName = "Elbow Bone"),
	EHAND UMETA(DisplayName = "Hand Bone"),
	EFOOT UMETA(DisplayName = "Foot Bone")
};



USTRUCT()
struct FIKinemaShape
{
	GENERATED_USTRUCT_BODY()

		//The collision Primitive to associate with this bone
		UPROPERTY(EditAnywhere, Category = ShapeDefinition)
		TEnumAsByte<EIKinemaCollisionShape> Shape;

	//The extents of the box in the bone local space
	UPROPERTY(EditAnywhere, Category = ShapeDefinition)
		FVector BoxHalfLengths;

	//The Sphere or Capsule radius
	UPROPERTY(EditAnywhere, Category = ShapeDefinition)
		float Radius;

	//The capsule half length
	UPROPERTY(EditAnywhere, Category = ShapeDefinition)
		float HalfLength;

	//The offset from the joint location to the centre of the primitive, in bone local space
	UPROPERTY(EditAnywhere, Category = ShapeDefinition)
		FTransform LocalOffset;
};


USTRUCT()
struct FLiveActionSelfCollisionTask
{
	GENERATED_USTRUCT_BODY()

		//Create a self collision constraint on this bone
		UPROPERTY(EditAnywhere, Category = SelfCollision, meta = (DisplayName = "Self Collision"))
		bool bIsPenetrating = false;

	//Array of bones to collide with
	UPROPERTY(EditAnywhere, Category = SelfPenetration, meta = (EditCondition = bIsPenetrating, DisplayName = "Bones to collide with"))
		TArray<FName> PenetrationSegments;

	//Offset from joint location to collision shape, in the normal direction
	UPROPERTY(EditAnywhere, Category = SelfPenetration, meta = (EditCondition = bIsPenetrating, DisplayName = "Mesh Offset"))
		float SelfPenetrationMeshOffset;

	//Smoothness of the collision normal.
	//Use this if you see jitter in the data.
	UPROPERTY(EditAnywhere, Category = SelfPenetration, meta = (EditCondition = bIsPenetrating, ClampMin = 0.0, ClampMax = 0.99, DisplayName = "Normal Smoothness"))
		float FilterAlpha = 0.5;

	UPROPERTY(EditAnywhere, Category = "SelfPenetration | Testing", meta = (EditCondition = bIsPenetrating, DisplayName = "Position Depth"))
		int32 PDepth = -1;

	UPROPERTY(EditAnywhere, Category = "SelfPenetration | Testing", meta = (EditCondition = bIsPenetrating, DisplayName = "Rotation Depth"))
		int32 RDepth = -1;


	UPROPERTY(EditAnywhere, Category = "SelfPenetration | Testing", meta = (EditCondition = bIsPenetrating, DisplayName = "Position Weight"))
		FVector PWeight = FVector(10, 10, 10);

	UPROPERTY(EditAnywhere, Category = "SelfPenetration | Testing", meta = (EditCondition = bIsPenetrating, DisplayName = "Rotation Weight"))
		FVector RWeight = FVector(10, 10, 10);

};

USTRUCT()
struct FLiveActionSelfCollision
{
	GENERATED_USTRUCT_BODY()

		//Create an IK constraint for this bone in the Self Penetration Pass
		UPROPERTY(EditAnywhere, Category = SelfCollision, meta = (DisplayName = "Create IK Constraint"))
		bool bIsEnabled = false;

	//Collision Primitive properties
	UPROPERTY(EditAnywhere, Category = ShapeDefinition)
		FIKinemaShape CollisionShape;

	//Self Penetration constraint properties
	UPROPERTY(EditAnywhere, Category = SelfCollision, meta = (EditCondition = bIsEnabled, DisplayName = "Self Penetration Constraint"))
		FLiveActionSelfCollisionTask Task;

};

USTRUCT()
struct FIKinemaLookAtLimits
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, Category = LookAt)
		FRotator Min;

	UPROPERTY(EditAnywhere, Category = LookAt)
		FRotator Max;

};

USTRUCT()
struct FIKinemaEulerLimits
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, Category = Advanced, meta = (ClampMin = "-360", ClampMax = "0.0"))
		FVector MinDegrees;

	UPROPERTY(EditAnywhere, Category = Advanced, meta = (ClampMin = "0.0", ClampMax = "360"))
		FVector MaxDegrees;

	UPROPERTY(EditAnywhere, Category = Advanced)
		TEnumAsByte<enum EIKinemaBoneAxisType> BoneAxis;

	// Set to default values..
	void InitializeValues();
};

USTRUCT()
struct IKINEMACORE_API FIKinemaBoneDef
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(VisibleAnywhere, Category = IKDefinition)
		FName	Name;

	UPROPERTY(VisibleAnywhere, AdvancedDisplay, Category = IKDefinition)
		FName	ParentName;

	//Edited to allow for retargeting
	//Name of the source bone
	UPROPERTY(VisibleAnywhere, AdvancedDisplay, Category = IKDefinition)
		FName SourceName;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		TEnumAsByte<EIKinemaCharacterDefinition> Characterisation;

	//MatchPose between source and target
	UPROPERTY()
		FTransform	MatchPose;

	UPROPERTY()
		int32 SourceIndex;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition, meta = (DisplayName = "Rotation From Source"))
		FQuat	RotFromSrc;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition, meta = (DisplayName = "Rotation From Source Parent"))
		FQuat	RotFromSrcPar;

	FQuat	rotFromSrc;
	FQuat	rotFromSrcPar;

	UPROPERTY(EditAnyWhere, Category = "Animation Cleaning Pipeline")
		bool bFloorLevelCompensation = false;

	UPROPERTY(EditAnyWhere, Category = "Animation Cleaning Pipeline")
		bool bAutoLocking = false;

	UPROPERTY(EditAnyWhere, Category = "Animation Cleaning Pipeline: Floor Level Compensation", meta = (EditCondition = bFloorLevelCompensation))
		float MeshOffset = 0.f;

	UPROPERTY(EditAnyWhere, Category = "Animation Cleaning Pipeline: Floor Level Compensation", meta = (EditCondition = bFloorLevelCompensation))
		bool bOffsetHips = false;

	UPROPERTY(EditAnyWhere, Category = "Animation Cleaning Pipeline: Auto locking", meta = (EditCondition = bAutoLocking))
		bool bIsLocking = false;

	UPROPERTY(EditAnywhere, Category = "Animation Cleaning Pipeline: Auto locking", meta = (DisplayName = "Min Velocity Threshold", EditCondition = bAutoLocking))
		float InThresh = 0.f;

	UPROPERTY(EditAnywhere, Category = "Animation Cleaning Pipeline: Auto locking", meta = (DisplayName = "Max Velocity Threshold", EditCondition = bAutoLocking))
		float OutThresh = 0.f;

	UPROPERTY(EditAnywhere, Category = "Animation Cleaning Pipeline: Auto locking", meta = (DisplayName = "Number of Frames to Blend over", EditCondition = bAutoLocking))
		int32 BlendFrames = 0;

	//Edit FInished

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		bool	DofX;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		bool	DofY;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		bool	DofZ;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		bool	StretchX;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		bool	StretchY;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		bool	StretchZ;

	// Index of the task that drives this bone or INDEX_NONE (reverse lookup of FIKinemaTaskDef::BoneIndex)
	UPROPERTY()
		int32	TaskIndex;

	UPROPERTY(EditAnywhere, Category = "Limits")
		bool	EnableLimits;

	// Index of the parent bone or INDEX_NONE (derived from ParentName)
	UPROPERTY()
		int32	ParentIndex;

	UPROPERTY(EditAnywhere, Category = "Limits", meta = (EditCondition = "EnableLimits"))
		FIKinemaEulerLimits Limits;

	UPROPERTY(EditAnywhere, Category = "Limits", meta = (EditCondition = "EnableLimits"))
		bool	EnforceLimits;

	// One weight value for each bone axis.
	UPROPERTY(EditAnywhere, Category = IKDefinition, meta = (UIMin = 0.0f, UIMax = 1.0f, ClampMin = 0.f, ClampMax = 1.f))
		FVector	Weight;

	// The mass of the bone.
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		float	Mass;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		bool	ResetToRest;


	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		float	MaxVelocity;

	UPROPERTY(EditAnywhere, Category = "Limits", meta = (EditCondition = "EnableLimits"))
		float	LimitsGain;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		bool	Active;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		bool	EnableRetargeting;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		float	RetargetingGain;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		FVector	RetargetingDoFGain;


	UPROPERTY(EditAnywhere, Category = SelfPenetration, meta = (ShowOnlyInnerProperties = true))
		FLiveActionSelfCollision SelfColision;




	UPROPERTY()
		FTransform	RestPose;

	// Set to default values..
	void InitializeValues();

	void PostLoadOrImport();

#if WITH_EDITOR
	void CopyBoneDefPropertiesFrom(FIKinemaBoneDef const* other);

	void CopyBoneDefPropertiesFrom(FIKinemaBoneDef const& other);

	void ResetToDefaults();

#endif

	// Bind to the given skeleton
	bool BindToSkeleton(FIK::ImportBone& importBone, USkeleton& skeleton, int32& ue4BoneIndex, bool upY, USkeletalMeshComponent* component, bool IsSource, bool isRetargeting);
	bool BindToSkeleton(FIK::ImportBone& importBone, USkeleton& skeleton, int32& ue4BoneIndex, bool upY, bool isSource, bool isRetargeting);
	// Configure an IKinema bone segment (inside the solver instance).
	void ConfigureIKSegment(FIK::IKSegment& segment, bool shadowPosing) const;

	// Transfer an IKinema pose to a UE4 pose, for this bone.
	void IKinemaToUE4Pose(const FIK::IKSegment& segment, FTransform& ue4Transform, bool applyTranslation, bool global = false) const;

	FIKinemaBoneDef();
private:

	// Cache ANSI names from the wide versions.
	void CacheANSINames();

	// Cache the ANSI version of the name and parent name.
	static const int32 MAX_NAME_LENGTH = 512;
	ANSICHAR mNameANSI[MAX_NAME_LENGTH];
	ANSICHAR mParentNameANSI[MAX_NAME_LENGTH];
};

USTRUCT()
struct IKINEMACORE_API FIKinemaTaskDef
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(VisibleAnywhere, Category = IKDefinition)
		FName	Name;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		bool	HasPositionTask;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		bool	HasRotationTask;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		bool IsParentConstraint;

	//Retargeting task offset
	UPROPERTY(EditAnywhere, Category = IKDefinition)
		FTransform TaskOffset;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		bool bLockOffset = false;

	UPROPERTY()
		bool	RootAsScalePivot;

	// Index of the FIKinemaBoneDef that is being driven by this task
	UPROPERTY()
		int32	BoneIndex;

	// Index of the parent task or INDEX_NONE (derived by walking up the bone hierarchy to find the first parent that has a corresponding task)
	UPROPERTY()
		int32	ParentIndex;

	UPROPERTY()
		int32 MarkerIndex;

	// Index of the source marker. trim(parentName,1,end) and convert to int
	UPROPERTY()
		FString ParentName;

	//Hold the source skeleton bone name and index to be used with the retargeting node
	UPROPERTY()
		FName	SourceName;

	UPROPERTY()
		int32 SourceIndex;
	//position

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = PositionTask)
		bool PositionAsPoleObject;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = PositionTask)
		bool PositionDofX;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = PositionTask)
		bool PositionDofY;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = PositionTask)
		bool PositionDofZ;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = PositionTask)
		uint32 PositionDepth;

	UPROPERTY(EditAnywhere, Category = PositionTask)
		FVector PositionWeight;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = PositionTask)
		FVector PositionPrecision;

	UPROPERTY()
		uint32 PositionPriority;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = PositionTask)
		FVector TipOffset;


	UPROPERTY()
		FVector ScaleRetargeting;

	UPROPERTY()
		FVector OffsetRetargeting;

	//orientation

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = OrientationTask)
		bool RotateDofX;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = OrientationTask)
		bool RotateDofY;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = OrientationTask)
		bool RotateDofZ;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = OrientationTask)
		uint32 RotateDepth;

	UPROPERTY(EditAnywhere, Category = OrientationTask)
		FVector RotateWeight;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = OrientationTask)
		FVector RotatePrecision;



	UPROPERTY()
		uint32 RotatePriority;

	UPROPERTY(EditAnywhere, Category = "OrientationTask:LookAt", meta = (EditCondition = "HasRotationTask"))
		FIKinemaLookAtLimits LookAtLimits;

	// Set to default values..
	void InitializeValues();

#if WITH_EDITOR
	void CopyTaskDefPropertiesFrom(FIKinemaTaskDef const* other);

	void CopyTaskDefPropertiesFrom(FIKinemaTaskDef const& other);

	void ResetToDefaults();
#endif

	void PostLoadOrImport();

	// Configure a position task using our properties.
	void ConfigurePositionTask(FIK::IKPositionTask& positionTask) const;

	// Configure an orientation task using our properties.
	void ConfigureOrientationTask(FIK::IKOrientationTask& orientationTask) const;
	FIKinemaTaskDef();
};

USTRUCT()
struct FIKinemaForceDef
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(VisibleAnywhere, Category = IKDefinition)
		FName		Name;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		uint32		BoneIndex;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		bool		Support;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		float		SupportRatio;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		FVector		Force;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		FVector		WorldForce;

	// Set to default values..
	void InitializeValues();
};

USTRUCT()
struct IKINEMACORE_API FIKinemaCOMDef
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, Category = IKDefinition, DisplayName = "DoFX")
		bool DegreeOfFreedomX;

	UPROPERTY(EditAnywhere, Category = IKDefinition, DisplayName = "DoFY")
		bool DegreeOfFreedomY;

	UPROPERTY(EditAnywhere, Category = IKDefinition, DisplayName = "DoFZ")
		bool DegreeOfFreedomZ;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		FVector Weight;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		FVector Precision;

	// Set to default values..
	void InitializeValues();

#if WITH_EDITOR
	void CopyTaskDefPropertiesFrom(FIKinemaCOMDef const* other);

	void CopyTaskDefPropertiesFrom(FIKinemaCOMDef const& other);

	void ResetToDefaults();
#endif
};

USTRUCT()
struct FIKinemaSolverDef
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, Category = IKDefinition)
		int32 MaxIterations;

	UPROPERTY(VisibleAnywhere, AdvancedDisplay, Category = IKDefinition)
		FName UpAxis;

	//Edit for Retargeting
	//Set to true if this is a stage 1 solver rig
	//in a retargeting pipeline
	UPROPERTY(VisibleAnywhere, Category = IKDefinition)
		bool IsSource;

	UPROPERTY(VisibleAnywhere, AdvancedDisplay, Category = IKDefinition)
		bool IsRetargeting;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = IKDefinition)
		bool EnableShadowPosing;
	//Edit finished

	UPROPERTY()
		bool ContinuouslySolving;

	UPROPERTY()
		float PoseTolerance;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		float SolutionTolerance;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		float RootTranslationWeight;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		FVector RootTargetTranslation;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		bool TranslateRoot;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		bool RootTranslationDofX;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		bool RootTranslationDofY;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		bool RootTranslationDofZ;

	UPROPERTY()
		bool Retargeting;

	UPROPERTY()
		FVector RetargetingTaskScale;

	UPROPERTY()
		FVector RetargetingTaskOffset;

	UPROPERTY()
		bool EnableRootTargetTranslation;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		bool EnableMoments;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		float MomentsWeight;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		float MomentsPriority;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		float FigureMass;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		bool UseDefaultZMP;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		FVector ZeroMomentPoint;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		float TasksPrecision;

	UPROPERTY()
		bool RescaleTasks;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		float LimitsGain;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		float RetargetingGain;

	UPROPERTY()
		float SecondaryTaskWeight;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		float SecondaryTaskPrecision;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		float PCoefficient;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		bool ActiveCOM;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		float SourceScale;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
		float ImportScale;

	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		struct FIKinemaCOMDef COM;

	UPROPERTY(EditFixedSize, EditAnywhere, Category = IKDefinition)
		TArray<struct FIKinemaBoneDef> Bones;

	UPROPERTY(EditFixedSize, EditAnywhere, Category = IKDefinition)
		TArray<struct FIKinemaTaskDef> Tasks;

	UPROPERTY(EditFixedSize, EditAnywhere, Category = IKDefinition)
		TArray<struct FIKinemaForceDef> Forces;




	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Advanced)
		bool bCombineWithAnimation;
	// Reset to default values.
	void InitializeValues();

	// Configure the given solver using our properties.
	bool ConfigureSolver(class FIKinemaSolverInstance& solverInstance) const;

	bool ApplySettings(class FIKinemaSolverInstance& solverInstance) const;

	// Called from the outer rig class.
	void PostLoadOrImport();

	// Find the index of the task with the given name in our list.
	// Returns INDEX_NONE if not found.
	IKINEMACORE_API int32 FindTaskIndex(const FName& taskName) const;

	// Find the index of the bone with the given name in our list.
	// Returns INDEX_NONE if not found.
	IKINEMACORE_API int32 FindBoneIndex(const FName& boneName) const;


	// Gets the index of the task driving the bone referenced by boneIndex.
	// Returns INDEX_NONE if boneIndex is invalid or if bone is not being driven.
	IKINEMACORE_API int32 GetTaskIndexFromBone(int32 boneIndex) const;

	// Gets the index of the bone being driven by the task referenced by taskIndex.
	// Returns INDEX_NONE if taskIndex is invalid.
	IKINEMACORE_API int32 GetBoneIndexFromTask(int32 taskIndex) const;
};

USTRUCT(BlueprintType)
struct IKINEMACORE_API FIKinemaSolverTask
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(BlueprintReadWrite, Category = Links)
		FTransform Target;
	UPROPERTY(BlueprintReadWrite, Category = Links)
		bool EnableTranslation;
	UPROPERTY(BlueprintReadWrite, Category = Links)
		bool EnableOrientation;
	UPROPERTY(BlueprintReadWrite, Category = Links)
		bool DebugDraw;
	UPROPERTY(BlueprintReadWrite, Category = Links)
		float	DebugDrawScale;
	//Used with retargeting tasks
	UPROPERTY(BlueprintReadWrite, Category = Links)
		FTransform Offset;
	UPROPERTY(BlueprintReadWrite, Category = Links)
		bool UseAsOffset;

	FIKinemaSolverTask();
};



USTRUCT(BlueprintType)
struct IKINEMACORE_API FIKinemaTaskProperties
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		bool PositionDofX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		bool PositionDofY;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		bool PositionDofZ;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		int32 PositionDepth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		FVector PositionWeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		FVector PositionPrecision;

	//orientation

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		bool RotateDofX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		bool RotateDofY;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		bool RotateDofZ;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		int32 RotateDepth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		FVector RotateWeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		FVector RotatePrecision;

	FIKinemaTaskProperties();
};

USTRUCT(BlueprintType)
struct IKINEMACORE_API FIKinemaSegmentProperties
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		bool	DofX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		bool	DofY;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		bool	DofZ;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		bool	EnforceLimits;

	// One weight value for each bone axis.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		FVector	Weight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		bool	EnableLimits;


	UPROPERTY(EditAnywhere, Category = "Limits", meta = (EditCondition = "EnableLimits"))
		float	LimitsGain;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		bool	EnableRetargeting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
		float	RetargetingGain;

	FIKinemaSegmentProperties();
};

USTRUCT()
struct IKINEMACORE_API FIKinemaTaskOverride
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, Category = Links)
		FName	TaskName;

	UPROPERTY(EditAnywhere, Category = Links)
		bool	ShowAsPin;
};

USTRUCT()
struct IKINEMACORE_API FIKinemaSegmentOverride
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, Category = Links)
		FName	BoneName;

	UPROPERTY(EditAnywhere, Category = Links)
		bool	ShowAsPin;
};

/*
Create an IKinema LiveAction Rig
*/
UCLASS(hidecategories = Object, MinimalAPI, BlueprintType)
class UIKinemaRig : public UObject
{
	GENERATED_UCLASS_BODY()

		//#if WITH_EDITORONLY_DATA
		// File containing a text description of this rig.
		// Not used from in-game runtime.
		UPROPERTY()
		FString ImportPath;
	//#endif //WITH_EDITORONLY_DATA

	UPROPERTY(EditAnywhere, Category = Skeleton)
		USkeleton* Skeleton;

	UPROPERTY()
		bool UseParentConstraint;

	int32	UpdateVersion;
	// The solver definition.
	UPROPERTY(EditAnywhere, Category = IKDefinition, meta = (ShowOnlyInnerProperties = true))
		FIKinemaSolverDef SolverDef;

	//For Retargeting to Enable/Disable Autotune
	bool EnableAutoTune;

	// Called just before re-importing properties from text.
	IKINEMACORE_API void PreReimport();

	// To do post-processing on loaded properties.
	virtual void PostLoad() override;

	// Called from the above, and from import.
	IKINEMACORE_API void PostLoadOrImport();

	void PostDuplicate(bool bDuplicateForPIE) override;

	UPROPERTY()
	UActorSkeleton* Actor;

	UPROPERTY()
	bool IsRigidBody;

	
	void SolveAndUpdatePose(class FIKinemaSolverInstance& solverInstance, FPoseContext& Output);

	void SolveAndUpdatePose(class FIKinemaSolverInstance& solverInstance, FCompactPose& Output);

#if WITH_EDITOR
	void PostEditChangeChainProperty( struct FPropertyChangedChainEvent& PropertyChangedEvent ) override;

	IKINEMACORE_API bool CreateRigFromSkeleton();

	IKINEMACORE_API void LinkSourceToTarget(const FMocapBone& sourceBone, FIKinemaBoneDef& targetBone, const FTransform& sourceGlobal, const FTransform& sourceParentGlobal);

	IKINEMACORE_API void CharacteriseBone(FName BoneName, EIKinemaCharacterDefinition Profile);

	IKINEMACORE_API void HandleSelfPenetrationChange(FIKinemaBoneDef& targetBone);
#endif
private:
	bool isInitialise;

	// Transfer an IKinema pose to UE4.
	void IKinemaToUE4Pose(class FIKinemaSolverInstance& solverInstance, FPoseContext& Output) const;


	void IKinemaToUE4Pose(FIKinemaSolverInstance& solverInstance, FCompactPose& Output) const;

	//void SetAsset(UIKinemaRig* rig);
public:
	FIK::ACP::ACPRig CreateACPRigFromRig(const USkeleton* Skeleton, const USkeletalMeshComponent* SkelComp, TArray<int32>& ue4Index) const;
	// Create an IKinema solver instance using this rig.
	void CreateSolver(class FIKinemaSolverInstance& solverInstance) const;

protected:
	// Keep the binding here.
	//FIKinemaRigToSkeletonBinding mSkeletonBinding;

	// The IKinema solver instance data (solver and tasks).
	//FIKinemaSolverInstance * mSolverInstance;

	//UPROPERTY(EditFixedSize, EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinHiddenByDefault))
	//	TArray<FIKinemaSolverTask> Tasks;
	//
	//
	//UPROPERTY(EditFixedSize, EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinHiddenByDefault))
	//	TArray<FIKinemaTaskProperties> TaskProperties;
	//
	//
	//UPROPERTY(EditFixedSize, EditAnywhere, Category = Links)
	//	TArray<FIKinemaTaskOverride> PinTaskProperties;
	//
	//UPROPERTY(EditFixedSize, EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinHiddenByDefault))
	//	TArray<FIKinemaSegmentProperties> SegmentProperties;
	//
	//
	//UPROPERTY(EditFixedSize, EditAnywhere, Category = Links)
	//	TArray<FIKinemaSegmentOverride> PinSegmentProperties;
	//
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinShownByDefault))
	//	bool DebugDraw;

};

// Represents a binding of an IKinemaRig to a specific skeleton.
// Resolves UE4 bone indices and stores them.
class IKINEMACORE_API FIKinemaRigToSkeletonBinding
{
public:

	// Default constructor.
	FIKinemaRigToSkeletonBinding();

	// Bind the given IKinemaRig to the given skeleton and store the result.
	// Since the member properties are not stored on file, this should be called
	// from runtime each time, to set up the members.
	bool BindToSkeleton(UIKinemaRig* ikinemaRig, USkeleton* skeleton, USkeletalMeshComponent* component);
	bool BindToSkeleton(UIKinemaRig* ikinemaRig, USkeleton* skeleton);
	// Configure the given solver using our cached data.
	void ConfigureSolver(FIK::IKSolver& solver) const;

	// Access the UE4 bone index given the bone def index.
	int32 GetUE4BoneIndex(int32 boneDefIndex) const;

	//int32 GetRootBoneIndex() const;

	// Access the IKinema rig.
	const UIKinemaRig* GetRig() const;

	UIKinemaRig* GetRig();

	const USkeleton* GetSkeleton() const;

	const USkeletalMeshComponent* GetSkeletalMeshComponent() const;

	void ClearImportBones();

	bool IsValid();

private:

	// Remember the rig.
	UIKinemaRig* mRig;

	USkeleton* mSkeleton;

	USkeletalMeshComponent* mComponent;

	// Array of bones (IKinema segments) to directly import into the solver.
	TArray<FIK::ImportBone> ImportBones;

	// Cache the UE4 bone indices corresponding to IKinema segments.
	TArray<int32> UE4BoneIndices;
	//int32 RootBoneIndex; 
};

// This represents a task instance on a segment/joint/bone.
struct FIKinemaJointTaskInstance
{
	// Either of these could be null.
	FIK::IKPositionTask* mPositionTask;
	FIK::IKOrientationTask* mOrientationTask;

	// Default constructor.
	FIKinemaJointTaskInstance();

	// Utility methods to configure the tasks.

	void SetPositionWeight(const FVector& weight);
	void SetOrientationWeight(const FVector& weight);
	void SetPositionTarget(const FVector& positionTarget);
	void SetOrientationTarget(const FQuat& orientationTarget);
	void SetPositionEnabled(bool state);
	void SetOrientationEnabled(bool state);
	void ApplyPositionProperties(const FIKinemaTaskProperties& properties);
	void ApplyOrientationProperties(const FIKinemaTaskProperties& properties);
};

// This represents an instance of an IKinema solver
// along with the tasks on it.
class IKINEMACORE_API FIKinemaSolverInstance
{
private:

	// Binding to a skeleton.
	FIKinemaRigToSkeletonBinding* mSkeletonBinding;

	// The array of tasks that were created on joints/segments.
	TArray<struct FIKinemaJointTaskInstance> mJointTasks;

	// The IKinema solver.
	FIK::IKSolver* mIKSolver;

	FTransform mSolverToComponentSpace;
	FTransform mSolverToWorldSpace;

public:

	unsigned int InstID;
	// Default constructor.
	FIKinemaSolverInstance();

	// Destructor.
	~FIKinemaSolverInstance();

	void Dump(const FString& Folder);

	// Create using the given skeleton binding.
	void Create(FIKinemaRigToSkeletonBinding& skeletonBinding);

	void Create(UIKinemaRig * skeletonBinding);

	// Destroy this solver instance.
	void Destroy();

	void Reset();

	bool UpdateAndSolve(
		const TArray<FIKinemaSolverTask>& tasks,
		const TArray<FIKinemaTaskProperties>& taskProperties,
		const TArray<FIKinemaTaskOverride>& taskOverride,
		const TArray<FIKinemaSegmentProperties>& segProperties,
		const TArray<FIKinemaSegmentOverride>& segOverride,
		TArray<FTransform>& inPose,
		FPoseContext& Output,
		const FTransform& solverToWorld = FTransform::Identity, bool DebugDraw = false);

	bool UpdateAndSolve(TArray<FIKinemaSolverTask>& tasks,
		const TArray<FIKinemaTaskProperties>& taskProperties,
		const TArray<FIKinemaTaskOverride>& taskOverride,
		const TArray<FIKinemaSegmentProperties>& segProperties,
		const TArray<FIKinemaSegmentOverride>& segOverride,
		FPoseContext& Output,
		bool DebugDraw = false);

	bool UpdateAndSolve(
		const TArray<FIKinemaSolverTask>& tasks,
		const TArray<FIKinemaTaskProperties>& taskProperties,
		const TArray<FIKinemaTaskOverride>& taskOverride,
		const TArray<FIKinemaSegmentProperties>& segProperties,
		const TArray<FIKinemaSegmentOverride>& segOverride,
		TArray<FTransform>& inPose,
		FCompactPose& Output,
		const FTransform& solverToWorld = FTransform::Identity, bool DebugDraw = false);

	

	// Access the skeleton binding.
	const FIKinemaRigToSkeletonBinding* GetSkeletonBinding() const;

private:

	bool UpdateTaskInstances(const TArray<FIKinemaSolverTask>& tasks, const TArray<FIKinemaTaskProperties>& taskProperties, const TArray<FIKinemaTaskOverride>& taskOverride);

	friend class UIKinemaRig;
	friend struct FIKinemaSolverDef;
};

