/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "IKinemaRig.h"
#include "Animation/AnimNodeBase.h"
#include "AnimNode_IKinemaSelfPenetration.generated.h"


USTRUCT()
struct FIKinemaSelfPenetrationTask
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FBoneReference BoneRef;

	UPROPERTY()
	int ue4Index;

	UPROPERTY()
	float MeshOffset;

	UPROPERTY()
	TArray<FName> PenetrationSegments;

	bool bPenetrating;

	UPROPERTY()
	float FilterAlpha;


	UPROPERTY()
		int32 PDepth = -1;

	UPROPERTY()
		int32 RDepth = -1;


	UPROPERTY()
		FVector PWeight = FVector(10, 10, 10);

	UPROPERTY()
		FVector RWeight = FVector(10, 10, 10);


	FIK::IKPositionTask* m_Pos = nullptr;
	FIK::IKOrientationTask* m_Rot = nullptr;

	FIKinemaSelfPenetrationTask();

	FIKinemaSelfPenetrationTask(const FIKinemaSelfPenetrationTask& other);
	FIKinemaSelfPenetrationTask& operator = (const FIKinemaSelfPenetrationTask& other);


	bool bIsCreated = false;
	bool createTask(FIK::IKSolver* solver);
	void SetTaskTarget(FCSPose<FCompactPose>&  MeshBases, USkeletalMeshComponent* SkelComp, FBoneReference& Root, float Alpha);
};

USTRUCT(BlueprintType)
struct IKINEMACORE_API FAnimNode_IKinemaSelfPenetration : public FAnimNode_Base
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
	FPoseLink InPose;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SolverTuning, meta = (PinHiddenByDefault))
	bool AutoTune;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SolverTuning, meta = (PinHiddenByDefault, DisplayName = "Pull Gain"))
	float pCoeff;

	// This is set by the anim graph node, so don't allow further edits.
	UPROPERTY()
	class UIKinemaRig* IKinemaRig;

	UPROPERTY()
		FBoneReference IKinemaRoot;

	//// The targets for the IKinema solver to try and achieve.
	//UPROPERTY(EditFixedSize, EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinHiddenByDefault))
	//TArray<FIKinemaSolverTask> Tasks;


	//UPROPERTY(EditFixedSize, EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinHiddenByDefault))
	//TArray<FIKinemaTaskProperties> TaskProperties;

	//
	//UPROPERTY(EditFixedSize, EditAnywhere, Category = Links)
	//TArray<FIKinemaTaskOverride> PinTaskProperties;

	//UPROPERTY(EditFixedSize, EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinHiddenByDefault))
	//TArray<FIKinemaSegmentProperties> SegmentProperties;


	//UPROPERTY(EditFixedSize, EditAnywhere, Category = Links)
	//TArray<FIKinemaSegmentOverride> PinSegmentProperties;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinShownByDefault))
	bool DebugDraw;



public:
	FAnimNode_IKinemaSelfPenetration();
	~FAnimNode_IKinemaSelfPenetration();

	virtual void SetAsset(class UIKinemaRig* rig);
	virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	virtual void Update_AnyThread(const FAnimationUpdateContext& Context) override;
	virtual void Evaluate_AnyThread(FPoseContext& Output) override;
	virtual void CacheBones_AnyThread(const FAnimationCacheBonesContext & Context) override;

protected:

	// Copied from FAnimNode_SkeletalControlBase interface
	bool IsValidToEvaluate(const USkeleton * Skeleton, const FBoneContainer & RequiredBones);
	void InitializeBoneReferences(const FBoneContainer & RequiredBones);
	// End ofCopied from FAnimNode_SkeletalControlBase interface

	void ClearSolver();

	void CreateTasks();
	void ApplyAndUpdateSolverSettings();
	void CreateSolver(const USkeleton* Skeleton, const USkeletalMeshComponent* SkelComp);

	int32	UpdateVersion;
	TSharedPtr<FIK::IKSolver> m_Solver;

	TArray<FIKinemaSelfPenetrationTask> Tasks;
	FIKinemaSelfPenetrationTask m_HipsTask;



	TArray<int32> ue4Index;
};
