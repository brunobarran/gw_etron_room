/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "IKinemaRig.h"
#include "LiveLinkRetargetAsset.h"
#include "IKinemaLiveLinkRetarget.generated.h"

UCLASS(Blueprintable)
class IKINEMACORE_API UIKinemaLiveLinkRetarget : public ULiveLinkRetargetAsset
{
	GENERATED_BODY()


		bool AutoTune;
		float pCoeff;

	// This is set by the anim graph node, so don't allow further edits.

		

	// The targets for the IKinema solver to try and achieve.
		TArray<FIKinemaSolverTask> Tasks;

		TArray<FIKinemaTaskProperties> TaskProperties;

		TArray<FIKinemaTaskOverride> PinTaskProperties;

		TArray<FIKinemaSegmentProperties> SegmentProperties;

		TArray<FIKinemaSegmentOverride> PinSegmentProperties;



		bool Initialized;

		TArray<FTransform> Pose;


public:
	UIKinemaLiveLinkRetarget();
	~UIKinemaLiveLinkRetarget();

	virtual void SetAsset(class UIKinemaRig* rig);
	virtual void BindSourceToTarget(const FLiveLinkSubjectFrame& vim);

	virtual void BuildPoseForSubject(float DeltaTime, const FLiveLinkSubjectFrame& InFrame, FCompactPose& OutPose, FBlendedCurve& OutCurve) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = IKinema, meta = (PinShownByDefault))
	class UIKinemaRig* IKinemaRig;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = IKinema, meta = (PinShownByDefault))
	bool DebugDraw;

protected:

	// Keep the binding here.
	FIKinemaRigToSkeletonBinding mSkeletonBinding;

	// The IKinema solver instance data (solver and tasks).
	FIKinemaSolverInstance mSolverInstance;

	int32	UpdateVersion;
	bool IsRigBound;
	// Ensures the specified object is preloaded.  ReferencedObject can be NULL.
	void PreloadObject(UObject* ReferencedObject)
	{
		if ((ReferencedObject != NULL) && ReferencedObject->HasAnyFlags(RF_NeedLoad))
		{
			ReferencedObject->GetLinker()->Preload(ReferencedObject);
		}
	}
};
