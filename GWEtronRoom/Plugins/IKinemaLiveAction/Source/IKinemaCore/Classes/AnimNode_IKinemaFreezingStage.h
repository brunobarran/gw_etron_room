/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "IKinemaRig.h"
#include "Animation/AnimNodeBase.h"
#include "AnimNode_IKinemaFreezingStage.generated.h"


USTRUCT(BlueprintType)
struct IKINEMACORE_API FAnimNode_IKinemaFreezingStage : public FAnimNode_Base
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
	FPoseLink InPose;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SolverTuning, meta = (PinHiddenByDefault, DisplayName = "Max Iterations"))
	float iter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Debug, meta = (PinShownByDefault))
	bool DebugDraw;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Debug, meta = (PinShownByDefault))
	FColor DebugColor;

	// This is set by the anim graph node. Don't allow further edits.
	UPROPERTY()
	class UIKinemaRig* IKinemaRig;
public:
	FAnimNode_IKinemaFreezingStage();
	~FAnimNode_IKinemaFreezingStage();

	void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	void Update_AnyThread(const FAnimationUpdateContext& Context) override;
	void Evaluate_AnyThread(FPoseContext& Output) override;
	void CacheBones_AnyThread(const FAnimationCacheBonesContext & Context) override;

private:
	/*
	 * Begin LiveAction ACP Node Interface
	 */
	void ClearSolver();
	void CreateSolver(const USkeleton* Skeleton, const USkeletalMeshComponent* SkelComp);

	FIK::AnimNode<FIK::ACP::ACPRig>* Node = nullptr;

	bool IsLicValid;
	TArray<int32> ue4Index;	
	int32 UpdateVersion;
	/*
	 * End LiveAction ACP Node Interface
	 */
};