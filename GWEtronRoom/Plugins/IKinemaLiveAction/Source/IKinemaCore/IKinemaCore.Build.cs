using UnrealBuildTool;
using System;
using System.IO;
public class IKinemaCore : ModuleRules
{
	public IKinemaCore(ReadOnlyTargetRules Target) : base(Target)
    {

        //we are going to be building without PCH's 
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
        //ensure IWYU is enforeced for potentially improved compile times
        //bEnforceIWYU = true;
       // bFasterWithoutUnity = true;

        //Currently only Win64 is supported
        if ((Target.Platform == UnrealTargetPlatform.Win64))
        {
   //         PublicIncludePaths.AddRange(new string[]{
			//"IKinemaCore/Classes",
			//"IKinemaCore/Public",
   //         });
            PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "IKinema","IKinemaACP", "LADataStreamCore", "LiveLink", "LiveLinkInterface" });
            PrivateDependencyModuleNames.Add("Projects");
            if (Target.Type == TargetRules.TargetType.Editor)
            {
                PublicDependencyModuleNames.AddRange(new string[] { "AssetTools" });
            }
        }
	}
}
