/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaRig.h"
#include "IKinemaCorePrivatePCH.h"
#include "Stats/Stats.h"
#include "IKinemaStatsHelper.h"
#include "PreviewScene.h"
#include "AnimNode.h"
#include "ACP/ACPRig.h"
#include "span.h"
#include "LiveLinkClient.h"

#if WITH_EDITOR
#include "IAssetTools.h"
#include "AssetToolsModule.h"
#define LOCTEXT_NAMESPACE "IKinemaCore"
#endif

void AutoTuneSolver(FIK::IKSolver* Solver, int32 MaxIter, float& PCoef)
{
	static const float MIN_P = 0.01;
	FIK::Real cp;
	cp = Solver->getP();
	PCoef = cp;
	if (cp > MIN_P)
	{
		if (!Solver->solve(MaxIter))
		{
			while (!Solver->solve(MaxIter) && cp >= MIN_P)
			{
				cp *= 0.8f;
				Solver->recomputeScale();
				Solver->setup();
				Solver->setP(cp);
				PCoef = cp;

			};
			Solver->resetSolver();
			Solver->solve(MaxIter);
		}
	}
	else
	{
		Solver->setP(5.f*cp);
		Solver->resetSolver();
		Solver->setup();
		Solver->solve(MaxIter);
		Solver->resetSolver();
		UE_LOG(LogIKinemaCore, Error, TEXT("IKinema Node: Autotune failed. with P gain: %f"), cp);
	}

}

void DebugDrawIKinema(FIK::IKSolver* Solver, FTransform const& mSolverToWorldSpace, FColor const& color, UWorld* World)
{
	if (!Solver || !World)
	{
		return;
	}
	for (unsigned int i = 0; i < Solver->numSegments(); i++)
	{
		const FIK::IKSegment* segment = Solver->getSegmentByHandle(i);
		check(segment != nullptr);
		if (!segment->Active())
		{
			continue;
		}

		FTransform Target;
		const FIK::Real *pos = segment->getGlobalPos();
		FVector trans;
		trans.X = pos[0];
		trans.Y = pos[1];
		trans.Z = pos[2];
		Target.SetTranslation(trans);

		const FIK::Real *quat = segment->getGlobalQ();
		FQuat rot;
		rot.X = quat[0];
		rot.Y = quat[1];
		rot.Z = quat[2];
		rot.W = quat[3];
		Target.SetRotation(rot);

		if (segment->Parent())
		{
			const FIK::Real *ppos = segment->Parent()->getGlobalPos();

			trans.X = ppos[0];
			trans.Y = ppos[1];
			trans.Z = ppos[2];
		}
		auto shape = segment->GetCollisionShape();

		FIK::Transform t = segment->getTransform();
		FVector normal, closest;
		if (shape)
		{
			FIK::Transform worldTransform = t * shape->GetLocalOffset();
			/*FIK::Transform localOffset =
			worldTransform.mTrans = t.mTrans + t.mRotQuat*localOffset.mTrans;
			worldTransform.mRotQuat = t.mRotQuat*localOffset.mRotQuat;
			worldTransform.mRot = worldTransform.mRotQuat.toMatrixTrans();*/

			FTransform GlobalRB;
			const auto& Translation = worldTransform.GetTranslation();
			const auto& Rot = worldTransform.GetRotation();
			GlobalRB.SetTranslation(FVector(Translation[0], Translation[1], Translation[2]));
			GlobalRB.SetRotation(FQuat(Rot[0], Rot[1], Rot[2], Rot[3]));

			normal[0] = shape->GetContactNormal()[0];
			normal[1] = shape->GetContactNormal()[1];
			normal[2] = shape->GetContactNormal()[2];

			closest[0] = shape->GetContactPoint()[0];
			closest[1] = shape->GetContactPoint()[1];
			closest[2] = shape->GetContactPoint()[2];

			ExecOnGameThread([World, mSolverToWorldSpace, normal, closest]() {
				::DrawDebugSphere(World, mSolverToWorldSpace.TransformPosition(closest), 2.f, 12, FColor::Black);
				::DrawDebugLine(World, mSolverToWorldSpace.TransformPosition(closest), mSolverToWorldSpace.TransformPosition(closest) + mSolverToWorldSpace.GetRotation()*normal * 100, FColor::Black);
			});

			if (shape->GetType() == FIK::IKSphere)
			{
				auto cap = static_cast<FIK::IKSphereShape*>(shape);
				auto radius = cap->GetRadius();


				ExecOnGameThread(
					[World, mSolverToWorldSpace, GlobalRB, normal, closest, radius]() {

					::DrawDebugSphere(World, mSolverToWorldSpace.TransformPosition(GlobalRB.GetTranslation()), radius, 12, FColor::Red);
				});
			}


			else if (shape->GetType() == FIK::IKBox)
			{
				auto cap = static_cast<FIK::IKBoxShape*>(shape);
				auto radius = FVector(cap->GetHalfExtent()[0], cap->GetHalfExtent()[1], cap->GetHalfExtent()[2]);


				ExecOnGameThread(
					[World, mSolverToWorldSpace, GlobalRB, radius]() {

					::DrawDebugBox(World, mSolverToWorldSpace.TransformPosition(GlobalRB.GetTranslation()), (GlobalRB*mSolverToWorldSpace).GetRotation().RotateVector(radius), FColor::Red);
				});
			}

			else if (shape->GetType() == FIK::IKCapsule)
			{
				auto cap = static_cast<FIK::IKCapsuleShape*>(shape);
				auto radius = cap->GetRadius();
				auto halfHeight = cap->GetHalfHeight();

				cap->GetLine(t);
				ExecOnGameThread(
					[World, mSolverToWorldSpace, GlobalRB, radius, halfHeight]() {
					::DrawDebugCapsule(World, mSolverToWorldSpace.TransformPosition(GlobalRB.GetTranslation()), halfHeight, radius, mSolverToWorldSpace.GetRotation() *GlobalRB.GetRotation() * FQuat(FVector(0, 1, 0), FMath::DegreesToRadians(90)), FColor::Black);
				});

			}

		}

	}
}

// FOR DEBUGGING ONLY!
#define IKINEMA_SOLVER_DUMP_SUPPORTED 0

// Fill an IKinema EulerLimits structure from FIKinemaEulerLimits.
void RigLimitsToEulerLimits(const FIKinemaEulerLimits& rigLimits, FIK::EulerLimits& limits)
{
	limits.rot[0][0] = FMath::Min<float>(-rigLimits.MinDegrees.X + KINDA_SMALL_NUMBER, -rigLimits.MaxDegrees.X);
	limits.rot[1][0] = FMath::Min<float>(-rigLimits.MinDegrees.Y + KINDA_SMALL_NUMBER, -rigLimits.MaxDegrees.Y);
	limits.rot[2][0] = FMath::Min<float>(-rigLimits.MinDegrees.Z + KINDA_SMALL_NUMBER, -rigLimits.MaxDegrees.Z);

	// Just in case, make sure that MaxDegrees is greater than MinDegrees.
	limits.rot[0][1] = FMath::Max<float>(-rigLimits.MinDegrees.X + KINDA_SMALL_NUMBER, -rigLimits.MaxDegrees.X);
	limits.rot[1][1] = FMath::Max<float>(-rigLimits.MinDegrees.Y + KINDA_SMALL_NUMBER, -rigLimits.MaxDegrees.Y);
	limits.rot[2][1] = FMath::Max<float>(-rigLimits.MinDegrees.Z + KINDA_SMALL_NUMBER, -rigLimits.MaxDegrees.Z);
	limits.bone_axis = rigLimits.BoneAxis;
}

// Convert between Maya-like space (left-handed, Y-down) and Maya space (right-handed, Y-up).
static void ConvertTransformToFromMaya(FTransform& Transform, bool convertTranslation = true)
{
	FVector Translation = Transform.GetTranslation();
	FQuat Rotation = Transform.GetRotation();

	Rotation.X = -Rotation.X;
	Rotation.Z = -Rotation.Z;
	Transform.SetRotation(Rotation);

	if (convertTranslation)
	{
		//Translation.X = -Translation.X;
		Translation.Y = -Translation.Y;
		Transform.SetTranslation(Translation);
	}
}

void SolveAndUpdateACPPose(FIK::AnimNode<FIK::ACP::ACPRig>* acpNode, const TArray<int32>& ACPToUE4, FPoseContext& Output)
{
	//get source joint data and pass as retargeting
	TArray<FIK::Transform> pose;
	pose.Reserve(ACPToUE4.Num());
	for (int32 i = 0; i < ACPToUE4.Num(); i++)
	{
		auto boneIndex = Output.Pose.GetBoneContainer().MakeCompactPoseIndex(FMeshPoseBoneIndex(ACPToUE4[i]));
		if (boneIndex.GetInt() == INDEX_NONE)
		{
			continue;
		}
		const FTransform& trans = Output.Pose[boneIndex];

		auto Trans = trans.GetTranslation();
		auto Rot = trans.GetRotation();
		FIK::Transform xForm({ Rot.X, Rot.Y, Rot.Z, Rot.W }, { Trans.X, Trans.Y, Trans.Z });
		pose.Push(xForm);
	}

	IKinema::span<FIK::Transform> Pose(&pose[0], pose.Num());
	acpNode->Solve(Pose);

	for (int32 i = 0; i < ACPToUE4.Num(); i++)
	{
		auto boneIndex = Output.Pose.GetBoneContainer().MakeCompactPoseIndex(FMeshPoseBoneIndex(ACPToUE4[i]));
		if (boneIndex.GetInt() == INDEX_NONE)
		{
			continue;
		}


		const FIK::Transform& xForm = Pose[i];

		FTransform& ue4Transform = Output.Pose[boneIndex];

		FVector trans;
		trans.X = xForm.GetTranslation()[0];
		trans.Y = xForm.GetTranslation()[1];
		trans.Z = xForm.GetTranslation()[2];
		ue4Transform.SetTranslation(trans);

		FQuat rot;
		rot.X = xForm.GetRotation()[0];
		rot.Y = xForm.GetRotation()[1];
		rot.Z = xForm.GetRotation()[2];
		rot.W = xForm.GetRotation()[3];
		ue4Transform.SetRotation(rot);
		ue4Transform.NormalizeRotation();
	}
}

// struct FIKinemaEulerLimits.
void FIKinemaEulerLimits::InitializeValues()
{
	MinDegrees = FVector(-360.f, -360.f, -360.f);
	MaxDegrees = FVector(360.f, 360.f, 360.f);
	BoneAxis = EIKBAT_X;
}

FIKinemaBoneDef::FIKinemaBoneDef()
{
	SourceIndex = INDEX_NONE;
	RetargetingDoFGain = FVector(1.f);
}

// struct FIKinemaBoneDef.
void FIKinemaBoneDef::InitializeValues()
{
	DofX = true;
	DofY = true;
	DofZ = true;

	StretchX = false;
	StretchY = false;
	StretchZ = false;

	TaskIndex = INDEX_NONE;
	ParentIndex = INDEX_NONE;
	SourceIndex = INDEX_NONE;

	EnableRetargeting = true;
	RetargetingGain = 1.0f;
	Mass = 0.f;
	ResetToRest = false;
	EnableLimits = false;
	EnforceLimits = false;
	Weight = FVector(1.f, 1.f, 1.f);
	MaxVelocity = 0.03f;
	LimitsGain = 1.0f;
	Active = true;

	Limits.InitializeValues();

	FGenericPlatformMemory::Memzero(mNameANSI, MAX_NAME_LENGTH);
	FGenericPlatformMemory::Memzero(mParentNameANSI, MAX_NAME_LENGTH);
	mNameANSI[0] = '\0';
	mParentNameANSI[0] = '\0';
	RetargetingDoFGain[0] = RetargetingDoFGain[1] = RetargetingDoFGain[2] = 1.0f;
	RestPose.SetIdentity();

	SourceIndex = -1;
	RotFromSrc = FQuat::Identity;
	RotFromSrcPar = FQuat::Identity;
	MatchPose.SetIdentity();
	SourceName = NAME_None;
	ParentName = NAME_None;
	Name = NAME_None;
}

#if WITH_EDITOR
void FIKinemaBoneDef::CopyBoneDefPropertiesFrom(FIKinemaBoneDef const* other)
{
	if (other)
	{
		DofX = other->DofX;
		DofY = other->DofY;
		DofZ = other->DofZ;
		Limits = other->Limits;
		EnforceLimits = other->EnforceLimits;
		Weight = other->Weight;
		Mass = other->Mass;
		ResetToRest = other->ResetToRest;
		EnableLimits = other->EnableLimits;
		MaxVelocity = other->MaxVelocity;
		LimitsGain = other->LimitsGain;
		Active = other->Active;
		EnableRetargeting = other->EnableRetargeting;
		RetargetingGain = other->RetargetingGain;
		RetargetingDoFGain = other->RetargetingDoFGain;
		RestPose = other->RestPose;
		RotFromSrc = other->RotFromSrc;
		RotFromSrcPar = other->RotFromSrcPar;
		SourceName = other->SourceName;
		SourceIndex = other->SourceIndex;

		Characterisation = other->Characterisation;


		bFloorLevelCompensation = other->bFloorLevelCompensation;
		bAutoLocking = other->bAutoLocking;
		MeshOffset = other->MeshOffset;
		bIsLocking = other->bIsLocking;
		InThresh = other->InThresh;
		OutThresh = other->OutThresh;
		BlendFrames = other->BlendFrames;
		bOffsetHips = other->bOffsetHips;
		SelfColision = other->SelfColision;
	}
}

void FIKinemaBoneDef::CopyBoneDefPropertiesFrom(FIKinemaBoneDef const& other)
{
	DofX = other.DofX;
	DofY = other.DofY;
	DofZ = other.DofZ;
	Limits = other.Limits;
	EnforceLimits = other.EnforceLimits;
	Weight = other.Weight;
	Mass = other.Mass;
	ResetToRest = other.ResetToRest;
	EnableLimits = other.EnableLimits;
	MaxVelocity = other.MaxVelocity;
	LimitsGain = other.LimitsGain;
	Active = other.Active;
	EnableRetargeting = other.EnableRetargeting;
	RetargetingGain = other.RetargetingGain;
	RestPose = other.RestPose;
	RetargetingDoFGain = other.RetargetingDoFGain;
	RotFromSrc = other.RotFromSrc;
	RotFromSrcPar = other.RotFromSrcPar;
	SourceName = other.SourceName;
	SourceIndex = other.SourceIndex;

	Characterisation = other.Characterisation;

	bFloorLevelCompensation = other.bFloorLevelCompensation;
	bAutoLocking = other.bAutoLocking;
	MeshOffset = other.MeshOffset;
	bIsLocking = other.bIsLocking;
	InThresh = other.InThresh;
	OutThresh = other.OutThresh;
	BlendFrames = other.BlendFrames;
	bOffsetHips = other.bOffsetHips;
	SelfColision = other.SelfColision;

}

void FIKinemaBoneDef::ResetToDefaults()
{
	DofX = true;
	DofY = true;
	DofZ = true;
	Limits.InitializeValues();
	EnforceLimits = false;
	Weight = FVector(1.f);
	Mass = 1.f;
	ResetToRest = false;
	EnableLimits = false;
	MaxVelocity = 0.01f;
	LimitsGain = 1.f;
	Active = true;
	EnableRetargeting = true;
	RetargetingGain = 1.f;
	RetargetingDoFGain = FVector(1.f);
}

#endif

// Cache ANSI names from the wide versions.
void FIKinemaBoneDef::CacheANSINames()
{
	// Cache the name and parent name in ANSI format.
	// This may be called more than once.

	/*FString temp = Name.ToString();
	temp.Split("_",nullptr,&temp);
	Name = FName(*temp);
	temp = ParentName.ToString();
	temp.Split("_", nullptr, &temp);
	ParentName = FName(*temp);*/
	TStringConversion<TStringConvert <TCHAR, ANSICHAR>> a(*Name.ToString());
	FGenericPlatformMemory::Memcpy(mNameANSI, a.Get(), a.Length());

	TStringConversion<TStringConvert <TCHAR, ANSICHAR>> b(*ParentName.ToString());
	FGenericPlatformMemory::Memcpy(mParentNameANSI, b.Get(), b.Length());


}

void FIKinemaBoneDef::PostLoadOrImport()
{
	// Reset the calculated indices (because they'll be recalculated later)
	TaskIndex = INDEX_NONE;
	ParentIndex = INDEX_NONE;
	CacheANSINames();
}

// Bind to the given skeleton
bool FIKinemaBoneDef::BindToSkeleton(FIK::ImportBone& importBone, USkeleton& skeleton, int32& ue4BoneIndex, bool upY, USkeletalMeshComponent* component, bool isSource, bool isRetargeting)
{

	if (mNameANSI[0] == '\0')
	{
		CacheANSINames();
	}
	importBone.name = mNameANSI;
	if (ParentName != NAME_None)
		importBone.parentname = mParentNameANSI;
	else
		importBone.parentname = NULL;

	bool success = true;

	// Cache the index of the bone in the UE4 skeleton.
	//ue4BoneIndex = skeleton.GetBoneTreeIndex(Name);
	ue4BoneIndex = component->GetBoneIndex(Name);
	const int32	ue4SkeletonBoneIndex = skeleton.GetReferenceSkeleton().FindBoneIndex(Name);

	//Edit Retargeting
	if (isSource)
	{
		//This rig is for a stage 1 in a retargeting pipeline
		//Ignore current UE skeleton and set up rig.
		FTransform restPose = RestPose;

		ConvertTransformToFromMaya(restPose);

		const FVector& restTrans = restPose.GetTranslation();
		importBone.rest_offset[0] = restTrans.X;
		importBone.rest_offset[1] = restTrans.Y;
		importBone.rest_offset[2] = restTrans.Z;

		const FQuat& restQuat = restPose.GetRotation();
		FIK::Quaternion q(restQuat.X, restQuat.Y, restQuat.Z, restQuat.W);
		q.normalize();

		//local orientation of the bone in the "bind pose", format: quaternion {w,x,y,z}
		importBone.rest_orientation[0] = q[3];
		importBone.rest_orientation[1] = q[0];
		importBone.rest_orientation[2] = q[1];
		importBone.rest_orientation[3] = q[2];
		importBone.user_data = const_cast<FIKinemaBoneDef*>(this);

	}
	else
	{
		//This is for the second and final stage. This is the most common case.
		if (ue4BoneIndex != INDEX_NONE)
		{
			// Validate the hierarchy.
			// The parent in the IKinema hierarchy should be the same
			// as the parent in the UE4 hierarchy.
			if (ParentName != NAME_None)
			{
				// Non-root bone.
				// Make sure that its parent corresponds to the UE4 parent.
				int32 parentBoneIndex = component->GetBoneIndex(ParentName);

				if (parentBoneIndex != INDEX_NONE)
				{
					FName parent = component->GetParentBone(Name);
					if (parentBoneIndex != component->GetBoneIndex(parent))
					{
						UE_LOG(LogIKinemaCore, Error, TEXT("IKinema parent joint %s for joint %s should be the parent in the UE4 skeleton hierarchy as well"), *ParentName.ToString(), *Name.ToString());
						success = false;
					}
				}
				else
					if (parentBoneIndex == INDEX_NONE)
					{
						UE_LOG(LogIKinemaCore, Error, TEXT("Unknown parent '%s' for joint '%s'"), *ParentName.ToString(), *Name.ToString());
						success = false;
					}
			}

			FTransform restPose = MatchPose* skeleton.GetRefLocalPoses()[ue4SkeletonBoneIndex];
			if (upY)
			{
				ConvertTransformToFromMaya(restPose);
			}

			static bool sDebugUseRestPoseFromDefinition = false;
			if (sDebugUseRestPoseFromDefinition)
			{
				restPose = RestPose;
			}

			const FVector& restTrans = restPose.GetTranslation();
			importBone.rest_offset[0] = restTrans.X;
			importBone.rest_offset[1] = restTrans.Y;
			importBone.rest_offset[2] = restTrans.Z;

			const FQuat& restQuat = restPose.GetRotation();
			FIK::Quaternion q(restQuat.X, restQuat.Y, restQuat.Z, restQuat.W);
			q.normalize();

			//local orientation of the bone in the "bind pose", format: quaternion {w,x,y,z}
			importBone.rest_orientation[0] = q[3];
			importBone.rest_orientation[1] = q[0];
			importBone.rest_orientation[2] = q[1];
			importBone.rest_orientation[3] = q[2];
			importBone.user_data = const_cast<FIKinemaBoneDef*>(this);


		}
		else
		{
			UE_LOG(LogIKinemaCore, Error, TEXT("Could not find joint named %s in skeleton"), *Name.ToString());
			success = false;
		}
	}
	if (isRetargeting)
	{
		//We are retargeting flip the signs on the X and Z components of the match quaternion.
		//Convert from Right to Left hand. 
		rotFromSrc = RotFromSrc;
		rotFromSrcPar = RotFromSrcPar;
		rotFromSrcPar.X = -rotFromSrcPar.X;
		rotFromSrc.X = -rotFromSrc.X;
		rotFromSrcPar.Z = -rotFromSrcPar.Z;
		rotFromSrc.Z = -rotFromSrc.Z;
	}
	return success;
}

bool FIKinemaBoneDef::BindToSkeleton(FIK::ImportBone& importBone, USkeleton& skeleton, int32& ue4BoneIndex, bool upY, bool isSource, bool isRetargeting)
{

	if (mNameANSI[0] == '\0')
	{
		CacheANSINames();
	}
	importBone.name = mNameANSI;
	if (ParentName != NAME_None)
		importBone.parentname = mParentNameANSI;
	else
		importBone.parentname = NULL;

	bool success = true;

	// Cache the index of the bone in the UE4 skeleton.
	//ue4BoneIndex = skeleton.GetBoneTreeIndex(Name);
	ue4BoneIndex = skeleton.GetReferenceSkeleton().FindBoneIndex(Name);
	//ue4BoneIndex = component->GetBoneIndex(Name);
	const int32	ue4SkeletonBoneIndex = skeleton.GetReferenceSkeleton().FindBoneIndex(Name);

	//Edit Retargeting
	if (isSource)
	{
		//This rig is for a stage 1 in a retargeting pipeline
		//Ignore current UE skeleton and set up rig.
		FTransform restPose = RestPose;

		ConvertTransformToFromMaya(restPose);

		const FVector& restTrans = restPose.GetTranslation();
		importBone.rest_offset[0] = restTrans.X;
		importBone.rest_offset[1] = restTrans.Y;
		importBone.rest_offset[2] = restTrans.Z;

		const FQuat& restQuat = restPose.GetRotation();
		FIK::Quaternion q(restQuat.X, restQuat.Y, restQuat.Z, restQuat.W);
		q.normalize();

		//local orientation of the bone in the "bind pose", format: quaternion {w,x,y,z}
		importBone.rest_orientation[0] = q[3];
		importBone.rest_orientation[1] = q[0];
		importBone.rest_orientation[2] = q[1];
		importBone.rest_orientation[3] = q[2];
		importBone.user_data = const_cast<FIKinemaBoneDef*>(this);

	}
	else
	{
		//This is for the second and final stage. This is the most common case.
		if (ue4BoneIndex != INDEX_NONE)
		{
			// Validate the hierarchy.
			// The parent in the IKinema hierarchy should be the same
			// as the parent in the UE4 hierarchy.
			if (ParentName != NAME_None)
			{
				// Non-root bone.
				// Make sure that its parent corresponds to the UE4 parent.
				int32 parentBoneIndex = skeleton.GetReferenceSkeleton().FindBoneIndex(ParentName);

				if (parentBoneIndex != INDEX_NONE)
				{
					//FName parent = skeleton.GetReferenceSkeleton().GetParentIndex(ue4BoneIndex);  //component->GetParentBone(Name);
					if (parentBoneIndex != skeleton.GetReferenceSkeleton().GetParentIndex(ue4BoneIndex))
					{
						UE_LOG(LogIKinemaCore, Error, TEXT("IKinema parent joint %s for joint %s should be the parent in the UE4 skeleton hierarchy as well"), *ParentName.ToString(), *Name.ToString());
						success = false;
					}
				}
				else
					if (parentBoneIndex == INDEX_NONE)
					{
						UE_LOG(LogIKinemaCore, Error, TEXT("Unknown parent '%s' for joint '%s'"), *ParentName.ToString(), *Name.ToString());
						success = false;
					}
			}

			FTransform restPose = MatchPose* skeleton.GetRefLocalPoses()[ue4SkeletonBoneIndex];
			if (upY)
			{
				ConvertTransformToFromMaya(restPose);
			}

			static bool sDebugUseRestPoseFromDefinition = false;
			if (sDebugUseRestPoseFromDefinition)
			{
				restPose = RestPose;
			}

			const FVector& restTrans = restPose.GetTranslation();
			importBone.rest_offset[0] = restTrans.X;
			importBone.rest_offset[1] = restTrans.Y;
			importBone.rest_offset[2] = restTrans.Z;

			const FQuat& restQuat = restPose.GetRotation();
			FIK::Quaternion q(restQuat.X, restQuat.Y, restQuat.Z, restQuat.W);
			q.normalize();

			//local orientation of the bone in the "bind pose", format: quaternion {w,x,y,z}
			importBone.rest_orientation[0] = q[3];
			importBone.rest_orientation[1] = q[0];
			importBone.rest_orientation[2] = q[1];
			importBone.rest_orientation[3] = q[2];
			importBone.user_data = const_cast<FIKinemaBoneDef*>(this);


		}
		else
		{
			UE_LOG(LogIKinemaCore, Error, TEXT("Could not find joint named %s in skeleton"), *Name.ToString());
			success = false;
		}
	}
	if (isRetargeting)
	{
		//We are retargeting flip the signs on the X and Z components of the match quaternion.
		//Convert from Right to Left hand. 
		rotFromSrc = RotFromSrc;
		rotFromSrcPar = RotFromSrcPar;
		rotFromSrcPar.X = -rotFromSrcPar.X;
		rotFromSrc.X = -rotFromSrc.X;
		rotFromSrcPar.Z = -rotFromSrcPar.Z;
		rotFromSrc.Z = -rotFromSrc.Z;
	}
	return success;
}



// Configure an IKinema bone segment (inside the solver instance).
void FIKinemaBoneDef::ConfigureIKSegment(FIK::IKSegment& segment, bool shadowPosing) const
{
	segment.setActiveDofs(DofX, DofY, DofZ);
	segment.setActiveDofs(StretchX, StretchY, StretchZ, false);
	segment.setWeight(0, Weight.X);
	segment.setWeight(1, Weight.Y);
	segment.setWeight(2, Weight.Z);
	segment.setMass(Mass);
	segment.enableShadowPosing(shadowPosing && EnableRetargeting);
	if (!shadowPosing)
	{
		segment.enableRetargeting(EnableRetargeting, false);
		segment.setRetargetingGain(RetargetingGain);
	}

	segment.setAlwaysToRest(ResetToRest);

	static bool sDebugAllowLimits = true;
	const bool reallyEnableLimits = sDebugAllowLimits && EnableLimits;
	if (reallyEnableLimits)
	{
		FIK::EulerLimits eulerLimits;
		RigLimitsToEulerLimits(Limits, eulerLimits);
		segment.setEulerLimits(eulerLimits);
	}
	segment.enforceLimits(EnforceLimits);
	segment.enableLimits(reallyEnableLimits);
	segment.setMaxW(MaxVelocity);
	segment.setLimitsGain(LimitsGain);
	segment.setRetargetingGain(0, RetargetingDoFGain[0]);
	segment.setRetargetingGain(1, RetargetingDoFGain[1]);
	segment.setRetargetingGain(2, RetargetingDoFGain[2]);
	segment.setActive(Active, false);

	//Create Collision Shape
	FIK::IKShape* shape = nullptr;

	auto CollisionShape = SelfColision.CollisionShape;
	switch (CollisionShape.Shape)
	{
	case EIKSPHERE:
		shape = new FIK::IKSphereShape(CollisionShape.Radius);
		break;
	case EIKCAPSULE:
		shape = new FIK::IKCapsuleShape(CollisionShape.HalfLength, CollisionShape.Radius);
		break;
	case EIKBOX:
		shape = new FIK::IKBoxShape(CollisionShape.BoxHalfLengths.X, CollisionShape.BoxHalfLengths.Y, CollisionShape.BoxHalfLengths.Z);
		break;
	default:
		break;
	}
	if (shape && shape->IsValid())
	{
		auto tran = CollisionShape.LocalOffset.GetTranslation();
		auto q = CollisionShape.LocalOffset.GetRotation();
		FIK::Transform local(FIK::Quaternion(q.X, q.Y, q.Z, q.W), FIK::Vector(tran.X, tran.Y, tran.Z));
		shape->SetLocalOffset(local);
		segment.SetCollisionShape(shape);
	}

}

// Transfer an IKinema pose to a UE4 pose, for this bone.
void FIKinemaBoneDef::IKinemaToUE4Pose(const FIK::IKSegment& segment, FTransform& ue4Transform, bool applyTranslation, bool global) const
{
	if (global)
	{
		const FIK::Real* x = segment.getGlobalPos();
		FVector trans;
		trans.X = x[0];
		trans.Y = x[1];
		trans.Z = x[2];
		const FIK::Real *quatG = segment.getGlobalQ();
		FQuat rot1;
		rot1.X = quatG[0];
		rot1.Y = quatG[1];
		rot1.Z = quatG[2];
		rot1.W = quatG[3];
		ue4Transform.SetTranslation(trans);
		ue4Transform.SetRotation(rot1);
		return;
	}
	if (applyTranslation)
	{
		const FIK::Real *pos = segment.getPos();
		FVector trans;
		trans.X = pos[0];
		trans.Y = pos[1];
		trans.Z = pos[2];
		ue4Transform.SetTranslation(trans);
	}

	const FIK::Real *quat = segment.getQ();
	FQuat rot;
	rot.X = quat[0];
	rot.Y = quat[1];
	rot.Z = quat[2];
	rot.W = quat[3];
	ue4Transform.SetRotation(rot);
}

FIKinemaTaskDef::FIKinemaTaskDef()
{
	SourceIndex = INDEX_NONE;
}

// struct FIKinemaTaskDef.
void FIKinemaTaskDef::InitializeValues()
{
	HasPositionTask = false;
	HasRotationTask = false;
	RootAsScalePivot = true;

	BoneIndex = INDEX_NONE;
	SourceIndex = INDEX_NONE;
	ParentIndex = INDEX_NONE;
	MarkerIndex = INDEX_NONE;
	ParentName = "";
	//position
	PositionAsPoleObject = false;

	PositionDofX = true;
	PositionDofY = true;
	PositionDofZ = true;

	PositionDepth = -1;
	PositionWeight = FVector::ZeroVector;
	PositionPrecision = FVector::ZeroVector;
	PositionPriority = 0;

	TipOffset = FVector::ZeroVector;
	IsParentConstraint = true;
	//orientation
	RotateDofX = true;
	RotateDofY = true;
	RotateDofZ = true;
	RotateDepth = -1;
	RotateWeight = FVector::ZeroVector;
	RotatePrecision = FVector::ZeroVector;
	RotatePriority = 0;

	ScaleRetargeting = FVector(1.f, 1.f, 1.f);
	OffsetRetargeting = FVector::ZeroVector;
}

#if WITH_EDITOR
void FIKinemaTaskDef::CopyTaskDefPropertiesFrom(FIKinemaTaskDef const* other)
{
	if (other)
	{
		HasPositionTask = other->HasPositionTask;
		HasRotationTask = other->HasRotationTask;
		RootAsScalePivot = other->RootAsScalePivot;
		PositionAsPoleObject = other->PositionAsPoleObject;
		PositionDofX = other->PositionDofX;
		PositionDofY = other->PositionDofY;
		PositionDofZ = other->PositionDofZ;
		PositionDepth = other->PositionDepth;
		PositionWeight = other->PositionWeight;
		PositionPrecision = other->PositionPrecision;
		PositionPriority = other->PositionPriority;
		TipOffset = other->TipOffset;
		ScaleRetargeting = other->ScaleRetargeting;
		OffsetRetargeting = other->OffsetRetargeting;
		RotateDofX = other->RotateDofX;
		RotateDofY = other->RotateDofY;
		RotateDofZ = other->RotateDofZ;
		RotateDepth = other->RotateDepth;
		RotateWeight = other->RotateWeight;
		RotatePrecision = other->RotatePrecision;
		RotatePriority = other->RotatePriority;
		TaskOffset = other->TaskOffset;
		IsParentConstraint = other->IsParentConstraint;
		bLockOffset = other->bLockOffset;

	}
}

void FIKinemaTaskDef::CopyTaskDefPropertiesFrom(FIKinemaTaskDef const& other)
{
	HasPositionTask = other.HasPositionTask;
	HasRotationTask = other.HasRotationTask;
	RootAsScalePivot = other.RootAsScalePivot;
	PositionAsPoleObject = other.PositionAsPoleObject;
	PositionDofX = other.PositionDofX;
	PositionDofY = other.PositionDofY;
	PositionDofZ = other.PositionDofZ;
	PositionDepth = other.PositionDepth;
	PositionWeight = other.PositionWeight;
	PositionPrecision = other.PositionPrecision;
	PositionPriority = other.PositionPriority;
	TipOffset = other.TipOffset;
	ScaleRetargeting = other.ScaleRetargeting;
	OffsetRetargeting = other.OffsetRetargeting;
	RotateDofX = other.RotateDofX;
	RotateDofY = other.RotateDofY;
	RotateDofZ = other.RotateDofZ;
	RotateDepth = other.RotateDepth;
	RotateWeight = other.RotateWeight;
	RotatePrecision = other.RotatePrecision;
	RotatePriority = other.RotatePriority;
	TaskOffset = other.TaskOffset;
	IsParentConstraint = other.IsParentConstraint;
	bLockOffset = other.bLockOffset;

}

void FIKinemaTaskDef::ResetToDefaults()
{
	//Reset task properties to defaults
	HasPositionTask = true;
	HasRotationTask = true;
	RootAsScalePivot = true;
	PositionAsPoleObject = false;
	PositionDofX = true;
	PositionDofY = true;
	PositionDofZ = true;
	PositionDepth = static_cast<unsigned int>(-1);
	PositionWeight = FVector(30.f);
	PositionPrecision = FVector(2.f);
	PositionPriority = 0;
	TipOffset = FVector::ZeroVector;
	ScaleRetargeting = FVector::ZeroVector;
	OffsetRetargeting = FVector::ZeroVector;
	RotateDofX = true;
	RotateDofY = true;
	RotateDofZ = true;
	RotateDepth = static_cast<unsigned int>(-1);
	RotateWeight = FVector(30.f);
	RotatePrecision = FVector(2.f);
	RotatePriority = 0;
	SourceIndex = INDEX_NONE;
}


#endif


void FIKinemaTaskDef::PostLoadOrImport()
{
	// Reset the calculated indices (because they'll be recalculated later)
	ParentIndex = INDEX_NONE;
	//Only keep numerical part of the task parent name (the marker ID) this is hardcoded for PhaseSpace
	if (ParentName != "")
	{
		FString markerID(ParentName.RightChop(1));
		TTypeFromString<int32>::FromString(MarkerIndex, *markerID);
	}
	else
	{
		FString markerID(Name.ToString().RightChop(1));
		markerID = markerID.LeftChop(5);
		TTypeFromString<int32>::FromString(MarkerIndex, *markerID);
	}

}

// Configure a position task using our properties.
void FIKinemaTaskDef::ConfigurePositionTask(FIK::IKPositionTask& positionTask) const
{
	check(HasPositionTask);

	positionTask.setDofs(PositionDofX, PositionDofY, PositionDofZ);
	positionTask.setLength(PositionDepth);
	positionTask.setDofWeight(0, PositionWeight.X);
	positionTask.setDofWeight(1, PositionWeight.Y);
	positionTask.setDofWeight(2, PositionWeight.Z);
	positionTask.setDofPrecision(0, PositionPrecision.X);
	positionTask.setDofPrecision(1, PositionPrecision.Y);
	positionTask.setDofPrecision(2, PositionPrecision.Z);
	positionTask.setPriority(PositionPriority);
	positionTask.setTipOffset(TipOffset.X, -TipOffset.Y, TipOffset.Z);
	positionTask.setAsPoleTask(PositionAsPoleObject);
	positionTask.setActive(true, false);
}

// Configure an orientation task using our properties.
void FIKinemaTaskDef::ConfigureOrientationTask(FIK::IKOrientationTask& orientationTask) const
{
	check(HasRotationTask);

	orientationTask.setDofs(RotateDofX, RotateDofY, RotateDofZ);
	orientationTask.setLength(RotateDepth);

	orientationTask.setDofWeight(0, RotateWeight.X);
	orientationTask.setDofWeight(1, RotateWeight.Y);
	orientationTask.setDofWeight(2, RotateWeight.Z);
	orientationTask.setDofPrecision(0, RotatePrecision.X);
	orientationTask.setDofPrecision(1, RotatePrecision.Y);
	orientationTask.setDofPrecision(2, RotatePrecision.Z);
	orientationTask.setPriority(RotatePriority);

	orientationTask.setActive(true, false);
}

// struct FIKinemaForceDef.

void FIKinemaForceDef::InitializeValues()
{
	Support = false;
}

// struct FIKinemaCOMDef.

void FIKinemaCOMDef::InitializeValues()
{
}

#if WITH_EDITOR
void FIKinemaCOMDef::CopyTaskDefPropertiesFrom(FIKinemaCOMDef const* other)
{
	if (other)
	{
		DegreeOfFreedomX = other->DegreeOfFreedomX;
		DegreeOfFreedomY = other->DegreeOfFreedomY;
		DegreeOfFreedomZ = other->DegreeOfFreedomZ;
		Weight = other->Weight;
		Precision = other->Precision;

	}
}

void FIKinemaCOMDef::CopyTaskDefPropertiesFrom(FIKinemaCOMDef const& other)
{
	DegreeOfFreedomX = other.DegreeOfFreedomX;
	DegreeOfFreedomY = other.DegreeOfFreedomY;
	DegreeOfFreedomZ = other.DegreeOfFreedomZ;
	Weight = other.Weight;
	Precision = other.Precision;
}

void FIKinemaCOMDef::ResetToDefaults()
{
	DegreeOfFreedomX = true;
	DegreeOfFreedomY = true;
	DegreeOfFreedomZ = true;
	Weight = FVector(10.f);
	Precision = FVector(2.f);
}

#endif

// struct FIKinemaSolverDef.

void FIKinemaSolverDef::InitializeValues()
{
	MaxIterations = 150;
	ContinuouslySolving = true;
	PoseTolerance = 0.00001f;
	SolutionTolerance = 0.00001f;
	RootTranslationWeight = 1.f;
	RootTargetTranslation = FVector(1.f, 1.f, 1.f);
	TranslateRoot = true;
	RootTranslationDofX = true;
	RootTranslationDofY = true;
	RootTranslationDofZ = true;
	Retargeting = true;
	RetargetingTaskScale = FVector(1.f, 1.f, 1.f);
	RetargetingTaskOffset = FVector::ZeroVector;
	EnableRootTargetTranslation = true;
	EnableMoments = false;
	MomentsWeight = 1.f;
	MomentsPriority = 1;
	FigureMass = 0.f;
	UseDefaultZMP = false;
	ZeroMomentPoint = FVector::ZeroVector;
	TasksPrecision = 4.f;
	RescaleTasks = false;
	LimitsGain = 0.f;
	RetargetingGain = 1.f;
	SecondaryTaskWeight = 0.f;
	SecondaryTaskPrecision = 0.f;
	PCoefficient = 0.2f;
	ActiveCOM = false;
	IsRetargeting = true;
	ImportScale = 1.0f;
	SourceScale = 1.0f;
	// Initialize substructures and arrays.

	COM.InitializeValues();

	for (auto i = 0; i < Bones.Num(); ++i)
	{
		Bones[i].InitializeValues();
	}
	for (auto i = 0; i < Tasks.Num(); ++i)
	{
		Tasks[i].InitializeValues();
	}
	for (auto i = 0; i < Forces.Num(); ++i)
	{
		Forces[i].InitializeValues();
	}
}

// Convert between UE4's math types and IKinema's math types.

static FIK::Vector UE4ToIKinemaVector(const FVector& ue4Vector)
{
	return FIK::Vector(ue4Vector.X, ue4Vector.Y, ue4Vector.Z);
}

static FIK::Quaternion UE4ToIKinemaQuaternion(const FQuat& ue4Quat)
{
	return FIK::Quaternion(ue4Quat.X, ue4Quat.Y, ue4Quat.Z, ue4Quat.W);
}

// Utility method to print out a rotation and translation.
static FString IKinemaFormatTransform(const TCHAR* transformName, const FQuat& q, const FVector& v)
{
	FString formattedString = FString::Printf(
		TEXT("%s:\n\tRotation: (%f, %f, %f, %f)\n\tPosition: (%f, %f, %f)\n"),
		transformName, q.X, q.Y, q.Z, q.W, v.X, v.Y, v.Z);
	return formattedString;
}

// Configure the given solver using our properties.
bool FIKinemaSolverDef::ConfigureSolver(FIKinemaSolverInstance& solverInstance) const
{
	check(solverInstance.mIKSolver != nullptr);

	FIK::IKSolver& solver = *(solverInstance.mIKSolver);

	solver.setSolutionTolerance(SolutionTolerance);
	solver.setTranslationWeight(RootTranslationWeight);
	solver.setTargetTranslation(RootTargetTranslation.X, RootTargetTranslation.Y, RootTargetTranslation.Z);
	solver.enableTargetTranslation(EnableRootTargetTranslation);
	solver.setMomentsWeight(MomentsWeight);
	solver.setFigureMass(FigureMass);
	solver.setZMP(ZeroMomentPoint.X, ZeroMomentPoint.Y, ZeroMomentPoint.Z);
	solver.useDefaultZMP(UseDefaultZMP);
	solver.setPrecision(-TasksPrecision); // Negative used due to how IKinema runtime works vs. Maya.
	solver.rescaleTasks(RescaleTasks);
	solver.setLimitsGain(LimitsGain);
	solver.setRetargetingGain(RetargetingGain);
	solver.setSecondaryTaskWeight(SecondaryTaskWeight);
	solver.setSecondaryTaskPrecision(SecondaryTaskPrecision);
	solver.setP(PCoefficient);
	solver.setAddErrors(bCombineWithAnimation);

	// Import the bones into the solver.
	const FIKinemaRigToSkeletonBinding* skeletonBinding = solverInstance.GetSkeletonBinding();
	//	check(skeletonBinding != nullptr);
	skeletonBinding->ConfigureSolver(solver);
	solver.setup();

	FIK::IKSegment* rootSegment = solver.getRootSegment();


	auto iJointsCount = Bones.Num();
	for (auto iJointId = 0; iJointId < iJointsCount; iJointId++)
	{
		FIK::IKSegment *pJoint = solver.getSegmentByHandle(iJointId);
		if (pJoint == nullptr)
		{
			UE_LOG(LogIKinemaCore, Error, TEXT("Expected IKinema joint at index %d"), iJointId);
			return false;
		}

		Bones[iJointId].ConfigureIKSegment(*pJoint, EnableShadowPosing);

		if (Bones[iJointId].SourceIndex == INDEX_NONE)
		{
			pJoint->enableShadowPosing(true);
		}
	}

	if (rootSegment != nullptr)
	{
		const bool bTranslateRoot = RootTranslationDofX || RootTranslationDofY || RootTranslationDofZ;
		if (bTranslateRoot)
		{
			rootSegment->setActiveDofs(RootTranslationDofX, RootTranslationDofY, RootTranslationDofZ, false);
			solver.translateRoot(true);
			rootSegment->setTranslating(true, false);
			rootSegment->resetStretch(false);
		}
	}

	if (TranslateRoot)
	{
		if (rootSegment != nullptr)
		{
			rootSegment->setActiveDofs(true, true, true, false);
		}
		solver.translateRoot(true);
		rootSegment->setTranslating(true, false);
		rootSegment->resetStretch(false);
	}

	// Set up the tasks.
	int32 iTasksCount = Tasks.Num();
	solverInstance.mJointTasks.Empty(iTasksCount);
	for (int32 taskIndex = 0; taskIndex < iTasksCount; ++taskIndex)
	{
		const FIKinemaTaskDef& taskDef = Tasks[taskIndex];
		FIK::IKSegment* pJoint = solver.getSegmentByHandle(taskDef.BoneIndex);
		if (pJoint == nullptr)
		{
			UE_LOG(LogIKinemaCore, Error, TEXT("Expected IKinema joint at index %d, for task %d"), taskDef.BoneIndex, taskIndex);
			return false;
		}

		FIKinemaJointTaskInstance taskInstance;

		// Position task.
		if (taskDef.HasPositionTask)
		{
			FIK::IKPositionTask* positionTask = solver.addPositionTask(pJoint->getName());
			if (positionTask == nullptr)
			{
				UE_LOG(LogIKinemaCore, Error, TEXT("Could not create position task at index %d"), taskIndex);
				return false;
			}
			taskDef.ConfigurePositionTask(*positionTask);

			taskInstance.mPositionTask = positionTask;
		}

		// Rotation/orientation task.
		if (taskDef.HasRotationTask)
		{
			FIK::IKOrientationTask* rotationTask = solver.addOrientationTask(pJoint->getName());
			if (rotationTask == nullptr)
			{
				UE_LOG(LogIKinemaCore, Error, TEXT("Could not create orientation task at index %d"), taskIndex);
				return false;
			}
			taskDef.ConfigureOrientationTask(*rotationTask);
			taskInstance.mOrientationTask = rotationTask;
		}

		solverInstance.mJointTasks.Add(taskInstance);
	}

#if IKINEMA_SOLVER_DUMP_SUPPORTED
	// Dump settings so we can compare with Maya.
	// Find the folder containing the IKinema license file.
	static bool sDebugDumpSolverConfig = true;
	if (sDebugDumpSolverConfig)
	{
		//solver.dumpSettings("D:\\solver_config_runtime.txt");
		sDebugDumpSolverConfig = false;
	}
#endif

	// Debugging difference in rest pose.
	static bool sDebugPrintRestPoses = false;
	if (sDebugPrintRestPoses)
	{
		FString defRestPoses;
		FString segmentRestPoses;
		int32 boneCount = Bones.Num();
		for (int32 boneIndex = 0; boneIndex < boneCount; boneIndex++)
		{
			FString boneName = Bones[boneIndex].Name.ToString();

			FTransform restPoseFromDef = Bones[boneIndex].RestPose;
			defRestPoses += IKinemaFormatTransform(*boneName, restPoseFromDef.GetRotation(), restPoseFromDef.GetTranslation());

			FIK::IKSegment* segment = solver.getSegmentByHandle(boneIndex);
			const FIK::Real* segP = segment->getRestPos().cptr();
			const FIK::Real* segQ = segment->getRestQ().cptr();
			segmentRestPoses += IKinemaFormatTransform(*boneName, FQuat(segQ[0], segQ[1], segQ[2], segQ[3]), FVector(segP[0], segP[1], segP[2]));
		}
		FString formattedString = FString::Printf(TEXT("Rest poses from Maya:\n%s\nRest poses at runtime:\n%s\n"), *defRestPoses, *segmentRestPoses);
		FGenericPlatformMisc::LowLevelOutputDebugString(*formattedString);
		sDebugPrintRestPoses = false;
	}

	solver.setup();

	if (TranslateRoot)
	{
		solver.getRootSegment()->setActiveDofs(true, true, true, false);
	}

	return true;
}

bool FIKinemaSolverDef::ApplySettings(class FIKinemaSolverInstance& solverInstance) const
{
	if (!solverInstance.mIKSolver)
		return false;

	FIK::IKSolver& solver = *(solverInstance.mIKSolver);

	check(solverInstance.mJointTasks.Num() == Tasks.Num());
	check(solver.numSegments() == Bones.Num());

	if (solverInstance.mJointTasks.Num() != Tasks.Num() || solver.numSegments() != Bones.Num())
		return false;

	solver.setSolutionTolerance(SolutionTolerance);
	solver.setTranslationWeight(RootTranslationWeight);
	solver.setTargetTranslation(RootTargetTranslation.X, RootTargetTranslation.Y, RootTargetTranslation.Z);
	solver.enableTargetTranslation(EnableRootTargetTranslation);
	solver.setMomentsWeight(MomentsWeight);
	solver.setFigureMass(FigureMass);
	solver.setZMP(ZeroMomentPoint.X, ZeroMomentPoint.Y, ZeroMomentPoint.Z);
	solver.useDefaultZMP(UseDefaultZMP);
	solver.setPrecision(-TasksPrecision); // Negative used due to how IKinema runtime works vs. Maya.
	solver.rescaleTasks(RescaleTasks);
	solver.setLimitsGain(LimitsGain);
	solver.setRetargetingGain(RetargetingGain);
	solver.setSecondaryTaskWeight(SecondaryTaskWeight);
	solver.setSecondaryTaskPrecision(SecondaryTaskPrecision);
	solver.setP(PCoefficient);

	FIK::IKSegment* rootSegment = solver.getRootSegment();


	int32 iJointsCount = Bones.Num();
	for (int32 iJointId = 0; iJointId < iJointsCount; iJointId++)
	{
		FIK::IKSegment *pJoint = solver.getSegmentByHandle(iJointId);
		if (pJoint == nullptr)
		{
			UE_LOG(LogIKinemaCore, Error, TEXT("Expected IKinema joint at index %d"), iJointId);
			return false;
		}

		Bones[iJointId].ConfigureIKSegment(*pJoint, EnableShadowPosing);
	}

	if (rootSegment != nullptr)
	{
		const bool bTranslateRoot = RootTranslationDofX || RootTranslationDofY || RootTranslationDofZ;
		if (bTranslateRoot)
		{
			rootSegment->setActiveDofs(RootTranslationDofX, RootTranslationDofY, RootTranslationDofZ, false);
			solver.translateRoot(true);
			rootSegment->setTranslating(true, false);
			rootSegment->resetStretch(false);
		}
	}

	if (TranslateRoot)
	{
		if (rootSegment != nullptr)
		{
			rootSegment->setActiveDofs(true, true, true, false);
		}
		solver.translateRoot(true);
		rootSegment->setTranslating(true, false);
		rootSegment->resetStretch(false);
	}

	// Set up the tasks.
	int32 iTasksCount = solverInstance.mJointTasks.Num();
	for (int32 taskIndex = 0; taskIndex < iTasksCount; ++taskIndex)
	{
		const FIKinemaTaskDef& taskDef = Tasks[taskIndex];
		FIKinemaJointTaskInstance& taskInstance = solverInstance.mJointTasks[taskIndex];

		FIK::IKSegment* pJoint = solver.getSegmentByHandle(taskDef.BoneIndex);
		if (pJoint == nullptr)
		{
			UE_LOG(LogIKinemaCore, Error, TEXT("Expected IKinema joint at index %d, for task %d"), taskDef.BoneIndex, taskIndex);
			return false;
		}

		// Position task.
		if (taskDef.HasPositionTask && taskInstance.mPositionTask)
		{
			taskDef.ConfigurePositionTask(*taskInstance.mPositionTask);
		}

		// Rotation/orientation task.
		if (taskDef.HasRotationTask && taskInstance.mOrientationTask)
		{
			taskDef.ConfigureOrientationTask(*taskInstance.mOrientationTask);
		}

	}

#if IKINEMA_SOLVER_DUMP_SUPPORTED
	// Dump settings so we can compare with Maya.
	// Find the folder containing the IKinema license file.
	static bool sDebugDumpSolverConfig = true;
	if (sDebugDumpSolverConfig)
	{
		solver.dumpSettings("D:\\solver_config_runtime.txt");
		sDebugDumpSolverConfig = false;
	}
#endif

	// Debugging difference in rest pose.
	static bool sDebugPrintRestPoses = false;
	if (sDebugPrintRestPoses)
	{
		FString defRestPoses;
		FString segmentRestPoses;
		int32 boneCount = Bones.Num();
		for (int32 boneIndex = 0; boneIndex < boneCount; boneIndex++)
		{
			FString boneName = Bones[boneIndex].Name.ToString();

			FTransform restPoseFromDef = Bones[boneIndex].RestPose;
			defRestPoses += IKinemaFormatTransform(*boneName, restPoseFromDef.GetRotation(), restPoseFromDef.GetTranslation());

			FIK::IKSegment* segment = solver.getSegmentByHandle(boneIndex);
			const FIK::Real* segP = segment->getRestPos().cptr();
			const FIK::Real* segQ = segment->getRestQ().cptr();
			segmentRestPoses += IKinemaFormatTransform(*boneName, FQuat(segQ[0], segQ[1], segQ[2], segQ[3]), FVector(segP[0], segP[1], segP[2]));
		}
		FString formattedString = FString::Printf(TEXT("Rest poses from Maya:\n%s\nRest poses at runtime:\n%s\n"), *defRestPoses, *segmentRestPoses);
		FGenericPlatformMisc::LowLevelOutputDebugString(*formattedString);
		sDebugPrintRestPoses = false;
	}

	solver.setup();

	return true;
}

// Called from the outer rig class.
void FIKinemaSolverDef::PostLoadOrImport()
{
	// Finalize the loaded data
	for (int32 iBoneIndex = 0; iBoneIndex < Bones.Num(); ++iBoneIndex)
	{
		Bones[iBoneIndex].PostLoadOrImport();
	}
	for (int32 iTaskIndex = 0; iTaskIndex < Tasks.Num(); ++iTaskIndex)
	{
		Tasks[iTaskIndex].PostLoadOrImport();
	}

	// Build the bone->task reverse lookup table
	for (int32 iTaskIndex = 0; iTaskIndex < Tasks.Num(); ++iTaskIndex)
	{
		Bones[Tasks[iTaskIndex].BoneIndex].TaskIndex = iTaskIndex;
	}

	// Cache the bone hierarchy information
	for (int32 iBoneIndex = 0; iBoneIndex < Bones.Num(); ++iBoneIndex)
	{
		Bones[iBoneIndex].ParentIndex = FindBoneIndex(Bones[iBoneIndex].ParentName);
		if (Bones[iBoneIndex].ParentIndex == INDEX_NONE)
		{
			Bones[iBoneIndex].ParentName = NAME_None;
		}
	}

	// Cache the task virtual hierarchy information
	for (int32 iTaskIndex = 0; iTaskIndex < Tasks.Num(); ++iTaskIndex)
	{
		int32 iParentBoneIndex = Bones[Tasks[iTaskIndex].BoneIndex].ParentIndex;
		int32 iParentTaskIndex = GetTaskIndexFromBone(iParentBoneIndex);
		while (iParentTaskIndex == INDEX_NONE && iParentBoneIndex != INDEX_NONE)
		{
			iParentBoneIndex = Bones[iParentBoneIndex].ParentIndex;
			iParentTaskIndex = GetTaskIndexFromBone(iParentBoneIndex);
		}
		Tasks[iTaskIndex].ParentIndex = iParentTaskIndex;
	}
}

// Find the index of the task with the given name in our list.
// Returns INDEX_NONE if not found.
int32 FIKinemaSolverDef::FindTaskIndex(const FName& taskName) const
{
	const int32 taskCount = Tasks.Num();
	for (int32 i = 0; i < taskCount; ++i)
	{
		if (Tasks[i].Name == taskName)
		{
			return i;
		}
	}
	return INDEX_NONE;
}

// Find the index of the task with the given name in our list.
// Returns INDEX_NONE if not found.
int32 FIKinemaSolverDef::FindBoneIndex(const FName& boneName) const
{
	const int32 boneCount = Bones.Num();
	for (int32 i = 0; i < boneCount; ++i)
	{
		if (Bones[i].Name == boneName)
		{
			return i;
		}
	}
	return INDEX_NONE;
}


// Gets the index of the task driving the bone referenced by boneIndex.
// Returns INDEX_NONE if boneIndex is invalid or if bone is not being driven.
int32 FIKinemaSolverDef::GetTaskIndexFromBone(int32 boneIndex) const
{
	return (boneIndex >= 0 && boneIndex < Bones.Num())
		? Bones[boneIndex].TaskIndex
		: INDEX_NONE;
}

// Gets the index of the bone being driven by the task referenced by taskIndex.
// Returns INDEX_NONE if taskIndex is invalid.
int32 FIKinemaSolverDef::GetBoneIndexFromTask(int32 taskIndex) const
{
	return (taskIndex >= 0 && taskIndex < Tasks.Num())
		? Tasks[taskIndex].BoneIndex
		: INDEX_NONE;
}

// class FIKinemaRigToSkeletonBinding.

// Default constructor.
FIKinemaRigToSkeletonBinding::FIKinemaRigToSkeletonBinding()
	: mRig(nullptr)
{
}

// Bind the given IKinemaRig to the given skeleton and store the result.
bool FIKinemaRigToSkeletonBinding::BindToSkeleton(UIKinemaRig* ikinemaRig, USkeleton* skeleton, USkeletalMeshComponent* component)
{
	if (ikinemaRig == nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Invalid IKinema rig for skeleton binding"));
		return false;
	}

	bool upY = ikinemaRig->SolverDef.UpAxis == "Y";

	TArray<FIKinemaBoneDef>& boneDefs = ikinemaRig->SolverDef.Bones;
	const int32 numBoneDefs = boneDefs.Num();
	ImportBones.Empty(numBoneDefs); //Reserve memory
	UE4BoneIndices.Empty(numBoneDefs);
	for (int32 i = 0; i < numBoneDefs; ++i)
	{
		FIK::ImportBone ikBone;
		int32 ue4BoneIndex = INDEX_NONE;
		if (!boneDefs[i].BindToSkeleton(ikBone, *skeleton, ue4BoneIndex, upY, component, ikinemaRig->SolverDef.IsSource, ikinemaRig->SolverDef.IsRetargeting))
		{
			return false;
		}
		ImportBones.Add(ikBone);
		UE4BoneIndices.Add(ue4BoneIndex);
	}

	mRig = ikinemaRig;
	mSkeleton = skeleton;
	mComponent = component;

	return true;
}

bool FIKinemaRigToSkeletonBinding::BindToSkeleton(UIKinemaRig* ikinemaRig, USkeleton* skeleton)
{
	if (ikinemaRig == nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Invalid IKinema rig for skeleton binding"));
		return false;
	}

	bool upY = ikinemaRig->SolverDef.UpAxis == "Y";
	upY = true;

	TArray<FIKinemaBoneDef>& boneDefs = ikinemaRig->SolverDef.Bones;
	const int32 numBoneDefs = boneDefs.Num();
	ImportBones.Empty(numBoneDefs); //Reserve memory
	UE4BoneIndices.Empty(numBoneDefs);
	for (int32 i = 0; i < numBoneDefs; ++i)
	{
		FIK::ImportBone ikBone;
		int32 ue4BoneIndex = INDEX_NONE;
		if (!boneDefs[i].BindToSkeleton(ikBone, *skeleton, ue4BoneIndex, upY, ikinemaRig->SolverDef.IsSource, ikinemaRig->SolverDef.IsRetargeting))
		{
			return false;
		}
		ImportBones.Add(ikBone);
		UE4BoneIndices.Add(ue4BoneIndex);
	}

	mRig = ikinemaRig;
	mSkeleton = skeleton;


	return true;
}

void FIKinemaRigToSkeletonBinding::ClearImportBones()
{
	ImportBones.SetNum(0);
}

// Configure the given solver using our cached data.
void FIKinemaRigToSkeletonBinding::ConfigureSolver(FIK::IKSolver& solver) const
{
	solver.importBones(ImportBones.GetData(), ImportBones.Num());
}

bool FIKinemaRigToSkeletonBinding::IsValid()
{
	if (mRig && mSkeleton && mComponent && UE4BoneIndices.Num() == mRig->SolverDef.Bones.Num())
		return true;

	return false;
}

// Access the UE4 bone index given the bone def index.
int32 FIKinemaRigToSkeletonBinding::GetUE4BoneIndex(int32 boneDefIndex) const
{
	return UE4BoneIndices[boneDefIndex];
}

// Access the IKinema rig.
const UIKinemaRig* FIKinemaRigToSkeletonBinding::GetRig() const
{
	return mRig;
}

UIKinemaRig* FIKinemaRigToSkeletonBinding::GetRig()
{
	return mRig;
}

const USkeleton* FIKinemaRigToSkeletonBinding::GetSkeleton() const
{
	return mSkeleton;
}

const USkeletalMeshComponent* FIKinemaRigToSkeletonBinding::GetSkeletalMeshComponent() const
{
	return mComponent;
}

// struct FIKinemaJointTaskInstance.

// Default constructor.
FIKinemaJointTaskInstance::FIKinemaJointTaskInstance()
	: mPositionTask(nullptr)
	, mOrientationTask(nullptr)
{
}

// Utility methods to configure the tasks.

void FIKinemaJointTaskInstance::SetPositionWeight(const FVector& weight)
{
	if (mPositionTask != nullptr)
	{
		mPositionTask->setDofWeight(0, weight.X);
		mPositionTask->setDofWeight(1, weight.Y);
		mPositionTask->setDofWeight(2, weight.Z);
	}
}

void FIKinemaJointTaskInstance::SetOrientationWeight(const FVector& weight)
{
	if (mOrientationTask != nullptr)
	{
		mOrientationTask->setDofWeight(0, weight.X);
		mOrientationTask->setDofWeight(1, weight.Y);
		mOrientationTask->setDofWeight(2, weight.Z);
	}
}

void FIKinemaJointTaskInstance::SetPositionTarget(const FVector& positionTarget)
{
	if (mPositionTask != nullptr)
	{
		mPositionTask->setTarget(positionTarget.X, positionTarget.Y, positionTarget.Z);
	}
}

void FIKinemaJointTaskInstance::SetOrientationTarget(const FQuat& orientationTarget)
{
	if (mOrientationTask != nullptr)
	{
		mOrientationTask->setTarget(orientationTarget.X, orientationTarget.Y, orientationTarget.Z, orientationTarget.W);
	}
}

void FIKinemaJointTaskInstance::SetPositionEnabled(bool state)
{
	if (mPositionTask != nullptr)
	{
		mPositionTask->setActive(state, false);
	}
}

void FIKinemaJointTaskInstance::SetOrientationEnabled(bool state)
{
	if (mOrientationTask != nullptr)
	{
		mOrientationTask->setActive(state, false);
	}
}

void FIKinemaJointTaskInstance::ApplyPositionProperties(const FIKinemaTaskProperties& properties)
{
	if (mPositionTask != nullptr)
	{
		if (mPositionTask->isDofEnabled(0) != properties.PositionDofX ||
			mPositionTask->isDofEnabled(1) != properties.PositionDofY ||
			mPositionTask->isDofEnabled(2) != properties.PositionDofZ)
			mPositionTask->setDofs(properties.PositionDofX, properties.PositionDofY, properties.PositionDofZ);

		if (mPositionTask->getLength() != properties.PositionDepth)
			mPositionTask->setLength(properties.PositionDepth);

		if (mPositionTask->getDofWeight(0) != properties.PositionWeight[0])
			mPositionTask->setDofWeight(0, properties.PositionWeight[0]);

		if (mPositionTask->getDofWeight(1) != properties.PositionWeight[1])
			mPositionTask->setDofWeight(1, properties.PositionWeight[1]);

		if (mPositionTask->getDofWeight(2) != properties.PositionWeight[2])
			mPositionTask->setDofWeight(2, properties.PositionWeight[2]);

		if (mPositionTask->getDofPrecision(0) != properties.PositionPrecision[0])
			mPositionTask->setDofPrecision(0, properties.PositionPrecision[0]);

		if (mPositionTask->getDofPrecision(1) != properties.PositionPrecision[1])
			mPositionTask->setDofPrecision(1, properties.PositionPrecision[1]);

		if (mPositionTask->getDofPrecision(2) != properties.PositionPrecision[2])
			mPositionTask->setDofPrecision(2, properties.PositionPrecision[2]);
	}
}

void FIKinemaJointTaskInstance::ApplyOrientationProperties(const FIKinemaTaskProperties& properties)
{
	if (mOrientationTask != nullptr)
	{
		if (mOrientationTask->isDofEnabled(0) != properties.RotateDofX ||
			mOrientationTask->isDofEnabled(1) != properties.RotateDofY ||
			mOrientationTask->isDofEnabled(2) != properties.RotateDofZ)
			mOrientationTask->setDofs(properties.RotateDofX, properties.RotateDofY, properties.RotateDofZ);

		if (mOrientationTask->getLength() != properties.RotateDepth && properties.RotateDepth != -1)
			mOrientationTask->setLength(properties.RotateDepth);

		if (mOrientationTask->getDofWeight(0) != properties.RotateWeight[0])
			mOrientationTask->setDofWeight(0, properties.RotateWeight[0]);

		if (mOrientationTask->getDofWeight(1) != properties.RotateWeight[1])
			mOrientationTask->setDofWeight(1, properties.RotateWeight[1]);

		if (mOrientationTask->getDofWeight(2) != properties.RotateWeight[2])
			mOrientationTask->setDofWeight(2, properties.RotateWeight[2]);

		if (mOrientationTask->getDofPrecision(0) != properties.RotatePrecision[0])
			mOrientationTask->setDofPrecision(0, properties.RotatePrecision[0]);

		if (mOrientationTask->getDofPrecision(1) != properties.RotatePrecision[1])
			mOrientationTask->setDofPrecision(1, properties.RotatePrecision[1]);

		if (mOrientationTask->getDofPrecision(2) != properties.RotatePrecision[2])
			mOrientationTask->setDofPrecision(2, properties.RotatePrecision[2]);
	}
}

// class FIKinemaSolverInstance.

// Default constructor.
FIKinemaSolverInstance::FIKinemaSolverInstance()
	: mSkeletonBinding(nullptr)
	, mIKSolver(nullptr)
{
}

// Destructor.
FIKinemaSolverInstance::~FIKinemaSolverInstance()
{
	Destroy();
}

void FIKinemaSolverInstance::Dump(const FString& Folder)
{
	if (mIKSolver)
	{
		FString FileName = FString::Printf(TEXT("%s/%s_%d.txt"), *Folder, *mSkeletonBinding->GetRig()->GetName(), InstID);
		ANSICHAR* fname = _strdup(TCHAR_TO_ANSI(*FileName));
		//mIKSolver->dumpSettings(fname);
		free(fname);
	}
}

// Create using the given skeleton binding.
void FIKinemaSolverInstance::Create(FIKinemaRigToSkeletonBinding& skeletonBinding)
{
	// Destroy if already created.
	Destroy();

	mSkeletonBinding = &skeletonBinding;
	const UIKinemaRig* rig = skeletonBinding.GetRig();
	if (rig != nullptr)
	{
		rig->CreateSolver(*this);
	}
}

void FIKinemaSolverInstance::Create(UIKinemaRig * skeletonBinding)
{
	Destroy();
	const UIKinemaRig* rig = skeletonBinding;
	if (rig != nullptr)
	{
		rig->CreateSolver(*this);
	}
}

// Destroy this solver instance.
void FIKinemaSolverInstance::Destroy()
{
	if (mIKSolver != nullptr)
	{
		delete mIKSolver;
		mIKSolver = nullptr;
	}

	mSkeletonBinding = nullptr;
	mJointTasks.Empty();
}

bool FIKinemaSolverInstance::UpdateAndSolve(const TArray<FIKinemaSolverTask>& tasks,
	const TArray<FIKinemaTaskProperties>& taskProperties,
	const TArray<FIKinemaTaskOverride>& taskOverride,
	const TArray<FIKinemaSegmentProperties>& segProperties,
	const TArray<FIKinemaSegmentOverride>& segOverride,
	TArray<FTransform>& inPose,
	FPoseContext& Output,
	const FTransform& solverToWorld, bool DebugDraw)
{
	if (mSkeletonBinding == nullptr || mIKSolver == nullptr || !mSkeletonBinding->GetSkeleton())
	{
		return false;
	}

	UIKinemaRig* rig = mSkeletonBinding->GetRig();
	if (rig == nullptr)
	{
		return false;
	}

	bool upY = rig->SolverDef.UpAxis == "Y";



	int32 segmentsCount = mIKSolver->numSegments();

	//Retargeting Edit
	if (!rig->SolverDef.IsSource)
	{

		if (rig->SolverDef.IsRetargeting)
		{
			mSolverToWorldSpace = solverToWorld;
		}
		else
		{
			//solver to component space matrix
			int32 RootBoneIndex = mSkeletonBinding->GetUE4BoneIndex(0);
			int32 ParentIndex = mSkeletonBinding->GetSkeleton()->GetReferenceSkeleton().GetParentIndex(RootBoneIndex);
			mSolverToWorldSpace = FTransform::Identity;
			mSolverToComponentSpace = FTransform::Identity;
			while (ParentIndex != INDEX_NONE)
			{
				mSolverToWorldSpace *= inPose[ParentIndex];
				FName parentName = mSkeletonBinding->GetSkeletalMeshComponent()->GetBoneName(ParentIndex);
				ParentIndex = mSkeletonBinding->GetSkeletalMeshComponent()->GetBoneIndex(mSkeletonBinding->GetSkeletalMeshComponent()->GetParentBone(parentName));
			}

			mSolverToWorldSpace = mSolverToComponentSpace * mSkeletonBinding->GetSkeletalMeshComponent()->GetComponentTransform();
		}
	}

	for (int32 s = 0; s < segmentsCount; s++)
	{
		FIK::IKSegment* seg = mIKSolver->getSegmentByHandle(s);
		check(seg);
		if (rig->SolverDef.Bones[s].ResetToRest)
			seg->resetSegment();
		//override segment settings
		if (segOverride[s].ShowAsPin)
		{
			if (seg->ActiveDof(0) != segProperties[s].DofX ||
				seg->ActiveDof(1) != segProperties[s].DofY ||
				seg->ActiveDof(2) != segProperties[s].DofZ)
				seg->setActiveDofs(segProperties[s].DofX, segProperties[s].DofY, segProperties[s].DofZ);
			if (seg->getEnforceLimits() != segProperties[s].EnforceLimits)
				seg->enforceLimits(segProperties[s].EnforceLimits);
			if (seg->Weight(0) != segProperties[s].Weight[0])
				seg->setWeight(0, segProperties[s].Weight[0]);
			if (seg->Weight(1) != segProperties[s].Weight[1])
				seg->setWeight(1, segProperties[s].Weight[1]);
			if (seg->Weight(2) != segProperties[s].Weight[2])
				seg->setWeight(2, segProperties[s].Weight[2]);
			if (!rig->SolverDef.EnableShadowPosing)
			{
				if (seg->haveLimits() != segProperties[s].EnableLimits)
					seg->enableLimits(segProperties[s].EnableLimits);
				if (seg->getLimitsGain() != segProperties[s].LimitsGain)
					seg->setLimitsGain(segProperties[s].LimitsGain);
				if (seg->haveRetargeting() != segProperties[s].EnableRetargeting)
					seg->enableRetargeting(segProperties[s].EnableRetargeting);
				if (seg->getRetargetingGain() != segProperties[s].RetargetingGain)
					seg->setRetargetingGain(segProperties[s].RetargetingGain);
			}
		}

		if (!rig->IsRigidBody && mSkeletonBinding->GetRig()->SolverDef.Bones[s].EnableRetargeting && !rig->SolverDef.EnableShadowPosing)
		{
			int32 bone = mSkeletonBinding->GetUE4BoneIndex(s);
			//Retargeting Edit
			if (mSkeletonBinding->GetRig()->SolverDef.IsRetargeting && !mSkeletonBinding->GetRig()->SolverDef.IsSource)
			{
				int32 bone_ = mSkeletonBinding->GetRig()->SolverDef.Bones[s].SourceIndex;
				bone = (bone_ == INDEX_NONE) ? INDEX_NONE : bone_;
			}
			//Edit Finished
			if (bone != INDEX_NONE)
			{
				if (inPose.IsValidIndex(bone))
				{
					FTransform trans = inPose[bone];
					if (upY)
					{
						ConvertTransformToFromMaya(trans);
					}

					FQuat q = trans.GetRotation();
					if (mSkeletonBinding->GetRig()->SolverDef.IsRetargeting)
					{
						q = rig->SolverDef.Bones[s].rotFromSrcPar*q*rig->SolverDef.Bones[s].rotFromSrc;
					}
					seg->setTargetOrientation(q.X, q.Y, q.Z, q.W);
				}
			}
		}
	}

	if (!UpdateTaskInstances(tasks, taskProperties, taskOverride))
	{
		return false;
	}

	if (rig->SolverDef.IsRetargeting)
	{
		Output.ResetToRefPose();
	}
	Output.Pose.ResetToRefPose();
	rig->SolveAndUpdatePose(*this, Output);


	if (DebugDraw)
	{
		DebugDrawIKinema(mIKSolver, mSolverToWorldSpace, FColor::Blue, Output.AnimInstanceProxy->GetSkelMeshComponent()->GetWorld());
		//DrawDebugString(GWorld,Loc+FVector(0,0,1), taskDefs[i].Name.ToString());
	}

	return true;
}

bool FIKinemaSolverInstance::UpdateAndSolve(
	const TArray<FIKinemaSolverTask>& tasks,
	const TArray<FIKinemaTaskProperties>& taskProperties,
	const TArray<FIKinemaTaskOverride>& taskOverride,
	const TArray<FIKinemaSegmentProperties>& segProperties,
	const TArray<FIKinemaSegmentOverride>& segOverride,
	TArray<FTransform>& inPose,
	FCompactPose& Output,
	const FTransform& solverToWorld, bool DebugDraw)
{
	if (mSkeletonBinding == nullptr || mIKSolver == nullptr || !mSkeletonBinding->GetSkeleton())
	{
		return false;
	}

	UIKinemaRig* rig = mSkeletonBinding->GetRig();
	if (rig == nullptr)
	{
		return false;
	}

	bool upY = rig->SolverDef.UpAxis == "Y";


	int32 segmentsCount = mIKSolver->numSegments();

	////Retargeting Edit
	if (!rig->SolverDef.IsSource)
	{

		if (rig->SolverDef.IsRetargeting)
		{
			mSolverToWorldSpace = solverToWorld;
		}
		else
		{
			//solver to component space matrix
			int32 RootBoneIndex = mSkeletonBinding->GetUE4BoneIndex(0);
			int32 ParentIndex = mSkeletonBinding->GetSkeleton()->GetReferenceSkeleton().GetParentIndex(RootBoneIndex);
			mSolverToWorldSpace = FTransform::Identity;
			mSolverToComponentSpace = FTransform::Identity;
			while (ParentIndex != INDEX_NONE)
			{
				mSolverToWorldSpace *= inPose[ParentIndex];
				FName parentName = mSkeletonBinding->GetSkeletalMeshComponent()->GetBoneName(ParentIndex);
				ParentIndex = mSkeletonBinding->GetSkeletalMeshComponent()->GetBoneIndex(mSkeletonBinding->GetSkeletalMeshComponent()->GetParentBone(parentName));
			}

			mSolverToWorldSpace = mSolverToComponentSpace * mSkeletonBinding->GetSkeletalMeshComponent()->GetComponentTransform();
		}
	}

	for (int32 s = 0; s < segmentsCount; s++)
	{
		FIK::IKSegment* seg = mIKSolver->getSegmentByHandle(s);
		check(seg);
		if (rig->SolverDef.Bones[s].ResetToRest)
			seg->resetSegment();
		//override segment settings
		if (segOverride[s].ShowAsPin)
		{
			if (seg->ActiveDof(0) != segProperties[s].DofX ||
				seg->ActiveDof(1) != segProperties[s].DofY ||
				seg->ActiveDof(2) != segProperties[s].DofZ)
				seg->setActiveDofs(segProperties[s].DofX, segProperties[s].DofY, segProperties[s].DofZ);
			if (seg->getEnforceLimits() != segProperties[s].EnforceLimits)
				seg->enforceLimits(segProperties[s].EnforceLimits);
			if (seg->Weight(0) != segProperties[s].Weight[0])
				seg->setWeight(0, segProperties[s].Weight[0]);
			if (seg->Weight(1) != segProperties[s].Weight[1])
				seg->setWeight(1, segProperties[s].Weight[1]);
			if (seg->Weight(2) != segProperties[s].Weight[2])
				seg->setWeight(2, segProperties[s].Weight[2]);
			if (!rig->SolverDef.EnableShadowPosing)
			{
				if (seg->haveLimits() != segProperties[s].EnableLimits)
					seg->enableLimits(segProperties[s].EnableLimits);
				if (seg->getLimitsGain() != segProperties[s].LimitsGain)
					seg->setLimitsGain(segProperties[s].LimitsGain);
				if (seg->haveRetargeting() != segProperties[s].EnableRetargeting)
					seg->enableRetargeting(segProperties[s].EnableRetargeting);
				if (seg->getRetargetingGain() != segProperties[s].RetargetingGain)
					seg->setRetargetingGain(segProperties[s].RetargetingGain);
			}
		}

		if (!rig->IsRigidBody && mSkeletonBinding->GetRig()->SolverDef.Bones[s].EnableRetargeting && !rig->SolverDef.EnableShadowPosing)
		{
			int32 bone = mSkeletonBinding->GetUE4BoneIndex(s);
			//Retargeting Edit
			if (mSkeletonBinding->GetRig()->SolverDef.IsRetargeting && !mSkeletonBinding->GetRig()->SolverDef.IsSource)
			{
				int32 bone_ = mSkeletonBinding->GetRig()->SolverDef.Bones[s].SourceIndex;
				bone = (bone_ == INDEX_NONE) ? INDEX_NONE : bone_;
			}
			//Edit Finished
			if (bone != INDEX_NONE)
			{
				if (inPose.IsValidIndex(bone))
				{
					FTransform trans = inPose[bone];
					if (upY) //yup
					{

						ConvertTransformToFromMaya(trans);
					}

					FQuat q = trans.GetRotation();
					if (mSkeletonBinding->GetRig()->SolverDef.IsRetargeting)
					{
						q = rig->SolverDef.Bones[s].rotFromSrcPar*q*rig->SolverDef.Bones[s].rotFromSrc;
					}
					seg->setTargetOrientation(q.X, q.Y, q.Z, q.W);
				}
			}
		}
	}

	if (!UpdateTaskInstances(tasks, taskProperties, taskOverride))
	{
		return false;
	}

	if (rig->SolverDef.IsRetargeting)
	{
		Output.ResetToRefPose();
	}
	Output.ResetToRefPose();
	rig->SolveAndUpdatePose(*this, Output);


	if (DebugDraw)
	{
		DebugDrawIKinema(mIKSolver, mSolverToWorldSpace, FColor::Blue, GWorld);
		//DrawDebugString(GWorld,Loc+FVector(0,0,1), taskDefs[i].Name.ToString());
	}

	return true;
}

bool FIKinemaSolverInstance::UpdateAndSolve(TArray<FIKinemaSolverTask>& tasks,
	const TArray<FIKinemaTaskProperties>& taskProperties,
	const TArray<FIKinemaTaskOverride>& taskOverride,
	const TArray<FIKinemaSegmentProperties>& segProperties,
	const TArray<FIKinemaSegmentOverride>& segOverride,
	FPoseContext& Output, bool DebugDraw)
{
	if (mIKSolver == nullptr || mSkeletonBinding == nullptr || !mSkeletonBinding->GetSkeleton())
	{
		return false;
	}

	UIKinemaRig* rig = mSkeletonBinding->GetRig();
	if (rig == nullptr)
	{
		return false;
	}

	bool upY = rig->SolverDef.UpAxis == "Y";

	{
		FTransform ToSolverSpace = FTransform::Identity;
		int32 segmentsCount = mIKSolver->numSegments();

		if (segmentsCount != mSkeletonBinding->GetRig()->SolverDef.Bones.Num() ||
			segmentsCount != segProperties.Num() ||
			segmentsCount != segOverride.Num())
		{
			return false;
		}
		const auto BoneContainer = Output.Pose.GetBoneContainer();
		FCSPose<FCompactPose> ComponentSpacePose;
		ComponentSpacePose.InitPose(Output.Pose);
		{
			//solver to component space matrix
			//Convert to ComponentSpace

			int32 RootBoneIndex = mSkeletonBinding->GetUE4BoneIndex(0);
			check(RootBoneIndex >= 0);
			auto ParentIndexBone = BoneContainer.GetCompactPoseIndexFromSkeletonIndex(RootBoneIndex);
			auto ParentIndex = Output.Pose.GetParentBoneIndex(ParentIndexBone);
			mSolverToWorldSpace = FTransform::Identity;
			if (ParentIndex != INDEX_NONE)
			{
				ToSolverSpace = ComponentSpacePose.GetComponentSpaceTransform(ParentIndex).Inverse();
			}
			mSolverToWorldSpace = ToSolverSpace.GetRelativeTransformReverse(Output.AnimInstanceProxy->GetSkelMeshComponent()->GetComponentTransform());
		}

		for (int32 s = 0; s < segmentsCount; s++)
		{
			FIK::IKSegment* seg = mIKSolver->getSegmentByHandle(s);
			check(seg);
			//override segment settings
			if (segOverride[s].ShowAsPin)
			{
				if (seg->ActiveDof(0) != segProperties[s].DofX ||
					seg->ActiveDof(1) != segProperties[s].DofY ||
					seg->ActiveDof(2) != segProperties[s].DofZ)
					seg->setActiveDofs(segProperties[s].DofX, segProperties[s].DofY, segProperties[s].DofZ);
				if (seg->getEnforceLimits() != segProperties[s].EnforceLimits)
					seg->enforceLimits(segProperties[s].EnforceLimits);
				if (seg->Weight(0) != segProperties[s].Weight[0])
					seg->setWeight(0, segProperties[s].Weight[0]);
				if (seg->Weight(1) != segProperties[s].Weight[1])
					seg->setWeight(1, segProperties[s].Weight[1]);
				if (seg->Weight(2) != segProperties[s].Weight[2])
					seg->setWeight(2, segProperties[s].Weight[2]);
				if (seg->haveLimits() != segProperties[s].EnableLimits)
					seg->enableLimits(segProperties[s].EnableLimits);
				if (seg->getLimitsGain() != segProperties[s].LimitsGain)
					seg->setLimitsGain(segProperties[s].LimitsGain);
				if (seg->haveRetargeting() != segProperties[s].EnableRetargeting)
					seg->enableRetargeting(segProperties[s].EnableRetargeting);
				if (seg->getRetargetingGain() != segProperties[s].RetargetingGain)
					seg->setRetargetingGain(segProperties[s].RetargetingGain);
			}

			if (rig->SolverDef.Bones[s].StretchX || rig->SolverDef.Bones[s].StretchY || rig->SolverDef.Bones[s].StretchZ)
			{
				seg->resetSegment();
			}

			{
				auto index = mSkeletonBinding->GetUE4BoneIndex(s);
				auto bone = BoneContainer.GetCompactPoseIndexFromSkeletonIndex(index);
				if (bone.GetInt() != INDEX_NONE)
				{
					FTransform trans = Output.Pose[bone];
					if (upY)
					{
						ConvertTransformToFromMaya(trans);
					}

					const FIK::IKSegment* parent = seg->Parent();
					if (parent)
					{
						const int32  BoneParent = mSkeletonBinding->GetUE4BoneIndex(parent->SegmentId());
						auto ue4ParentBoneIndex = BoneContainer.GetCompactPoseIndexFromSkeletonIndex(BoneParent);
						FTransform ue4ParentTransform = Output.Pose[ue4ParentBoneIndex];
						trans.SetTranslation(trans.GetTranslation()*ue4ParentTransform.GetScale3D());
					}
					FVector t = trans.GetTranslation();
					FQuat q = trans.GetRotation();
					seg->setTargetOrientation(q.X, q.Y, q.Z, q.W);
					if (seg != mIKSolver->getRootSegment())
						seg->setTranslation(t.X, t.Y, t.Z);
				}
			}
		}

		if (!UpdateTaskInstances(tasks, taskProperties, taskOverride))
		{
			return false;
		}

	}

	{
		rig->SolveAndUpdatePose(*this, Output);
	}

	if (DebugDraw)
	{
		DebugDrawIKinema(mIKSolver, mSolverToWorldSpace, FColor::Blue, Output.AnimInstanceProxy->GetSkelMeshComponent()->GetWorld());
		//DrawDebugString(GWorld,Loc+FVector(0,0,1), taskDefs[i].Name.ToString());
	}

	return true;
}




const FIKinemaRigToSkeletonBinding* FIKinemaSolverInstance::GetSkeletonBinding() const
{
	return mSkeletonBinding;
}

bool FIKinemaSolverInstance::UpdateTaskInstances(const TArray<FIKinemaSolverTask>& tasks, const TArray<FIKinemaTaskProperties>& taskProperties, const TArray<FIKinemaTaskOverride>& taskOverride)
{
	const int32 numTasks = mJointTasks.Num();
	// Make sure that we have just the right number of bones in the incoming pose.
	if (numTasks != tasks.Num() ||
		numTasks != taskProperties.Num() ||
		numTasks != taskOverride.Num())
	{
		return false;
	}

	const TArray<FIKinemaTaskDef>& taskDefs = mSkeletonBinding->GetRig()->SolverDef.Tasks;

	bool upY = mSkeletonBinding->GetRig()->SolverDef.UpAxis == "Y";


	FTransform solverToComponent = mSkeletonBinding->GetSkeletalMeshComponent()->GetComponentTransform();//FTransform::Identity;//mSkeletonBinding->GetSkeleton()->GetReferenceSkeleton().GetRefBonePose()[0]; //mSkeletonBinding->GetSkeletalMeshComponent()->GetComponentTransform();

	solverToComponent = mSolverToWorldSpace.GetRelativeTransform(solverToComponent);
	// Update targets on all the tasks in the solver.
	for (int32 i = 0; i < numTasks; ++i)
	{

		FIKinemaJointTaskInstance& taskInstance = mJointTasks[i];
		// The task weights pose is already converted to Maya space.
		// Convert the task transform.

		FTransform Target = tasks[i].Target;
		if (mSkeletonBinding->GetRig()->SolverDef.IsRetargeting)
		{

			FTransform offset = taskDefs[i].TaskOffset;

			ConvertTransformToFromMaya(offset, true);
			FVector p = Target.GetTranslation();
			FQuat q = Target.GetRotation();
			if (mSkeletonBinding->GetRig()->UseParentConstraint || taskDefs[i].IsParentConstraint)
			{
				p = p + q*offset.GetTranslation() *mSkeletonBinding->GetRig()->SolverDef.SourceScale;
			}
			else
			{
				FVector of(offset.GetTranslation().X, offset.GetTranslation().Z, -offset.GetTranslation().Y);
				p = p + of;
			}
			q = solverToComponent.GetRotation().Inverse()*q*offset.GetRotation();

			Target.SetRotation(q);
			p = solverToComponent.InverseTransformPosition(p);
			Target.SetTranslation(p);
		}

		FTransform inputTransform = Target;
		if (upY)
		{
			ConvertTransformToFromMaya(inputTransform);
		}

		taskInstance.SetPositionEnabled(tasks[i].EnableTranslation);
		if (tasks[i].EnableTranslation)
		{
			if (taskOverride[i].ShowAsPin)
			{
				taskInstance.ApplyPositionProperties(taskProperties[i]);
			}

			taskInstance.SetPositionTarget(inputTransform.GetTranslation());
		}

		taskInstance.SetOrientationEnabled(tasks[i].EnableOrientation);
		if (tasks[i].EnableOrientation)
		{
			if (taskOverride[i].ShowAsPin)
			{
				taskInstance.ApplyOrientationProperties(taskProperties[i]);
			}

			taskInstance.SetOrientationTarget(inputTransform.GetRotation());
		}

		if (tasks[i].DebugDraw)
		{
			FVector Loc = mSolverToWorldSpace.TransformPosition(Target.GetTranslation());
			FRotator Rot = (Target*mSolverToWorldSpace).Rotator();/**/
			float scale = tasks[i].DebugDrawScale;

			ExecOnGameThread(
				[Loc, Rot, scale]() {

				::DrawDebugCoordinateSystem(GWorld, Loc, Rot, scale, false, -1.0f, SDPG_Foreground);

				::DrawDebugSphere(GWorld, Loc, 2.f, 12, FColor::Blue);
			});
		}
	}

	return true;
}

void FIKinemaSolverInstance::Reset()
{
	mIKSolver->resetSolver();
}

FIKinemaSolverTask::FIKinemaSolverTask() :
	EnableTranslation(false),
	EnableOrientation(false),
	DebugDraw(false),
	DebugDrawScale(1.0f),
	UseAsOffset(false)
{
}


FIKinemaTaskProperties::FIKinemaTaskProperties()
{
}

FIKinemaSegmentProperties::FIKinemaSegmentProperties()
{
}

// class UIKinemaRig
UIKinemaRig::UIKinemaRig(const FObjectInitializer& PCIP)
	: Super(PCIP),
	UpdateVersion(0),
	IsRigidBody(false)
{
	// Property initialization
	SolverDef.InitializeValues();
}

// Called just before re-importing properties from text.
void UIKinemaRig::PreReimport()
{
	Actor = nullptr;
	SolverDef.InitializeValues();
}

// To do post-processing on loaded properties.
void UIKinemaRig::PostLoad()
{
	Super::PostLoad();
	PostLoadOrImport();
}

void UIKinemaRig::PostDuplicate(bool bDuplicateForPIE)
{
#if WITH_EDITOR
	if (!bDuplicateForPIE)
	{
		if (Actor)
		{
			//Create MocapActor Asset
			FAssetToolsModule& AssetToolsModule = FModuleManager::Get().LoadModuleChecked<FAssetToolsModule>("AssetTools");
			FString Name;
			FString PackageName;

			// Get a unique name for the rig
			const FString DefaultSuffix = TEXT("_MocapActor");
			AssetToolsModule.Get().CreateUniqueAssetName(GetPathName(), DefaultSuffix, /*out*/ PackageName, /*out*/ Name);
			const FString PackagePath = FPackageName::GetLongPackagePath(PackageName);
			TArray<UObject*> ObjectsToSync;
			if (UObject* NewAsset = AssetToolsModule.Get().DuplicateAsset(Name + DefaultSuffix, PackagePath, Actor))
			{
				Actor = Cast<UActorSkeleton>(NewAsset);
			}
		}
	}
#endif
}


// Called from the above, and from import.
void UIKinemaRig::PostLoadOrImport()
{
	SolverDef.PostLoadOrImport();
}

// Create an IKinema solver instance using this rig.
void UIKinemaRig::CreateSolver(FIKinemaSolverInstance& solverInstance) const
{
	check(solverInstance.mIKSolver == nullptr);
	static unsigned int InstID = 1;

	// Find the folder containing the IKinema license file.
	//FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(TEXT("../../Binaries/ThirdParty/IKinema/license"));
	FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(FPaths::ConvertRelativePathToFull(FPaths::EngineDir() + TEXT("Binaries/ThirdParty/IKinema/license"))));
	char* errorMessage = nullptr;
	FIK::IKSolver* solver = new FIK::IKSolver(TCHAR_TO_ANSI(*licenseRelativeFolder), &errorMessage);
	if (solver == nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Could not create IKinema solver. Constructor returned null"));
		return;
	}
	if (errorMessage != nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Could not create IKinema solver. Constructor returned error: %s"), ANSI_TO_TCHAR(errorMessage));
		delete solver;
		return;
	}

	solverInstance.mIKSolver = solver;

	// The solver definition do the rest.
	if (!SolverDef.ConfigureSolver(solverInstance))
	{
		delete solver;
		solverInstance.mIKSolver = nullptr;
	}

	solverInstance.InstID = InstID++;
}

void UIKinemaRig::SolveAndUpdatePose(FIKinemaSolverInstance& solverInstance, FPoseContext& Output)
{
	FIK::IKSolver* solver = solverInstance.mIKSolver;
	if (solver == nullptr)
	{
		return;
	}

	// Do the IKinema magic.
	{
		SCOPE_CYCLE_COUNTER(STAT_StatsSolverSolve);
		FIK::AutoTune::Enum Result;
		if (EnableAutoTune)
		{
			solver->solve(SolverDef.MaxIterations, Result);
			if (Result != FIK::AutoTune::SOLVED)
			{
				UE_LOG(LogIKinemaCore, Warning, TEXT("IKinema solver has automatically tuned P-Gain to %f. Please ensure rig behaves correctly and save the IKinemaRig asset"), solver->getP());
				if (Result == FIK::AutoTune::TUNED)
				{
					auto rig = this;
					ExecOnGameThread(
						[rig]() {

						rig->Modify();
					});
					SolverDef.PCoefficient = solver->getP();

					if (SolverDef.PCoefficient < 0.01f)
					{
						SolverDef.PCoefficient = 0.01f;
					}
				}
			}

		}
		else
		{
			solver->solve(SolverDef.MaxIterations);
		}



		// Update rest translation on the root segment,
		// so that the next update might be stabler.
		// This is what the IKinema Maya plug-in does,
		// so this also brings Maya and runtime behaviours
		// closer to each other.
		FIK::IKSegment* root = solver->getRootSegment();
		if (root != nullptr)
		{
			const FIK::Real* pos = root->getPos();
			root->setRestTranslation(pos[0], pos[1], pos[2]);
		}
	}
	//DebugDrawIKinema(solver, FTransform::Identity, FColor::Green);
	// Copy back from IKinema to our pose.
	{
		SCOPE_CYCLE_COUNTER(STAT_StatsIKinemaToUE4Pose);
		IKinemaToUE4Pose(solverInstance, Output);
	}
}

void UIKinemaRig::SolveAndUpdatePose(FIKinemaSolverInstance& solverInstance, FCompactPose& Output)
{
	FIK::IKSolver* solver = solverInstance.mIKSolver;
	if (solver == nullptr)
	{
		return;
	}

	// Do the IKinema magic.
	{
		SCOPE_CYCLE_COUNTER(STAT_StatsSolverSolve);
		FIK::AutoTune::Enum Result;
		if (EnableAutoTune)
		{
			solver->solve(SolverDef.MaxIterations, Result);
			if (Result != FIK::AutoTune::SOLVED)
			{
				UE_LOG(LogIKinemaCore, Warning, TEXT("IKinema solver has automatically tuned P-Gain to %f. Please ensure rig behaves correctly and save the IKinemaRig asset"), solver->getP());
				if (Result == FIK::AutoTune::TUNED)
				{
					auto rig = this;
					ExecOnGameThread(
						[rig]() {

						rig->Modify();
					});
					SolverDef.PCoefficient = solver->getP();

					if (SolverDef.PCoefficient < 0.01f)
					{
						SolverDef.PCoefficient = 0.01f;
					}
				}
			}

		}
		else
		{
			solver->solve(SolverDef.MaxIterations);
		}



		// Update rest translation on the root segment,
		// so that the next update might be stabler.
		// This is what the IKinema Maya plug-in does,
		// so this also brings Maya and runtime behaviours
		// closer to each other.
		FIK::IKSegment* root = solver->getRootSegment();
		if (root != nullptr)
		{
			const FIK::Real* pos = root->getPos();
			root->setRestTranslation(pos[0], pos[1], pos[2]);
		}
	}
	//DebugDrawIKinema(solver, FTransform::Identity, FColor::Green);
	// Copy back from IKinema to our pose.
	{
		SCOPE_CYCLE_COUNTER(STAT_StatsIKinemaToUE4Pose);
		IKinemaToUE4Pose(solverInstance, Output);
	}
}

// Transfer an IKinema pose to UE4.
void UIKinemaRig::IKinemaToUE4Pose(FIKinemaSolverInstance& solverInstance, FPoseContext& Output) const
{
	if (solverInstance.mIKSolver == nullptr)
	{
		return;
	}

	const FIKinemaRigToSkeletonBinding* skeletonBinding = solverInstance.GetSkeletonBinding();
	check(skeletonBinding != nullptr);

	bool upY = SolverDef.UpAxis == "Y";

	FIK::IKSolver& solver = *(solverInstance.mIKSolver);
	FIK::IKSegment* rootSegment = solver.getRootSegment();
	int32 boneCount = SolverDef.Bones.Num();
	check(solver.numSegments() == boneCount);
	bool containsNaN = false;
	bool containsLargeRootTranslation = false;

	//TODO: If this is a stage 1 rig, just pass results from solver to ue4Pose directly
	if (SolverDef.IsSource)
	{
		TArray<FTransform> ue4Pose;
		ue4Pose = Output.Pose.GetBones();
		Output.Pose.SetBoneContainer(&Output.AnimInstanceProxy->GetRequiredBones());
		for (int32 i = 0; i < boneCount; ++i)
		{
			const FIK::IKSegment* segment = solver.getSegmentByHandle(i);
			const FIKinemaBoneDef& boneDef = SolverDef.Bones[i];

			check(segment != nullptr);
			FTransform ue4Transform;
			boneDef.IKinemaToUE4Pose(*segment, ue4Transform, true);

			if (!ue4Transform.ContainsNaN())
			{
				ue4Transform.NormalizeRotation();
				ue4Pose[i] = ue4Transform;
			}
			else
			{
				containsNaN = true;
			}
		}
		//Also pass global bone transforms in case we have some tasks
		for (int32 i = 0; i < boneCount; ++i)
		{
			const FIK::IKSegment* segment = solver.getSegmentByHandle(i);
			const FIKinemaBoneDef& boneDef = SolverDef.Bones[i];

			check(segment != nullptr);
			FTransform ue4Transform;
			boneDef.IKinemaToUE4Pose(*segment, ue4Transform, true, true);
			if (!ue4Transform.ContainsNaN())
			{
				ue4Transform.NormalizeRotation();
				ue4Pose.Add(ue4Transform);
			}
			else
			{
				containsNaN = true;
			}
		}
		if (containsNaN)
		{
			UE_LOG(LogIKinemaCore, Error, TEXT("IKinema solve resulted in one or more NaN's in the pose - any NaNs were ignored"));
		}
		Output.Pose.CopyBonesFrom(ue4Pose);
		return;
	}

	for (int32 i = 0; i < boneCount; ++i)
	{
		const FIK::IKSegment* segment = solver.getSegmentByHandle(i);
		//UE_LOG(LogIKinemaCore, Error, TEXT("IKinema segment %s"), ANSI_TO_TCHAR(segment->getName()));
		check(segment != nullptr);

		if (!segment->Active())
		{
			continue;
		}

		const FIKinemaBoneDef& boneDef = SolverDef.Bones[i];
		const auto ue4BoneIndex = Output.Pose.GetBoneContainer().MakeCompactPoseIndex(FMeshPoseBoneIndex(skeletonBinding->GetUE4BoneIndex(i)));
		if (ue4BoneIndex != INDEX_NONE)
		{
			FTransform ue4Transform = Output.Pose[ue4BoneIndex];
			// Only apply translation for the root segment.
			const bool applyRootTranslation = (segment == rootSegment);
			boneDef.IKinemaToUE4Pose(*segment, ue4Transform, applyRootTranslation);
			if (!ue4Transform.ContainsNaN())
			{
				if (upY)
				{
					ConvertTransformToFromMaya(ue4Transform);
				}

				// Make sure the root translation is within reasonable limits.
				if (applyRootTranslation)
				{
					static const float MAX_ROOT_TRANSLATION = 10000.0f;
					FVector rootTranslation = ue4Transform.GetTranslation();
					const float rootTranslationMagnitude = rootTranslation.Size();
					if (rootTranslationMagnitude > MAX_ROOT_TRANSLATION)
					{
						// Clamp.
						rootTranslation *= (MAX_ROOT_TRANSLATION / rootTranslationMagnitude);
						ue4Transform.SetTranslation(rootTranslation);
						containsLargeRootTranslation = true;
					}
				}

				ue4Transform.NormalizeRotation();
				Output.Pose[ue4BoneIndex] = ue4Transform;
			}
			else
			{
				containsNaN = true;
			}
		}
	}

	if (containsNaN)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("IKinema solve resulted in one or more NaN's in the pose - any NaNs were ignored"));
	}
	if (containsLargeRootTranslation)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("IKinema solve resulted in a large root translation - the root translation was clamped"));
		//TODO: AutoTune
		auto cp = solver.getP();
		cp = cp * 0.8f;
		solver.setP(cp);
		solver.resetSolver();
		auto rig = const_cast<UIKinemaRig*>(this);
		ExecOnGameThread(
			[rig]() {

			rig->Modify();
		});
		rig->SolverDef.PCoefficient = solver.getP();

	}
}

void UIKinemaRig::IKinemaToUE4Pose(FIKinemaSolverInstance& solverInstance, FCompactPose& Output) const
{
	if (solverInstance.mIKSolver == nullptr)
	{
		return;
	}

	const FIKinemaRigToSkeletonBinding* skeletonBinding = solverInstance.GetSkeletonBinding();
	check(skeletonBinding != nullptr);

	bool upY = SolverDef.UpAxis == "Y";

	FIK::IKSolver& solver = *(solverInstance.mIKSolver);
	FIK::IKSegment* rootSegment = solver.getRootSegment();
	int32 boneCount = SolverDef.Bones.Num();
	check(solver.numSegments() == boneCount);
	bool containsNaN = false;
	bool containsLargeRootTranslation = false;

	//TODO: If this is a stage 1 rig, just pass results from solver to ue4Pose directly
	if (SolverDef.IsSource)
	{
		TArray<FTransform> ue4Pose;
		ue4Pose = Output.GetBones();
		//Output.Pose.SetBoneContainer(&Output.AnimInstanceProxy->GetRequiredBones());
		for (int32 i = 0; i < boneCount; ++i)
		{
			const FIK::IKSegment* segment = solver.getSegmentByHandle(i);
			const FIKinemaBoneDef& boneDef = SolverDef.Bones[i];

			check(segment != nullptr);
			FTransform ue4Transform;
			boneDef.IKinemaToUE4Pose(*segment, ue4Transform, true);

			if (!ue4Transform.ContainsNaN())
			{
				ue4Transform.NormalizeRotation();
				ue4Pose[i] = ue4Transform;
			}
			else
			{
				containsNaN = true;
			}
		}
		//Also pass global bone transforms in case we have some tasks
		for (int32 i = 0; i < boneCount; ++i)
		{
			const FIK::IKSegment* segment = solver.getSegmentByHandle(i);
			const FIKinemaBoneDef& boneDef = SolverDef.Bones[i];

			check(segment != nullptr);
			FTransform ue4Transform;
			boneDef.IKinemaToUE4Pose(*segment, ue4Transform, true, true);
			if (!ue4Transform.ContainsNaN())
			{
				ue4Transform.NormalizeRotation();
				ue4Pose.Add(ue4Transform);
			}
			else
			{
				containsNaN = true;
			}
		}
		if (containsNaN)
		{
			UE_LOG(LogIKinemaCore, Error, TEXT("IKINEMA solve resulted in one or more NaN's in the pose - any NaNs were ignored"));
		}
		Output.CopyBonesFrom(ue4Pose);
		return;
	}

	for (int32 i = 0; i < boneCount; ++i)
	{
		const FIK::IKSegment* segment = solver.getSegmentByHandle(i);
		//UE_LOG(LogIKinemaCore, Error, TEXT("IKinema segment %s"), ANSI_TO_TCHAR(segment->getName()));
		check(segment != nullptr);

		if (!segment->Active())
		{
			continue;
		}

		const FIKinemaBoneDef& boneDef = SolverDef.Bones[i];
		const auto ue4BoneIndex = Output.GetBoneContainer().MakeCompactPoseIndex(FMeshPoseBoneIndex(skeletonBinding->GetUE4BoneIndex(i)));
		if (ue4BoneIndex != INDEX_NONE)
		{
			FTransform ue4Transform = Output[ue4BoneIndex];
			// Only apply translation for the root segment.
			const bool applyRootTranslation = (segment == rootSegment);
			boneDef.IKinemaToUE4Pose(*segment, ue4Transform, applyRootTranslation);
			if (!ue4Transform.ContainsNaN())
			{
				if (upY)
				{
					ConvertTransformToFromMaya(ue4Transform);
				}

				// Make sure the root translation is within reasonable limits.
				if (applyRootTranslation)
				{
					static const float MAX_ROOT_TRANSLATION = 10000.0f;
					FVector rootTranslation = ue4Transform.GetTranslation();
					const float rootTranslationMagnitude = rootTranslation.Size();
					if (rootTranslationMagnitude > MAX_ROOT_TRANSLATION)
					{
						// Clamp.
						rootTranslation *= (MAX_ROOT_TRANSLATION / rootTranslationMagnitude);
						ue4Transform.SetTranslation(rootTranslation);
						containsLargeRootTranslation = true;
					}
				}

				ue4Transform.NormalizeRotation();
				Output[ue4BoneIndex] = ue4Transform;
			}
			else
			{
				containsNaN = true;
			}
		}
	}

	if (containsNaN)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("IKinema solve resulted in one or more NaN's in the pose - any NaNs were ignored"));
	}
	if (containsLargeRootTranslation)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("IKinema solve resulted in a large root translation - the root translation was clamped"));
		//TODO: AutoTune
		auto cp = solver.getP();
		cp = cp * 0.8f;
		solver.setP(cp);
		solver.resetSolver();
		auto rig = const_cast<UIKinemaRig*>(this);
		ExecOnGameThread(
			[rig]() {

			rig->Modify();
		});
		rig->SolverDef.PCoefficient = solver.getP();

	}
}

FIK::ACP::ACPRig UIKinemaRig::CreateACPRigFromRig(const USkeleton* skeleton, const USkeletalMeshComponent* SkelComp, TArray<int32>& ue4Index) const
{
	FIK::ACP::ACPRig rig;
	for (auto& BoneDef : SolverDef.Bones)
	{
		FName Name = BoneDef.Name;
		const int32	ue4SkeletonBoneIndex = skeleton->GetReferenceSkeleton().FindBoneIndex(Name);
		const int32 ue4BoneIndex = SkelComp->GetBoneIndex(Name);

		FName parentName = SkelComp->GetParentBone(Name);

		FTransform restPose = skeleton->GetRefLocalPoses()[ue4SkeletonBoneIndex];
		const FVector& restTrans = restPose.GetTranslation();

		FIK::Vector trans(restTrans.X, restTrans.Y, restTrans.Z);
		const FQuat& restQuat = restPose.GetRotation();
		FIK::Quaternion q(restQuat.X, restQuat.Y, restQuat.Z, restQuat.W);
		q.normalize();

		FIK::IKBone b(TCHAR_TO_ANSI(*Name.ToString()), TCHAR_TO_ANSI(*parentName.ToString()), trans, q);

		//Set Bone Settings
		b.Active = BoneDef.Active;

		b.DofX = BoneDef.DofX;
		b.DofY = BoneDef.DofY;
		b.DofZ = BoneDef.DofZ;

		b.StretchX = BoneDef.StretchX;
		b.StretchY = BoneDef.StretchY;
		b.StretchZ = BoneDef.StretchZ;


		// One weight value for each bone axis.
		b.Weight = FIK::Vector(BoneDef.Weight.X, BoneDef.Weight.Y, BoneDef.Weight.Z);
		b.MaxVelocity = BoneDef.MaxVelocity;

		rig.AddBone(b);
		ue4Index.Push(ue4BoneIndex);
	}
	return rig;
}

//void FAnimNode_IKinemaRetargeting::SetAsset(class UIKinemaRig* rig)
//{
//	if (rig == nullptr)
//	{
//		UE_LOG(LogIKinemaCore, Error, TEXT("Invalid IKinemaRig for IKinemaSolve node"));
//		return;
//	}
//
//	IKinemaRig = rig;
//	PreloadObject(IKinemaRig);
//
//	if (mSkeletonBinding.IsValid() && Tasks.Num() != rig->SolverDef.Tasks.Num())
//	{
//		mSolverInstance.Destroy();
//		mSolverInstance.Create(mSkeletonBinding);
//	}
//
//	// Initialize the targets array with the number of tasks in the rig.
//	Tasks.Init(FIKinemaSolverTask(), rig->SolverDef.Tasks.Num());
//
//	TaskProperties.Init(FIKinemaTaskProperties(), rig->SolverDef.Tasks.Num());
//	PinTaskProperties.Init(FIKinemaTaskOverride(), rig->SolverDef.Tasks.Num());
//	for (int t = 0; t < rig->SolverDef.Tasks.Num(); t++)
//	{
//		TaskProperties[t].PositionDofX = rig->SolverDef.Tasks[t].PositionDofX;
//		TaskProperties[t].PositionDofY = rig->SolverDef.Tasks[t].PositionDofY;
//		TaskProperties[t].PositionDofZ = rig->SolverDef.Tasks[t].PositionDofZ;
//		TaskProperties[t].PositionDepth = rig->SolverDef.Tasks[t].PositionDepth;
//		TaskProperties[t].PositionWeight = rig->SolverDef.Tasks[t].PositionWeight;
//		TaskProperties[t].PositionPrecision = rig->SolverDef.Tasks[t].PositionPrecision;
//
//		//orientation
//		TaskProperties[t].RotateDofX = rig->SolverDef.Tasks[t].RotateDofX;
//		TaskProperties[t].RotateDofY = rig->SolverDef.Tasks[t].RotateDofY;
//		TaskProperties[t].RotateDofZ = rig->SolverDef.Tasks[t].RotateDofZ;
//		TaskProperties[t].RotateDepth = rig->SolverDef.Tasks[t].RotateDepth;
//		TaskProperties[t].RotateWeight = rig->SolverDef.Tasks[t].RotateWeight;
//		TaskProperties[t].RotatePrecision = rig->SolverDef.Tasks[t].RotatePrecision;
//
//		PinTaskProperties[t].TaskName = rig->SolverDef.Tasks[t].Name;
//		PinTaskProperties[t].ShowAsPin = false;
//
//		Tasks[t].EnableTranslation = TaskProperties[t].PositionDofX || TaskProperties[t].PositionDofY || TaskProperties[t].PositionDofZ;
//		Tasks[t].EnableOrientation = TaskProperties[t].RotateDofX || TaskProperties[t].RotateDofY || TaskProperties[t].RotateDofZ;
//		Tasks[t].UseAsOffset = false;
//		Tasks[t].DebugDraw = false;
//	}
//
//	SegmentProperties.Init(FIKinemaSegmentProperties(), rig->SolverDef.Bones.Num());
//	PinSegmentProperties.Init(FIKinemaSegmentOverride(), rig->SolverDef.Bones.Num());
//	//OverrideSegmentProperties.Init(false, rig->SolverDef.Tasks.Num());
//	for (int b = 0; b < rig->SolverDef.Bones.Num(); b++)
//	{
//		SegmentProperties[b].DofX = rig->SolverDef.Bones[b].DofX;
//		SegmentProperties[b].DofY = rig->SolverDef.Bones[b].DofY;
//		SegmentProperties[b].DofZ = rig->SolverDef.Bones[b].DofZ;
//		SegmentProperties[b].EnforceLimits = rig->SolverDef.Bones[b].EnforceLimits;
//		SegmentProperties[b].Weight = rig->SolverDef.Bones[b].Weight;
//		SegmentProperties[b].EnableLimits = rig->SolverDef.Bones[b].EnableLimits;
//		SegmentProperties[b].LimitsGain = rig->SolverDef.Bones[b].LimitsGain;
//		SegmentProperties[b].EnableRetargeting = rig->SolverDef.Bones[b].EnableRetargeting;
//		SegmentProperties[b].RetargetingGain = rig->SolverDef.Bones[b].RetargetingGain;
//
//		PinSegmentProperties[b].BoneName = rig->SolverDef.Bones[b].Name;
//		PinSegmentProperties[b].ShowAsPin = false;
//	}
//	pCoeff = rig->SolverDef.PCoefficient;
//}

#if WITH_EDITOR
void UIKinemaRig::PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent)
{
	Super::PostEditChangeChainProperty(PropertyChangedEvent);

	UProperty* Property = PropertyChangedEvent.PropertyChain.GetHead()->GetValue();

	if (Property)
	{
		if (Property->GetName() == TEXT("SolverDef"))
		{
			UpdateVersion++;
		}
	}
}

bool UIKinemaRig::CreateRigFromSkeleton()
{
	if (!Skeleton)
	{
		return false;
	}
	//Create the SolverDef.
	SolverDef.InitializeValues();

	//Populate Bone structure

	auto refSkel = Skeleton->GetReferenceSkeleton().GetRefBoneInfo();
	auto refPoses = Skeleton->GetReferenceSkeleton().GetRefBonePose();

	for (int i = 0; i < refSkel.Num(); i++)
	{
		//Exclude the first bone, if it is located at the origin.
		if (i == 0 && refPoses[i].GetTranslation().Size() < 1.0f)
		{
			continue;
		}
		FIKinemaBoneDef bone;
		FName boneName = refSkel[i].Name;
		bone.SelfColision.CollisionShape.Shape = EIKNOTSET;
		bone.Characterisation = ENONE;
		int parentIndex = refSkel[i].ParentIndex;
		FName parentName = NAME_None;
		if (parentIndex != INDEX_NONE)
		{
			parentName = refSkel[parentIndex].Name;
		}

		const auto& refPose = refPoses[i];


		bone.InitializeValues();
		//Create Hieararchy
		bone.Name = boneName;
		bone.ParentName = parentName;
		bone.ParentIndex = parentIndex;

		bone.RestPose = refPose;
		SolverDef.Bones.Add(bone);

	}

	return true;

}

void UIKinemaRig::LinkSourceToTarget(const FMocapBone& sourceBone, FIKinemaBoneDef& targetBone, const FTransform& targetGlobal, const FTransform& targetParentGlobal)
{
	targetBone.SourceName = sourceBone.Name;
	FTransform sourceGlobal = sourceBone.GlobalTransform;
	FTransform targetGlobalMaya = targetGlobal*Actor->SourceTransform.Inverse();
	targetBone.RotFromSrc = (targetGlobalMaya*sourceGlobal.Inverse()).GetRotation();
	targetBone.EnableRetargeting = true;
	//Negate X and Z components to match Maya exports
	targetBone.RotFromSrc.X = -targetBone.RotFromSrc.X;
	targetBone.RotFromSrc.Z = -targetBone.RotFromSrc.Z;
	SolverDef.IsRetargeting = true;
	if (sourceBone.ParentName == NAME_None)
	{
		sourceGlobal = sourceBone.GlobalTransform;
		targetGlobalMaya = targetGlobal*Actor->SourceTransform.Inverse();

		targetBone.RotFromSrc = (targetGlobalMaya*sourceGlobal.Inverse()).GetRotation();
		targetBone.RotFromSrc.X = -targetBone.RotFromSrc.X;
		targetBone.RotFromSrc.Z = -targetBone.RotFromSrc.Z;
		targetBone.RotFromSrcPar = (targetParentGlobal.Inverse()).GetRotation();
		targetBone.RotFromSrcPar.X = -targetBone.RotFromSrcPar.X;
		targetBone.RotFromSrcPar.Z = -targetBone.RotFromSrcPar.Z;
		return;
	}
	auto sourceParentBone = Actor->GetBone(sourceBone.ParentName);
	targetGlobalMaya = targetParentGlobal*Actor->SourceTransform.Inverse();
	sourceGlobal = sourceParentBone.GlobalTransform;
	targetBone.RotFromSrcPar = (sourceGlobal * targetGlobalMaya.Inverse()).GetRotation();
	targetBone.RotFromSrcPar.X = -targetBone.RotFromSrcPar.X;
	targetBone.RotFromSrcPar.Z = -targetBone.RotFromSrcPar.Z;

}
IKINEMACORE_API void UIKinemaRig::CharacteriseBone(FName NodeName, EIKinemaCharacterDefinition CharacterisationIndex)
{
	//TODO enable penetration node tasks
	const int targetIndex = SolverDef.FindBoneIndex(NodeName);
	auto& target = SolverDef.Bones[targetIndex];

	target.Characterisation = CharacterisationIndex;
	auto& TargetSelfCol = target.SelfColision;
	if (target.Characterisation != EIKinemaCharacterDefinition::ENONE)
	{
		TargetSelfCol.bIsEnabled = true;
		TargetSelfCol.Task.bIsPenetrating = false;
		switch (target.Characterisation)
		{
		case EIKinemaCharacterDefinition::EELBOW:
		case EIKinemaCharacterDefinition::EHAND:
			TargetSelfCol.Task.bIsPenetrating = true;
			HandleSelfPenetrationChange(target);
			break;
		default:
			TargetSelfCol.Task.bIsPenetrating = false;

		}

	}

}


IKINEMACORE_API void UIKinemaRig::HandleSelfPenetrationChange(FIKinemaBoneDef& targetBone)
{
	if (targetBone.ParentIndex == INDEX_NONE)
	{
		UE_LOG(LogIKinemaCore, Warning, TEXT("Self collision on the root bone is enabled, can not auto generate colliding bones"));

		return;
	}
	if (targetBone.SelfColision.Task.bIsPenetrating && targetBone.SelfColision.Task.PenetrationSegments.Num() == 0)
	{
		for (auto bone : SolverDef.Bones)
		{
			//Do not add a bone with no shape or the same bone to the list of the current bones
			if (bone.Name != targetBone.Name && bone.SelfColision.CollisionShape.Shape != EIKNONE)
			{

				//Do not add the parent and grand parent to the list
				if (targetBone.ParentName != bone.Name && SolverDef.Bones[targetBone.ParentIndex].ParentName != bone.Name)
				{
					targetBone.SelfColision.Task.PenetrationSegments.Push(bone.Name);
				}
			}
		}
	}


	//Order the collider lowest in skeleton first
	auto PreviewScene = FPreviewScene(FPreviewScene::ConstructionValues().ShouldSimulatePhysics(true));
	USkeletalMeshComponent* EditorSkelComp = NewObject<USkeletalMeshComponent>();
	auto skelPrev = Skeleton->GetPreviewMesh(true);
	EditorSkelComp->SetAnimationMode(EAnimationMode::Type::AnimationSingleNode);
	EditorSkelComp->SetSkeletalMesh(skelPrev);


	PreviewScene.AddComponent(EditorSkelComp, FTransform::Identity);
	EditorSkelComp->TickPose(0.01f, false);
	auto colliders = targetBone.SelfColision.Task.PenetrationSegments;
	for (int i = 1; i < colliders.Num(); i++)
	{
		auto collider = colliders[i];
		if (collider == targetBone.Name)
		{

			//Warn user, remove it and break from the loop
			FMessageDialog::Open(EAppMsgType::Ok, LOCTEXT("CollidingWithSelf", "Can not set a bone to collide with itself"));
			colliders.RemoveAt(i);
			break;

		}
		int j = i - 1;
		int index = EditorSkelComp->GetBoneIndex(collider);
		int sortedIndex = EditorSkelComp->GetBoneIndex(colliders[j]);
		if (sortedIndex == INDEX_NONE || index == INDEX_NONE)
		{
			continue;
		}
		auto Colheight = EditorSkelComp->GetComponentSpaceTransforms()[index].GetTranslation().Z;

		auto sortedHeight = EditorSkelComp->GetComponentSpaceTransforms()[sortedIndex].GetTranslation().Z;
		while (j >= 0 && sortedHeight > Colheight)
		{
			auto temp = colliders[j + 1];
			colliders[j + 1] = colliders[j];
			j = j - 1;
			if (j < 0)
			{
				break;
			}
			sortedIndex = EditorSkelComp->GetBoneIndex(colliders[j]);
			if (sortedIndex == INDEX_NONE)
			{
				continue;
			}
			sortedHeight = EditorSkelComp->GetComponentSpaceTransforms()[sortedIndex].GetTranslation().Z;
		}
		colliders[j + 1] = collider;
	}
	targetBone.SelfColision.Task.PenetrationSegments = colliders;
}


#endif