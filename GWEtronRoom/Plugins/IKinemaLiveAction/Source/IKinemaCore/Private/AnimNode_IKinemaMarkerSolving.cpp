/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "AnimNode_IKinemaMarkerSolving.h"
#include "IKinemaCorePrivatePCH.h"

FAnimNode_IKinemaMarkerSolving::FAnimNode_IKinemaMarkerSolving() :
FAnimNode_IKinemaSolver()
{
}

FAnimNode_IKinemaMarkerSolving::~FAnimNode_IKinemaMarkerSolving()
{
}

void FAnimNode_IKinemaMarkerSolving::SetAsset(class UIKinemaRig* rig)
{
	FAnimNode_IKinemaSolver::SetAsset(rig);
	//Enable all position tasks
	for (int i = 0; i < Tasks.Num(); i++)
	{
		Tasks[i].EnableTranslation = true;
		Tasks[i].DebugDraw = false;
	}
}

void FAnimNode_IKinemaMarkerSolving::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	check(Context.AnimInstanceProxy != nullptr);
	USkeleton* skeleton = Context.AnimInstanceProxy->GetSkeleton();
	if (skeleton != nullptr)
	{
		mSkeletonBinding.BindToSkeleton(IKinemaRig, skeleton, Context.AnimInstanceProxy->GetSkelMeshComponent());
	}
	mSolverInstance.Create(mSkeletonBinding);

	//construct rigid markers from markers connected to a bone
	for (int i = 0; i < IKinemaRig->SolverDef.Tasks.Num(); i++)
	{
		//Add markers to figid body groups
		Tasks[i].DebugDraw = false;
	}
	for (int i = 0; i < IKinemaRig->SolverDef.Bones.Num(); i++)
	{
		//Add markers to figid body groups
		PinSegmentProperties[i].ShowAsPin = true;
	}
	// Forward to the incoming pose link.
	InPose.Initialize(Context);
}

void FAnimNode_IKinemaMarkerSolving::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	IKinemaRig->EnableAutoTune = AutoTune;
	FAnimNode_IKinemaSolver::Update_AnyThread(Context);
}

void FAnimNode_IKinemaMarkerSolving::Evaluate_AnyThread(FPoseContext& Output)
{

	if (IKinemaRig == nullptr)
	{
		return;
	}

	check(Output.AnimInstanceProxy->GetSkeleton() != nullptr);

	// Make sure we have just the right number of input bones.
	const int32 numTaskBones = IKinemaRig->SolverDef.Tasks.Num();
	if (numTaskBones < 1)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("IKinemaRig %s for IKinemaMarkerSolving node expected at least one bone in the skeleton"),
			*IKinemaRig->GetName());
		return;
	}
	TArray<FTransform> RefBones;
	for (int i = 0; i < Output.Pose.GetNumBones(); i++)
	{
		RefBones.Push(Output.Pose.GetRefPose(FCompactPoseBoneIndex(i)));
	}
	InPose.Evaluate(Output);
	TArray<FTransform> Bones;
	Bones = Output.Pose.GetBones();
	Output.Pose.SetBoneContainer(&Output.AnimInstanceProxy->GetRequiredBones());

	TArray<bool> isLocked;
	isLocked.Init(false, IKinemaRig->SolverDef.Bones.Num());

	for (int i = 0; i < numTaskBones; i++)
	{
		FTransform trans = Bones[IKinemaRig->SolverDef.Tasks[i].MarkerIndex];


		trans.SetScale3D(FVector(1, 1, 1));
		trans.SetTranslation(trans.GetTranslation() * IKinemaRig->SolverDef.SourceScale);
		Tasks[i].DebugDraw = DebugDraw;
		Tasks[i].Target = trans;
	}
	
	if (DebugDraw)
	{
		FTransform world = Output.AnimInstanceProxy->GetSkelMeshComponent()->GetComponentTransform();
		for (int32 i = 0; i < Bones.Num(); i++)
		{

			FVector Loc = world.TransformPosition(Bones[i].GetTranslation() * IKinemaRig->SolverDef.SourceScale);
			FRotator Rot = (Bones[i].GetRotation() * world.GetRotation()).Rotator();

			DrawDebugCoordinateSystem(GWorld, Loc, Rot, 10.0f, false, -1.0f, SDPG_Foreground);
		}
	}
	mSolverInstance.UpdateAndSolve(Tasks, TaskProperties, PinTaskProperties,
		SegmentProperties, PinSegmentProperties, Bones, Output);
}
