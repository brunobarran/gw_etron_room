/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once



//#include "CoreUObject.h"
//#include "Engine.h"
//Removing monolithic headers (Engine.h/CoreUObject.h) replacing with IWYU
#include "DrawDebugHelpers.h"
#include "HAL/FileManager.h"
#include "Misc/MessageDialog.h"

#include "AnimationRuntime.h"
#include "Animation/AnimInstanceProxy.h"

// You should place include statements to your module's private header files here.  You only need to
// add includes for headers that are used in most of your module's source files though.
#include "Modules/ModuleManager.h"
//#if !UE_ROCKET
//#include "EngineAnimationNodeClasses.h"
//#endif
#include "IKinemaCoreClasses.h"
#include "LADataStreamCoreClasses.h"
#include "IKinemaCoreModule.h"

DECLARE_LOG_CATEGORY_EXTERN(LogIKinemaCore, Warning, All);
