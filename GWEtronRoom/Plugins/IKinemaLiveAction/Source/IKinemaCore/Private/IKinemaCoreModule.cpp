/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaCoreModule.h"
#include "IKinemaCorePrivatePCH.h"
#include "Interfaces/IPluginManager.h"
#include "LADataStreamModule.h"
#include <IKSolver.h>
#include <IKinemaKey.h>
DEFINE_LOG_CATEGORY(LogIKinemaCore);




#if WITH_IKINEMA_LIBS

#include "IKMemory.h"

// IKinemaCore module implementation

static FIK::FIKAllocCB sIKinemaOldAllocCB = nullptr;
static FIK::FIKFreeCB sIKinemaOldFreeCB = nullptr;

HMODULE IKinemaDLL = nullptr;

static void* IKinemaMemoryAlloc(unsigned long long size, unsigned long long /*alignment*/)
{
	return FMemory::Malloc(size); 
}

static void IKinemaMemoryFree(void* memblock)
{
	FMemory::Free(memblock);  
}

#endif // WITH_IKINEMA_LIBS


void FIKinemaCoreModule::StartupModule()
{

	FString path;
	FString PluginsPath = IPluginManager::Get().FindPlugin("IKinemaLiveAction")->GetBaseDir();
	if (IKinemaDLL == NULL)
	{
		path = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(PluginsPath / TEXT("Source/ThirdParty/IKinema/CoreSolver/sdk/lib/Win64/IKinema.dll")));
		IKinemaDLL = LoadLibraryW(*(path));
		if (!IKinemaDLL)
		{
			UE_LOG(LogIKinemaCore, Fatal, TEXT("Failed to load Xsens Type Dlls."));
			return;
		}
		path = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(PluginsPath / TEXT("Source/ThirdParty/IKinema/ACP/sdk/lib/Win64/ACPLib.dll")));
		IKinemaDLL = LoadLibraryW(*(path));
		if (!IKinemaDLL)
		{
			UE_LOG(LogIKinemaCore, Fatal, TEXT("Failed to load Xsens Type Dlls."));
			return;
		}
	}



#if WITH_IKINEMA_LIBS
	// Redirect memory allocation to UE4, after remembering the old settings.
	//sIKinemaOldAllocCB = FIK::FIKAlloc;
	//sIKinemaOldFreeCB = FIK::FIKFree;
	FIK::SetMemoryRoutines(&IKinemaMemoryAlloc, &IKinemaMemoryFree, 128);
#endif // WITH_IKINEMA_LIBS

	FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(FPaths::ConvertRelativePathToFull(FPaths::EngineDir() + TEXT("Binaries/ThirdParty/IKinema/license"))));

	FIK::IKinemaInit(STUDIO_NAME, "UE4Game", TCHAR_TO_ANSI(*licenseRelativeFolder), IKINEMA_SIGNATURE);

}

void FIKinemaCoreModule::ShutdownModule()
{
#if WITH_IKINEMA_LIBS
	// Restore memory callbacks.
	//FIK::SetMemoryRoutines(sIKinemaOldAllocCB, sIKinemaOldFreeCB, DEFAULT_ALIGNMENT);
	//sIKinemaOldAllocCB = 0;
	//sIKinemaOldFreeCB = 0;

	if (IKinemaDLL)
	{
		FreeLibrary(IKinemaDLL);
	}

#endif // WITH_IKINEMA_LIBS
}



FGraphEventRef ExecOnGameThread(TFunction<void()> funcLambda)
{
	FGraphEventRef funcTask = FFunctionGraphTask::CreateAndDispatchWhenReady(funcLambda, TStatId(), NULL, ENamedThreads::GameThread);
	return funcTask;
}

IMPLEMENT_MODULE( FIKinemaCoreModule, IKinemaCore );


