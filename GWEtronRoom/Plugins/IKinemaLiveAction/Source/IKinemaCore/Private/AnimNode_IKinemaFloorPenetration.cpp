/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "AnimNode_IKinemaFloorPenetration.h"
#include "IKinemaCorePrivatePCH.h"
#include "ACP/FloorLevel.h"

void FAnimNode_IKinemaFloorPenetration::CalculateTaskTarget(FCSPose<FCompactPose>& MeshBases, USkeletalMeshComponent* SkelComp, FName& Root, FName TaskName, bool DrawDebug)
{

	auto FloorNode = (FIK::ACP::FloorLevel*)(Node);
	auto RootBoneIndex = MeshBases.GetPose().GetBoneContainer().GetPoseBoneIndexForBoneName(Root);
	auto TaskBoneIndex = MeshBases.GetPose().GetBoneContainer().GetPoseBoneIndexForBoneName(TaskName);
	FTransform root = MeshBases.GetComponentSpaceTransform((FCompactPose::BoneIndexType(RootBoneIndex)));
	FTransform task = MeshBases.GetComponentSpaceTransform(FCompactPose::BoneIndexType(TaskBoneIndex));
	FCompactPose::BoneIndexType ParentIndex(INDEX_NONE);

	if (SkelComp->GetParentBone(Root) != NAME_None)
	{
		ParentIndex = MeshBases.GetPose().GetParentBoneIndex(FCompactPose::BoneIndexType(RootBoneIndex));
	}
	if (ParentIndex == INDEX_NONE)
	{
		FAnimationRuntime::ConvertCSTransformToBoneSpace(FTransform::Identity, MeshBases, task, FCompactPose::BoneIndexType(TaskBoneIndex), BCS_ComponentSpace);
	}
	else
	{
		FAnimationRuntime::ConvertCSTransformToBoneSpace(FTransform::Identity, MeshBases, task, ParentIndex, BCS_BoneSpace);
	}

	FTransform ComponentToWorld = SkelComp->GetComponentToWorld();
	FTransform SolverToComponent, SolverSpaceTM;
	FVector pN, pP;
	root = root * ComponentToWorld;
	if (ParentIndex != INDEX_NONE)
	{
		SolverToComponent = MeshBases.GetComponentSpaceTransform(ParentIndex);
	}


	if (true)
	{
		//Ray Trace from Task Location + Set the normal and position to the return from the trace
		FTransform TaskWorld = task * SolverToComponent * SkelComp->GetComponentTransform();
		FVector p = TaskWorld.GetLocation();

		if (DrawDebug)
		{
			::DrawDebugSphere(SkelComp->GetWorld(), p, 5, 25, FColor::Red);
		}
		FCollisionQueryParams Params(TaskName, true, SkelComp->GetOwner());
		Params.bReturnPhysicalMaterial = true;
		Params.bTraceAsyncScene = true;
		Params.AddIgnoredComponent(SkelComp);
		//reset
		FHitResult OutHit = FHitResult();
		bool bHit = SkelComp->GetWorld()->LineTraceSingleByChannel(OutHit, FVector(p.X, p.Y, p.Z + (root.GetTranslation().Z - p.Z)), FVector(p.X, p.Y, p.Z - 200), ECollisionChannel::ECC_WorldStatic, Params, FCollisionResponseParams());
		if (DrawDebug)
		{
			if (bHit && OutHit.bBlockingHit)
			{
				::DrawDebugLine(SkelComp->GetWorld(), FVector(p.X, p.Y, p.Z + (root.GetTranslation().Z - p.Z)), OutHit.ImpactPoint, FColor::Red);
				::DrawDebugLine(SkelComp->GetWorld(), OutHit.ImpactPoint, FVector(p.X, p.Y, p.Z - 200), FColor::Green);
				::DrawDebugPoint(SkelComp->GetWorld(), OutHit.ImpactPoint, 10.0, FColor::Red);
			}
			else
			{
				::DrawDebugLine(SkelComp->GetWorld(), FVector(p.X, p.Y, p.Z + (root.GetTranslation().Z - p.Z)), FVector(p.X, p.Y, p.Z - 200), FColor::Green);
			}
		}
		SolverSpaceTM = ComponentToWorld.Inverse();
		SolverSpaceTM.SetToRelativeTransform(SolverToComponent);
		pN = SolverSpaceTM.TransformVector(OutHit.ImpactNormal);
		pP = SolverSpaceTM.TransformPosition(OutHit.ImpactPoint);

	}
	pN.Normalize();
	IKString n(TCHAR_TO_ANSI(*TaskName.ToString()));
	FloorNode->SetTaskPlaneNormal(n, FIK::Vector(pN.X, pN.Y, pN.Z));
	FloorNode->SetTaskPlanePosition(n, FIK::Vector(pP.X, pP.Y, pP.Z));

}


FAnimNode_IKinemaFloorPenetration::FAnimNode_IKinemaFloorPenetration() :
	UpdateVersion(0),
	iter(150),
	DebugDraw(false),
	DebugColor(FColor::Blue), IKinemaRig(nullptr),
	IsLicValid(true)
{
}

FAnimNode_IKinemaFloorPenetration::~FAnimNode_IKinemaFloorPenetration()
{
	ClearSolver();
}


void FAnimNode_IKinemaFloorPenetration::ClearSolver()
{
	if (Node)
	{
		delete Node;
		Node = nullptr;
	}
}



void FAnimNode_IKinemaFloorPenetration::CreateSolver(const USkeleton* Skeleton, const USkeletalMeshComponent* SkelComp)
{
	// Forward to the incoming pose link.
	ue4Index.Empty();
	TaskNames.Empty();
	//Create the IK heirarchy, we will use the exact heirarchy as the skeleton
	//Loop through all the bones in the skeleton


	if (!IKinemaRig)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("No reference to IKinemaRig in the node"));
		return;
	}

	auto rig = IKinemaRig->CreateACPRigFromRig(Skeleton, SkelComp, ue4Index);

	//Create a task array to use
	TArray<FIK::ACP::FloorLevel::FloorTask> Tasks;
	for (auto& BoneDef : IKinemaRig->SolverDef.Bones)
	{
		if (BoneDef.bFloorLevelCompensation)
		{
			FIK::ACP::FloorLevel::FloorTask task(TCHAR_TO_ANSI(*BoneDef.Name.ToString()));
			task.MeshOffset = BoneDef.MeshOffset;
			Tasks.Push(task);
			TaskNames.Add(BoneDef.Name);
		}
	}

	//Construct the solver
	//FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(TEXT("../../Binaries/ThirdParty/IKinema/license"));
	FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(FPaths::ConvertRelativePathToFull(FPaths::EngineDir() + TEXT("Binaries/ThirdParty/IKinema/license"))));
	char* errorMessage = nullptr;
	if (Tasks.IsValidIndex(0))
	{
		Node = FIK::ACP::FloorLevel::Create(rig, TCHAR_TO_ANSI(*licenseRelativeFolder), &errorMessage, &Tasks[0], Tasks.Num());
	}
	if (Node == nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Could not create IKinema solver. Constructor returned null"));
		return;
	}

	if (errorMessage != nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Could not create IKinema solver. Constructor returned error: %s"), ANSI_TO_TCHAR(errorMessage));
		IsLicValid = false;
		delete Node;
		Node = nullptr;
	}
	//Set the normal for the hips task to -Y as this points up in Unreal

	int32 ParentIndex = Skeleton->GetReferenceSkeleton().GetParentIndex(ue4Index[0]);
	FTransform mSolverToComponentSpace = FTransform::Identity;
	while (ParentIndex != INDEX_NONE)
	{
		mSolverToComponentSpace *= Skeleton->GetRefLocalPoses()[ParentIndex];//Bones[ParentIndex];
		ParentIndex = Skeleton->GetReferenceSkeleton().GetParentIndex(ParentIndex);
	}
	auto upVector = mSolverToComponentSpace.GetRotation().GetUpVector();
	upVector.X = upVector.X * rig.GetBone(0).RestTranslation[0];
	upVector.Y = upVector.Y * rig.GetBone(0).RestTranslation[1];
	upVector.Z = upVector.Z * rig.GetBone(0).RestTranslation[2];
	upVector.Normalize();
	auto FloorNode = (FIK::ACP::FloorLevel*)(Node);
	FloorNode->SetTaskPlaneNormal(rig.GetBone(0).Name, FIK::Vector(upVector.X, upVector.Y, upVector.Z));

}

void FAnimNode_IKinemaFloorPenetration::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	// Forward to the incoming pose link.
	InPose.Initialize(Context);
	ClearSolver();

	check(Context.AnimInstanceProxy != nullptr);
	USkeleton* skeleton = Context.AnimInstanceProxy->GetSkeleton();

	USkeletalMeshComponent* component = Context.AnimInstanceProxy->GetSkelMeshComponent();

	CreateSolver(skeleton, component);

}



void FAnimNode_IKinemaFloorPenetration::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	if (IKinemaRig && UpdateVersion < IKinemaRig->UpdateVersion)
	{
		UpdateVersion = IKinemaRig->UpdateVersion;

		//Recreate the solver
		ClearSolver();
		CreateSolver(Context.AnimInstanceProxy->GetSkeleton(), Context.AnimInstanceProxy->GetSkelMeshComponent());
	}

	InPose.Update(Context);

	// Handle non-pose pins.
	EvaluateGraphExposedInputs.Execute(Context);

	if (Node)
	{
		Node->SetFrameRate(1.f / Context.GetDeltaTime());
		Node->SetNumOfIterations(iter);
	}

}


void FAnimNode_IKinemaFloorPenetration::Evaluate_AnyThread(FPoseContext& Output)
{
	InPose.Evaluate(Output);
	if (!IsLicValid)
	{
		Output.ResetToRefPose();
		return;
	}
	if (!Node)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("IKinema Feet Sliding Node: IKinema Solver is not created"));
		return;
	}

	//Get component space transforms
	FCSPose<FCompactPose>  MeshBases;
	MeshBases.InitPose(Output.Pose);

	for (auto&& taskName : TaskNames)
	{
		CalculateTaskTarget(MeshBases, Output.AnimInstanceProxy->GetSkelMeshComponent(), IKinemaRig->SolverDef.Bones[0].Name, taskName, DebugDraw);
	}

	SolveAndUpdateACPPose(Node, ue4Index, Output);

}

void FAnimNode_IKinemaFloorPenetration::CacheBones_AnyThread(const FAnimationCacheBonesContext & Context)
{
	InPose.CacheBones(Context);
}
