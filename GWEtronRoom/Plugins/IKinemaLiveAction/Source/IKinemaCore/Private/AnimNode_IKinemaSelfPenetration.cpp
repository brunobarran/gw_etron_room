/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "AnimNode_IKinemaSelfPenetration.h"
#include "IKinemaStatsHelper.h"

DECLARE_STATS_GROUP(TEXT("SelfPenetration"), STATGROUP_SelfPenetration, STATCAT_Advanced);


FIKinemaSelfPenetrationTask::FIKinemaSelfPenetrationTask()
{
	m_Pos = nullptr;
	m_Rot = nullptr;
	BoneRef = FBoneReference();
	ue4Index = 0;
	MeshOffset = 0.0f;
	PenetrationSegments.Empty();

	bPenetrating= false;
	FilterAlpha = false;
	
}

FIKinemaSelfPenetrationTask::FIKinemaSelfPenetrationTask(const FIKinemaSelfPenetrationTask& other)
{
	m_Pos = other.m_Pos;
	m_Rot = other.m_Rot;
	BoneRef = other.BoneRef;
	ue4Index = other.ue4Index;
	MeshOffset = other.MeshOffset;
	PenetrationSegments = other.PenetrationSegments;

	bPenetrating = other.bPenetrating;
	FilterAlpha = other.FilterAlpha;

	PWeight = other.PWeight;
	RWeight = other.RWeight;
	PDepth = other.PDepth;
	RDepth = other.RDepth;
}

FIKinemaSelfPenetrationTask& FIKinemaSelfPenetrationTask::operator=(const FIKinemaSelfPenetrationTask& other)
{
	m_Pos = other.m_Pos;
	m_Rot = other.m_Rot;
	BoneRef = other.BoneRef;
	ue4Index = other.ue4Index;
	MeshOffset = other.MeshOffset;
	PenetrationSegments = other.PenetrationSegments;

	bPenetrating = other.bPenetrating;
	FilterAlpha = other.FilterAlpha;
	PWeight = other.PWeight;
	RWeight = other.RWeight;
	PDepth = other.PDepth;
	RDepth = other.RDepth;
	return *this;
}

bool FIKinemaSelfPenetrationTask::createTask(FIK::IKSolver* solver)
{
	if (!solver || bIsCreated)
	{
		return false;
	}

	FIK::IKSegment* pSeg = solver->getSegment(TCHAR_TO_ANSI(*BoneRef.BoneName.ToString()));
	if (pSeg)
	{
		
			m_Pos = solver->addPositionTask(TCHAR_TO_ANSI(*BoneRef.BoneName.ToString()));
		
		m_Pos->setDofWeight(0, PWeight.X);
		m_Pos->setDofWeight(1, PWeight.Y);
		m_Pos->setDofWeight(2, PWeight.Z);
		m_Pos->setLength(PDepth);
		m_Pos->setPrecision(2.0f);
		m_Pos->AdjustNormalFilter(FilterAlpha);
		
		m_Rot = solver->addOrientationTask(TCHAR_TO_ANSI(*BoneRef.BoneName.ToString()));

		m_Rot->setDofWeight(0, RWeight.X);
		m_Rot->setDofWeight(1, RWeight.Y);
		m_Rot->setDofWeight(2, RWeight.Z);
		m_Rot->setLength(RDepth);
		m_Rot->setPrecision(2.0f);
		m_Pos->setWithPenetrationTask(bPenetrating);


		//If penetrating, assign all the segments.
		if (bPenetrating)
		{
			for (auto segmentName : PenetrationSegments)
			{
				auto t = segmentName.ToString();
				auto seg = solver->getSegment(TCHAR_TO_ANSI(*t));
				if (seg)
				{
					m_Pos->addCollisionSegment(seg);
				}
			}
			m_Pos->SetMeshOffset(MeshOffset);
		}


		//Move one bone up the chain
		/*pSeg = pSeg->Parent();
		if (pSeg)
		{
			pSeg->setRetargetingGain(0.1f);
			pSeg = pSeg->Parent();
			if (pSeg)
			{
				pSeg->setRetargetingGain(0.1f);
			}
		}*/
		bIsCreated = true;
		return true;
	}

	return false;
}


void FIKinemaSelfPenetrationTask::SetTaskTarget(FCSPose<FCompactPose>&  MeshBases, USkeletalMeshComponent* SkelComp, FBoneReference& Root, float Alpha)
{
	FTransform task = MeshBases.GetComponentSpaceTransform(FCompactPose::BoneIndexType(BoneRef.BoneIndex));
	FCompactPose::BoneIndexType ParentIndex(INDEX_NONE);
	if (SkelComp->GetParentBone(Root.BoneName) != NAME_None)
	{
		ParentIndex = MeshBases.GetPose().GetParentBoneIndex(FCompactPose::BoneIndexType(Root.BoneIndex));
	}
	if (ParentIndex == INDEX_NONE)
	{
		FAnimationRuntime::ConvertCSTransformToBoneSpace(FTransform::Identity, MeshBases, task, FCompactPose::BoneIndexType(BoneRef.BoneIndex), BCS_ComponentSpace);
	}
	else
	{
		FAnimationRuntime::ConvertCSTransformToBoneSpace(FTransform::Identity, MeshBases, task, ParentIndex, BCS_BoneSpace);
	}

	if (m_Rot)
	{
		FQuat q = task.GetRotation();
		m_Rot->setTarget(q.X, q.Y, q.Z, q.W);
	}

	{
		FVector v = task.GetLocation();
		m_Pos->setTarget(v.X, v.Y, v.Z);
	}



}

FAnimNode_IKinemaSelfPenetration::FAnimNode_IKinemaSelfPenetration() :
	m_Solver(nullptr), UpdateVersion(0),
	AutoTune(true),
	pCoeff(0.2f), DebugDraw(false),
	IKinemaRig(nullptr)
{
}

FAnimNode_IKinemaSelfPenetration::~FAnimNode_IKinemaSelfPenetration()
{
	ClearSolver();
}

void FAnimNode_IKinemaSelfPenetration::SetAsset(class UIKinemaRig* rig)
{
	IKinemaRig = rig;

	//create as many penetration tasks as bone with penetration in the rig
	if (IKinemaRig)
	{
		ClearSolver();
		Tasks.Empty();

		for (const auto& bone : IKinemaRig->SolverDef.Bones)
		{
			if (bone.SelfColision.bIsEnabled)
			{
				FIKinemaSelfPenetrationTask task;
				task.BoneRef.BoneName = bone.Name;
				task.MeshOffset = 0;
				task.bPenetrating = false;
				if (bone.SelfColision.Task.bIsPenetrating)
				{
					task.MeshOffset = bone.SelfColision.Task.SelfPenetrationMeshOffset;
					task.PenetrationSegments = bone.SelfColision.Task.PenetrationSegments;
					task.bPenetrating = true;
					task.FilterAlpha = bone.SelfColision.Task.FilterAlpha;
					task.PDepth = bone.SelfColision.Task.PDepth;
					task.RDepth = bone.SelfColision.Task.RDepth;
					task.PWeight = bone.SelfColision.Task.PWeight;
					task.RWeight = bone.SelfColision.Task.RWeight;
				}
				Tasks.Push(task);
			}
		}

		IKinemaRoot.BoneName = IKinemaRig->SolverDef.Bones[0].Name;

	}
}


void FAnimNode_IKinemaSelfPenetration::ClearSolver()
{
	m_HipsTask.bIsCreated = false;
	for (auto& task : Tasks)
	{
		task.bIsCreated = false;
	}
	if (m_Solver.IsValid())
	{
		if (m_HipsTask.m_Pos)
		{
			m_Solver->removeTask(m_HipsTask.m_Pos);
			m_HipsTask.m_Pos = nullptr;
		}
		if (m_HipsTask.m_Rot)
		{
			m_Solver->removeTask(m_HipsTask.m_Rot);
			m_HipsTask.m_Rot = nullptr;
		}
		for (auto& task : Tasks)
		{
			if (task.m_Pos)
			{
				m_Solver->removeTask(task.m_Pos);
				task.m_Pos = nullptr;
			}
			if (task.m_Rot)
			{
				m_Solver->removeTask(task.m_Rot);
				task.m_Rot = nullptr;
			}
		}
	}
}


void FAnimNode_IKinemaSelfPenetration::ApplyAndUpdateSolverSettings()
{
	if (!m_Solver.IsValid())
	{
		return;
	}
	m_Solver->translateRoot(true);

	//Enable retargeting for all bones
	if (IKinemaRig)
	{
		for (const auto& SegmentDef : IKinemaRig->SolverDef.Bones)
		{
			auto pSeg = m_Solver->getSegment(TCHAR_TO_ANSI(*SegmentDef.Name.ToString()));
			if (pSeg)
			{
				SegmentDef.ConfigureIKSegment(*pSeg, false);
				pSeg->enableRetargeting(true);
				pSeg->setRetargetingGain(.1f);
			}
		}
		m_Solver->setP(IKinemaRig->SolverDef.PCoefficient);
	}

	m_Solver->setPrecision(-2.0f);
	m_Solver->setup();
	m_Solver->setP(pCoeff);
	m_Solver->updateFK();
	m_Solver->setSolutionTolerance(0.00001);
	if (IKinemaRig)
	{
		FIK::IKSegment* rootSegment = m_Solver->getRootSegment();
		if (rootSegment != nullptr)
		{
			rootSegment->setActiveDofs(true, true, true, false);
		}
		m_Solver->translateRoot(true);
		rootSegment->setTranslating(true, false);
		rootSegment->resetStretch(false);
	}
}



void FAnimNode_IKinemaSelfPenetration::CreateTasks()
{
	//Create a hips task
	if (m_Solver.IsValid() && !m_HipsTask.bIsCreated )
	{
		
		if (m_Solver->getRootSegment())
		{
			std::string rootName = m_Solver->getRootSegment()->getName();
			m_HipsTask.m_Pos = m_Solver->addPositionTask(rootName.c_str());
			m_HipsTask.m_Rot = m_Solver->addOrientationTask(rootName.c_str());

			m_HipsTask.m_Pos->setWeight(50.0f); // 3
			m_HipsTask.m_Pos->setPrecision(2.0f); // 2
			
			m_HipsTask.m_Rot->setPrecision(2.0f); // 2
			m_HipsTask.m_Rot->setWeight(50.0f); // 3
			m_HipsTask.BoneRef = IKinemaRoot;
			m_HipsTask.bPenetrating = false;

			m_HipsTask.bIsCreated = true;
		}
	}

	//Initialize task setup as well
	for (int i = 0; i < Tasks.Num(); i++)
	{
		Tasks[i].createTask(m_Solver.Get());
	}
}

void FAnimNode_IKinemaSelfPenetration::CreateSolver(const USkeleton* Skeleton, const USkeletalMeshComponent* SkelComp)
{
	//Construct the solver
	//FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(TEXT("../../Binaries/ThirdParty/IKinema/license"));
	FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(FPaths::ConvertRelativePathToFull(FPaths::EngineDir() + TEXT("Binaries/ThirdParty/IKinema/license"))));
	char* errorMessage = nullptr;
	FIK::IKSolver* solver = new FIK::IKSolver(TCHAR_TO_ANSI(*licenseRelativeFolder), &errorMessage);
	if (solver == nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Could not create IKinema solver. Constructor returned null"));
		return;
	}
	if (errorMessage != nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Could not create IKinema solver. Constructor returned error: %s"), ANSI_TO_TCHAR(errorMessage));
		delete solver;
		return;
	}

	m_Solver = MakeShareable(solver);
	TArray<ANSICHAR*> names;
	TArray<ANSICHAR*> parentNames;
	TArray<FIK::ImportBone> importBones;
	if (IKinemaRig)
	{
		for (auto& BoneDef : IKinemaRig->SolverDef.Bones)
		{
			FIK::ImportBone bone;
			FName Name = BoneDef.Name;
			const int32	ue4SkeletonBoneIndex = Skeleton->GetReferenceSkeleton().FindBoneIndex(Name);
			const int32 ue4BoneIndex = SkelComp->GetBoneIndex(Name);

			{
				ANSICHAR* m = new ANSICHAR[512];
				FGenericPlatformMemory::Memcpy(m, TCHAR_TO_ANSI(*Name.ToString()), Name.ToString().Len() + 1); //m = TCHAR_TO_ANSI(*Name.ToString());
				names.Push(m);
				bone.name = m;
			}
			{
				FName parentName = SkelComp->GetParentBone(Name);
				ANSICHAR* m = new ANSICHAR[512];
				FGenericPlatformMemory::Memcpy(m, TCHAR_TO_ANSI(*parentName.ToString()), parentName.ToString().Len() + 1);
				parentNames.Push(m);
				bone.parentname = m;
			}

			FTransform restPose = Skeleton->GetRefLocalPoses()[ue4SkeletonBoneIndex];
			const FVector& restTrans = restPose.GetTranslation();
			bone.rest_offset[0] = restTrans.X;
			bone.rest_offset[1] = restTrans.Y;
			bone.rest_offset[2] = restTrans.Z;

			const FQuat& restQuat = restPose.GetRotation();
			FIK::Quaternion q(restQuat.X, restQuat.Y, restQuat.Z, restQuat.W);
			q.normalize();

			//local orientation of the bone in the "bind pose", format: quaternion {w,x,y,z}
			bone.rest_orientation[0] = q[3];
			bone.rest_orientation[1] = q[0];
			bone.rest_orientation[2] = q[1];
			bone.rest_orientation[3] = q[2];
			importBones.Push(bone);
			ue4Index.Push(ue4BoneIndex);

		}
	}

	m_Solver->importBones(importBones.GetData(), importBones.Num());

	//delete array of names and parentName
	for (int i = 0; i < names.Num(); i++)
	{
		if (names[i])
		{
			delete[] names[i];
			names[i] = nullptr;
		}
	}
	for (int i = 0; i < parentNames.Num(); i++)
	{
		if (parentNames[i])
		{
			delete[] parentNames[i];
			parentNames[i] = nullptr;
		}
	}
}


void FAnimNode_IKinemaSelfPenetration::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	// Forward to the incoming pose link.
	// Forward to the incoming pose link.
	InPose.Initialize(Context);
	ClearSolver();
	SetAsset(IKinemaRig);


	check(Context.AnimInstanceProxy != nullptr);
	USkeleton* skeleton = Context.AnimInstanceProxy->GetSkeleton();
	InitializeBoneReferences(Context.AnimInstanceProxy->GetRequiredBones());
	if ((!IsValidToEvaluate(skeleton, Context.AnimInstanceProxy->GetRequiredBones())))
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Could not Initialize IKinema Node: Please select the IKinemaRoot bone and end effector bones"));
		return;
	}


	//Create the IK heirarchy, we will use the exact heirarchy as the skeleton
	//Loop through all the bones in the skeleton
	USkeletalMeshComponent* component = Context.AnimInstanceProxy->GetSkelMeshComponent();



	CreateSolver(skeleton, component);
	ApplyAndUpdateSolverSettings();
}

void FAnimNode_IKinemaSelfPenetration::Update_AnyThread(const FAnimationUpdateContext& Context)
{	// Forward to the incoming pose link.
	
	if (IKinemaRig && UpdateVersion < IKinemaRig->UpdateVersion)
	{
		SetAsset(IKinemaRig);
		UpdateVersion = IKinemaRig->UpdateVersion;

		//Recreate the solver
		InitializeBoneReferences(Context.AnimInstanceProxy->GetRequiredBones());
		CreateSolver(Context.AnimInstanceProxy->GetSkeleton(), Context.AnimInstanceProxy->GetSkelMeshComponent());
		ApplyAndUpdateSolverSettings();
		CreateTasks();
	}

	if (m_Solver.IsValid())
	{
		if (!FIK::fuzzyZero(m_Solver->getP() - pCoeff))
		{
			m_Solver->setP(pCoeff);
		}
	}

	InPose.Update(Context);

	// Handle non-pose pins.
	EvaluateGraphExposedInputs.Execute(Context);
}



void FAnimNode_IKinemaSelfPenetration::Evaluate_AnyThread(FPoseContext& Output)
{
	DECLARE_SCOPE_CYCLE_COUNTER(TEXT("Evaluate"), STAT_StatsSelfPenetrationEval, STATGROUP_SelfPenetration);
	InPose.Evaluate(Output);
	

	if (!m_Solver.IsValid() || (!IsValidToEvaluate(Output.AnimInstanceProxy->GetSkeleton(), Output.AnimInstanceProxy->GetRequiredBones())))
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("IKinema Filtering Node: IKinema Solver is not created"));
		return;
	}
	if (!IKinemaRig) {
		UE_LOG(LogIKinemaCore, Error, TEXT("IKINEMA rig referenced in self penetration node is invalid"));
		return;
	}
	//Get component space transforms
	FCSPose<FCompactPose>  MeshBases;
	MeshBases.InitPose(Output.Pose);
	auto SkelMesh = Output.AnimInstanceProxy->GetSkelMeshComponent();
	m_HipsTask.SetTaskTarget(MeshBases, SkelMesh, m_HipsTask.BoneRef, 0);
	
	//get source joint data and pass as retargeting
	for (unsigned int i = 0; i < m_Solver->numSegments(); i++)
	{
		auto BoneIndex = Output.Pose.GetBoneContainer().MakeCompactPoseIndex(FMeshPoseBoneIndex(ue4Index[i]));
		FTransform trans = Output.Pose[BoneIndex];
		FQuat q = trans.GetRotation();
		FIK::IKSegment* pSeg = m_Solver->getSegmentByHandle(i);
		pSeg->setOrientation(q.X, q.Y, q.Z, q.W);
		pSeg->setTargetOrientation(q.X, q.Y, q.Z, q.W);
		if (i == 0)
		{
			auto t = trans.GetTranslation();
			pSeg->setTranslation(t.X, t.Y, t.Z);
		}

	}
	m_Solver->updateFK();

	
		for (int i = 0; i < Tasks.Num(); i++)
		{
			Tasks[i].SetTaskTarget(MeshBases, SkelMesh, m_HipsTask.BoneRef, 0);
		}


		static int iter = 150;
	if (AutoTune)
	{
		//AutoTuneSolver(m_Solver, iter, pCoeff);
		DECLARE_SCOPE_CYCLE_COUNTER(TEXT("Solve"), STAT_StatsSelfPenetrationSolve, STATGROUP_SelfPenetration);
		FIK::AutoTune::Enum Result;
		m_Solver->solve(iter, Result);
		if (Result != FIK::AutoTune::SOLVED)
		{
			UE_LOG(LogIKinemaCore, Warning, TEXT("IKinema solver has automatically tuned P-Gain to %f. Please ensure rig behaves correctly and save the IKinemaRig asset"), m_Solver->getP());
			pCoeff = m_Solver->getP();
			//m_Solver->resetSolver();
			
			//we tuned lets do another solve with update of FK 
			//get source joint data and pass as retargeting
			for (unsigned int i = 0; i < m_Solver->numSegments(); i++)
			{
				auto BoneIndex = Output.Pose.GetBoneContainer().MakeCompactPoseIndex(FMeshPoseBoneIndex(ue4Index[i]));
				FTransform trans = Output.Pose[BoneIndex];
				FQuat q = trans.GetRotation();
				FIK::IKSegment* pSeg = m_Solver->getSegmentByHandle(i);
				pSeg->setOrientation(q.X, q.Y, q.Z, q.W);
				pSeg->setTargetOrientation(q.X, q.Y, q.Z, q.W);
				if (i == 0)
				{
					auto t = trans.GetTranslation();
					pSeg->setTranslation(t.X, t.Y, t.Z);
				}

			}
			m_Solver->updateFK();

			m_Solver->solve(iter);

		}
	}
	else
	{
		m_Solver->solve(iter);
	}


	auto Skeleton = Output.AnimInstanceProxy->GetSkeleton()->GetReferenceSkeleton();
	//solver to component space matrix
	int32 RootBoneIndex = m_HipsTask.BoneRef.BoneIndex;
	//check(RootBoneIndex >= 0 && RootBoneIndex < segmentsCount);
	int32 ParentIndex = Skeleton.GetParentIndex(RootBoneIndex);
	FTransform mSolverToWorldSpace = FTransform::Identity;
	FTransform mSolverToComponentSpace = FTransform::Identity;

	while (ParentIndex != INDEX_NONE)
	{
		mSolverToComponentSpace *= Output.Pose[FCompactPose::BoneIndexType(ParentIndex)];
		ParentIndex = Skeleton.GetParentIndex(ParentIndex);
	}
	mSolverToWorldSpace = mSolverToComponentSpace * SkelMesh->GetComponentTransform();
	//Move data from solver to UE4 bone
	for (unsigned int i = 0; i < m_Solver->numSegments(); i++)
	{
		const FIK::IKSegment* segment = m_Solver->getSegmentByHandle(i);
		check(segment != nullptr);
		if (!segment->Active())
		{
			continue;
		}
		auto BoneIndex = Output.Pose.GetBoneContainer().MakeCompactPoseIndex(FMeshPoseBoneIndex(ue4Index[i]));
		FTransform ue4Transform = Output.Pose[BoneIndex];
		const FIK::Real *pos = segment->getPos();
		FVector trans;
		trans.X = pos[0];
		trans.Y = pos[1];
		trans.Z = pos[2];
		ue4Transform.SetTranslation(trans);
		const FIK::Real *quat = segment->getQ();
		FQuat rot;
		rot.X = quat[0];
		rot.Y = quat[1];
		rot.Z = quat[2];
		rot.W = quat[3];

		ue4Transform.SetRotation(rot);
		ue4Transform.NormalizeRotation();
		//if (!ue4Transform.ContainsNaN())
		{
			Output.Pose[BoneIndex] = ue4Transform;
		}
	}
	if (DebugDraw)
	{
		DebugDrawIKinema(m_Solver.Get(), mSolverToWorldSpace, FColor::Red, Output.AnimInstanceProxy->GetSkelMeshComponent()->GetWorld());
	}

}

void FAnimNode_IKinemaSelfPenetration::CacheBones_AnyThread(const FAnimationCacheBonesContext & Context)
{
	InitializeBoneReferences(Context.AnimInstanceProxy->GetRequiredBones());

	CreateTasks();
	InPose.CacheBones(Context);
}


void FAnimNode_IKinemaSelfPenetration::InitializeBoneReferences(const FBoneContainer & RequiredBones)
{
	for (int i = 0; i < Tasks.Num(); i++)
	{
		Tasks[i].BoneRef.Initialize(RequiredBones);
	}
	IKinemaRoot.Initialize(RequiredBones);


}

bool FAnimNode_IKinemaSelfPenetration::IsValidToEvaluate(const USkeleton * Skeleton, const FBoneContainer & RequiredBones)
{
	// Allow evaluation if all parameters are initialized and TipBone is child of RootBone
	bool valid = true;
	for (int i = 0; i < Tasks.Num(); i++)
	{
		valid = valid && Tasks[i].BoneRef.IsValidToEvaluate(RequiredBones);
	}

	return valid && IKinemaRoot.IsValidToEvaluate(RequiredBones);
}
