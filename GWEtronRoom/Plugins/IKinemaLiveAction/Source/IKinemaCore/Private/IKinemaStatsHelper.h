/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include "Stats/Stats.h"

// Utilities to generate a custom stat group id and range of stat ids.
// TODO: Move this to another shared module?

struct FIKinemaStatsConstants
{
	static const uint32 sLicenseeIndex = 0;
	static const uint32 sMaxStatsPerLicensee = 100;
	static const uint32 sStatIdOffset = sLicenseeIndex*sMaxStatsPerLicensee;
};

enum EIKinemaStatGroup
{
	EIKinemaStatGroup_GroupId = 1100,// STATGROUP_LicenseeFirstStatGroup + FIKinemaStatsConstants::sLicenseeIndex,
};

enum EIKinemaStat
{
	EIKinemaStat_InPoseEvaluate = 1100,//STAT_LicenseeFirstStat + FIKinemaStatsConstants::sStatIdOffset,
	EIKinemaStat_InputPrepare,
	EIKinemaStat_SolveAndUpdatePose,
	EIKinemaStat_SolverSolve,
	EIKinemaStat_IKinemaToUE4Pose,
};


DECLARE_STATS_GROUP(TEXT("IKinema"), STATGROUP_IKinema, STATCAT_IKinema);

DECLARE_CYCLE_STAT(TEXT("    IKinema To UE4 Pose"), STAT_StatsIKinemaToUE4Pose, STATGROUP_IKinema);
DECLARE_CYCLE_STAT(TEXT("    Solver Solve"), STAT_StatsSolverSolve, STATGROUP_IKinema);
DECLARE_CYCLE_STAT(TEXT("Solve And Update Pose"), STAT_StatsSolveAndUpdatePose, STATGROUP_IKinema);
DECLARE_CYCLE_STAT(TEXT("Input Prepare"), STAT_StatsInputPrepare, STATGROUP_IKinema);
DECLARE_CYCLE_STAT(TEXT("In Pose Evaluate"), STAT_StatsInPoseEvaluate, STATGROUP_IKinema);