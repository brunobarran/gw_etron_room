/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "AnimNode_IKinemaRetargeting.h"
#include "IKinemaCorePrivatePCH.h"

FAnimNode_IKinemaRetargeting::FAnimNode_IKinemaRetargeting() :
	AutoTune(true),
	pCoeff(0.2),
	IKinemaRig(nullptr), DebugDraw(false),
	UpdateVersion(0),
	IsRigBound(false),
	RootBoneID(0),
	ExtractionAlpha(0.5)
{
}

FAnimNode_IKinemaRetargeting::~FAnimNode_IKinemaRetargeting()
{
	mSolverInstance.Destroy();
}

void FAnimNode_IKinemaRetargeting::SetAsset(class UIKinemaRig* rig)
{
	if (rig == nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Invalid IKinemaRig for IKinemaSolve node"));
		return;
	}

	IKinemaRig = rig;
	PreloadObject(IKinemaRig);

	if (mSkeletonBinding.IsValid() && Tasks.Num() != rig->SolverDef.Tasks.Num())
	{
		mSolverInstance.Destroy();
		mSolverInstance.Create(mSkeletonBinding);
	}

	// Initialize the targets array with the number of tasks in the rig.
	Tasks.Init(FIKinemaSolverTask(), rig->SolverDef.Tasks.Num());

	TaskProperties.Init(FIKinemaTaskProperties(), rig->SolverDef.Tasks.Num());
	PinTaskProperties.Init(FIKinemaTaskOverride(), rig->SolverDef.Tasks.Num());
	for (int t = 0; t < rig->SolverDef.Tasks.Num(); t++)
	{
		TaskProperties[t].PositionDofX = rig->SolverDef.Tasks[t].PositionDofX;
		TaskProperties[t].PositionDofY = rig->SolverDef.Tasks[t].PositionDofY;
		TaskProperties[t].PositionDofZ = rig->SolverDef.Tasks[t].PositionDofZ;
		TaskProperties[t].PositionDepth = rig->SolverDef.Tasks[t].PositionDepth;
		TaskProperties[t].PositionWeight = rig->SolverDef.Tasks[t].PositionWeight;
		TaskProperties[t].PositionPrecision = rig->SolverDef.Tasks[t].PositionPrecision;

		//orientation
		TaskProperties[t].RotateDofX = rig->SolverDef.Tasks[t].RotateDofX;
		TaskProperties[t].RotateDofY = rig->SolverDef.Tasks[t].RotateDofY;
		TaskProperties[t].RotateDofZ = rig->SolverDef.Tasks[t].RotateDofZ;
		TaskProperties[t].RotateDepth = rig->SolverDef.Tasks[t].RotateDepth;
		TaskProperties[t].RotateWeight = rig->SolverDef.Tasks[t].RotateWeight;
		TaskProperties[t].RotatePrecision = rig->SolverDef.Tasks[t].RotatePrecision;

		PinTaskProperties[t].TaskName = rig->SolverDef.Tasks[t].Name;
		PinTaskProperties[t].ShowAsPin = false;

		Tasks[t].EnableTranslation = TaskProperties[t].PositionDofX || TaskProperties[t].PositionDofY || TaskProperties[t].PositionDofZ;
		Tasks[t].EnableOrientation = TaskProperties[t].RotateDofX || TaskProperties[t].RotateDofY || TaskProperties[t].RotateDofZ;
		Tasks[t].UseAsOffset = false;
		Tasks[t].DebugDraw = false;
	}

	SegmentProperties.Init(FIKinemaSegmentProperties(), rig->SolverDef.Bones.Num());
	PinSegmentProperties.Init(FIKinemaSegmentOverride(), rig->SolverDef.Bones.Num());
	//OverrideSegmentProperties.Init(false, rig->SolverDef.Tasks.Num());
	for (int b = 0; b < rig->SolverDef.Bones.Num(); b++)
	{
		SegmentProperties[b].DofX = rig->SolverDef.Bones[b].DofX;
		SegmentProperties[b].DofY = rig->SolverDef.Bones[b].DofY;
		SegmentProperties[b].DofZ = rig->SolverDef.Bones[b].DofZ;
		SegmentProperties[b].EnforceLimits = rig->SolverDef.Bones[b].EnforceLimits;
		SegmentProperties[b].Weight = rig->SolverDef.Bones[b].Weight;
		SegmentProperties[b].EnableLimits = rig->SolverDef.Bones[b].EnableLimits;
		SegmentProperties[b].LimitsGain = rig->SolverDef.Bones[b].LimitsGain;
		SegmentProperties[b].EnableRetargeting = rig->SolverDef.Bones[b].EnableRetargeting;
		SegmentProperties[b].RetargetingGain = rig->SolverDef.Bones[b].RetargetingGain;

		PinSegmentProperties[b].BoneName = rig->SolverDef.Bones[b].Name;
		PinSegmentProperties[b].ShowAsPin = false;
	}
	pCoeff = rig->SolverDef.PCoefficient;
}

void FAnimNode_IKinemaRetargeting::UpdateSolverSettings(class UIKinemaRig* rig)
{
	if (rig != IKinemaRig) {
		return;
	}
	IKinemaRig->SolverDef.ApplySettings(mSolverInstance);
}

void FAnimNode_IKinemaRetargeting::BindSourceToTarget(FAnimInstanceProxy * vim)
{

	auto vimInterface = (vim->GetAnimClassInterface());
	auto AnimNodeProp = vimInterface->GetAnimNodeProperties();
	TArray<FString> names;
	FString subjectName;
	for (int an = 0; an < AnimNodeProp.Num(); an++)
	{
		UStructProperty* prop = AnimNodeProp[an];
		if (prop->Struct == FAnimNode_LADataStream::StaticStruct())
		{
			const FAnimNode_LADataStream* node = prop->ContainerPtrToValuePtr<FAnimNode_LADataStream>(vim->GetAnimInstanceObject());
			if (node)
			{
				const FAnimNode_LADataStream* streamer = static_cast<const FAnimNode_LADataStream*>(node);
				subjectName = streamer->SubjectName.ToString();
				for (int i = 0; i < streamer->mSkeletonBinding.GetNumberOfBones(); i++)
					names.Push(streamer->mSkeletonBinding.GetViconSegmentName(i));
			}
		}
	}

	//Loop through all the bonedefs in teh rig and set the source index correctly
	IKinemaRig->SolverDef.IsRetargeting = true;
	FString rightString;
	auto findBoneIndex = [&rightString](const FString& arg) {
		return arg == rightString; };
	for (int i = 0; i < IKinemaRig->SolverDef.Bones.Num(); i++)
	{
		FIKinemaBoneDef& boneDef = IKinemaRig->SolverDef.Bones[i];
		if (boneDef.SourceIndex == INDEX_NONE)
		{
			rightString = boneDef.SourceName.ToString();
			int32 j1 = names.IndexOfByPredicate(findBoneIndex);
			if (j1 == INDEX_NONE)
			{
				FString leftString;
				rightString.Split(subjectName + "_", &leftString, &rightString);
				j1 = names.IndexOfByPredicate(findBoneIndex);
			}
			boneDef.SourceIndex = j1;
		}

	}
	//Store the task source bone index.	
	for (int i = 0; i < IKinemaRig->SolverDef.Tasks.Num(); i++)
	{
		FIKinemaTaskDef& boneDef = IKinemaRig->SolverDef.Tasks[i];
		if (boneDef.SourceIndex == INDEX_NONE)
		{
			rightString = boneDef.SourceName.ToString();
			int32 j1 = names.IndexOfByPredicate(findBoneIndex);
			if (j1 == INDEX_NONE)
			{
				FString leftString;
				rightString.Split(subjectName + "_", &leftString, &rightString);
				j1 = names.IndexOfByPredicate(findBoneIndex);
			}
			boneDef.SourceIndex = j1;
		}
	}
}

void FAnimNode_IKinemaRetargeting::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	// Forward to the incoming pose link.
	if (IKinemaRig == nullptr) {
		return;
	}
	InPose.Initialize(Context);
	SetAsset(IKinemaRig);
	IsRigBound = false;
	//check(Context.AnimInstanceProxy != nullptr);
	//USkeleton* skeleton = Context.AnimInstanceProxy->GetSkeleton();
	///**Do the mapping through direct access to get the bone index*/
	//auto vim = Context.AnimInstanceProxy;
	//if (!vim)
	//{
	//	return;
	//}
	//
	//BindSourceToTarget(vim);
	//if (skeleton != nullptr)
	//{
	//	IsRigBound = mSkeletonBinding.BindToSkeleton(IKinemaRig, skeleton, vim->GetSkelMeshComponent());
	//}
	//if (!IsRigBound)
	//{
	//	UE_LOG(LogIKinemaCore, Error, TEXT("Couldn't bound IKinemaRig  %s to UE4 Skeleton %s"),
	//		*IKinemaRig->GetName(),
	//		*skeleton->GetName());
	//}
	//mSolverInstance.Create(mSkeletonBinding);
	return;
}

void FAnimNode_IKinemaRetargeting::Update_AnyThread(const FAnimationUpdateContext& Context)
{	// Forward to the incoming pose link.

	InPose.Update(Context);
	// Handle non-pose pins.
	EvaluateGraphExposedInputs.Execute(Context);
	if (IKinemaRig != mSkeletonBinding.GetRig())
	{
		SetAsset(IKinemaRig);
		mSkeletonBinding.BindToSkeleton(IKinemaRig, Context.AnimInstanceProxy->GetSkeleton(), Context.AnimInstanceProxy->GetSkelMeshComponent());
		mSolverInstance.Create(mSkeletonBinding);
	}
	USkeleton* skeleton = Context.AnimInstanceProxy->GetSkeleton();
	if (IKinemaRig) {
		if (!IsRigBound)
		{

			/**Do the mapping through direct access to get the bone index*/
			auto vim = Context.AnimInstanceProxy;
			if (!vim)
			{
				return;
			}

			BindSourceToTarget(vim);
			if (skeleton != nullptr)
			{
				IsRigBound = mSkeletonBinding.BindToSkeleton(IKinemaRig, skeleton, vim->GetSkelMeshComponent());
			}
			if (!IsRigBound)
			{
				UE_LOG(LogIKinemaCore, Error, TEXT("Couldn't bound IKinemaRig  %s to UE4 Skeleton %s"),
					*IKinemaRig->GetName(),
					*skeleton->GetName());
			}
			mSolverInstance.Create(mSkeletonBinding);
		}

		if (IKinemaRig && UpdateVersion < IKinemaRig->UpdateVersion)
		{
			SetAsset(IKinemaRig);
			UpdateSolverSettings(IKinemaRig);
			BindSourceToTarget(Context.AnimInstanceProxy);
			IsRigBound = mSkeletonBinding.BindToSkeleton(IKinemaRig, skeleton, Context.AnimInstanceProxy->GetSkelMeshComponent());
			UpdateVersion = IKinemaRig->UpdateVersion;
			mSolverInstance.Create(mSkeletonBinding);
		}
	}
	if (IKinemaRig) {
		IKinemaRig->EnableAutoTune = AutoTune;
	}
	return;





}

void FAnimNode_IKinemaRetargeting::Evaluate_AnyThread(FPoseContext& Output)
{

	if (!IsRigBound || IKinemaRig == nullptr || !mSkeletonBinding.IsValid())
	{
		return;
	}
	if (!mSkeletonBinding.IsValid())
		return;

	check(Output.AnimInstanceProxy->GetSkeleton() != nullptr);
	auto Skeleton = Output.AnimInstanceProxy->GetSkeleton();
	// Make sure we have just the right number of input bones.
	const int32 numRegularBones = Skeleton->GetReferenceSkeleton().GetNum();
	if (numRegularBones < 1)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("IKinemaRig %s for IKinemaSolve node expected at least one bone in the skeleton"),
			*IKinemaRig->GetName());
		return;
	}
	// Evaluate the input pose onto the output first.
	// The input pose should have the regular local FK pose and
	// then the global transforms appended at the end.

	TArray<FTransform> RefBones;
	for (int i = 0; i < Output.Pose.GetNumBones(); i++)
	{
		RefBones.Push(Output.Pose.GetRefPose(FCompactPoseBoneIndex(i)));
	}
	InPose.Evaluate(Output);

	TArray<FTransform> Bones;
	Bones = Output.Pose.GetBones();
	Output.Pose.SetBoneContainer(&Output.AnimInstanceProxy->GetRequiredBones());


	const auto ActorBones = IKinemaRig->Actor ? IKinemaRig->Actor->Bones.Num() : Bones.Num() / 2;
	auto IsRigidBody = IKinemaRig->IsRigidBody;

	if (Bones.Num() <= 0 || (IsRigidBody && ActorBones != Bones.Num()) || (!IsRigidBody && ActorBones * 2 != Bones.Num()))
	{
		// Pass on Reference pose to output.
		Output.Pose.CopyBonesFrom(RefBones);
		UE_LOG(LogIKinemaCore, Warning, TEXT("Mocap Server node returned no Pose"));
		return;
	}

	//Tasks target need to be updated some how?
	// - the first half of input pose is local transforms
	// - second half is global transforms, task target index = number of bones in source + source bone index
	const TArray<FIKinemaTaskDef>& tasks = IKinemaRig->SolverDef.Tasks;
	const int32 numRetargetBones = Bones.Num() / 2;

	//solver to component space matrix

	int32 RootBoneIndex = mSkeletonBinding.GetUE4BoneIndex(0);
	int32 ParentIndex = mSkeletonBinding.GetSkeleton()->GetReferenceSkeleton().GetParentIndex(RootBoneIndex);
	FTransform mSolverToComponentSpace = FTransform::Identity;
	while (ParentIndex != INDEX_NONE)
	{
		mSolverToComponentSpace *= Skeleton->GetRefLocalPoses()[ParentIndex];//Bones[ParentIndex];
		ParentIndex = mSkeletonBinding.GetSkeleton()->GetReferenceSkeleton().GetParentIndex(ParentIndex);
	}
	FTransform mSolverToWorldSpace = mSolverToComponentSpace * mSkeletonBinding.GetSkeletalMeshComponent()->GetComponentTransform();
	float scale = IKinemaRig->SolverDef.SourceScale * IKinemaRig->SolverDef.ImportScale;
	for (int i = 0; i < tasks.Num(); i++)
	{
		int32 sourceIndex = tasks[i].SourceIndex;
		if (sourceIndex == -1)
		{
			UE_LOG(LogIKinemaCore, Log, TEXT("Task %s is not associated to a source bone"), *(tasks[i].Name.ToString()));
			IsRigBound = false;
			Output.Pose.CopyBonesFrom(RefBones);
			return;
		}
		int taskIndex = sourceIndex;
		if (!IKinemaRig->IsRigidBody)
		{
			taskIndex = numRetargetBones + sourceIndex;
		}
		if (Bones.IsValidIndex(taskIndex))
		{
			FTransform target = Bones[taskIndex];
			FVector p = target.GetTranslation() * scale;
			target.SetTranslation(p);
			Tasks[i].DebugDraw = DebugDraw;
			Tasks[i].DebugDrawScale = 10;
			//If the tasks pin is shown then we use that transform as an offset on top of MoCap
			if (Tasks[i].UseAsOffset)
				Tasks[i].Target = (target*Tasks[i].Offset);
			else
				Tasks[i].Target = (target);
		}
	}

	if (DebugDraw)
	{
		for (int i = numRetargetBones; i < Bones.Num(); i++)
		{
			auto target = mSolverToWorldSpace.GetTranslation()* 1.f / scale + (Bones[i].GetTranslation());
			auto world = Output.AnimInstanceProxy->GetSkelMeshComponent()->GetWorld();
			ExecOnGameThread(
				[world, target, scale]() {

				::DrawDebugSphere(world, target* scale, 2.f, 12, FColor::Red);
			});

		}
	}

	if (!mSolverInstance.UpdateAndSolve(Tasks, TaskProperties, PinTaskProperties,
		SegmentProperties, PinSegmentProperties, Bones, Output, mSolverToWorldSpace, DebugDraw))
	{
		Output.ResetToRefPose();
	}
if (ExtractRootMotion) {
		/*Root motion extraction*/

		//Update alpha
		filterObjectX.UpdateAlpha(ExtractionAlpha);
		filterObjectY.UpdateAlpha(ExtractionAlpha);

		FCompactPoseBoneIndex hipBoneIndex = FCompactPoseBoneIndex(RootBoneID);

		//Get hip bone location
		hipBoneWorldPos = Output.Pose[hipBoneIndex].GetLocation();
		//Get hip bone rotation
		FQuat hipBoneWorldRotation = Output.Pose[hipBoneIndex].GetRotation();

		//Get the USceneComponent
		characterSceneComponent = mSkeletonBinding.GetSkeletalMeshComponent()->GetOwner()->GetRootComponent();

		//New vector with all the hip motion
		if (InverseBoneAxis) {
			transferedMotion = FVector(hipBoneWorldPos.X, hipBoneWorldPos.Y, characterSceneComponent->GetComponentLocation().Z);		
		}
		else {
			transferedMotion = FVector(hipBoneWorldPos.X, hipBoneWorldPos.Z, characterSceneComponent->GetComponentLocation().Z); //Original
		}

		//Filter it
		filteredMotion = FVector(filterObjectX.filter(transferedMotion.X), filterObjectY.filter(transferedMotion.Y), characterSceneComponent->GetComponentLocation().Z); //original
		
		//Get rotation
		auto rot = characterSceneComponent->GetComponentRotation();

		float YawToRadians = (FMath::Fmod(rot.Yaw + 360, 360) * 3.1415926) / 180;

		//2D Vector rotation maths (as the pivot point is now always under the character)
		axisChanged.X = filteredMotion.X * FMath::Cos(YawToRadians) - filteredMotion.Y * FMath::Sin(YawToRadians) + PositionOffset.X;
		axisChanged.Y = filteredMotion.X * FMath::Sin(YawToRadians) + filteredMotion.Y * FMath::Cos(YawToRadians) + PositionOffset.Y;
		axisChanged.Z = filteredMotion.Z;

		FVector fm = axisChanged;
		USceneComponent* csc = characterSceneComponent;

		//Move the 
		ExecOnGameThread(
			[fm, csc]() {
			csc->SetWorldLocation(fm, false, nullptr, ETeleportType::TeleportPhysics);
		});

		//Apply motion thats filtered off back to hips
		if (InverseBoneAxis) {
			resetHipVector = FVector(transferedMotion.X - filteredMotion.X, transferedMotion.Y - filteredMotion.Y, hipBoneWorldPos.Z);
		}
		else {
			resetHipVector = FVector(transferedMotion.X - filteredMotion.X, hipBoneWorldPos.Y, transferedMotion.Y - filteredMotion.Y); //Original
		}
		Output.Pose[hipBoneIndex].SetLocation(resetHipVector);
	}
	return;

}