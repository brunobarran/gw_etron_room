#include "RootFilter.h"

RootFilter::RootFilter() {

	prev = 90.f;
}

float RootFilter::filter(float input) {

	retVal = alpha * prev + (1 - alpha) * input;

	prev = retVal;

	return retVal;
}

void RootFilter::UpdateAlpha(float a) {
	alpha = a;
}

