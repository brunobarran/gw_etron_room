/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaLiveLinkRetarget.h"
#include "IKinemaCorePrivatePCH.h"
#include "LiveLinkTypes.h"
#include "BonePose.h"
#include "Animation/AnimInstanceProxy.h"

UIKinemaLiveLinkRetarget::UIKinemaLiveLinkRetarget() :
	AutoTune(true),
	pCoeff(0.2),
	IKinemaRig(nullptr), DebugDraw(false),
	UpdateVersion(0),
	IsRigBound(false),
	Initialized(false)

{

}

UIKinemaLiveLinkRetarget::~UIKinemaLiveLinkRetarget()
{
	mSolverInstance.Destroy();
}

void UIKinemaLiveLinkRetarget::SetAsset(UIKinemaRig* rig)
{
	if (rig == nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Invalid IKinemaRig for IKinemaSolve node"));
		return;
	}

	IKinemaRig = rig;
	PreloadObject(IKinemaRig);

	if (mSkeletonBinding.IsValid() && Tasks.Num() != rig->SolverDef.Tasks.Num())
	{
		mSolverInstance.Destroy();
		mSolverInstance.Create(mSkeletonBinding);
	}

	// Initialize the targets array with the number of tasks in the rig.
	Tasks.Init(FIKinemaSolverTask(), rig->SolverDef.Tasks.Num());

	TaskProperties.Init(FIKinemaTaskProperties(), rig->SolverDef.Tasks.Num());
	PinTaskProperties.Init(FIKinemaTaskOverride(), rig->SolverDef.Tasks.Num());
	for (int t = 0; t < rig->SolverDef.Tasks.Num(); t++)
	{
		TaskProperties[t].PositionDofX = rig->SolverDef.Tasks[t].PositionDofX;
		TaskProperties[t].PositionDofY = rig->SolverDef.Tasks[t].PositionDofY;
		TaskProperties[t].PositionDofZ = rig->SolverDef.Tasks[t].PositionDofZ;
		TaskProperties[t].PositionDepth = rig->SolverDef.Tasks[t].PositionDepth;
		TaskProperties[t].PositionWeight = rig->SolverDef.Tasks[t].PositionWeight;
		TaskProperties[t].PositionPrecision = rig->SolverDef.Tasks[t].PositionPrecision;

		//orientation
		TaskProperties[t].RotateDofX = rig->SolverDef.Tasks[t].RotateDofX;
		TaskProperties[t].RotateDofY = rig->SolverDef.Tasks[t].RotateDofY;
		TaskProperties[t].RotateDofZ = rig->SolverDef.Tasks[t].RotateDofZ;
		TaskProperties[t].RotateDepth = rig->SolverDef.Tasks[t].RotateDepth;
		TaskProperties[t].RotateWeight = rig->SolverDef.Tasks[t].RotateWeight;
		TaskProperties[t].RotatePrecision = rig->SolverDef.Tasks[t].RotatePrecision;

		PinTaskProperties[t].TaskName = rig->SolverDef.Tasks[t].Name;
		PinTaskProperties[t].ShowAsPin = false;

		Tasks[t].EnableTranslation = TaskProperties[t].PositionDofX || TaskProperties[t].PositionDofY || TaskProperties[t].PositionDofZ;
		Tasks[t].EnableOrientation = TaskProperties[t].RotateDofX || TaskProperties[t].RotateDofY || TaskProperties[t].RotateDofZ;
		Tasks[t].UseAsOffset = false;
		Tasks[t].DebugDraw = false;
	}

	SegmentProperties.Init(FIKinemaSegmentProperties(), rig->SolverDef.Bones.Num());
	PinSegmentProperties.Init(FIKinemaSegmentOverride(), rig->SolverDef.Bones.Num());
	//OverrideSegmentProperties.Init(false, rig->SolverDef.Tasks.Num());
	for (int b = 0; b < rig->SolverDef.Bones.Num(); b++)
	{
		SegmentProperties[b].DofX = rig->SolverDef.Bones[b].DofX;
		SegmentProperties[b].DofY = rig->SolverDef.Bones[b].DofY;
		SegmentProperties[b].DofZ = rig->SolverDef.Bones[b].DofZ;
		SegmentProperties[b].EnforceLimits = rig->SolverDef.Bones[b].EnforceLimits;
		SegmentProperties[b].Weight = rig->SolverDef.Bones[b].Weight;
		SegmentProperties[b].EnableLimits = rig->SolverDef.Bones[b].EnableLimits;
		SegmentProperties[b].LimitsGain = rig->SolverDef.Bones[b].LimitsGain;
		SegmentProperties[b].EnableRetargeting = rig->SolverDef.Bones[b].EnableRetargeting;
		SegmentProperties[b].RetargetingGain = rig->SolverDef.Bones[b].RetargetingGain;

		PinSegmentProperties[b].BoneName = rig->SolverDef.Bones[b].Name;
		PinSegmentProperties[b].ShowAsPin = false;
	}
	pCoeff = rig->SolverDef.PCoefficient;
}

void UIKinemaLiveLinkRetarget::BindSourceToTarget(const FLiveLinkSubjectFrame& vim)
{


	TArray<FName> names = vim.RefSkeleton.GetBoneNames();
	

	//Loop through all the bonedefs in teh rig and set the source index correctly
	IKinemaRig->SolverDef.IsRetargeting = true;
	FName rightString;
	auto findBoneIndex = [&rightString](const FName& arg) {
		return arg == rightString; };
	for (int i = 0; i < IKinemaRig->SolverDef.Bones.Num(); i++)
	{
		FIKinemaBoneDef& boneDef = IKinemaRig->SolverDef.Bones[i];
		if (boneDef.SourceIndex == INDEX_NONE)
		{
			rightString = boneDef.SourceName;
			int32 j1 = names.IndexOfByPredicate(findBoneIndex);
			if (j1 == INDEX_NONE)
			{
				
				j1 = names.IndexOfByPredicate(findBoneIndex);
			}
			boneDef.SourceIndex = j1;
		}

	}
	//Store the task source bone index.	
	for (int i = 0; i < IKinemaRig->SolverDef.Tasks.Num(); i++)
	{
		FIKinemaTaskDef& boneDef = IKinemaRig->SolverDef.Tasks[i];
		if (boneDef.SourceIndex == INDEX_NONE)
		{
			rightString = boneDef.SourceName;
			int32 j1 = names.IndexOfByPredicate(findBoneIndex);
			if (j1 == INDEX_NONE)
			{
				j1 = names.IndexOfByPredicate(findBoneIndex);
			}
			boneDef.SourceIndex = j1;
		}
	}
}


void UIKinemaLiveLinkRetarget::BuildPoseForSubject(float DeltaTime,const FLiveLinkSubjectFrame& InFrame, FCompactPose& OutPose, FBlendedCurve& OutCurve)
{

	//do once - Move to initialize if ever possible
	if (!Initialized)
	{
		//initialize
		SetAsset(IKinemaRig);
		BindSourceToTarget(InFrame);
		//get skeleton and mesh
		auto skeleton = OutPose.GetBoneContainer().GetSkeletonAsset();
		auto casted = static_cast<UAnimInstance*>(Super::GetOuter());
		auto component = casted->GetSkelMeshComponent();

		if (skeleton != nullptr)
		{
			IsRigBound = mSkeletonBinding.BindToSkeleton(IKinemaRig, skeleton, component);
		}
		if (!IsRigBound)
		{
			UE_LOG(LogIKinemaCore, Error, TEXT("Couldn't bound IKinemaRig  %s to UE4 Skeleton %s"),
				*IKinemaRig->GetName(),
				*skeleton->GetName());
		}
		IsRigBound = true;
		//create solver
		mSolverInstance.Create(mSkeletonBinding);
		Pose.Init(FTransform::Identity, InFrame.RefSkeleton.GetBoneNames().Num());
		Initialized = true;
	}

	if (!IsRigBound || IKinemaRig == nullptr )
	{
		return;
	}
	if (!mSkeletonBinding.IsValid())
	{
		return;
	}
	// Make sure we have just the right number of input bones.
	const int32 numRegularBones = InFrame.RefSkeleton.GetBoneNames().Num();  
	if (numRegularBones < 1)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("IKinemaRig %s for IKinemaSolve node expected at least one bone in the skeleton"),
			*IKinemaRig->GetName());
		return;
	}
	// Evaluate the input pose onto the output first.
	// The input pose should have the regular local FK pose and
	// then the global transforms appended at the end.


	//get bones from livelink`
	TArray<FTransform> Bones;
	Bones = InFrame.Transforms;


	auto IsRigidBody = IKinemaRig->IsRigidBody;

	//Tasks target need to be updated some how?
	// - the first half of input pose is local transforms
	// - second half is global transforms, task target index = number of bones in source + source bone index
	const TArray<FIKinemaTaskDef>& tasks = IKinemaRig->SolverDef.Tasks;
	const int32 numRetargetBones = Bones.Num();

	//solver to component space matrix

	
	int32 RootBoneIndex = mSkeletonBinding.GetUE4BoneIndex(0);
	int32 ParentIndex = mSkeletonBinding.GetSkeleton()->GetReferenceSkeleton().GetParentIndex(RootBoneIndex);
	FTransform mSolverToComponentSpace = FTransform::Identity;
	while (ParentIndex != INDEX_NONE)
	{
		mSolverToComponentSpace *= mSkeletonBinding.GetSkeleton()->GetRefLocalPoses()[ParentIndex];
		ParentIndex = mSkeletonBinding.GetSkeleton()->GetReferenceSkeleton().GetParentIndex(ParentIndex);
	}
	FTransform mSolverToWorldSpace = mSolverToComponentSpace * mSkeletonBinding.GetSkeletalMeshComponent()->GetComponentTransform(); 
	
	float scale = IKinemaRig->SolverDef.SourceScale * IKinemaRig->SolverDef.ImportScale;




	//calculate globals
	for (int i = 0; i < InFrame.RefSkeleton.GetBoneNames().Num(); i++)
	{
		FTransform global;
		if (i == 0)
		{
			
			global = Bones[0];
		}
		else
		{
			global = Bones[i];
			int parent = IKinemaRig->Actor->GetParentBoneIndex(InFrame.RefSkeleton.GetBoneNames()[i]);
			if (parent != INDEX_NONE)
			{
				global = Bones[i] * Pose[parent];
			}
		}
	
		Pose[i] = global;
	}




	for (int i = 0; i < tasks.Num(); i++)
	{
		int32 sourceIndex = tasks[i].SourceIndex;
		if (sourceIndex == -1)
		{
			UE_LOG(LogIKinemaCore, Log, TEXT("Task %s is not associated to a source bone"), *(tasks[i].Name.ToString()));
			IsRigBound = false;
			OutPose.ResetToRefPose();
			return;
		}
		int taskIndex = sourceIndex;

		if (Pose.IsValidIndex(taskIndex))
		{
			FTransform target = Pose[taskIndex];
			FVector p = target.GetTranslation() * scale;
			target.SetTranslation(p);
			Tasks[i].DebugDraw = DebugDraw;
			Tasks[i].DebugDrawScale = 100;
			//If the tasks pin is shown then we use that transform as an offset on top of MoCap
			if (Tasks[i].UseAsOffset)
				Tasks[i].Target = (target*Tasks[i].Offset);
			else
				Tasks[i].Target = (target);
		}
	}

	if (DebugDraw)
	{
		for (int i = 0; i < Pose.Num(); i++)
		{
			auto target = mSolverToWorldSpace.GetTranslation()* 1.f / scale + (Pose[i].GetTranslation());
			auto world = GWorld;
			ExecOnGameThread(
				[world, target, scale]() {

				::DrawDebugSphere(world, target* scale, 2.f, 12, FColor::Red);
			});

		}
	}

	if (!mSolverInstance.UpdateAndSolve(Tasks, TaskProperties, PinTaskProperties,
		SegmentProperties, PinSegmentProperties, Bones, OutPose, mSolverToWorldSpace, DebugDraw))
	{
		OutPose.ResetToRefPose();
	}

	BuildCurveData(InFrame, OutPose, OutCurve);

	return;
}