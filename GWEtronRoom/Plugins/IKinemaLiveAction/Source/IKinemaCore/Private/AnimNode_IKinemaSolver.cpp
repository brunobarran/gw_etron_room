/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "AnimNode_IKinemaSolver.h"
#include "IKinemaCorePrivatePCH.h"
#include "Stats/Stats.h"
#include "IKinemaStatsHelper.h"

FAnimNode_IKinemaSolver::FAnimNode_IKinemaSolver(): 
IKinemaRig(nullptr),
UpdateVersion(0)
{
}

FAnimNode_IKinemaSolver::~FAnimNode_IKinemaSolver()
{
}

void FAnimNode_IKinemaSolver::Dump(const FString& Folder)
{
	mSolverInstance.Dump(Folder);
}

void FAnimNode_IKinemaSolver::CacheBones_AnyThread(const FAnimationCacheBonesContext & Context)
{
	//InitializeBoneReferences(Context.AnimInstance->RequiredBones);
	InPose.CacheBones(Context);
}

// Set the rig on this.
void FAnimNode_IKinemaSolver::SetAsset(class UIKinemaRig* rig)
{
	if (rig == nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Invalid IKinemaRig for IKinemaSolve node"));
		return;
	}
	IKinemaRig = rig;
	PreloadObject(IKinemaRig);

	// Initialize the targets array with the number of tasks in the rig.
	Tasks.Init(FIKinemaSolverTask(), rig->SolverDef.Tasks.Num());

	TaskProperties.Init(FIKinemaTaskProperties(), rig->SolverDef.Tasks.Num());
	PinTaskProperties.Init(FIKinemaTaskOverride(), rig->SolverDef.Tasks.Num());
	for(int t=0;t<rig->SolverDef.Tasks.Num();t++)
	{
		TaskProperties[t].PositionDofX = rig->SolverDef.Tasks[t].PositionDofX;
		TaskProperties[t].PositionDofY = rig->SolverDef.Tasks[t].PositionDofY;
		TaskProperties[t].PositionDofZ = rig->SolverDef.Tasks[t].PositionDofZ;
		TaskProperties[t].PositionDepth = rig->SolverDef.Tasks[t].PositionDepth;
		TaskProperties[t].PositionWeight = rig->SolverDef.Tasks[t].PositionWeight;
		TaskProperties[t].PositionPrecision = rig->SolverDef.Tasks[t].PositionPrecision;

		//orientation
		TaskProperties[t].RotateDofX = rig->SolverDef.Tasks[t].RotateDofX;
		TaskProperties[t].RotateDofY = rig->SolverDef.Tasks[t].RotateDofY;
		TaskProperties[t].RotateDofZ = rig->SolverDef.Tasks[t].RotateDofZ;
		TaskProperties[t].RotateDepth = rig->SolverDef.Tasks[t].RotateDepth;
		TaskProperties[t].RotateWeight = rig->SolverDef.Tasks[t].RotateWeight;
		TaskProperties[t].RotatePrecision = rig->SolverDef.Tasks[t].RotatePrecision;

		PinTaskProperties[t].TaskName = rig->SolverDef.Tasks[t].Name;
		PinTaskProperties[t].ShowAsPin = false;
	}

	SegmentProperties.Init(FIKinemaSegmentProperties(), rig->SolverDef.Bones.Num());
	PinSegmentProperties.Init(FIKinemaSegmentOverride(), rig->SolverDef.Bones.Num());
	//OverrideSegmentProperties.Init(false, rig->SolverDef.Tasks.Num());
	for(int b=0;b<rig->SolverDef.Bones.Num();b++)
	{
		SegmentProperties[b].DofX = rig->SolverDef.Bones[b].DofX;
		SegmentProperties[b].DofY = rig->SolverDef.Bones[b].DofY;
		SegmentProperties[b].DofZ = rig->SolverDef.Bones[b].DofZ;
		SegmentProperties[b].EnforceLimits = rig->SolverDef.Bones[b].EnforceLimits;
		SegmentProperties[b].Weight = rig->SolverDef.Bones[b].Weight;
		SegmentProperties[b].EnableLimits = rig->SolverDef.Bones[b].EnableLimits;
		SegmentProperties[b].LimitsGain = rig->SolverDef.Bones[b].LimitsGain;
		SegmentProperties[b].EnableRetargeting = rig->SolverDef.Bones[b].EnableRetargeting;
		SegmentProperties[b].RetargetingGain = rig->SolverDef.Bones[b].RetargetingGain;

		PinSegmentProperties[b].BoneName = rig->SolverDef.Bones[b].Name;
		PinSegmentProperties[b].ShowAsPin = false;
	}
}

void FAnimNode_IKinemaSolver::UpdateSolverSettings(class UIKinemaRig* rig)
{
	if(rig != IKinemaRig)
		return;

	IKinemaRig->SolverDef.ApplySettings(mSolverInstance);
}

void FAnimNode_IKinemaSolver::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	check(Context.AnimInstanceProxy != nullptr);
	USkeleton* skeleton = Context.AnimInstanceProxy->GetSkeleton();
	if (skeleton != nullptr)
	{
		mSkeletonBinding.BindToSkeleton(IKinemaRig, skeleton, Context.AnimInstanceProxy->GetSkelMeshComponent());
	}
	mSolverInstance.Create(mSkeletonBinding);

	// Forward to the incoming pose link.
	InPose.Initialize(Context);
	//TaskTargets.Initialize(Context);
}

void FAnimNode_IKinemaSolver::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	if(IKinemaRig && UpdateVersion < IKinemaRig->UpdateVersion)
	{
		UpdateSolverSettings(IKinemaRig);
		UpdateVersion = IKinemaRig->UpdateVersion;
	}

	PreloadObject(IKinemaRig);
	// Forward to the incoming pose link.
	InPose.Update(Context);

	// Handle non-pose pins.
	EvaluateGraphExposedInputs.Execute(Context);
}

void FAnimNode_IKinemaSolver::Evaluate_AnyThread(FPoseContext& Output)
{
	if (IKinemaRig == nullptr)
	{
		// Pass on input to output.
		//SCOPE_CYCLE_COUNTER(EIKinemaStat_InPoseEvaluate);
		InPose.Evaluate(Output);
		return;
	}
	TArray<FTransform> RefBones;
	for (int i = 0; i < Output.Pose.GetNumBones(); i++)
	{
		RefBones.Push(Output.Pose.GetRefPose(FCompactPoseBoneIndex(i)));
	}
	PreloadObject(IKinemaRig);
	check(Output.AnimInstanceProxy->GetSkeleton() != nullptr);

	// Evaluate the input pose onto the output first.
	// The input pose should have the regular FK pose and
	// then the task targets appended at the end.
	// TODO: See if we pre-allocate just the right number of bones for the context.
	{
		//SCOPE_CYCLE_COUNTER(EIKinemaStat_InPoseEvaluate);
		InPose.Evaluate(Output);
	}

	// Make sure we have just the right number of input bones.
	const int32 numRegularBones = Output.AnimInstanceProxy->GetSkeleton()->GetReferenceSkeleton().GetNum();
	if (numRegularBones < 1)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("IKinemaRig %s for IKinemaSolve node expected at least one bone in the skeleton"),
			*IKinemaRig->GetName());
		return;
	}
	
	auto bonesContainer = Output.Pose.GetBoneContainer();
	TArray<FTransform> Bones;
	Bones = Output.Pose.GetBones();
	Output.Pose.SetBoneContainer(&bonesContainer);
	mSolverInstance.UpdateAndSolve(Tasks, TaskProperties, PinTaskProperties, 
		SegmentProperties, PinSegmentProperties, Bones, Output);

}
