/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "AnimNode_IKinemaFiltering.h"
#include "IKinemaCorePrivatePCH.h"
#include "ACP/FilterNode.h"

FAnimNode_IKinemaFiltering::FAnimNode_IKinemaFiltering() :
Freq(0.f),
iter(150),
DebugDraw(false),
DebugColor(FColor::Green),
IsLicValid(true)
{
	
}

FAnimNode_IKinemaFiltering::~FAnimNode_IKinemaFiltering()
{
	ClearSolver();
}


void FAnimNode_IKinemaFiltering::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	// Forward to the incoming pose link.
	ue4Index.Empty();
	InPose.Initialize(Context);

	check(Context.AnimInstanceProxy != nullptr);
	
	if (!IKinemaRig)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("No reference to IKinemaRig in the node"));
		return;
	}

	
	USkeleton* skeleton = Context.AnimInstanceProxy->GetSkeleton();
	USkeletalMeshComponent* component = Context.AnimInstanceProxy->GetSkelMeshComponent();
	
	CreateSolver(skeleton, component);
}

void FAnimNode_IKinemaFiltering::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	InPose.Update(Context);

	// Handle non-pose pins.
	EvaluateGraphExposedInputs.Execute(Context);
	auto filterNode = (FIK::ACP::FilterNode*)(Node);
	if (Node)
	{
		Node->SetFrameRate(1.f / Context.GetDeltaTime());
		Node->SetNumOfIterations(iter);
	
	if(!FMath::IsNearlyEqual(Freq, filterNode->GetFilterAlpha(), 1e-3f))
		filterNode->SetFilterAlpha(Freq);
	}
}

void FAnimNode_IKinemaFiltering::Evaluate_AnyThread(FPoseContext& Output)
{
	InPose.Evaluate(Output);
	if (!IsLicValid)
	{
		Output.ResetToRefPose();
		return;
	}

	SolveAndUpdateACPPose(Node, ue4Index, Output);
}

void FAnimNode_IKinemaFiltering::CacheBones_AnyThread(const FAnimationCacheBonesContext & Context)
{
	InPose.CacheBones(Context);
}

void FAnimNode_IKinemaFiltering::ClearSolver()
{
	//Delete the solver and remove all tasks
	if (Node)
	{
		delete Node;
	}
}

void FAnimNode_IKinemaFiltering::CreateSolver(const USkeleton * Skeleton, const USkeletalMeshComponent * SkelComp)
{
	auto&& rig = IKinemaRig->CreateACPRigFromRig(Skeleton, SkelComp, ue4Index);

	//Construct the solver
	//FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(TEXT("../../Binaries/ThirdParty/IKinema/license"));
	FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(FPaths::ConvertRelativePathToFull(FPaths::EngineDir() + TEXT("Binaries/ThirdParty/IKinema/license"))));
	char* errorMessage = nullptr;
	Node = FIK::ACP::FilterNode::Create(rig, Freq, TCHAR_TO_ANSI(*licenseRelativeFolder), &errorMessage);
	if (Node == nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Could not create IKinema solver. Constructor returned null"));
		return;
	}
	if (errorMessage != nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Could not create IKinema solver. Constructor returned error: %s"), ANSI_TO_TCHAR(errorMessage));
		IsLicValid = false;
		delete Node;
		Node = nullptr;
	}
}
