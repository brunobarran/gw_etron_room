/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/
#include "AnimNode_IKinemaFreezingStage.h"
#include "IKinemaCorePrivatePCH.h"
#include "ACP/FeetSliding.h"

FAnimNode_IKinemaFreezingStage::FAnimNode_IKinemaFreezingStage() :
	iter(150),
	DebugDraw(false),
	DebugColor(FColor::Red),
	IKinemaRig(nullptr),
	IsLicValid(true),
	UpdateVersion(0)
{
}

FAnimNode_IKinemaFreezingStage::~FAnimNode_IKinemaFreezingStage()
{
	ClearSolver();
}

void FAnimNode_IKinemaFreezingStage::ClearSolver()
{
	if (Node)
	{
		delete Node;
		Node = nullptr;
	}
}

void FAnimNode_IKinemaFreezingStage::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	// Forward to the incoming pose link.
	InPose.Initialize(Context);
	ClearSolver();
	check(Context.AnimInstanceProxy != nullptr);
	USkeleton* skeleton = Context.AnimInstanceProxy->GetSkeleton();
	USkeletalMeshComponent* component = Context.AnimInstanceProxy->GetSkelMeshComponent();

	CreateSolver(skeleton, component);
}

void FAnimNode_IKinemaFreezingStage::CreateSolver(const USkeleton* Skeleton, const USkeletalMeshComponent* SkelComp)
{
	// Forward to the incoming pose link.
	ue4Index.Empty();

	if (!IKinemaRig)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("No reference to IKinemaRig in the node"));
		return;
	}

	auto rig = IKinemaRig->CreateACPRigFromRig(Skeleton, SkelComp, ue4Index);

	TArray<FIK::ACP::FeetSliding::SlidingTask> Tasks;
	for (auto& BoneDef : IKinemaRig->SolverDef.Bones)
	{
		if (BoneDef.bAutoLocking)
		{
			FIK::ACP::FeetSliding::SlidingTask task(TCHAR_TO_ANSI(*BoneDef.Name.ToString()));
			task.MinVelThresh = BoneDef.InThresh;
			task.MaxVelThresh = BoneDef.OutThresh;
			task.BlendFrames = BoneDef.BlendFrames;
			task.bIsLocking = BoneDef.bIsLocking;
			Tasks.Push(task);
		}
	}

	//Construct the solver
	//FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(TEXT("../../Binaries/ThirdParty/IKinema/license"));
	FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(FPaths::ConvertRelativePathToFull(FPaths::EngineDir() + TEXT("Binaries/ThirdParty/IKinema/license"))));
	char* errorMessage = nullptr;
	if (Tasks.IsValidIndex(0))
	{
		Node = FIK::ACP::FeetSliding::Create(rig, TCHAR_TO_ANSI(*licenseRelativeFolder), &errorMessage, &Tasks[0], Tasks.Num());
	}
	if (Node == nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Could not create IKinema solver. Constructor returned null"));
		return;
	}

	if (errorMessage != nullptr)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("Could not create IKinema solver. Constructor returned error: %s"), ANSI_TO_TCHAR(errorMessage));
		IsLicValid = false;
		delete Node;
		Node = nullptr;
	}

}

void FAnimNode_IKinemaFreezingStage::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	InPose.Update(Context);

	// Handle non-pose pins.
	EvaluateGraphExposedInputs.Execute(Context);

	if (IKinemaRig && UpdateVersion < IKinemaRig->UpdateVersion)
	{
		UpdateVersion = IKinemaRig->UpdateVersion;

		//Recreate the solver
		CreateSolver(Context.AnimInstanceProxy->GetSkeleton(), Context.AnimInstanceProxy->GetSkelMeshComponent());
	}

	if (Node)
	{
		Node->SetFrameRate(1.f / Context.GetDeltaTime());
		Node->SetNumOfIterations(iter);
	}


}

void FAnimNode_IKinemaFreezingStage::Evaluate_AnyThread(FPoseContext& Output)
{
	//static const float MIN_P = 0.01;
	InPose.Evaluate(Output);
	if (!IsLicValid)
	{
		Output.ResetToRefPose();
		return;
	}

	if (!Node)
	{
		UE_LOG(LogIKinemaCore, Error, TEXT("IKinema Filtering Node: IKinema Solver is not created"));
		return;
	}

	SolveAndUpdateACPPose(Node, ue4Index, Output);

	//if (DebugDraw)
	//{
	//	DebugDrawIKinema(m_Solver.Get(), mSolverToWorldSpace, DebugColor, Output.AnimInstanceProxy->GetSkelMeshComponent()->GetWorld());
	//}
}

void FAnimNode_IKinemaFreezingStage::CacheBones_AnyThread(const FAnimationCacheBonesContext & Context)
{
	InPose.CacheBones(Context);
}
