using UnrealBuildTool;

public class LADataStreamEditor : ModuleRules
{
	public LADataStreamEditor(ReadOnlyTargetRules Target) : base(Target)
	{
	    //we are going to be building without PCH's 
	    PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
        bEnforceIWYU = false;
        //PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "UnrealEd", "LADataStreamCore", "AnimGraph" });
        //PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "MessageLog", "AnimGraph", "BlueprintGraph" });
        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "UnrealEd", "LADataStreamCore",  "AnimGraph", "BlueprintGraph" });
        PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "MessageLog"});
        //PrivateIncludePathModuleNames.AddRange(new string[] { "AssetTools", "PropertyEditor" });
        //DynamicallyLoadedModuleNames.AddRange(new string[] { "AssetTools", "PropertyEditor" });

	}
}