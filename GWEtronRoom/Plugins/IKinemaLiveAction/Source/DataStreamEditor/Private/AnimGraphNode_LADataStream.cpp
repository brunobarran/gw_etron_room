/* Copyright (C) 2006-2017, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "AnimGraphNode_LADataStream.h"
#include "LADataStreamEditorPrivatePCH.h"
#include "Kismet2/CompilerResultsLog.h"

UAnimGraphNode_LADataStream::UAnimGraphNode_LADataStream(const FObjectInitializer&  PCIP)
	: Super(PCIP)
{
}

FText UAnimGraphNode_LADataStream::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	FString title("Mocap");
	switch (Node.MocapServer)
	{
	case EMocapServer::VICON:
		title = "Vicon";
		break;
	case EMocapServer::NatNet:
		title = "OptiTrack";
		break;
	default:
		break;
	}
	return FText::FromString(FString::Printf(TEXT("%s Stream"), (*title)));
}

FLinearColor UAnimGraphNode_LADataStream::GetNodeTitleColor() const
{
	return FLinearColor(0.75f, 0.75f, 0.75f);
}

FText UAnimGraphNode_LADataStream::GetTooltipText() const
{
	FString title("Mocap");
	return FText::FromString(FString::Printf(TEXT("Retrieves streamed skeletal animation from %s Server"), (*title)));
}

FString UAnimGraphNode_LADataStream::GetNodeCategory() const
{
	return FString("IKINEMA LiveAction");
}

void UAnimGraphNode_LADataStream::ValidateAnimNodeDuringCompilation(class USkeleton* ForSkeleton, class FCompilerResultsLog& MessageLog)
{
	if (Node.MocapServer == EMocapServer::None)
	{
		MessageLog.Error(TEXT("@@ Please chose a Mocap streaming server."), this);
	}
}