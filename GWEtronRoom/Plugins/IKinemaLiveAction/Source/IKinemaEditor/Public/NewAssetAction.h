#pragma once
#include "AssetRegistryModule.h"
/////////////////////////////////////////////////////
// TNewAssetAction
// Utility Action to add an asset to the graph
// This might be useful for other modules, so
// it's here in the Public folder.
//
template <class AssetType, class NodeType>
struct TNewAssetAction : public FEdGraphSchemaAction_K2NewNode
{
	TNewAssetAction(AssetType* asset, const FText& actionCategory)
	{
		check(asset != nullptr);

		NodeType* templateNode = NewObject<NodeType>();
		templateNode->Node.IKinemaRig = asset;
		NodeTemplate = templateNode;

		MenuDescriptionArray.Push(templateNode->GetNodeTitle(ENodeTitleType::ListView).ToString());

		MenuDescriptionArray.Push(templateNode->GetTooltipText().ToString());
		FullSearchCategoryArray.Push(actionCategory.ToString());

		// Grab extra keywords
		FullSearchKeywordsArray.Push(asset->ImportPath);
	}

	// Utility methods to use from the "GetMenuEntries" implementation of an AnimGraphNode.

	static void GetMenuEntriesAll(FGraphContextMenuBuilder& ContextMenuBuilder, const FText& actionCategory)
	{
		if ((ContextMenuBuilder.FromPin == NULL) || (UAnimationGraphSchema::IsPosePin(ContextMenuBuilder.FromPin->PinType) && (ContextMenuBuilder.FromPin->Direction == EGPD_Output)))
		{
			// Add an entry for each loaded asset.
			int i = 0;
			UBlueprint* Blueprint = FBlueprintEditorUtils::FindBlueprintForGraphChecked(ContextMenuBuilder.CurrentGraph);
			if (UAnimBlueprint* AnimBlueprint = Cast<UAnimBlueprint>(Blueprint))
			{
				// Load the asset registry module
				FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));

				// Filter by skeleton
				FAssetData SkeletonData(AnimBlueprint->TargetSkeleton);
				
				// Find matching assets and add an entry for each one
				for (TObjectIterator<AssetType> assetIt; assetIt; ++assetIt)
				{
					AssetType* asset = *assetIt;
					FAssetData RigSkeleton(asset->Skeleton);
					if (RigSkeleton.GetExportTextName() == SkeletonData.GetExportTextName())
					{
						ContextMenuBuilder.AddAction(TSharedPtr< TNewAssetAction<AssetType, NodeType> >(new TNewAssetAction<AssetType, NodeType>(asset, actionCategory)));
						i++;
					}
				}
			}
		}
	}
};
