/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

/*=============================================================================
	Implementing factory for an IKinema solver definition
=============================================================================*/

#include "IKinemaRigFactory.h"
#include "IKinemaEditorPrivatePCH.h"

#include "UnrealEd.h"
#include "Factories.h"
#include "SoundDefinitions.h"
#include "BlueprintUtilities.h"
#include "Kismet2/KismetEditorUtilities.h"
#include "BusyCursor.h"
#include "BSPOps.h"
#include "LevelUtils.h"
#include "ObjectTools.h"
#include "SSkeletonWidget.h"

#include "ContentBrowserModule.h"
#include "ClassViewerModule.h"
#include "ClassViewerFilter.h"
#include "JsonUtils.h"
#include "ContentBrowser/Private/SAssetDialog.h"

#define LOCTEXT_NAMESPACE "IKinemaRigFactory"

static FString GetExtension( FString fileName, bool bIncludeDot=false ) 
{
	FString Filename;
	int32 Pos = fileName.Find(TEXT("/"),ESearchCase::IgnoreCase,ESearchDir::FromEnd);
	// in case we are using backslashes on a platform that doesn't use backslashes
	Pos = FMath::Max(Pos, fileName.Find(TEXT("\\"),ESearchCase::IgnoreCase,ESearchDir::FromEnd));

	if ( Pos != INDEX_NONE )
	{
		Filename = fileName.Mid(Pos + 1);
	}
	
	
	int32 DotPos = Filename.Find(TEXT("."),ESearchCase::IgnoreCase,ESearchDir::FromEnd);
	if (DotPos != INDEX_NONE)
	{
		return Filename.Mid(DotPos + (bIncludeDot ? 0 : 1));
	}

	return TEXT("");
}


/*------------------------------------------------------------------------------
Dialog to configure creation properties
------------------------------------------------------------------------------*/
class SIKinemaRigCreateDialog : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SIKinemaRigCreateDialog){}

	SLATE_END_ARGS()

		/** Constructs this widget with InArgs */
		void Construct(const FArguments& InArgs)
	{
			bOkClicked = false;

			ChildSlot
				[
					SNew(SBorder)
					.Visibility(EVisibility::Visible)
					.BorderImage(FEditorStyle::GetBrush("Menu.Background"))
					[
						SNew(SBox)
						.Visibility(EVisibility::Visible)
						.WidthOverride(500.0f)
						[
							SNew(SVerticalBox)
							+ SVerticalBox::Slot()
							.FillHeight(1)
							.Padding(0.0f, 10.0f, 0.0f, 0.0f)
							[
								SNew(SBorder)
								.BorderImage(FEditorStyle::GetBrush("ToolPanel.GroupBorder"))
								.Content()
								[
									SAssignNew(SkeletonContainer, SVerticalBox)
								]
							]

							// Constraint Type
							+ SVerticalBox::Slot()
								.AutoHeight()
								.HAlign(HAlign_Left)
								.VAlign(VAlign_Center)
								.Padding(0)
								[
									SAssignNew(ParentConstraintbox, SCheckBox)
									.HAlign(HAlign_Center)
									.Content()
									[
										SNew(STextBlock)
										.Text(LOCTEXT("CreateIKinemaRigParentConstraint", "Enable Parent Contraints"))
									]
								]
							// Ok/Cancel buttons
							+ SVerticalBox::Slot()
								.AutoHeight()
								.HAlign(HAlign_Right)
								.VAlign(VAlign_Bottom)
								.Padding(8)
								[
									SNew(SUniformGridPanel)
									.SlotPadding(FEditorStyle::GetMargin("StandardDialog.SlotPadding"))
									.MinDesiredSlotWidth(FEditorStyle::GetFloat("StandardDialog.MinDesiredSlotWidth"))
									.MinDesiredSlotHeight(FEditorStyle::GetFloat("StandardDialog.MinDesiredSlotHeight"))
									+ SUniformGridPanel::Slot(0, 0)
									[
										SNew(SButton)
										.HAlign(HAlign_Center)
										.ContentPadding(FEditorStyle::GetMargin("StandardDialog.ContentPadding"))
										.OnClicked(this, &SIKinemaRigCreateDialog::OkClicked)
										.Text(LOCTEXT("CreateIKinemaRigOk", "OK"))
									]
									+ SUniformGridPanel::Slot(1, 0)
										[
											SNew(SButton)
											.HAlign(HAlign_Center)
											.ContentPadding(FEditorStyle::GetMargin("StandardDialog.ContentPadding"))
											.OnClicked(this, &SIKinemaRigCreateDialog::CancelClicked)
											.Text(LOCTEXT("CreateIKinemaRigCancel", "Cancel"))
										]
								]
						]
					]
				];
			MakeSkeletonPicker();
	}

	/** Sets properties for the supplied UIKinemaRigFactory */
	bool ConfigureProperties(TWeakObjectPtr<UIKinemaRigFactory> InAIKinemaRigFactory)
	{
		AnimBlueprintFactory = InAIKinemaRigFactory;

		TSharedRef<SWindow> Window = SNew(SWindow)
			.Title(LOCTEXT("CreateIKinemaRigOptions", "Create IKINEMA Rig"))
			.ClientSize(FVector2D(400, 450))
			.SupportsMinimize(false).SupportsMaximize(false)
			[
				AsShared()
			];

		PickerWindow = Window;

		GEditor->EditorAddModalWindow(Window);
		AnimBlueprintFactory.Reset();

		return bOkClicked;
	}

private:



	/** Creates the combo menu for the target skeleton */
	void MakeSkeletonPicker()
	{
		FContentBrowserModule& ContentBrowserModule = FModuleManager::Get().LoadModuleChecked<FContentBrowserModule>(TEXT("ContentBrowser"));

		FAssetPickerConfig AssetPickerConfig;
		AssetPickerConfig.Filter.ClassNames.Add(USkeleton::StaticClass()->GetFName());
		AssetPickerConfig.OnAssetSelected = FOnAssetSelected::CreateSP(this, &SIKinemaRigCreateDialog::OnSkeletonSelected);
		AssetPickerConfig.bAllowNullSelection = true;
		AssetPickerConfig.InitialAssetViewType = EAssetViewType::Column;
		AssetPickerConfig.InitialAssetSelection = TargetSkeleton;

		SkeletonContainer->ClearChildren();
		SkeletonContainer->AddSlot()
			.AutoHeight()
			[
				SNew(STextBlock)
				.Text(LOCTEXT("TargetSkeleton", "Target Skeleton:"))
				.ShadowOffset(FVector2D(1.0f, 1.0f))
			];

		SkeletonContainer->AddSlot()
			.FillHeight(1)
			[

				ContentBrowserModule.Get().CreateAssetPicker(AssetPickerConfig)
			];
	}

	/** Handler for when a skeleton is selected */
	void OnSkeletonSelected(const FAssetData& AssetData)
	{
		TargetSkeleton = AssetData;
	}

	/** Handler for when ok is clicked */
	FReply OkClicked()
	{
		if (AnimBlueprintFactory.IsValid())
		{
			AnimBlueprintFactory->TargetSkeleton = Cast<USkeleton>(TargetSkeleton.GetAsset());
			if (ParentConstraintbox.IsValid())
			{
				AnimBlueprintFactory->UseParentConstraint = ParentConstraintbox->IsChecked();
			}
		}

		if (!TargetSkeleton.IsValid())
		{
			// if TargetSkeleton is not valid
			FMessageDialog::Open(EAppMsgType::Ok, LOCTEXT("NeedValidSkeleton", "Must specify a valid skeleton for the IKINEMA rig to target."));
			return FReply::Handled();
		}

		CloseDialog(true);

		return FReply::Handled();
	}

	void CloseDialog(bool bWasPicked = false)
	{
		bOkClicked = bWasPicked;
		if (PickerWindow.IsValid())
		{
			PickerWindow.Pin()->RequestDestroyWindow();
		}
	}

	/** Handler for when cancel is clicked */
	FReply CancelClicked()
	{
		CloseDialog();
		return FReply::Handled();
	}

	FReply OnKeyDown(const FGeometry& MyGeometry, const FKeyEvent& InKeyboardEvent)
	{
		if (InKeyboardEvent.GetKey() == EKeys::Escape)
		{
			CloseDialog();
			return FReply::Handled();
		}
		return SWidget::OnKeyDown(MyGeometry, InKeyboardEvent);
	}

private:
	/** The factory for which we are setting up properties */
	TWeakObjectPtr<UIKinemaRigFactory> AnimBlueprintFactory;

	/** A pointer to the window that is asking the user to select a parent class */
	TWeakPtr<SWindow> PickerWindow;

	/** The container for the target skeleton picker*/
	TSharedPtr<SVerticalBox> SkeletonContainer;

	/** The Parent constraint object CheckBox */
	TSharedPtr<SCheckBox> ParentConstraintbox;

	/** The selected skeleton */
	FAssetData TargetSkeleton;

	/** True if Ok was clicked */
	bool bOkClicked;
};




UIKinemaRigFactory::UIKinemaRigFactory(const FObjectInitializer& PCIP)
	: Super(PCIP)
{
	// Property initialization
	SupportedClass = UIKinemaRig::StaticClass();
	//Description = LocalizeEnglish(LOC_NAMESPACE,TEXT("IKinemaRigFactoryDescription"),TEXT( "IKinemaRig"));
	bCreateNew = true;
	bEditAfterNew = true;
	bEditorImport = false;
	bText = false;
	ImportPriority = 1;
	Formats.Add(TEXT("ikn;IKinema rig definition"));

}

// Factory implementation.

UObject* UIKinemaRigFactory::FactoryCreateText(
	UClass* InClass,
	UObject* InParent,
	FName InName,
	EObjectFlags Flags,
	UObject* Context,
	const TCHAR* Type,
	const TCHAR*& Buffer,
	const TCHAR* BufferEnd,
	FFeedbackContext* Warn )
{
	UIKinemaRig* newRig = NewObject<UIKinemaRig>(InParent, InClass,InName,Flags);
	if (newRig == nullptr)
	{
		const TCHAR ErrorMsg[] = (TEXT("IKinemaRigFactory can only create an IKINEMA"));
		if (Warn)
		{

			Warn->Logf(ErrorMsg);
		}
		else
		{
			UE_LOG(LogIKinemaEditor, Error,TEXT("%s"), *ErrorMsg);
		}
		
		return nullptr;
	}

	// The current filename is set by the caller.
	newRig->ImportPath = IFileManager::Get().ConvertToRelativePath(*(UFactory::GetCurrentFilename()));

	if (!ReadFromText(newRig, Type, Buffer, BufferEnd, Warn) )
	{
		return nullptr;
	}

	//Import mocap actor skeleton
	FAssetToolsModule& AssetToolsModule = FModuleManager::Get().LoadModuleChecked<FAssetToolsModule>("AssetTools");
	UMocapActorSourceFactory* RigFactory = NewObject<UMocapActorSourceFactory>();
	// Get a unique name for the rig
	const FString DefaultSuffix = "_" + newRig->Actor->SubjectName + TEXT("_MocapActor");
	FString Name;
	FString PackageName;
	AssetToolsModule.Get().CreateUniqueAssetName(newRig->GetPathName(), DefaultSuffix, /*out*/ PackageName, /*out*/ Name);
	Name.RemoveFromStart(newRig->GetName() + "_");

	const FString PackagePath = FPackageName::GetLongPackagePath(PackageName);
	TArray<UObject*> ObjectsToSync;
	if (UObject* NewAsset = AssetToolsModule.Get().CreateAsset(Name + DefaultSuffix, PackagePath, UActorSkeleton::StaticClass(), RigFactory))
	{

		auto t = Cast<UActorSkeleton>(NewAsset);
		t->Bones = newRig->Actor->Bones;
		t->ImportScale = newRig->Actor->ImportScale;
		t->IsRigidBody = newRig->Actor->IsRigidBody;
		t->LegBones = newRig->Actor->LegBones;
		t->LegLength = newRig->Actor->LegLength;
		t->NameIndexMap = newRig->Actor->NameIndexMap;
		t->ServerIP = newRig->Actor->ServerIP;
		t->ServerPort = newRig->Actor->ServerPort;
		t->ServerType = newRig->Actor->ServerType;
		t->SourceTransform = newRig->Actor->SourceTransform;
		t->SubjectName = newRig->Actor->SubjectName;
		t->TemplateName = newRig->Actor->TemplateName;

		newRig->Actor->ConditionalBeginDestroy();

		newRig->Actor = t;
	}
	//end of mocap skeleton import

	if (!IsCompatibleSkeleton(newRig,TargetSkeleton))
	{
		newRig->Skeleton = TargetSkeleton;
		newRig->UseParentConstraint = UseParentConstraint;
		const TCHAR ErrorMsg[] = (TEXT("IKinemaRigFactory Selected skeleton is not compatible with the imported rig file."));
		if (Warn)
		{

			Warn->Logf(ErrorMsg);
		}
		else
		{
			UE_LOG(LogIKinemaEditor, Error,TEXT("%s"), *ErrorMsg);
		}
		return nullptr;
	}

	newRig->Skeleton = TargetSkeleton;
	newRig->UseParentConstraint = UseParentConstraint;

	return newRig;
}

bool UIKinemaRigFactory::IsCompatibleSkeleton(UIKinemaRig const * newRig, USkeleton * InSkeleton) const
{
	// at least % of bone should match 
	int32 NumOfBoneMatches = 0;


	const FReferenceSkeleton& SkeletonRefSkel = InSkeleton->GetReferenceSkeleton();
	const auto & rigBones = newRig->SolverDef.Bones;
	const int32 NumBones = rigBones.Num();

	// first ensure the parent exists for each bone
	for (int32 MeshBoneIndex = 0; MeshBoneIndex<NumBones; MeshBoneIndex++)
	{
		// See if Mesh bone exists in Skeleton.
		int32 SkeletonBoneIndex = SkeletonRefSkel.FindBoneIndex(rigBones[MeshBoneIndex].Name);

		// if found, increase num of bone matches count
		if (SkeletonBoneIndex != INDEX_NONE)
		{
			//Make sure that the IKinema parent matches the UE parent.
			int32 SkeletonParentBoneIndex = SkeletonRefSkel.GetParentIndex(SkeletonBoneIndex);
			if (SkeletonParentBoneIndex != INDEX_NONE)
			{
				if (SkeletonRefSkel.GetBoneName(SkeletonParentBoneIndex) == rigBones[MeshBoneIndex].ParentName || MeshBoneIndex == 0 )
				{
					++NumOfBoneMatches;
				}
				else
				{
					return false;
				}
			}
			else
			{
				++NumOfBoneMatches;
			}
		}
	}

	// if the hierarchy matches all bones, we allow
	return (NumOfBoneMatches == NumBones);
}

// Implementing FReimportHandler interface.

bool UIKinemaRigFactory::CanReimport( UObject* Obj, TArray<FString>& OutFilenames )
{	
	UIKinemaRig* rig = Cast<UIKinemaRig>(Obj);
	if (rig != nullptr)
	{
		// If the import path is empty, UE4 will ask the user to select it.
		const FString& importPath = rig->ImportPath;
		if (!importPath.IsEmpty())
		{
			OutFilenames.Add(IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*importPath));
		}
		return true;
	}
	return false;
}

void UIKinemaRigFactory::SetReimportPaths( UObject* Obj, const TArray<FString>& NewReimportPaths )
{	
	UIKinemaRig* rig = Cast<UIKinemaRig>(Obj);
	if ((rig != nullptr) && ensure(NewReimportPaths.Num() == 1))
	{
		rig->ImportPath = IFileManager::Get().ConvertToRelativePath(*NewReimportPaths[0]);
	}
}

EReimportResult::Type UIKinemaRigFactory::Reimport( UObject* Obj )
{	
	UIKinemaRig* rig = Cast<UIKinemaRig>(Obj);
	if (rig == nullptr)
	{
		return EReimportResult::Failed;
	}

	const FString& importPath = rig->ImportPath;

	if(importPath.IsEmpty())
	{
		return EReimportResult::Failed;
	}

	FString filePath = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*importPath);

	FString stringDataFromFile;
	if( !FFileHelper::LoadFileToString( stringDataFromFile, *filePath) )
	{
		return EReimportResult::Failed;
	}

	const TCHAR* stringDataPtr = *stringDataFromFile;

	int32 PrevUpdateVersion = rig->UpdateVersion;
	// Reset the rig's solver definition to the default values.
	rig->PreReimport();

	// Now read from the json file.
	if ( !ReadFromText(
		rig,
		*GetExtension(filePath),
		stringDataPtr,
		stringDataPtr+stringDataFromFile.Len(),
		NULL ) )
	{
		return EReimportResult::Type::Failed;
	}

	//Bump the version and force update in all FAnimNode_IKinemaSolver nodes
	rig->UpdateVersion = PrevUpdateVersion+1;
	// Needs save.
	rig->MarkPackageDirty();

	return EReimportResult::Succeeded;
}

UObject* UIKinemaRigFactory::FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn)
{
	UIKinemaRig* NewRig = NewObject<UIKinemaRig>(InParent, Class, Name, Flags | RF_Transactional);

	if (NewRig && TargetSkeleton)
	{
		NewRig->Skeleton = TargetSkeleton;
		if (NewRig->CreateRigFromSkeleton())
		{
			NewRig->PostLoadOrImport();
		}
	}
	return NewRig;
}

bool UIKinemaRigFactory::ConfigureProperties()
{
	TSharedRef<SIKinemaRigCreateDialog> Dialog = SNew(SIKinemaRigCreateDialog);
	return Dialog->ConfigureProperties(this);
};

// Read from a text buffer.
bool UIKinemaRigFactory::ReadFromText(class UIKinemaRig* rig, const TCHAR* type, const TCHAR*& buffer, const TCHAR* bufferEnd, FFeedbackContext* warn)
{
	if (rig == nullptr)
	{
		UE_LOG(LogIKinemaEditor, Warning, TEXT("IKinema rig factory import should be given a rig to import onto"));
		return false;
	}

	FString fileContents = buffer;
	bool success = FJsonUtils::ImportObjectFromJsonString(rig, fileContents);
	if (success)
	{
		rig->PostLoadOrImport();
	}
	return success;
}

#undef LOCTEXT_NAMESPACE