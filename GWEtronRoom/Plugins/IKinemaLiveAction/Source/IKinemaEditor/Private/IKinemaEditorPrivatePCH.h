#pragma once

#include "UnrealEd.h"
//#if !UE_ROCKET
//	#include "EngineAnimationNodeClasses.h"
//#endif
#include "BlueprintGraphClasses.h"
#include "AnimGraphClasses.h"
#include "Kismet2/BlueprintEditorUtils.h"
#include "IKinemaCoreClasses.h"

#include "IKinemaEditorClasses.h"
#include "AssetTypeActions_IKinema.h"
#include "IKinemaEditorModule.h"
