/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "AnimGraphNode_IKinemaMarkerSolving.h"
#include "IKinemaEditorPrivatePCH.h"
#include "BlueprintActionFilter.h"
#include "BlueprintActionDatabaseRegistrar.h"
#include "Kismet2/CompilerResultsLog.h"
#include "NewAssetAction.h"

UAnimGraphNode_IKinemaMarkerSolving::UAnimGraphNode_IKinemaMarkerSolving(const FObjectInitializer& PCIP)
	: Super(PCIP)
{
}

FText UAnimGraphNode_IKinemaMarkerSolving::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return FText::FromString(FString::Printf(TEXT("Marker Solving using IKINEMA rig %s"), (Node.IKinemaRig != NULL) ? *(Node.IKinemaRig->GetName()) : TEXT("(None)")));
}

FLinearColor UAnimGraphNode_IKinemaMarkerSolving::GetNodeTitleColor() const
{
	return FLinearColor(0.75f, 0.75f, 0.75f);
}

FText UAnimGraphNode_IKinemaMarkerSolving::GetTooltipText() const
{
	return FText::FromString(FString::Printf(TEXT("Solve markers pose using IKINEMA rig")));
}

FString UAnimGraphNode_IKinemaMarkerSolving::GetNodeCategory() const
{
	return TEXT("IKINEMA");
}

void UAnimGraphNode_IKinemaMarkerSolving::GetMenuEntries(FGraphContextMenuBuilder& ContextMenuBuilder) const
{
	TNewAssetAction<UIKinemaRig, UAnimGraphNode_IKinemaMarkerSolving>::GetMenuEntriesAll(ContextMenuBuilder, FText::FromString("IKinema"));
}

void UAnimGraphNode_IKinemaMarkerSolving::GetMenuActions(FBlueprintActionDatabaseRegistrar& ActionRegistrar) const
{
	auto PostSpawnSetupLambda = [](UEdGraphNode* NewNode, bool bIsTemplateNode, UIKinemaRig* asset)
	{
		UAnimGraphNode_IKinemaMarkerSolving* SolverNode = CastChecked<UAnimGraphNode_IKinemaMarkerSolving>(NewNode);
		//We set the rig associated with the Node
		SolverNode->Node.SetAsset(asset);
	};

	const UObject* QueryObject = ActionRegistrar.GetActionKeyFilter();
	if (QueryObject == nullptr)
	{
		FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
		// define a filter to help in pulling IKinemaRig asset data from the registry
		FARFilter Filter;
		Filter.ClassNames.Add(UIKinemaRig::StaticClass()->GetFName());
		Filter.bRecursiveClasses = true;
		// Find matching assets and add an entry for each one
		TArray<FAssetData> RigList;
		AssetRegistryModule.Get().GetAssets(Filter, /*out*/RigList);

		for (auto AssetIt = RigList.CreateConstIterator(); AssetIt; ++AssetIt)
		{
			const FAssetData& Asset = *AssetIt;

			UBlueprintNodeSpawner* NodeSpawner = UBlueprintNodeSpawner::Create(GetClass());
			check(NodeSpawner != nullptr);
			NodeSpawner->CustomizeNodeDelegate = UBlueprintNodeSpawner::FCustomizeNodeDelegate::CreateStatic(PostSpawnSetupLambda, Cast<UIKinemaRig>(Asset.GetAsset()));
			ActionRegistrar.AddBlueprintAction(Asset, NodeSpawner);
		}
	}
	else if (const UIKinemaRig* Rig = Cast<UIKinemaRig>(QueryObject))
	{
		UBlueprintNodeSpawner* NodeSpawner = UBlueprintNodeSpawner::Create(GetClass());

		TWeakObjectPtr<UIKinemaRig> RigPtr = MakeWeakObjectPtr(const_cast<UIKinemaRig*>(Rig));
		NodeSpawner->CustomizeNodeDelegate = UBlueprintNodeSpawner::FCustomizeNodeDelegate::CreateStatic(PostSpawnSetupLambda, RigPtr.Get());
		ActionRegistrar.AddBlueprintAction(QueryObject, NodeSpawner);
	}
	else if (QueryObject == GetClass())
	{
		FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
		// define a filter to help in pulling UAnimSequence asset data from the registry
		FARFilter Filter;
		Filter.ClassNames.Add(UIKinemaRig::StaticClass()->GetFName());
		Filter.bRecursiveClasses = true;
		// Find matching assets and add an entry for each one
		TArray<FAssetData> RigList;
		AssetRegistryModule.Get().GetAssets(Filter, /*out*/RigList);

		for (auto AssetIt = RigList.CreateConstIterator(); AssetIt; ++AssetIt)
		{
			const FAssetData& Asset = *AssetIt;

			UBlueprintNodeSpawner* NodeSpawner = UBlueprintNodeSpawner::Create(GetClass());
			NodeSpawner->CustomizeNodeDelegate = UBlueprintNodeSpawner::FCustomizeNodeDelegate::CreateStatic(PostSpawnSetupLambda, Cast<UIKinemaRig>(Asset.GetAsset()));
			ActionRegistrar.AddBlueprintAction(Asset, NodeSpawner);
		}
	}

}

bool UAnimGraphNode_IKinemaMarkerSolving::IsActionFilteredOut(class FBlueprintActionFilter const& Filter)
{
	bool bIsFilteredOut = false;
	FBlueprintActionContext const& FilterContext = Filter.Context;

	for (UBlueprint* Blueprint : FilterContext.Blueprints)
	{
		if (UAnimBlueprint* AnimBlueprint = Cast<UAnimBlueprint>(Blueprint))
		{
			if (!Node.IKinemaRig->Skeleton)
			{
				bIsFilteredOut = false;
				break;
			}
			if (Node.IKinemaRig->Skeleton != AnimBlueprint->TargetSkeleton)
			{
				// Sequence does not use the same skeleton as the Blueprint, cannot use
				bIsFilteredOut = true;
				break;
			}
		}

		else
		{
			// Not an animation Blueprint, cannot use
			bIsFilteredOut = true;
			break;
		}
	}
	return bIsFilteredOut;
}


void UAnimGraphNode_IKinemaMarkerSolving::CustomizePinData(UEdGraphPin* Pin, FName SourcePropertyName, int32 ArrayIndex) const
{
	if (Node.IKinemaRig == nullptr)
	{
		return;
	}

	if (SourcePropertyName == TEXT("Tasks"))
	{
		const FIKinemaSolverDef& solverDef = Node.IKinemaRig->SolverDef;
		if ((ArrayIndex >= 0) && (ArrayIndex < solverDef.Tasks.Num()))
		{
			Pin->PinFriendlyName = FText::FromString(FString::Printf(TEXT("%s"), *(solverDef.Tasks[ArrayIndex].Name.ToString())));
			//Pin->bHidden = true;
		}
	}
	else if (SourcePropertyName == TEXT("TaskProperties"))
	{
		const FIKinemaSolverDef& solverDef = Node.IKinemaRig->SolverDef;
		if ((ArrayIndex >= 0) && (ArrayIndex < solverDef.Tasks.Num()))
		{
			Pin->PinFriendlyName = FText::FromString(FString::Printf(TEXT("Properties_%s"), *(solverDef.Tasks[ArrayIndex].Name.ToString())));
		}
	}
	else if (SourcePropertyName == TEXT("SegmentProperties"))
	{
		const FIKinemaSolverDef& solverDef = Node.IKinemaRig->SolverDef;
		if ((ArrayIndex >= 0) && (ArrayIndex < solverDef.Bones.Num()))
		{
			Pin->PinFriendlyName = FText::FromString(FString::Printf(TEXT("Properties_%s"), *(solverDef.Bones[ArrayIndex].Name.ToString())));
		}
	}
}

void UAnimGraphNode_IKinemaMarkerSolving::ValidateAnimNodeDuringCompilation(class USkeleton* ForSkeleton, class FCompilerResultsLog& MessageLog)
{
	if (Node.IKinemaRig == NULL)
	{
		MessageLog.Error(TEXT("@@ references an unknown IKinema rig."), this);
	}
}

void UAnimGraphNode_IKinemaMarkerSolving::PreloadRequiredAssets()
{
	PreloadObject(Node.IKinemaRig);

	Super::PreloadRequiredAssets();
}




//Some crazy stuff with pins
///////////////////////////////////////////////////////////////////////
// FA3NodeOptionalPinManager

struct FIKMarkerSolverNodeOptionalPinManager : public FOptionalPinManager
{
protected:
	class UAnimGraphNode_Base* BaseNode;
	TArray<UEdGraphPin*>* OldPins;

	TMap<FString, UEdGraphPin*> OldPinMap;

public:
	FIKMarkerSolverNodeOptionalPinManager(class UAnimGraphNode_Base* Node, TArray<UEdGraphPin*>* InOldPins)
		: BaseNode(Node)
		, OldPins(InOldPins)
	{
		if (OldPins != NULL)
		{
			for (auto PinIt = OldPins->CreateIterator(); PinIt; ++PinIt)
			{
				UEdGraphPin* Pin = *PinIt;
				OldPinMap.Add(Pin->PinName.ToString(), Pin);
			}
		}
	}

	virtual ~FIKMarkerSolverNodeOptionalPinManager()
	{}
	void GetRecordDefaults(UProperty* TestProperty, FOptionalPinFromProperty& Record) const override
	{
		const UAnimationGraphSchema* Schema = GetDefault<UAnimationGraphSchema>();

		// Determine if this is a pose or array of poses
		UArrayProperty* ArrayProp = Cast<UArrayProperty>(TestProperty);
		UStructProperty* StructProp = Cast<UStructProperty>((ArrayProp != NULL) ? ArrayProp->Inner : TestProperty);
		const bool bIsPoseInput = (StructProp != NULL) && (StructProp->Struct->IsChildOf(FPoseLinkBase::StaticStruct()));

		//@TODO: Error if they specified two or more of these flags
		const bool bAlwaysShow = TestProperty->HasMetaData(Schema->NAME_AlwaysAsPin) || bIsPoseInput;
		const bool bOptional_ShowByDefault = TestProperty->HasMetaData(Schema->NAME_PinShownByDefault);
		const bool bOptional_HideByDefault = TestProperty->HasMetaData(Schema->NAME_PinHiddenByDefault);
		const bool bNeverShow = TestProperty->HasMetaData(Schema->NAME_NeverAsPin);

		Record.bCanToggleVisibility = bOptional_ShowByDefault || bOptional_HideByDefault;
		Record.bShowPin = bAlwaysShow || bOptional_ShowByDefault || !bNeverShow;
	}
	void CustomizePinData(UEdGraphPin* Pin, FName SourcePropertyName, int32 ArrayIndex, UProperty* Property = NULL) const override
	{
		if (BaseNode != NULL)
		{
			BaseNode->CustomizePinData(Pin, SourcePropertyName, ArrayIndex);
		}
	}

	void PostInitNewPin(UEdGraphPin* Pin, FOptionalPinFromProperty& Record, int32 ArrayIndex, UProperty* Property, uint8* PropertyAddress, uint8* DefeaultPropertyAddress) const override
	{
		check(PropertyAddress != NULL);
		check(Record.bShowPin);

		if (OldPins == NULL)
		{
			// Initial construction of a visible pin; copy values from the struct
			FString LiteralValue;
			FBlueprintEditorUtils::PropertyValueToString_Direct(Property, PropertyAddress, LiteralValue);
		}
		else if (Record.bCanToggleVisibility)
		{
			if (OldPinMap.FindRef(Pin->PinName.ToString()))
			{
				// Was already visible
			}
			else
			{
				// Showing a pin that was previously hidden, during a reconstruction
				// Convert the struct property into DefaultValue/DefaultValueObject
				FString LiteralValue;
				FBlueprintEditorUtils::PropertyValueToString_Direct(Property, PropertyAddress, LiteralValue);
			}
		}
	}

	void PostRemovedOldPin(FOptionalPinFromProperty& Record, int32 ArrayIndex, UProperty* Property, uint8* PropertyAddress, uint8* DefaultPropertyAddress) const override
	{
		check(PropertyAddress != NULL);
		check(!Record.bShowPin);

		if (Record.bCanToggleVisibility && (OldPins != NULL))
		{
			const FString OldPinName = (ArrayIndex != INDEX_NONE) ? FString::Printf(TEXT("%s_%d"), *(Record.PropertyName.ToString()), ArrayIndex) : Record.PropertyName.ToString();
			if (UEdGraphPin* OldPin = OldPinMap.FindRef(OldPinName))
			{
				// Pin was visible but it's now hidden
				// Convert DefaultValue/DefaultValueObject and push back into the struct
				FBlueprintEditorUtils::PropertyValueFromString_Direct(Property, OldPin->GetDefaultAsString(), PropertyAddress);
			}
		}
	}

	void IKCreateVisiblePins(TArray<FOptionalPinFromProperty>& Properties, UStruct* SourceStruct, EEdGraphPinDirection Direction, UK2Node* TargetNode, uint8* StructBasePtr)
	{
		const UEdGraphSchema_K2* Schema = GetDefault<UEdGraphSchema_K2>();

		for (auto ExtraPropertyIt = Properties.CreateIterator(); ExtraPropertyIt; ++ExtraPropertyIt)
		{
			FOptionalPinFromProperty& PropertyEntry = *ExtraPropertyIt;

			if (UProperty* OuterProperty = FindFieldChecked<UProperty>(SourceStruct, PropertyEntry.PropertyName))
			{
				// Do we treat an array property as one pin, or a pin per entry in the array?
				// Depends on if we have an instance of the struct to work with.
				UArrayProperty* ArrayProperty = Cast<UArrayProperty>(OuterProperty);
				if ((ArrayProperty != NULL) && (StructBasePtr != NULL))
				{
					UProperty* InnerProperty = ArrayProperty->Inner;

					FEdGraphPinType PinType;
					if (Schema->ConvertPropertyToPinType(InnerProperty, PinType))
					{
						FScriptArrayHelper_InContainer ArrayHelper(ArrayProperty, StructBasePtr);

						UArrayProperty* CheckArrayProp = NULL;
						bool checkTask = false;
						if (PropertyEntry.PropertyName == "TaskProperties")
						{
							UProperty* OverrideTaskProperty = FindFieldChecked<UProperty>(SourceStruct, "PinTaskProperties");
							CheckArrayProp = Cast<UArrayProperty>(OverrideTaskProperty);
							checkTask = true;
						}
						else if (PropertyEntry.PropertyName == "SegmentProperties")
						{
							UProperty* OverrideSegmentProperty = FindFieldChecked<UProperty>(SourceStruct, "PinSegmentProperties");
							CheckArrayProp = Cast<UArrayProperty>(OverrideSegmentProperty);
						}
						if (PropertyEntry.PropertyName == "Tasks")
						{
							// Hack to hide the marker solving tasks pins from the graph
							// regardless of user input
							PropertyEntry.bShowPin = false;
						}

						for (int32 Index = 0; Index < ArrayHelper.Num(); ++Index)
						{
							const FString PinName = FString::Printf(TEXT("%s_%d"), *(PropertyEntry.PropertyName.ToString()), Index);

							// Create the pin
							UEdGraphPin* NewPin = NULL;

							bool ArrayPinEnabled = true;
							if (CheckArrayProp != NULL)
							{
								FScriptArrayHelper_InContainer CheckArray(CheckArrayProp, StructBasePtr);
								if (checkTask)
								{
									FIKinemaTaskOverride* taskOverride = (FIKinemaTaskOverride*)CheckArray.GetRawPtr(Index);
									if (taskOverride)
									{
										ArrayPinEnabled = taskOverride->ShowAsPin;
									}
								}
								else
								{
									FIKinemaSegmentOverride* segOverride = (FIKinemaSegmentOverride*)CheckArray.GetRawPtr(Index);
									if (segOverride)
									{
										ArrayPinEnabled = segOverride->ShowAsPin;
									}
								}
							}

							if (PropertyEntry.bShowPin && ArrayPinEnabled)
							{
								NewPin = TargetNode->CreatePin(Direction, PinType.PinCategory, PinType.PinSubCategory, PinType.PinSubCategoryObject.Get(), FName(*PinName));
								// Allow the derived class to customize the created pin
								CustomizePinData(NewPin, PropertyEntry.PropertyName, Index);
							}

							// Let derived classes take a crack at transferring default values
							uint8* ValuePtr = ArrayHelper.GetRawPtr(Index);
							if (NewPin != NULL)
							{
								PostInitNewPin(NewPin, PropertyEntry, Index, ArrayProperty->Inner, ValuePtr, nullptr);
							}
							else if (ArrayPinEnabled)
							{
								PostRemovedOldPin(PropertyEntry, Index, ArrayProperty->Inner, ValuePtr, nullptr);
							}
						}
					}
				}
				else
				{
					// Not an array property

					FEdGraphPinType PinType;
					if (Schema->ConvertPropertyToPinType(OuterProperty, PinType))
					{
						FString PinName = PropertyEntry.PropertyName.ToString();

						// Create the pin
						UEdGraphPin* NewPin = NULL;
						if (PropertyEntry.bShowPin)
						{
							NewPin = TargetNode->CreatePin(Direction, PinType.PinCategory, PinType.PinSubCategory, PinType.PinSubCategoryObject.Get(), FName(*PinName));
							// Allow the derived class to customize the created pin
							CustomizePinData(NewPin, PropertyEntry.PropertyName, INDEX_NONE);
						}

						// Let derived classes take a crack at transferring default values
						if (StructBasePtr != NULL)
						{
							uint8* ValuePtr = OuterProperty->ContainerPtrToValuePtr<uint8>(StructBasePtr);
							if (NewPin != NULL)
							{
								PostInitNewPin(NewPin, PropertyEntry, INDEX_NONE, OuterProperty, ValuePtr, nullptr);
							}
							else
							{
								PostRemovedOldPin(PropertyEntry, INDEX_NONE, OuterProperty, ValuePtr, nullptr);
							}
						}
					}
				}
			}
		}
	}

	void AllocateDefaultPins(UStruct* SourceStruct, uint8* StructBasePtr)
	{
		RebuildPropertyList(BaseNode->ShowPinForProperties, SourceStruct);
		IKCreateVisiblePins(BaseNode->ShowPinForProperties, SourceStruct, EGPD_Input, BaseNode, StructBasePtr);
	}
};

void UAnimGraphNode_IKinemaMarkerSolving::IKinemaInternalPinCreation(TArray<UEdGraphPin*>* OldPins)
{
	// preload required assets first before creating pins
	PreloadRequiredAssets();


	if (UStructProperty* NodeStruct = GetFNodeProperty())
	{
		// Display any currently visible optional pins
		{
			FIKMarkerSolverNodeOptionalPinManager OptionalPinManager(this, OldPins);
			OptionalPinManager.AllocateDefaultPins(NodeStruct->Struct, NodeStruct->ContainerPtrToValuePtr<uint8>(this));
		}

		// Create the output pin, if needed
		CreateOutputPins();
	}
}

void UAnimGraphNode_IKinemaMarkerSolving::AllocateDefaultPins()
{
	IKinemaInternalPinCreation(NULL);
}

void UAnimGraphNode_IKinemaMarkerSolving::ReallocatePinsDuringReconstruction(TArray<UEdGraphPin*>& OldPins)
{
	IKinemaInternalPinCreation(&OldPins);
}
