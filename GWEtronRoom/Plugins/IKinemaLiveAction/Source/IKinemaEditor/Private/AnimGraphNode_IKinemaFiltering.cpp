/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "AnimGraphNode_IKinemaFiltering.h"
#include "IKinemaEditorPrivatePCH.h"
#include "BlueprintActionDatabaseRegistrar.h"
#include "Kismet2/CompilerResultsLog.h"
#include "NewAssetAction.h"
#include "ARFilter.h"


UAnimGraphNode_IKinemaFiltering::UAnimGraphNode_IKinemaFiltering(const FObjectInitializer& PCIP)
: Super(PCIP)
{
}

FText UAnimGraphNode_IKinemaFiltering::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return FText::FromString(FString::Printf(TEXT("Motion Filtering & Cleaning using IKINEMA rig %s"), (Node.IKinemaRig != NULL) ? *(Node.IKinemaRig->GetName()) : TEXT("(None)")));
}

FLinearColor UAnimGraphNode_IKinemaFiltering::GetNodeTitleColor() const
{
	return FLinearColor(0.75f, 0.75f, 0.75f);
}

FText UAnimGraphNode_IKinemaFiltering::GetTooltipText() const
{
	return FText::FromString(FString::Printf(TEXT("Cleaned pose")));
}


FString UAnimGraphNode_IKinemaFiltering::GetNodeCategory() const
{
	return FString("IKINEMA");
}

void UAnimGraphNode_IKinemaFiltering::PreloadRequiredAssets()
{
	PreloadObject(Node.IKinemaRig);

	Super::PreloadRequiredAssets();
}

void UAnimGraphNode_IKinemaFiltering::ValidateAnimNodeDuringCompilation(class USkeleton* ForSkeleton, class FCompilerResultsLog& MessageLog)
{
	if (Node.IKinemaRig == NULL)
	{
		MessageLog.Error(TEXT("@@ references an unknown IKinema rig."), this);
	}
}

void UAnimGraphNode_IKinemaFiltering::GetMenuEntries(FGraphContextMenuBuilder& ContextMenuBuilder) const
{
	TNewAssetAction<UIKinemaRig, UAnimGraphNode_IKinemaFiltering>::GetMenuEntriesAll(ContextMenuBuilder, FText::FromString("IKinema"));
}

void UAnimGraphNode_IKinemaFiltering::GetMenuActions(FBlueprintActionDatabaseRegistrar& ActionRegistrar) const
{
	auto LoadedAssetSetup = [](UEdGraphNode* NewNode, bool bIsTemplateNode, UIKinemaRig* asset)
	{
		UAnimGraphNode_IKinemaFiltering* SolverNode = CastChecked<UAnimGraphNode_IKinemaFiltering>(NewNode);
		//We set the rig associated with the Node
		SolverNode->Node.IKinemaRig = asset;
	};

	const UObject* QueryObject = ActionRegistrar.GetActionKeyFilter();
	if (QueryObject == nullptr)
	{
		FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
		// define a filter to help in pulling IKinemaRig asset data from the registry
		FARFilter Filter;
		Filter.ClassNames.Add(UIKinemaRig::StaticClass()->GetFName());
		Filter.bRecursiveClasses = true;
		// Find matching assets and add an entry for each one
		TArray<FAssetData> RigList;
		AssetRegistryModule.Get().GetAssets(Filter, /*out*/RigList);

		for (auto AssetIt = RigList.CreateConstIterator(); AssetIt; ++AssetIt)
		{
			const FAssetData& Asset = *AssetIt;

			UBlueprintNodeSpawner* NodeSpawner = UBlueprintNodeSpawner::Create(GetClass());
			check(NodeSpawner != nullptr);
			//Include one rig per skeleton
			auto rig = Cast<UIKinemaRig>(Asset.GetAsset());
			NodeSpawner->CustomizeNodeDelegate = UBlueprintNodeSpawner::FCustomizeNodeDelegate::CreateStatic(LoadedAssetSetup, rig);
			ActionRegistrar.AddBlueprintAction(Asset, NodeSpawner);
		}
	}
	else if (const UIKinemaRig* Rig = Cast<UIKinemaRig>(QueryObject))
	{
		UBlueprintNodeSpawner* NodeSpawner = UBlueprintNodeSpawner::Create(GetClass());

		TWeakObjectPtr<UIKinemaRig> RigPtr = MakeWeakObjectPtr(const_cast<UIKinemaRig*>(Rig));
		NodeSpawner->CustomizeNodeDelegate = UBlueprintNodeSpawner::FCustomizeNodeDelegate::CreateStatic(LoadedAssetSetup, RigPtr.Get());
		ActionRegistrar.AddBlueprintAction(QueryObject, NodeSpawner);
	}
	else if (QueryObject == GetClass())
	{
		FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
		// define a filter to help in pulling UAnimSequence asset data from the registry
		FARFilter Filter;
		Filter.ClassNames.Add(UIKinemaRig::StaticClass()->GetFName());
		Filter.bRecursiveClasses = true;
		// Find matching assets and add an entry for each one
		TArray<FAssetData> RigList;
		AssetRegistryModule.Get().GetAssets(Filter, /*out*/RigList);

		for (auto AssetIt = RigList.CreateConstIterator(); AssetIt; ++AssetIt)
		{
			const FAssetData& Asset = *AssetIt;

			UBlueprintNodeSpawner* NodeSpawner = UBlueprintNodeSpawner::Create(GetClass());
			//Include one rig per skeleton
			auto rig = Cast<UIKinemaRig>(Asset.GetAsset());
			NodeSpawner->CustomizeNodeDelegate = UBlueprintNodeSpawner::FCustomizeNodeDelegate::CreateStatic(LoadedAssetSetup, rig);
			ActionRegistrar.AddBlueprintAction(Asset, NodeSpawner);
		}
	}

}

bool UAnimGraphNode_IKinemaFiltering::IsActionFilteredOut(class FBlueprintActionFilter const& Filter)
{
	bool bIsFilteredOut = false;
	FBlueprintActionContext const& FilterContext = Filter.Context;

	for (UBlueprint* Blueprint : FilterContext.Blueprints)
	{
		if (UAnimBlueprint* AnimBlueprint = Cast<UAnimBlueprint>(Blueprint))
		{
			if (!Node.IKinemaRig->Skeleton)
			{
				bIsFilteredOut = true;
				break;
			}
			if (Node.IKinemaRig->Skeleton != AnimBlueprint->TargetSkeleton)
			{
				//rig does not use the same skeleton as the Blueprint, cannot use
				bIsFilteredOut = true;
				break;
			}
		}

		else
		{
			// Not an animation Blueprint, cannot use
			bIsFilteredOut = true;
			break;
		}
	}
	return bIsFilteredOut;
}

