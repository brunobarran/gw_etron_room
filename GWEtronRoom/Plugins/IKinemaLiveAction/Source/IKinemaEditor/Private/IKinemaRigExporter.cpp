/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaRigExporter.h"
#include "IKinemaEditorPrivatePCH.h"
#include "JsonUtils.h"
#include "UnrealEd.h"
#include "IKinemaRig.h"

UIKinemaRigExporter::UIKinemaRigExporter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SupportedClass = UIKinemaRig::StaticClass();
	bText = true;
	PreferredFormatIndex = 0;
	FormatExtension.Add(TEXT("ikn"));
	FormatDescription.Add(TEXT("IKINEMA Rig File"));
}

bool UIKinemaRigExporter::ExportText(const FExportObjectInnerContext* Context, UObject* Object, const TCHAR* Type, FOutputDevice& Ar, FFeedbackContext* Warn, uint32 PortFlags)
{
	FString fileContents;
	bool success = FJsonUtils::ExportObjectToJsonString(Object, fileContents, false);
	Ar.Log(*fileContents);
	return success;
}