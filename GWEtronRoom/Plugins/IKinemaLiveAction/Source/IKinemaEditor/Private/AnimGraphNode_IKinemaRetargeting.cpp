/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "AnimGraphNode_IKinemaRetargeting.h"
#include "IKinemaEditorPrivatePCH.h"
#include "BlueprintActionFilter.h"
#include "BlueprintActionDatabaseRegistrar.h"
#include "Kismet2/CompilerResultsLog.h"
#include "NewAssetAction.h"

UAnimGraphNode_IKinemaRetargeting::UAnimGraphNode_IKinemaRetargeting(const FObjectInitializer&  PCIP)
	: Super(PCIP)
{
}

FText UAnimGraphNode_IKinemaRetargeting::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return FText::FromString(FString::Printf(TEXT("Retargeting using IKINEMA rig %s"), (Node.IKinemaRig != NULL) ? *(Node.IKinemaRig->GetName()) : TEXT("(None)")));
}

FLinearColor UAnimGraphNode_IKinemaRetargeting::GetNodeTitleColor() const
{
	return FLinearColor(0.75f, 0.75f, 0.75f);
}

FText UAnimGraphNode_IKinemaRetargeting::GetTooltipText() const
{
	return FText::FromString(TEXT("Retargeting pose using IKINEMA rig"));
}

FString UAnimGraphNode_IKinemaRetargeting::GetNodeCategory() const
{
	return (FString::Printf(TEXT("IKINEMA")));
}

void UAnimGraphNode_IKinemaRetargeting::GetMenuEntries(FGraphContextMenuBuilder& ContextMenuBuilder) const
{
	TNewAssetAction<UIKinemaRig, UAnimGraphNode_IKinemaRetargeting>::GetMenuEntriesAll(ContextMenuBuilder, FText::FromString("IKINEMA"));
}

void UAnimGraphNode_IKinemaRetargeting::GetMenuActions(FBlueprintActionDatabaseRegistrar& ActionRegistrar) const
{
	auto LoadedAssetSetup = [](UEdGraphNode* NewNode, bool bIsTemplateNode, UIKinemaRig* asset)
	{
		UAnimGraphNode_IKinemaRetargeting* SolverNode = CastChecked<UAnimGraphNode_IKinemaRetargeting>(NewNode);
		//We set the rig associated with the Node
		SolverNode->Node.SetAsset(asset);
	};
	
	const UObject* QueryObject = ActionRegistrar.GetActionKeyFilter();
	if (QueryObject == nullptr)
	{
		FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
		// define a filter to help in pulling IKinemaRig asset data from the registry
		FARFilter Filter;
		Filter.ClassNames.Add(UIKinemaRig::StaticClass()->GetFName());
		Filter.bRecursiveClasses = true;
		// Find matching assets and add an entry for each one
		TArray<FAssetData> RigList;
		AssetRegistryModule.Get().GetAssets(Filter, /*out*/RigList);

		for (auto AssetIt = RigList.CreateConstIterator(); AssetIt; ++AssetIt)
		{
			const FAssetData& Asset = *AssetIt;

			UBlueprintNodeSpawner* NodeSpawner = UBlueprintNodeSpawner::Create(GetClass());
			check(NodeSpawner != nullptr);
			NodeSpawner->CustomizeNodeDelegate = UBlueprintNodeSpawner::FCustomizeNodeDelegate::CreateStatic(LoadedAssetSetup, Cast<UIKinemaRig>(Asset.GetAsset()));
			ActionRegistrar.AddBlueprintAction(Asset, NodeSpawner);
		}
	}
	else if (const UIKinemaRig* Rig = Cast<UIKinemaRig>(QueryObject))
	{
		UBlueprintNodeSpawner* NodeSpawner = UBlueprintNodeSpawner::Create(GetClass());

		TWeakObjectPtr<UIKinemaRig> RigPtr = MakeWeakObjectPtr(const_cast<UIKinemaRig*>(Rig));
		NodeSpawner->CustomizeNodeDelegate = UBlueprintNodeSpawner::FCustomizeNodeDelegate::CreateStatic(LoadedAssetSetup, RigPtr.Get());
		ActionRegistrar.AddBlueprintAction(QueryObject, NodeSpawner);
	}
	else if (QueryObject == GetClass())
	{
		FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
		// define a filter to help in pulling UAnimSequence asset data from the registry
		FARFilter Filter;
		Filter.ClassNames.Add(UIKinemaRig::StaticClass()->GetFName());
		Filter.bRecursiveClasses = true;
		// Find matching assets and add an entry for each one
		TArray<FAssetData> RigList;
		AssetRegistryModule.Get().GetAssets(Filter, /*out*/RigList);

		for (auto AssetIt = RigList.CreateConstIterator(); AssetIt; ++AssetIt)
		{
			const FAssetData& Asset = *AssetIt;
			
			UBlueprintNodeSpawner* NodeSpawner = UBlueprintNodeSpawner::Create(GetClass());
			NodeSpawner->CustomizeNodeDelegate = UBlueprintNodeSpawner::FCustomizeNodeDelegate::CreateStatic(LoadedAssetSetup, Cast<UIKinemaRig>(Asset.GetAsset()));
			ActionRegistrar.AddBlueprintAction(Asset, NodeSpawner);
		}
	}

}

bool UAnimGraphNode_IKinemaRetargeting::IsActionFilteredOut(class FBlueprintActionFilter const& Filter)
{
	bool bIsFilteredOut = false;
	FBlueprintActionContext const& FilterContext = Filter.Context;

	for (UBlueprint* Blueprint : FilterContext.Blueprints)
	{
		if (UAnimBlueprint* AnimBlueprint = Cast<UAnimBlueprint>(Blueprint))
		{
			if (!Node.IKinemaRig->Skeleton)
			{
				bIsFilteredOut = false;
				break;
			}
			if (Node.IKinemaRig->Skeleton != AnimBlueprint->TargetSkeleton)
			{
				// Sequence does not use the same skeleton as the Blueprint, cannot use
				bIsFilteredOut = true;
				break;
			}
		}

		else
		{
			// Not an animation Blueprint, cannot use
			bIsFilteredOut = true;
			break;
		}
	}
	return bIsFilteredOut;
}


void UAnimGraphNode_IKinemaRetargeting::CustomizePinData(UEdGraphPin* Pin, FName SourcePropertyName, int32 ArrayIndex) const
{
	if (Node.IKinemaRig == nullptr )
	{
		return;
	}

	if (SourcePropertyName == TEXT("Tasks"))
	{
		const FIKinemaSolverDef& solverDef = Node.IKinemaRig->SolverDef;
		if ((ArrayIndex >= 0) && (ArrayIndex < solverDef.Tasks.Num()))
		{
			Pin->PinFriendlyName = FText::FromString(FString::Printf(TEXT("%s"), *(solverDef.Tasks[ArrayIndex].Name.ToString())));
			//Pin->bHidden = true;
		}
	}
	else if (SourcePropertyName == TEXT("TaskProperties"))
	{
		const FIKinemaSolverDef& solverDef = Node.IKinemaRig->SolverDef;
		if ((ArrayIndex >= 0) && (ArrayIndex < solverDef.Tasks.Num()))
		{
			Pin->PinFriendlyName = FText::FromString(FString::Printf(TEXT("Properties_%s"), *(solverDef.Tasks[ArrayIndex].Name.ToString())));
		}
	}
	else if (SourcePropertyName == TEXT("SegmentProperties"))
	{
		const FIKinemaSolverDef& solverDef = Node.IKinemaRig->SolverDef;
		if ((ArrayIndex >= 0) && (ArrayIndex < solverDef.Bones.Num()))
		{
			Pin->PinFriendlyName = FText::FromString(FString::Printf(TEXT("Properties_%s"), *(solverDef.Bones[ArrayIndex].Name.ToString())));
		}
	}
}

void UAnimGraphNode_IKinemaRetargeting::ValidateAnimNodeDuringCompilation(class USkeleton* ForSkeleton, class FCompilerResultsLog& MessageLog)
{
	if (Node.IKinemaRig == NULL)
	{
		MessageLog.Error(TEXT("@@ references an unknown IKINEMA rig."), this);
		return;
	}
	if (Node.IKinemaRig->SolverDef.Tasks.Num() < 3 && !Node.isProp)
	{
		MessageLog.Error(TEXT("@@ The rig appears to have low number of IK constraints.\nFor a typical solving behaviour, a minimal configuration needs IK Tasks (constraints) on hips and feet (ankles) and world reach needs additional tasks on hands (wrists), chest and head."), this);
	}
}

void UAnimGraphNode_IKinemaRetargeting::PreloadRequiredAssets()
{
	PreloadObject(Node.IKinemaRig);

	Super::PreloadRequiredAssets();
}

// some crazy stuff with pins
///////////////////////////////////////////////////////////////////////
// FA3NodeOptionalPinManager

struct FIKRetargetingNodeOptionalPinManager : public FOptionalPinManager
{
protected:
	class UAnimGraphNode_Base* BaseNode;
	TArray<UEdGraphPin*>* OldPins;

	TMap<FString, UEdGraphPin*> OldPinMap;

public:
	FIKRetargetingNodeOptionalPinManager(class UAnimGraphNode_Base* Node, TArray<UEdGraphPin*>* InOldPins)
		: BaseNode(Node)
		, OldPins(InOldPins)
	{
		if (OldPins != NULL)
		{
			for (auto PinIt = OldPins->CreateIterator(); PinIt; ++PinIt)
			{
				UEdGraphPin* Pin = *PinIt;
				OldPinMap.Add(Pin->PinName.ToString(), Pin);
			}
		}
	}

	virtual void GetRecordDefaults(UProperty* TestProperty, FOptionalPinFromProperty& Record) const override
	{
		const UAnimationGraphSchema* Schema = GetDefault<UAnimationGraphSchema>();

		// Determine if this is a pose or array of poses
		UArrayProperty* ArrayProp = Cast<UArrayProperty>(TestProperty);
		UStructProperty* StructProp = Cast<UStructProperty>((ArrayProp != NULL) ? ArrayProp->Inner : TestProperty);
		const bool bIsPoseInput = (StructProp != NULL) && (StructProp->Struct->IsChildOf(FPoseLinkBase::StaticStruct()));

		//@TODO: Error if they specified two or more of these flags
		const bool bAlwaysShow = TestProperty->HasMetaData(Schema->NAME_AlwaysAsPin) || bIsPoseInput;
		const bool bOptional_ShowByDefault = TestProperty->HasMetaData(Schema->NAME_PinShownByDefault);
		const bool bOptional_HideByDefault = TestProperty->HasMetaData(Schema->NAME_PinHiddenByDefault);
		const bool bNeverShow = TestProperty->HasMetaData(Schema->NAME_NeverAsPin);
		const bool bPropertyIsCustomized = TestProperty->HasMetaData(Schema->NAME_CustomizeProperty);

		Record.bCanToggleVisibility = !bNeverShow || bOptional_ShowByDefault || bOptional_HideByDefault;
		Record.bShowPin = bAlwaysShow || bOptional_ShowByDefault;
		Record.bPropertyIsCustomized = bPropertyIsCustomized;
	}

	virtual void CustomizePinData(UEdGraphPin* Pin, FName SourcePropertyName, int32 ArrayIndex, UProperty* Property = NULL) const override
	{
		if (BaseNode != NULL)
		{
			BaseNode->CustomizePinData(Pin, SourcePropertyName, ArrayIndex);
		}
	}


	virtual void PostInitNewPin(UEdGraphPin* Pin, FOptionalPinFromProperty& Record, int32 ArrayIndex, UProperty* Property, uint8* PropertyAddress, uint8* DefaultPropertyAddress) const override
	{

		check(PropertyAddress != NULL);
		check(Record.bShowPin);

		const UAnimationGraphSchema* Schema = GetDefault<UAnimationGraphSchema>();

		// In all cases set autogenerated default value from node defaults
		if (DefaultPropertyAddress)
		{
			FString LiteralValue;
			FBlueprintEditorUtils::PropertyValueToString_Direct(Property, DefaultPropertyAddress, LiteralValue);
			Schema->SetPinAutogeneratedDefaultValue(Pin, LiteralValue);
		}

		else
		{
			Schema->SetPinAutogeneratedDefaultValueBasedOnType(Pin);
		}

		if (OldPins == nullptr)
		{
			// Initial construction of a visible pin; copy values from the struct
			FString LiteralValue;
			FBlueprintEditorUtils::PropertyValueToString_Direct(Property, PropertyAddress, LiteralValue);
			Schema->SetPinDefaultValueAtConstruction(Pin, LiteralValue);
		}
		else if (Record.bCanToggleVisibility)
		{
			if (UEdGraphPin* OldPin = OldPinMap.FindRef(Pin->PinName.ToString()))
			{
				// Was already visible
			}
			else
			{
				// Showing a pin that was previously hidden, during a reconstruction
				// Convert the struct property into DefaultValue/DefaultValueObject
				FString LiteralValue;
				FBlueprintEditorUtils::PropertyValueToString_Direct(Property, PropertyAddress, LiteralValue);
				Schema->SetPinDefaultValueAtConstruction(Pin, LiteralValue);
			}
		}
	}

	virtual void PostRemovedOldPin(FOptionalPinFromProperty& Record, int32 ArrayIndex, UProperty* Property, uint8* PropertyAddress, uint8* DefaultPropertyAddress) const override
	{
		check(PropertyAddress != NULL);
		check(!Record.bShowPin);

		if (Record.bCanToggleVisibility && (OldPins != NULL))
		{
			const FString OldPinName = (ArrayIndex != INDEX_NONE) ? FString::Printf(TEXT("%s_%d"), *(Record.PropertyName.ToString()), ArrayIndex) : Record.PropertyName.ToString();
			if (UEdGraphPin* OldPin = OldPinMap.FindRef(OldPinName))
			{
				// Convert DefaultValue/DefaultValueObject and push back into the struct
				FBlueprintEditorUtils::PropertyValueFromString_Direct(Property, OldPin->GetDefaultAsString(), PropertyAddress);
			}
		}
	}


	void ShowOrHideLookAtPins(FAnimNode_IKinemaRetargeting& Node)
	{
		const auto Tasks = Node.IKinemaRig->SolverDef.Tasks;
		auto* BaseNodeCopy = BaseNode;
		auto ShowOrHideProperty = [&BaseNodeCopy](const FString& propertyName, bool bHide)
		{
			int optionalPin = BaseNodeCopy->ShowPinForProperties.IndexOfByPredicate([&propertyName](const FOptionalPinFromProperty& property)
			{return property.PropertyName.ToString() == propertyName; });
			if (optionalPin != INDEX_NONE)
			{
				BaseNodeCopy->ShowPinForProperties[optionalPin].bShowPin = bHide;
			}
		};
	}

	void IKCreateVisiblePins(TArray<FOptionalPinFromProperty>& Properties, UStruct* SourceStruct, EEdGraphPinDirection Direction, UK2Node* TargetNode, uint8* StructBasePtr)
	{
		auto& Node = Cast<UAnimGraphNode_IKinemaRetargeting>(BaseNode)->Node;

		if (Node.IKinemaRig == NULL)
		{
			return;
		}

		ShowOrHideLookAtPins(Node);

		const UEdGraphSchema_K2* Schema = GetDefault<UEdGraphSchema_K2>();

		for (auto ExtraPropertyIt = Properties.CreateIterator(); ExtraPropertyIt; ++ExtraPropertyIt)
		{
			FOptionalPinFromProperty& PropertyEntry = *ExtraPropertyIt;

			if (UProperty* OuterProperty = FindFieldChecked<UProperty>(SourceStruct, PropertyEntry.PropertyName))
			{
				UArrayProperty* ArrayProperty = Cast<UArrayProperty>(OuterProperty);
				if ((ArrayProperty != NULL) && (StructBasePtr != NULL))
				{
					UProperty* InnerProperty = ArrayProperty->Inner;

					FEdGraphPinType PinType;
					if (Schema->ConvertPropertyToPinType(InnerProperty, /*out*/ PinType))
					{
						FScriptArrayHelper_InContainer ArrayHelper(ArrayProperty, StructBasePtr);

						UArrayProperty* CheckArrayProp = NULL;
						bool checkTask = false;

						for (int32 Index = 0; Index < ArrayHelper.Num(); ++Index)
						{
							const FString PinName = FString::Printf(TEXT("%s_%d"), *(PropertyEntry.PropertyName.ToString()), Index);

							// Create the pin
							UEdGraphPin* NewPin = NULL;

							bool ArrayPinEnabled = true;
							if (CheckArrayProp != NULL)
							{
								FScriptArrayHelper_InContainer CheckArray(CheckArrayProp, StructBasePtr);
								if (checkTask)
								{
									FIKinemaTaskOverride* taskOverride = (FIKinemaTaskOverride*)CheckArray.GetRawPtr(Index);
									if (taskOverride)
									{
										ArrayPinEnabled = taskOverride->ShowAsPin;
										PropertyEntry.bShowPin = ArrayPinEnabled;
									}
								}
								else
								{
									FIKinemaSegmentOverride* segOverride = (FIKinemaSegmentOverride*)CheckArray.GetRawPtr(Index);
									if (segOverride)
									{
										ArrayPinEnabled = segOverride->ShowAsPin;
										PropertyEntry.bShowPin = ArrayPinEnabled;
									}
								}
							}

							if (PropertyEntry.bShowPin && ArrayPinEnabled)
							{
								//FEdGraphPin::bIsArray is deprecated now - Don't use. 
								NewPin = TargetNode->CreatePin(Direction, PinType.PinCategory, PinType.PinSubCategory, PinType.PinSubCategoryObject.Get(), FName(*PinName));
								// Set the new pin tooltip from from uproperty we are creating pin for
								NewPin->PinToolTip = PropertyEntry.PropertyTooltip.ToString();
								// Allow the derived class to customize the created pin
								CustomizePinData(NewPin, PropertyEntry.PropertyName, Index);
							}

							// Let derived classes take a crack at transferring default values
							uint8* ValuePtr = ArrayHelper.GetRawPtr(Index);
							uint8* DefaultAddPtr = nullptr;

							if (NewPin != NULL)
							{
								PostInitNewPin(NewPin, PropertyEntry, Index, ArrayProperty->Inner, ValuePtr, DefaultAddPtr);
							}
							else if (ArrayPinEnabled)
							{
								PostRemovedOldPin(PropertyEntry, Index, ArrayProperty->Inner, ValuePtr, DefaultAddPtr);
							}
						}
					}
				}
				else
				{
					// Not an array property

					FEdGraphPinType PinType;
					if (Schema->ConvertPropertyToPinType(OuterProperty, /*out*/ PinType))
					{
						FString PinName = PropertyEntry.PropertyName.ToString();

						// Create the pin
						UEdGraphPin* NewPin = NULL;
						if (PropertyEntry.bShowPin)
						{
							NewPin = TargetNode->CreatePin(Direction, PinType.PinCategory, PinType.PinSubCategory, PinType.PinSubCategoryObject.Get(), FName(*PinName));
							// Set the new pin tooltip from from uproperty we are creating pin for
							NewPin->PinToolTip = PropertyEntry.PropertyTooltip.ToString();
							// Allow the derived class to customize the created pin
							CustomizePinData(NewPin, PropertyEntry.PropertyName, INDEX_NONE);
						}

						// Let derived classes take a crack at transferring default values
						if (StructBasePtr != NULL)
						{
							uint8* ValuePtr = OuterProperty->ContainerPtrToValuePtr<uint8>(StructBasePtr);
							uint8* DefaultAddPtr = nullptr;
							if (NewPin != NULL)
							{
								PostInitNewPin(NewPin, PropertyEntry, INDEX_NONE, OuterProperty, ValuePtr, DefaultAddPtr);
							}
							else
							{
								PostRemovedOldPin(PropertyEntry, INDEX_NONE, OuterProperty, ValuePtr, DefaultAddPtr);
							}
						}
					}
				}
			}
		}
	}

	void AllocateDefaultPins(UStruct* SourceStruct, uint8* StructBasePtr)
	{
		FAnimNode_IKinemaRetargeting& Node = Cast<UAnimGraphNode_IKinemaRetargeting>(BaseNode)->Node;
		//[PHX][SB] Begin - crash fix, IkinemaRig is null when doing a fixup redirects
		if (Node.IKinemaRig == nullptr)
		{
			return;
		}
		//[PHX][SB] End 
		RebuildPropertyList(BaseNode->ShowPinForProperties, SourceStruct);

		IKCreateVisiblePins(BaseNode->ShowPinForProperties, SourceStruct, EGPD_Input, BaseNode, StructBasePtr);
	}
};

void UAnimGraphNode_IKinemaRetargeting::IKinemaInternalPinCreation(TArray<UEdGraphPin*>* OldPins)
{
	PreloadRequiredAssets();
	if (UStructProperty* NodeStruct = GetFNodeProperty())
	{
		// Display any currently visible optional pins
		{
			//if (Node.ExposeSolverTasksArray.Num() != Node.Tasks.Num())
				//PopulateExposeTaskArrayFromLimbTransforms();
			FIKRetargetingNodeOptionalPinManager OptionalPinManager(this, OldPins);
			OptionalPinManager.AllocateDefaultPins(NodeStruct->Struct, NodeStruct->ContainerPtrToValuePtr<uint8>(this));
		}
		CreateOutputPins();
	}
}

void UAnimGraphNode_IKinemaRetargeting::AllocateDefaultPins()
{
	IKinemaInternalPinCreation(nullptr);
}

void UAnimGraphNode_IKinemaRetargeting::ReallocatePinsDuringReconstruction(TArray<UEdGraphPin*>& OldPins)
{
	IKinemaInternalPinCreation(&OldPins);
}

