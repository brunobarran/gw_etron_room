/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaStyle.h"
#include "IKinemaEditorPrivatePCH.h"
#include "Slate/SlateGameResources.h"
#include "Interfaces/IPluginManager.h"



#define IMAGE_BRUSH( RelativePath, ... ) FSlateImageBrush( Style->RootToContentDir( RelativePath, TEXT(".png") ), __VA_ARGS__ )
const FVector2D Icon20x20(20.0f, 20.0f);
const FVector2D Icon40x40(40.0f, 40.0f);

TSharedPtr< FSlateStyleSet > FIKinemaStyle::StyleInstance = NULL;
void FIKinemaStyle::Initialize()
{
	if (!StyleInstance.IsValid())
	{
		StyleInstance = Create();
		FSlateStyleRegistry::RegisterSlateStyle(*StyleInstance);
	}
}

void FIKinemaStyle::Shutdown()
{
	FSlateStyleRegistry::UnRegisterSlateStyle(*StyleInstance);
	ensure(StyleInstance.IsUnique());
	StyleInstance.Reset();
}





FName FIKinemaStyle::GetStyleSetName()
{
	static FName StyleSetName(TEXT("IKinemaStyle"));
	return StyleSetName;
}


TSharedRef< class FSlateStyleSet > FIKinemaStyle::Create()
{
	TSharedRef< FSlateStyleSet > Style = MakeShareable(new FSlateStyleSet("IKinemaStyle"));
	Style->SetContentRoot(IPluginManager::Get().FindPlugin("IKinemaLiveAction")->GetBaseDir() / TEXT("Resources"));

	Style->Set("IKinema.ClassIcon", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_copytasks_40x", Icon40x40));
	Style->Set("IKinema.ClassIcon.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_copytasks_40x", Icon20x20));

	return Style;
}
#undef IMAGE_BRUSH
/** reloads textures used by slate renderer */
void FIKinemaStyle::ReloadTextures()
{
	FSlateApplication::Get().GetRenderer()->ReloadTextureResources();
}
/** @return The Slate style set for the Shooter game */
const ISlateStyle& FIKinemaStyle::Get()
{
	return *StyleInstance;
}