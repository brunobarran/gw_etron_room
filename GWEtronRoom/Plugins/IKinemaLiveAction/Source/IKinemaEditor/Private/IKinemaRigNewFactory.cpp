/*=============================================================================
	Implementing factory for an IKinema solver definition
=============================================================================*/

#include "IKinemaRigNewFactory.h"
#include "IKinemaEditorPrivatePCH.h"

#define LOC_NAMESPACE TEXT("IKinemaRigNewFactory")
#define LOCTEXT_NAMESPACE "IKinemaRigNewFactory"



UIKinemaRigNewFactory::UIKinemaRigNewFactory(const class FObjectInitializer& PCIP)
	: Super(PCIP)
{
	// Property initialization
	SupportedClass = UIKinemaRig::StaticClass();
	//Description = LocalizeEnglish(LOC_NAMESPACE,TEXT("IKinemaRigFactoryDescription"),TEXT( "IKinemaRig"));
	bCreateNew = false;
	bEditAfterNew = true;
	bEditorImport = true;
	bText = true;
	ImportPriority = 100; //Very high priority. When importing we will use this first
	Formats.Add(TEXT("ikn;IKinema rig definition"));
}

#undef LOCTEXT_NAMESPACE

//// Factory implementation.
//
//UObject* UIKinemaRigNewFactory::FactoryCreateText(
//	UClass* InClass,
//	UObject* InParent,
//	FName InName,
//	EObjectFlags Flags,
//	UObject* Context,
//	const TCHAR* Type,
//	const TCHAR*& Buffer,
//	const TCHAR* BufferEnd,
//	FFeedbackContext* Warn )
//{
//	UIKinemaRig* newRig = NewObject<UIKinemaRig>(InParent, InName, Flags); 
//	if (newRig == nullptr)
//	{
//		UE_LOG(LogIKinemaEditor, Error, TEXT("IKinemaRigFactory can only create an IKinemaRig"));
//		return nullptr;
//	}
//
//	
//
//	if (!ReadFromText(newRig, Type, Buffer, BufferEnd, Warn) )
//	{
//		return nullptr;
//	}
//	newRig->Skeleton = TargetSkeleton;
//	// The current filename is set by the caller.
//	newRig->ImportPath = IFileManager::Get().ConvertToRelativePath(*(UFactory::GetCurrentFilename()));
//	return newRig;
//}
//
//// Implementing FReimportHandler interface.
//
//bool UIKinemaRigNewFactory::CanReimport(UObject* Obj, TArray<FString>& OutFilenames)
//{	
//	UIKinemaRig* rig = Cast<UIKinemaRig>(Obj);
//	if (rig != nullptr)
//	{
//		// If the import path is empty, UE4 will ask the user to select it.
//		const FString& importPath = rig->ImportPath;
//		if (!importPath.IsEmpty())
//		{
//			OutFilenames.Add(IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*importPath));
//		}
//		return true;
//	}
//	return false;
//}
//
//void UIKinemaRigNewFactory::SetReimportPaths(UObject* Obj, const TArray<FString>& NewReimportPaths)
//{	
//	UIKinemaRig* rig = Cast<UIKinemaRig>(Obj);
//	if ((rig != nullptr) && ensure(NewReimportPaths.Num() == 1))
//	{
//		rig->ImportPath = IFileManager::Get().ConvertToRelativePath(*NewReimportPaths[0]);
//	}
//}
//
//EReimportResult::Type UIKinemaRigNewFactory::Reimport(UObject* Obj)
//{	
//	UIKinemaRig* rig = Cast<UIKinemaRig>(Obj);
//	if (rig == nullptr)
//	{
//		return EReimportResult::Failed;
//	}
//
//	const FString& importPath = rig->ImportPath;
//
//	if(importPath.IsEmpty())
//	{
//		return EReimportResult::Failed;
//	}
//
//	FString filePath = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*importPath);
//
//	FString stringDataFromFile;
//	if( !FFileHelper::LoadFileToString( stringDataFromFile, *filePath) )
//	{
//		return EReimportResult::Failed;
//	}
//
//	const TCHAR* stringDataPtr = *stringDataFromFile;
//
//	int32 PrevUpdateVersion = rig->UpdateVersion;
//	
//	// Reset the rig's solver definition to the default values.
//	rig->PreReimport();
//
//
//
//	// Now read from the json file.
//	if ( !ReadFromText(
//		rig,
//		*GetExtension(filePath),
//		stringDataPtr,
//		stringDataPtr+stringDataFromFile.Len(),
//		NULL ) )
//	{
//		return EReimportResult::Type::Failed;
//	}
//
//	//Bump the version and force update in all FAnimNode_IKinemaSolver nodes
//	rig->UpdateVersion = PrevUpdateVersion+1;
//	
//	// Needs save.
//	rig->MarkPackageDirty();
//
//	return EReimportResult::Succeeded;
//}
//
//// Read from a text buffer.
//bool UIKinemaRigNewFactory::ReadFromText(class UIKinemaRig* rig, const TCHAR* type, const TCHAR*& buffer, const TCHAR* bufferEnd, FFeedbackContext* warn)
//{
//	if (rig == nullptr)
//	{
//		UE_LOG(LogIKinemaEditor, Warning, TEXT("IKinema rig factory import should be given a rig to import onto"));
//		return false;
//	}
//
//	FString fileContents = buffer;
//	bool success = FJsonUtils::ImportObjectFromJsonString(rig, fileContents);
//	if (success)
//	{
//		rig->PostLoadOrImport();
//	}
//	return success;
//}
//
//bool UIKinemaRigNewFactory::ConfigureProperties()
//{
//	TSharedRef<SIKinemaRigCreateDialog> Dialog = SNew(SIKinemaRigCreateDialog);
//	return Dialog->ConfigureProperties(this);
//};