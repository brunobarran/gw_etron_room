/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include "AssetTypeActions_Base.h"

class FAssetTypeActions_IKinemaBase : public FAssetTypeActions_Base
{
public:
	// IAssetTypeActions Implementation
	virtual FColor GetTypeColor() const override { return FColor(200, 100, 100); }
	virtual bool HasActions ( const TArray<UObject*>& InObjects ) const override { return true; }
	virtual void OpenAssetEditor( const TArray<UObject*>& InObjects, TSharedPtr<class IToolkitHost> EditWithinLevelEditor = TSharedPtr<IToolkitHost>() ) override;
	virtual void AssetsActivated( const TArray<UObject*>& InObjects, EAssetTypeActivationMethod::Type ActivationType ) override;
	virtual bool CanFilter() override { return true; }
	virtual uint32 GetCategories() override { return EAssetTypeCategories::Animation; }
	virtual bool ShouldForceWorldCentric() override { return false; }
	virtual void PerformAssetDiff(UObject* OldAsset, UObject* NewAsset, const struct FRevisionInfo& OldRevision, const struct FRevisionInfo& NewRevision) const override {};
	virtual class UThumbnailInfo* GetThumbnailInfo(UObject* Asset) const override { return NULL; }
};

class FAssetTypeActions_IKinemaRig : public FAssetTypeActions_IKinemaBase
{
public:
	// FAssetTypeActions_Base Implementation

	virtual FText GetName() const override {  FText t;return t.FromString((TEXT("AssetTypeActions"), TEXT("AssetTypeActions_IKinemaRig"), TEXT("IKINEMA Rig"))); }
	virtual UClass* GetSupportedClass() const override;
	virtual void GetActions( const TArray<UObject*>& InObjects, FMenuBuilder& MenuBuilder ) override;

	// Handler for when Reimport is clicked.
	virtual void ExecuteReimport(TArray<TWeakObjectPtr<class UIKinemaRig>> assets);
};

