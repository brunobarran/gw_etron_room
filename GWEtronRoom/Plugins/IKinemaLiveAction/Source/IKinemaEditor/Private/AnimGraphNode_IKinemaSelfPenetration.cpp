/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "AnimGraphNode_IKinemaSelfPenetration.h"
#include "IKinemaEditorPrivatePCH.h"
#include "BlueprintActionFilter.h"
#include "BlueprintActionDatabaseRegistrar.h"
#include "Kismet2/CompilerResultsLog.h"
#include "NewAssetAction.h"

UAnimGraphNode_IKinemaSelfPenetration::UAnimGraphNode_IKinemaSelfPenetration(const FObjectInitializer&  PCIP)
	: Super(PCIP)
{
}

FText UAnimGraphNode_IKinemaSelfPenetration::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return FText::FromString(FString::Printf(TEXT("Self penetration using IKINEMA rig %s"), (Node.IKinemaRig != NULL) ? *(Node.IKinemaRig->GetName()) : TEXT("(None)")));
}

FLinearColor UAnimGraphNode_IKinemaSelfPenetration::GetNodeTitleColor() const
{
	return FLinearColor(0.75f, 0.75f, 0.75f);
}

FText UAnimGraphNode_IKinemaSelfPenetration::GetTooltipText() const
{
	return FText::FromString(TEXT("Pose using IKINEMA rig"));
}

FString UAnimGraphNode_IKinemaSelfPenetration::GetNodeCategory() const
{
	return (FString::Printf(TEXT("IKINEMA")));
}

void UAnimGraphNode_IKinemaSelfPenetration::GetMenuEntries(FGraphContextMenuBuilder& ContextMenuBuilder) const
{
	TNewAssetAction<UIKinemaRig, UAnimGraphNode_IKinemaSelfPenetration>::GetMenuEntriesAll(ContextMenuBuilder, FText::FromString("IKinema"));
}

void UAnimGraphNode_IKinemaSelfPenetration::GetMenuActions(FBlueprintActionDatabaseRegistrar& ActionRegistrar) const
{
	auto LoadedAssetSetup = [](UEdGraphNode* NewNode, bool bIsTemplateNode, UIKinemaRig* asset)
	{
		UAnimGraphNode_IKinemaSelfPenetration* SolverNode = CastChecked<UAnimGraphNode_IKinemaSelfPenetration>(NewNode);
		//We set the rig associated with the Node
		SolverNode->Node.SetAsset(asset);
	};
	
	const UObject* QueryObject = ActionRegistrar.GetActionKeyFilter();
	if (QueryObject == nullptr)
	{
		FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
		// define a filter to help in pulling IKinemaRig asset data from the registry
		FARFilter Filter;
		Filter.ClassNames.Add(UIKinemaRig::StaticClass()->GetFName());
		Filter.bRecursiveClasses = true;
		// Find matching assets and add an entry for each one
		TArray<FAssetData> RigList;
		AssetRegistryModule.Get().GetAssets(Filter, /*out*/RigList);

		for (auto AssetIt = RigList.CreateConstIterator(); AssetIt; ++AssetIt)
		{
			const FAssetData& Asset = *AssetIt;

			UBlueprintNodeSpawner* NodeSpawner = UBlueprintNodeSpawner::Create(GetClass());
			check(NodeSpawner != nullptr);
			NodeSpawner->CustomizeNodeDelegate = UBlueprintNodeSpawner::FCustomizeNodeDelegate::CreateStatic(LoadedAssetSetup, Cast<UIKinemaRig>(Asset.GetAsset()));
			ActionRegistrar.AddBlueprintAction(Asset, NodeSpawner);
		}
	}
	else if (const UIKinemaRig* Rig = Cast<UIKinemaRig>(QueryObject))
	{
		UBlueprintNodeSpawner* NodeSpawner = UBlueprintNodeSpawner::Create(GetClass());

		TWeakObjectPtr<UIKinemaRig> RigPtr = MakeWeakObjectPtr(const_cast<UIKinemaRig*>(Rig));
		NodeSpawner->CustomizeNodeDelegate = UBlueprintNodeSpawner::FCustomizeNodeDelegate::CreateStatic(LoadedAssetSetup, RigPtr.Get());
		ActionRegistrar.AddBlueprintAction(QueryObject, NodeSpawner);
	}
	else if (QueryObject == GetClass())
	{
		FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
		// define a filter to help in pulling UAnimSequence asset data from the registry
		FARFilter Filter;
		Filter.ClassNames.Add(UIKinemaRig::StaticClass()->GetFName());
		Filter.bRecursiveClasses = true;
		// Find matching assets and add an entry for each one
		TArray<FAssetData> RigList;
		AssetRegistryModule.Get().GetAssets(Filter, /*out*/RigList);

		for (auto AssetIt = RigList.CreateConstIterator(); AssetIt; ++AssetIt)
		{
			const FAssetData& Asset = *AssetIt;
			
			UBlueprintNodeSpawner* NodeSpawner = UBlueprintNodeSpawner::Create(GetClass());
			NodeSpawner->CustomizeNodeDelegate = UBlueprintNodeSpawner::FCustomizeNodeDelegate::CreateStatic(LoadedAssetSetup, Cast<UIKinemaRig>(Asset.GetAsset()));
			ActionRegistrar.AddBlueprintAction(Asset, NodeSpawner);
		}
	}

}

bool UAnimGraphNode_IKinemaSelfPenetration::IsActionFilteredOut(class FBlueprintActionFilter const& Filter)
{
	bool bIsFilteredOut = false;
	FBlueprintActionContext const& FilterContext = Filter.Context;

	for (UBlueprint* Blueprint : FilterContext.Blueprints)
	{
		if (UAnimBlueprint* AnimBlueprint = Cast<UAnimBlueprint>(Blueprint))
		{
			if (!Node.IKinemaRig->Skeleton)
			{
				bIsFilteredOut = false;
				break;
			}
			if (Node.IKinemaRig->Skeleton != AnimBlueprint->TargetSkeleton)
			{
				// Sequence does not use the same skeleton as the Blueprint, cannot use
				bIsFilteredOut = true;
				break;
			}
		}

		else
		{
			// Not an animation Blueprint, cannot use
			bIsFilteredOut = true;
			break;
		}
	}
	return bIsFilteredOut;
}


void UAnimGraphNode_IKinemaSelfPenetration::CustomizePinData(UEdGraphPin* Pin, FName SourcePropertyName, int32 ArrayIndex) const
{
	if (Node.IKinemaRig == nullptr )
	{
		return;
	}

	if (SourcePropertyName == TEXT("Tasks"))
	{
		const FIKinemaSolverDef& solverDef = Node.IKinemaRig->SolverDef;
		if ((ArrayIndex >= 0) && (ArrayIndex < solverDef.Tasks.Num()))
		{
			Pin->PinFriendlyName = FText::FromString(FString::Printf(TEXT("%s"), *(solverDef.Tasks[ArrayIndex].Name.ToString())));
			//Pin->bHidden = true;
		}
	}
	else if (SourcePropertyName == TEXT("TaskProperties"))
	{
		const FIKinemaSolverDef& solverDef = Node.IKinemaRig->SolverDef;
		if ((ArrayIndex >= 0) && (ArrayIndex < solverDef.Tasks.Num()))
		{
			Pin->PinFriendlyName = FText::FromString(FString::Printf(TEXT("Properties_%s"), *(solverDef.Tasks[ArrayIndex].Name.ToString())));
		}
	}
	else if (SourcePropertyName == TEXT("SegmentProperties"))
	{
		const FIKinemaSolverDef& solverDef = Node.IKinemaRig->SolverDef;
		if ((ArrayIndex >= 0) && (ArrayIndex < solverDef.Bones.Num()))
		{
			Pin->PinFriendlyName = FText::FromString(FString::Printf(TEXT("Properties_%s"), *(solverDef.Bones[ArrayIndex].Name.ToString())));
		}
	}
}

void UAnimGraphNode_IKinemaSelfPenetration::ValidateAnimNodeDuringCompilation(class USkeleton* ForSkeleton, class FCompilerResultsLog& MessageLog)
{
	if (Node.IKinemaRig == NULL)
	{
		MessageLog.Error(TEXT("@@ references an unknown IKinema rig."), this);
		return;
	}
	
	int32 NumOfTask = 0;
	int32 NumOfCharBones = 0;
	for (auto&& Bone : Node.IKinemaRig->SolverDef.Bones)
	{
		if (Bone.SelfColision.bIsEnabled)
		{
			NumOfTask++;
		}
		if(Bone.Characterisation != EIKinemaCharacterDefinition::ENONE)
		{
			NumOfCharBones++;
		}
	}
	//TODO: Decide on Number of Tasks needed
	if (NumOfTask < 3 || NumOfCharBones < 5)
	{
		MessageLog.Error(TEXT("@@ The rig is missing  Self Penetration characterization\nPlease characterise the important bones in the skeleton."), this);
	}
}

void UAnimGraphNode_IKinemaSelfPenetration::PreloadRequiredAssets()
{
	PreloadObject(Node.IKinemaRig);

	Super::PreloadRequiredAssets();
}

