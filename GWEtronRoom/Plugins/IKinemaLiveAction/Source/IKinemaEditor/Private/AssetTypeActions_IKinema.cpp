/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "AssetTYpeActions_IKinema.h"
#include "IKinemaEditorPrivatePCH.h"
#include "Toolkits/SimpleAssetEditor.h"
#include "AssetTypeActions_Base.h"
#include "IKinemaRigToolModule.h"
#include "IKinemaRig.h"

#define LOC_NAMESPACE TEXT("AssetTypeActions")
#define LOCTEXT_NAMESPACE "AssetTypeActions"
// Helper class for reimport.
template <class AssetType>
class TAssetReimportHelper
{
public:

	template <class CallerType>
	static void GetActions(
		CallerType* thisCaller,
		const TArray<TWeakObjectPtr<AssetType>> InAssets,
		FMenuBuilder& MenuBuilder,
		const FString& InLabel,
		const FString& InTooltip)
	{
		// Add the "Reimport" option.

		FText label;
		label.FromString(InLabel);
		TAttribute<FText> AttrLabel(label);

		FText toolTip;
		toolTip.FromString(InTooltip);
		TAttribute<FText> AttrToolTip(toolTip);
		const FSlateIcon icon;


		FUIAction action(FExecuteAction::CreateSP(thisCaller, &CallerType::ExecuteReimport, InAssets), FCanExecuteAction());
		//AddMenuEntry(const TAttribute<FText>& InLabel, const TAttribute<FText>& InToolTip, const FSlateIcon& InIcon, const FUIAction& InAction, FName InExtensionHook, const EUserInterfaceActionType::Type UserInterfaceActionType)
		
		MenuBuilder.AddMenuEntry(AttrLabel, AttrToolTip, icon, action,NAME_None,EUserInterfaceActionType::Button);
			/*AddMenuEntry(
			AttrLabel,
			AttrToolTip,
			NAME_None,
			FUIAction(
				FExecuteAction::CreateSP( thisCaller, &CallerType::ExecuteReimport, InAssets ),
				FCanExecuteAction()
				)
			);*/
	}

	static void ExecuteReimport(TArray< TWeakObjectPtr<AssetType> > assets)
	{
		for (auto assetIt = assets.CreateConstIterator(); assetIt; ++assetIt)
		{
			AssetType* asset = (*assetIt).Get();
			if (asset != nullptr)
			{
				FReimportManager::Instance()->Reimport(asset, /*bAskForNewFileIfMissing=*/true);
			}
		}
	}
};

// Base class implementation.
void FAssetTypeActions_IKinemaBase::OpenAssetEditor(const TArray<UObject*>& InObjects, TSharedPtr<IToolkitHost> EditWithinLevelEditor)
{
	EToolkitMode::Type Mode = EditWithinLevelEditor.IsValid() ? EToolkitMode::WorldCentric : EToolkitMode::Standalone;

	for (auto ObjIt = InObjects.CreateConstIterator(); ObjIt; ++ObjIt)
	{
		auto IKinemaRig = Cast<UIKinemaRig>(*ObjIt);
		if (IKinemaRig != NULL)
		{
			//if (IKinemaRig->Skeleton != NULL)
			{
				IIKinemaRigToolModule* IKinemaRigToolModule = &FModuleManager::LoadModuleChecked<IIKinemaRigToolModule>("IKinemaRigTool");
				IKinemaRigToolModule->CreateIKinemaRigTool(Mode, EditWithinLevelEditor, IKinemaRig);
			}
			/*else
			{
			FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");

			PropertyModule.UnregisterCustomClassLayout(UIKinemaRig::StaticClass()->GetFName());

			FSimpleAssetEditor::CreateEditor(EToolkitMode::Standalone, EditWithinLevelEditor, IKinemaRig);

			}*/
		}
	}
}

void FAssetTypeActions_IKinemaBase::AssetsActivated(const TArray<UObject*>& InObjects, EAssetTypeActivationMethod::Type ActivationType)
{
	if (ActivationType == EAssetTypeActivationMethod::DoubleClicked)
	{
		if (InObjects.Num() == 1)
		{
			FAssetEditorManager::Get().OpenEditorForAsset(InObjects[0]);
		}
		else if (InObjects.Num() > 1)
		{
			FAssetEditorManager::Get().OpenEditorForAssets(InObjects);
		}
	}
}

// Subclasses.

// class FAssetTypeActions_IKinemaRig

UClass* FAssetTypeActions_IKinemaRig::GetSupportedClass() const
{
	return UIKinemaRig::StaticClass();
}

void FAssetTypeActions_IKinemaRig::GetActions( const TArray<UObject*>& InObjects, FMenuBuilder& MenuBuilder )
{
	auto assets = GetTypedWeakObjectPtrs<UIKinemaRig>(InObjects);

	MenuBuilder.AddMenuEntry(
		LOCTEXT("IKinemaRig_Reimport", "Reimport"),
		LOCTEXT("IKinemaRig_ReimportTooltip", "Reimport the selected IKinema Rig."),
		FSlateIcon(),
		FUIAction(
			FExecuteAction::CreateSP(this, &FAssetTypeActions_IKinemaRig::ExecuteReimport, assets),
			FCanExecuteAction()
			)
		);
}

// Handler for when Reimport is clicked.
void FAssetTypeActions_IKinemaRig::ExecuteReimport(TArray<TWeakObjectPtr<class UIKinemaRig>> assets)
{
	for (auto assetIt = assets.CreateConstIterator(); assetIt; ++assetIt)
	{
		auto asset = (*assetIt).Get();
		if (asset != nullptr)
		{
			FReimportManager::Instance()->Reimport(asset, /*bAskForNewFileIfMissing=*/true);
		}
	}
}


#undef LOC_NAMESPACE
#undef LOCTEXT_NAMESPACE