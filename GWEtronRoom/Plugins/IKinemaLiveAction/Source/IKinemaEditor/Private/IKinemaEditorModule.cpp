/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaEditorModule.h"
#include "IKinemaEditorPrivatePCH.h"
#include "Modules/ModuleManager.h"
#include "ContentBrowserExtension/ContentBrowserExtensions.h"
#include "AssetToolsModule.h"

class FIKinemaEditorCommands : public TCommands<FIKinemaEditorCommands>
{
public:
	FIKinemaEditorCommands()
		: TCommands<FIKinemaEditorCommands>(
		TEXT("IKinemaEditor"), // Context name for fast lookup
		NSLOCTEXT("Contexts", "IKinemaEditor", "IKinema Editor"), // Localized context name for displaying
		NAME_None, // Parent
		TEXT("IKinemaRigToolStyle") // Icon Style Set
		)
	{
	}

	// TCommand<> interface
	virtual void RegisterCommands() override
	{
	};
	// End of TCommand<> interface

public:

};

/**
 * IKinemaEditor module implementation
 */

void FIKinemaEditorModule::StartupModule()
{
	IKinemaEditor_MenuExtensibilityManager = MakeShareable(new FExtensibilityManager);
	IKinemaEditor_ToolBarExtensibilityManager = MakeShareable(new FExtensibilityManager);
	FIKinemaEditorCommands::Register();
	IAssetTools& AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();

	mIKinemaRigAssetTypeActions = MakeShareable( new FAssetTypeActions_IKinemaRig );
	AssetTools.RegisterAssetTypeActions( mIKinemaRigAssetTypeActions.ToSharedRef() );
	
	// Integrate Paper2D actions into existing editor context menus
	if (!IsRunningCommandlet())
	{
		FIKinemaContentBrowserExtensions::InstallHooks();
	}
}

void FIKinemaEditorModule::ShutdownModule()
{
	// Only unregister if the asset tools module is loaded.  We don't want to forcibly load it during shutdown phase.
		IKinemaEditor_MenuExtensibilityManager.Reset();
	IKinemaEditor_ToolBarExtensibilityManager.Reset();
	if (UObjectInitialized())
	{
		FIKinemaContentBrowserExtensions::RemoveHooks();
	}
	
	check( mIKinemaRigAssetTypeActions.IsValid() );

	if( FModuleManager::Get().IsModuleLoaded( "AssetTools" ) )
	{
		IAssetTools& AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();

		AssetTools.UnregisterAssetTypeActions( mIKinemaRigAssetTypeActions.ToSharedRef() );
		//AssetTools.UnregisterAssetTypeActions( mIKinemaTaskSkeletonAssetTypeActions.ToSharedRef() );
		//AssetTools.UnregisterAssetTypeActions( mIKinemaTaskAnimAssetTypeActions.ToSharedRef() );
		mIKinemaRigAssetTypeActions.Reset();
	}
	//UnloadSSLLibraries();
	//mIKinemaTaskSkeletonAssetTypeActions.Reset();
	//mIKinemaTaskAnimAssetTypeActions.Reset();
}


DEFINE_LOG_CATEGORY(LogIKinemaEditor);
IMPLEMENT_MODULE( FIKinemaEditorModule, IKinemaEditor );

