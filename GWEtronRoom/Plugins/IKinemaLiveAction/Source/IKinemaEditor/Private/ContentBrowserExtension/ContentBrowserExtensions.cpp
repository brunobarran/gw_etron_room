/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "ContentBrowserExtensions.h"
#include "IKinemaEditorPrivatePCH.h"
#include "ContentBrowserModule.h"
#include "IKinemaRigFactory.h"
#include "IKinemaRig.h"
#include "IKinemaStyle.h"
#include "IContentBrowserSingleton.h"

#define LOCTEXT_NAMESPACE "IKinema"

DECLARE_LOG_CATEGORY_EXTERN(LogIKinemaCBExtensions, Log, All);
DEFINE_LOG_CATEGORY(LogIKinemaCBExtensions);

//////////////////////////////////////////////////////////////////////////

FContentBrowserMenuExtender_SelectedAssets ContentBrowserExtenderDelegate;
FDelegateHandle ContentBrowserExtenderDelegateHandle;

//////////////////////////////////////////////////////////////////////////
// FContentBrowserSelectedAssetExtensionBase

struct FContentBrowserSelectedAssetExtensionBase
{
public:
	TArray<struct FAssetData> SelectedAssets;

public:
	virtual void Execute() {}
	virtual ~FContentBrowserSelectedAssetExtensionBase() {}
};

//////////////////////////////////////////////////////////////////////////
// FCreateSpriteFromTextureExtension

#include "AssetToolsModule.h"
#include "AssetRegistryModule.h"


struct FCreateRigFromSkeletonExtension : public FContentBrowserSelectedAssetExtensionBase
{

	FCreateRigFromSkeletonExtension()
	{
	}

	void CreateRigFromSkeleton(USkeleton* Skeleton)
	{
		FAssetToolsModule& AssetToolsModule = FModuleManager::Get().LoadModuleChecked<FAssetToolsModule>("AssetTools");
		FContentBrowserModule& ContentBrowserModule = FModuleManager::LoadModuleChecked<FContentBrowserModule>("ContentBrowser");

		TArray<UObject*> ObjectsToSync;


		// Create the factory used to generate the rig
		UIKinemaRigFactory* RigFactory = NewObject<UIKinemaRigFactory>();
		RigFactory->TargetSkeleton = Skeleton;

		// Create the Rig
		FString Name;
		FString PackageName;


		// Get a unique name for the rig
		const FString DefaultSuffix = TEXT("_IKINEMARig");
		AssetToolsModule.Get().CreateUniqueAssetName(Skeleton->GetOutermost()->GetName(), DefaultSuffix, /*out*/ PackageName, /*out*/ Name);
		const FString PackagePath = FPackageName::GetLongPackagePath(PackageName);

		if (UObject* NewAsset = AssetToolsModule.Get().CreateAsset(Name, PackagePath, UIKinemaRig::StaticClass(), RigFactory))
		{
			ObjectsToSync.Add(NewAsset);
		}

		if (ObjectsToSync.Num() > 0)
		{
			ContentBrowserModule.Get().SyncBrowserToAssets(ObjectsToSync);
		}
	}

	virtual void Execute() override
	{
		// Create sprites for any selected textures
		for (auto AssetIt = SelectedAssets.CreateConstIterator(); AssetIt; ++AssetIt)
		{
			const FAssetData& AssetData = *AssetIt;
			if (USkeleton* Skeleton = Cast<USkeleton>(AssetData.GetAsset()))
			{
				CreateRigFromSkeleton(Skeleton);
			}
		}


	}
};

//////////////////////////////////////////////////////////////////////////
// FPaperContentBrowserExtensions_Impl

class FIKinemaContentBrowserExtensions_Impl
{
public:
	static void ExecuteSelectedContentFunctor(TSharedPtr<FContentBrowserSelectedAssetExtensionBase> SelectedAssetFunctor)
	{
		SelectedAssetFunctor->Execute();
	}

	static void CreateRigActionsSubMenu(FMenuBuilder& MenuBuilder, TArray<FAssetData> SelectedAssets)
	{
		MenuBuilder.AddSubMenu(
			LOCTEXT("IKinemaRigActionsSubMenuLabel", "IKINEMA Rig Actions"),
			LOCTEXT("IKinemaRigActionsSubMenuToolTip", "IKINEMA Rig -related actions for this skeleton."),
			FNewMenuDelegate::CreateStatic(&FIKinemaContentBrowserExtensions_Impl::PopulateRigActionsMenu, SelectedAssets),
			false,
			FSlateIcon(FIKinemaStyle::GetStyleSetName(), "IKinema.ClassIcon")
			);
	}

	static void PopulateRigActionsMenu(FMenuBuilder& MenuBuilder, TArray<FAssetData> SelectedAssets)
	{
		// Create sprites
		TSharedPtr<FCreateRigFromSkeletonExtension> RigCreatorFunctor = MakeShareable(new FCreateRigFromSkeletonExtension());
		RigCreatorFunctor->SelectedAssets = SelectedAssets;

		FUIAction Action_CreateRigFromSkeleton(
			FExecuteAction::CreateStatic(&FIKinemaContentBrowserExtensions_Impl::ExecuteSelectedContentFunctor, StaticCastSharedPtr<FContentBrowserSelectedAssetExtensionBase>(RigCreatorFunctor)));

		const FName IKinemaStyleSetName = TEXT("IKinemaStyle");

		MenuBuilder.AddMenuEntry(
			LOCTEXT("CB_Extension_Skeleton_CreateRig", "Create IKINEMA Rig"),
			LOCTEXT("CB_Extension_Texture_CreateRig_Tooltip", "Create IKINEMA Rig from selected skeleton"),
			FSlateIcon(IKinemaStyleSetName, "AssetActions.CreateRig"),
			Action_CreateRigFromSkeleton,
			NAME_None,
			EUserInterfaceActionType::Button);
	}

	static TSharedRef<FExtender> OnExtendContentBrowserAssetSelectionMenu(const TArray<FAssetData>& SelectedAssets)
	{
		TSharedRef<FExtender> Extender(new FExtender());

		// Run thru the assets to determine if any meet our criteria
		bool bAnySkeleton = false;
		for (auto AssetIt = SelectedAssets.CreateConstIterator(); AssetIt; ++AssetIt)
		{
			const FAssetData& Asset = *AssetIt;
			bAnySkeleton = bAnySkeleton || (Asset.AssetClass == USkeleton::StaticClass()->GetFName());
		}

		if (bAnySkeleton)
		{
			// Add the sprite actions sub-menu extender
			Extender->AddMenuExtension(
				"GetAssetActions",
				EExtensionHook::After,
				nullptr,
				FMenuExtensionDelegate::CreateStatic(&FIKinemaContentBrowserExtensions_Impl::CreateRigActionsSubMenu, SelectedAssets));
		}

		return Extender;
	}

	static TArray<FContentBrowserMenuExtender_SelectedAssets>& GetExtenderDelegates()
	{
		FContentBrowserModule& ContentBrowserModule = FModuleManager::LoadModuleChecked<FContentBrowserModule>(TEXT("ContentBrowser"));
		return ContentBrowserModule.GetAllAssetViewContextMenuExtenders();
	}
};

//////////////////////////////////////////////////////////////////////////
// FPaperContentBrowserExtensions

void FIKinemaContentBrowserExtensions::InstallHooks()
{
	ContentBrowserExtenderDelegate = FContentBrowserMenuExtender_SelectedAssets::CreateStatic(&FIKinemaContentBrowserExtensions_Impl::OnExtendContentBrowserAssetSelectionMenu);

	TArray<FContentBrowserMenuExtender_SelectedAssets>& CBMenuExtenderDelegates = FIKinemaContentBrowserExtensions_Impl::GetExtenderDelegates();
	CBMenuExtenderDelegates.Add(ContentBrowserExtenderDelegate);
	ContentBrowserExtenderDelegateHandle = CBMenuExtenderDelegates.Last().GetHandle();

	FIKinemaStyle::Initialize();
}

void FIKinemaContentBrowserExtensions::RemoveHooks()
{
	TArray<FContentBrowserMenuExtender_SelectedAssets>& CBMenuExtenderDelegates = FIKinemaContentBrowserExtensions_Impl::GetExtenderDelegates();
	CBMenuExtenderDelegates.RemoveAll([](const FContentBrowserMenuExtender_SelectedAssets& Delegate){ return Delegate.GetHandle() == ContentBrowserExtenderDelegateHandle; });
	FIKinemaStyle::Shutdown();
}

//////////////////////////////////////////////////////////////////////////

#undef LOCTEXT_NAMESPACE