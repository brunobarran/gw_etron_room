// Copyright 2009-2015 IKinema, Ltd. All Rights Reserved.

#pragma once

// Integrate IKinema actions associated with existing engine types (e.g., Skeleton) into the content browser
class FIKinemaContentBrowserExtensions
{
public:
	static void InstallHooks();
	static void RemoveHooks();
};