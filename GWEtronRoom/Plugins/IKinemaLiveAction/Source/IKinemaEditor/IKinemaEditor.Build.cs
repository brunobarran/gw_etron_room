using UnrealBuildTool;

public class IKinemaEditor : ModuleRules
{
    public IKinemaEditor(ReadOnlyTargetRules Target) : base(Target)
    {
        //we are going to be building without PCH's 
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
        //ensure IWYU is enforeced for potentially improved compile times
        bEnforceIWYU = false;
        // bFasterWithoutUnity = true;

        PrivateIncludePaths.AddRange(new string[] {
            "IKinemaEditor/Public",
            "IKinemaEditor/Private",
            "IKinemaCore/Classes"
        });

        PublicDependencyModuleNames.AddRange(
             new string[]
             {
                "Projects",
                "Core",
                "CoreUObject",
                "Engine",
                "UnrealEd",
                "IKinemaCore",
                "BlueprintGraph",
                "AnimGraph" ,
                "SlateCore" ,
                "InputCore",
                "EditorStyle",
                "LADataStreamCore",
                "IKinemaRigTool",
                "JsonUtils",
                "Slate"
             }
             );
        PrivateDependencyModuleNames.AddRange(new string[] { "UnrealEd", "Slate", "MessageLog", "ContentBrowser", "ClassViewer", "JsonUtils", "ContentBrowser", "ClassViewer", "IKinemaRigTool" });
        PrivateIncludePathModuleNames.AddRange(new string[] { "AssetTools", "PropertyEditor", "JsonUtils" });
        DynamicallyLoadedModuleNames.AddRange(new string[] { "AssetTools", "PropertyEditor" });
    }
}