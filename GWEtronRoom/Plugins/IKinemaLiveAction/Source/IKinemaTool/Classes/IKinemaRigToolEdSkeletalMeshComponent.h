/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "IKinemaRig.h"
#include "AnimIKinemaPreviewInstance.h"
#include "IKinemaRigToolEdSkeletalMeshComponent.generated.h"


UCLASS()
class UIKinemaRigToolEdSkeletalMeshComponent : public USkeletalMeshComponent
{
	GENERATED_UCLASS_BODY()

	/** Data and methods shared across multiple classes */
	class FIKinemaRigToolSharedData* SharedData;

	// Draw colors

	FColor BoneUnselectedColor;
	FColor SourceBoneUnselectedColor;
	FColor BoneSelectedColor;
	FColor SourceBoneSelectedColor;
	FColor HierarchyDrawColor;
	FColor AnimSkelDrawColor;
	FColor LinkedTargetBoneColor;
	FColor LinkedSourceBoneColor;
	
	UPROPERTY(transient)
	class UAnimIKinemaPreviewInstance* PreviewInstance;


	/** Mesh-space matrices showing state of just animation (ie before IKinema) - useful for debugging! */
	TArray<FTransform> AnimationSpaceBases;


	/** UPrimitiveComponent interface */
	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;
	
	/** Renders non-hitproxy elements for the viewport, this function is called in the Game Thread */
	virtual void Render(const FSceneView* View, class FPrimitiveDrawInterface* PDI);
	
	/** Renders hitproxy elements for the viewport, this function is called in the Game Thread */
	virtual void RenderHitTest(const FSceneView* View,class FPrimitiveDrawInterface* PDI);

	/** Handles most of the rendering logic for this component */
	void RenderAssetTools(const FSceneView* View, class FPrimitiveDrawInterface* PDI, bool bHitTest);

	/** Draw the mesh skeleton using lines. bAnimSkel means draw the animation skeleton */
	void DrawHierarchy(FPrimitiveDrawInterface* PDI, bool bAnimSkel);

	/** Draws a constraint */
	void DrawConstraint(int32 ConstraintIndex, const FSceneView* View, FPrimitiveDrawInterface* PDI);

	/** Draws the CoM constraint */
	void DrawCoMConstraint(const FSceneView* View, FPrimitiveDrawInterface* PDI);

	/** Returns the physics asset for this PhATEd component - note: This hides the implementation in the USkinnedMeshComponent base class */
	class UIKinemaRig* GetIKinemaRig() const;

	void DrawLimits(FPrimitiveDrawInterface* PDI, const FIKinemaTaskDef& task, const FTransform& transform);
	void DrawLimits(FPrimitiveDrawInterface* PDI, const FIKinemaBoneDef& Bone, const FTransform& transform);

	virtual void InitAnim(bool bForceReinit);
private:
	void DrawSourceActor(const FSceneView* View, class FPrimitiveDrawInterface* PDI, bool bHitTest) const;
	void DrawCollisionPrimitive(class FPrimitiveDrawInterface* PDI, const FIKinemaShape& CollisionShape, const FTransform& BoneTransform, FLinearColor LineColor, int32 BoneIndex ) const;
	template<class HitProxy, class BoneType, typename EndVector>
	void DrawChildrenCones(class FPrimitiveDrawInterface* PDI, const FTransform& BoneTransform, const TArray<BoneType>& RigBones, FLinearColor LineColor, int32 BoneIndex, EndVector eVec) const;
	
};
