/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include "Animation/AnimSingleNodeInstanceProxy.h"
#include "Animation/AnimSingleNodeInstance.h"
#include "BoneControllers/AnimNode_ModifyBone.h"

#include "AnimIKinemaPreviewInstance.generated.h"


/** Proxy override for this UAnimInstance-derived class */
USTRUCT()
struct FAnimIKinemaPreviewInstanceProxy : public FAnimSingleNodeInstanceProxy
{
	GENERATED_BODY()

public:
	FAnimIKinemaPreviewInstanceProxy()
		:  bEnableControllers(true)
	{
		bCanProcessAdditiveAnimations = true;
	}

	FAnimIKinemaPreviewInstanceProxy(UAnimInstance* InAnimInstance)
		: FAnimSingleNodeInstanceProxy(InAnimInstance), bEnableControllers(true)
	{
		bCanProcessAdditiveAnimations = true;
	}

	virtual void Initialize(UAnimInstance* InAnimInstance) override;
	virtual void Update(float DeltaSeconds) override;
	virtual bool Evaluate(FPoseContext& Output) override;

	/**
	* Return the task instance to edit, based on the TaskIndex passed in
	* @param   TaskIndex   The index of the task to edit
	* @return  the task instance
	*/
	//FIKinemaSolverTask* GetTask(const int& TaskIndex);

	/**
	* Finds an already modified bone
	* @param	InBoneName	The name of the bone modification to find
	* @return the bone modification or NULL if no current modification was found
	*/
	FAnimNode_ModifyBone* FindModifiedBone(const FName& InBoneName);

	/**
	* Modifies a single bone. Create a new FAnimNode_ModifyBone if one does not exist for the passed-in bone.
	* @param	InBoneName	The name of the bone to modify
	* @return the new or existing bone modification
	*/
	FAnimNode_ModifyBone& ModifyBone(const FName& InBoneName);

	/**
	* Removes an existing bone modification
	* @param	InBoneName	The name of the existing modification to remove
	*/
	void RemoveBoneModification(const FName& InBoneName);

	/**
	* Reset all bone modified
	*/
	void ResetModifiedBone();

	TArray<FAnimNode_ModifyBone>& GetBoneControllers()
	{
		return BoneControllers;
	}
private:
	/**
	* Apply Bone Controllers to the Outpose
	*
	* @param	Component	Component to apply bone controller to
	* @param	BoneControllers	 List of Bone Controllers to apply
	* @param 	OutMeshPose	Outpose in Mesh Space once applied
	*/
	void ApplyBoneControllers(TArray<FAnimNode_ModifyBone> &InBoneControllers, FComponentSpacePoseContext& ComponentSpacePoseContext);

private:

	/** Controllers for individual bones */
	TArray<FAnimNode_ModifyBone> BoneControllers;

	/**
	* Used to determine if controller has to be applied or not
	* Used to disable controller during editing
	*/
	bool bEnableControllers;

};

/**
* This Instance only contains one AnimationAsset, and produce poses
* Used by Preview in AnimGraph, Playing single animation in Kismet2 and etc
*/

UCLASS(transient, NotBlueprintable, noteditinlinenew)
class  UAnimIKinemaPreviewInstance : public UAnimSingleNodeInstance
{
	GENERATED_UCLASS_BODY()

		// Disable compiler-generated deprecation warnings by implementing our own destructor
	PRAGMA_DISABLE_DEPRECATION_WARNINGS
	~UAnimIKinemaPreviewInstance() {}
	PRAGMA_ENABLE_DEPRECATION_WARNINGS

	//~ Begin UObject Interface
	virtual void Serialize(FArchive& Ar) override;
	//~ End UObject Interface

	// Begin UAnimInstance interface
	virtual void NativeInitializeAnimation() override;
	virtual FAnimInstanceProxy* CreateAnimInstanceProxy() override;
	// End UAnimInstance interface

	UAnimSequence* GetAnimSequence();

	// Begin UAnimSingleNodeInstance interface
	virtual void SetAnimationAsset(UAnimationAsset* NewAsset, bool bIsLooping = true, float InPlayRate = 1.f) override;
	// End UAnimSingleNodeInstance interface



	/**
	* Modifies a single bone. Create a new FAnimNode_ModifyBone if one does not exist for the passed-in bone.
	* @param	InBoneName	The name of the bone to modify
	* @return the new or existing bone modification
	*/
	FAnimNode_ModifyBone& ModifyBone(const FName& InBoneName);

	/**
	* Removes an existing bone modification
	* @param	InBoneName	The name of the existing modification to remove
	*/
	void RemoveBoneModification(const FName& InBoneName);
	/**
	* Reset all bone modified
	*/
	void ResetModifiedBone();
	

	/**
	* Enable Controllers
	* This is used by when editing, when controller has to be disabled
	*/
	void EnableControllers(bool bEnable) { bEnableControllers = bEnable; };

private:

	

	/**
	* Used to determine if controller has to be applied or not
	* Used to disable controller during editing
	*/
	bool bEnableSolver = false;

	/**
	* Used to determine if controller has to be applied or not
	* Used to disable controller during editing
	*/
	bool bEnableControllers = false;
	/**
	* Map the rig task index to the look at task index
	*/
	TArray<int16> LookAtTasksMap;

};



