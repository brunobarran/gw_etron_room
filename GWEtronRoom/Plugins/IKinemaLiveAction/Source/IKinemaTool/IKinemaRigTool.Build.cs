// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
using UnrealBuildTool;

public class IKinemaRigTool : ModuleRules
{
    public IKinemaRigTool(ReadOnlyTargetRules Target) : base(Target)
    {

        //we are going to be building without PCH's 
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
        bEnforceIWYU = false;

        PublicIncludePaths.Add("Editor/UnrealEd/Public");
        PublicDependencyModuleNames.Add("LiveLinkInterface");
        PublicDependencyModuleNames.Add("LiveLink");
        PrivateDependencyModuleNames.AddRange(
            new string[] {
                "Projects",
                "Core",
                "CoreUObject",
                "Engine",
                "InputCore",
                "RenderCore",
                "Slate",
                "SlateCore",
                "EditorStyle",
                "PropertyEditor",
                "LevelEditor",
                "UnrealEd",
                "Kismet",
                "Persona",
                "ClassViewer",
                "AnimGraph",
                "AnimGraphRuntime",
                "IKinemaCore",
                "LADataStreamCore",
                "IKinemaEditor",
                "ContentBrowser",
                "AssetTools",
                "JsonUtilities",
                "Json"
            }
        );

        DynamicallyLoadedModuleNames.AddRange(
            new string[] {
                "MainFrame",
                "MeshUtilities",
            }
        );
    }
}
