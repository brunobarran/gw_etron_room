/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaRigToolModule.h"
#include "IKinemaRigToolSharedData.h"
#include "IKinemaRigToolHitProxies.h"
#include "IKinemaRigToolEdSkeletalMeshComponent.h"


UIKinemaRigToolEdSkeletalMeshComponent::UIKinemaRigToolEdSkeletalMeshComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, BoneUnselectedColor(170, 155, 225)
	, SourceBoneUnselectedColor(233, 103, 245)
	, BoneSelectedColor(185, 70, 0)
	, SourceBoneSelectedColor(39, 245, 56)
	, HierarchyDrawColor(220, 255, 220)
	, AnimSkelDrawColor(255, 64, 64)
	, LinkedTargetBoneColor(207, 14, 53)
	, LinkedSourceBoneColor(222, 55, 0)

{
	static FName CollisionProfileName(TEXT("IKinemaRigActor"));
	UPrimitiveComponent::SetCollisionProfileName(CollisionProfileName);
}

UIKinemaRig* UIKinemaRigToolEdSkeletalMeshComponent::GetIKinemaRig() const
{
	return SharedData->IKinemaRig;
}

void UIKinemaRigToolEdSkeletalMeshComponent::InitAnim(bool bForceReinit)
{
	// If we already have PreviewInstnace and its asset's Skeleton does not match with mesh's Skeleton
	// then we need to clear it up to avoid an issue
	if (PreviewInstance && PreviewInstance->CurrentAsset && SkeletalMesh)
	{
		if (PreviewInstance->CurrentAsset->GetSkeleton() != SkeletalMesh->Skeleton)
		{
			// if it doesn't match, just clear it
			PreviewInstance->SetAnimationAsset(NULL);
		}
	}

	Super::InitAnim(bForceReinit);

	// if PreviewInstance is NULL, create here once
	if (PreviewInstance == NULL)
	{
		PreviewInstance = NewObject<UAnimIKinemaPreviewInstance>(this);
		check(PreviewInstance);

		//Set transactional flag in order to restore slider position when undo operation is performed
		PreviewInstance->SetFlags(RF_Transactional);
	}

	// if anim script instance is null because it's not playing a blueprint, set to PreviewInstnace by default
	// that way if user would like to modify bones or do extra stuff, it will work
	if ((AnimationMode != EAnimationMode::AnimationBlueprint))
	{
		AnimScriptInstance = PreviewInstance;
		AnimScriptInstance->InitializeAnimation();
	}
}

void DrawHfCircle(FPrimitiveDrawInterface* PDI, const FVector& Base, const FVector& X, const FVector& Y, const FLinearColor& Color, float Radius, int32 NumSides, float Thickness, float DepthBias, bool bScreenSpace)
{
	const float	AngleDelta = static_cast<float>(PI) / static_cast<float>(NumSides);
	FVector	LastVertex = Base + X * Radius;

	for (int32 SideIndex = 0; SideIndex < NumSides; SideIndex++)
	{
		const FVector	Vertex = Base + (X * FMath::Cos(AngleDelta * (SideIndex + 1)) + Y * FMath::Sin(AngleDelta * (SideIndex + 1))) * Radius;
		PDI->DrawLine(LastVertex, Vertex, Color, SDPG_World, Thickness, DepthBias, bScreenSpace);
		LastVertex = Vertex;
	}
}

void DrawCapsule(class FPrimitiveDrawInterface* PDI, const FVector& Base, const FVector& X, const FVector& Y, const FVector& Z, const FLinearColor& Color, float Radius, float HalfHeight, int32 NumSides, uint8 DepthPriority, float Thickness = 0.0f, float DepthBias = 0.0f, bool bScreenSpace = false)
{
	const FVector Origin = Base;
	const FVector XAxis = X.GetSafeNormal();
	const FVector YAxis = Y.GetSafeNormal();
	const FVector ZAxis = Z.GetSafeNormal();

	float HalfAxis = FMath::Max<float>(HalfHeight - Radius, 1.f);
	FVector TopEnd = Origin + HalfAxis * ZAxis;
	FVector BottomEnd = Origin - HalfAxis * ZAxis;

	// Draw top and bottom circles

	DrawCircle(PDI, TopEnd, XAxis, YAxis, Color, Radius, NumSides, DepthPriority, Thickness, DepthBias, bScreenSpace);
	DrawCircle(PDI, BottomEnd, XAxis, YAxis, Color, Radius, NumSides, DepthPriority, Thickness, DepthBias, bScreenSpace);

	// Draw domed caps
	DrawHfCircle(PDI, TopEnd, YAxis, ZAxis, Color, Radius, NumSides / 2, Thickness, DepthBias, bScreenSpace);
	DrawHfCircle(PDI, TopEnd, XAxis, ZAxis, Color, Radius, NumSides / 2, Thickness, DepthBias, bScreenSpace);

	const FVector NegZAxis = -ZAxis;

	DrawHfCircle(PDI, BottomEnd, YAxis, NegZAxis, Color, Radius, NumSides / 2, Thickness, DepthBias, bScreenSpace);
	DrawHfCircle(PDI, BottomEnd, XAxis, NegZAxis, Color, Radius, NumSides / 2, Thickness, DepthBias, bScreenSpace);

	// we set NumSides to 4 as it makes a nicer looking capsule as we only draw 2 HalfCircles above
	const int32 NumCylinderLines = 4;

	// Draw lines for the cylinder portion 
	const float	AngleDelta = 2.0f * PI / NumCylinderLines;
	FVector	LastVertex = Base + XAxis * Radius;

	for (int32 SideIndex = 0; SideIndex < NumCylinderLines; SideIndex++)
	{
		const FVector Vertex = Base + (XAxis * FMath::Cos(AngleDelta * (SideIndex + 1)) + YAxis * FMath::Sin(AngleDelta * (SideIndex + 1))) * Radius;

		PDI->DrawLine(LastVertex - ZAxis * HalfAxis, LastVertex + ZAxis * HalfAxis, Color, DepthPriority, Thickness, DepthBias, bScreenSpace);

		LastVertex = Vertex;
	}
}


void UIKinemaRigToolEdSkeletalMeshComponent::RenderAssetTools(const FSceneView* View, class FPrimitiveDrawInterface* PDI, bool bHitTest)
{
	check(SharedData);

#if DEBUG_CLICK_VIEWPORT
	PDI->DrawLine(SharedData->LastClickOrigin, SharedData->LastClickOrigin + SharedData->LastClickDirection * 5000.0f, FLinearColor(1, 1, 0, 1), SDPG_Foreground);
	PDI->DrawPoint(SharedData->LastClickOrigin, FLinearColor(1, 0, 0), 5, SDPG_Foreground);
#endif
	const auto EditorSkelComp = SharedData->EditorSkelComp;
	const auto RigBones = SharedData->IKinemaRig->SolverDef.Bones;
	// Draw bodies
	if (SharedData->IKinemaRig->Skeleton && (SharedData->bShowAnimSkel || SharedData->bShowCollisionShapes))
	{
		for (int32 i = 0; i < RigBones.Num(); ++i)
		{
			const auto& Bone = RigBones[i];
			if (!SharedData->bShowNonActive && !Bone.Active)
			{
				continue;
			}

			const int32 BoneIndex = GetBoneIndex(Bone.Name);
			if (BoneIndex != INDEX_NONE)
			{
				FLinearColor LineColor = BoneUnselectedColor;
				if (Bone.SourceName != NAME_None)
				{
					LineColor = LinkedTargetBoneColor;
				}
				if (!Bone.Active)
				{
					LineColor = FLinearColor::Red;
				}
				if (SharedData->IsBodySelected(i))
				{
					LineColor = BoneSelectedColor;
				}

				const FTransform WorldTransforms = EditorSkelComp->GetComponentSpaceTransforms()[BoneIndex] * EditorSkelComp->GetComponentTransform();
				
				if (SharedData->bShowAnimSkel)
				{

					DrawChildrenCones<HIKinemaRigToolEdBoneProxy>(PDI, WorldTransforms, RigBones, LineColor, i, [&](int32 BoneIndex, FVector start) -> FVector
					{
						const int32 childBoneIndex = GetBoneIndex(RigBones[BoneIndex].Name);
						if (childBoneIndex == INDEX_NONE)
							return start;
						return (EditorSkelComp->GetComponentSpaceTransforms()[childBoneIndex] * EditorSkelComp->GetComponentTransform()).GetLocation();
					});
				}


				//Draw collision shapes only if we are in collision shape editing mode
				if (SharedData->bShowCollisionShapes)
				{
					DrawCollisionPrimitive(PDI,Bone.SelfColision.CollisionShape,WorldTransforms,LineColor,i);
				}


				if (SharedData->IsBodySelected(i))
				{
					LineColor = BoneSelectedColor;
					if (Bone.EnableLimits)
					{
						DrawLimits(PDI, Bone, WorldTransforms);
					}
				}
			}
		}
	}
	// Draw Constraints
	if (SharedData->bShowConstraints)
	{
		for (int32 i = 0; i < SharedData->IKinemaRig->SolverDef.Tasks.Num(); ++i)
		{
			DrawConstraint(i, View, PDI);	
		}
		//Render the CoM task
		if (SharedData->IKinemaRig->SolverDef.ActiveCOM)
		{
			DrawCoMConstraint(View, PDI);
		}
	}

	//Draw Mocap Actor
	if (SharedData->IKinemaRig->Actor)
	{
		DrawSourceActor(View, PDI, bHitTest);
	}

}

template<class HitProxy, class BoneType, typename EndVectorCalc>
void UIKinemaRigToolEdSkeletalMeshComponent::DrawChildrenCones(class FPrimitiveDrawInterface* PDI, const FTransform& BoneTransform, const TArray<BoneType>& RigBones, FLinearColor LineColor, int32 BoneIndex, EndVectorCalc eVec) const
{
	const FVector Start = BoneTransform.GetLocation();
	static const float SphereRadius = 0.05f;
	//Draw cones from this bone to all its children
	PDI->SetHitProxy(new HitProxy(BoneIndex));
	bool HasChildren = false;
	for (auto r = BoneIndex; r < RigBones.Num(); r++)
	{
		if (RigBones[r].ParentName == RigBones[BoneIndex].Name)
		{
			HasChildren = true;
			const FVector End = eVec(r,Start); 
			
			//Calc cone size 
			const FVector EndToStart = (Start - End);
			const float ConeLength = EndToStart.Size();
			const float Angle = FMath::RadiansToDegrees(FMath::Atan((SphereRadius * SharedData->AverageBoneLength) / ConeLength));

			//Render Sphere for bone end point and a cone between it and its parent.
			DrawWireSphere(PDI, Start, LineColor, SphereRadius * SharedData->AverageBoneLength, 10, SDPG_Foreground);
			DrawCoordinateSystem(PDI, Start, BoneTransform.GetRotation().Rotator(), 5.f*SphereRadius * SharedData->AverageBoneLength, SDPG_Foreground);
			TArray<FVector> Verts;
			DrawWireCone(PDI, Verts, FRotationMatrix::MakeFromX(EndToStart)*FTranslationMatrix(End), ConeLength, Angle, 4, LineColor, SDPG_Foreground);
		}
	}
	if(!HasChildren)
	{
		DrawWireSphere(PDI, Start, LineColor, SphereRadius * SharedData->AverageBoneLength, 10, SDPG_Foreground);
	}
	PDI->SetHitProxy(NULL);
}

void UIKinemaRigToolEdSkeletalMeshComponent::DrawCollisionPrimitive(class FPrimitiveDrawInterface* PDI, const FIKinemaShape& CollisionShape, const FTransform& BoneTransform, FLinearColor LineColor, int32 BoneIndex) const
{
	PDI->SetHitProxy(new HIKinemaRigToolEdBoneProxy(BoneIndex));

	if (SharedData->IsBodySelected(BoneIndex))
	{
		LineColor = FColor::Green;
	}

	const FQuat q = BoneTransform.GetRotation()*CollisionShape.LocalOffset.GetRotation();
	const FVector Start = BoneTransform.GetTranslation() + BoneTransform.GetRotation()*CollisionShape.LocalOffset.GetTranslation();


	const FTransform WorldTransforms(q, Start);

	//TODO: Move to seperate function draw IKinema Collision object
	const auto Box = FBox(Start - WorldTransforms.GetRotation().RotateVector(CollisionShape.BoxHalfLengths), Start + WorldTransforms.GetRotation().RotateVector(CollisionShape.BoxHalfLengths));
	switch (CollisionShape.Shape)
	{
	case EIKSPHERE:

		DrawWireSphere(PDI, Start, LineColor, CollisionShape.Radius, 10, SDPG_Foreground);
		break;
	case EIKBOX:

		DrawWireBox(PDI, Box, LineColor, SDPG_Foreground);
		break;
	case EIKCAPSULE:

		DrawCapsule(PDI, Start, WorldTransforms.GetUnitAxis(EAxis::Z), WorldTransforms.GetUnitAxis(EAxis::Y), WorldTransforms.GetUnitAxis(EAxis::X), LineColor, CollisionShape.Radius, CollisionShape.HalfLength, 10, SDPG_Foreground/*How to draw this*/);
		break;
	default:
		break;
	}

	PDI->SetHitProxy(NULL);
}


void UIKinemaRigToolEdSkeletalMeshComponent::DrawSourceActor(const FSceneView* View, class FPrimitiveDrawInterface* PDI, bool bHitTest) const
{
	for (int32 i = 0; i < SharedData->IKinemaRig->Actor->NumOfBones(); ++i)
	{
		auto ParentIndex = SharedData->IKinemaRig->Actor->GetParentBoneIndex(i);
		auto Bone = SharedData->IKinemaRig->Actor->GetBone(i);
		FVector Start, End;
		FLinearColor LineColor = SourceBoneUnselectedColor;
		if (Bone.TargetBone != NAME_None)
		{
			LineColor = LinkedSourceBoneColor;
		}
		if (SharedData->IsSourceBodySelected(i))
		{
			LineColor = SourceBoneSelectedColor;
		}
		FTransform WorldTransforms = Bone.GlobalTransform * SharedData->IKinemaRig->Actor->SourceTransform;
		WorldTransforms.SetLocation(WorldTransforms.GetLocation()*SharedData->IKinemaRig->Actor->ImportScale);

		if (!SharedData->IKinemaRig->Actor->IsRigidBody)
		{
			const auto actor = SharedData->IKinemaRig->Actor;
			DrawChildrenCones<HIKinemaRigToolEdSourceBoneProxy>(PDI, WorldTransforms, actor->GetBones(), LineColor, i, [&](int BoneIndex, FVector Start) -> FVector
			{
				return (actor->GetBone(BoneIndex).GlobalTransform * actor->SourceTransform).GetLocation() * actor->ImportScale;
			});
		}
		else
		{
			PDI->SetHitProxy(new HIKinemaRigToolEdSourceBoneProxy(i));
			DrawWireDiamond(PDI, FTranslationMatrix(WorldTransforms.GetTranslation()), 10.f*0.05f * SharedData->AverageBoneLength, LineColor, SDPG_Foreground);
			DrawCoordinateSystem(PDI, WorldTransforms.GetTranslation(), (WorldTransforms).GetRotation().Rotator(), 5.f*0.05f * SharedData->AverageBoneLength, SDPG_Foreground);
			PDI->SetHitProxy(NULL);
		}
	}
}

void UIKinemaRigToolEdSkeletalMeshComponent::Render(const FSceneView* View, FPrimitiveDrawInterface* PDI)
{
	RenderAssetTools(View, PDI, 0);
}

void UIKinemaRigToolEdSkeletalMeshComponent::RenderHitTest(const FSceneView* View, class FPrimitiveDrawInterface* PDI)
{
	RenderAssetTools(View, PDI, 1);
}

FPrimitiveSceneProxy* UIKinemaRigToolEdSkeletalMeshComponent::CreateSceneProxy()
{
	FPrimitiveSceneProxy* Proxy = NULL;
	FIKinemaRigToolSharedData::EIKinemaRigToolRenderMode MeshViewMode = SharedData->GetCurrentMeshViewMode();
	if (MeshViewMode != FIKinemaRigToolSharedData::PRM_None)
	{
		Proxy = USkeletalMeshComponent::CreateSceneProxy();
	}

	return Proxy;
}

void UIKinemaRigToolEdSkeletalMeshComponent::DrawConstraint(int32 ConstraintIndex, const FSceneView* View, FPrimitiveDrawInterface* PDI)
{
	PDI->SetHitProxy(new HIKinemaRigToolEdConstraintProxy(ConstraintIndex));
	auto& task = SharedData->IKinemaRig->SolverDef.Tasks[ConstraintIndex];

	FColor DrawColor = FColor::Yellow;
	FString Name = SharedData->IKinemaRig->SolverDef.Bones[task.BoneIndex].Name.ToString();
	Name.Split(":", nullptr, &Name);
	Name = Name.Replace(TEXT("FBXASC032"), TEXT("-"));
	auto SelectedBoneName = FName(*Name);

	int32 BoneIndex = GetBoneIndex(SelectedBoneName);

	if (BoneIndex == INDEX_NONE)
	{
		return;
	}
	FTransform WorldTransforms = SharedData->EditorSkelComp->GetComponentSpaceTransforms()[BoneIndex] * SharedData->EditorSkelComp->GetComponentTransform();
	static const float SphereRadius = .5f;

	if (task.HasPositionTask && !task.HasRotationTask)
	{
		DrawColor = FColor::Red;
	}
	else if (!task.HasPositionTask && task.HasRotationTask)
	{
		DrawColor = FColor::Blue;
	}
	else if (task.PositionAsPoleObject)
	{
		DrawColor = FColor::Cyan;
	}
	else if (!task.HasPositionTask && !task.HasRotationTask)
	{
		DrawColor = FColor::White;
	}
	if (SharedData->IsConstraintSelected(ConstraintIndex))
	{
		DrawColor = FColor::Magenta;
	}

	DrawWireSphere(PDI, WorldTransforms.GetLocation(), DrawColor, SphereRadius * SharedData->AverageBoneLength, 10, SDPG_Foreground);
	PDI->SetHitProxy(NULL);
}

void UIKinemaRigToolEdSkeletalMeshComponent::DrawCoMConstraint(const FSceneView* View, FPrimitiveDrawInterface* PDI)
{
	//Calculate CoM location.
	FVector CoMPosition(0.f, 0.f, 0.f);
	for (const auto& segment : SharedData->IKinemaRig->SolverDef.Bones)
	{
		int32 BoneIndex = GetBoneIndex(segment.Name);
		if (BoneIndex == INDEX_NONE)
		{
			continue;
		}
		FTransform WorldTransforms = SharedData->EditorSkelComp->GetComponentSpaceTransforms()[BoneIndex] * SharedData->EditorSkelComp->GetComponentTransform();
		CoMPosition += WorldTransforms.GetTranslation()*segment.Mass;
	}

	CoMPosition /= SharedData->IKinemaRig->SolverDef.FigureMass;
	static const float SphereRadius = .5f;
	FColor DrawColor(FColor::Red);
	DrawWireSphere(PDI, CoMPosition, DrawColor, SphereRadius * SharedData->AverageBoneLength, 10, SDPG_Foreground);
	UMaterial* AxisMaterialBase = LoadObject<UMaterial>(NULL, TEXT("/IKinema/CoMMaterial.CoMMaterial"), NULL, LOAD_None, NULL);
	if (AxisMaterialBase)
	{
		UMaterialInstanceDynamic* AxisMaterialX = UMaterialInstanceDynamic::Create(AxisMaterialBase, NULL);
		PDI->SetHitProxy(new HIKinemaRigToolEdCoMConstraintProxy());
		DrawSphere(PDI, CoMPosition, FRotator(FRotator::ZeroRotator), FVector(SphereRadius * SharedData->AverageBoneLength), 25, 50, AxisMaterialX->GetRenderProxy(false), SDPG_Foreground);
		PDI->SetHitProxy(NULL);
	}
}

//Return unit vector with the largest element in InVector set to +/- 1 depending on sign and all other set to 0
FVector GetMaxVector(const FVector& InVector)
{
	FVector ret(InVector.GetAbs());

	if (ret.X > ret.Y && ret.X > ret.Z)
	{
		return FVector(FMath::Sign(InVector.X) * 1, 0, 0);
	}
	if (ret.Y> ret.Z && ret.Y> ret.X)
	{
		return FVector(0, FMath::Sign(InVector.Y) * 1, 0);
	}

	return FVector(0, 0, FMath::Sign(InVector.Z) * 1);
}

////creates fan shape along visualized axis for rotation axis of length Length
FMatrix HelpBuildFan(const FTransform& Con1Frame, const FTransform& Con2Frame, const FVector& DrawOnAxis, const FVector& RotationAxis, float Length)
{

	FVector Con1DrawOnAxis = Con1Frame.TransformVector(GetMaxVector(DrawOnAxis));
	FVector Con2DrawOnAxis = Con2Frame.TransformVector(GetMaxVector(DrawOnAxis));

	FVector Con1RotationAxis = Con1Frame.TransformVector(GetMaxVector(RotationAxis));
	FVector Con2RotationAxis = Con2Frame.TransformVector(GetMaxVector(RotationAxis));

	// Rotate parent twist ref axis
	FQuat Con2ToCon1Rot = FQuat::FindBetween(Con2RotationAxis, Con1RotationAxis);
	FVector Con2InCon1DrawOnAxis = Con2ToCon1Rot.RotateVector(Con2DrawOnAxis);

	FTransform ConeLimitTM(Con2InCon1DrawOnAxis, Con1RotationAxis ^ Con2InCon1DrawOnAxis, Con1RotationAxis, Con1Frame.GetTranslation());
	FMatrix ConeToWorld = FScaleMatrix(FVector(Length * 0.9f)) * ConeLimitTM.ToMatrixWithScale();
	return ConeToWorld;
}

// Draw fan implementation for Max and Min limits
void DrawLimitFan(const FTransform& transform, const FVector& RotAxis, const FVector& DrawVector, const float Max, const float Min, const float scale, UMaterialInterface* Material, FPrimitiveDrawInterface* PDI)
{
	static const int DrawConeLimitSides = 40;
	auto trans = transform;
	if (!FMath::IsNearlyZero(Max))
	{
		float Limit = FMath::DegreesToRadians(Max) * 0.5f;
		FQuat q(-RotAxis, Limit);
		trans.SetRotation(transform.GetRotation()*q);
		FMatrix ConeToWorld = HelpBuildFan(transform, trans, DrawVector, RotAxis, scale);
		DrawCone(PDI, ConeToWorld, Limit, 0, DrawConeLimitSides, false, FColor::Red, Material->GetRenderProxy(false), SDPG_World);
	}
	if (!FMath::IsNearlyZero(Min))
	{
		float Limit = FMath::DegreesToRadians(Min) * 0.5f;
		FQuat q(-RotAxis, Limit);
		trans.SetRotation(transform.GetRotation()*q);
		FMatrix ConeToWorld = HelpBuildFan(transform, trans, DrawVector, RotAxis, scale);
		DrawCone(PDI, ConeToWorld, -Limit, 0, DrawConeLimitSides, false, FColor::Red, Material->GetRenderProxy(false), SDPG_World);
	}

}


void UIKinemaRigToolEdSkeletalMeshComponent::DrawLimits(FPrimitiveDrawInterface* PDI, const FIKinemaBoneDef& Bone, const FTransform& transform)
{
	static UMaterialInterface * MaterialRed = GEngine->ConstraintLimitMaterialX;
	static UMaterialInterface * MaterialGreen = GEngine->ConstraintLimitMaterialY;
	static UMaterialInterface * MaterialBlue = GEngine->ConstraintLimitMaterialZ;

	DrawCoordinateSystem(PDI, transform.GetTranslation(), transform.GetRotation().Rotator(), 50, SDPG_Foreground);


	auto BoneAxis = FVector(0, 0, 0);
	auto upVector = FVector(0, 0, 0);
	switch (Bone.Limits.BoneAxis)
	{
	case EIKinemaBoneAxisType::EIKBAT_X:
		BoneAxis = FVector(1.f, 0.f, 0.f);
		upVector = FVector(0.f, 1.f, 0.f);
		break;
	case EIKinemaBoneAxisType::EIKBAT_Y:
		BoneAxis = FVector(0.f, 1.f, 0.f);
		upVector = FVector(0.f, 0.f, 1.f);
		break;
	case EIKinemaBoneAxisType::EIKBAT_Z:
		BoneAxis = FVector(0.f, 0.f, 1.f);
		upVector = FVector(1.f, 0.f, 0.f);
		break;
	case EIKinemaBoneAxisType::EIKBAT_NEGX:
		BoneAxis = FVector(-1.f, 0.f, 0.f);
		upVector = FVector(0.f, 1.f, 0.f);
		break;
	case EIKinemaBoneAxisType::EIKBAT_NEGY:
		BoneAxis = FVector(0.f, -1.f, 0.f);
		upVector = FVector(0.f, 0.f, 1.f);
		break;
	case EIKinemaBoneAxisType::EIKBAT_NEGZ:
		BoneAxis = FVector(0.f, 0.f, -1.f);
		upVector = FVector(1.f, 0.f, 0.f);
		break;
	}
	auto lookAt = transform.GetRotation().RotateVector(BoneAxis);


	const float DrawScale = SharedData->AverageBoneLength;// *2;
														  //Draw the Yaw
	{
		auto RotAxis = FVector(0, 0, 1);
		//Make sure we draw on an Axis that is perpendicular to the RotAxis, LookAt or UpAxis
		auto DrawAxis = FMath::IsNearlyEqual(FMath::Abs(BoneAxis | RotAxis), 1.f, 0.1f) ? upVector : BoneAxis;
		DrawLimitFan(transform, RotAxis, DrawAxis, Bone.Limits.MaxDegrees.Z, Bone.Limits.MinDegrees.Z, DrawScale, MaterialBlue, PDI);

	}

	//Pitch
	{
		auto RotAxis = FVector(0, 1, 0);
		//Make sure we draw on an Axis that is perpendicular to the RotAxis, LookAt or UpAxis
		auto DrawAxis = FMath::IsNearlyEqual(FMath::Abs(BoneAxis | RotAxis), 1.f, 0.1f) ? upVector : BoneAxis;
		DrawLimitFan(transform, RotAxis, DrawAxis, Bone.Limits.MaxDegrees.Y, Bone.Limits.MinDegrees.Y, DrawScale, MaterialGreen, PDI);
	}

	//Roll
	{
		auto RotAxis = FVector(1, 0, 0);
		//Make sure we draw on an Axis that is perpendicular to the RotAxis, LookAt or UpAxis
		auto DrawAxis = FMath::IsNearlyEqual(FMath::Abs(BoneAxis | RotAxis), 1.f, 0.1f) ? upVector : BoneAxis;
		DrawLimitFan(transform, RotAxis, DrawAxis, Bone.Limits.MaxDegrees.X, Bone.Limits.MinDegrees.X, DrawScale, MaterialRed, PDI);
	}
}


void UIKinemaRigToolEdSkeletalMeshComponent::DrawLimits(FPrimitiveDrawInterface* PDI, const FIKinemaTaskDef& task, const FTransform& transform)
{
	static UMaterialInterface * MaterialRed = GEngine->ConstraintLimitMaterialX;
	static UMaterialInterface * MaterialGreen = GEngine->ConstraintLimitMaterialY;
	static UMaterialInterface * MaterialBlue = GEngine->ConstraintLimitMaterialZ;

	DrawCoordinateSystem(PDI, transform.GetTranslation(), transform.GetRotation().Rotator(), 50, SDPG_Foreground);
	auto lookAt = transform.GetRotation().UnrotateVector(task.TipOffset);
	auto upVector = transform.GetRotation().UnrotateVector(FVector(0, 0, 1));

	const float DrawScale = SharedData->AverageBoneLength * 2;
	//Draw the Yaw
	{
		auto RotAxis = FVector(0, 0, 1);
		//Make sure we draw on an Axis that is perpendicular to the RotAxis, LookAt or UpAxis
		auto DrawAxis = FMath::IsNearlyEqual(FMath::Abs(lookAt | RotAxis), 1.f, 0.1f) ? upVector : lookAt;
		DrawLimitFan(transform, RotAxis, DrawAxis, task.LookAtLimits.Max.Yaw, task.LookAtLimits.Min.Yaw, DrawScale, MaterialBlue, PDI);

	}

	//Pitch
	{
		auto RotAxis = FVector(0, 1, 0);
		//Make sure we draw on an Axis that is perpendicular to the RotAxis, LookAt or UpAxis
		auto DrawAxis = FMath::IsNearlyEqual(FMath::Abs(lookAt | RotAxis), 1.f, 0.1f) ? upVector : lookAt;
		DrawLimitFan(transform, RotAxis, DrawAxis, task.LookAtLimits.Max.Pitch, task.LookAtLimits.Min.Pitch, DrawScale, MaterialGreen, PDI);
	}

	//Roll
	{
		auto RotAxis = FVector(1, 0, 0);
		//Make sure we draw on an Axis that is perpendicular to the RotAxis, LookAt or UpAxis
		auto DrawAxis = FMath::IsNearlyEqual(FMath::Abs(lookAt | RotAxis), 1.f, 0.1f) ? upVector : lookAt;
		DrawLimitFan(transform, RotAxis, DrawAxis, task.LookAtLimits.Max.Roll, task.LookAtLimits.Min.Roll, DrawScale, MaterialRed, PDI);
	}

}
