/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include "AnimNode_LADataStream.h"


DECLARE_LOG_CATEGORY_CLASS(LogIKinemaRunTimeRigTool, Warning, All);
DECLARE_DELEGATE_OneParam(FOnSourceScaleChanged, float /** SourceScale */);

class SMocapSourcePicker : public SCompoundWidget
{
public:
	
	enum RenderTag
	{
		Other,
		LiveLinkMenu
	};

	SLATE_BEGIN_ARGS(SMocapSourcePicker) {}
		SLATE_EVENT(FOnSourceScaleChanged, OnSourceScaleChanged)
		SLATE_ARGUMENT(RenderTag, renderTag)
	SLATE_END_ARGS()

public:

	void Construct(const FArguments& InArgs);

	const EMocapServer GetMocapServer() const;
	const FString& GetServerIP() const;
	const FString& GetSubjectName() const;
	const FString& GetTemplateName() const;

	const int GetPortNumber() const;
	const float GetImportScale() const;


	void SetMocapServer(EMocapServer Type);
	void SetServerIP(const FString& ServerIP);
	void SetSubjectName(const FString& SubjectName);
	void SetTemplateName(const FString& TemplateName);
	void SetPortNumber(const int portNumber);
	void SetImportScale(const float ImportScale);

	bool IsYup() { return bIsYUp; };
	bool IsRigidBodies() { return bIsRigidBody; };
	bool ZeroRotations() { return bZeroRotations; };
private:

	/** Delegate called when changing Mocap source providers */
	void ChangeMocapSourceProvider(int32 ProviderIndex);

	void ChangeTemplateName(int32 TemplateIndex);

	void OnServerAddressCommitted(const FText& InputText, ETextCommit::Type CommitType);
	void OnServerPortCommitted(const FText& InputText, ETextCommit::Type CommitType);
	void OnSubjectNameCommitted(const FText& InputText, ETextCommit::Type CommitType);
	void OnImportScaleCommitted(const FText& InputText, ETextCommit::Type CommitType);
	void SetYup(ECheckBoxState InState);
	void SetRigidBodies(ECheckBoxState InState);
	void OnZeroRotations(ECheckBoxState InState);

	void MakeStandardWidget();
	void MakeLiveLinkWeidget();

	bool CanZeroRotations() const;

	/** Get the content for the drop-down menu for picking providers */
	TSharedRef<SWidget> OnGetMenuContent() const;

	/** Get the button text for the drop-down */
	FText OnGetButtonText() const;

	/** Get the text to be displayed given the name of the provider */
	FText GetProviderText(const FName& InName) const;

	TSharedRef<SWidget> OnGetTemplateMenuContent() const;

	FText OnGetTemplateButtonText() const;
	EVisibility IsLicRB;
	EMocapServer MocapServer;
	FString ServerIP;
	FString SubjectName;
	FString TemplateName;
	int PortNumber;
	float ImportScale;
	bool bIsYUp;
	bool bIsRigidBody;
	bool bZeroRotations;
	TSharedPtr<SEditableTextBox> ServerAddressBox;
	TSharedPtr<SEditableTextBox> ServerPortBox;
	TSharedPtr<SEditableTextBox> SubjectNameBox;
	TSharedPtr<SEditableTextBox> ImportScaleBox;
	FOnSourceScaleChanged OnSourceScaleChanged;
};

