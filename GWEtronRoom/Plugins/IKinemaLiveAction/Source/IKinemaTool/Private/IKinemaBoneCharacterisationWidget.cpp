/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/


#include "IKinemaBoneCharacterisationWidget.h"
#include "IKinemaRigToolModule.h"
#include "ObjectTools.h"
#include "ScopedTransaction.h"
#include "AssetRegistryModule.h"
#include "Editor/PropertyEditor/Public/PropertyEditorModule.h"
#include "Editor/ContentBrowser/Public/ContentBrowserModule.h"
#include "WorkflowOrientedApp/SContentReference.h"
#include "AssetNotifications.h"
#include "Animation/Rig.h"

#include "Widgets/Input/SSearchBox.h"
#include "Widgets/Text/SInlineEditableTextBlock.h"

#define LOCTEXT_NAMESPACE "SIKinemaBoneCharacterisationWidget"
#include "BoneSelectionWidget.h"
#include "Widgets/Input/SSearchBox.h"
#include "Editor/PropertyEditor/Public/DetailLayoutBuilder.h"

#define LOCTEXT_NAMESPACE "SIKinemaBoneCharacterisationWidget"

/////////////////////////////////////////////////////

void SIKinemaBoneCharacterisationWidget::Construct(const FArguments& InArgs)
{
	if(InArgs._Skeleton.IsValid())
	{
		TargetSkeleton = InArgs._Skeleton.Get();
	}

	//check(TargetSkeleton);

	OnBoneSelectionChanged = InArgs._OnBoneSelectionChanged;
	OnGetSelectedBone = InArgs._OnGetSelectedBone;

	FText FinalTooltip = FText::Format(LOCTEXT("CharacterisationClickToolTip", "{0}\nClick to choose a bone characterisation"), InArgs._Tooltip);

	SAssignNew(TreeView, STreeView<TSharedPtr<FBoneNameInfo>>)
		.TreeItemsSource(&SkeletonTreeInfo)
		.OnGenerateRow(this, &SIKinemaBoneCharacterisationWidget::MakeTreeRowWidget)
		.OnGetChildren(this, &SIKinemaBoneCharacterisationWidget::GetChildrenForInfo)
		.OnSelectionChanged(this, &SIKinemaBoneCharacterisationWidget::OnSelectionChanged)
		.SelectionMode(ESelectionMode::Single);

	this->ChildSlot
	[
		SAssignNew(BonePickerButton, SComboButton)
		.OnGetMenuContent(FOnGetContent::CreateSP(this, &SIKinemaBoneCharacterisationWidget::CreateSkeletonWidgetMenu))
		.ContentPadding(FMargin(4.0f, 2.0f, 4.0f, 2.0f))
		.ButtonContent()
		[
			SNew(STextBlock)
			.Text(this, &SIKinemaBoneCharacterisationWidget::GetCurrentBoneName)
			.Font(IDetailLayoutBuilder::GetDetailFont())
			.ToolTipText(FinalTooltip)
		]
	];
}

TSharedRef<SWidget> SIKinemaBoneCharacterisationWidget::CreateSkeletonWidgetMenu()
{
	RebuildBoneList();

	TSharedPtr<SSearchBox> SearchWidgetToFocus = NULL;
	TSharedRef<SBorder> MenuWidget = SNew(SBorder)
		.Padding(6)
		.BorderImage(FEditorStyle::GetBrush("NoBorder"))
		.Content()
		[
			SNew(SBox)
			.WidthOverride(300)
			.HeightOverride(512)
			.Content()
			[
				SNew(SVerticalBox)
				+SVerticalBox::Slot()
				.AutoHeight()
				[
					SNew(STextBlock)
					.Font(FEditorStyle::GetFontStyle("BoldFont"))
						.Text(LOCTEXT("CharacterisationPickerTitle", "Pick Characterisation..."))
				]
				+SVerticalBox::Slot()
				.AutoHeight()
				[
					SNew(SSeparator)
					.SeparatorImage(FEditorStyle::GetBrush("Menu.Separator"))
						.Orientation(Orient_Horizontal)
				]
				+SVerticalBox::Slot()
				.AutoHeight()
				[
					SAssignNew(SearchWidgetToFocus, SSearchBox)
					.SelectAllTextWhenFocused(true)
					.OnTextChanged(this, &SIKinemaBoneCharacterisationWidget::OnFilterTextChanged)
					.OnTextCommitted(this, &SIKinemaBoneCharacterisationWidget::OnFilterTextCommitted)
					.HintText(NSLOCTEXT("BonePicker", "Search", "Search..."))
				]
				+SVerticalBox::Slot()
				[
					TreeView->AsShared()
				]
			]
		];

	BonePickerButton->SetMenuContentWidgetToFocus(SearchWidgetToFocus);

	return MenuWidget;
}

TSharedRef<ITableRow> SIKinemaBoneCharacterisationWidget::MakeTreeRowWidget(TSharedPtr<FBoneNameInfo> InInfo, const TSharedRef<STableViewBase>& OwnerTable)
{
	return SNew(STableRow<TSharedPtr<FBoneNameInfo>>, OwnerTable)
		.Content()
		[
			SNew(STextBlock)
			.HighlightText(FilterText)
			.Text(FText::FromName(InInfo->BoneName))
		];
}

void SIKinemaBoneCharacterisationWidget::GetChildrenForInfo(TSharedPtr<FBoneNameInfo> InInfo, TArray< TSharedPtr<FBoneNameInfo> >& OutChildren)
{
	//OutChildren = InInfo->Children;
}

void SIKinemaBoneCharacterisationWidget::OnFilterTextChanged(const FText& InFilterText)
{
	FilterText = InFilterText;

	RebuildBoneList();
}

void SIKinemaBoneCharacterisationWidget::OnFilterTextCommitted(const FText& SearchText, ETextCommit::Type CommitInfo)
{
	// Already committed as the text was typed
}

void SIKinemaBoneCharacterisationWidget::RebuildBoneList()
{
	SkeletonTreeInfo.Empty();
	SkeletonTreeInfoFlat.Empty();
	
	UEnum* EnumPtr = nullptr;
	int index = UEnum::LookupEnumNameSlow(TEXT("EHIPS"), &EnumPtr);
	
	TArray< TPair<FName, int32> > SortedProviderNames;
	const int NumProviders = EnumPtr->NumEnums() - 1;
	SortedProviderNames.Reserve(NumProviders);
	for (int ProviderIndex = 0; ProviderIndex < NumProviders; ++ProviderIndex)
	{
		const FText ProviderName = EnumPtr->GetDisplayNameTextByIndex(ProviderIndex);
		int32 ProviderSortKey = ProviderName.ToString() == ("None") ? -1 * ProviderIndex : ProviderIndex;
		SortedProviderNames.Add(TPairInitializer<FName, int32>(FName(*ProviderName.ToString()), ProviderSortKey));
	}

	// Sort based on the provider index
	SortedProviderNames.Sort([](const TPair<FName, int32>& One, const TPair<FName, int32>& Two)
	{
		return One.Value < Two.Value;
	});


	for (auto SortedProviderNameIter = SortedProviderNames.CreateConstIterator(); SortedProviderNameIter; ++SortedProviderNameIter)
	{
		const FText ProviderText = SortedProviderNameIter->Key == "None"? FText::FromString("") : FText::FromName(SortedProviderNameIter->Key);
		const int32  ProviderIndex = SortedProviderNameIter->Value;
		TSharedRef<FBoneNameInfo> BoneInfo = MakeShareable(new FBoneNameInfo(SortedProviderNameIter->Key, ProviderIndex));
		SkeletonTreeInfo.Add(BoneInfo);
		SkeletonTreeInfoFlat.Add(BoneInfo);
		TreeView->SetItemExpansion(BoneInfo, true);

	}
	TreeView->RequestTreeRefresh();
}

void SIKinemaBoneCharacterisationWidget::OnSelectionChanged(TSharedPtr<FBoneNameInfo> BoneInfo, ESelectInfo::Type SelectInfo)
{
	FilterText = FText::FromString("");

	//Because we recreate all our items on tree refresh we will get a spurious null selection event initially.
	if (BoneInfo.IsValid())
	{
		if (OnBoneSelectionChanged.IsBound())
		{
			OnBoneSelectionChanged.Execute(BoneInfo->BoneName, BoneInfo->Index);
		}

		BonePickerButton->SetIsOpen(false);
	}
}

FText SIKinemaBoneCharacterisationWidget::GetCurrentBoneName() const
{
	if(OnGetSelectedBone.IsBound())
	{
		FName Name = OnGetSelectedBone.Execute();

		return FText::FromName(Name);
	}

	// @todo implement default solution?
	return FText::GetEmpty();
}
#undef LOCTEXT_NAMESPACE

