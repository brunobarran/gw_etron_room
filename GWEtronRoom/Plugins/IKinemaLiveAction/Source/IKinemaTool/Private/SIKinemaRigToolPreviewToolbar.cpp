/* Copyright (C) 2006-2017, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "SIKinemaRigToolPreviewToolbar.h"
#include "IKinemaRigToolModule.h"
#include "IKinemaRigTool.h"
#include "SViewportToolBar.h"
#include "STransformViewportToolbar.h"
#include "SEditorViewportToolBarMenu.h"
#include "SEditorViewportViewMenu.h"
#include "IKinemaRigToolActions.h"
#include "SEditorViewport.h"
#include "SIKinemaRigToolPreviewToolbar.h"
#include "IKinemaRigToolPreviewViewportClient.h"

#define LOCTEXT_NAMESPACE "IKinemaRigViewport"

void SIKinemaRigToolPreviewViewportToolBar::Construct(const FArguments& InArgs, TSharedPtr<class SEditorViewport> InRealViewport)
{
	IKinemaRigToolPtr = InArgs._IKinemaRigToolPtr;
	
	static const FName DefaultForegroundName("DefaultForeground");

	this->ChildSlot
	[
		SNew(SBorder)
		.BorderImage(FEditorStyle::GetBrush("NoBorder"))
		// Color and opacity is changed based on whether or not the mouse cursor is hovering over the toolbar area
		.ColorAndOpacity(this, &SViewportToolBar::OnGetColorAndOpacity)
		.ForegroundColor( FEditorStyle::GetSlateColor(DefaultForegroundName) )
		[
			//FOV Menu
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			.AutoWidth()
			.Padding(2.0f, 2.0f)
			[
				SNew(SEditorViewportToolbarMenu)
				.ParentToolBar(SharedThis(this))
				.Cursor(EMouseCursor::Default)
				.Image("EditorViewportToolBar.MenuDropdown")
				.AddMetaData<FTagMetaData>(FTagMetaData(TEXT("EditorViewportToolBar.MenuDropdown")))
				.OnGetMenuContent(this, &SIKinemaRigToolPreviewViewportToolBar::GenerateOptionsMenu)
			]

			//Camrea Type
			+ SHorizontalBox::Slot()
			.AutoWidth()
			.Padding(5.0f, 2.0f)
			[
				SNew(SEditorViewportToolbarMenu)
				.ParentToolBar(SharedThis(this))
				.Cursor(EMouseCursor::Default)
				.Label(this, &SIKinemaRigToolPreviewViewportToolBar::GetCameraMenuLabel)
				.LabelIcon(this, &SIKinemaRigToolPreviewViewportToolBar::GetCameraMenuLabelIcon)
				.OnGetMenuContent(this, &SIKinemaRigToolPreviewViewportToolBar::GeneratePerspectiveMenu)
			]
			+SHorizontalBox::Slot()
			.AutoWidth()
			.Padding(5.0f, 2.0f)
			[
				SNew(SEditorViewportToolbarMenu)
				.ParentToolBar(SharedThis(this))
				.Cursor(EMouseCursor::Default)
				.Label(NSLOCTEXT("IKinemaRigTool", "ViewMenuTitle_Default", "View"))
				.OnGetMenuContent(this, &SIKinemaRigToolPreviewViewportToolBar::GenerateViewMenu) 
			]
			+SHorizontalBox::Slot()
			.AutoWidth()
			.Padding(5.0f, 2.0f)
			[
				SNew(SEditorViewportToolbarMenu)
				.ParentToolBar(SharedThis(this))
				.Cursor(EMouseCursor::Default)
				.Label(NSLOCTEXT("IKinemaRigTool", "ModesMenuTitle_Default", "Modes"))
				.OnGetMenuContent(this, &SIKinemaRigToolPreviewViewportToolBar::GenerateModesMenu) 
			]

			+ SHorizontalBox::Slot()
			.Padding(3.0f, 1.0f)
			.HAlign(HAlign_Right)
			[
				SNew(STransformViewportToolBar)
				.Viewport(InRealViewport)
				.CommandList(InRealViewport->GetCommandList())
			]
		]
	];

	SViewportToolBar::Construct(SViewportToolBar::FArguments());
}


FText SIKinemaRigToolPreviewViewportToolBar::GetCameraMenuLabel() const
{
	FText Label = LOCTEXT("Viewport_Default", "Camera");
	TSharedPtr<SIKinemaRigToolPreviewViewport> EditorViewport = IKinemaRigToolPtr.Pin()->GetPreviewViewportWidget();
	if (EditorViewport.IsValid())
	{
		switch (EditorViewport->GetViewportClient()->GetViewportType())
		{
		case LVT_Perspective:
			Label = LOCTEXT("CameraMenuTitle_Perspective", "Perspective");
			break;

		case LVT_OrthoXY:
			Label = LOCTEXT("CameraMenuTitle_Top", "Top");
			break;

		case LVT_OrthoYZ:
			Label = LOCTEXT("CameraMenuTitle_Left", "Left");
			break;

		case LVT_OrthoXZ:
			Label = LOCTEXT("CameraMenuTitle_Front", "Front");
			break;

		case LVT_OrthoNegativeXY:
			Label = LOCTEXT("CameraMenuTitle_Bottom", "Bottom");
			break;

		case LVT_OrthoNegativeYZ:
			Label = LOCTEXT("CameraMenuTitle_Right", "Right");
			break;

		case LVT_OrthoNegativeXZ:
			Label = LOCTEXT("CameraMenuTitle_Back", "Back");
			break;
		case LVT_OrthoFreelook:
			break;
		}
	}

	return Label;
}

const FSlateBrush* SIKinemaRigToolPreviewViewportToolBar::GetCameraMenuLabelIcon() const
{
	FName Icon = NAME_None;
	TSharedPtr<SIKinemaRigToolPreviewViewport> EditorViewport = IKinemaRigToolPtr.Pin()->GetPreviewViewportWidget();
	if (EditorViewport.IsValid())
	{
		switch (EditorViewport->GetViewportClient()->GetViewportType())
		{
		case LVT_Perspective:
			Icon = FName("EditorViewport.Perspective");
			break;

		case LVT_OrthoXY:
			Icon = FName("EditorViewport.Top");
			break;

		case LVT_OrthoYZ:
			Icon = FName("EditorViewport.Left");
			break;

		case LVT_OrthoXZ:
			Icon = FName("EditorViewport.Front");
			break;

		case LVT_OrthoNegativeXY:
			Icon = FName("EditorViewport.Bottom");
			break;

		case LVT_OrthoNegativeYZ:
			Icon = FName("EditorViewport.Right");
			break;

		case LVT_OrthoNegativeXZ:
			Icon = FName("EditorViewport.Back");
			break;
		case LVT_OrthoFreelook:
			break;
		}
	}

	return FEditorStyle::GetBrush(Icon);
}



TSharedRef<SWidget> SIKinemaRigToolPreviewViewportToolBar::GeneratePerspectiveMenu() const
{
	const FIKinemaRigToolCommands& Actions = FIKinemaRigToolCommands::Get();

	const bool bInShouldCloseWindowAfterMenuSelection = true;
	FMenuBuilder PerspectiveMenuBuilder(bInShouldCloseWindowAfterMenuSelection, IKinemaRigToolPtr.Pin()->GetToolkitCommands());
	{
		PerspectiveMenuBuilder.AddMenuEntry(Actions.PerspectiveView);
		PerspectiveMenuBuilder.AddMenuEntry(Actions.TopView);
		PerspectiveMenuBuilder.AddMenuEntry(Actions.BottomView);
		PerspectiveMenuBuilder.AddMenuEntry(Actions.LeftView);
		PerspectiveMenuBuilder.AddMenuEntry(Actions.RightView);
		PerspectiveMenuBuilder.AddMenuEntry(Actions.FrontView);
		PerspectiveMenuBuilder.AddMenuEntry(Actions.BackView);
	}

	return PerspectiveMenuBuilder.MakeWidget();
}

TSharedRef<SWidget> SIKinemaRigToolPreviewViewportToolBar::GenerateViewMenu() const
{
	const FIKinemaRigToolCommands& Actions = FIKinemaRigToolCommands::Get();
		
	const bool bInShouldCloseWindowAfterMenuSelection = true;
	FMenuBuilder ViewMenuBuilder(bInShouldCloseWindowAfterMenuSelection, IKinemaRigToolPtr.Pin()->GetToolkitCommands());
	{
		ViewMenuBuilder.AddMenuEntry(Actions.ShowSkeleton);
		ViewMenuBuilder.AddMenuEntry(Actions.ShowCollisions);
		ViewMenuBuilder.AddMenuEntry(Actions.DrawGroundBox);
		ViewMenuBuilder.AddMenuEntry(Actions.ShowNonActive);
		ViewMenuBuilder.AddMenuEntry(Actions.ShowConstraints);
		ViewMenuBuilder.AddMenuEntry(Actions.ToggleGraphicsHierarchy);
	}

	return ViewMenuBuilder.MakeWidget();	
}

TSharedRef<SWidget> SIKinemaRigToolPreviewViewportToolBar::GenerateModesMenu() const
{
	const bool bInShouldCloseWindowAfterMenuSelection = true;
	FMenuBuilder ModesMenuBuilder(bInShouldCloseWindowAfterMenuSelection, IKinemaRigToolPtr.Pin()->GetToolkitCommands());
	{
		// Mesh, collision and constraint rendering modes
		{
			struct Local
			{
				static void BuildMeshRenderModeMenu(FMenuBuilder& Menu)
				{
					const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();

					Menu.BeginSection("IKinemaRigToolRenderingMode", NSLOCTEXT("IKinemaRigTool", "MeshRenderModeHeader", "Mesh Rendering Mode"));
					{
						Menu.AddMenuEntry(Commands.MeshRenderingMode_Solid);
						Menu.AddMenuEntry(Commands.MeshRenderingMode_Wireframe);
						Menu.AddMenuEntry(Commands.MeshRenderingMode_None);
					}
					Menu.EndSection();
				}
			};

			ModesMenuBuilder.BeginSection("IKinemaRigToolSubMenus");
			{
				ModesMenuBuilder.AddSubMenu(NSLOCTEXT("IKinemaRigTool", "MeshRenderModeSubMenu", "MeshRender Mode"), FText::GetEmpty(), FNewMenuDelegate::CreateStatic(&Local::BuildMeshRenderModeMenu));
			}
			ModesMenuBuilder.EndSection();
		}
	}

	return ModesMenuBuilder.MakeWidget();	
}


TSharedRef<SWidget> SIKinemaRigToolPreviewViewportToolBar::GenerateOptionsMenu() const
{

	const bool bIsPerspective = IKinemaRigToolPtr.Pin()->GetPreviewViewportWidget()->GetViewportClient()->IsPerspective();
	const bool bInShouldCloseWindowAfterMenuSelection = true;
	FMenuBuilder OptionsMenuBuilder(bInShouldCloseWindowAfterMenuSelection, IKinemaRigToolPtr.Pin()->GetToolkitCommands());
	{
		OptionsMenuBuilder.BeginSection("LevelViewportViewportOptions", LOCTEXT("OptionsMenuHeader", "Viewport Options"));
		{
			if (bIsPerspective)
			{
				OptionsMenuBuilder.AddWidget(GenerateFOVMenu(), LOCTEXT("FOVAngle", "Field of View (H)"));
			}
		}
		OptionsMenuBuilder.EndSection();
	}

	return OptionsMenuBuilder.MakeWidget();
}

TSharedRef<SWidget> SIKinemaRigToolPreviewViewportToolBar::GenerateFOVMenu() const
{
	const float FOVMin = 5.f;
	const float FOVMax = 170.f;

	return
		SNew(SBox)
		.HAlign(HAlign_Right)
		[
			SNew(SBox)
			.Padding(FMargin(4.0f, 0.0f, 0.0f, 0.0f))
		.WidthOverride(100.0f)
		[
			SNew(SSpinBox<float>)
			.Font(FEditorStyle::GetFontStyle(TEXT("MenuItem.Font")))
		.MinValue(FOVMin)
		.MaxValue(FOVMax)
		.Value(this, &SIKinemaRigToolPreviewViewportToolBar::OnGetFOVValue)
		.OnValueChanged(this, &SIKinemaRigToolPreviewViewportToolBar::OnFOVValueChanged)
		]
		];
}

float SIKinemaRigToolPreviewViewportToolBar::OnGetFOVValue() const
{
	return IKinemaRigToolPtr.Pin()->GetPreviewViewportWidget()->GetViewportClient()->ViewFOV;
}

void SIKinemaRigToolPreviewViewportToolBar::OnFOVValueChanged(float NewValue)
{
	bool bUpdateStoredFOV = true;
	FIKinemaRigToolEdPreviewViewportClient* ViewportClient = IKinemaRigToolPtr.Pin()->GetPreviewViewportWidget()->GetViewportClient().Get();
	ViewportClient->FOVAngle = NewValue;
	ViewportClient->ViewFOV = NewValue;
	ViewportClient->Invalidate();
}

#undef LOCTEXT_NAMESPACE