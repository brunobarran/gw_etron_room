/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaRigToolActions.h"
#include "IKinemaRigToolModule.h"
#include "IKinemaRigTool.h"

#define LOCTEXT_NAMESPACE ""

void FIKinemaRigToolCommands::RegisterCommands()
{
	UI_COMMAND(ChangeDefaultMesh, "Change Mesh", "Change Default SkeletalMesh", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(ResetBoneSettings, "Reset", "Reset IKinema Segment Properties", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(EditingMode_Body, "Body Mode", "Body Editing Mode", EUserInterfaceActionType::RadioButton, FInputChord());
	UI_COMMAND(EditingMode_Constraint, "Constraint Mode", "Constraint Editing Mode", EUserInterfaceActionType::RadioButton, FInputChord());
	UI_COMMAND(CopyProperties, "Copy Properties", "Copy Properties: Copy Properties Of Currently Selected Object To Next Selected Object", EUserInterfaceActionType::Button, FInputChord(EModifierKey::Control, EKeys::C));
	UI_COMMAND(CopyBones, "Copy Bone Settings", "Copy The Settings Of Currently Selected Segment to Next Selected Segment", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(CopyTasks, "Copy Task Settings", "Copy The Settings Of Currently Selected Task to Next Selected Task", EUserInterfaceActionType::Button, FInputChord());


	UI_COMMAND(PasteProperties, "Paste Properties", "Paste Properties: Copy Properties Of Currently Selected Object To Next Selected Object", EUserInterfaceActionType::Button, FInputChord(EModifierKey::Control, EKeys::V));
	UI_COMMAND(PasteBones, "Paste Bone Settings", "Paste The Settings Of Currently Selected Segment to Next Selected Segment", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(PasteTasks, "Paste Task Settings", "Paste The Settings Of Currently Selected Task to Next Selected Task", EUserInterfaceActionType::Button, FInputChord());

	UI_COMMAND(AddHipTask, "Hip Task", "Create task with hips preset", EUserInterfaceActionType::Button, FInputChord(EKeys::H));
	UI_COMMAND(AddFootTask, "Foot Task", "Create task with foot preset", EUserInterfaceActionType::Button, FInputChord(EKeys::T));
	UI_COMMAND(AddHandTask, "Hand Task", "Create task with hand preset", EUserInterfaceActionType::Button, FInputChord(EKeys::D));
	UI_COMMAND(AddHeadTask, "Head Task", "Create task with head preset", EUserInterfaceActionType::Button, FInputChord(EKeys::B));
	UI_COMMAND(AddChestTask, "Chest Task", "Create task with chest preset", EUserInterfaceActionType::Button, FInputChord(EKeys::C));
	UI_COMMAND(AddElbowTask, "Elbow Task", "Create task with Elbow preset", EUserInterfaceActionType::Button, FInputChord(EKeys::L));
	UI_COMMAND(AddKneeTask, "Knee Task", "Create task with knee preset", EUserInterfaceActionType::Button, FInputChord(EKeys::K));

	UI_COMMAND(SetAsHips, "Hips", "Represents the hip bone", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(SetAsChest, "Chest", "Represents the Chest bone", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(SetAsShoulder, "Shoulder", "Represents the Shoulder bone", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(SetAsElbow, "Elbow", "Represents the Elbow bone", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(SetAsHand, "Hand", "Represents the Hand bonet", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(SetAsHead, "Head", "Represents the Head bone", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(SetAsFoot, "Foot", "Represents the Foot bone", EUserInterfaceActionType::Button, FInputChord());


	UI_COMMAND(RepeatLastSimulation, "Simulate", "Previews IKINEMA Rig Simulation", EUserInterfaceActionType::RadioButton, FInputChord(EKeys::Enter));
	UI_COMMAND(MeshRenderingMode_Solid, "Solid", "Solid Mesh Rendering Mode", EUserInterfaceActionType::RadioButton, FInputChord());
	UI_COMMAND(MeshRenderingMode_Wireframe, "Wireframe", "Wireframe Mesh Rendering Mode", EUserInterfaceActionType::RadioButton, FInputChord());
	UI_COMMAND(MeshRenderingMode_None, "None", "No Mesh Rendering Mode", EUserInterfaceActionType::RadioButton, FInputChord());
	UI_COMMAND(ShowNonActive, "Non Active IKINEMA Bones", "Display non active IKINEMA bones", EUserInterfaceActionType::ToggleButton, FInputChord());
	UI_COMMAND(DrawGroundBox, "Ground Box", "Displays Floor Grid", EUserInterfaceActionType::ToggleButton, FInputChord());
	UI_COMMAND(ToggleGraphicsHierarchy, "Hierarchy", "Show Graphical Hierarchy Of Joints In Preview Viewport", EUserInterfaceActionType::ToggleButton, FInputChord());
	UI_COMMAND(ShowConstraints, "IKINEMA Constraints", "Displays IKINEMA constraints", EUserInterfaceActionType::ToggleButton, FInputChord());
	UI_COMMAND(ToggleMassProperties, "Mass Properties", "Show Mass Properties For Bodies When Simulation Is Enabled", EUserInterfaceActionType::ToggleButton, FInputChord());
	UI_COMMAND(AddOrientationalConstraint, "Add Orientation task", "Add orientation task To Selected Bone", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(AddPositionalConstraint, "Add Position task", "Add position task To Selected Bone", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(AddDualConstraint, "Add Dual task", "Add dual IK task To Selected Bone", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(ResetConstraint, "Reset", "Reset Constraint", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(ResetCoMConstraint, "Reset", "Reset CoM Constraint", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(DeleteConstraint, "Delete", "Delete Selected Constraint", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(PlayAnimation, "Play", "Play Animation", EUserInterfaceActionType::ToggleButton, FInputChord());
	UI_COMMAND(RecordAnimation, "Record", "Record Animation", EUserInterfaceActionType::ToggleButton, FInputChord());
	UI_COMMAND(ShowSkeleton, "Skeleton", "Show Skeleton", EUserInterfaceActionType::ToggleButton, FInputChord());
	UI_COMMAND(ShowCollisions, "Collision Primitives", "Show self collision primitives", EUserInterfaceActionType::ToggleButton, FInputChord());
	UI_COMMAND(DeleteBody, "Delete", "Delete Selected Body", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(DeleteAllBodiesBelow, "Delete All Bodies Below", "Delete All Bodies Below", EUserInterfaceActionType::Button, FInputChord(EModifierKey::Control, EKeys::Platform_Delete));
	UI_COMMAND(SelectAllObjectsBelow, "Select Hierarchy Below", "Select Hierarchy Below", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(SelectAllObjects, "Select All Objects", "Select All Objects", EUserInterfaceActionType::Button, FInputChord(EModifierKey::Control, EKeys::A));
	UI_COMMAND(HierarchyFilterAll, "All Bones", "Show Entire Hierarchy", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(HierarchyFilterBodies, "Constraints", "Show Constraints", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(PerspectiveView, "Perspective", "Perspective", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(LeftView, "Left", "Orthographic view from left", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(TopView, "Top", "Orthographic view from top", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(FrontView, "Front", "Orthographic view from front", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(RightView, "Right", "Orthographic view from right", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(BottomView, "Bottom", "Orthographic view from bottom", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(BackView, "Back", "Orthographic view from back", EUserInterfaceActionType::Button, FInputChord());
	

	UI_COMMAND(SelectionLock, "Lock Selection", "", EUserInterfaceActionType::Button, FInputChord(EKeys::X));
	UI_COMMAND(FocusOnSelection, "Focus the viewport on the current selection", "", EUserInterfaceActionType::Button, FInputChord(EModifierKey::Control, EKeys::F));
	
	UI_COMMAND(LinkBones, "Link Bones", "Link the source bone to the target bone", EUserInterfaceActionType::Button, FInputChord(EModifierKey::Control, EKeys::L));
	UI_COMMAND(SourceSelection, "Source Hips Selection", "Source Hips Selection", EUserInterfaceActionType::Button, FInputChord(EModifierKey::Shift, EKeys::H));
	UI_COMMAND(MapBones, "Mapping Editor", "Edit the bone mapping from source to target", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(Characterise, "Characterisation Editor", "Edit the bone mapping from source to target", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(License, "License Details", "Modify License Details", EUserInterfaceActionType::Button, FInputChord());

	/** As two commands cannot have the same key; this command wraps both 
	  * DeletePrimitive and DeleteConstraint so the user can delete whatever is selected 
	  */
	UI_COMMAND(DeleteSelected, "Delete selected primitive or constraint", "", EUserInterfaceActionType::Button, FInputChord(EKeys::Platform_Delete));
	
}

#undef LOCTEXT_NAMESPACE