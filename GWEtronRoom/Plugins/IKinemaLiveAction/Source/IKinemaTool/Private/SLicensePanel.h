/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include "IKinemaRigToolModule.h"
#include "Modules/ModuleManager.h"
#include "IKinemaRigTool.h"
#include "IKinemaRigToolStyle.h"
#include "IKinemaRigToolSharedData.h"
#include "LADataStreamModule.h"
#include "Internationalization/Text.h"




class SIKinemaLicenseDialog : public SCompoundWidget
{
public:
	SLATE_USER_ARGS(SIKinemaLicenseDialog)
	{}
	SLATE_END_ARGS()

		/** Constructs this widget with InArgs */
		void Construct(const FArguments& InArgs);

	/** Sets properties for the supplied AnimBlueprintFactory */
	bool ConfigureProperties();


private:


	/** Handler for when ok is clicked */
	FReply OkClicked();


	void CloseDialog(bool bWasPicked = false);


		/** Handler for when cancel is clicked */
	FReply CancelClicked();


	FReply OnKeyDown(const FGeometry& MyGeometry, const FKeyEvent& InKeyEvent) override;


	void OnTextCommitted(const FText& InputText, ETextCommit::Type CommitType);

	FText GetLicense() const;

	FText GetExpiration() const;

	FText GetOptions() const;

private:


	/** A pointer to the window that is asking the user to select a parent class */
	TWeakPtr<SWindow> PickerWindow;

	/** The container for the Parent Class picker */
	TSharedPtr<SVerticalBox> ParentClassContainer;

	/** The container for the target skeleton picker*/
	TSharedPtr<SVerticalBox> SkeletonContainer;

	/** Don't ask box */
	TSharedPtr<SCheckBox> DontAskBox;

	/** Store the license key*/
	FText LicenseKey;
	/** The selected class */
	TWeakObjectPtr<UClass> ParentClass;

	/** True if Ok was clicked */
	bool bOkClicked = false;

	bool bDontAsk;
	bool bChanged;

	bool isLicensed;
};

DECLARE_LOG_CATEGORY_EXTERN(LogIKinemaLicensing, Warning, All);

