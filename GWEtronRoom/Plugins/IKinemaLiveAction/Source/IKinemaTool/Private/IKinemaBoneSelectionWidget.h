/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/
#pragma once
#include "Widgets/SCompoundWidget.h"

DECLARE_DELEGATE_OneParam(FOnBoneSelectionChanged, FName);
DECLARE_DELEGATE_RetVal(FName, FGetSelectedIKBone);

class SIKinemaBoneSelectionWidget : public SCompoundWidget
{
	
public: 

	SLATE_BEGIN_ARGS( SIKinemaBoneSelectionWidget )
		:_Skeleton()
		,_Tooltip()
		,_OnBoneSelectionChanged()
		,_OnGetSelectedBone()
	{}

		/** Vertical alignment */
		SLATE_ARGUMENT(TWeakObjectPtr<UActorSkeleton>, Skeleton);
		/** Set tooltip attribute */
		SLATE_ARGUMENT(FText, Tooltip);

		/** set selected bone name */
		SLATE_EVENT(FOnBoneSelectionChanged, OnBoneSelectionChanged);

		/** get selected bone name **/
		SLATE_EVENT(FGetSelectedIKBone, OnGetSelectedBone);

	SLATE_END_ARGS();

	/**
	 * Construct this widget
	 *
	 * @param	InArgs	The declaration data for this widget
	 */
	void Construct( const FArguments& InArgs );

private: 

	// Storage object for bone hierarchy
	struct FBoneNameInfo
	{
		FBoneNameInfo(FName Name): BoneName(Name) {}

		FName BoneName;
		TArray<TSharedPtr<FBoneNameInfo>> Children;
	};

	// Creates the combo button menu when clicked
	TSharedRef<SWidget> CreateSkeletonWidgetMenu();
	// Using the current filter, repopulate the tree view
	void RebuildBoneList();
	// Make a single tree row widget
	TSharedRef<ITableRow> MakeTreeRowWidget(TSharedPtr<FBoneNameInfo> InInfo, const TSharedRef<STableViewBase>& OwnerTable);
	// Get the children for the provided bone info
	void GetChildrenForInfo(TSharedPtr<FBoneNameInfo> InInfo, TArray< TSharedPtr<FBoneNameInfo> >& OutChildren);
	// Called when the user changes the search filter
	void OnFilterTextChanged(const FText& InFilterText);
	void OnFilterTextCommitted(const FText& SearchText, ETextCommit::Type CommitInfo);
	// Called when the user selects a bone name
	void OnSelectionChanged(TSharedPtr<FBoneNameInfo>, ESelectInfo::Type SelectInfo);
	// Gets the current bone name, used to get the right name for the combo button
	FText GetCurrentBoneName() const;

	// Tree view used in the button menu
	TSharedPtr<STreeView<TSharedPtr<FBoneNameInfo>>> TreeView;

	// Tree info entries for bone picker
	TArray<TSharedPtr<FBoneNameInfo>> SkeletonTreeInfo;
	// Mirror of SkeletonTreeInfo but flattened for searching
	TArray<TSharedPtr<FBoneNameInfo>> SkeletonTreeInfoFlat;

	// Base combo button 
	TSharedPtr<SComboButton> BonePickerButton;

	// Skeleton to search
	UActorSkeleton* TargetSkeleton;

	// Text to filter bone tree with
	FText FilterText;

	// delegates
	FOnBoneSelectionChanged OnBoneSelectionChanged;
	FGetSelectedIKBone OnGetSelectedBone;
};

