/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaRigToolSharedData.h"
#include "IKinemaRigToolModule.h"
#include "EditorSupportDelegates.h"
#include "ScopedTransaction.h"
#include "IKinemaRigToolEdSkeletalMeshComponent.h"
#include "IKinemaRigToolSharedData.h"
#include "PhysicsEngine/BodySetup.h"
#include "Engine/CollisionProfile.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Engine/StaticMesh.h"
#include "AssetToolsModule.h"
#include "MocapActorSourceFactory.h"
#include "IKinemaDefWrappers/IKinemaBoneDefWrapper.h"
#include "IKinemaDefWrappers/IKinemaTaskDefWrapper.h"
#include "IKinemaDefWrappers/IKinemaCoMTaskDefWrapper.h"
#include "IKinemaDefWrappers/IKinemaSourceBoneWrapper.h"
#include "PhysicsEngine/PhysicsAsset.h"
//#include "LADataStreamInterface.h"
#include "AnimNode_LADataStream.h"
#include "IAssetTools.h" 
#include "ILiveLinkClient.h"
#include "LiveLinkClient.h"
#include "LiveLinkMessageBusSource.h"
#include "LiveLinkInterfaceClasses.h"
#include "ILiveLinkModule.h"
#include "LiveLinkRetargetAsset.h"
#include "IKinemaLiveLinkRetarget.h"
#include "Features/IModularFeatures.h"

#define LOCTEXT_NAMESPACE "IKinemaRigToolShared"

TMap<FName, FTemplate> FIKinemaRigToolSharedData::Templates;

FIKinemaRigToolSharedData::FIKinemaRigToolSharedData()
	: IKinemaRig(nullptr), EditorSkelComp(nullptr), EditorSkelMesh(nullptr)
	, EditorFloorComp(nullptr), PreviewScene(FPreviewScene::ConstructionValues().ShouldSimulatePhysics(true)), NewBodyResponse()
	, CopiedBodySetup(nullptr)
	, CopiedConstraintTemplate(nullptr), bIsCoMSelected(false)
	, bInsideSelChange(false), AverageBoneLength(0)
{
	// Editor variables
	BodyEdit_MeshViewMode = PRM_Solid;
	ConstraintEdit_MeshViewMode = PRM_Solid;
	Sim_MeshViewMode = PRM_Solid;


	EditingMode = PEM_BodyEdit;

	bShowHierarchy = false;
	bShowNonActive = true;
	bShowConstraints = true;
	bDrawGround = true;
	bShowAnimSkel = true;
	bShowCollisionShapes = false;
	bSelectionLock = false;
	bRunningSimulation = false;
	bIsPlaying = false;
	bManipulating = false;
	// Construct mouse handle
	MouseHandle = NewObject<UPhysicsHandleComponent>();
	//Recorder.bRecordLocalToWorld = true;
	Templates = FTemplate::LoadTemplates();
	DefaultTemplates = FTemplate::LoadDefaults();
}

FIKinemaRigToolSharedData::~FIKinemaRigToolSharedData()
{
}

void FIKinemaRigToolSharedData::Initialize()
{
	EditorSkelComp = nullptr;

	USkeletalMesh * PreviewMesh = nullptr;
	if (IKinemaRig->Skeleton)
	{
		PreviewMesh = IKinemaRig->Skeleton->GetPreviewMesh(true);
	}
	if (PreviewMesh == nullptr)
	{
		// Fall back to the default skeletal mesh in the EngineMeshes package.
		// This is statically loaded as the package is likely not fully loaded
		// (otherwise, it would have been found in the above iteration).
		PreviewMesh = (USkeletalMesh*)StaticLoadObject(
			USkeletalMesh::StaticClass(), nullptr, TEXT("/Engine/EngineMeshes/SkeletalCube.SkeletalCube"), nullptr, LOAD_None, nullptr);
		check(PreviewMesh);

		FMessageDialog::Open(EAppMsgType::Ok, FText::Format(
			NSLOCTEXT("UnrealEd", "Error_IKinemaRigHasNoSkelMesh",
				"Warning: IKINEMARig has no skeleton!  For now, a simple default skeletal mesh ({0}) will be used.  You can fix this by opening IKINEMARigTool, selecting the appropriate skeletal mesh in the content browser, and using (Asset -> Change Mesh) before saving this asset."),
			FText::FromString(PreviewMesh->GetFullName())));
	}

	EditorSkelMesh = PreviewMesh;

	// Create SkeletalMeshComponent for rendering skeletal mesh
	EditorSkelComp = NewObject<UIKinemaRigToolEdSkeletalMeshComponent>();
	EditorSkelComp->SharedData = this;
	EditorSkelComp->SetAnimationMode(EAnimationMode::Type::AnimationSingleNode);

	//
	// first disable collision first to avoid creating physics body
	EditorSkelComp->SetCollisionProfileName(UCollisionProfile::BlockAll_ProfileName);


	// Create floor component
	UStaticMesh* FloorMesh = LoadObject<UStaticMesh>(nullptr, TEXT("/Engine/EditorMeshes/PhAT_FloorBox.PhAT_FloorBox"), nullptr, LOAD_None, nullptr);
	check(FloorMesh);

	EditorFloorComp = NewObject<UStaticMeshComponent>();
	EditorFloorComp->SetStaticMesh(FloorMesh);
	EditorFloorComp->SetRelativeScale3D(FVector(4.f));

	PreviewScene.AddComponent(EditorSkelComp, FTransform::Identity);
	PreviewScene.AddComponent(EditorFloorComp, FTransform::Identity);

	//EditorSkelComp->SetSkeletalMesh(EditorSkelMesh);
	EditorSkelComp->SetSkeletalMesh(PreviewMesh);
	//Compute average bone length
	AverageBoneLength = 10.f;
	if (IKinemaRig->Skeleton && EditorSkelComp)
	{
		float length(0.f);
		for (int32 i = 0; i < IKinemaRig->SolverDef.Bones.Num(); ++i)
		{
			const auto& Bone = IKinemaRig->SolverDef.Bones[i];

			int32 BoneIndex = EditorSkelComp->GetBoneIndex(Bone.Name);
			if (BoneIndex != INDEX_NONE)
			{
				const int32 ParentIndex = IKinemaRig->Skeleton->GetReferenceSkeleton().GetParentIndex(BoneIndex);
				FVector Start, End;

				FTransform WorldTransforms = EditorSkelComp->GetComponentSpaceTransforms()[BoneIndex] * EditorSkelComp->GetComponentTransform();
				if (ParentIndex >= 0)
				{
					Start = (EditorSkelComp->GetComponentSpaceTransforms()[ParentIndex] * EditorSkelComp->GetComponentTransform()).GetLocation();
					End = WorldTransforms.GetLocation();
				}
				else
				{
					Start = FVector::ZeroVector;
					End = WorldTransforms.GetLocation();
				}

				length += (End - Start).Size();
			}
			bShowCollisionShapes = bShowCollisionShapes || (Bone.Characterisation != EIKinemaCharacterDefinition::ENONE);
		}

		AverageBoneLength = length / IKinemaRig->SolverDef.Bones.Num();
	}

	// Register handle component
	MouseHandle->RegisterComponentWithWorld(PreviewScene.GetWorld());
	bIsPlaying = false;

	// Support undo/redo
	IKinemaRig->SetFlags(RF_Transactional);


	//Apply Saved match pose to the loaded SkelMesh Component through SkelControllers.
	for (auto bone : IKinemaRig->SolverDef.Bones)
	{
		int32 BoneIndex = EditorSkelComp->GetBoneIndex(bone.Name);
		FAnimNode_ModifyBone* SkelControl = NULL;
		if (BoneIndex >= 0)
		{
			//Get the skeleton control manipulating this bone
			const FName BoneName = EditorSkelComp->SkeletalMesh->RefSkeleton.GetBoneName(BoneIndex);
			SkelControl = &(EditorSkelComp->PreviewInstance->ModifyBone(BoneName));
		}
		if (SkelControl)
		{
			SkelControl->Rotation = bone.MatchPose.GetRotation().Rotator();
			SkelControl->Translation = bone.MatchPose.GetTranslation();
		}
	}

	EditorSkelComp->Stop();

	GenerateIKShapes();
}

void FIKinemaRigToolSharedData::GenerateIKShapes()
{
	UPhysicsAsset* PhysicsAsset = EditorSkelComp->GetPhysicsAsset();
	if (PhysicsAsset)
	{
		for (UBodySetup* body : PhysicsAsset->SkeletalBodySetups)
		{
			int32 BoneIndex = EditorSkelComp->GetBoneIndex(body->BoneName);
			auto IKRigBoneIndex = IKinemaRig->SolverDef.FindBoneIndex(body->BoneName);
			if (IKRigBoneIndex != INDEX_NONE && IKinemaRig->SolverDef.Bones[IKRigBoneIndex].SelfColision.CollisionShape.Shape == EIKNOTSET)
			{
				auto& IKBone = IKinemaRig->SolverDef.Bones[IKRigBoneIndex];
				FKAggregateGeom* AggGeom = &body->AggGeom;
				for (int32 j = 0; j < AggGeom->SphereElems.Num(); ++j)
				{
					FTransform ElemTM = AggGeom->SphereElems[j].GetTransform();
					IKBone.SelfColision.CollisionShape.Shape = EIKSPHERE;
					IKBone.SelfColision.CollisionShape.LocalOffset = ElemTM;
					IKBone.SelfColision.CollisionShape.Radius = AggGeom->SphereElems[j].Radius;
				}

				for (int32 j = 0; j < AggGeom->BoxElems.Num(); ++j)
				{
					FTransform ElemTM = AggGeom->BoxElems[j].GetTransform();
					IKBone.SelfColision.CollisionShape.Shape = EIKBOX;
					IKBone.SelfColision.CollisionShape.LocalOffset = ElemTM;
					IKBone.SelfColision.CollisionShape.BoxHalfLengths = FVector(AggGeom->BoxElems[j].X, AggGeom->BoxElems[j].Y, AggGeom->BoxElems[j].Z);
				}

				for (int32 j = 0; j < AggGeom->SphylElems.Num(); ++j)
				{
					FTransform ElemTM = AggGeom->SphylElems[j].GetTransform();
					IKBone.SelfColision.CollisionShape.Shape = EIKCAPSULE;
					IKBone.SelfColision.CollisionShape.LocalOffset = ElemTM;
					IKBone.SelfColision.CollisionShape.Radius = AggGeom->SphylElems[j].Radius;
					IKBone.SelfColision.CollisionShape.HalfLength = (AggGeom->SphylElems[j].Length + 2 * AggGeom->SphylElems[j].Radius) * 0.5f;
				}
				IKBone.SelfColision.CollisionShape.LocalOffset.SetRotation(IKBone.SelfColision.CollisionShape.LocalOffset.GetRotation()*FQuat(FVector(0, 1, 0), FMath::DegreesToRadians(90)));
			}

		}
	}
}

FIKinemaRigToolSharedData::EIKinemaRigToolRenderMode FIKinemaRigToolSharedData::GetCurrentMeshViewMode()
{
	if (bRunningSimulation)
	{
		return Sim_MeshViewMode;
	}
	else if (EditingMode == PEM_BodyEdit)
	{
		return BodyEdit_MeshViewMode;
	}
	else
	{
		return ConstraintEdit_MeshViewMode;
	}
}

void FIKinemaRigToolSharedData::HitBone(int32 BodyIndex, bool bGroupSelect /* = false*/, bool bGroupSelectRemove /* = true */)
{
	if (EditingMode == PEM_BodyEdit && !bSelectionLock && BodyIndex != INDEX_NONE)
	{
		FSelection Selection(BodyIndex);
		SetSelectedBody(&Selection, bGroupSelect, bGroupSelectRemove);
	}
}

void FIKinemaRigToolSharedData::HitConstraint(int32 ConstraintIndex, bool bGroupSelect)
{
	if (EditingMode == PEM_ConstraintEdit && !bSelectionLock)
	{
		SetSelectedConstraint(ConstraintIndex, bGroupSelect);
	}
}

void FIKinemaRigToolSharedData::HitCoMConstraint(bool bGroupSelect)
{
	if (EditingMode == PEM_ConstraintEdit && !bSelectionLock)
	{
		SetSelectedCoMConstraint(bGroupSelect);
	}
}

void FIKinemaRigToolSharedData::HitSourceBone(int32 BodyIndex, bool bGroupSelect)
{
	if (EditingMode == PEM_BodyEdit && !bSelectionLock)
	{
		//TODO: Implementing Selection approach
		SetSelectedSourceBone(BodyIndex, bGroupSelect);
	}
}

void FIKinemaRigToolSharedData::RefreshIKinemaRigAssetChange(const UIKinemaRig* InPhysAsset)
{
	if (InPhysAsset)
	{
		FEditorSupportDelegates::RedrawAllViewports.Broadcast();
	}
}

void FIKinemaRigToolSharedData::SetSelectedBody(const FSelection* Body, bool bGroupSelect /*= false*/, bool bGroupSelectRemove /* = true */)
{
	if (bInsideSelChange)
	{
		return;
	}

	if (bGroupSelect == false)
	{
		SelectedBodies.Empty();
	}

	if (Body)
	{
		bool bAlreadySelected = false;
		//unselect if already selected
		for (int32 i = 0; i < SelectedBodies.Num(); ++i)
		{
			if (SelectedBodies[i] == *Body)
			{
				if (bGroupSelectRemove)
				{
					SelectedBodies.RemoveAt(i);
				}
				bAlreadySelected = true;
				break;
			}
		}

		if (bAlreadySelected == false)
		{
			SelectedBodies.AddUnique(*Body);
		}
	}

	if (SelectedBodies.Num() != 0)
	{

		check(GetSelectedBody() && GetSelectedBody()->Index >= 0 && GetSelectedBody()->Index < IKinemaRig->SolverDef.Bones.Num());



		// Set properties dialog to display selected bone (or bone instance) info.
		TArray<UObject*> Objs;
		for (int i = 0; i < SelectedBodies.Num(); ++i)
		{
			UIKinemaBoneDefWrapper*  bone = FindObject<UIKinemaBoneDefWrapper>(IKinemaRig->GetOutermost(), *(IKinemaRig->SolverDef.Bones[SelectedBodies[i].Index].Name.ToString()));

			if (!bone)
			{
				bone = NewObject<UIKinemaBoneDefWrapper>(IKinemaRig->GetOutermost(), IKinemaRig->SolverDef.Bones[SelectedBodies[i].Index].Name);
				bone->SetFlags(bone->GetFlags() | RF_Standalone);
			}
			bone->BoneDef = IKinemaRig->SolverDef.Bones[SelectedBodies[i].Index];
			bone->BonePtr = &IKinemaRig->SolverDef.Bones[SelectedBodies[i].Index];
			bone->rig = IKinemaRig;

			//Don't show anything expect what we are looking for

			Objs.Add(bone);
		}

		GroupSelectionChangedEvent.Broadcast(Objs);
	}
	else
	{
		//Show the IKinemaRig data
		TArray<UObject*> Objs;
		Objs.Add(IKinemaRig);
		GroupSelectionChangedEvent.Broadcast(Objs);
	}

	//bInsideSelChange = true;
	//HierarchySelectionChangedEvent.Broadcast();	//TODO: disable for now
	PreviewChangedEvent.Broadcast();
}

void FIKinemaRigToolSharedData::SetSelectedSourceBone(int32 SourceBoneIndex, bool bGroupSelect)
{
	if (bInsideSelChange)
	{
		return;
	}

	if (bGroupSelect == false)
	{
		SelectedSourceBodies.Empty();
	}

	if (SourceBoneIndex != INDEX_NONE)
	{
		bool bAlreadySelected = false;
		for (int32 i = 0; i < SelectedSourceBodies.Num(); ++i)
		{
			if (SelectedSourceBodies[i].Index == SourceBoneIndex)
			{
				bAlreadySelected = true;
				SelectedSourceBodies.RemoveAt(i);
				break;
			}
		}

		if (bAlreadySelected == false)
		{
			FSelection SourceBody(SourceBoneIndex);
			SelectedSourceBodies.AddUnique(SourceBody);
		}
	}

	TArray<UObject*> Objs;
	if (SelectedSourceBodies.Num() > 0)
	{

		for (int i = 0; i < SelectedSourceBodies.Num(); ++i)
		{
			FMocapBone& sourceBone = IKinemaRig->Actor->GetBone(SelectedSourceBodies[i].Index);
			UIKinemaSourceBoneWrapper*  bone = FindObject<UIKinemaSourceBoneWrapper>(IKinemaRig->Actor->GetOutermost(), *(sourceBone.Name.ToString()));

			if (!bone)
			{
				if (IKinemaRig->Actor)
				{
					bone = NewObject<UIKinemaSourceBoneWrapper>(IKinemaRig->Actor->GetOutermost(), sourceBone.Name);
				}
				bone->SetFlags(bone->GetFlags() | RF_Transactional);
			}
			bone->BoneDef = sourceBone;
			bone->rig = IKinemaRig;

			selected = bone->BoneDef;

			//Don't show anything expect what we are looking for

			Objs.Add(bone);
		}
	}
	else
	{
		Objs.Add(IKinemaRig);
	}
	GroupSelectionChangedEvent.Broadcast(Objs);
	PreviewChangedEvent.Broadcast();
}

void FIKinemaRigToolSharedData::SetSelectedConstraint(int32 ConstraintIndex, bool bGroupSelect /*= false*/)
{
	if (bInsideSelChange)
	{
		return;
	}

	if (bGroupSelect == false)
	{
		SelectedConstraints.Empty();
	}

	if (ConstraintIndex != INDEX_NONE)
	{
		bool bAlreadySelected = false;
		for (int32 i = 0; i < SelectedConstraints.Num(); ++i)
		{
			if (SelectedConstraints[i].Index == ConstraintIndex)
			{
				bAlreadySelected = true;
				SelectedConstraints.RemoveAt(i);
				break;
			}
		}

		if (bAlreadySelected == false)
		{
			FSelection Constraint(ConstraintIndex);
			SelectedConstraints.AddUnique(Constraint);
		}
	}


	//check(GetSelectedBody() && GetSelectedBody()->Index >= 0 && GetSelectedBody()->Index < IKinemaRig->SolverDef.Bones.Num());



	// Set properties dialog to display selected bone (or bone instance) info.
	TArray<UObject*> Objs;
	if (SelectedConstraints.Num() > 0)
	{
		for (int i = 0; i < SelectedConstraints.Num(); ++i)
		{

			if (ConstraintIndex != INDEX_NONE)
			{
				UIKinemaTaskDefWrapper*  task = FindObject<UIKinemaTaskDefWrapper>(IKinemaRig->GetOutermost(), *(IKinemaRig->SolverDef.Tasks[SelectedConstraints[i].Index].Name.ToString()));
				if (!task)
				{
					task = NewObject<UIKinemaTaskDefWrapper>(IKinemaRig->GetOutermost()/*GetTransientPackage()*/, IKinemaRig->SolverDef.Tasks[SelectedConstraints[i].Index].Name);
					task->SetFlags(task->GetFlags() | RF_Standalone);
				}

				task->TaskDef = IKinemaRig->SolverDef.Tasks[SelectedConstraints[i].Index];
				task->TaskPtr = &IKinemaRig->SolverDef.Tasks[SelectedConstraints[i].Index];
				task->rig = IKinemaRig;

				//Don't show anything expect what we are looking for

				Objs.Add(task);
			}
		}
	}
	else
	{
		//Show the IKinemaRig data
		Objs.Add(IKinemaRig);
	}
	GroupSelectionChangedEvent.Broadcast(Objs);

	//bInsideSelChange = true;
	//HierarchySelectionChangedEvent.Broadcast();	//TODO: disable for now
	PreviewChangedEvent.Broadcast();
}

void FIKinemaRigToolSharedData::SetSelectedCoMConstraint(bool bGroupSelect /*= false*/)
{
	if (bInsideSelChange)
	{
		return;
	}


	if (bGroupSelect == false)
	{
		SelectedConstraints.Empty();
	}

	bIsCoMSelected = true;

	// Set properties dialog to display selected bone (or bone instance) info.
	TArray<UObject*> Objs;
	//for (int i = 0; i<SelectedBodies.Num(); ++i)

	UIKinemaCoMTaskDefWrapper* task = FindObject<UIKinemaCoMTaskDefWrapper>(IKinemaRig->GetOutermost(), TEXT("CoMSettings"));
	if (!task)
	{
		task = NewObject<UIKinemaCoMTaskDefWrapper>(IKinemaRig->GetOutermost()/*GetTransientPackage()*/, TEXT("CoMSettings"));
		task->SetFlags(task->GetFlags() | RF_Standalone);
		//EditorSimOptions = NewObject<UBoneDef>(GetTransientPackage(), TEXT("Test"));
		task->TaskDef = IKinemaRig->SolverDef.COM;
		task->TaskPtr = &IKinemaRig->SolverDef.COM;
		task->rig = IKinemaRig;
	}
	//Don't show anything expect what we are looking for

	Objs.Add(task);

	GroupSelectionChangedEvent.Broadcast(Objs);

	//bInsideSelChange = true;
	//HierarchySelectionChangedEvent.Broadcast();	//TODO: disable for now
	PreviewChangedEvent.Broadcast();
}


void FIKinemaRigToolSharedData::CopyBody()
{
	check(SelectedBodies.Num() == 1);
	//TODO: Copy Segment properties.
	CopiedBodySetup = &IKinemaRig->SolverDef.Bones[GetSelectedBody()->Index];
}

void FIKinemaRigToolSharedData::PasteBodyProperties()
{
	// Can't do this while simulating!
	if (bRunningSimulation)
	{
		return;
	}

	// Must have two valid bodies (which are different)
	if (CopiedBodySetup == NULL)
	{
		return;
	}

	const FScopedTransaction Transaction(NSLOCTEXT("IKinemaRigTool", "PasteBodyProperties", "Paste Body Properties"));

	for (int32 i = 0; i < SelectedBodies.Num(); ++i)
	{
		// Copy setup/instance properties - based on what we are viewing.

		FIKinemaBoneDef* ToBodySetup = &IKinemaRig->SolverDef.Bones[GetSelectedBody()->Index];
		FIKinemaBoneDef const * FromBodySetup = CopiedBodySetup;
		IKinemaRig->Modify();

		ToBodySetup->CopyBoneDefPropertiesFrom(FromBodySetup);
		UIKinemaBoneDefWrapper*  bone = FindObject<UIKinemaBoneDefWrapper>(IKinemaRig->GetOutermost(), *(IKinemaRig->SolverDef.Bones[SelectedBodies[i].Index].Name.ToString()));
		if (bone)
		{
			bone->Modify();
			bone->BoneDef = IKinemaRig->SolverDef.Bones[SelectedBodies[i].Index];
			bone->BonePtr = ToBodySetup;

		}

	}
	SetSelectedBody(NULL);	//paste can change the primitives on our selected bodies. There's probably a way to properly update this, but for now just deselect
	PreviewChangedEvent.Broadcast();
}

void FIKinemaRigToolSharedData::ImportActor(EMocapServer ServerType, const FString & address, const FString & subject, const FString & templateName, const int port, const float importScale, bool IsYUp /*= false*/, bool ZeroRotations /* = false*/)
{
	//Create the connection
	int numRetries = 0;
	FString serverSocket = address + FString::FromInt(port);
	IDataStreamInterface* DataStream = IDataStreamInterface::Get(serverSocket, ServerType);
	FLADataStreamToSkeletonBinding mSkeletonBinding;
	//establish
	auto LiveLinkClient = &IModularFeatures::Get().GetModularFeature<ILiveLinkClient>(ILiveLinkClient::ModularFeatureName);

	const FLiveLinkSubjectFrame * frame = LiveLinkClient->GetSubjectData(FName(*subject));

	if (ServerType != LiveLink)
	{
		if (!DataStream)
		{

			(IDataStreamInterface::Remove(DataStream));
			return;
		}

		while (numRetries < 3 && DataStream->Connect(address, port) == ELAResult::ELAError)
		{
			numRetries++;
		}
		if (numRetries >= 3 && !DataStream->isConnected())
		{
			FMessageDialog::Open(EAppMsgType::Ok,
				NSLOCTEXT("UnrealEd", "Error_LiveActionConnection",
					"Error: Could not connect to Mocap stream server. Make sure the IP address and port number are correct and try again please."));
			(IDataStreamInterface::Remove(DataStream));
			return;
		}
		//Impor
		mSkeletonBinding.bIsRetargeting = true;
		numRetries = 0;
		DataStream->GetFrame();
		while (!mSkeletonBinding.BindToSkeleton(DataStream, subject) && numRetries < 10)
		{
			numRetries++;
			FPlatformProcess::Sleep(0.1f);
		}
		if (numRetries >= 10 && !mSkeletonBinding.IsBound())
		{
			FMessageDialog::Open(EAppMsgType::Ok,
				NSLOCTEXT("UnrealEd", "Error_LiveActionSkeletonNotBound",
					"Error: Could not Mocap stream to character. Please try importing again"));
			(IDataStreamInterface::Remove(DataStream));
			return;
		}
	}

	DeleteAllConstraints();

	//Populate The Actor Skeleton
	if (!IKinemaRig->Actor)
	{
		//Create MocapActor Asset
		FAssetToolsModule& AssetToolsModule = FModuleManager::Get().LoadModuleChecked<FAssetToolsModule>("AssetTools");
		FString Name;
		FString PackageName;

		UMocapActorSourceFactory* RigFactory = NewObject<UMocapActorSourceFactory>();
		// Get a unique name for the rig
		const FString DefaultSuffix = "_" + subject + TEXT("_MocapActor");
		AssetToolsModule.Get().CreateUniqueAssetName(IKinemaRig->GetPathName(), DefaultSuffix, /*out*/ PackageName, /*out*/ Name);
		const FString PackagePath = FPackageName::GetLongPackagePath(PackageName);
		//Remove the outer name (else name = RigName_RigName)
		Name.RemoveFromStart(IKinemaRig->GetName() + "_");

		TArray<UObject*> ObjectsToSync;
		if (UObject* NewAsset = AssetToolsModule.Get().CreateAsset(Name, PackagePath, UActorSkeleton::StaticClass(), RigFactory))
		{
			IKinemaRig->Actor = Cast<UActorSkeleton>(NewAsset);
		}
		else
		{
			IKinemaRig->Actor = NewObject<UActorSkeleton>(IKinemaRig->GetOutermost(), FName(*subject));
		}


	}
	else
	{
		IKinemaRig->Actor->Bones.Empty();
	}


	IKinemaRig->Actor->SourceTransform.SetIdentity();
	if (IsYUp && (ServerType != LiveLink))
	{
		IKinemaRig->Actor->SourceTransform.SetRotation(FQuat(FVector(1, 0, 0), FMath::DegreesToRadians(-90.f)));
	}

	if (ServerType == XSens)
	{
		IKinemaRig->Actor->SourceTransform.SetRotation(FQuat(FVector(0, 0, 1), FMath::DegreesToRadians(90.f)));
	}

	IKinemaRig->Actor->SubjectName = subject;
	IKinemaRig->Actor->ServerIP = address;
	IKinemaRig->Actor->ServerPort = port;
	IKinemaRig->Actor->ImportScale = importScale;
	IKinemaRig->Actor->ServerType = ServerType;
	IKinemaRig->SolverDef.SourceScale = importScale;

	if (ServerType == LiveLink)
	{
		if (!frame)
		{
			FMessageDialog::Open(EAppMsgType::Ok,
				NSLOCTEXT("UnrealEd", "Error_LiveActionSkeletonNotBound",
					"Error: Could not establish LiveLink to character. Please try importing again"));
			return;
		}
		for (int i = 0; i < frame->RefSkeleton.GetBoneNames().Num(); i++)
		{
			FName name;
			FName parentName;
			name = frame->RefSkeleton.GetBoneNames()[i];

			int32 index = frame->RefSkeleton.GetBoneParents()[i];
			if (index > -1)
			{
				parentName = frame->RefSkeleton.GetBoneNames()[index];
			}

			IKinemaRig->Actor->AddBone(name, parentName);
		}

		TArray<FTransform> Pose;
		Pose.Init(FTransform::Identity, frame->RefSkeleton.GetBoneNames().Num() * 2);
		FPlatformProcess::Sleep(0.5f);
		for (int i = 0; i < frame->RefSkeleton.GetBoneNames().Num(); i++)
		{
			FTransform global;
			FTransform local;
			if (i == 0)
			{
				local = frame->Transforms[0];
				global = local;
			}
			else
			{
				local = frame->Transforms[i];
				int globalInd = IKinemaRig->Actor->GetParentBoneIndex(frame->RefSkeleton.GetBoneNames()[i]) + frame->RefSkeleton.GetBoneNames().Num();
				global = local * Pose[IKinemaRig->Actor->GetParentBoneIndex(frame->RefSkeleton.GetBoneNames()[i]) + frame->RefSkeleton.GetBoneNames().Num()];
			}
			Pose[i] = local;
			Pose[i + frame->RefSkeleton.GetBoneNames().Num()] = global;
		}
		IKinemaRig->Actor->PopulateTransforms(Pose, ZeroRotations);
	}
	else
	{
		for (int i = 0; i < mSkeletonBinding.GetNumberOfBones(); i++)
		{
			FString name;
			FString parentName;
			DataStream->GetSegmentNameForSubject(subject, i, name);
			DataStream->GetSegmentParentNameForSubject(subject, name, parentName);
			IKinemaRig->Actor->AddBone(FName(*name), FName(*parentName));
		}

		TArray<FTransform> Pose;
		Pose.Init(FTransform::Identity, mSkeletonBinding.GetNumberOfBones() * 2);
		FPlatformProcess::Sleep(0.5f);
		mSkeletonBinding.UpdatePose(Pose);
		IKinemaRig->Actor->PopulateTransforms(Pose, ZeroRotations);
		IKinemaRig->Actor->DoFK(0);

		IDataStreamInterface::Remove(DataStream);
	}

	
	//AutoScale character
	AutoScaleSource();

	auto RootTransform = IKinemaRig->Actor->GetBone(0);
	auto trans = RootTransform.GlobalTransform.GetTranslation();
	trans = IKinemaRig->Actor->SourceTransform.TransformPosition(trans);
	trans = -trans; //Adjust X-Y plane
	trans.Z = 0;
	IKinemaRig->Actor->SourceTransform.SetTranslation(trans);


	//Adjust the Z position to match source to target
	FTransform ComponentToWorld = EditorSkelComp->GetComponentTransform();
	auto targetHips = (EditorSkelComp->GetComponentSpaceTransforms()[EditorSkelComp->GetBoneIndex(IKinemaRig->SolverDef.Bones[0].Name)] * ComponentToWorld).GetTranslation();
	targetHips /= IKinemaRig->Actor->ImportScale;
	auto hipsPos = (IKinemaRig->Actor->GetBone(0).GlobalTransform * IKinemaRig->Actor->SourceTransform).GetTranslation();
	hipsPos.Z = targetHips.Z;
	hipsPos = IKinemaRig->Actor->SourceTransform.InverseTransformPosition(hipsPos);
	IKinemaRig->Actor->GetBone(0).LocalTransform.SetTranslation(hipsPos);

	IKinemaRig->Actor->DoFK(0);

	TMap<FName, FTemplate> TemplatesToUse;
	bool bUsingDefaults = false;
	if (templateName.IsEmpty() || templateName == "None")
	{
		bUsingDefaults = true;
		TemplatesToUse = DefaultTemplates;
	}
	else
	{
		if (Templates.Contains(FName(*templateName)))
		{
			TemplatesToUse.Add(FName(*templateName), Templates[FName(*templateName)]);
		}
		else
		{
			//selected template not found we need to reupdate the map
			Templates = FTemplate::LoadTemplates();
			FMessageDialog::Open(EAppMsgType::Ok,
				NSLOCTEXT("UnrealEd", "Error_TemplateNotFound",
					"Template not found! Skeleton imported succesfully with default mapping!"));
			bUsingDefaults = true;
			TemplatesToUse = DefaultTemplates;
		}
	}

	//Map source to target bones
	for (const auto temp : TemplatesToUse)
	{
		const auto MatchTemplate = temp.Value;
		bool Matched = false;
		for (auto& sourceBone : IKinemaRig->Actor->Bones)
		{
			FString sourceBoneName = sourceBone.Name.ToString();
			sourceBoneName.RemoveFromStart(subject + "_");

			auto targets = MatchTemplate.GetTargets(sourceBoneName);
			if (targets.Num() == 0 && bUsingDefaults)
			{
				Matched = false;
				break;
			}
			for (auto& target : targets)
			{
				if (!target.IsEmpty())
				{
					int index = IKinemaRig->SolverDef.FindBoneIndex(FName(*target));
					if (index != INDEX_NONE)
					{
						Matched = true;
						IKinemaRig->SolverDef.Bones[index].SourceName = (sourceBone.Name);
						IKinemaRig->SolverDef.Bones[index].SourceIndex = (IKinemaRig->Actor->GetBoneIndex(sourceBone.Name));
					}
					else
					{
						Matched = false;
						break;
					}
				}
			}
		}
		if (Matched)
		{
			if (!bUsingDefaults)
			{
				IKinemaRig->Actor->TemplateName = temp.Key.ToString();
			}
			break;
		}
		else
		{
			for (auto& TargetBone : IKinemaRig->SolverDef.Bones)
			{
				TargetBone.SourceName = NAME_None;
				TargetBone.SourceIndex = INDEX_NONE;
			}
		}
	}

	for (auto& bone : IKinemaRig->SolverDef.Bones)
	{
		if (bone.SourceName != NAME_None)
		{
			auto& source = IKinemaRig->Actor->GetBone(bone.SourceName);
			LinkBones(bone, source);
		}
	}

	//	IKinemaRig->Actor->DoFK(0);

	const FScopedTransaction Transaction(NSLOCTEXT("IKinemaRigTool", "Save Bones Maps", "Link source bone to target"));
	IKinemaRig->Modify();
	IKinemaRig->PostLoadOrImport();
	IKinemaRig->UpdateVersion++;
	IKinemaRig->Actor->Modify();

}

void FIKinemaRigToolSharedData::ImportActor(EMocapServer ServerType, const FString & address, const FString & subject, const int port, const float importScale, bool IsYUp)
{
	//Create the connection
	int numRetries = 0;
	IDataStreamInterface* DataStream = IDataStreamInterface::Get(address, ServerType);
	auto LiveLinkClient = &IModularFeatures::Get().GetModularFeature<ILiveLinkClient>(ILiveLinkClient::ModularFeatureName);

	const FLiveLinkSubjectFrame * frame = LiveLinkClient->GetSubjectData(FName(*subject));

	if (ServerType != LiveLink)
	{
		if (!DataStream)
		{

			(IDataStreamInterface::Remove(DataStream));
			return;
		}

		while (numRetries < 3 && DataStream->Connect(address, port) == ELAResult::ELAError)
		{
			numRetries++;
		}
		if (numRetries >= 3 && !DataStream->isConnected())
		{
			FMessageDialog::Open(EAppMsgType::Ok,
				NSLOCTEXT("UnrealEd", "Error_LiveActionConnection",
					"Error: Could not connect to Mocap stream server. Make sure the IP address and port number are correct and try again please."));
			(IDataStreamInterface::Remove(DataStream));
			return;
		}
	}

	//Populate The Actor Skeleton
	if (!IKinemaRig->Actor)
	{
		//Create MocapActor Asset
		FAssetToolsModule& AssetToolsModule = FModuleManager::Get().LoadModuleChecked<FAssetToolsModule>("AssetTools");
		FString Name;
		FString PackageName;

		UMocapActorSourceFactory* RigFactory = NewObject<UMocapActorSourceFactory>();
		// Get a unique name for the rig
		const FString DefaultSuffix = TEXT("_MocapActor");
		AssetToolsModule.Get().CreateUniqueAssetName(IKinemaRig->GetPathName(), DefaultSuffix, /*out*/ PackageName, /*out*/ Name);
		const FString PackagePath = FPackageName::GetLongPackagePath(PackageName);
		TArray<UObject*> ObjectsToSync;
		if (UObject* NewAsset = AssetToolsModule.Get().CreateAsset(Name + DefaultSuffix, PackagePath, UActorSkeleton::StaticClass(), RigFactory))
		{
			IKinemaRig->Actor = Cast<UActorSkeleton>(NewAsset);
		}
		else
		{
			IKinemaRig->Actor = NewObject<UActorSkeleton>(IKinemaRig->GetOutermost(), FName(*(Name + DefaultSuffix)));
		}
	}
	else
	{
		IKinemaRig->Actor->Bones.Empty();
	}
	if (IsYUp)
	{
		IKinemaRig->Actor->SourceTransform.SetRotation(FQuat(FVector(1, 0, 0), FMath::DegreesToRadians(-90.f)));
	}

	IKinemaRig->Actor->SubjectName = "Rigid_Bodies";
	IKinemaRig->Actor->ServerIP = address;
	IKinemaRig->Actor->ServerPort = port;
	IKinemaRig->Actor->ImportScale = importScale;
	IKinemaRig->Actor->ServerType = ServerType;
	IKinemaRig->SolverDef.SourceScale = importScale;
	IKinemaRig->Actor->IsRigidBody = true;
	IKinemaRig->SolverDef.EnableShadowPosing = true;

	FPlatformProcess::Sleep(0.5f);
	if (ServerType != EMocapServer::LiveLink)
	{
		auto RigidBodies = DataStream->GetRigidBodyNames();

		//Hack to deal with Tracker RB streaming
		if (ServerType == EMocapServer::VICON)
		{
			DataStream->GetFrame();
			int Count = 0;
			DataStream->GetSubjectCount(Count);
			for (int i = 0; i < Count; i++)
			{
				FString RbName;
				int SegCount = -1;
				DataStream->GetSubjectName(i, RbName);
				DataStream->GetSegmentCountForSubject(RbName, SegCount);
				if (SegCount == 1)
				{
					RigidBodies.Add(RbName);
				}
			}
		}
		for (auto& RigidBody : RigidBodies)
		{
			FTransform trans;
			DataStream->GetRigidBodyTransform(RigidBody, trans);
			IKinemaRig->Actor->AddBone(FName(*RigidBody), NAME_None, trans);
		}
		IDataStreamInterface::Remove(DataStream);
	}
	else
	{
		if (!frame)
		{
			FMessageDialog::Open(EAppMsgType::Ok,
				NSLOCTEXT("UnrealEd", "Error_LiveActionSkeletonNotBound",
					"Error: Could not establish LiveLink to character. Please try importing again"));
			return;
		}

		if (frame->RefSkeleton.GetBoneNames().Num() != frame->Transforms.Num())
		{
			FMessageDialog::Open(EAppMsgType::Ok,
				NSLOCTEXT("UnrealEd", "Error_LiveActionSkeletonNotBound",
					"Error: Number of LiveLink bones and source transforms did not match"));
			return;
		}
		for (int i = 0; i < frame->RefSkeleton.GetBoneNames().Num(); i++)
		{
			FTransform trans;
			IKinemaRig->Actor->AddBone(FName(frame->RefSkeleton.GetBoneNames()[i]), NAME_None, frame->Transforms[i]);
		}
	}
	IKinemaRig->IsRigidBody = true;

}

void FIKinemaRigToolSharedData::AutoScaleSource()
{
	//In UE4 Up-Axis is always Z

	const auto hipsPos = (IKinemaRig->Actor->GetBone(0).GlobalTransform * IKinemaRig->Actor->SourceTransform).GetTranslation();
	//search for the point with the lowest Z value
	float lowestValue = hipsPos.Z;
	FMocapBone lowestBone;
	float legLength = 0.f;
	for (const auto& bone : IKinemaRig->Actor->Bones)
	{
		auto pos = (bone.GlobalTransform * IKinemaRig->Actor->SourceTransform).GetTranslation();

		if (pos.Z < lowestValue)
		{
			lowestValue = pos.Z;
			lowestBone = bone;
		}
	}
	//Calculate the length of the leg
	FMocapBone& bone = lowestBone;
	while (bone.ParentName != NAME_None)
	{
		legLength += bone.LocalTransform.GetTranslation().Size();
		IKinemaRig->Actor->LegBones.AddUnique(bone.Name);
		bone = IKinemaRig->Actor->GetBone(bone.ParentName);
	}
	IKinemaRig->Actor->LegLength = legLength;

	IKinemaRig->Actor->Modify();
	//Do the same for the target	
	FTransform ComponentToWorld = EditorSkelComp->GetComponentTransform();
	const auto targetHips = (EditorSkelComp->GetComponentSpaceTransforms()[EditorSkelComp->GetBoneIndex(IKinemaRig->SolverDef.Bones[0].Name)] * ComponentToWorld).GetTranslation();

	float lowestTargetValue = targetHips.Z;
	for (const auto& TargetBone : IKinemaRig->SolverDef.Bones)
	{
		const int32 BoneIndex = EditorSkelComp->GetBoneIndex(TargetBone.Name);
		if (BoneIndex != INDEX_NONE)
		{
			auto pos = (EditorSkelComp->GetComponentSpaceTransforms()[BoneIndex] * ComponentToWorld).GetTranslation();
			if (pos.Z < lowestTargetValue)
			{
				lowestTargetValue = pos.Z;
			}

		}
	}
	if (FMath::IsNearlyZero(FMath::Abs(hipsPos.Z - lowestValue)))
	{
		return;
	}
	if (FMath::IsNearlyZero(FMath::Abs(targetHips.Z - lowestTargetValue)))
	{
		return;
	}
	IKinemaRig->Actor->ImportScale *= ((FMath::Abs(targetHips.Z - lowestTargetValue)) / (FMath::Abs(hipsPos.Z - lowestValue)*IKinemaRig->Actor->ImportScale));
	IKinemaRig->SolverDef.SourceScale = IKinemaRig->Actor->ImportScale;
}

void FIKinemaRigToolSharedData::ResetSelectedConstraint()
{
	//Reset constraint to its default settings
	FIKinemaTaskDef* task = &IKinemaRig->SolverDef.Tasks[GetSelectedConstraint()->Index];

	IKinemaRig->Modify();
	//Reset to defaults.
	if (task)
	{
		task->ResetToDefaults();

		UIKinemaTaskDefWrapper*  bone = FindObject<UIKinemaTaskDefWrapper>(IKinemaRig->GetOutermost(), *(task->Name.ToString()));
		if (bone)
		{
			bone->Modify();
			bone->TaskDef.ResetToDefaults();
		}
	}
}

void FIKinemaRigToolSharedData::ResetCoMConstraint()
{
	//Reset constraint to its default settings
	FIKinemaCOMDef* task = &IKinemaRig->SolverDef.COM;

	IKinemaRig->Modify();
	//Reset to defaults.
	if (task)
	{
		task->ResetToDefaults();

		UIKinemaCoMTaskDefWrapper*  bone = FindObject<UIKinemaCoMTaskDefWrapper>(IKinemaRig->GetOutermost(), TEXT("CoMSettings"));
		if (bone)
		{
			bone->Modify();
			bone->TaskDef.ResetToDefaults();
		}
	}
}


void FIKinemaRigToolSharedData::ResetSelectedBone()
{
	FIKinemaBoneDef* bone = &IKinemaRig->SolverDef.Bones[GetSelectedBody()->Index];

	IKinemaRig->Modify();
	//Reset to defaults.
	if (bone)
	{
		bone->ResetToDefaults();

		UIKinemaBoneDefWrapper*  boneWrapper = FindObject<UIKinemaBoneDefWrapper>(IKinemaRig->GetOutermost(), *(bone->Name.ToString()));
		if (boneWrapper)
		{
			boneWrapper->Modify();
			boneWrapper->BoneDef.ResetToDefaults();
		}
	}
}

void FIKinemaRigToolSharedData::CopyConstraint()
{
	check(SelectedConstraints.Num() == 1);
	CopiedConstraintTemplate = &IKinemaRig->SolverDef.Tasks[GetSelectedConstraint()->Index]; //PhysicsAsset->ConstraintSetup[GetSelectedConstraint()->Index];
}

void FIKinemaRigToolSharedData::PasteConstraintProperties()
{
	if (CopiedConstraintTemplate == NULL)
	{
		return;
	}
	const FScopedTransaction Transaction(NSLOCTEXT("IKinemaRigTool", "PasteConstraintProperties", "Paste Constraint Properties"));

	auto FromConstraintSetup = CopiedConstraintTemplate;

	for (int32 i = 0; i < SelectedConstraints.Num(); ++i)
	{
		// If we are showing instance properties - copy instance properties. If showing setup, just copy setup properties.
		auto* ToConstraintSetup = &IKinemaRig->SolverDef.Tasks[SelectedConstraints[i].Index];

		IKinemaRig->Modify();
		ToConstraintSetup->CopyTaskDefPropertiesFrom(FromConstraintSetup);
		UIKinemaTaskDefWrapper*  bone = FindObject<UIKinemaTaskDefWrapper>(IKinemaRig->GetOutermost(), *(IKinemaRig->SolverDef.Tasks[SelectedConstraints[i].Index].Name.ToString()));
		if (bone)
		{
			bone->Modify();
			bone->TaskDef = IKinemaRig->SolverDef.Tasks[SelectedConstraints[i].Index];
			bone->TaskPtr = ToConstraintSetup;

		}
	}
	PreviewChangedEvent.Broadcast();
}

void FIKinemaRigToolSharedData::DeleteBody()
{
	if (EditingMode != PEM_BodyEdit || !GetSelectedBody())
	{
		return;
	}
	auto DelBodyIndex = GetSelectedBody()->Index;
	//Find Children of the Bone. If it has a child don't delete it
	const auto& name = IKinemaRig->SolverDef.Bones[DelBodyIndex].Name;
	auto contains = IKinemaRig->SolverDef.Bones.ContainsByPredicate([&name](const FIKinemaBoneDef& bone) {
		return bone.ParentName == name; });
	if (contains && DelBodyIndex != 0)
	{
		FMessageDialog::Open(EAppMsgType::Ok, FText::Format(
			NSLOCTEXT("IKinemaRigTool", "Error_IKinemaRigDelHasChild",
				"Warning: Deletion of bone ({0}) is not possible due to children dependency in the rig skeleton hierarchy.  You can delete this bone and its children by select (Select Hierarchy Below -> Delete) from the context menu."),
			FText::FromString(name.ToString())));
		return;
	}

	const FScopedTransaction Transaction(NSLOCTEXT("IKinemaRigTool", "DeleteBody", "Delete Body"));

	DeleteBody(DelBodyIndex);
	if (DelBodyIndex == 0)
	{
		IKinemaRig->SolverDef.Bones[0].ParentName = NAME_None;
		// Set properties dialog to display selected bone (or bone instance) info.
		TArray<UObject*> Objs;
		UIKinemaBoneDefWrapper*  bone = FindObject<UIKinemaBoneDefWrapper>(IKinemaRig->GetOutermost(), *(IKinemaRig->SolverDef.Bones[0].Name.ToString()));
		if (!bone)
		{
			bone = NewObject<UIKinemaBoneDefWrapper>(IKinemaRig->GetOutermost(), IKinemaRig->SolverDef.Bones[0].Name);
			bone->SetFlags(bone->GetFlags() | RF_Transactional);
			bone->BoneDef = IKinemaRig->SolverDef.Bones[0];
			bone->BonePtr = &IKinemaRig->SolverDef.Bones[0];
			bone->rig = IKinemaRig;
		}
		//Don't show anything expect what we are looking for

		Objs.Add(bone);
		GroupSelectionChangedEvent.Broadcast(Objs);
	}
	SetSelectedBody(NULL);

	HierarchyChangedEvent.Broadcast();
	PreviewChangedEvent.Broadcast();
}

void FIKinemaRigToolSharedData::DeleteBody(int DelBodyIndex)
{
	if (DelBodyIndex == INDEX_NONE)
	{
		return;
	}
	if (&IKinemaRig->SolverDef.Bones[DelBodyIndex] == CopiedBodySetup)
	{
		CopiedBodySetup = NULL;
	}




	IKinemaRig->Modify();
	//Remove item from task array and The wrapper object
	UIKinemaBoneDefWrapper*  bone = FindObject<UIKinemaBoneDefWrapper>(IKinemaRig->GetOutermost(), *(IKinemaRig->SolverDef.Bones[DelBodyIndex].Name.ToString()));
	if (bone)
	{
		bone->Modify();
		DeleteObject(bone);
	}

	//Update BoneIndex for all tasks after delete
	//Bones array indices have changed.
	TMap<int32, FName> BoneNames;

	for (int32 i = IKinemaRig->SolverDef.Tasks.Num() - 1; i >= 0; i--)
	{
		auto& task = IKinemaRig->SolverDef.Tasks[i];

		FName TestName = IKinemaRig->SolverDef.Bones[task.BoneIndex].Name;
		if (task.BoneIndex >= DelBodyIndex)
		{
			BoneNames.Add(i, TestName);
		}
	}
	IKinemaRig->SolverDef.Bones.RemoveAt(DelBodyIndex);
	for (auto boneName : BoneNames)
	{
		FName name = boneName.Value;
		int boneIndex = IKinemaRig->SolverDef.Bones.IndexOfByPredicate([&name](const FIKinemaBoneDef& arg) {
			return name == arg.Name;
		});


		if (boneIndex == INDEX_NONE)
		{
			//Remove item from task array and The wrapper object
			UIKinemaTaskDefWrapper*  task = FindObject<UIKinemaTaskDefWrapper>(IKinemaRig->GetOutermost(), *(IKinemaRig->SolverDef.Tasks[boneName.Key].Name.ToString()));
			if (task)
			{
				bone->Modify();
				DeleteObject(task);
			}
			IKinemaRig->SolverDef.Tasks.RemoveAt(boneName.Key);
		}
		else
		{
			IKinemaRig->SolverDef.Tasks[boneName.Key].BoneIndex = boneIndex;
		}
	}

	IKinemaRig->PostLoad();
	IKinemaRig->UpdateVersion++;
}

void FIKinemaRigToolSharedData::DeleteObject(UObject* obj)
{
	if (obj && obj->IsValidLowLevel())
	{
		obj->ConditionalBeginDestroy(); //instantly clears UObject out of memory
	}
}

void FIKinemaRigToolSharedData::DeleteAllConstraints()
{

	//Save indices before delete because delete modifies our Selected array
	TArray<int32> Indices;
	for (int32 i = 0; i < IKinemaRig->SolverDef.Tasks.Num(); ++i)
	{
		Indices.Add(i);
	}

	Indices.Sort();

	//These are indices into an array, we must remove it from greatest to smallest so that the indices don't shift
	for (int32 i = Indices.Num() - 1; i >= 0; --i)
	{

		if (&IKinemaRig->SolverDef.Tasks[Indices[i]] == CopiedConstraintTemplate)
		{
			CopiedConstraintTemplate = NULL;
		}

		IKinemaRig->Modify();
		//Remove item from task array and The wrapper object
		UIKinemaTaskDefWrapper* bone = FindObject<UIKinemaTaskDefWrapper>(IKinemaRig->GetOutermost(), *(IKinemaRig->SolverDef.Tasks[Indices[i]].Name.ToString()));
		if (bone)
		{
			bone->Modify();
			DeleteObject(bone);
		}
		IKinemaRig->SolverDef.Tasks.RemoveAt(Indices[i]);
		IKinemaRig->PostLoad();
		IKinemaRig->UpdateVersion++;
	}

	SetSelectedConstraint(INDEX_NONE);

	HierarchyChangedEvent.Broadcast();
	PreviewChangedEvent.Broadcast();
}


void FIKinemaRigToolSharedData::DeleteCurrentConstraint()
{
	if (EditingMode != PEM_ConstraintEdit || !GetSelectedConstraint())
	{
		return;
	}

	const FScopedTransaction Transaction(NSLOCTEXT("IKinemaRigTool", "DeleteConstraint", "Delete Constraint"));

	//Save indices before delete because delete modifies our Selected array
	TArray<int32> Indices;
	for (int32 i = 0; i < SelectedConstraints.Num(); ++i)
	{
		Indices.Add(SelectedConstraints[i].Index);
	}

	Indices.Sort();

	//These are indices into an array, we must remove it from greatest to smallest so that the indices don't shift
	for (int32 i = Indices.Num() - 1; i >= 0; --i)
	{

		if (&IKinemaRig->SolverDef.Tasks[Indices[i]] == CopiedConstraintTemplate)
		{
			CopiedConstraintTemplate = NULL;
		}

		IKinemaRig->Modify();
		//Remove item from task array and The wrapper object
		UIKinemaTaskDefWrapper*  bone = FindObject<UIKinemaTaskDefWrapper>(IKinemaRig->GetOutermost(), *(IKinemaRig->SolverDef.Tasks[Indices[i]].Name.ToString()));
		if (bone)
		{
			bone->Modify();
			DeleteObject(bone);
		}
		IKinemaRig->SolverDef.Tasks.RemoveAt(Indices[i]);
		IKinemaRig->PostLoad();
		IKinemaRig->UpdateVersion++;
	}

	SetSelectedConstraint(INDEX_NONE);

	HierarchyChangedEvent.Broadcast();
	PreviewChangedEvent.Broadcast();
}


static void ConvertTransformToFromMaya(FTransform& Transform, bool convertTranslation = true)
{
	FVector Translation = Transform.GetTranslation();
	FQuat Rotation = Transform.GetRotation();

	Rotation.X = -Rotation.X;
	Rotation.Z = -Rotation.Z;
	Transform.SetRotation(Rotation);

	if (convertTranslation)
	{
		Translation.Y = -Translation.Y;
		Transform.SetTranslation(Translation);
	}
}


void FIKinemaRigToolSharedData::CalculateTaskOffsets(FIKinemaTaskDef& newTask, const FIKinemaBoneDef& bone)
{
	if (newTask.bLockOffset)
	{
		return;
	}
	auto sourceSel = GetSelectedSourceBody();
	auto source = sourceSel ? IKinemaRig->Actor->GetBone(sourceSel->Index) : bone.SourceName == NAME_None ? FMocapBone() : IKinemaRig->Actor->GetBone(bone.SourceName);

	if (newTask.SourceIndex == INDEX_NONE)
	{
		newTask.SourceIndex = bone.SourceIndex;
		newTask.SourceName = source.Name;
	}
	else
	{
		source = IKinemaRig->Actor->GetBone(newTask.SourceName);
	}
	FTransform ComponentToWorld = EditorSkelComp->GetComponentTransform();
	const int32 BoneIndex = EditorSkelComp->GetBoneIndex(bone.Name);

	if (BoneIndex != INDEX_NONE)
	{

		FTransform targetWorld = EditorSkelComp->GetComponentSpaceTransforms()[BoneIndex] * ComponentToWorld;
		FTransform sourceWorld = source.GlobalTransform *IKinemaRig->Actor->SourceTransform;
		auto srcInv = (sourceWorld.GetRotation()).Inverse();
		FVector scaledSource = sourceWorld.GetTranslation()*IKinemaRig->SolverDef.ImportScale *IKinemaRig->SolverDef.SourceScale;
		FVector POffset = ((targetWorld.GetTranslation()) - scaledSource) / IKinemaRig->SolverDef.SourceScale;
		POffset = srcInv * POffset;
		POffset.Y = -POffset.Y;
		newTask.TaskOffset.SetTranslation(POffset);

		//Rotation Offset
		FQuat ROffset = srcInv *  targetWorld.GetRotation();
		ROffset.X = -ROffset.X;
		ROffset.Z = -ROffset.Z;
		newTask.TaskOffset.SetRotation(ROffset);
	}
}

void FIKinemaRigToolSharedData::AddConstraint(EIKinemaRigToolTaskType TaskType)
{
	if (EditingMode != PEM_BodyEdit || (SelectedBodies.Num() != 1 && SelectedSourceBodies.Num() != 1))
	{
		return;
	}


	//Get teh name of the bone create default task settings for this.
	check(GetSelectedBody() && GetSelectedBody()->Index >= 0 && GetSelectedBody()->Index < IKinemaRig->SolverDef.Bones.Num());

	//Set properties dialog to display selected bone (or bone instance) info.
	auto& bone = IKinemaRig->SolverDef.Bones[GetSelectedBody()->Index];
	auto sourceSel = GetSelectedSourceBody();
	bool IgnoreSelection = false;
	if (sourceSel)
	{
		auto SourceName = IKinemaRig->Actor->GetBone(sourceSel->Index).Name;
		if (SourceName != bone.SourceName)
		{
			FMessageDialog::Open(EAppMsgType::Ok, FText::Format(
				NSLOCTEXT("LiveActionRigEditor", "Error_BoneMappSelectionMismatch",
					"Warning: Target bone {0} is mapped to {1}, while Source bone {2} is Selected.\nPlease review the mapping or cancel the source selection."),
				FText::FromName(bone.Name), FText::FromName(bone.SourceName), FText::FromName(SourceName)));
			return;
		}
	}
	if (!sourceSel  && bone.SourceName == NAME_None)
	{
		//Warn user and return
		FMessageDialog::Open(EAppMsgType::Ok,
			NSLOCTEXT("LiveActionRigEditor", "Error_BoneHasNoSource",
				"Error: Can not create a task on the specified bone. No source is selected or mapped to this bone."));
		return;
	}
	FIKinemaTaskDef newTask;
	newTask.InitializeValues();
	FString name = bone.Name.ToString() + "_task";
	newTask.Name = FName(*name);
	newTask.BoneIndex = GetSelectedBody()->Index;

	//Set up position tasks
	if ((int)TaskType < (int)PTT_Hip)
	{
		if (TaskType == PTT_Positional || TaskType == PTT_Dual)
		{
			newTask.HasPositionTask = true;
			newTask.PositionDepth = -1; //All the way to the root by default.
			newTask.PositionAsPoleObject = false;
			newTask.PositionDepth = -1;
			newTask.PositionDofX = true;
			newTask.PositionDofY = true;
			newTask.PositionDofZ = true;
			newTask.PositionPrecision = FVector(2.f);
			newTask.PositionPriority = 0;
			newTask.PositionWeight = FVector(60.f);
		}
		//Set up orientation task
		if (TaskType == PTT_Orientational || TaskType == PTT_Dual)
		{
			newTask.HasRotationTask = true;
			newTask.RotateDepth = -1;
			newTask.RotateDofX = true;
			newTask.RotateDofY = true;
			newTask.RotateDofZ = true;
			newTask.RotatePrecision = FVector(2.f);
			newTask.RotatePriority = 0;
			newTask.RotateWeight = FVector(50.f);
			newTask.TipOffset = FVector(0, 0, 0); //Set in view port.
		}
	}
	else
	{
		switch (TaskType)
		{
		case PTT_Hip:
			newTask.HasRotationTask = true;
			newTask.RotateDepth = -1;
			newTask.RotateDofX = true;
			newTask.RotateDofY = true;
			newTask.RotateDofZ = true;
			newTask.RotatePrecision = FVector(2.f);
			newTask.RotatePriority = 0;
			newTask.RotateWeight = FVector(30.f);
			newTask.TipOffset = FVector(0, 0, 0); //Set in view port.
			newTask.HasPositionTask = true;
			newTask.PositionDepth = -1; //All the way to the root by default.
			newTask.PositionAsPoleObject = false;
			newTask.PositionDepth = -1;
			newTask.PositionDofX = true;
			newTask.PositionDofY = true;
			newTask.PositionDofZ = true;
			newTask.PositionPrecision = FVector(2.f);
			newTask.PositionPriority = 0;
			newTask.PositionWeight = FVector(20.f);
			break;
		case PTT_Foot:
			newTask.HasRotationTask = true;
			newTask.RotateDepth = -1;
			newTask.RotateDofX = true;
			newTask.RotateDofY = true;
			newTask.RotateDofZ = true;
			newTask.RotatePrecision = FVector(2.f);
			newTask.RotatePriority = 0;
			newTask.RotateWeight = FVector(50.f);
			newTask.TipOffset = FVector(0, 0, 0); //Set in view port.
			newTask.HasPositionTask = true;
			newTask.PositionDepth = -1; //All the way to the root by default.
			newTask.PositionAsPoleObject = false;
			newTask.PositionDepth = -1;
			newTask.PositionDofX = true;
			newTask.PositionDofY = true;
			newTask.PositionDofZ = true;
			newTask.PositionPrecision = FVector(2.f);
			newTask.PositionPriority = 0;
			newTask.PositionWeight = FVector(60.f);
			break;
		case PTT_Hand:
			newTask.HasRotationTask = true;
			newTask.RotateDepth = -1;
			newTask.RotateDofX = true;
			newTask.RotateDofY = true;
			newTask.RotateDofZ = true;
			newTask.RotatePrecision = FVector(2.f);
			newTask.RotatePriority = 0;
			newTask.RotateWeight = FVector(30.f);
			newTask.TipOffset = FVector(0, 0, 0); //Set in view port.
			newTask.HasPositionTask = true;
			newTask.PositionDepth = -1; //All the way to the root by default.
			newTask.PositionAsPoleObject = false;
			newTask.PositionDepth = -1;
			newTask.PositionDofX = true;
			newTask.PositionDofY = true;
			newTask.PositionDofZ = true;
			newTask.PositionPrecision = FVector(2.f);
			newTask.PositionPriority = 0;
			newTask.PositionWeight = FVector(30.f);
			break;
		case PTT_Head:
			newTask.HasRotationTask = true;
			newTask.RotateDepth = -1;
			newTask.RotateDofX = true;
			newTask.RotateDofY = true;
			newTask.RotateDofZ = true;
			newTask.RotatePrecision = FVector(2.f);
			newTask.RotatePriority = 0;
			newTask.RotateWeight = FVector(30.f);
			newTask.TipOffset = FVector(0, -1.f, 0); //Set in view port.
			newTask.HasPositionTask = true;
			newTask.PositionDepth = -1; //All the way to the root by default.
			newTask.PositionAsPoleObject = false;
			newTask.PositionDepth = -1;
			newTask.PositionDofX = true;
			newTask.PositionDofY = true;
			newTask.PositionDofZ = true;
			newTask.PositionPrecision = FVector(2.f);
			newTask.PositionPriority = 0;
			newTask.PositionWeight = FVector(10.f);
			break;
		case PTT_Chest:
			newTask.HasRotationTask = true;
			newTask.RotateDepth = -1;
			newTask.RotateDofX = true;
			newTask.RotateDofY = true;
			newTask.RotateDofZ = true;
			newTask.RotatePrecision = FVector(2.f);
			newTask.RotatePriority = 0;
			newTask.RotateWeight = FVector(30.f);
			newTask.TipOffset = FVector(0, 0.f, 0); //Set in view port.
			newTask.HasPositionTask = true;
			newTask.PositionDepth = -1; //All the way to the root by default.
			newTask.PositionAsPoleObject = false;
			newTask.PositionDepth = -1;
			newTask.PositionDofX = true;
			newTask.PositionDofY = true;
			newTask.PositionDofZ = true;
			newTask.PositionPrecision = FVector(2.f);
			newTask.PositionPriority = 0;
			newTask.PositionWeight = FVector(30.f);
			break;
		case PTT_Knee:
			newTask.HasPositionTask = true;
			newTask.PositionDepth = -1; //All the way to the root by default.
			newTask.PositionAsPoleObject = true;
			newTask.PositionDepth = -1;
			newTask.PositionDofX = true;
			newTask.PositionDofY = true;
			newTask.PositionDofZ = true;
			newTask.PositionPrecision = FVector(2.f);
			newTask.PositionPriority = 0;
			newTask.PositionWeight = FVector(10.f);
			break;
		case PTT_Elbow:
			newTask.HasRotationTask = true;
			newTask.RotateDepth = -1;
			newTask.RotateDofX = true;
			newTask.RotateDofY = true;
			newTask.RotateDofZ = true;
			newTask.RotatePrecision = FVector(2.f);
			newTask.RotatePriority = 0;
			newTask.RotateWeight = FVector(1.f);
			newTask.TipOffset = FVector(0.f);
			newTask.HasPositionTask = true;
			newTask.PositionDepth = -1; //All the way to the root by default.
			newTask.PositionAsPoleObject = false;
			newTask.PositionDepth = -1;
			newTask.PositionDofX = true;
			newTask.PositionDofY = true;
			newTask.PositionDofZ = true;
			newTask.PositionPrecision = FVector(2.f);
			newTask.PositionPriority = 0;
			newTask.PositionWeight = FVector(10.f);
			break;
		case PTT_Positional:
		case PTT_Orientational:
		case PTT_Dual:
		default: break;
		}
	}

	//Calculate offsets
	CalculateTaskOffsets(newTask, bone);
	if (sourceSel)
	{
		newTask.SourceIndex = sourceSel->Index;
	}
	const FScopedTransaction Transaction(NSLOCTEXT("IKinemaRigTool", "Add Constraint", "Add new constraint"));
	IKinemaRig->Modify();
	int32 index = IKinemaRig->SolverDef.Tasks.Add(newTask);
	IKinemaRig->PostLoadOrImport();
	IKinemaRig->UpdateVersion++;

	UIKinemaTaskDefWrapper*  task = FindObject<UIKinemaTaskDefWrapper>(IKinemaRig->GetOutermost(), *(newTask.Name.ToString()));

	if (!task)
	{
		task = NewObject<UIKinemaTaskDefWrapper>(IKinemaRig->GetOutermost(), newTask.Name);
		task->SetFlags(task->GetFlags() | RF_Transactional);
		task->Modify();
		task->TaskDef = newTask;
		task->TaskPtr = &IKinemaRig->SolverDef.Tasks[index];
		task->rig = IKinemaRig;
	}

	//Update the view object of any settings that we have updated in code.
	auto*  boneDef = FindObject<UIKinemaBoneDefWrapper>(IKinemaRig->GetOutermost(), *(bone.Name.ToString()));
	if (boneDef)
	{
		boneDef->BoneDef = bone;
	}
	TArray<UObject*> Objs;
	//Don't show anything expect what we are looking for

	Objs.Add(boneDef);
	GroupSelectionChangedEvent.Broadcast(Objs);
}

void FIKinemaRigToolSharedData::ToggleSimulation()
{
	bRunningSimulation = !bRunningSimulation;
}

void FIKinemaRigToolSharedData::LinkBones(FIKinemaBoneDef& target, FMocapBone& source)
{
	const int32 BoneIndex = EditorSkelComp->GetBoneIndex(target.Name);
	if (BoneIndex != INDEX_NONE)
	{
		const int32 ParentIndex = EditorSkelComp->GetBoneIndex(EditorSkelComp->GetParentBone(target.Name));;
		FTransform WorldTransform = EditorSkelComp->GetComponentSpaceTransforms()[BoneIndex] * EditorSkelComp->GetComponentTransform();
		FTransform WorldParentTransform(FTransform::Identity);
		if (ParentIndex != INDEX_NONE)
		{
			WorldParentTransform = (EditorSkelComp->GetComponentSpaceTransforms()[ParentIndex] * EditorSkelComp->GetComponentTransform());
		}
		IKinemaRig->LinkSourceToTarget(source, target, WorldTransform, WorldParentTransform);
		source.TargetBone = target.Name;
		target.SourceIndex = IKinemaRig->Actor->GetBoneIndex(source.Name);
	}

	//Loop over all task, and update any task attached to a a bone in the subhierarchy of this target bone
	for (auto& task : IKinemaRig->SolverDef.Tasks)
	{
		auto& BoneDef = IKinemaRig->SolverDef.Bones[task.BoneIndex];
		if (EditorSkelComp->BoneIsChildOf(BoneDef.Name, target.Name) || BoneDef.Name == target.Name)
		{
			//Recalculate Task offsets
			CalculateTaskOffsets(task, BoneDef);

		}
	}

	const FScopedTransaction Transaction(NSLOCTEXT("IKinemaRigTool", "Link Bones", "Liking source bone to target"));
	IKinemaRig->Modify();
	IKinemaRig->PostLoadOrImport();
	IKinemaRig->UpdateVersion++;
	IKinemaRig->Actor->Modify();
}

void FIKinemaRigToolSharedData::LinkBones()
{
	auto targetSel = GetSelectedBody();
	auto sourceSel = GetSelectedSourceBody();

	auto& target = IKinemaRig->SolverDef.Bones[targetSel->Index];
	auto& source = IKinemaRig->Actor->GetBone(selected.Name);

	//source.TargetBone = target.Name;
	//	IKinemaRig->SolverDef.Bones[targetSel->Index].SourceIndex = sourceSel->Index;
	LinkBones(target, source);

	TArray<UObject*> Objs;

	auto*  boneDef = FindObject<UIKinemaBoneDefWrapper>(IKinemaRig->GetOutermost(), *(target.Name.ToString()));
	if (boneDef)
	{
		boneDef->BoneDef = target;
	}
	//Don't show anything expect what we are looking for

	Objs.Add(boneDef);
	GroupSelectionChangedEvent.Broadcast(Objs);
}

void FIKinemaRigToolSharedData::PostUndo()
{
	if (bRunningSimulation)
	{
		return;
	}

	// Clear selection before we undo. We don't transact the editor itself - don't want to have something selected that is then removed.
	SetSelectedBody(NULL);
	SetSelectedConstraint(INDEX_NONE);

	PreviewChangedEvent.Broadcast();
	HierarchyChangedEvent.Broadcast();

	IKinemaRig->PostLoad();
	IKinemaRig->UpdateVersion++;
}

void FIKinemaRigToolSharedData::Redo()
{
	if (bRunningSimulation)
	{
		return;
	}

	SetSelectedBody(NULL);
	SetSelectedConstraint(INDEX_NONE);

	GEditor->RedoTransaction();
	PreviewChangedEvent.Broadcast();
	HierarchyChangedEvent.Broadcast();
}

#undef LOCTEXT_NAMESPACE