/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include "PhysicsPublic.h"

/*-----------------------------------------------------------------------------
   Hit Proxies
-----------------------------------------------------------------------------*/

struct HIKinemaRigToolEdBoneProxy : public HHitProxy
{
	DECLARE_HIT_PROXY();

	int32							BodyIndex;
	HIKinemaRigToolEdBoneProxy(int32 InBodyIndex):
	HHitProxy(HPP_UI),
		BodyIndex(InBodyIndex){}
};

struct HIKinemaRigToolEdSourceBoneProxy : public HHitProxy
{
	DECLARE_HIT_PROXY();

	int32							BodyIndex;
	HIKinemaRigToolEdSourceBoneProxy(int32 InBodyIndex) :
		HHitProxy(HPP_UI),
		BodyIndex(InBodyIndex) {}
};

struct HIKinemaRigToolEdConstraintProxy : public HHitProxy
{
	DECLARE_HIT_PROXY();

	int32							ConstraintIndex;

	HIKinemaRigToolEdConstraintProxy(int32 InConstraintIndex):
	HHitProxy(HPP_UI),
		ConstraintIndex(InConstraintIndex) {}
};

struct HIKinemaRigToolEdBoneNameProxy : public HHitProxy
{
	DECLARE_HIT_PROXY();

	int32			BoneIndex;

	HIKinemaRigToolEdBoneNameProxy(int32 InBoneIndex):
	HHitProxy(HPP_UI),
		BoneIndex(InBoneIndex) {}
};

struct HIKinemaRigToolEdCoMConstraintProxy : public HHitProxy
{
	DECLARE_HIT_PROXY();

	HIKinemaRigToolEdCoMConstraintProxy() :
	HHitProxy(HPP_UI)
	{}
};
