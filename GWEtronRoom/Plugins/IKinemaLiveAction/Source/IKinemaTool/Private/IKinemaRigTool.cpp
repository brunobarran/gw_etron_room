/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by cotntacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaRigTool.h"
#include "IKinemaRigToolModule.h"
#include "AssetSelection.h"
#include "ScopedTransaction.h"
#include "ObjectTools.h"
#include "Toolkits/IToolkitHost.h"
#include "PreviewScene.h"
#include "IKinemaRigToolPreviewViewportClient.h"
#include "SIKinemaRigToolPreviewViewport.h"
#include "IKinemaRigToolActions.h"
#include "IKinemaRigToolSharedData.h"
#include "IKinemaRigToolEdSkeletalMeshComponent.h"

#include "Editor/WorkspaceMenuStructure/Public/WorkspaceMenuStructureModule.h"
#include "Editor/PropertyEditor/Public/PropertyEditorModule.h"
#include "Editor/PropertyEditor/Public/IDetailsView.h"
#include "Editor/PropertyEditor/Public/DetailLayoutBuilder.h"
#include "Editor/ContentBrowser/Public/ContentBrowserModule.h"

#include "PropertyCustomizationHelpers.h"
#include "ClassViewerModule.h"
#include "ClassViewerFilter.h"
#include "Engine/Selection.h"

#include "WorkflowOrientedApp/SContentReference.h"
#include "AssetData.h"
#include "UnrealEd.h"

#include "EngineAnalytics.h"
#include "Runtime/Analytics/Analytics/Public/Interfaces/IAnalyticsProvider.h"
#include "Widgets/Docking/SDockTab.h"
#include "SIKinemaBoneMapper.h"
#include "SLicensePanel.h"
#include "IKinemaBoneCharacterisationWidget.h"
#include "IKinemaRigDetailsCustomization.h"
DEFINE_LOG_CATEGORY(LogIKinemaRigTool);
#define LOCTEXT_NAMESPACE "IKinemaRigTool"

namespace IKinemaRigTool
{
	const float	DefaultPrimSize = 15.0f;
	const float	DuplicateXOffset = 10.0f;
}


class FIKinemaRigToolTreeInfo
{
public:
	FIKinemaRigToolTreeInfo(FName InName,
		bool bInBold,
		int32 InParentBoneIdx = INDEX_NONE,
		int32 InBoneOrConstraintIdx = INDEX_NONE,
		int32 InBodyIdx = INDEX_NONE)
		: Name(InName)
		, bBold(bInBold)
		, ParentBoneIdx(InParentBoneIdx)
		, BoneOrConstraintIdx(InBoneOrConstraintIdx)
		, BodyIdx(InBodyIdx)
	{
	}

	FName Name;
	bool bBold;
	int32 ParentBoneIdx;
	int32 BoneOrConstraintIdx;
	int32 BodyIdx;
};


static const FName IKinemaRigToolPreviewViewportName("IKinemaRigTool_PreviewViewport");
static const FName IKinemaRigToolPropertiesName("IKinemaRigTool_Properties");
static const FName IKinemaRigToolHierarchyName("IKinemaRigTool_Hierarchy");
static const FName IKinemaRigToolMocapSourceName("IKinemaRigTool_MocapSource");
static const FName IKinemaRigToolBoneMapperName("IKinemaRigTool_BoneMapper");
static const FName IKinemaRigToolCharacterisationName("IKinemaRigTool_Characterisation");


void FIKinemaRigTool::RegisterTabSpawners(const TSharedRef<class FTabManager>& InTabManager)
{
	WorkspaceMenuCategory = InTabManager->AddLocalWorkspaceMenuCategory(LOCTEXT("WorkspaceMenu_IKinemaRigTool", "IKinema Rig Editor"));
	auto WorkspaceMenuCategoryRef = WorkspaceMenuCategory.ToSharedRef();

	FAssetEditorToolkit::RegisterTabSpawners(InTabManager);

	InTabManager->RegisterTabSpawner(IKinemaRigToolPreviewViewportName, FOnSpawnTab::CreateSP(this, &FIKinemaRigTool::SpawnTab, IKinemaRigToolPreviewViewportName))
		.SetDisplayName(LOCTEXT("ViewportTab", "Viewport"))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FEditorStyle::GetStyleSetName(), "LevelEditor.Tabs.Viewports"));

	InTabManager->RegisterTabSpawner(IKinemaRigToolPropertiesName, FOnSpawnTab::CreateSP(this, &FIKinemaRigTool::SpawnTab, IKinemaRigToolPropertiesName))
		.SetDisplayName(LOCTEXT("PropertiesTab", "Details"))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FIKinemaRigToolStyle::GetStyleSetName(), "IKinemaRigTool.Details"));

	InTabManager->RegisterTabSpawner(IKinemaRigToolHierarchyName, FOnSpawnTab::CreateSP(this, &FIKinemaRigTool::SpawnTab, IKinemaRigToolHierarchyName))
		.SetDisplayName(LOCTEXT("HierarchyTab", "Hierarchy"))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FIKinemaRigToolStyle::GetStyleSetName(), "IKinemaRigTool.Hierarchy"));

	InTabManager->RegisterTabSpawner(IKinemaRigToolMocapSourceName, FOnSpawnTab::CreateSP(this, &FIKinemaRigTool::SpawnTab, IKinemaRigToolMocapSourceName))
		.SetDisplayName(LOCTEXT("MocapSourceTab", "Mocap Source"))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FIKinemaRigToolStyle::GetStyleSetName(), "IKinemaRigTool.MocapSource"));

	InTabManager->RegisterTabSpawner(IKinemaRigToolBoneMapperName, FOnSpawnTab::CreateSP(this, &FIKinemaRigTool::SpawnMapper, IKinemaRigToolBoneMapperName))
		.SetDisplayName(LOCTEXT("BoneMapperTab", "IKINEMA Bone Mapper"))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FIKinemaRigToolStyle::GetStyleSetName(), "IKinemaRigTool.MapBones.Small"));

	InTabManager->RegisterTabSpawner(IKinemaRigToolCharacterisationName, FOnSpawnTab::CreateSP(this, &FIKinemaRigTool::SpawnCharacterisation, IKinemaRigToolCharacterisationName))
		.SetDisplayName(LOCTEXT("CharacterisationTab", "IKINEMA Character Characterisation"))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FIKinemaRigToolStyle::GetStyleSetName(), "IKinemaRigTool.Characterise.Small")); //TODO: change to new icon from Atbin

}

void FIKinemaRigTool::UnregisterTabSpawners(const TSharedRef<class FTabManager>& InTabManager)
{
	FAssetEditorToolkit::UnregisterTabSpawners(InTabManager);

	InTabManager->UnregisterTabSpawner(IKinemaRigToolPreviewViewportName);
	InTabManager->UnregisterTabSpawner(IKinemaRigToolPropertiesName);
	InTabManager->UnregisterTabSpawner(IKinemaRigToolHierarchyName);
	InTabManager->UnregisterTabSpawner(IKinemaRigToolMocapSourceName);
	InTabManager->UnregisterTabSpawner(IKinemaRigToolBoneMapperName);
	InTabManager->UnregisterTabSpawner(IKinemaRigToolCharacterisationName);
}

TSharedRef<SDockTab> FIKinemaRigTool::SpawnTab(const FSpawnTabArgs& TabSpawnArgs, FName TabIdentifier)
{
	if (TabIdentifier == IKinemaRigToolPreviewViewportName)
	{
		const TSharedRef<SDockTab> SpawnedTab = SNew(SDockTab)
			.Label(LOCTEXT("IKinemaRigToolViewportTitle", "Viewport"))
			[
				PreviewViewport.ToSharedRef()
			];

		PreviewViewport->ParentTab = SpawnedTab;

		return SpawnedTab;
	}
	else if (TabIdentifier == IKinemaRigToolPropertiesName)
	{
		return SNew(SDockTab)
			.Icon(FIKinemaRigToolStyle::GetBrush("IKinemaRigTool.Details"))
			.Label(LOCTEXT("IKinemaRigToolPropertiesTitle", "Details"))
			[
				Properties.ToSharedRef()
			];
	}
	else if (TabIdentifier == IKinemaRigToolHierarchyName)
	{
		const TSharedRef<SDockTab> NewTab = SNew(SDockTab)
			//.Icon(FEditorStyle::GetBrush("PhAT.Tabs.Hierarchy"))
			.Icon(FIKinemaRigToolStyle::GetBrush("IKinemaRigTool.Hierarchy"))
			.Label(LOCTEXT("IKinemaRigToolHierarchyTitle", "Hierarchy"))
			[
				SNew(SVerticalBox)

				+ SVerticalBox::Slot()
			.AutoHeight()
			.Padding(FMargin(5.0f, 0.0f, 0.0f, 5.0f))
			[
				SNew(SHorizontalBox)

				+ SHorizontalBox::Slot()
			.AutoWidth()
			.HAlign(HAlign_Left)
			[
				SAssignNew(HierarchyFilter, SComboButton)
				.ContentPadding(3)
			.OnGetMenuContent(this, &FIKinemaRigTool::BuildHierarchyFilterMenu)
			.ButtonContent()
			[
				SNew(STextBlock)
				.Text(this, &FIKinemaRigTool::GetHierarchyFilter)
			]
			]

		+ SHorizontalBox::Slot()
			.FillWidth(1.f)

			]

		+ SVerticalBox::Slot()
			.Padding(FMargin(0.f, 0.f, 0.f, 0.f))
			[
				HierarchyControl.ToSharedRef()
			]
			];

		RefreshHierachyTree();

		return NewTab;
	}
	else if (TabIdentifier == IKinemaRigToolMocapSourceName)
	{
		auto tag = SMocapSourcePicker::Other;
		auto actor = SharedData->IKinemaRig->Actor;
		if (actor != nullptr)
		{
			EMocapServer server = SharedData->IKinemaRig->Actor->ServerType;
			if (server == EMocapServer::LiveLink)
			{
				tag = SMocapSourcePicker::LiveLinkMenu;
			}
		}

		const TSharedRef<SDockTab> NewTab = SNew(SDockTab)
			.Icon(FIKinemaRigToolStyle::GetBrush("IKinemaRigTool.MocapSource"))
			.Label(LOCTEXT("IKinemaRigToolMocapSourceTitle", "Mocap Source"))
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
			.AutoHeight()
			.Padding(FMargin(5.0f, 0.0f, 0.0f, 5.0f))
			[
				SAssignNew(MocapSourcePicker, SMocapSourcePicker)
				.OnSourceScaleChanged(this, &FIKinemaRigTool::OnSourceScaleChanged)
			.renderTag(tag)
			]
		+ SVerticalBox::Slot()
			.AutoHeight()
			.Padding(FMargin(5.f, 0.f, 0.f, 5.f))
			[
				SNew(SButton)
				.HAlign(HAlign_Center)
			.Text(LOCTEXT("IKinemaRigToolImportFigure", "Import Actor"))
			.OnClicked(this, &FIKinemaRigTool::OnImportActor)
			.IsEnabled(this, &FIKinemaRigTool::CanImportActor)
			]
			];
		//MocapSourceCombo->RequestListRefresh();
		return NewTab;
	}
	else
	{
		return SNew(SDockTab);
	}

}

TSharedRef<SDockTab> FIKinemaRigTool::SpawnMapper(const FSpawnTabArgs& TabSpawnArgs, FName TabIdentifier)
{
	TSharedPtr<FIKinemaRigTool> ThisPtr(SharedThis(this));
	TSharedRef<SWidget> TabBody = SNew(SIKinemaRetargetEditor)
		.RigTool(ThisPtr)
		.bCharacterisation(false);

	TSharedPtr<FTagMetaData> MetaData = TabBody->GetMetaData<FTagMetaData>();

	TabBody = SNew(SBorder)
		.Padding(4.0f)
		//		.FillWidth()
		.BorderImage(FEditorStyle::GetBrush("ToolPanel.GroupBorder"))
		.AddMetaData<FTagMetaData>(MetaData.IsValid() ? MetaData->Tag : TabIdentifier)
		[
			TabBody
		];


	TSharedRef<SDockTab> NewTab = SNew(SDockTab)
		.TabRole(ETabRole::PanelTab)
		.Icon(FIKinemaRigToolStyle::GetBrush("IKinemaRigTool.MapBones.Small"))
		.Label(LOCTEXT("RetargetManagerTabTitle", "Retarget Manager"))
		.ShouldAutosize(false)
		[
			TabBody
		];
	return NewTab;

}

TSharedRef<SDockTab> FIKinemaRigTool::SpawnCharacterisation(const FSpawnTabArgs& TabSpawnArgs, FName TabIdentifier)
{
	TSharedPtr<FIKinemaRigTool> ThisPtr(SharedThis(this));
	TSharedRef<SWidget> TabBody = SNew(SIKinemaRetargetEditor)
		.RigTool(ThisPtr)
		.bCharacterisation(true);

	TSharedPtr<FTagMetaData> MetaData = TabBody->GetMetaData<FTagMetaData>();

	TabBody = SNew(SBorder)
		.Padding(4.0f)
		//		.FillWidth()
		.BorderImage(FEditorStyle::GetBrush("ToolPanel.GroupBorder"))
		.AddMetaData<FTagMetaData>(MetaData.IsValid() ? MetaData->Tag : TabIdentifier)
		[
			TabBody
		];


	TSharedRef<SDockTab> NewTab = SNew(SDockTab)
		.TabRole(ETabRole::PanelTab)
		.Icon(FIKinemaRigToolStyle::GetBrush("IKinemaRigTool.Characterise.Small"))
		.Label(LOCTEXT("RetargetManagerTabTitle", "Characterisation Manager"))
		.ShouldAutosize(false)
		[
			TabBody
		];
	return NewTab;
}

FIKinemaRigTool::~FIKinemaRigTool()
{
	GEditor->UnregisterForUndo(this);
}

void FIKinemaRigTool::InitIKinemaRigTool(const EToolkitMode::Type Mode, const TSharedPtr< class IToolkitHost >& InitToolkitHost, UIKinemaRig* ObjectToEdit)
{
	HierarchyFilterMode = PHFM_All;
	SelectedAnimation = NULL;
	SelectedSimulation = false;
	BeforeSimulationWidgetMode = FWidget::EWidgetMode::WM_None;

	SharedData = MakeShareable(new FIKinemaRigToolSharedData);

	SharedData->SelectionChangedEvent.AddRaw(this, &FIKinemaRigTool::SetPropertiesSelection);
	SharedData->GroupSelectionChangedEvent.AddRaw(this, &FIKinemaRigTool::SetPropertiesGroupSelection);
	SharedData->HierarchyChangedEvent.AddRaw(this, &FIKinemaRigTool::RefreshHierachyTree);
	SharedData->HierarchySelectionChangedEvent.AddRaw(this, &FIKinemaRigTool::RefreshHierachyTreeSelection);
	SharedData->PreviewChangedEvent.AddRaw(this, &FIKinemaRigTool::RefreshPreviewViewport);

	SharedData->IKinemaRig = ObjectToEdit;

	SharedData->Initialize();

	InsideSelChanged = false;

	GEditor->RegisterForUndo(this);

	// Register our commands. This will only register them if not previously registered
	FIKinemaRigToolCommands::Register();

	BindCommands();

	CreateInternalWidgets();

	const TSharedRef<FTabManager::FLayout> StandaloneDefaultLayout = FTabManager::NewLayout("Standalone_LiveActionRigTool_Layout")
		->AddArea
		(
			FTabManager::NewPrimaryArea()
			->SetOrientation(Orient_Vertical)
			->Split
			(
				//Toolbar
				FTabManager::NewStack()
				->SetSizeCoefficient(0.f)
				->SetHideTabWell(true)
				->AddTab(GetToolbarTabId(), ETabState::OpenedTab)
			)
			->Split
			(
				//Rest of Editor
				FTabManager::NewSplitter()
				->SetSizeCoefficient(0.8f)
				->SetOrientation(Orient_Horizontal)
				->Split
				(
					FTabManager::NewStack()
					->SetSizeCoefficient(0.6f)
					->AddTab(IKinemaRigToolPreviewViewportName, ETabState::OpenedTab)
				)
				->Split
				(
					FTabManager::NewSplitter()
					->SetSizeCoefficient(0.4f)
					->Split
					(
						FTabManager::NewStack()->AddTab(IKinemaRigToolPropertiesName, ETabState::OpenedTab)
					)
					->Split
					(
						FTabManager::NewSplitter()
						->SetSizeCoefficient(0.7f)
						->SetOrientation(Orient_Vertical)
						->Split
						(
							FTabManager::NewStack()
							->SetSizeCoefficient(0.7f)
							->AddTab(IKinemaRigToolHierarchyName, ETabState::OpenedTab)
						)
						->Split
						(
							FTabManager::NewStack()
							->SetSizeCoefficient(0.3f)
							->AddTab(IKinemaRigToolMocapSourceName, ETabState::OpenedTab)
						)
					)

				)
			)
		);

	const bool bCreateDefaultStandaloneMenu = true;
	const bool bCreateDefaultToolbar = true;
	FAssetEditorToolkit::InitAssetEditor(Mode, InitToolkitHost, IKinemaRigToolAppIdentifier, StandaloneDefaultLayout, bCreateDefaultStandaloneMenu, bCreateDefaultToolbar, ObjectToEdit);
	//TODO: Source Mocap Import
	if (SharedData->IKinemaRig->Actor && MocapSourcePicker.IsValid())
	{
		auto actor = SharedData->IKinemaRig->Actor;
		if (actor->ServerType != EMocapServer::LiveLink)
		{

			MocapSourcePicker->SetServerIP(actor->ServerIP);
			MocapSourcePicker->SetPortNumber(actor->ServerPort);
		}
		MocapSourcePicker->SetSubjectName(actor->SubjectName);
		MocapSourcePicker->SetImportScale(actor->ImportScale);
		MocapSourcePicker->SetMocapServer(actor->ServerType);
		MocapSourcePicker->SetTemplateName(actor->TemplateName);
	}

	ExtendMenu();
	ExtendToolbar();
	RegenerateMenusAndToolbars();
}

TSharedPtr<FIKinemaRigToolSharedData> FIKinemaRigTool::GetSharedData() const
{
	return SharedData;
}

void FIKinemaRigTool::SetPropertiesSelection(UObject* Obj, FIKinemaRigToolSharedData::FSelection * Body)
{
	if (Properties.IsValid())
	{
		TArray<UObject*> Selection;
		Selection.Add(Obj);
		Properties->SetObjects(Selection);
	}

	if (Hierarchy.IsValid())
	{
		if (Body)
		{
			bool bFound = false;
			for (int32 ItemIdx = 0; ItemIdx < TreeElements.Num(); ++ItemIdx)
			{
				FIKinemaRigToolTreeInfo& Info = *TreeElements[ItemIdx];
				if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
				{
					if ((Info.BodyIdx == Body->Index))
					{
						Hierarchy->ClearSelection();
						Hierarchy->SetItemSelection(TreeElements[ItemIdx], true);
						bFound = true;
						break;
					}
				}
				else
				{
					if (Info.BoneOrConstraintIdx == Body->Index)
					{
						Hierarchy->ClearSelection();
						Hierarchy->SetItemSelection(TreeElements[ItemIdx], true);
						bFound = true;
						break;
					}
				}
			}
		}
	}
}


void FIKinemaRigTool::SetPropertiesGroupSelection(const TArray<UObject*> & Objs)
{
	if (Properties.IsValid())
	{
		Properties->SetObjects(Objs);
	}
}

bool TreeElemSelected(FTreeElemPtr TreeElem, TSharedPtr<FIKinemaRigToolSharedData> SharedData, TSharedPtr< STreeView<FTreeElemPtr> > Hierarchy)
{
	bool bIsExpanded = Hierarchy->IsItemExpanded(TreeElem);

	if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
	{
		FIKinemaRigToolSharedData::FSelection Selection(TreeElem->BodyIdx);
		for (int32 i = 0; i < SharedData->SelectedBodies.Num(); ++i)
		{
			if (Selection == SharedData->SelectedBodies[i])
			{
				return true;
			}
		}
	}
	return false;
}

void FIKinemaRigTool::RefreshHierachyTreeSelection()
{
	if (InsideSelChanged)	//we only want to update if the change came from viewport
	{
		return;
	}

	if (Hierarchy.Get())
	{
		for (int32 i = 0; i < TreeElements.Num(); ++i)
		{
			Hierarchy->SetItemSelection(TreeElements[i], TreeElemSelected(TreeElements[i], SharedData, Hierarchy), ESelectInfo::Direct);
		}
	}
}

bool FIKinemaRigTool::FilterTreeElement(FTreeElemPtr TreeElem) const
{
	if (HierarchyFilterMode == PHFM_All)
	{
		return true;
	}

	if (HierarchyFilterMode == PHFM_Bodies)
	{
		if (TreeElem->bBold || TreeElem->BodyIdx != INDEX_NONE)
		{
			return true;
		}
	}

	return false;
}

void FIKinemaRigTool::RefreshHierachyTree()
{
	TreeElements.Empty();
	RootBone.Empty();
	auto ExtractedName = [](const FString& name)
	{
		FString BoneName(name);
		BoneName.Split(":", nullptr, &BoneName);
		BoneName = BoneName.Replace(TEXT("FBXASC032"), TEXT("-"));
		return FName(*BoneName);
	};
	// if next event is selecting a bone to create a new body, Fill up tree with bone names
	if (SharedData->IKinemaRig)
	{
		if (SharedData->IKinemaRig->Skeleton)
		{
			const auto& RefSkeleton = SharedData->IKinemaRig->Skeleton->GetReferenceSkeleton();
			if (HierarchyFilterMode == PHFM_All)
			{
				for (int32 i = 0; i < SharedData->IKinemaRig->SolverDef.Bones.Num(); ++i)
				{
					FString BoneName = SharedData->IKinemaRig->SolverDef.Bones[i].Name.ToString();
					FName Name = ExtractedName(BoneName);
					const int32 BoneIndex = RefSkeleton.FindBoneIndex(Name);
					FString parentBoneName = SharedData->IKinemaRig->SolverDef.Bones[i].ParentName.ToString();
					const int32 ParentBoneIndex = RefSkeleton.FindBoneIndex(ExtractedName(parentBoneName));
					if (BoneIndex != INDEX_NONE)
					{
						TreeElements.Add(FTreeElemPtr(new FIKinemaRigToolTreeInfo(Name, true, ParentBoneIndex, BoneIndex)));
					}
				}
			}
			else //Only show constraints
			{
				for (int32 i = 0; i < SharedData->IKinemaRig->SolverDef.Tasks.Num(); ++i)
				{
					auto& task = SharedData->IKinemaRig->SolverDef.Tasks[i];
					FString BoneName = SharedData->IKinemaRig->SolverDef.Bones[i].Name.ToString();
					const int32 BoneIndex = RefSkeleton.FindBoneIndex(ExtractedName(BoneName));
					if (BoneIndex != INDEX_NONE)
					{
						TreeElements.Add(FTreeElemPtr(new FIKinemaRigToolTreeInfo(task.Name, true, INDEX_NONE, i)));
					}
				}
			}
		}
	}
	else
	{
		// Add inert bones
		for (int32 BoneIndex = 0; BoneIndex < SharedData->EditorSkelMesh->RefSkeleton.GetNum(); ++BoneIndex)
		{
			bool bFound = false;
			for (int32 ItemIdx = 0; ItemIdx < TreeElements.Num(); ++ItemIdx)
			{
				const FIKinemaRigToolTreeInfo& Info = *TreeElements[ItemIdx];
				if (SharedData->EditorSkelComp->GetBoneIndex(Info.Name) == BoneIndex)
				{
					bFound = true;
					break;
				}
			}

			if (!bFound)
			{
				const int32 BoneOrConstraintIdx = (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit) ? BoneIndex : INDEX_NONE;
				TreeElements.Add(FTreeElemPtr(new FIKinemaRigToolTreeInfo(SharedData->EditorSkelMesh->RefSkeleton.GetBoneName(BoneIndex), false, INDEX_NONE, BoneOrConstraintIdx)));
			}
		}
	}
	struct FCompareBoneIndex
	{
		TSharedPtr<FIKinemaRigToolSharedData> SharedData;

		FCompareBoneIndex(TSharedPtr<FIKinemaRigToolSharedData> InSharedData)
			: SharedData(InSharedData)
		{
		}

		FORCEINLINE bool operator()(const FTreeElemPtr &A, const FTreeElemPtr &B) const
		{
			const auto& RefSkeleton = SharedData->IKinemaRig->Skeleton->GetReferenceSkeleton();
			const int32 ValA = RefSkeleton.FindBoneIndex((*A).Name);
			const int32 ValB = RefSkeleton.FindBoneIndex((*B).Name);
			return ValA < ValB;
		}

	};

	TreeElements.Sort(FCompareBoneIndex(SharedData));


	if (HierarchyFilterMode != PHFM_All)
	{
		for (auto& elem : TreeElements)
		{
			RootBone.Add(elem);
		}
	}
	else
	{
		if (TreeElements.Num())
		{
			auto elements = TreeElements.FilterByPredicate([](FTreeElemPtr TreeElement) {
				return TreeElement->ParentBoneIdx == INDEX_NONE;
			});
			RootBone.Append(elements);
		}
	}
	if (Hierarchy.IsValid())
	{
		Hierarchy->RequestTreeRefresh();

		for (int32 BoneIndex = 0; BoneIndex < TreeElements.Num(); ++BoneIndex)
		{
			Hierarchy->SetItemExpansion(TreeElements[BoneIndex], true);
		}

		// Force the tree to refresh now instead of next tick
		const FGeometry Stub;
		Hierarchy->Tick(Stub, 0.0f, 0.0f);

		if (!InsideSelChanged && (Hierarchy->GetNumItemsSelected() > 0))
		{
			Hierarchy->RequestScrollIntoView(Hierarchy->GetSelectedItems()[0]);
		}
	}
}

void FIKinemaRigTool::RefreshPreviewViewport()
{
	if (PreviewViewport.IsValid())
	{
		PreviewViewport->RefreshViewport();
	}
}

FName FIKinemaRigTool::GetToolkitFName() const
{
	return FName("IKinemaRigTool");
}

FText FIKinemaRigTool::GetBaseToolkitName() const
{
	return LOCTEXT("AppLabel", "IKinemaRigTool");
}

FString FIKinemaRigTool::GetWorldCentricTabPrefix() const
{
	return LOCTEXT("WorldCentricTabPrefix", "IKinemaRigTool ").ToString();
}

FLinearColor FIKinemaRigTool::GetWorldCentricTabColorScale() const
{
	return FLinearColor(0.3f, 0.2f, 0.5f, 0.5f);
}

void PopulateLayoutMenu(FMenuBuilder& MenuBuilder, const TSharedRef<SDockTabStack>& DockTabStack)
{

}

void FIKinemaRigTool::AddReferencedObjects(FReferenceCollector& Collector)
{
	Collector.AddReferencedObject(SharedData->IKinemaRig);

	if (PreviewViewport.IsValid())
	{
		SharedData->PreviewScene.AddReferencedObjects(Collector);
	}

	Collector.AddReferencedObject(SharedData->MouseHandle);
}

void FIKinemaRigTool::PostUndo(bool bSuccess)
{
	SharedData->PostUndo();
	RefreshHierachyTree();

	SharedData->RefreshIKinemaRigAssetChange(SharedData->IKinemaRig);
	RefreshPreviewViewport();
}

void FIKinemaRigTool::PostRedo(bool bSuccess)
{
	PostUndo(bSuccess);
	RefreshPreviewViewport();
}

void FIKinemaRigTool::CreateInternalWidgets()
{
	PreviewViewport =
		SNew(SIKinemaRigToolPreviewViewport)
		.IKinemaRigTool(SharedThis(this));

	FDetailsViewArgs Args;
	Args.bHideSelectionTip = true;

	FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");

	PropertyModule.RegisterCustomClassLayout(UIKinemaRig::StaticClass()->GetFName(), FOnGetDetailCustomizationInstance::CreateStatic(&FIKinemaRigDetailsCustomization::MakeInstance));
	Properties = PropertyModule.CreateDetailView(Args);

	if (Properties.IsValid())
	{
		TArray<UObject*> objs;
		objs.Add(SharedData->IKinemaRig);
		Properties->SetObjects(objs);
	}



	HierarchyControl =
		SNew(SBorder)
		.Padding(8)
		[
			SAssignNew(Hierarchy, STreeView<FTreeElemPtr>)
			.SelectionMode(ESelectionMode::Multi)
		.TreeItemsSource(&RootBone)
		.OnGetChildren(this, &FIKinemaRigTool::OnGetChildrenForTree)
		.OnGenerateRow(this, &FIKinemaRigTool::OnGenerateRowForTree)
		.OnSelectionChanged(this, &FIKinemaRigTool::OnTreeSelectionChanged)
		.OnMouseButtonDoubleClick(this, &FIKinemaRigTool::OnTreeDoubleClick)
		.OnContextMenuOpening(this, &FIKinemaRigTool::OnTreeRightClick)
		.IsEnabled(this, &FIKinemaRigTool::IsNotSimulation)
		.HeaderRow
		(
			SNew(SHeaderRow)
			.Visibility(EVisibility::Collapsed)
			+ SHeaderRow::Column(FName(TEXT("Hierarchy")))
			.DefaultLabel(LOCTEXT("Hierarchy", "Hierarchy"))
		)
		];
}

FText FIKinemaRigTool::GetRepeatLastSimulationToolTip() const
{
	return FIKinemaRigToolCommands::Get().RepeatLastSimulation->GetDescription();
}

FSlateIcon FIKinemaRigTool::GetRepeatLastSimulationIcon() const
{
	return FIKinemaRigToolCommands::Get().RepeatLastSimulation->GetIcon();
}

FText FIKinemaRigTool::GetEditModeLabel() const
{
	if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
	{
		return FIKinemaRigToolCommands::Get().EditingMode_Body->GetLabel();
	}
	else
	{
		return FIKinemaRigToolCommands::Get().EditingMode_Constraint->GetLabel();
	}
}

FText FIKinemaRigTool::GetEditModeToolTip() const
{
	if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
	{
		return FIKinemaRigToolCommands::Get().EditingMode_Body->GetDescription();
	}
	else
	{
		return FIKinemaRigToolCommands::Get().EditingMode_Constraint->GetDescription();
	}
}

FSlateIcon FIKinemaRigTool::GetEditModeIcon() const
{
	if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
	{
		return FIKinemaRigToolCommands::Get().EditingMode_Body->GetIcon();
	}
	else
	{
		return FIKinemaRigToolCommands::Get().EditingMode_Constraint->GetIcon();
	}
}

void FIKinemaRigTool::ExtendToolbar()
{
	struct Local
	{
		static TSharedRef< SWidget > FillEditMode(TSharedRef<FUICommandList> InCommandList)
		{
			const bool bShouldCloseWindowAfterMenuSelection = true;
			FMenuBuilder MenuBuilder(bShouldCloseWindowAfterMenuSelection, InCommandList);

			const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();

			MenuBuilder.AddMenuEntry(Commands.EditingMode_Body);
			MenuBuilder.AddMenuEntry(Commands.EditingMode_Constraint);

			return MenuBuilder.MakeWidget();
		}

		static void FillToolbar(FToolBarBuilder& ToolbarBuilder, TSharedRef<SWidget> IKinemaRigToolAnimation, FIKinemaRigToolSharedData::EIKinemaRigToolEditingMode InIKinemaRigToolEditingMode, FIKinemaRigTool * IKinemaRigTool)
		{
			const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();
			TSharedRef<FUICommandList> InCommandList = IKinemaRigTool->GetToolkitCommands();
			// Simulate
			ToolbarBuilder.BeginSection("IKinemaRigToolSimulation");

			ToolbarBuilder.AddToolBarButton(Commands.RepeatLastSimulation);
			ToolbarBuilder.EndSection();

			//IKinemaRigTool edit mode combo
			FUIAction IKinemaRigToolMode;
			IKinemaRigToolMode.CanExecuteAction = FCanExecuteAction::CreateSP(IKinemaRigTool, &FIKinemaRigTool::IsNotSimulation);
			ToolbarBuilder.BeginSection("IKinemaRigToolMode");
			{
				ToolbarBuilder.AddComboButton(
					IKinemaRigToolMode,
					FOnGetContent::CreateStatic(&FillEditMode, InCommandList),
					TAttribute< FText >::Create(TAttribute< FText >::FGetter::CreateSP(IKinemaRigTool, &FIKinemaRigTool::GetEditModeLabel)),
					TAttribute< FText >::Create(TAttribute< FText >::FGetter::CreateSP(IKinemaRigTool, &FIKinemaRigTool::GetEditModeToolTip)),
					TAttribute< FSlateIcon >::Create(TAttribute< FSlateIcon >::FGetter::CreateSP(IKinemaRigTool, &FIKinemaRigTool::GetEditModeIcon))
				);
			}
			ToolbarBuilder.EndSection();


			//Mapping Editor
			ToolbarBuilder.BeginSection("IKinemaRigToolMapping");
			{
				ToolbarBuilder.AddToolBarButton(Commands.MapBones);
			}
			ToolbarBuilder.EndSection();
			//Characterisation
			ToolbarBuilder.BeginSection("IKinemaRigToolCharacterisation");
			{
				ToolbarBuilder.AddToolBarButton(Commands.Characterise);
			}

			ToolbarBuilder.EndSection();
			//Play Animation
			ToolbarBuilder.BeginSection("IKinemaRigToolPlayAnimation");
			{
				ToolbarBuilder.AddToolBarButton(Commands.PlayAnimation);
			}
			ToolbarBuilder.EndSection();

			ToolbarBuilder.BeginSection("IKinemaRigToolAnimation");
			{
				ToolbarBuilder.AddWidget(IKinemaRigToolAnimation);
			}
			ToolbarBuilder.EndSection();

			ToolbarBuilder.BeginSection("IKinemaRigToolRecord");
			{
				ToolbarBuilder.AddToolBarButton(Commands.RecordAnimation,
					NAME_None,
					TAttribute<FText>(IKinemaRigTool, &FIKinemaRigTool::GetRecordStatusLabel),
					TAttribute<FText>(IKinemaRigTool, &FIKinemaRigTool::GetRecordStatusTooltip),
					TAttribute<FSlateIcon>(IKinemaRigTool, &FIKinemaRigTool::GetRecordStatusImage),
					NAME_None);
			}
			ToolbarBuilder.EndSection();
		}
	};

	// If the ToolbarExtender is valid, remove it before rebuilding it
	if (ToolbarExtender.IsValid())
	{
		RemoveToolbarExtender(ToolbarExtender);
		ToolbarExtender.Reset();
	}

	ToolbarExtender = MakeShareable(new FExtender);

	TSharedRef<SWidget> IKinemaRigToolAnimation = SNew(SBox)
		.WidthOverride(250)
		.IsEnabled_Lambda([this] {return this->SharedData->bRunningSimulation; })
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.AutoWidth()
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("IKinemaRigToolToolbarAnimation", "Animation: "))
		]
	+ SHorizontalBox::Slot()
		.FillWidth(1.0f)
		[
			SAssignNew(ClassPickerComboButton, SComboButton)
			.OnGetMenuContent(this, &FIKinemaRigTool::GetClassPickerMenuContent)
		.ContentPadding(0)
		.ButtonContent()
		[
			SNew(STextBlock)
			.Font(IDetailLayoutBuilder::GetDetailFont())
		.Text(this, &FIKinemaRigTool::GetSelectedAnimBlueprintName)
		]
		]
	+ SHorizontalBox::Slot()
		.AutoWidth()
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Center)
		.Padding(2.0f, 1.0f)
		[
			PropertyCustomizationHelpers::MakeBrowseButton(FSimpleDelegate::CreateSP(this, &FIKinemaRigTool::OnBrowseToAnimBlueprint))
		]
	+ SHorizontalBox::Slot()
		.AutoWidth()
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Center)
		.Padding(2.0f, 1.0f)
		[
			PropertyCustomizationHelpers::MakeUseSelectedButton(FSimpleDelegate::CreateSP(this, &FIKinemaRigTool::UseSelectedAnimBlueprint))
		]



		];

	ToolbarExtender->AddToolBarExtension(
		"Asset",
		EExtensionHook::After,
		GetToolkitCommands(),
		FToolBarExtensionDelegate::CreateStatic(&Local::FillToolbar, IKinemaRigToolAnimation, SharedData->EditingMode, this)
	);

	AddToolbarExtender(ToolbarExtender);

	IIKinemaRigToolModule* IKinemaRigToolModule = &FModuleManager::LoadModuleChecked<IIKinemaRigToolModule>("IKinemaRigTool");
	AddToolbarExtender(IKinemaRigToolModule->GetToolBarExtensibilityManager()->GetAllExtenders(GetToolkitCommands(), GetEditingObjects()));
}

void FIKinemaRigTool::ExtendMenu()
{
	struct Local
	{
		static void FillEdit(FMenuBuilder& MenuBarBuilder)
		{
			const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();
			MenuBarBuilder.BeginSection("Selection", LOCTEXT("IKinemaRigToolEditSelection", "Selection"));
			MenuBarBuilder.AddMenuEntry(Commands.SelectAllObjects);
			MenuBarBuilder.EndSection();
		}

		static void FillAsset(FMenuBuilder& MenuBarBuilder)
		{
			const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();
			MenuBarBuilder.BeginSection("Settings", LOCTEXT("IKinemaRigToolAssetSettings", "Settings"));
			MenuBarBuilder.AddMenuEntry(Commands.ChangeDefaultMesh);
			MenuBarBuilder.EndSection();
		}
		static void FillHelp(FMenuBuilder& MenuBarBuilder)
		{
			const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();
			MenuBarBuilder.BeginSection("License", LOCTEXT("LicenseSettings", "License"));
			MenuBarBuilder.AddMenuEntry(Commands.License);
			MenuBarBuilder.EndSection();
		}
	};
	MenuExtender = MakeShareable(new FExtender);
	MenuExtender->AddMenuExtension(
		"EditHistory",
		EExtensionHook::After,
		GetToolkitCommands(),
		FMenuExtensionDelegate::CreateStatic(&Local::FillEdit));

	MenuExtender->AddMenuExtension(
		"AssetEditorActions",
		EExtensionHook::After,
		GetToolkitCommands(),
		FMenuExtensionDelegate::CreateStatic(&Local::FillAsset));

	MenuExtender->AddMenuExtension(
		"HelpBrowse",
		EExtensionHook::After,
		GetToolkitCommands(),
		FMenuExtensionDelegate::CreateStatic(&Local::FillHelp));


	AddMenuExtender(MenuExtender);

	IIKinemaRigToolModule* IKinemaRigToolModule = &FModuleManager::LoadModuleChecked<IIKinemaRigToolModule>("IKinemaRigTool");
	AddMenuExtender(IKinemaRigToolModule->GetToolBarExtensibilityManager()->GetAllExtenders(GetToolkitCommands(), GetEditingObjects()));
}

void FIKinemaRigTool::BindCommands()
{
	const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();

	ToolkitCommands->MapAction(
		Commands.ChangeDefaultMesh,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnChangeDefaultMesh),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::IsNotSimulation));

	ToolkitCommands->MapAction(
		Commands.ResetBoneSettings,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnResetBone),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::IsSelectedEditBodyMode));

	ToolkitCommands->MapAction(
		Commands.EditingMode_Body,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnEditingMode, (int32)FIKinemaRigToolSharedData::PEM_BodyEdit),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::IsNotSimulation),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsEditingMode, (int32)FIKinemaRigToolSharedData::PEM_BodyEdit));

	ToolkitCommands->MapAction(
		Commands.EditingMode_Constraint,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnEditingMode, (int32)FIKinemaRigToolSharedData::PEM_ConstraintEdit),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::IsNotSimulation),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsEditingMode, (int32)FIKinemaRigToolSharedData::PEM_ConstraintEdit));

	ToolkitCommands->MapAction(
		Commands.CopyProperties,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnCopyProperties),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanCopyProperties),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsCopyProperties));

	ToolkitCommands->MapAction(
		Commands.PasteProperties,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnPasteProperties),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanPasteProperties));

	ToolkitCommands->MapAction(
		Commands.RepeatLastSimulation,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnToggleSimulation),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanStartSimulation),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsToggleSimulation));

	ToolkitCommands->MapAction(
		Commands.MeshRenderingMode_Solid,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnMeshRenderingMode, FIKinemaRigToolSharedData::PRM_Solid),
		FCanExecuteAction(),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsMeshRenderingMode, FIKinemaRigToolSharedData::PRM_Solid));

	ToolkitCommands->MapAction(
		Commands.MeshRenderingMode_Wireframe,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnMeshRenderingMode, FIKinemaRigToolSharedData::PRM_Wireframe),
		FCanExecuteAction(),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsMeshRenderingMode, FIKinemaRigToolSharedData::PRM_Wireframe));

	ToolkitCommands->MapAction(
		Commands.MeshRenderingMode_None,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnMeshRenderingMode, FIKinemaRigToolSharedData::PRM_None),
		FCanExecuteAction(),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsMeshRenderingMode, FIKinemaRigToolSharedData::PRM_None));

	ToolkitCommands->MapAction(
		Commands.ShowNonActive,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnShowNonActive),
		FCanExecuteAction(),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsShowNonActive));

	ToolkitCommands->MapAction(
		Commands.DrawGroundBox,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnDrawGroundBox),
		FCanExecuteAction(),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsDrawGroundBox));

	ToolkitCommands->MapAction(
		Commands.ToggleGraphicsHierarchy,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnToggleGraphicsHierarchy),
		FCanExecuteAction(),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsToggleGraphicsHierarchy));

	ToolkitCommands->MapAction(
		Commands.ShowConstraints,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnShowConstraints),
		FCanExecuteAction(),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsShowConstraints));

	//Create Tasks from Presets
	ToolkitCommands->MapAction(
		Commands.AddHipTask,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnAddHipTask),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.AddFootTask,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnAddFootTask),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.AddHandTask,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnAddHandTask),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.AddHeadTask,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnAddHeadTask),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.AddChestTask,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnAddChestTask),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.AddElbowTask,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnAddElbowTask),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.AddKneeTask,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnAddKneeTask),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));


	//Characterisation

	ToolkitCommands->MapAction(
		Commands.SetAsHips,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnCharacteriseHip),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.SetAsChest,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnCharacteriseChest),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.SetAsHead,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnCharacteriseHead),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.SetAsShoulder,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnCharacteriseShoulder),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.SetAsElbow,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnCharacteriseElbow),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.SetAsHand,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnCharacteriseHand),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.SetAsFoot,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnCharacteriseFoot),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));



	ToolkitCommands->MapAction(
		Commands.AddOrientationalConstraint,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OneAddOrientationalConstraint),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.AddPositionalConstraint,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnAddPositionalConstraint),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.AddDualConstraint,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnAddConstraint),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanAddConstraint));

	ToolkitCommands->MapAction(
		Commands.ResetConstraint,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnResetConstraint),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::IsSelectedEditConstraintMode));

	ToolkitCommands->MapAction(
		Commands.ResetCoMConstraint,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnResetCoMConstraint));/* ,
																				FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::IsSelectedEditConstraintMode));*/

	ToolkitCommands->MapAction(
		Commands.DeleteConstraint,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnDeleteConstraint),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::IsSelectedEditConstraintMode));

	ToolkitCommands->MapAction(
		Commands.PlayAnimation,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnPlayAnimation),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::IsToggleSimulation),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsPlayAnimation));

	ToolkitCommands->MapAction(
		Commands.ShowSkeleton,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnShowSkeleton),
		FCanExecuteAction(),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsShowSkeleton));

	ToolkitCommands->MapAction(
		Commands.ShowCollisions,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnShowCollisionShape),
		FCanExecuteAction(),
		FIsActionChecked::CreateSP(this, &FIKinemaRigTool::IsShowCollisionShape));

	ToolkitCommands->MapAction(
		Commands.PerspectiveView,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnViewType, ELevelViewportType::LVT_Perspective),
		FCanExecuteAction()
	);

	ToolkitCommands->MapAction(
		Commands.TopView,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnViewType, ELevelViewportType::LVT_OrthoXY),
		FCanExecuteAction()
	);

	ToolkitCommands->MapAction(
		Commands.LeftView,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnViewType, ELevelViewportType::LVT_OrthoYZ),
		FCanExecuteAction()
	);

	ToolkitCommands->MapAction(
		Commands.FrontView,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnViewType, ELevelViewportType::LVT_OrthoXZ),
		FCanExecuteAction()
	);

	ToolkitCommands->MapAction(
		Commands.BottomView,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnViewType, ELevelViewportType::LVT_OrthoNegativeXY),
		FCanExecuteAction()
	);

	ToolkitCommands->MapAction(
		Commands.RightView,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnViewType, ELevelViewportType::LVT_OrthoNegativeYZ),
		FCanExecuteAction()
	);

	ToolkitCommands->MapAction(
		Commands.BackView,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnViewType, ELevelViewportType::LVT_OrthoNegativeXZ),
		FCanExecuteAction()
	);

	ToolkitCommands->MapAction(
		Commands.DeleteBody,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnDeleteBody));

	ToolkitCommands->MapAction(
		Commands.DeleteAllBodiesBelow,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnDeleteAllBodiesBelow));

	ToolkitCommands->MapAction(
		Commands.SelectionLock,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnLockSelection),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::IsNotSimulation));

	ToolkitCommands->MapAction(
		Commands.FocusOnSelection,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnFocusSelection));

	ToolkitCommands->MapAction(
		Commands.DeleteSelected,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnDeleteSelection),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::IsNotSimulation));

	ToolkitCommands->MapAction(
		Commands.SelectAllObjectsBelow,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnSelectAllBelow),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanSelectAllBelow));

	ToolkitCommands->MapAction(
		Commands.LinkBones,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnLinkBones),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanLinkBones));

	ToolkitCommands->MapAction(
		Commands.SelectAllObjects,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnSelectAll));

	ToolkitCommands->MapAction(
		Commands.HierarchyFilterAll,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::SetHierarchyFilter, PHFM_All));

	ToolkitCommands->MapAction(
		Commands.HierarchyFilterBodies,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::SetHierarchyFilter, PHFM_Bodies));

	ToolkitCommands->MapAction(
		Commands.MapBones,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnMapBones),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::CanMapBones));

	ToolkitCommands->MapAction(
		Commands.Characterise,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnCharacterise));

	ToolkitCommands->MapAction(
		Commands.License,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnLicense));

	ToolkitCommands->MapAction(
		Commands.SourceSelection,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::OnSourceSelection));


	// record animation
	ToolkitCommands->MapAction(
		Commands.RecordAnimation,
		FExecuteAction::CreateSP(this, &FIKinemaRigTool::RecordAnimation),
		FCanExecuteAction::CreateSP(this, &FIKinemaRigTool::IsRecordAvailable),
		FIsActionChecked(),
		FIsActionButtonVisible()
	);
}

void FIKinemaRigTool::OnSourceScaleChanged(float Scale)
{
	if (SharedData->IKinemaRig && SharedData->IKinemaRig->Actor)
	{
		SharedData->IKinemaRig->Actor->ImportScale = Scale;
		SharedData->IKinemaRig->SolverDef.SourceScale = Scale;
	}
}

FReply FIKinemaRigTool::OnImportActor()
{
	if (MocapSourcePicker.IsValid())
	{
		auto mocapServer = MocapSourcePicker->GetMocapServer();
		auto address = MocapSourcePicker->GetServerIP();
		auto subject = MocapSourcePicker->GetSubjectName();
		auto templateName = MocapSourcePicker->GetTemplateName();
		auto port = MocapSourcePicker->GetPortNumber();
		auto importScale = MocapSourcePicker->GetImportScale();
		auto ZeroRotations = MocapSourcePicker->ZeroRotations();

		if (!SharedData->IKinemaRig->Actor)
		{
			for (auto& bone : SharedData->IKinemaRig->SolverDef.Bones)
			{
				bone.SourceName = NAME_None;
				bone.SourceIndex = INDEX_NONE;
			}

			SharedData->IKinemaRig->SolverDef.Tasks.Empty();
		}

		if (!MocapSourcePicker->IsRigidBodies())
		{
			SharedData->ImportActor(mocapServer, address, subject, templateName, port, importScale, MocapSourcePicker->IsYup(), ZeroRotations);
		}
		else
		{
			SharedData->ImportActor(mocapServer, address, subject, port, importScale, MocapSourcePicker->IsYup());
		}
		if (SharedData->IKinemaRig->Actor)
		{
			MocapSourcePicker->SetImportScale(SharedData->IKinemaRig->Actor->ImportScale);
		}
	}
	return FReply::Handled();
}

bool FIKinemaRigTool::CanImportActor() const
{
	bool enable = false;
	if (MocapSourcePicker.IsValid())
	{
		auto mocapServer = MocapSourcePicker->GetMocapServer();
		auto address = MocapSourcePicker->GetServerIP();
		auto subject = MocapSourcePicker->GetSubjectName();

		enable = (mocapServer != EMocapServer::None) && (!address.IsEmpty()) && (!subject.IsEmpty());
		if (MocapSourcePicker->GetMocapServer() == EMocapServer::LiveLink)
		{
			enable = !subject.IsEmpty();
		}
	}
	return enable;
}


TSharedRef<ITableRow> FIKinemaRigTool::OnGenerateRowForTree(FTreeElemPtr Item, const TSharedRef<STableViewBase>& OwnerTable)
{
	class SHoverDetectTableRow : public STableRow<FTreeElemPtr>
	{
	public:
		SLATE_BEGIN_ARGS(SHoverDetectTableRow)
		{}
		SLATE_DEFAULT_SLOT(FArguments, Content)
			SLATE_END_ARGS()

			void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& InOwnerTableView, FIKinemaRigTool * InIKinemaRigTool)
		{
			STableRow::Construct(

				STableRow::FArguments()
				[
					InArgs._Content.Widget
				]

			,

				InOwnerTableView
				);

			IKinemaRigTool = InIKinemaRigTool;
		}

		virtual void OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override
		{
			IKinemaRigTool->OnTreeHighlightChanged();
		}

		virtual void OnMouseLeave(const FPointerEvent& MouseEvent) override
		{
			IKinemaRigTool->OnTreeHighlightChanged();
		}

	private:
		FIKinemaRigTool * IKinemaRigTool;
	};

	return
		SNew(SHoverDetectTableRow, OwnerTable, this)
		[
			SNew(STextBlock)
			.Font((*Item).bBold ? FEditorStyle::GetFontStyle("BoldFont") : FEditorStyle::GetFontStyle("NormalFont"))
		.Text(FText::FromName((*Item).Name))
		];
}

void FIKinemaRigTool::OnTreeHighlightChanged()
{
}

void FIKinemaRigTool::OnGetChildrenForTree(FTreeElemPtr Parent, TArray<FTreeElemPtr>& OutChildren)
{

	const auto& RefSkeleton = SharedData->EditorSkelMesh->Skeleton->GetReferenceSkeleton();
	int32 ParentIndex = RefSkeleton.FindBoneIndex((*Parent).Name);
	for (int32 BoneIndex = 0; BoneIndex < RefSkeleton.GetNum(); ++BoneIndex)
	{
		const FMeshBoneInfo& Bone = RefSkeleton.GetRefBoneInfo()[BoneIndex];
		if (Bone.ParentIndex != INDEX_NONE)
		{
			const FMeshBoneInfo& ParentBone = RefSkeleton.GetRefBoneInfo()[Bone.ParentIndex];
			if ((BoneIndex != ParentIndex) && (ParentBone.Name == (*Parent).Name))
			{
				for (int32 i = 0; i < TreeElements.Num(); ++i)
				{
					if ((*TreeElements[i]).Name == Bone.Name)
					{
						if (FilterTreeElement(TreeElements[i]))
						{
							//normal element gets added
							if (SharedData->EditorSkelComp->GetBoneIndex(TreeElements[i]->Name) == INDEX_NONE)
							{
								TreeElements[i]->bBold = false;
							}
							OutChildren.Add(TreeElements[i]);
						}
						else
						{
							//we still need to see if any of this element's children get added
							OnGetChildrenForTree(TreeElements[i], OutChildren);
						}

					}
				}
			}
		}
	}
}

void FIKinemaRigTool::OnTreeSelectionChanged(FTreeElemPtr TreeElem, ESelectInfo::Type SelectInfo)
{
	// prevent re-entrancy
	if (InsideSelChanged)
	{
		return;
	}

	if (!TreeElem.IsValid())
	{
		//Nothing selected show the global solver properties.
		TArray<UObject*> Objs;
		Objs.Add(SharedData->IKinemaRig);
		SetPropertiesGroupSelection(Objs);
		return;
	}

	InsideSelChanged = true;

	TArray<FTreeElemPtr> SelectedElems = Hierarchy->GetSelectedItems();

	//clear selection first
	if (SelectedElems.Num() && SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
	{
		SharedData->SetSelectedBody(NULL);
		SharedData->SetSelectedSourceBone(INDEX_NONE);
	}
	else if (SelectedElems.Num() && SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_ConstraintEdit)
	{
		SharedData->SetSelectedConstraint(INDEX_NONE);
	}

	for (FTreeElemPtr SelectedElem : SelectedElems)
	{
		int32 ObjIndex = (*SelectedElem).BoneOrConstraintIdx;
		if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
		{
			if (ObjIndex != INDEX_NONE)
			{
				for (int32 i = 0; i < SharedData->IKinemaRig->SolverDef.Bones.Num(); ++i)
				{
					if (SharedData->IKinemaRig->SolverDef.Bones[i].Name == (*SelectedElem).Name)
					{

						SharedData->HitBone(i, true, false);
						break;
					}
				}
			}
			else
			{
				FIKinemaRigToolTreeInfo Info = *SelectedElem;
				if (Info.ParentBoneIdx != INDEX_NONE)
				{
					SharedData->HitBone(Info.BodyIdx, true, false);
				}
			}
		}
		else
		{
			if (ObjIndex != INDEX_NONE)
			{
				SharedData->HitConstraint(ObjIndex, true);
			}
		}
	}

	InsideSelChanged = false;
}

void FIKinemaRigTool::OnTreeDoubleClick(FTreeElemPtr TreeElem)
{
	//TODO: Add task if no task is available on the bone:

}

TSharedPtr<SWidget> FIKinemaRigTool::OnTreeRightClick()
{
	//If Rig Creation Mode, create show a menu to create Rig
	//If we have a rig ready, just show the ability to tune.

	if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
	{
		if (SharedData->SelectedBodies.Num())	//if we have anything selected just give us context menu of it
		{
			return BuildMenuWidgetBody(true);
		}
	}
	else
	{
		if (SharedData->SelectedConstraints.Num())	//if we have anything selected just give us context menu of it
		{
			return BuildMenuWidgetConstraint(true);
		}
	}

	return NULL;
}

TSharedPtr<SWidget> FIKinemaRigTool::BuildMenuWidgetBody(bool bHierarchy /*= false*/)
{
	if (!SharedData->GetSelectedBody())
	{
		return NULL;
	}

	const bool bShouldCloseWindowAfterMenuSelection = true;	// Set the menu to automatically close when the user commits to a choice
	FMenuBuilder MenuBuilder(bShouldCloseWindowAfterMenuSelection, GetToolkitCommands());
	{
		const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();

		struct FLocal
		{
			static void FillConstraintPreset(FMenuBuilder& InMenuBuilder)
			{
				const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();

				InMenuBuilder.BeginSection("IKinemaConstraintTypeActions", LOCTEXT("IKinemaConstraintTypeHeader", "Constraint from preset"));
				InMenuBuilder.AddMenuEntry(Commands.AddHipTask);
				InMenuBuilder.AddMenuEntry(Commands.AddFootTask);
				InMenuBuilder.AddMenuEntry(Commands.AddHandTask);
				InMenuBuilder.AddMenuEntry(Commands.AddHeadTask);
				InMenuBuilder.AddMenuEntry(Commands.AddChestTask);
				InMenuBuilder.AddMenuEntry(Commands.AddElbowTask);
				InMenuBuilder.AddMenuEntry(Commands.AddKneeTask);
				InMenuBuilder.EndSection();
				InMenuBuilder.EndSection();
			};

			static void FillCharacterisationPreset(FMenuBuilder& InMenuBuilder)
			{
				const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();

				InMenuBuilder.BeginSection("IKinemaCharacterisationTypeActions", LOCTEXT("IKinemaCharacterisationTypeHeader", "Bone Characterisation"));
				InMenuBuilder.AddMenuEntry(Commands.SetAsHips);
				InMenuBuilder.AddMenuEntry(Commands.SetAsChest);
				InMenuBuilder.AddMenuEntry(Commands.SetAsShoulder);
				InMenuBuilder.AddMenuEntry(Commands.SetAsElbow);
				InMenuBuilder.AddMenuEntry(Commands.SetAsHand);
				InMenuBuilder.AddMenuEntry(Commands.SetAsHead);
				InMenuBuilder.AddMenuEntry(Commands.SetAsFoot);
				InMenuBuilder.EndSection();
				InMenuBuilder.EndSection();
			}

		};

		MenuBuilder.BeginSection("BoneActions", LOCTEXT("BoneHeader", "Create Task"));
		MenuBuilder.AddSubMenu(LOCTEXT("IKinemaConstraintTypeMenu", "Constraint Type"), LOCTEXT("ConstraintTypeMenu_ToolTip", "IKINEMA Constraint Type"),
			FNewMenuDelegate::CreateStatic(&FLocal::FillConstraintPreset));
		MenuBuilder.AddMenuEntry(Commands.AddDualConstraint);
		MenuBuilder.AddMenuEntry(Commands.AddPositionalConstraint);
		MenuBuilder.AddMenuEntry(Commands.AddOrientationalConstraint);
		MenuBuilder.AddMenuEntry(Commands.ResetBoneSettings);
		MenuBuilder.EndSection();

		MenuBuilder.BeginSection("BodyActions", LOCTEXT("BodyHeader", "Body"));
		MenuBuilder.AddSubMenu(LOCTEXT("IKinemaCharacterisationMenu", "Characterisation"), LOCTEXT("CharacterisationMenu_ToolTip", "IKINEMA Bone Characterisation"),
			FNewMenuDelegate::CreateStatic(&FLocal::FillCharacterisationPreset));
		MenuBuilder.AddMenuEntry(Commands.SelectAllObjectsBelow);
		MenuBuilder.AddMenuEntry(Commands.CopyProperties, NAME_None, TAttribute<FText>(), TAttribute<FText>(Commands.CopyBones->GetDescription()), Commands.CopyBones->GetIcon());
		MenuBuilder.AddMenuEntry(Commands.PasteProperties);
		MenuBuilder.AddMenuEntry(Commands.DeleteBody);
		MenuBuilder.AddMenuEntry(Commands.LinkBones);
		if (bHierarchy)
		{
			MenuBuilder.AddMenuEntry(Commands.DeleteAllBodiesBelow);
		}
		MenuBuilder.EndSection();

	}

	return MenuBuilder.MakeWidget();
}

TSharedPtr<SWidget> FIKinemaRigTool::BuildMenuWidgetConstraint(bool bHierarchy /*= false*/)
{
	if (!SharedData->GetSelectedConstraint())
	{
		return NULL;
	}

	const bool bShouldCloseWindowAfterMenuSelection = true;	// Set the menu to automatically close when the user commits to a choice
	const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();
	FMenuBuilder MenuBuilder(bShouldCloseWindowAfterMenuSelection, GetToolkitCommands());
	{
		MenuBuilder.BeginSection("EditTypeActions", LOCTEXT("ConstraintEditTypeHeader", "Edit"));
		MenuBuilder.AddMenuEntry(Commands.CopyProperties, NAME_None, TAttribute<FText>(), TAttribute<FText>(Commands.CopyTasks->GetDescription()), Commands.CopyTasks->GetIcon());
		MenuBuilder.AddMenuEntry(Commands.PasteProperties);
		MenuBuilder.AddMenuEntry(Commands.ResetConstraint);
		MenuBuilder.AddMenuEntry(Commands.DeleteConstraint);
		MenuBuilder.EndSection();
	}

	return MenuBuilder.MakeWidget();
}

TSharedPtr<SWidget> FIKinemaRigTool::BuildMenuWidgetCoM()
{
	if (!SharedData->bIsCoMSelected)
	{
		return NULL;
	}

	const bool bShouldCloseWindowAfterMenuSelection = true;	// Set the menu to automatically close when the user commits to a choice
	const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();
	FMenuBuilder MenuBuilder(bShouldCloseWindowAfterMenuSelection, GetToolkitCommands());
	{
		MenuBuilder.BeginSection("EditTypeActions", LOCTEXT("ConstraintEditTypeHeader", "Edit"));
		MenuBuilder.AddMenuEntry(Commands.ResetCoMConstraint);
		//MenuBuilder.AddMenuEntry(Commands.DeleteConstraint);
		MenuBuilder.EndSection();
	}

	return MenuBuilder.MakeWidget();

}

void FIKinemaRigTool::AnimationSelectionChanged(UObject* Object)
{
	FEditorDelegates::LoadSelectedAssetsIfNeeded.Broadcast();

	USelection* AssetSelection = GEditor->GetSelectedObjects();
	if (AssetSelection && AssetSelection->Num() == 1)
	{
		UAnimBlueprint* AnimBlueprintToAssign = AssetSelection->GetTop<UAnimBlueprint>();
		if (AnimBlueprintToAssign)
		{
			SharedData->EditorSkelComp->SetAnimationMode(EAnimationMode::AnimationBlueprint);
			SharedData->EditorSkelComp->SetAnimInstanceClass(AnimBlueprintToAssign->GeneratedClass);
		}
	}

}

UObject* FIKinemaRigTool::GetSelectedAnimation() const
{
	return SelectedAnimation;
}


// Filter class for animation blueprint picker
class FAnimBlueprintFilter : public IClassViewerFilter
{
public:
	virtual bool IsClassAllowed(const FClassViewerInitializationOptions& InInitOptions, const UClass* InClass, TSharedRef< class FClassViewerFilterFuncs > InFilterFuncs) override
	{
		if (InFilterFuncs->IfInChildOfClassesSet(AllowedChildrenOfClasses, InClass) != EFilterReturn::Failed)
		{
			//

			const UAnimBlueprintGeneratedClass* AnimBlueprintToAssign = Cast<UAnimBlueprintGeneratedClass>(InClass);
			if (AnimBlueprintToAssign)
			{
				if (AnimBlueprintToAssign->TargetSkeleton && SkeletonName == AnimBlueprintToAssign->TargetSkeleton->GetFName())
				{
					return true;
				}
			}
		}
		return false;
	}

	virtual bool IsUnloadedClassAllowed(const FClassViewerInitializationOptions& InInitOptions, const TSharedRef< const class IUnloadedBlueprintData > InUnloadedClassData, TSharedRef< class FClassViewerFilterFuncs > InFilterFuncs) override
	{
		return false;
	}

	/** Only children of the classes in this set will be unfiltered */
	TSet<const UClass*> AllowedChildrenOfClasses;
	FName SkeletonName;
};

TSharedRef<SWidget> FIKinemaRigTool::GetClassPickerMenuContent()
{
	TSharedPtr<FAnimBlueprintFilter> Filter = MakeShareable(new FAnimBlueprintFilter);
	Filter->AllowedChildrenOfClasses.Add(UAnimInstance::StaticClass());
	Filter->SkeletonName = SharedData->IKinemaRig->Skeleton->GetFName();
	FClassViewerModule& ClassViewerModule = FModuleManager::LoadModuleChecked<FClassViewerModule>("ClassViewer");
	FClassViewerInitializationOptions InitOptions;
	InitOptions.Mode = EClassViewerMode::ClassPicker;
	InitOptions.ClassFilter = Filter;
	InitOptions.bShowNoneOption = true;
	InitOptions.bShowUnloadedBlueprints = false;

	return SNew(SBorder)
		.Padding(3)
		.BorderImage(FEditorStyle::GetBrush("Menu.Background"))
		.ForegroundColor(FEditorStyle::GetColor("DefaultForeground"))
		[
			SNew(SBox)
			.WidthOverride(280)
		[
			ClassViewerModule.CreateClassViewer(InitOptions, FOnClassPicked::CreateSP(this, &FIKinemaRigTool::OnClassPicked))
		]
		];
}

FText FIKinemaRigTool::GetSelectedAnimBlueprintName() const
{

	UObject* Object = SharedData->EditorSkelComp->GetAnimInstance() ? SharedData->EditorSkelComp->GetAnimInstance()->GetClass()->ClassGeneratedBy : nullptr;
	if (Object)
	{
		return FText::FromString(Object->GetName());
	}
	else
	{
		return LOCTEXT("None", "None");
	}
}

void FIKinemaRigTool::OnClassPicked(UClass* PickedClass)
{
	ClassPickerComboButton->SetIsOpen(false);
	if (PickedClass)
	{
		const UAnimBlueprintGeneratedClass* AnimBlueprintToAssign = Cast<UAnimBlueprintGeneratedClass>(PickedClass);
		if (AnimBlueprintToAssign)
		{
			if (AnimBlueprintToAssign->TargetSkeleton && SharedData->IKinemaRig->Skeleton->GetFName() == AnimBlueprintToAssign->TargetSkeleton->GetFName())
			{
				SharedData->EditorSkelComp->SetAnimationMode(EAnimationMode::AnimationBlueprint);
				SharedData->EditorSkelComp->SetAnimInstanceClass(PickedClass);
			}
		}
	}
	else
	{
		SharedData->EditorSkelComp->SetAnimInstanceClass(nullptr);
	}
}

void FIKinemaRigTool::OnBrowseToAnimBlueprint()
{
	UObject* Object = SharedData->EditorSkelComp->GetAnimInstance();

	TArray<UObject*> Objects;
	Objects.Add(Object);
	GEditor->SyncBrowserToObjects(Objects);
}

void FIKinemaRigTool::UseSelectedAnimBlueprint()
{
	FEditorDelegates::LoadSelectedAssetsIfNeeded.Broadcast();

	USelection* AssetSelection = GEditor->GetSelectedObjects();
	if (AssetSelection && AssetSelection->Num() == 1)
	{
		UAnimBlueprint* AnimBlueprintToAssign = AssetSelection->GetTop<UAnimBlueprint>();
		if (AnimBlueprintToAssign)
		{
			if (USkeleton* AnimBlueprintSkeleton = AnimBlueprintToAssign->TargetSkeleton)
			{
				FString BlueprintSkeletonName = FString::Printf(TEXT("%s'%s'"), *AnimBlueprintSkeleton->GetClass()->GetName(), *AnimBlueprintSkeleton->GetPathName());
				if (BlueprintSkeletonName == SharedData->IKinemaRig->Skeleton->GetName())
				{
					OnClassPicked(AnimBlueprintToAssign->GetAnimBlueprintGeneratedClass());
				}
			}
		}
	}
}

bool FIKinemaRigTool::IsNotSimulation() const
{
	return !SharedData->bRunningSimulation;
}

bool FIKinemaRigTool::IsEditBodyMode() const
{
	return IsNotSimulation() && (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit);
}

bool FIKinemaRigTool::IsSelectedEditBodyMode() const
{
	return IsEditBodyMode() && (SharedData->GetSelectedBody());
}

bool FIKinemaRigTool::IsEditConstraintMode() const
{
	return IsNotSimulation() && (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_ConstraintEdit);
}

bool FIKinemaRigTool::IsSelectedEditConstraintMode() const
{
	return IsEditConstraintMode() && (SharedData->GetSelectedConstraint());
}

bool FIKinemaRigTool::IsSelectedEditMode() const
{
	return IsSelectedEditBodyMode() || IsSelectedEditConstraintMode();
}

void FIKinemaRigTool::OnChangeDefaultMesh()
{
	// Get the currently selected SkeletalMesh. Fail if there ain't one.
	FEditorDelegates::LoadSelectedAssetsIfNeeded.Broadcast();

	USkeletalMesh* NewSkelMesh = GEditor->GetSelectedObjects()->GetTop<USkeletalMesh>();
	if (!NewSkelMesh)
	{
		FMessageDialog::Open(EAppMsgType::Ok, LOCTEXT("NoSkelMeshSelected", "No SkeletalMesh Selected.\nSelect the SkeletalMesh in the Content Browser that you want to use as the new Default SkeletalMesh for this IKINEMARig."));
		return;
	}

	// Confirm they want to do this.
	bool bDoChange = EAppReturnType::Yes == FMessageDialog::Open(EAppMsgType::YesNo,
		FText::Format(NSLOCTEXT("IKinemaRigTool", "SureChangeAssetSkelMesh", "Are you sure you want to change the IKINEMARig '{0}' to use the SkeletalMesh '{1}'?"),
			FText::FromString(SharedData->IKinemaRig->GetName()), FText::FromString(NewSkelMesh->GetName())));
	if (bDoChange)
	{
		// Change preview
		SharedData->EditorSkelMesh = NewSkelMesh;
		SharedData->EditorSkelComp->SetSkeletalMesh(NewSkelMesh);

		// Update Skeleton in the IKinemaRig
		SharedData->IKinemaRig->Skeleton = NewSkelMesh->Skeleton;

		auto& SolverDef = SharedData->IKinemaRig->SolverDef;
		if (SharedData->IKinemaRig->Skeleton && SharedData->EditorSkelComp)
		{
			float length(0.f);
			for (int32 i = 0; i < SolverDef.Bones.Num(); ++i)
			{
				const auto& Bone = SolverDef.Bones[i];
				FString BoneName = Bone.Name.ToString();
				BoneName.Split(":", nullptr, &BoneName);
				BoneName = BoneName.Replace(TEXT("FBXASC032"), TEXT("-"));
				auto Name = FName(*BoneName);
				const int32 BoneIndex = SharedData->EditorSkelComp->GetBoneIndex(Name);
				if (BoneIndex != INDEX_NONE)
				{
					const int32 ParentIndex = NewSkelMesh->Skeleton->GetReferenceSkeleton().GetParentIndex(BoneIndex);
					FVector Start, End;

					FTransform WorldTransforms = SharedData->EditorSkelComp->GetComponentSpaceTransforms()[BoneIndex] * SharedData->EditorSkelComp->GetComponentTransform();
					if (ParentIndex >= 0)
					{
						Start = (SharedData->EditorSkelComp->GetComponentSpaceTransforms()[ParentIndex] * SharedData->EditorSkelComp->GetComponentTransform()).GetLocation();
						End = WorldTransforms.GetLocation();
					}
					else
					{
						Start = FVector::ZeroVector;
						End = WorldTransforms.GetLocation();
					}

					length += (End - Start).Size();
				}
			}

			SharedData->AverageBoneLength = length / SolverDef.Bones.Num();
		}

		RefreshHierachyTree();
		// Mark asset's package as dirty as its changed.
		SharedData->IKinemaRig->MarkPackageDirty();
	}
}

void FIKinemaRigTool::OnSourceSelection()
{
	if (SharedData->IKinemaRig->Actor) {
		SharedData->SetSelectedSourceBone(0);
	}
	else {
		FMessageDialog::Debugf(NSLOCTEXT("IKinemaRigTool", "CannotEditMocapActor", "No mocap actor skeleton imported so cannot select root"));
	}
}


void FIKinemaRigTool::OnEditingMode(int32 Mode)
{

	switch (Mode)
	{
	case FIKinemaRigToolSharedData::PEM_BodyEdit:
		SharedData->EditingMode = FIKinemaRigToolSharedData::PEM_BodyEdit;
		SetHierarchyFilter(EIKinemaRigToolHierarchyFilterMode::PHFM_All);
		RefreshHierachyTree();
		break;
	case FIKinemaRigToolSharedData::PEM_ConstraintEdit:
		SharedData->EditingMode = FIKinemaRigToolSharedData::PEM_ConstraintEdit;
		SetHierarchyFilter(EIKinemaRigToolHierarchyFilterMode::PHFM_Bodies);
		RefreshHierachyTree();
		break;
	default:
		FMessageDialog::Debugf(NSLOCTEXT("IKinemaRigTool", "SureChangeAssetSkelMesh", "Editing mode error"));
	}
	SharedData->SetSelectedBody(NULL); // Forces properties panel to update...
	SharedData->SetSelectedConstraint(INDEX_NONE);
	RefreshPreviewViewport();

	// Rebuild the toolbar, as the icons shown will have changed
	ExtendToolbar();

	OnAddIKinemaRigToolRecord(TEXT("ModeSelected"), false, true);
}

bool FIKinemaRigTool::IsEditingMode(int32 Mode) const
{
	return (FIKinemaRigToolSharedData::EIKinemaRigToolEditingMode)Mode == SharedData->EditingMode;
}

void FIKinemaRigTool::OnCopyProperties()
{
	switch (SharedData->EditingMode)
	{
	case FIKinemaRigToolSharedData::PEM_BodyEdit:
		SharedData->CopyBody();
		break;
	case FIKinemaRigToolSharedData::PEM_ConstraintEdit:

		SharedData->CopyConstraint();
		break;
	default:
		FMessageDialog::Debugf(NSLOCTEXT("IKinemaRigTool", "SureChangeAssetSkelMesh", "Editing mode error"));
	}

	RefreshPreviewViewport();
}

void FIKinemaRigTool::OnPasteProperties()
{
	switch (SharedData->EditingMode)
	{
	case FIKinemaRigToolSharedData::PEM_BodyEdit:
		SharedData->PasteBodyProperties();
		break;
	case FIKinemaRigToolSharedData::PEM_ConstraintEdit:

		SharedData->PasteConstraintProperties();
		break;
	default:
		FMessageDialog::Debugf(NSLOCTEXT("IKinemaRigTool", "SureChangeAssetSkelMesh", "Editing mode error"));
	}


}

bool FIKinemaRigTool::CanCopyProperties() const
{
	if (IsSelectedEditMode())
	{
		if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit && SharedData->SelectedBodies.Num() == 1)
		{
			return true;
		}
		else if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_ConstraintEdit && SharedData->SelectedConstraints.Num() == 1)
		{
			return true;
		}
	}

	return false;
}

bool FIKinemaRigTool::CanPasteProperties() const
{
	return IsSelectedEditMode() && IsCopyProperties();
}

bool FIKinemaRigTool::IsCopyProperties() const
{
	return (SharedData->CopiedBodySetup && SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit) || (SharedData->CopiedConstraintTemplate && SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_ConstraintEdit);
}

void FIKinemaRigTool::ImpToggleSimulation()
{
	TSharedPtr<FIKinemaRigToolEdPreviewViewportClient> PreviewClient = PreviewViewport->GetViewportClient();

	SharedData->ToggleSimulation();

	if (!PreviewViewport->GetViewportClient()->IsRealtime() && !IsPIERunning())
	{
		PreviewViewport->GetViewportClient()->SetRealtime(true);
	}
	OnEditingMode(FIKinemaRigToolSharedData::PEM_ConstraintEdit);
	// add to analytics record
	OnAddIKinemaRigToolRecord(TEXT("ToggleSimulate"), true, true);
}



void FIKinemaRigTool::OnToggleSimulation()
{
	ImpToggleSimulation();
}

bool FIKinemaRigTool::IsToggleSimulation() const
{
	return SharedData->bRunningSimulation;
}

void FIKinemaRigTool::OnMeshRenderingMode(FIKinemaRigToolSharedData::EIKinemaRigToolRenderMode Mode)
{
	if (SharedData->bRunningSimulation)
	{
		SharedData->Sim_MeshViewMode = Mode;
	}
	else if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
	{
		SharedData->BodyEdit_MeshViewMode = Mode;
	}
	else
	{
		SharedData->ConstraintEdit_MeshViewMode = Mode;
	}

	RefreshPreviewViewport();
}

bool FIKinemaRigTool::IsMeshRenderingMode(FIKinemaRigToolSharedData::EIKinemaRigToolRenderMode Mode) const
{
	return Mode == SharedData->GetCurrentMeshViewMode();
}

void FIKinemaRigTool::OnShowNonActive()
{
	SharedData->bShowNonActive = !SharedData->bShowNonActive;

	RefreshPreviewViewport();
}

bool FIKinemaRigTool::IsShowNonActive() const
{
	return SharedData->bShowNonActive;
}

void FIKinemaRigTool::OnDrawGroundBox()
{
	SharedData->bDrawGround = !SharedData->bDrawGround;

	RefreshPreviewViewport();
}

bool FIKinemaRigTool::IsDrawGroundBox() const
{
	return SharedData->bDrawGround;
}

void FIKinemaRigTool::OnToggleGraphicsHierarchy()
{
	SharedData->bShowHierarchy = !SharedData->bShowHierarchy;

	RefreshPreviewViewport();
}

bool FIKinemaRigTool::IsToggleGraphicsHierarchy() const
{
	return SharedData->bShowHierarchy;
}

void FIKinemaRigTool::OnShowConstraints()
{
	SharedData->bShowConstraints = !SharedData->bShowConstraints;

	RefreshPreviewViewport();
}

bool FIKinemaRigTool::IsShowConstraints() const
{
	return SharedData->bShowConstraints;
}



bool FIKinemaRigTool::CanAddConstraint() const
{
	return IsEditBodyMode() && (SharedData->IKinemaRig->Actor != nullptr) && (SharedData->GetSelectedBody() != nullptr);
}

void FIKinemaRigTool::OnAddPositionalConstraint()
{
	SharedData->AddConstraint(FIKinemaRigToolSharedData::PTT_Positional);
	RefreshPreviewViewport();
}

void FIKinemaRigTool::OneAddOrientationalConstraint()
{
	SharedData->AddConstraint(FIKinemaRigToolSharedData::PTT_Orientational);
	RefreshPreviewViewport();
}

void FIKinemaRigTool::OnAddHipTask()
{
	SharedData->AddConstraint(FIKinemaRigToolSharedData::PTT_Hip);
	RefreshPreviewViewport();
}

void FIKinemaRigTool::OnAddElbowTask()
{
	SharedData->AddConstraint(FIKinemaRigToolSharedData::PTT_Elbow);
	RefreshPreviewViewport();
}

void FIKinemaRigTool::OnAddFootTask()
{
	SharedData->AddConstraint(FIKinemaRigToolSharedData::PTT_Foot);
	RefreshPreviewViewport();
}

void FIKinemaRigTool::OnAddHandTask()
{
	SharedData->AddConstraint(FIKinemaRigToolSharedData::PTT_Hand);
	RefreshPreviewViewport();
}

void FIKinemaRigTool::OnAddHeadTask()
{
	SharedData->AddConstraint(FIKinemaRigToolSharedData::PTT_Head);
	RefreshPreviewViewport();
}

void FIKinemaRigTool::OnAddChestTask()
{
	SharedData->AddConstraint(FIKinemaRigToolSharedData::PTT_Chest);
	RefreshPreviewViewport();
}

void FIKinemaRigTool::OnAddKneeTask()
{
	SharedData->AddConstraint(FIKinemaRigToolSharedData::PTT_Knee);
	RefreshPreviewViewport();
}

void FIKinemaRigTool::OnCharacteriseHip()
{
	const auto& Name = SharedData->IKinemaRig->SolverDef.Bones[SharedData->GetSelectedBody()->Index].Name;
	SharedData->IKinemaRig->CharacteriseBone(Name, EIKinemaCharacterDefinition::EHIPS);
}
void FIKinemaRigTool::OnCharacteriseChest()
{
	const auto& Name = SharedData->IKinemaRig->SolverDef.Bones[SharedData->GetSelectedBody()->Index].Name;
	SharedData->IKinemaRig->CharacteriseBone(Name, EIKinemaCharacterDefinition::ECHEST);
}
void FIKinemaRigTool::OnCharacteriseHead()
{
	const auto& Name = SharedData->IKinemaRig->SolverDef.Bones[SharedData->GetSelectedBody()->Index].Name;
	SharedData->IKinemaRig->CharacteriseBone(Name, EIKinemaCharacterDefinition::EHEAD);
}
void FIKinemaRigTool::OnCharacteriseShoulder()
{
	const auto& Name = SharedData->IKinemaRig->SolverDef.Bones[SharedData->GetSelectedBody()->Index].Name;
	SharedData->IKinemaRig->CharacteriseBone(Name, EIKinemaCharacterDefinition::ESHOULDER);
}
void FIKinemaRigTool::OnCharacteriseElbow()
{
	const auto& Name = SharedData->IKinemaRig->SolverDef.Bones[SharedData->GetSelectedBody()->Index].Name;
	SharedData->IKinemaRig->CharacteriseBone(Name, EIKinemaCharacterDefinition::EELBOW);
}
void FIKinemaRigTool::OnCharacteriseHand()
{
	const auto& Name = SharedData->IKinemaRig->SolverDef.Bones[SharedData->GetSelectedBody()->Index].Name;
	SharedData->IKinemaRig->CharacteriseBone(Name, EIKinemaCharacterDefinition::EHAND);
}
void FIKinemaRigTool::OnCharacteriseFoot()
{
	const auto& Name = SharedData->IKinemaRig->SolverDef.Bones[SharedData->GetSelectedBody()->Index].Name;
	SharedData->IKinemaRig->CharacteriseBone(Name, EIKinemaCharacterDefinition::EFOOT);
}


void FIKinemaRigTool::OnAddConstraint()
{
	SharedData->AddConstraint(FIKinemaRigToolSharedData::PTT_Dual);
	RefreshPreviewViewport();
}

void FIKinemaRigTool::OnResetConstraint()
{
	//Reset Constraint to default settings
	SharedData->ResetSelectedConstraint();

	RefreshPreviewViewport();
}

void FIKinemaRigTool::OnResetCoMConstraint()
{
	//Reset Constraint to default settings
	SharedData->ResetCoMConstraint();

	RefreshPreviewViewport();
}


void FIKinemaRigTool::OnLinkBones()
{
	SharedData->LinkBones();

}

bool FIKinemaRigTool::CanLinkBones() const
{

	if (SharedData->SelectedBodies.Num() == 1)
	{
		return true;
	}

	return false;
}

void FIKinemaRigTool::OnMapBones()
{
	//TODO:: Spawn Retarget Map Tab
	/*const TSharedRef<FTabManager::FLayout> StandaloneDefaultLayout = FTabManager::NewLayout("Standalone_IKinemaRigTool_Layout_v2")
	->AddArea
	(
	FTabManager::NewPrimaryArea()
	->SetOrientation(Orient_Vertical)
	->AddTab()
	)*/
	TabManager->InvokeTab(IKinemaRigToolBoneMapperName);
}

bool FIKinemaRigTool::CanMapBones() const
{
	return (SharedData->IKinemaRig->Actor) ? true : false;
}

void FIKinemaRigTool::OnLicense()
{
	TSharedRef<SIKinemaLicenseDialog> Dialog = SNew(SIKinemaLicenseDialog);
	bool success = Dialog->ConfigureProperties();
}

void FIKinemaRigTool::OnCharacterise()
{
	SharedData->bShowCollisionShapes = true;

	TabManager->InvokeTab(IKinemaRigToolCharacterisationName);

}

void FIKinemaRigTool::OnResetBone()
{
	SharedData->ResetSelectedBone();
	RefreshPreviewViewport();
}

void FIKinemaRigTool::OnDeleteConstraint()
{
	SharedData->DeleteCurrentConstraint();
}

void FIKinemaRigTool::OnPlayAnimation()
{
	if (!SharedData->bIsPlaying && SharedData->bRunningSimulation)
	{
		SharedData->bIsPlaying = true;
	}
	else
	{
		SharedData->bIsPlaying = false;
	}
}

bool FIKinemaRigTool::IsPlayAnimation() const
{
	return SharedData->bIsPlaying;
}

void FIKinemaRigTool::OnViewType(ELevelViewportType ViewType)
{
	if (PreviewViewport.IsValid())
	{
		PreviewViewport->SetViewportType(ViewType);
	}
}

void FIKinemaRigTool::OnShowSkeleton()
{
	SharedData->bShowAnimSkel = !SharedData->bShowAnimSkel;

	RefreshPreviewViewport();
}

bool FIKinemaRigTool::IsShowSkeleton() const
{
	return SharedData->bShowAnimSkel;
}


void FIKinemaRigTool::OnShowCollisionShape()
{
	SharedData->bShowCollisionShapes = !SharedData->bShowCollisionShapes;
	SharedData->bShowConstraints = !SharedData->bShowCollisionShapes;
	RefreshPreviewViewport();
}

bool FIKinemaRigTool::IsShowCollisionShape() const
{
	return SharedData->bShowCollisionShapes;
}

void FIKinemaRigTool::OnDeleteBody()
{
	const FScopedTransaction Transaction(LOCTEXT("DeleteBodies", "Delete Bodies"));

	auto selectedBodies = SharedData->SelectedBodies;
	selectedBodies.Sort([](const FIKinemaRigToolSharedData::FSelection& body1, const FIKinemaRigToolSharedData::FSelection& body2) {
		return body1.Index < body2.Index;
	});
	while (selectedBodies.Num() > 0)
	{
		auto bodytoDelete = selectedBodies.Pop();
		SharedData->SetSelectedBody(&bodytoDelete);
		SharedData->DeleteBody();
		SharedData->SetSelectedBody(nullptr);
	}

	SharedData->HierarchyChangedEvent.Broadcast();
	SharedData->PreviewChangedEvent.Broadcast();
	return;

}

void FIKinemaRigTool::OnDeleteAllBodiesBelow()
{
	if (SharedData->EditingMode != FIKinemaRigToolSharedData::PEM_BodyEdit || !SharedData->GetSelectedBody())
	{
		return;
	}

	auto selectedBodies = SharedData->SelectedBodies;
	selectedBodies.Sort([](const FIKinemaRigToolSharedData::FSelection& body1, const FIKinemaRigToolSharedData::FSelection& body2) {
		return body1.Index < body2.Index;
	});

	for (int i = selectedBodies.Num() - 1; i >= 0; i--)
	{
		auto boneIndex = selectedBodies[i].Index;
		auto& boneName = SharedData->IKinemaRig->SolverDef.Bones[boneIndex].Name;
		auto& RefSkel = SharedData->IKinemaRig->Skeleton->GetReferenceSkeleton();
		int32 BaseIndex = SharedData->IKinemaRig->Skeleton->GetReferenceSkeleton().FindBoneIndex(boneName);

		TArray<int32> BoneIndices;
		BoneIndices.Add(boneIndex);
		// Iterate over all other bodies, looking for 'children' of this one
		for (int32 i = 0; i < SharedData->IKinemaRig->SolverDef.Bones.Num(); i++)
		{
			FName TestName = SharedData->IKinemaRig->SolverDef.Bones[i].Name;
			int32 TestIndex = RefSkel.FindBoneIndex(TestName);

			if (RefSkel.BoneIsChildOf(TestIndex, BaseIndex))
			{
				BoneIndices.Add(i);
			}
		}
		//These are indices into an array, we must remove it from greatest to smallest so that the indices don't shift
		BoneIndices.Sort();
		const FScopedTransaction Transaction(NSLOCTEXT("IKinemaRigTool", "DeleteAllBelow", "Delete All Bodies Below"));
		for (int32 i = BoneIndices.Num() - 1; i >= 0; i--)
		{
			SharedData->DeleteBody(BoneIndices[i]);
		}
		SharedData->SetSelectedBody(nullptr);
		SharedData->HierarchyChangedEvent.Broadcast();
		SharedData->PreviewChangedEvent.Broadcast();
	}
}


void FIKinemaRigTool::OnSelectAllBelow()
{
	if (SharedData->EditingMode != FIKinemaRigToolSharedData::PEM_BodyEdit || !SharedData->GetSelectedBody())
	{
		return;
	}

	auto DelBodyIndex = SharedData->GetSelectedBody()->Index;
	auto boneName = SharedData->IKinemaRig->SolverDef.Bones[DelBodyIndex].Name;
	auto& RefSkel = SharedData->IKinemaRig->Skeleton->GetReferenceSkeleton();
	int32 BaseIndex = RefSkel.FindBoneIndex(boneName);

	// Iterate over all other bodies, looking for 'children' of this one
	for (int32 i = 0; i < SharedData->IKinemaRig->SolverDef.Bones.Num(); i++)
	{
		FName TestName = SharedData->IKinemaRig->SolverDef.Bones[i].Name;
		int32 TestIndex = RefSkel.FindBoneIndex(TestName);

		if (RefSkel.BoneIsChildOf(TestIndex, BaseIndex))
		{
			FIKinemaRigToolSharedData::FSelection body(i);
			SharedData->SetSelectedBody(&body, true, false);
		}
	}

}

bool FIKinemaRigTool::CanSelectAllBelow()
{
	if (SharedData->EditingMode != FIKinemaRigToolSharedData::PEM_BodyEdit || !SharedData->GetSelectedBody())
	{
		return false;
	}

	//Only show this option if the selected bone has children
	auto DelBodyIndex = SharedData->GetSelectedBody()->Index;
	auto boneName = SharedData->IKinemaRig->SolverDef.Bones[DelBodyIndex].Name;
	auto& RefSkel = SharedData->IKinemaRig->Skeleton->GetReferenceSkeleton();
	int32 BaseIndex = RefSkel.FindBoneIndex(boneName);

	for (int32 i = 0; i < SharedData->IKinemaRig->SolverDef.Bones.Num(); i++)
	{
		FName TestName = SharedData->IKinemaRig->SolverDef.Bones[i].Name;
		int32 TestIndex = RefSkel.FindBoneIndex(TestName);

		if (RefSkel.BoneIsChildOf(TestIndex, BaseIndex))
		{
			return true;
		}
	}
	return false;
}

void FIKinemaRigTool::OnLockSelection()
{
	SharedData->bSelectionLock = !SharedData->bSelectionLock;
}

void FIKinemaRigTool::OnDeleteSelection()
{
	switch (SharedData->EditingMode)
	{
		//Delete constraint
	case FIKinemaRigToolSharedData::PEM_ConstraintEdit:
	{
		SharedData->DeleteCurrentConstraint();
	}
	break;
	case FIKinemaRigToolSharedData::PEM_BodyEdit:
	{
		OnDeleteBody();
	}
	break;
	/** Add new editor modes here */
	default:
		break;
	}
}

void FIKinemaRigTool::OnFocusSelection()
{
	//TODO: Add focus on selection command
	switch (SharedData->EditingMode)
	{
	case FIKinemaRigToolSharedData::PEM_BodyEdit:
	{
		if (SharedData->GetSelectedBody())
		{
			int32 BoneIdx = SharedData->EditorSkelComp->GetBoneIndex(SharedData->IKinemaRig->SolverDef.Bones[SharedData->GetSelectedBody()->Index].Name);
			FMatrix BoneTransform = SharedData->EditorSkelComp->GetBoneMatrix(BoneIdx);
			FBoxSphereBounds Bounds(BoneTransform.GetOrigin(), FVector(20), 20);

			PreviewViewport->GetViewportClient()->FocusViewportOnBox(Bounds.GetBox());
		}
	}
	break;
	case FIKinemaRigToolSharedData::PEM_ConstraintEdit:
	{
		if (SharedData->GetSelectedConstraint())
		{
			//FTransform ConstraintTransform = SharedData->GetConstraintMatrix(SharedData->GetSelectedConstraint()->Index, EConstraintFrame::Frame2, 1.0f);
			int32 rigBoneIndex = SharedData->IKinemaRig->SolverDef.Tasks[SharedData->GetSelectedConstraint()->Index].BoneIndex;
			int32 BoneIdx = SharedData->EditorSkelComp->GetBoneIndex(SharedData->IKinemaRig->SolverDef.Bones[rigBoneIndex].Name);
			FMatrix BoneTransform = SharedData->EditorSkelComp->GetBoneMatrix(BoneIdx);

			FBoxSphereBounds Bounds(BoneTransform.GetOrigin(), FVector(20), 20);

			PreviewViewport->GetViewportClient()->FocusViewportOnBox(Bounds.GetBox());
		}
	}
	break;
	/** Add any new edit modes here */
	default:
		break;
	}
}


TSharedRef<SWidget> FIKinemaRigTool::BuildHierarchyFilterMenu()
{
	const bool bShouldCloseWindowAfterMenuSelection = true;	// Set the menu to automatically close when the user commits to a choice
	const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();
	FMenuBuilder MenuBuilder(bShouldCloseWindowAfterMenuSelection, GetToolkitCommands());
	{
		MenuBuilder.AddMenuEntry(Commands.HierarchyFilterAll);
		MenuBuilder.AddMenuEntry(Commands.HierarchyFilterBodies);
	}

	return MenuBuilder.MakeWidget();
}

TSharedRef<SWidget> FIKinemaRigTool::BuildMocapMenu()
{
	const bool bShouldCloseWindowAfterMenuSelection = true;	// Set the menu to automatically close when the user commits to a choice
	const FIKinemaRigToolCommands& Commands = FIKinemaRigToolCommands::Get();
	FMenuBuilder MenuBuilder(bShouldCloseWindowAfterMenuSelection, GetToolkitCommands());
	{
		//MenuBuilder.AddMenuEntry();
		//MenuBuilder.AddMenuEntry(Commands.HierarchyFilterBodies);
	}

	return MenuBuilder.MakeWidget();
}

FText FIKinemaRigTool::GetHierarchyFilter() const
{
	FText FilterMenuText;

	switch (HierarchyFilterMode)
	{
	case PHFM_All:
		FilterMenuText = FIKinemaRigToolCommands::Get().HierarchyFilterAll->GetLabel();
		break;
	case PHFM_Bodies:
		FilterMenuText = FIKinemaRigToolCommands::Get().HierarchyFilterBodies->GetLabel();
		break;
	default:
		// Unknown mode
		check(0);
		break;
	}

	return FilterMenuText;

}

void FIKinemaRigTool::SaveAsset_Execute()
{
	if (SharedData.IsValid() && SharedData->IKinemaRig && SharedData->IKinemaRig->Actor)
	{
		AddEditingObject(SharedData->IKinemaRig->Actor);
	}
	FAssetEditorToolkit::SaveAsset_Execute();
	if (SharedData.IsValid() && SharedData->IKinemaRig && SharedData->IKinemaRig->Actor)
	{
		RemoveEditingObject(SharedData->IKinemaRig->Actor);
	}
}



bool FIKinemaRigTool::CanStartSimulation() const
{
	return !IsPIERunning();
}

bool FIKinemaRigTool::IsPIERunning()
{
	for (const FWorldContext& Context : GEngine->GetWorldContexts())
	{
		if (Context.World()->IsPlayInEditor())
		{
			return true;
		}
	}

	return false;
}

void FIKinemaRigTool::SetHierarchyFilter(EIKinemaRigToolHierarchyFilterMode Mode)
{
	HierarchyFilterMode = Mode;
	if (Mode == EIKinemaRigToolHierarchyFilterMode::PHFM_All)
	{
		SharedData->EditingMode = FIKinemaRigToolSharedData::PEM_BodyEdit;
	}
	else
	{
		SharedData->EditingMode = FIKinemaRigToolSharedData::PEM_ConstraintEdit;
	}

	SharedData->SetSelectedBody(NULL); // Forces properties panel to update...
	SharedData->SetSelectedConstraint(INDEX_NONE);

	RefreshHierachyTree();
	RefreshHierachyTreeSelection();
}

void FIKinemaRigTool::OnSelectAll()
{
	FMessageDialog::Open(EAppMsgType::Ok, (NSLOCTEXT("IKinemaRigTool", "Error_IKinemaRigNotImplemented", "Warning: Function not implemented")));


	if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
	{
		//Bodies
		//first deselect everything
		SharedData->SetSelectedBody(NULL);
	}
	else
	{
		//Constraints
		//Deselect everything first
		SharedData->SetSelectedConstraint(INDEX_NONE);
	}
}

// record if simulating or not, or mode changed or not, or what mode it is in while simulating and what kind of simulation options
void FIKinemaRigTool::OnAddIKinemaRigToolRecord(const FString& Action, bool bRecordSimulate, bool bRecordMode)
{
	// Don't attempt to report usage stats if analytics isn't available
	if (Action.IsEmpty() == false && SharedData.IsValid() && FEngineAnalytics::IsAvailable())
	{
		TArray<FAnalyticsEventAttribute> Attribs;
		if (bRecordSimulate)
		{
			Attribs.Add(FAnalyticsEventAttribute(TEXT("Simulation"), SharedData->bRunningSimulation ? TEXT("ON") : TEXT("OFF")));
		}

		if (bRecordMode)
		{
			Attribs.Add(FAnalyticsEventAttribute(TEXT("EditMode"), SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_ConstraintEdit ? TEXT("Constraint") : TEXT("Body")));
		}

		FString EventString = FString::Printf(TEXT("Editor.Usage.IKinemaRigTool.%s"), *Action);
		FEngineAnalytics::GetProvider().RecordEvent(EventString, Attribs);
	}
}

// only during simulation
bool FIKinemaRigTool::IsRecordAvailable() const
{
	// make sure mesh exists
	return (SharedData->EditorSkelComp && SharedData->EditorSkelComp->SkeletalMesh && SharedData->bRunningSimulation);
}

FSlateIcon FIKinemaRigTool::GetRecordStatusImage() const
{
	/*
	if(SharedData->Recorder.InRecording())
	{
	return FSlateIcon(FEditorStyle::GetStyleSetName(), "Persona.StopRecordAnimation");
	}
	*/

	return FSlateIcon(FEditorStyle::GetStyleSetName(), "Persona.StartRecordAnimation");
}

FText FIKinemaRigTool::GetRecordMenuLabel() const
{
	/*
	if(SharedData->Recorder.InRecording())
	{
	return LOCTEXT("Persona_StopRecordAnimationMenuLabel", "Stop Record Animation");
	}
	*/

	return LOCTEXT("Persona_StartRecordAnimationLabel", "Start Record Animation");
}

FText FIKinemaRigTool::GetRecordStatusLabel() const
{
	/*
	if(SharedData->Recorder.InRecording())
	{
	return LOCTEXT("Persona_StopRecordAnimationLabel", "Stop");
	}*/

	return LOCTEXT("Persona_StartRecordAnimationLabel", "Record");
}

FText FIKinemaRigTool::GetRecordStatusTooltip() const
{
	/*
	if(SharedData->Recorder.InRecording())
	{
	return LOCTEXT("Persona_StopRecordAnimation", "Stop Record Animation");
	}
	*/
	return LOCTEXT("Persona_StartRecordAnimation", "Start Record Animation");
}

void FIKinemaRigTool::RecordAnimation()
{
	return;
	if (!SharedData->EditorSkelComp || !SharedData->EditorSkelComp->SkeletalMesh)
	{
		// error
		return;
	}
	/*
	if(SharedData->Recorder.InRecording())
	{
	UAnimSequence * AnimSeq = SharedData->Recorder.StopRecord(true);

	// open the asset?
	if (AnimSeq)
	{
	TArray<UObject*> ObjectsToSync;
	ObjectsToSync.Add(AnimSeq);

	FContentBrowserModule& ContentBrowserModule = FModuleManager::Get().LoadModuleChecked<FContentBrowserModule>("ContentBrowser");
	ContentBrowserModule.Get().SyncBrowserToAssets(ObjectsToSync, true);
	}
	}
	else
	{
	SharedData->Recorder.TriggerRecordAnimation(SharedData->EditorSkelComp);
	}
	*/
}

void FIKinemaRigTool::Tick(float DeltaTime)
{
}

TStatId FIKinemaRigTool::GetStatId() const
{
	RETURN_QUICK_DECLARE_CYCLE_STAT(FIKinemaRigTool, STATGROUP_Tickables);
}

#undef LOCTEXT_NAMESPACE
