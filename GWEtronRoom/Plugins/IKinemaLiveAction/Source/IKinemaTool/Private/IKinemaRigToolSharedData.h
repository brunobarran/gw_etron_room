/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include "PreviewScene.h"
#include "Templates.h"
#include "IKinemaRig.h"

class UIKinemaRigToolEdSkeletalMeshComponent;
class UPhysicsHandleComponent;
class FLiveLinkModule;

#define DEBUG_CLICK_VIEWPORT 0

/*-----------------------------------------------------------------------------
FIKinemaRigToolSharedData
-----------------------------------------------------------------------------*/
class FIKinemaRigToolSharedData
{
public:
	/** Constructor/Destructor */
	FIKinemaRigToolSharedData();
	virtual ~FIKinemaRigToolSharedData();

	enum EIKinemaRigToolEditingMode
	{
		PEM_BodyEdit,
		PEM_ConstraintEdit,
		PEM_SelfCollisionEdit
	};

	enum EIKinemaRigToolRenderMode
	{
		PRM_Solid,
		PRM_Wireframe,
		PRM_None
	};

	enum EIKinemaRigToolTaskType
	{
		PTT_Positional,
		PTT_Orientational,
		PTT_Dual,
		PTT_Hip,
		PTT_Foot,
		PTT_Hand,
		PTT_Head,
		PTT_Chest,
		PTT_Knee,
		PTT_Elbow
	};

	struct FSelection
	{
		int32 Index;

		FTransform WidgetTM;
		FTransform ManipulateTM;

		FSelection(int32 GivenBodyIndex) :
			Index(GivenBodyIndex), WidgetTM(FTransform::Identity),
			ManipulateTM(FTransform::Identity)
		{
		}

		bool operator==(const FSelection& rhs) const
		{
			return Index == rhs.Index;
		}
	};


	/** Initializes members */
	void Initialize();

	void GenerateIKShapes();

	/** Accessors */
	EIKinemaRigToolRenderMode GetCurrentMeshViewMode();


	/** Constraint editing */
	void SetSelectedConstraint(int32 ConstraintIndex, bool bGroupSelect = false);
	void SetSelectedCoMConstraint(bool bGroupSelect /*= false*/);
	void ResetSelectedConstraint();
	void ResetCoMConstraint();
	void ResetSelectedBone();
	void DeleteAllConstraints();
	void DeleteCurrentConstraint();
	void DeleteObject(UObject* obj);
	void AddConstraint(EIKinemaRigToolTaskType TaskType);
	void LinkBones();
	void LinkBones(FIKinemaBoneDef& target, FMocapBone& source);
	void CopyConstraint();
	void PasteConstraintProperties();
	void CalculateTaskOffsets(FIKinemaTaskDef& Task, const FIKinemaBoneDef& Bone);

	/** Collision geometry editing */
	void SetSelectedBody(const FSelection* Body, bool bGroupSelect = false, bool bGroupSelectRemove = true);// int32 BodyIndex, EKCollisionPrimitiveType PrimitiveType, int32 PrimitiveIndex);
	void DeleteBody();
	void DeleteBody(int32 DelBodyIndex);
	void RefreshIKinemaRigAssetChange(const UIKinemaRig* InIKinemaRig);
	void CopyBody();
	void PasteBodyProperties();
	void SetSelectedSourceBone(int32 SourceBoneIndex, bool bGroupSelect = false);
	/** Import Mocap Actor */
	void ImportActor(EMocapServer ServerType, const FString& address, const FString& subject, const FString& templateName, const int port, const float importScale, bool IsYUp = false, bool ZeroRotations = false);
	/** Import All rigid bodies in the volume*/
	void ImportActor(EMocapServer ServerType, const FString& address, const FString & subject, const int port, const float importScale, bool IsYUp = false);
	void AutoScaleSource();
	/** Misc */
	void ToggleSimulation();
	void HitBone(int32 BodyIndex, bool bGroupSelect = false, bool bGroupSelectRemove = true);
	void HitSourceBone(int32 BodyIndex, bool bGroupSelect = false);
	void HitConstraint(int32 ConstraintIndex, bool bGroupSelect = false);
	void HitCoMConstraint(bool bGroupSelect);
	/** Undo/Redo */
	void PostUndo();
	void Redo();

public:
	/** Callback for handling selection changes */
	DECLARE_EVENT_TwoParams(FIKinemaRigToolSharedData, FSelectionChanged, UObject*, FSelection *);
	FSelectionChanged SelectionChangedEvent;

	DECLARE_EVENT_OneParam(FIKinemaRigToolSharedData, FGroupSelectionChanged, const TArray<UObject*> &);
	FGroupSelectionChanged GroupSelectionChangedEvent;

	/** Callback for handling changes to the bone/body/constraint hierarchy */
	DECLARE_EVENT(FIKinemaRigToolSharedData, FHierarchyChanged);
	FHierarchyChanged HierarchyChangedEvent;

	/** Callback for handling changes to the current selection in the tree */
	DECLARE_EVENT(FIKinemaRigToolSharedData, FHierarchySelectionChangedEvent);
	FHierarchySelectionChangedEvent HierarchySelectionChangedEvent;


	/** Callback for triggering a refresh of the preview viewport */
	DECLARE_EVENT(FIKinemaRigToolSharedData, FPreviewChanged);
	FPreviewChanged PreviewChangedEvent;

	/** The PhysicsAsset asset being inspected */
	UIKinemaRig* IKinemaRig;

	/** IKinemaRigTool specific skeletal mesh component */
	UIKinemaRigToolEdSkeletalMeshComponent* EditorSkelComp;

	/** The skeletal mesh being used to preview the physics asset */
	USkeletalMesh* EditorSkelMesh;

	/** Floor static mesh component */
	UStaticMeshComponent* EditorFloorComp;

	/** Preview scene */
	FPreviewScene PreviewScene;

	/** Results from the new body dialog */
	EAppReturnType::Type NewBodyResponse;

	/** Helps define how the asset behaves given user interaction in simulation mode*/
	UPhysicsHandleComponent* MouseHandle;

	TArray<FSelection> SelectedBodies;
	FSelection * GetSelectedBody()
	{
		int32 Count = SelectedBodies.Num();
		return Count ? &SelectedBodies[Count - 1] : NULL;
	}

	bool IsBodySelected(int32 index)
	{
		auto IsSelected = SelectedBodies.ContainsByPredicate([&index](const FSelection& bone) {
			return bone.Index == index; });
		return IsSelected;
	}

	FIKinemaBoneDef const * CopiedBodySetup;
	FIKinemaTaskDef const * CopiedConstraintTemplate;

	//struct FAnimationRecorder Recorder;

	/** Constraint editing */
	TArray<FSelection> SelectedConstraints;
	FSelection * GetSelectedConstraint()
	{
		int32 Count = SelectedConstraints.Num();
		return Count ? &SelectedConstraints[Count - 1] : NULL;
	}

	bool IsConstraintSelected(int32 index)
	{
		auto IsSelected = SelectedConstraints.ContainsByPredicate([&index](const FSelection& bone) {
			return bone.Index == index; });
		return IsSelected;
	}

	/** Source Bone Editing */
	TArray<FSelection> SelectedSourceBodies;
	FSelection * GetSelectedSourceBody()
	{
		int32 Count = SelectedSourceBodies.Num();
		return Count ? &SelectedSourceBodies[Count - 1] : NULL;
	}

	bool IsSourceBodySelected(int32 index)
	{
		auto IsSelected = SelectedSourceBodies.ContainsByPredicate([&index](const FSelection& bone) {
			return bone.Index == index; });
		return IsSelected;
	}


	bool bIsCoMSelected;

	/** Show flags */
	bool bShowHierarchy;
	bool bShowNonActive;
	bool bShowConstraints;
	bool bDrawGround;
	bool bShowAnimSkel;
	bool bShowCollisionShapes;

	/** Misc toggles */
	bool bSelectionLock;
	bool bRunningSimulation;
	bool bIsPlaying;
	bool bManipulating;
	/** Various mode flags */
	EIKinemaRigToolEditingMode EditingMode;

	/** We have a different set of view setting per editing mode */
	EIKinemaRigToolRenderMode BodyEdit_MeshViewMode;
	EIKinemaRigToolRenderMode ConstraintEdit_MeshViewMode;
	EIKinemaRigToolRenderMode Sim_MeshViewMode;

	/** Used to prevent recursion with tree hierarchy ... needs to be rewritten! */
	bool bInsideSelChange;

	FMocapBone selected;

	float AverageBoneLength;
#if DEBUG_CLICK_VIEWPORT
	FVector LastClickOrigin;
	FVector LastClickDirection;
#endif
	FIntPoint LastClickPos;

public:
	static TMap<FName, FTemplate> Templates;
private:
	TMap<FName, FTemplate> DefaultTemplates;
};
