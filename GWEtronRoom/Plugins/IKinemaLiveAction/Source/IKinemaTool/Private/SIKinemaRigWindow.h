/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once


#include "IKinemaRigTool.h"
//////////////////////////////////////////////////////////////////////////
// FDisplayedBoneMappingInfo

class FDisplayedBoneMappingInfo
{
public:
	FName Name;
	FString DisplayName;
	UActorSkeleton* Skeleton;

	/** Static function for creating a new item, but ensures that you can only have a TSharedRef to one */
	static TSharedRef<FDisplayedBoneMappingInfo> Make(const FName NodeName,  UActorSkeleton* InSkeleton)
	{
		return MakeShareable(new FDisplayedBoneMappingInfo(NodeName,  InSkeleton));
	}

	FName GetNodeName() const
	{
		return Name;
	}

	FString GetDisplayName() const
	{
		return DisplayName;
	}

protected:
	/** Hidden constructor, always use Make above */
	FDisplayedBoneMappingInfo(const FName InNodeName, UActorSkeleton* InSkeleton)
		: Name( InNodeName )
		, Skeleton( InSkeleton )
	{
		DisplayName = InNodeName.ToString();
	}

	/** Hidden constructor, always use Make above */
	FDisplayedBoneMappingInfo() {}
};

typedef SListView< TSharedPtr<FDisplayedBoneMappingInfo> > SBoneMappingListType;

//////////////////////////////////////////////////////////////////////////
// SRigWindow

class SIKinemaRigWindow : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS( SIKinemaRigWindow )
		: _RigTool()
	{}
		
		/* The Persona that owns this table */
		SLATE_ARGUMENT( TWeakPtr< FIKinemaRigTool >, RigTool )

	SLATE_END_ARGS()

	/**
	* Slate construction function
	*
	* @param InArgs - Arguments passed from Slate
	*
	*/
	void Construct( const FArguments& InArgs );

	/**
	* Destructor - resets the morph targets
	*
	*/
	virtual ~SIKinemaRigWindow();

	/**
	* Filters the SListView when the user changes the search text box (NameFilterBox)
	*
	* @param SearchText - The text the user has typed
	*
	*/
	void OnFilterTextChanged( const FText& SearchText );

	/**
	* Filters the SListView when the user hits enter or clears the search box
	* Simply calls OnFilterTextChanged
	*
	* @param SearchText - The text the user has typed
	* @param CommitInfo - Not used
	*
	*/
	void OnFilterTextCommitted( const FText& SearchText, ETextCommit::Type CommitInfo );

	/**
	* Create a widget for an entry in the tree from an info
	*
	* @param InInfo - Shared pointer to the morph target we're generating a row for
	* @param OwnerTable - The table that owns this row
	*
	* @return A new Slate widget, containing the UI for this row
	*/
	TSharedRef<ITableRow> GenerateBoneMappingRow(TSharedPtr<FDisplayedBoneMappingInfo> InInfo, const TSharedRef<STableViewBase>& OwnerTable);

	/**
	* Return true if it can delete
	*
	*/
	bool CanPerformRefresh() const;

	/**
	* Handler for the delete of retarget source
	*/
	void OnRefreshBoneMappingList();

	/**
	* Accessor so our rows can grab the filtertext for highlighting
	*
	*/
	FText& GetFilterText() { return FilterText; }
private:

	/**
	* Clears and rebuilds the table, according to an optional search string
	*
	* @param SearchText - Optional search string
	*
	*/
	void CreateBoneMappingList( const FString& SearchText = FString("") );

	/** Pointer back to the Persona that owns us */
	TWeakPtr<FIKinemaRigTool> RigToolPtr;

	/** Box to filter to a specific morph target name */
	TSharedPtr<SSearchBox>	NameFilterBox;

	/** Widget used to display the list of retarget sources */
	TSharedPtr<SBoneMappingListType> BoneMappingListView;

	/** A list of retarget sources. Used by the BoneMappingListView. */
	TArray< TSharedPtr<FDisplayedBoneMappingInfo> > BoneMappingList;

	/** The skeletal mesh that we grab the morph targets from */
	TWeakPtr<FIKinemaRigToolSharedData> SharedData;

	/** Current text typed into NameFilterBox */
	FText FilterText;

	/** show advanced? */
	bool bDisplayAdvanced;

	/** rig combo button */
	TSharedPtr< class SComboButton > AssetComboButton;

	/** Delegate for undo/redo transaction **/
	void PostUndo();

	/**
	 * Callback for asset picker
	 */
	/* Set rig set combo box*/
	void OnAssetSelected(UObject* Object);
	FText GetAssetName() const;
	void CloseComboButton();
	//TSharedRef<SWidget> MakeRigPickerWithMenu();

	/** Returns true if the asset shouldn't show  */
	bool ShouldFilterAsset(const struct FAssetData& AssetData);

	UObject* GetRigObject() const;
	
	void OnBoneMappingChanged( FName NodeName, FName BoneName );
	FName GetBoneMapping( FName NodeName );

	FReply OnToggleAdvanced();
	FText GetAdvancedButtonText() const;
};