/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "SIKinemaBoneMapper.h"
#include "IKinemaRigToolModule.h"
#include "SIKinemaRigWindow.h"
#include "SIKinemaCharacterisation.h"
#include "ScopedTransaction.h"
#define LOCTEXT_NAMESPACE "SIKinemaRetargetEditor"

void SIKinemaRetargetEditor::Construct(const FArguments& InArgs)
{
	RigToolPtr = InArgs._RigTool;
	SharedData = NULL;

	if ( RigToolPtr.IsValid() )
	{
		SharedData = RigToolPtr.Pin()->GetSharedData();
		if (!SharedData.Pin()->IKinemaRig->Actor & !InArgs._bCharacterisation)
		{
			return;
		}
		//RigToolPtr.Pin()->RegisterOnPostUndo(FIKinemaRigTool::FOnPostUndo::CreateSP( this, &SIKinemaRetargetEditor::PostUndo ) );
	}
	if(InArgs._bCharacterisation)
	{
		ChildSlot
			[
		SNew(SVerticalBox)
			+ SVerticalBox::Slot()
			.Padding(5, 5)
			.AutoHeight()
			[
				// explainint this is retarget source window
				// and what it is
				SNew(STextBlock)
				.TextStyle(FEditorStyle::Get(), "Persona.RetargetManager.ImportantText")
			.Text(LOCTEXT("IKinemaBoneMapping_Title", "Set up Rig"))
			]
		+ SVerticalBox::Slot()
			.FillHeight(1)
			.Padding(2, 5)
			[
				// construct rig manager window
				SNew(SIKinemaCharacterisationWindow)
				.RigTool(RigToolPtr)
			]
			];
	
	}
	else
	{
		ChildSlot
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
			.Padding(5, 5)
			.AutoHeight()
			[
				// explainint this is retarget source window
				// and what it is
				SNew(STextBlock)
				.AutoWrapText(true)
			.ToolTipText(LOCTEXT("RigSetup_Tooltip", "Set up Rig for retargeting between skeletons."))
			.Font(FEditorStyle::GetFontStyle(TEXT("Persona.RetargetManager.FilterFont")))
			.Text(LOCTEXT("RigTemplate_Description", "You can set up the bone mapping for this IKinemaRig"))
			]
			
				+ SVerticalBox::Slot()
			.FillHeight(1)
			.Padding(2, 5)
			[
				// construct rig manager window
				SNew(SIKinemaRigWindow)
				.RigTool(RigToolPtr)
			]

		+ SVerticalBox::Slot()
			.Padding(2, 5)
			.AutoHeight()
			[
				SNew(SSeparator)
				.Orientation(Orient_Horizontal)
			]

		+ SVerticalBox::Slot()
			.Padding(5, 5)
			.AutoHeight()
			[
				// explainint this is retarget source window
				// and what it is
				SNew(STextBlock)
				.TextStyle(FEditorStyle::Get(), "Persona.RetargetManager.ImportantText")
			.Text(LOCTEXT("BasePose_Title", "Manage Mapping"))
			]
		+ SVerticalBox::Slot()
			.AutoHeight()		// This is required to make the scrollbar work, as content overflows Slate containers by default
			.VAlign(VAlign_Center)
			//.HAlign(HAlign_Right)
			.Padding(2, 5)
			[
				// two button 1. view 2. save to base pose
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
			.AutoWidth()
			.HAlign(HAlign_Left)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
			.AutoHeight()		// This is required to make the scrollbar work, as content overflows Slate containers by default
			.VAlign(VAlign_Center)
			//.HAlign(HAlign_Right)
			.Padding(2.0f)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
			.AutoWidth()
			.Padding(2.0f)
			.HAlign(HAlign_Center)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
			.FillHeight(1.0f)
			.Padding(2.0f)
			.VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
			.Text(LOCTEXT("TemplateName_Label", "Template Name"))
			]
			]
		+ SHorizontalBox::Slot()
			.FillWidth(5.0f)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
			.FillHeight(1.0f)
			.Padding(2.0f)
			.VAlign(VAlign_Center)
			[
				SAssignNew(TemplateNameBox, SEditableTextBox)
				.ToolTipText(LOCTEXT("TemplateNameTextToolTip", "The template name to be"))
			.OnTextCommitted(this, &SIKinemaRetargetEditor::OnTemplateNameCommitted)
			.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
			.HintText(LOCTEXT("TemplateNameTextLabel", "Template Name"))
			]
			]
			]
		+ SVerticalBox::Slot()
			.AutoHeight()		// This is required to make the scrollbar work, as content overflows Slate containers by default
			.VAlign(VAlign_Center)
			.HAlign(HAlign_Left)
			.Padding(2, 5)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
			.AutoWidth()		// This is required to make the scrollbar work, as content overflows Slate containers by default
			.VAlign(VAlign_Center)
			.HAlign(HAlign_Left)
			.Padding(2, 5)
			[
				SNew(SButton)
				.OnClicked(FOnClicked::CreateSP(this, &SIKinemaRetargetEditor::OnSaveTemplate))
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			.Text(LOCTEXT("SaveTemplate_Label", "Save Template"))
			.ToolTipText(LOCTEXT("SaveTemplate_Tooltip", "Save the current mapping as template"))
			]
		+ SHorizontalBox::Slot()
			.AutoWidth()		// This is required to make the scrollbar work, as content overflows Slate containers by default
			.VAlign(VAlign_Center)
			.HAlign(HAlign_Left)
			.Padding(2, 5)
			[
				/*
				SNew(SButton)
				.OnClicked(FOnClicked::CreateSP(this, &SIKinemaRetargetEditor::OnSaveTemplate))
				.HAlign(HAlign_Center)
				.VAlign(VAlign_Center)
				.Text(LOCTEXT("DeleteTemplate_Label", "Delete Template"))
				.ToolTipText(LOCTEXT("DeleteTemplate_Tooltip", "Delete the current mapping as template"))
				*/
				SNew(SComboButton)
				.OnGetMenuContent(this, &SIKinemaRetargetEditor::OnGetTemplateMenuContent)
			.ContentPadding(2.2)
			.ButtonContent()
			[
				SNew(STextBlock)
				.Text(LOCTEXT("DeleteTemplate_Label", "Delete Template"))
			.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
			]
		.ToolTipText(LOCTEXT("DeleteTemplate_Tooltip", "Delete the current mapping as template"))
			]
			]
			]

		+ SHorizontalBox::Slot()
			.FillWidth(2.f)
			.Padding(2, 2)
			.HAlign(HAlign_Right)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot()
			.AutoWidth()
			.Padding(2, 15)
			.HAlign(HAlign_Right)
			.VAlign(VAlign_Bottom)
			[
				SNew(SButton)
				.OnClicked(FOnClicked::CreateSP(this, &SIKinemaRetargetEditor::OnResetMapping))
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			.Text(LOCTEXT("ResetRetargetBasePose_Label", "Reset All Mappings"))
			.ToolTipText(LOCTEXT("ResetRetargetBasePose_Tooltip", "Rest All Bone Mappings"))
			]
		+ SHorizontalBox::Slot()
			.Padding(2, 15)
			.HAlign(HAlign_Right)
			.VAlign(VAlign_Bottom)
			[
				SNew(SButton)
				.OnClicked(FOnClicked::CreateSP(this, &SIKinemaRetargetEditor::OnSaveMapping))
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			.Text(LOCTEXT("SaveRetargetBasePose_Label", "Save Mapping"))
			.ToolTipText(LOCTEXT("SaveRetargetBasePose_Tooltip", "Save Current Bone Mappings"))
			]
			]
			]
			];
	}
}

SIKinemaRetargetEditor::~SIKinemaRetargetEditor()
{
	/*if (RigToolPtr.IsValid())
	{
		RigToolPtr.Pin()->UnregisterOnPostUndo(this);
	}*/
}

void SIKinemaRetargetEditor::OnTemplateNameCommitted(const FText& InputText, ETextCommit::Type CommitType)
{
	TemplateName = InputText.ToString();
}


FReply SIKinemaRetargetEditor::OnResetMapping()
{
	auto Rig = SharedData.Pin()->IKinemaRig;
	auto sharedData = SharedData.Pin();
	for (auto& bone : Rig->SolverDef.Bones)
	{
		if (bone.SourceName != NAME_None && Rig->Actor->NumOfBones())
		{
			auto& source = Rig->Actor->GetBone(bone.SourceName);
			source.TargetBone = NAME_None;
		}
		bone.SourceName = NAME_None;
		bone.SourceIndex = INDEX_NONE;
		bone.MatchPose.SetIdentity();

		int32 BoneIndex = sharedData->EditorSkelComp->GetBoneIndex(bone.Name);
		FAnimNode_ModifyBone* SkelControl = NULL;
		//Get the skeleton control manipulating this bone
		const FName BoneName = sharedData->EditorSkelComp->SkeletalMesh->RefSkeleton.GetBoneName(BoneIndex);
		SkelControl = &(sharedData->EditorSkelComp->PreviewInstance->ModifyBone(BoneName));

		SkelControl->Rotation = FRotator(0,0,0);
	}
	const FScopedTransaction Transaction(NSLOCTEXT("IKinemaRigTool", "Reset Bones Maps", "Clear source bone to target link"));
	Rig->Modify();
	Rig->PostLoadOrImport();
	Rig->UpdateVersion++;
	Rig->Actor->Modify();
	return FReply::Handled();
}
FReply SIKinemaRetargetEditor::OnSaveMapping()
{
	auto Rig = SharedData.Pin()->IKinemaRig;
	for (auto& bone : Rig->SolverDef.Bones)
	{
		if (bone.SourceName != NAME_None)
		{
			auto& source = Rig->Actor->GetBone(bone.SourceName);
			SharedData.Pin()->LinkBones(bone, source);
		}
	}
	const FScopedTransaction Transaction(NSLOCTEXT("IKinemaRigTool", "Save Bones Maps", "Link source bone to target"));
	Rig->Modify();
	Rig->PostLoadOrImport();
	Rig->UpdateVersion++;
	Rig->Actor->Modify();

	return FReply::Handled();
}

FReply SIKinemaRetargetEditor::OnSaveTemplate()
{
	auto Rig = SharedData.Pin()->IKinemaRig;
	bool OverWrite = true;
	if (SharedData.Pin()->Templates.Contains(FName(*TemplateName)))
	{
		auto DialogReturn = FMessageDialog::Open(EAppMsgType::YesNo,
			NSLOCTEXT("UnrealEd", "Error_LiveActionTemplate",
				"There already exists a template with this name. Overwrite it?"));
		if (DialogReturn == EAppReturnType::No)
		{
			OverWrite = false;
		}
	}
	//Create and save template
	if (OverWrite)
	{
		FTemplate temp(Rig, FName(*TemplateName));
		SharedData.Pin()->Templates.Add(FName(*TemplateName), temp);
	}

	return FReply::Handled();
}

void SIKinemaRetargetEditor::DeleteTemplate(int32 TemplateIndex)
{

	if (TemplateIndex == -1)
	{
		return;
	}

	auto Templates = FTemplate::LoadTemplates();
	TArray<FName> temp;
	Templates.GenerateKeyArray(temp);

	auto Template = Templates.Find(temp[TemplateIndex]);
	if (Template)
	{
		Template->DeleteTemplate();
	}
	SharedData.Pin()->Templates.FindAndRemoveChecked(temp[TemplateIndex]);

}

TSharedRef<SWidget> SIKinemaRetargetEditor::OnGetTemplateMenuContent() const
{
	FMenuBuilder MenuBuilder(true, NULL);

	// Get a list of all templates
	auto Templates = FTemplate::LoadTemplates();
	int32 index = -1;

	FFormatNamedArguments Arguments;
	Arguments.Add(TEXT("TemplateName"), FText::FromString("None"));
	MenuBuilder.AddMenuEntry(
		FText::FromString("None"),
		FText::Format(LOCTEXT("Template_Tooltip", "Use {TemplateName} as a match template"), Arguments),
		FSlateIcon(),
		FUIAction(
			FExecuteAction::CreateSP(this, &SIKinemaRetargetEditor::DeleteTemplate, index),
			FCanExecuteAction()
			)
		);

	for (auto temp : Templates)
	{
		index++;
		const FText TemplateNameText = FText::FromString(temp.Key.ToString());

		FFormatNamedArguments arguments;
		arguments.Add(TEXT("TemplateName"), TemplateNameText);
		MenuBuilder.AddMenuEntry(
			TemplateNameText,
			FText::Format(LOCTEXT("Template_Tooltip", "Use {TemplateName} as a match template"), arguments),
			FSlateIcon(),
			FUIAction(
				FExecuteAction::CreateSP(this, &SIKinemaRetargetEditor::DeleteTemplate, index),
				FCanExecuteAction()
				)
			);
	}

	return MenuBuilder.MakeWidget();
}

#undef LOCTEXT_NAMESPACE