/* Copyright (C) 2006-2017, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "Templates.h"
#include "IKinemaRigToolModule.h"
#include "Interfaces/IPluginManager.h"
//#include "JsonUtils.h"

static TMap<FName, FTemplate> Templates;
static TMap<FName, FTemplate> DefaultTemplates;

FTemplate::FTemplate(const FName& InTemplateName, const FString& InJSON)
	:TemplateName(InTemplateName)
{

	//Read JSON and populate map
	TSharedPtr<FJsonObject> JsonParsed = MakeShareable(new FJsonObject());
	TSharedRef<TJsonReader<TCHAR>> JsonReader = TJsonReaderFactory<TCHAR>::Create(InJSON);

	bool readSuccess = FJsonSerializer::Deserialize(JsonReader, JsonParsed);

	if (readSuccess)
	{
		auto  NameMap = JsonParsed->GetArrayField("NameMap");
		for (auto elem : NameMap)
		{
			auto obj = elem->AsObject();
			FString source = obj->GetStringField("Source");
			FString target = obj->GetStringField("Target");
			Map.Add(source, target);
		}

	}
}

//Create template from matching
FTemplate::FTemplate(UIKinemaRig* IKinemaRig, const FName& InTemplateName)
	:TemplateName(InTemplateName)
{
	for (const auto& bone : IKinemaRig->SolverDef.Bones)
	{
		if (bone.SourceName == NAME_None)
		{
			continue;
		}
		FString source = bone.SourceName.ToString();
		source.RemoveFromStart(IKinemaRig->Actor->SubjectName + "_");
		FString target = bone.Name != NAME_None? bone.Name.ToString() : "";
		Map.Add(source, target);
	}
	WriteTemplate();
}

//Return the name of the template
const FName& FTemplate::GetName()
{
	return TemplateName;
}

bool FTemplate::SourceExists(const FString& Source) const
{
	return Map.Contains(Source);
}

//Get the target bone name
const TArray<FString> FTemplate::GetTargets(const FString& Source) const
{
	TArray<FString> Targets;
	Map.MultiFind(Source, Targets);

	return Targets;
}

void FTemplate::WriteTemplate()
{
	//Create JSON
	TArray<TSharedPtr<FJsonValue>> NameMap;
	for (auto elem : Map)
	{
		TSharedPtr<FJsonObject> map = MakeShareable(new FJsonObject());


		map->SetStringField("Source", elem.Key);
		map->SetStringField("Target", elem.Value);
		TSharedRef< FJsonValueObject > JsonValue = MakeShareable(new FJsonValueObject(map));
		NameMap.Push(JsonValue);
	}


	TSharedPtr<FJsonObject> templ = MakeShareable(new FJsonObject());
	templ->SetArrayField("NameMap", NameMap);

	FString OutputString;
	TSharedRef< TJsonWriter<> > Writer = TJsonWriterFactory<>::Create(&OutputString);
	FJsonSerializer::Serialize(templ.ToSharedRef(), Writer);


	//Write to file
	auto path = IPluginManager::Get().FindPlugin("IKinemaLiveAction")->GetBaseDir() / TEXT("Resources/Templates/") + TemplateName.ToString() + ".json";
	FFileHelper::SaveStringToFile(OutputString, *path);
}

//Visitor class to be called for every template file under Resources/Templates
class FVisitor : public IPlatformFile::FDirectoryVisitor
{
	friend FTemplate;
	//Extract only the file name from the entire path
	FString GetFileName(FString fileName)
	{
		FString Filename;
		int32 Pos = fileName.Find(TEXT("/"), ESearchCase::IgnoreCase, ESearchDir::FromEnd);
		// in case we are using backslashes on a platform that doesn't use backslashes
		Pos = FMath::Max(Pos, fileName.Find(TEXT("\\"), ESearchCase::IgnoreCase, ESearchDir::FromEnd));

		if (Pos != INDEX_NONE)
		{
			Filename = fileName.Mid(Pos + 1);
		}


		int32 DotPos = Filename.Find(TEXT("."), ESearchCase::IgnoreCase, ESearchDir::FromEnd);
		if (DotPos != INDEX_NONE)
		{
			return Filename.Left(DotPos);
		}

		return Filename;
	}

	//Read every template file and add to the static map
	bool Visit(const TCHAR* FilenameOrDirectory, bool bIsDirectory) override
	{
		bool ret = true;
		//Make sure we are a file
		if (!bIsDirectory)
		{
			FString InputString;
			ret = FFileHelper::LoadFileToString(InputString, FilenameOrDirectory);
			FString FullFileName(FilenameOrDirectory);
			auto FileName = GetFileName(FullFileName);
			FTemplate temp(FName(*FileName), InputString);
			temp.SetFilePath (FullFileName);
			Templates.Add(FName(*FileName), temp);
			
			return ret;
		}
		return ret;
	}
};

TMap<FName, FTemplate> FTemplate::LoadTemplates()
{
	FVisitor visitor;
	Templates.Empty();
	auto FilePath = IPluginManager::Get().FindPlugin("IKinemaLiveAction")->GetBaseDir() / TEXT("Resources/Templates/");
	IFileManager::Get().IterateDirectory(*FilePath, visitor);

	return Templates;
}

bool FTemplate::DeleteTemplate()
{
	return IFileManager::Get().Delete(*FilePath);
}

void FTemplate::SetFilePath(const FString& path)
{
	FilePath = path;
}
TMap<FName, FTemplate> FTemplate::LoadDefaults()
{
	FVisitor visitor;
	Templates.Empty();
	auto FilePath = IPluginManager::Get().FindPlugin("IKinemaLiveAction")->GetBaseDir() / TEXT("Resources/Templates/Defaults/");
	IFileManager::Get().IterateDirectory(*FilePath, visitor);

	return Templates;
}