/* Copyright (C) 2006-2017, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "SIKinemaRigToolPreviewViewport.h"
#include "IKinemaRigToolModule.h"
#include "IKinemaRigTool.h"
#include "Runtime/Engine/Public/Slate/SceneViewport.h"
#include "IKinemaRigToolPreviewViewportClient.h"
#include "SIKinemaRigToolPreviewToolbar.h"
#include "Widgets/Docking/SDockTab.h"



SIKinemaRigToolPreviewViewport::~SIKinemaRigToolPreviewViewport()
{
	if (EditorViewportClient.IsValid())
	{
		EditorViewportClient->Viewport->Destroy();
		EditorViewportClient->Viewport = NULL;
	}
}

void SIKinemaRigToolPreviewViewport::SetViewportType(ELevelViewportType ViewType)
{
	EditorViewportClient->SetViewportType(ViewType);
}

void SIKinemaRigToolPreviewViewport::Construct(const FArguments& InArgs)
{
	IKinemaRigToolPtr = InArgs._IKinemaRigTool;

	SEditorViewport::Construct(SEditorViewport::FArguments());
	ViewportWidget->SetEnabled(FSlateApplication::Get().GetNormalExecutionAttribute());
}

void SIKinemaRigToolPreviewViewport::RefreshViewport()
{
	SceneViewport->Invalidate();
	SceneViewport->InvalidateDisplay();
}

bool SIKinemaRigToolPreviewViewport::IsVisible() const
{
	return ViewportWidget.IsValid() && (!ParentTab.IsValid() || ParentTab.Pin()->IsForeground());
}

TSharedPtr<FSceneViewport> SIKinemaRigToolPreviewViewport::GetViewport() const
{
	return SceneViewport;
}

TSharedPtr<FIKinemaRigToolEdPreviewViewportClient> SIKinemaRigToolPreviewViewport::GetViewportClient() const
{
	return EditorViewportClient;
}

TSharedPtr<SViewport> SIKinemaRigToolPreviewViewport::GetViewportWidget() const
{
	return ViewportWidget;
}

TSharedRef<FEditorViewportClient> SIKinemaRigToolPreviewViewport::MakeEditorViewportClient()
{
	EditorViewportClient = MakeShareable(new FIKinemaRigToolEdPreviewViewportClient(IKinemaRigToolPtr, IKinemaRigToolPtr.Pin()->GetSharedData(), SharedThis(this)));

	EditorViewportClient->bSetListenerPosition = false;

	EditorViewportClient->SetRealtime(!FIKinemaRigTool::IsPIERunning());
	EditorViewportClient->VisibilityDelegate.BindSP(this, &SIKinemaRigToolPreviewViewport::IsVisible);

	return EditorViewportClient.ToSharedRef();
}

TSharedPtr<SWidget> SIKinemaRigToolPreviewViewport::MakeViewportToolbar()
{
	return SNew(SIKinemaRigToolPreviewViewportToolBar, SharedThis(this))
			.IKinemaRigToolPtr(IKinemaRigToolPtr)
			.IsEnabled(FSlateApplication::Get().GetNormalExecutionAttribute());
}
