/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Toolkits/AssetEditorToolkit.h"
#include "IKinemaRigToolSharedData.h"
#include "SMocapSourcePicker.h"
#include "EditorUndoClient.h"

class SIKinemaRigToolPreviewViewport;
class FIKinemaRigToolTreeInfo;

typedef TSharedPtr<FIKinemaRigToolTreeInfo> FTreeElemPtr;



/*-----------------------------------------------------------------------------
FIKinemaRigTool
-----------------------------------------------------------------------------*/

class FIKinemaRigTool : public IIKinemaRigTool, public FGCObject, public FEditorUndoClient, public FTickableEditorObject
{
public:
	virtual void RegisterTabSpawners(const TSharedRef<class FTabManager>& TabManager) override;
	virtual void UnregisterTabSpawners(const TSharedRef<class FTabManager>& TabManager) override;
	TSharedRef<SDockTab> SpawnTab(const FSpawnTabArgs& TabSpawnArgs, FName TabIdentifier);
	TSharedRef<SDockTab> SpawnMapper(const FSpawnTabArgs& TabSpawnArgs, FName TabIdentifier);
	TSharedRef<SDockTab> SpawnCharacterisation(const FSpawnTabArgs& TabSpawnArgs, FName TabIdentifier);
	/** Destructor */
	virtual ~FIKinemaRigTool();

	/** Edits the specified IKinemaRig object */
	void InitIKinemaRigTool(const EToolkitMode::Type Mode, const TSharedPtr< class IToolkitHost >& InitToolkitHost, UIKinemaRig* ObjectToEdit);

	/** Shared data accessor */
	TSharedPtr<FIKinemaRigToolSharedData> GetSharedData() const;

	/** Handles a selection change... assigns the proper object to the properties widget and the hierarchy tree view */
	void SetPropertiesSelection(UObject* Obj, FIKinemaRigToolSharedData::FSelection * Selection = 0);

	/** Handles a group selection change... assigns the proper object to the properties widget and the hierarchy tree view */
	void SetPropertiesGroupSelection(const TArray<UObject*> & Objs);

	/** Repopulates the hierarchy tree view */
	void RefreshHierachyTree();

	/** Updates the selections in the hierarchy tree */
	void RefreshHierachyTreeSelection();

	/** Refreshes the preview viewport */
	void RefreshPreviewViewport();
	TSharedPtr< SIKinemaRigToolPreviewViewport > GetPreviewViewportWidget() const { return PreviewViewport; }

	/** Methods for building the various context menus */
	TSharedPtr<SWidget> BuildMenuWidgetBody(bool bHierarchy = false);
	TSharedPtr<SWidget> BuildMenuWidgetConstraint(bool bHierarchy = false);
	TSharedPtr<SWidget> BuildMenuWidgetCoM();
	//TSharedPtr<SWidget> BuildMenuWidgetBone();
	TSharedRef<SWidget> BuildStaticMeshAssetPicker();
	TSharedRef<SWidget> BuildHierarchyFilterMenu();
	TSharedRef<SWidget> BuildMocapMenu();
	FText GetHierarchyFilter() const;

	// FAssetEditorToolkit interface
	virtual void SaveAsset_Execute() override;
	// End of FAssetEditorToolkit 

	/** IToolkit interface */
	virtual FName GetToolkitFName() const override;
	virtual FText GetBaseToolkitName() const override;
	virtual FString GetWorldCentricTabPrefix() const override;
	virtual FLinearColor GetWorldCentricTabColorScale() const override;

	/** @return the documentation location for this editor */
	virtual FString GetDocumentationLink() const override
	{
		return FString("");
	}

	/** FGCObject interface */
	virtual void AddReferencedObjects(FReferenceCollector& Collector) override;

	/** Returns whether a PIE session is running. */
	static bool IsPIERunning();

	// FTickableEditorObject interface
	virtual void Tick(float DeltaTime) override;
	virtual bool IsTickable() const override { return true; }
	virtual TStatId GetStatId() const override;
	// End of FTickableEditorObject interface

	void OnSourceScaleChanged(float Scale);

private:

	enum EIKinemaRigToolHierarchyFilterMode
	{
		PHFM_All,
		PHFM_Bodies
	};

	// Begin FEditorUndoClient Interface
	virtual void PostUndo(bool bSuccess) override;
	virtual void PostRedo(bool bSuccess) override;
	// End of FEditorUndoClient

	/** Creates all internal widgets for the tabs to point at */
	void CreateInternalWidgets();

	/** Builds the menu for the PhysicsAsset editor */
	void ExtendMenu();

	/** Builds the toolbar widget for the PhysicsAsset editor */
	void ExtendToolbar();

	/**	Binds our UI commands to delegates */
	void BindCommands();

	/** Methods related to the bone hiearchy tree view */
	TSharedRef<ITableRow> OnGenerateRowForTree(FTreeElemPtr Item, const TSharedRef<STableViewBase>& OwnerTable);
	void OnGetChildrenForTree(FTreeElemPtr Parent, TArray<FTreeElemPtr>& OutChildren);
	void OnTreeSelectionChanged(FTreeElemPtr TreeElem, ESelectInfo::Type SelectInfo);
	void OnTreeDoubleClick(FTreeElemPtr TreeElem);
	void OnTreeHighlightChanged();
	TSharedPtr<SWidget> OnTreeRightClick();

	/** SContentReference uses this to set the current animation */
	void AnimationSelectionChanged(UObject* Object);

	/** Returns the currently selected animation (if any) */
	UObject* GetSelectedAnimation() const;

	/** Generates menu content for the class picker when it is clicked */
	TSharedRef<SWidget> GetClassPickerMenuContent();

	/** Gets the currently selected blueprint name to display on the class picker combo button */
	FText GetSelectedAnimBlueprintName() const;

	/** Callback from the class picker when the user selects a class */
	void OnClassPicked(UClass* PickedClass);

	/** Callback from the detail panel to browse to the selected anim asset */
	void OnBrowseToAnimBlueprint();

	/** Callback from the details panel to use the currently selected asset in the content browser */
	void UseSelectedAnimBlueprint();

	FReply OnImportActor();
	bool CanImportActor() const;



	/** Toolbar/menu command methods */
	bool IsNotSimulation() const;
	bool IsEditBodyMode() const;
	bool IsSelectedEditBodyMode() const;
	bool IsEditConstraintMode() const;
	bool IsSelectedEditConstraintMode() const;
	bool CanStartSimulation() const;
	void OnChangeDefaultMesh();

	void OnSourceSelection();

	void OnEditingMode(int32 Mode);
	bool IsEditingMode(int32 Mode) const;
	void OnCopyProperties();
	bool IsCopyProperties() const;
	bool CanCopyProperties() const;
	void OnPasteProperties();
	bool CanPasteProperties() const;
	bool IsSelectedEditMode() const;
	void OnToggleSimulation();
	bool IsToggleSimulation() const;
	void OnMeshRenderingMode(FIKinemaRigToolSharedData::EIKinemaRigToolRenderMode Mode);
	bool IsMeshRenderingMode(FIKinemaRigToolSharedData::EIKinemaRigToolRenderMode Mode) const;
	void OnShowNonActive();
	bool IsShowNonActive() const;
	void OnDrawGroundBox();
	bool IsDrawGroundBox() const;
	void OnToggleGraphicsHierarchy();
	bool IsToggleGraphicsHierarchy() const;
	void OnShowConstraints();
	bool IsShowConstraints() const;

	bool CanAddConstraint() const;
	void OnResetBone();
	void OnResetConstraint();
	void OnResetCoMConstraint();
	void OnDeleteConstraint();
	void OnPlayAnimation();
	bool IsPlayAnimation() const;
	void OnShowSkeleton();
	void OnViewType(ELevelViewportType ViewType);
	bool IsShowSkeleton() const;
	void OnDeleteBody();
	void OnDeleteAllBodiesBelow();
	void OnLockSelection();
	void OnDeleteSelection();
	void OnFocusSelection();

	void OnShowCollisionShape();
	bool IsShowCollisionShape() const;

	void OnLinkBones();
	bool CanLinkBones() const;

	void OnMapBones();
	bool CanMapBones() const;
	void OnCharacterise();

	void OnLicense();

	//Characterise Bone
	void OnCharacteriseHip();
	void OnCharacteriseChest();
	void OnCharacteriseHead();
	void OnCharacteriseShoulder();
	void OnCharacteriseElbow();
	void OnCharacteriseHand();
	void OnCharacteriseFoot();


	//Create Task from Preset
	void OnAddHipTask();
	void OnAddFootTask();
	void OnAddHandTask();
	void OnAddHeadTask();
	void OnAddChestTask();
	void OnAddElbowTask();
	void OnAddKneeTask();

	//Create Constraints
	void OnAddConstraint();
	void OneAddOrientationalConstraint();
	void OnAddPositionalConstraint();

	void OnSelectAllBelow();
	bool CanSelectAllBelow();
	//menu commands
	void OnSelectAll();
	void SetHierarchyFilter(EIKinemaRigToolHierarchyFilterMode Mode);
	bool FilterTreeElement(FTreeElemPtr TreeElem) const;

	FText GetRepeatLastSimulationToolTip() const;
	FSlateIcon GetRepeatLastSimulationIcon() const;

	FText GetEditModeLabel() const;
	FText GetEditModeToolTip() const;
	FSlateIcon GetEditModeIcon() const;

private:
	/** List of open tool panels; used to ensure only one exists at any one time */
	TMap< FName, TWeakPtr<SDockableTab> > SpawnedToolPanels;

	/** Preview Viewport */
	TSharedPtr<SIKinemaRigToolPreviewViewport> PreviewViewport;

	/** Properties Tab */
	TSharedPtr<class IDetailsView> Properties;

	/** Bone Hierarchy Tree View */
	TSharedPtr< STreeView<FTreeElemPtr> > Hierarchy;
	TSharedPtr<SBorder> HierarchyControl;
	TSharedPtr<SComboButton> HierarchyFilter;
	TSharedPtr<SComboButton> PickerComboButton;

	/** Hierarchy tree items */
	TArray<FTreeElemPtr> TreeElements;
	TArray<FTreeElemPtr> RootBone;


	TSharedPtr<SMocapSourcePicker> MocapSourcePicker;

	/** Data and methods shared across multiple classes */
	TSharedPtr<FIKinemaRigToolSharedData> SharedData;

	/** Toolbar extender - used repeatedly as the body/constraints mode will remove/add this when changed */
	TSharedPtr<FExtender> ToolbarExtender;

	/** Menu extender - used for commands like Select All */
	TSharedPtr<FExtender> MenuExtender;

	/** Currently selected animation */
	UAnimInstance* SelectedAnimation;

	/** True if in OnTreeSelectionChanged()... protects against infinite recursion */
	bool InsideSelChanged;

	/** True if we want to only simulate from selected body/constraint down*/
	bool SelectedSimulation;

	/** Determine which filter mode to use for the hierarchy */
	EIKinemaRigToolHierarchyFilterMode HierarchyFilterMode;

	/** The combo button for the class picker, Cached so we can close it when the user picks something */
	TSharedPtr<SComboButton> ClassPickerComboButton;

	void ImpToggleSimulation();

	/** Records IKinemaRigTool related data - simulating or mode change */
	void OnAddIKinemaRigToolRecord(const FString& Action, bool bRecordSimulate, bool bRecordMode);

private:
	void RecordAnimation();
	bool IsRecordAvailable() const;
	FSlateIcon GetRecordStatusImage() const;
	FText GetRecordStatusTooltip() const;
	FText GetRecordStatusLabel() const;
	FText GetRecordMenuLabel() const;
	FWidget::EWidgetMode BeforeSimulationWidgetMode;
};
