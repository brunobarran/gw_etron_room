/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "IKinemaRigTool.h"
#include "Widgets/Docking/SDockTab.h"


///////////////////////////////////////////
// SIKinemaRetargetEditor

class SIKinemaRetargetEditor : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS( SIKinemaRetargetEditor )
		: _RigTool()
	{}
		
		/* The Rig tool that owns this table */
		SLATE_ARGUMENT( TWeakPtr< FIKinemaRigTool >, RigTool )

		/* Is it a characterisation tab or not*/
		SLATE_ARGUMENT(bool, bCharacterisation)

	SLATE_END_ARGS()

	/**
	* Slate construction function
	*
	* @param InArgs - Arguments passed from Slate
	*
	*/
	void Construct( const FArguments& InArgs );

	/**
	* Destructor
	*
	*/
	virtual ~SIKinemaRetargetEditor();

private:
	FReply OnResetMapping();
	FReply OnSaveMapping();
	FReply OnSaveTemplate();
	/** Pointer back to the RugTool that owns us */
	TWeakPtr<FIKinemaRigTool> RigToolPtr;

	bool bCharacterisationTab = false;
	/** The IKinemaRig shared data */
	TWeakPtr<FIKinemaRigToolSharedData> SharedData;
	TSharedPtr<SEditableTextBox> TemplateNameBox;
	/** Delegate for undo/redo transaction **/
	void PostUndo();
	FText GetToggleRetargetBasePose() const;	
	void OnTemplateNameCommitted(const FText& InputText, ETextCommit::Type CommitType);
	TSharedRef<SWidget> OnGetTemplateMenuContent() const;
	void DeleteTemplate(int32 TemplateIndex);
	FString TemplateName;

};