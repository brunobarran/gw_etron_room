/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "IKinemaRigToolStyle.h"

/*-----------------------------------------------------------------------------
   FIKinemaRigToolCommands
-----------------------------------------------------------------------------*/

class FIKinemaRigToolCommands : public TCommands<FIKinemaRigToolCommands>
{
public:
	/** Constructor */
	FIKinemaRigToolCommands() 
		: TCommands<FIKinemaRigToolCommands>("IKinemaRigTool", NSLOCTEXT("Contexts", "IKinemaRigTool", "IKinemaRigTool"), NAME_None, FIKinemaRigToolStyle::GetStyleSetName())
	{
	}
	
	/** See tooltips in cpp for documentation */
	
	TSharedPtr<FUICommandInfo> ChangeDefaultMesh;
	TSharedPtr<FUICommandInfo> ResetBoneSettings;
	TSharedPtr<FUICommandInfo> EditingMode_Body;
	TSharedPtr<FUICommandInfo> EditingMode_Constraint;
	TSharedPtr<FUICommandInfo> CopyProperties;
	TSharedPtr<FUICommandInfo> CopyBones;
	TSharedPtr<FUICommandInfo> CopyTasks;
	TSharedPtr<FUICommandInfo> PasteProperties;
	TSharedPtr<FUICommandInfo> PasteBones;
	TSharedPtr<FUICommandInfo> PasteTasks;
	TSharedPtr<FUICommandInfo> RepeatLastSimulation;
	TSharedPtr<FUICommandInfo> MeshRenderingMode_Solid;
	TSharedPtr<FUICommandInfo> MeshRenderingMode_Wireframe;
	TSharedPtr<FUICommandInfo> MeshRenderingMode_None;
	TSharedPtr<FUICommandInfo> ShowNonActive;
	TSharedPtr<FUICommandInfo> DrawGroundBox;
	TSharedPtr<FUICommandInfo> ToggleGraphicsHierarchy;
	TSharedPtr<FUICommandInfo> ShowConstraints;
	TSharedPtr<FUICommandInfo> ShowCollisions;
	TSharedPtr<FUICommandInfo> ToggleMassProperties;
	//Create Constraints
	TSharedPtr<FUICommandInfo> AddOrientationalConstraint;
	TSharedPtr<FUICommandInfo> AddPositionalConstraint;
	TSharedPtr<FUICommandInfo> AddDualConstraint;

	//Create Task from presets
	TSharedPtr<FUICommandInfo> AddHipTask;
	TSharedPtr<FUICommandInfo> AddFootTask;
	TSharedPtr<FUICommandInfo> AddHandTask;
	TSharedPtr<FUICommandInfo> AddHeadTask;
	TSharedPtr<FUICommandInfo> AddChestTask;
	TSharedPtr<FUICommandInfo> AddKneeTask;
	TSharedPtr<FUICommandInfo> AddElbowTask;
	
	//Characterisation
	TSharedPtr<FUICommandInfo> SetAsHips;
	TSharedPtr<FUICommandInfo> SetAsChest;
	TSharedPtr<FUICommandInfo> SetAsShoulder;
	TSharedPtr<FUICommandInfo> SetAsElbow;
	TSharedPtr<FUICommandInfo> SetAsHand;
	TSharedPtr<FUICommandInfo> SetAsHead;
	TSharedPtr<FUICommandInfo> SetAsFoot;

	TSharedPtr<FUICommandInfo> ResetConstraint;
	TSharedPtr<FUICommandInfo> ResetCoMConstraint;
	TSharedPtr<FUICommandInfo> DeleteConstraint;
	TSharedPtr<FUICommandInfo> PlayAnimation;
	TSharedPtr<FUICommandInfo> RecordAnimation;
	TSharedPtr<FUICommandInfo> ShowSkeleton;
	TSharedPtr<FUICommandInfo> DeleteBody;
	TSharedPtr<FUICommandInfo> DeleteAllBodiesBelow;
	TSharedPtr<FUICommandInfo> SelectAllObjectsBelow;
	TSharedPtr<FUICommandInfo> SelectAllObjects;
	TSharedPtr<FUICommandInfo> HierarchyFilterAll;
	TSharedPtr<FUICommandInfo> HierarchyFilterBodies;
	TSharedPtr<FUICommandInfo> PerspectiveView;
	TSharedPtr<FUICommandInfo> TopView;
	TSharedPtr<FUICommandInfo> LeftView;
	TSharedPtr<FUICommandInfo> FrontView;
	TSharedPtr<FUICommandInfo> BottomView;
	TSharedPtr<FUICommandInfo> RightView;
	TSharedPtr<FUICommandInfo> BackView;

	/** Hotkey only commands */
	TSharedPtr<FUICommandInfo> SelectionLock;
	TSharedPtr<FUICommandInfo> DeleteSelected;
	TSharedPtr<FUICommandInfo> FocusOnSelection;
	
	TSharedPtr<FUICommandInfo> LinkBones;

	TSharedPtr<FUICommandInfo> MapBones;
	TSharedPtr<FUICommandInfo> Characterise;
	TSharedPtr<FUICommandInfo> SourceSelection;
	TSharedPtr<FUICommandInfo> License;



	/** Initialize commands */
	virtual void RegisterCommands() override;
};
