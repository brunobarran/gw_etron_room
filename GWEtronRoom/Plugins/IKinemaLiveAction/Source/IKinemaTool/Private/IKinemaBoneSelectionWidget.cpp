/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaBoneSelectionWidget.h"
#include "IKinemaRigToolModule.h"
#include "ObjectTools.h"
#include "ScopedTransaction.h"
#include "AssetRegistryModule.h"
#include "Editor/PropertyEditor/Public/PropertyEditorModule.h"
#include "Editor/ContentBrowser/Public/ContentBrowserModule.h"
#include "WorkflowOrientedApp/SContentReference.h"
#include "AssetNotifications.h"
#include "Animation/Rig.h"

#include "Widgets/Input/SSearchBox.h"
#include "Widgets/Text/SInlineEditableTextBlock.h"

#define LOCTEXT_NAMESPACE "SIKinemaBoneSelectionWidget"
#include "BoneSelectionWidget.h"
#include "Widgets/Input/SSearchBox.h"
#include "Editor/PropertyEditor/Public/DetailLayoutBuilder.h"

#define LOCTEXT_NAMESPACE "SIKinemaBoneSelectionWidget"

/////////////////////////////////////////////////////

void SIKinemaBoneSelectionWidget::Construct(const FArguments& InArgs)
{
	if(InArgs._Skeleton.IsValid())
	{
		TargetSkeleton = InArgs._Skeleton.Get();
	}

	//check(TargetSkeleton);

	OnBoneSelectionChanged = InArgs._OnBoneSelectionChanged;
	OnGetSelectedBone = InArgs._OnGetSelectedBone;

	FText FinalTooltip = FText::Format(LOCTEXT("BoneClickToolTip", "{0}\nClick to choose a different bone"), InArgs._Tooltip);

	SAssignNew(TreeView, STreeView<TSharedPtr<FBoneNameInfo>>)
		.TreeItemsSource(&SkeletonTreeInfo)
		.OnGenerateRow(this, &SIKinemaBoneSelectionWidget::MakeTreeRowWidget)
		.OnGetChildren(this, &SIKinemaBoneSelectionWidget::GetChildrenForInfo)
		.OnSelectionChanged(this, &SIKinemaBoneSelectionWidget::OnSelectionChanged)
		.SelectionMode(ESelectionMode::Single);

	this->ChildSlot
	[
		SAssignNew(BonePickerButton, SComboButton)
		.OnGetMenuContent(FOnGetContent::CreateSP(this, &SIKinemaBoneSelectionWidget::CreateSkeletonWidgetMenu))
		.ContentPadding(FMargin(4.0f, 2.0f, 4.0f, 2.0f))
		.ButtonContent()
		[
			SNew(STextBlock)
			.Text(this, &SIKinemaBoneSelectionWidget::GetCurrentBoneName)
			.Font(IDetailLayoutBuilder::GetDetailFont())
			.ToolTipText(FinalTooltip)
		]
	];
}

TSharedRef<SWidget> SIKinemaBoneSelectionWidget::CreateSkeletonWidgetMenu()
{
	RebuildBoneList();

	TSharedPtr<SSearchBox> SearchWidgetToFocus = NULL;
	TSharedRef<SBorder> MenuWidget = SNew(SBorder)
		.Padding(6)
		.BorderImage(FEditorStyle::GetBrush("NoBorder"))
		.Content()
		[
			SNew(SBox)
			.WidthOverride(300)
			.HeightOverride(512)
			.Content()
			[
				SNew(SVerticalBox)
				+SVerticalBox::Slot()
				.AutoHeight()
				[
					SNew(STextBlock)
					.Font(FEditorStyle::GetFontStyle("BoldFont"))
						.Text(LOCTEXT("BonePickerTitle", "Pick Bone..."))
				]
				+SVerticalBox::Slot()
				.AutoHeight()
				[
					SNew(SSeparator)
					.SeparatorImage(FEditorStyle::GetBrush("Menu.Separator"))
						.Orientation(Orient_Horizontal)
				]
				+SVerticalBox::Slot()
				.AutoHeight()
				[
					SAssignNew(SearchWidgetToFocus, SSearchBox)
					.SelectAllTextWhenFocused(true)
					.OnTextChanged(this, &SIKinemaBoneSelectionWidget::OnFilterTextChanged)
					.OnTextCommitted(this, &SIKinemaBoneSelectionWidget::OnFilterTextCommitted)
					.HintText(NSLOCTEXT("BonePicker", "Search", "Search..."))
				]
				+SVerticalBox::Slot()
				[
					TreeView->AsShared()
				]
			]
		];

	BonePickerButton->SetMenuContentWidgetToFocus(SearchWidgetToFocus);

	return MenuWidget;
}

TSharedRef<ITableRow> SIKinemaBoneSelectionWidget::MakeTreeRowWidget(TSharedPtr<FBoneNameInfo> InInfo, const TSharedRef<STableViewBase>& OwnerTable)
{
	return SNew(STableRow<TSharedPtr<FBoneNameInfo>>, OwnerTable)
		.Content()
		[
			SNew(STextBlock)
			.HighlightText(FilterText)
			.Text(FText::FromName(InInfo->BoneName))
		];
}

void SIKinemaBoneSelectionWidget::GetChildrenForInfo(TSharedPtr<FBoneNameInfo> InInfo, TArray< TSharedPtr<FBoneNameInfo> >& OutChildren)
{
	OutChildren = InInfo->Children;
}

void SIKinemaBoneSelectionWidget::OnFilterTextChanged(const FText& InFilterText)
{
	FilterText = InFilterText;

	RebuildBoneList();
}

void SIKinemaBoneSelectionWidget::OnFilterTextCommitted(const FText& SearchText, ETextCommit::Type CommitInfo)
{
	// Already committed as the text was typed
}

void SIKinemaBoneSelectionWidget::RebuildBoneList()
{
	SkeletonTreeInfo.Empty();
	SkeletonTreeInfoFlat.Empty();
	const auto& Bones = TargetSkeleton->Bones;
	for(int32 BoneIdx = 0; BoneIdx < Bones.Num(); ++BoneIdx)
	{
		TSharedRef<FBoneNameInfo> BoneInfo = MakeShareable(new FBoneNameInfo(TargetSkeleton->GetBone(BoneIdx).Name));

		// Filter if Necessary
		if(!FilterText.IsEmpty() && !BoneInfo->BoneName.ToString().Contains(FilterText.ToString()))
		{
			continue;
		}

		int32 ParentIdx = TargetSkeleton->GetParentBoneIndex(BoneIdx);
		bool bAddToParent = false;

		if(ParentIdx != INDEX_NONE && FilterText.IsEmpty())
		{
			// We have a parent, search for it in the flat list
			FName ParentName = TargetSkeleton->GetBone(ParentIdx).Name;

			for(int32 FlatListIdx = 0; FlatListIdx < SkeletonTreeInfoFlat.Num(); ++FlatListIdx)
			{
				TSharedPtr<FBoneNameInfo> InfoEntry = SkeletonTreeInfoFlat[FlatListIdx];
				if(InfoEntry->BoneName == ParentName)
				{
					bAddToParent = true;
					ParentIdx = FlatListIdx;
					break;
				}
			}

			if(bAddToParent)
			{
				SkeletonTreeInfoFlat[ParentIdx]->Children.Add(BoneInfo);
			}
			else
			{
				SkeletonTreeInfo.Add(BoneInfo);
			}
		}
		else
		{
			SkeletonTreeInfo.Add(BoneInfo);
		}

		SkeletonTreeInfoFlat.Add(BoneInfo);
		TreeView->SetItemExpansion(BoneInfo, true);
	}

	TreeView->RequestTreeRefresh();
}

void SIKinemaBoneSelectionWidget::OnSelectionChanged(TSharedPtr<FBoneNameInfo> BoneInfo, ESelectInfo::Type SelectInfo)
{
	FilterText = FText::FromString("");

	//Because we recreate all our items on tree refresh we will get a spurious null selection event initially.
	if (BoneInfo.IsValid())
	{
		if (OnBoneSelectionChanged.IsBound())
		{
			OnBoneSelectionChanged.Execute(BoneInfo->BoneName);
		}

		BonePickerButton->SetIsOpen(false);
	}
}

FText SIKinemaBoneSelectionWidget::GetCurrentBoneName() const
{
	if(OnGetSelectedBone.IsBound())
	{
		FName Name = OnGetSelectedBone.Execute();

		return FText::FromName(Name);
	}

	// @todo implement default solution?
	return FText::GetEmpty();
}
#undef LOCTEXT_NAMESPACE

