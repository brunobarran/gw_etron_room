/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include "PhysicsPublic.h"

class FIKinemaRigTool;
class FIKinemaRigToolSharedData;
class SIKinemaRigToolPreviewViewport;


/*-----------------------------------------------------------------------------
FIKinemaRigToolViewportClient
-----------------------------------------------------------------------------*/

class FIKinemaRigToolEdPreviewViewportClient : public FEditorViewportClient
{
public:
	/** Constructor */
	FIKinemaRigToolEdPreviewViewportClient(TWeakPtr<FIKinemaRigTool> InIKinemaRigTool, TSharedPtr<FIKinemaRigToolSharedData> Data, const TSharedRef<SIKinemaRigToolPreviewViewport>& InIKinemaRigToolPreviewViewport);
	~FIKinemaRigToolEdPreviewViewportClient();

	/** FEditorViewportClient interface */
	virtual void DrawCanvas(FViewport& InViewport, FSceneView& View, FCanvas& Canvas) override;
	virtual void Draw(const FSceneView* View, FPrimitiveDrawInterface* PDI) override;
	virtual bool InputKey(FViewport* Viewport, int32 ControllerId, FKey Key, EInputEvent Event, float AmountDepressed = 1.0f, bool bGamepad = false) override;
	virtual bool InputAxis(FViewport* Viewport, int32 ControllerId, FKey Key, float Delta, float DeltaTime, int32 NumSamples = 1, bool bGamepad = false) override;
	virtual void ProcessClick(class FSceneView& View, class HHitProxy* HitProxy, FKey Key, EInputEvent Event, uint32 HitX, uint32 HitY) override;
	virtual bool InputWidgetDelta(FViewport* Viewport, EAxisList::Type CurrentAxis, FVector& Drag, FRotator& Rot, FVector& Scale) override;
	virtual void TrackingStarted(const struct FInputEventState& InInputState, bool bIsDragging, bool bNudge) override;
	virtual void TrackingStopped() override;

	virtual bool CanUseDragTool() const override;
	virtual FWidget::EWidgetMode GetWidgetMode() const override;
	virtual FVector GetWidgetLocation() const override;
	virtual FMatrix GetWidgetCoordSystem() const override;
	virtual ECoordSystem GetWidgetCoordSystemSpace() const override;
	virtual void Tick(float DeltaSeconds) override;
	virtual FSceneInterface* GetScene() const override;
	virtual FLinearColor GetBackgroundColor() const override;

private:
	/** Methods for building the various context menus */
	void OpenBodyMenu();
	void OpenConstraintMenu();
	void OpenCoMMenu();

	/** Called when no scene proxy is hit, deselects everything */
	void HitNothing();

	void StartManipulating(EAxisList::Type Axis, const FViewportClick& Click, const FMatrix& WorldToCamera);
	void EndManipulating();

private:
	/** Pointer back to the PhysicsAsset editor tool that owns us */
	TWeakPtr<FIKinemaRigTool> IKinemaRigToolPtr;

	/** Data and methods shared across multiple classes */
	TSharedPtr<FIKinemaRigToolSharedData> SharedData;

	/** Misc consts */
	const float	MinPrimSize;
	const float IKinemaRigTool_TranslateSpeed;
	const float IKinemaRigTool_RotateSpeed;
	const float IKinemaRigTool_LightRotSpeed;

	/** Font used for drawing debug text to the viewport */
	UFont* IKinemaRigToolFont;

	/** Misc members used for input handling */
	bool bAllowedToMoveCamera;

	float DragX;
	float DragY;

	FVector prevDrag;
};
