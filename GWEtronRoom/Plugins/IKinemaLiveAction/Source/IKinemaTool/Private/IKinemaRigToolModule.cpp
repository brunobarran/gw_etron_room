/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/


#include "IKinemaRigToolModule.h"
#include "Modules/ModuleManager.h"
#include "IKinemaRigTool.h"
#include "IKinemaRigToolStyle.h"
#include "IKinemaRigToolSharedData.h"

const FName IKinemaRigToolAppIdentifier = FName(TEXT("IKinemaRigToolApp"));


/*-----------------------------------------------------------------------------
   FIKinemaRigToolModule
-----------------------------------------------------------------------------*/

class FIKinemaRigToolModule : public IIKinemaRigToolModule
{
public:
	/** Constructor, set up console commands and variables **/
	FIKinemaRigToolModule()
	{
	}

	/** Called right after the module DLL has been loaded and the module object has been created */
	virtual void StartupModule() override
	{
		FIKinemaRigToolStyle::Initialize();

		MenuExtensibilityManager = MakeShareable(new FExtensibilityManager);
		ToolBarExtensibilityManager = MakeShareable(new FExtensibilityManager);
	}

	/** Called before the module is unloaded, right before the module object is destroyed. */
	virtual void ShutdownModule() override
	{
		FIKinemaRigToolStyle::Shutdown();

		MenuExtensibilityManager.Reset();
		ToolBarExtensibilityManager.Reset();
	}

	virtual TSharedRef<IIKinemaRigTool> CreateIKinemaRigTool(const EToolkitMode::Type Mode, const TSharedPtr< IToolkitHost >& InitToolkitHost, UIKinemaRig* IKinemaRig) override
	{
		TSharedRef<FIKinemaRigTool> NewIKinemaRigTool(new FIKinemaRigTool());
		NewIKinemaRigTool->InitIKinemaRigTool(Mode, InitToolkitHost, IKinemaRig);
		return NewIKinemaRigTool;
	}

	/** Gets the extensibility managers for outside entities to extend static mesh editor's menus and toolbars */
	virtual TSharedPtr<FExtensibilityManager> GetMenuExtensibilityManager() override { return MenuExtensibilityManager; }
	virtual TSharedPtr<FExtensibilityManager> GetToolBarExtensibilityManager() override { return ToolBarExtensibilityManager; }

private:
	TSharedPtr<FExtensibilityManager> MenuExtensibilityManager;
	TSharedPtr<FExtensibilityManager> ToolBarExtensibilityManager;
};

IMPLEMENT_MODULE(FIKinemaRigToolModule, IKinemaRigTool);
