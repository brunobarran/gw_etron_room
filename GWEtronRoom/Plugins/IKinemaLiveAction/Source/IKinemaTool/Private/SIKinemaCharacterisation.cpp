/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "SIKinemaCharacterisation.h"
#include "IKinemaRigToolModule.h"
#include "IKinemaBoneCharacterisationWidget.h"
#include "ObjectTools.h"
#include "ScopedTransaction.h"
#include "AssetRegistryModule.h"
#include "Editor/PropertyEditor/Public/PropertyEditorModule.h"
#include "Editor/ContentBrowser/Public/ContentBrowserModule.h"
#include "WorkflowOrientedApp/SContentReference.h"
#include "AssetNotifications.h"
#include "Animation/Rig.h"
#include "BoneSelectionWidget.h"
#include "Widgets/Input/SSearchBox.h"
#include "Widgets/Text/SInlineEditableTextBlock.h"

#define LOCTEXT_NAMESPACE "SIKinemaCharacterisationWindow"

static const FName ColumnId_NodeName1Label("Node Name");
static const FName ColumnID_BoneName1Label("Bone");

DECLARE_DELEGATE_ThreeParams(FOnBoneCharacterisationChanged, FName /** NodeName */, FName /** BoneName **/, int32);
DECLARE_DELEGATE_RetVal_OneParam(FName, FOnGetBoneCharacterisation, FName /** Node Name **/);

//////////////////////////////////////////////////////////////////////////
// SBoneCharacterisationListRow

typedef TSharedPtr< FDisplayedBoneCharacterisationInfo > FDisplayedBoneCharacterisationInfoPtr;

class SBoneCharacterisationListRow
	: public SMultiColumnTableRow< FDisplayedBoneCharacterisationInfoPtr >
{
public:

	SLATE_BEGIN_ARGS(SBoneCharacterisationListRow) {}

	/** The item for this row **/
	SLATE_ARGUMENT(FDisplayedBoneCharacterisationInfoPtr, Item)

		/* The SIKinemaRigWindow that handles all retarget sources */
		SLATE_ARGUMENT(class SIKinemaCharacterisationWindow*, RigWindow)

		/* Widget used to display the list of retarget sources*/
		SLATE_ARGUMENT(TSharedPtr<SBoneCharacterisationListType>, BoneCharacterisationListView)


		/* Persona used to update the viewport when a weight slider is dragged */
		SLATE_ARGUMENT(TWeakPtr<FIKinemaRigTool>, RigTool)

		SLATE_EVENT(FOnBoneCharacterisationChanged, OnBoneCharacterisationChanged)

		SLATE_EVENT(FOnGetBoneCharacterisation, OnGetBoneCharacterisation)

		SLATE_END_ARGS()

		void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& OwnerTableView);

	/** Overridden from SMultiColumnTableRow.  Generates a widget for this column of the tree row. */
	virtual TSharedRef<SWidget> GenerateWidgetForColumn(const FName& ColumnName) override;

private:

	/* The SIKinemaRigWindow that handles all retarget sources*/
	// @todo remove
	SIKinemaCharacterisationWindow* RigWindow;

	/** Widget used to display the list of retarget sources*/
	TSharedPtr<SBoneCharacterisationListType> BoneCharacterisationListView;


	/** The name and weight of the retarget source*/
	FDisplayedBoneCharacterisationInfoPtr	Item;

	/** Pointer back to the Persona that owns us */
	TWeakPtr<FIKinemaRigTool> RigToolPtr;

	// Bone tree widget delegates
	void OnBoneSelectionChanged(FName Name, int32 index);
	FReply OnClearButtonClicked();
	FName GetSelectedBone() const;

	FOnBoneCharacterisationChanged OnBoneCharacterisationChanged;
	FOnGetBoneCharacterisation OnGetBoneCharacterisation;
};

void SBoneCharacterisationListRow::Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& InOwnerTableView)
{
	Item = InArgs._Item;
	RigWindow = InArgs._RigWindow;
	BoneCharacterisationListView = InArgs._BoneCharacterisationListView;
	OnBoneCharacterisationChanged = InArgs._OnBoneCharacterisationChanged;
	OnGetBoneCharacterisation = InArgs._OnGetBoneCharacterisation;
	RigToolPtr = InArgs._RigTool;

	check(Item.IsValid());

	SMultiColumnTableRow< FDisplayedBoneCharacterisationInfoPtr >::Construct(FSuperRowType::FArguments(), InOwnerTableView);
}

TSharedRef< SWidget > SBoneCharacterisationListRow::GenerateWidgetForColumn(const FName& ColumnName)
{
	if (ColumnName == ColumnId_NodeName1Label)
	{
		TSharedPtr< SInlineEditableTextBlock > InlineWidget;
		TSharedRef< SWidget > NewWidget =
			SNew(SVerticalBox)

			+ SVerticalBox::Slot()
			.AutoHeight()
			.Padding(0.0f, 4.0f)
			.VAlign(VAlign_Center)
			[
				SAssignNew(InlineWidget, SInlineEditableTextBlock)
				.Text(FText::FromString(Item->GetDisplayName()))
			.HighlightText(RigWindow->GetFilterText())
			.IsReadOnly(true)
			.IsSelected(this, &SMultiColumnTableRow< FDisplayedBoneCharacterisationInfoPtr >::IsSelectedExclusively)
			];

		return NewWidget;
	}
	else
	{

		// show bone list
		// Encase the SSpinbox in an SVertical box so we can apply padding. Setting ItemHeight on the containing SListView has no effect :-(
		return
			SNew(SVerticalBox)

			+ SVerticalBox::Slot()
			.AutoHeight()
			.Padding(0.0f, 1.0f)
			.VAlign(VAlign_Center)
			[
				SNew(SHorizontalBox)

				+ SHorizontalBox::Slot()
			[
				SNew(SIKinemaBoneCharacterisationWidget)
				.Tooltip(FText::Format(LOCTEXT("BoneSelectinWidget", "Select Characterisation for Bone {0}"), FText::FromString(Item->GetDisplayName())))
			.OnBoneSelectionChanged(this, &SBoneCharacterisationListRow::OnBoneSelectionChanged)
			.OnGetSelectedBone(this, &SBoneCharacterisationListRow::GetSelectedBone)
			]

		+ SHorizontalBox::Slot()
			.AutoWidth()
			[
				SNew(SButton)
				.OnClicked(FOnClicked::CreateSP(this, &SBoneCharacterisationListRow::OnClearButtonClicked))
			.Text(FText::FromString(TEXT("x")))
			]
			];
	}
}

FReply SBoneCharacterisationListRow::OnClearButtonClicked()
{
	if (OnBoneCharacterisationChanged.IsBound())
	{
		OnBoneCharacterisationChanged.Execute(Item->GetNodeName(), NAME_None, INDEX_NONE);
	}

	return FReply::Handled();
}

void SBoneCharacterisationListRow::OnBoneSelectionChanged(FName Name, int32 Index)
{
	if (OnBoneCharacterisationChanged.IsBound())
	{
		OnBoneCharacterisationChanged.Execute(Item->GetNodeName(), Name, Index);
	}
}

FName SBoneCharacterisationListRow::GetSelectedBone() const
{
	if (OnGetBoneCharacterisation.IsBound())
	{
		return OnGetBoneCharacterisation.Execute(Item->GetNodeName());
	}

	// @todo delete?
	//	return Item->BoneName

	return NAME_None;
}

//////////////////////////////////////////////////////////////////////////
// SIKinemaRigWindow

void SIKinemaCharacterisationWindow::Construct(const FArguments& InArgs)
{
	RigToolPtr = InArgs._RigTool;
	bDisplayAdvanced = false;

	if (RigToolPtr.IsValid())
	{
		SharedData = RigToolPtr.Pin()->GetSharedData();
	}

	// @todo it will crash right nwo without Skeleton
	if (!SharedData.IsValid())
	{
		return;
	}
	auto SharedDataPtr = SharedData.Pin();

	ChildSlot
		[
			SNew(SVerticalBox)

			// first add rig asset picker
		+ SVerticalBox::Slot()
		.AutoHeight()
		[
			SNew(SHorizontalBox)

			+ SHorizontalBox::Slot()
		.AutoWidth()
		[
			SNew(STextBlock)
			.Text(LOCTEXT("RigNameLabel", "Select Rig "))
		.Font(FEditorStyle::GetFontStyle("Persona.RetargetManager.BoldFont"))
		]

		]

	// now show bone mapping
	+ SVerticalBox::Slot()
		.AutoHeight()
		.Padding(0, 2)
		[
			SNew(SHorizontalBox)
			// Filter entry
		+ SHorizontalBox::Slot()
		.FillWidth(1)
		[
			SAssignNew(NameFilterBox, SSearchBox)
			.SelectAllTextWhenFocused(true)
		.OnTextChanged(this, &SIKinemaCharacterisationWindow::OnFilterTextChanged)
		.OnTextCommitted(this, &SIKinemaCharacterisationWindow::OnFilterTextCommitted)
		]
		]

	+ SVerticalBox::Slot()
		.FillHeight(1.0f)		// This is required to make the scrollbar work, as content overflows Slate containers by default
		[
			SAssignNew(BoneCharacterisationListView, SBoneCharacterisationListType)
			.ListItemsSource(&BoneCharacterisationList)
		.OnGenerateRow(this, &SIKinemaCharacterisationWindow::GenerateBoneCharacterisationRow)
		.ItemHeight(22.0f)
		.HeaderRow
		(
			SNew(SHeaderRow)
			+ SHeaderRow::Column(ColumnId_NodeName1Label)
			.DefaultLabel(LOCTEXT("RigWindow_NodeNameLabel", "Bone (Skeleton)"))
			.ManualWidth(150.f)

			+ SHeaderRow::Column(ColumnID_BoneName1Label)
			.DefaultLabel(LOCTEXT("RigWindow_BoneNameLabel", "Node (Rig)"))
		)
		]
		];

	CreateBoneCharacterisationList();
}

void SIKinemaCharacterisationWindow::OnFilterTextChanged(const FText& SearchText)
{
	// need to make sure not to have the same text go
	// otherwise, the widget gets recreated multiple times causing 
	// other issue
	if (FilterText.CompareToCaseIgnored(SearchText) != 0)
	{
		FilterText = SearchText;

		CreateBoneCharacterisationList(SearchText.ToString());
	}
}

void SIKinemaCharacterisationWindow::OnFilterTextCommitted(const FText& SearchText, ETextCommit::Type CommitInfo)
{
	// Just do the same as if the user typed in the box
	OnFilterTextChanged(SearchText);
}

TSharedRef<ITableRow> SIKinemaCharacterisationWindow::GenerateBoneCharacterisationRow(TSharedPtr<FDisplayedBoneCharacterisationInfo> InInfo, const TSharedRef<STableViewBase>& OwnerTable)
{
	check(InInfo.IsValid());

	return
		SNew(SBoneCharacterisationListRow, OwnerTable)
		.RigTool(RigToolPtr)
		.Item(InInfo)
		.RigWindow(this)
		.BoneCharacterisationListView(BoneCharacterisationListView)
		.OnBoneCharacterisationChanged(this, &SIKinemaCharacterisationWindow::OnBoneCharacterisationChanged)
		.OnGetBoneCharacterisation(this, &SIKinemaCharacterisationWindow::GetBoneCharacterisation);
}

void SIKinemaCharacterisationWindow::CreateBoneCharacterisationList(const FString& SearchText)
{
	BoneCharacterisationList.Empty();

	const auto* Rig = SharedData.Pin()->IKinemaRig;

	if (Rig)
	{
		bool bDoFiltering = !SearchText.IsEmpty();
		const auto& Nodes = Rig->SolverDef.Bones;

		for (const auto Node : Nodes)
		{
			const FName& Name = Node.Name;


			if (bDoFiltering)
			{
				// make sure it doens't fit any of them
				if (!Name.ToString().Contains(SearchText))
				{
					continue; // Skip items that don't match our filter
				}
			}

			TSharedRef<FDisplayedBoneCharacterisationInfo> Info = FDisplayedBoneCharacterisationInfo::Make(Name);

			BoneCharacterisationList.Add(Info);

		}
	}

	BoneCharacterisationListView->RequestListRefresh();
}


void SIKinemaCharacterisationWindow::OnAssetSelected(UObject* Object)
{
	if (SharedData.IsValid())
	{
		AssetComboButton->SetIsOpen(false);

		const FScopedTransaction Transaction(LOCTEXT("RigAssetChanged", "Select Rig"));
		SharedData.Pin()->IKinemaRig->Modify();

		CreateBoneCharacterisationList(FilterText.ToString());
	}
}

/** Returns true if the asset shouldn't show  */
bool SIKinemaCharacterisationWindow::ShouldFilterAsset(const struct FAssetData& AssetData)
{
	return (AssetData.GetAsset() == GetRigObject());
}

UObject* SIKinemaCharacterisationWindow::GetRigObject() const
{
	return NULL;
}

SIKinemaCharacterisationWindow::~SIKinemaCharacterisationWindow()
{
	auto SharedDataPtr = SharedData.Pin();

	SharedDataPtr->bShowCollisionShapes = false;
}

void SIKinemaCharacterisationWindow::PostUndo()
{
	CreateBoneCharacterisationList(FilterText.ToString());
}

void SIKinemaCharacterisationWindow::OnBoneCharacterisationChanged(FName NodeName, FName BoneName, int32 CharacterisationIndex)
{
	const FScopedTransaction Transaction(LOCTEXT("BoneCharacterisationChanged", "Change Bone Characterisation"));
	auto Rig = SharedData.Pin()->IKinemaRig;

	//TODO: This change should happen in the rig. rather than here
	Rig->CharacteriseBone(NodeName, static_cast<EIKinemaCharacterDefinition>(CharacterisationIndex));
	Rig->Modify();
}

FName SIKinemaCharacterisationWindow::GetBoneCharacterisation(FName NodeName)
{

	UEnum* EnumPtr = nullptr;
	int index = UEnum::LookupEnumNameSlow(TEXT("EHIPS"), &EnumPtr);

	TArray< TPair<FName, int32> > SortedProviderNames;
	const int NumProviders = EnumPtr->NumEnums() - 1;
	//SortedProviderNames.Reserve(NumProviders);
	//for (int ProviderIndex = 0; ProviderIndex < NumProviders; ++ProviderIndex)
	//{
	//	const FText ProviderName = EnumPtr->GetDisplayNameText(ProviderIndex);
	//	int32 ProviderSortKey = ProviderName.ToString() == ("None") ? -1 * ProviderIndex : ProviderIndex;
	//	SortedProviderNames.Add(TPairInitializer<FName, int32>(FName(*ProviderName.ToString()), ProviderSortKey));
	//}


	auto Rig = SharedData.Pin()->IKinemaRig;
	int targetIndex = Rig->SolverDef.FindBoneIndex(NodeName);
	auto& target = Rig->SolverDef.Bones[targetIndex];

	const FText ProviderName = EnumPtr->GetDisplayNameTextByIndex(target.Characterisation);

	return FName(*ProviderName.ToString());
}

void SIKinemaCharacterisationWindow::CloseComboButton()
{
	AssetComboButton->SetIsOpen(false);
}

FText SIKinemaCharacterisationWindow::GetAssetName() const
{
	UObject* Rig = GetRigObject();
	if (Rig)
	{
		return FText::FromString(Rig->GetName());
	}

	return LOCTEXT("None", "None");
}

#undef LOCTEXT_NAMESPACE

