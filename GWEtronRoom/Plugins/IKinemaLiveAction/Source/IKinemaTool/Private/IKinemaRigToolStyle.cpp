/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaRigToolStyle.h"
#include "IKinemaRigToolModule.h"
#include "Slate/SlateGameResources.h"
#include "Interfaces/IPluginManager.h"

TSharedPtr< FSlateStyleSet > FIKinemaRigToolStyle::StyleInstance = NULL;

void FIKinemaRigToolStyle::Initialize()
{
	if (!StyleInstance.IsValid())
	{
		StyleInstance = Create();
		FSlateStyleRegistry::RegisterSlateStyle(*StyleInstance);
	}
}

void FIKinemaRigToolStyle::Shutdown()
{
	FSlateStyleRegistry::UnRegisterSlateStyle(*StyleInstance);
	ensure(StyleInstance.IsUnique());
	StyleInstance.Reset();
}

FName FIKinemaRigToolStyle::GetStyleSetName()
{
	static FName StyleSetName(TEXT("IKinemaRigToolStyle"));
	return StyleSetName;
}

#define IMAGE_BRUSH( RelativePath, ... ) FSlateImageBrush( Style->RootToContentDir( RelativePath, TEXT(".png") ), __VA_ARGS__ )
#define BOX_BRUSH( RelativePath, ... ) FSlateBoxBrush( Style->RootToContentDir( RelativePath, TEXT(".png") ), __VA_ARGS__ )
#define BORDER_BRUSH( RelativePath, ... ) FSlateBorderBrush( Style->RootToContentDir( RelativePath, TEXT(".png") ), __VA_ARGS__ )
#define TTF_FONT( RelativePath, ... ) FSlateFontInfo( Style->RootToContentDir( RelativePath, TEXT(".ttf") ), __VA_ARGS__ )
#define OTF_FONT( RelativePath, ... ) FSlateFontInfo( Style->RootToContentDir( RelativePath, TEXT(".otf") ), __VA_ARGS__ )

const FVector2D Icon16x16(16.0f, 16.0f);
const FVector2D Icon20x20(20.0f, 20.0f);
const FVector2D Icon40x40(40.0f, 40.0f);

TSharedRef< FSlateStyleSet > FIKinemaRigToolStyle::Create()
{
	TSharedRef< FSlateStyleSet > Style = MakeShareable(new FSlateStyleSet("IKinemaRigToolStyle"));
	Style->SetContentRoot(IPluginManager::Get().FindPlugin("IKinemaLiveAction")->GetBaseDir() / TEXT("Resources"));

	Style->Set("IKinemaRigTool.EditingMode_Body", new IMAGE_BRUSH("/RigTool/icon_IKinemaRigTool_bodymode_512x", Icon40x40));
	Style->Set("IKinemaRigTool.EditingMode_Constraint", new IMAGE_BRUSH("/RigTool/icon_ikinemarigtool_constraintmode_512x", Icon40x40));
	Style->Set("IKinemaRigTool.EditingMode_Body.Small", new IMAGE_BRUSH("/RigTool/icon_IKinemaRigTool_bodymode_512x", Icon20x20));
	Style->Set("IKinemaRigTool.EditingMode_Constraint.Small", new IMAGE_BRUSH("/RigTool/icon_ikinemarigtool_constraintmode_512x", Icon20x20));
	Style->Set("IKinemaRigTool.RepeatLastSimulation", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_simulate_512x", Icon40x40));

	Style->Set("IKinemaRigTool.AddOrientationalConstraint", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_addorientationtask_40x", Icon40x40));
	Style->Set("IKinemaRigTool.AddOrientationalConstraint.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_addorientationtask_40x", Icon20x20));
	Style->Set("IKinemaRigTool.AddPositionalConstraint", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_addpositiontask_40x", Icon40x40));
	Style->Set("IKinemaRigTool.AddPositionalConstraint.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_addpositiontask_40x", Icon20x20));
	Style->Set("IKinemaRigTool.AddDualConstraint", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_adddualtask_40x", Icon40x40));
	Style->Set("IKinemaRigTool.AddDualConstraint.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_adddualtask_40x", Icon20x20));
	
	Style->Set("IKinemaRigTool.CopyProperties", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_copytasks_40x", Icon40x40));
	Style->Set("IKinemaRigTool.CopyProperties.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_copytasks_40x", Icon20x20));
	Style->Set("IKinemaRigTool.CopyBones", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_copybones_40x", Icon40x40));
	Style->Set("IKinemaRigTool.CopyBones.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_copybones_40x", Icon20x20));
	Style->Set("IKinemaRigTool.CopyTasks", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_copytasks_40x", Icon40x40));
	Style->Set("IKinemaRigTool.CopyTasks.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_copytasks_40x", Icon20x20));

	Style->Set("IKinemaRigTool.PasteProperties", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_pastetasks_40x", Icon40x40));
	Style->Set("IKinemaRigTool.PasteProperties.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_pastetasks_40x", Icon20x20));
	Style->Set("IKinemaRigTool.PasteBones", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_pastebones_40x", Icon40x40));
	Style->Set("IKinemaRigTool.PasteBones.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_pastebones_40x", Icon20x20));
	Style->Set("IKinemaRigTool.PasteTasks", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_pastetasks_40x", Icon40x40));
	Style->Set("IKinemaRigTool.PasteTasks.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_pastetasks_40x", Icon20x20));

	Style->Set("IKinemaRigTool.ChangeDefaultMesh", new IMAGE_BRUSH("RigTool/icon_IKinemaRigTool_Mesh_40x", Icon40x40));

	Style->Set("IKinemaRigTool.ResetBoneSettings", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_resetbones_40x", Icon40x40));
	Style->Set("IKinemaRigTool.CopyJointSettings", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_copybones_40x", Icon40x40));
	Style->Set("IKinemaRigTool.PlayAnimation", new IMAGE_BRUSH("RigTool/icon_IKinemaRigTool_Play_40x", Icon40x40));
	Style->Set("IKinemaRigTool.DeleteBody", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_deletetbones_40x", Icon40x40));
	Style->Set("IKinemaRigTool.DeleteBody.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_deletetbones_40x", Icon20x20));
	Style->Set("IKinemaRigTool.ResetConstraint", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_resetasks_40x", Icon40x40));
	Style->Set("IKinemaRigTool.DeleteConstraint", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_deletetasks_40x", Icon40x40));
	Style->Set("IKinemaRigTool.RepeatLastSimulation.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_simulate_512x", Icon20x20));
	Style->Set("IKinemaRigTool.ToggleSimulation.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_simulate_512x", Icon20x20));

	Style->Set("IKinemaRigTool.ChangeDefaultMesh.Small", new IMAGE_BRUSH("RigTool/icon_IKinemaRigTool_Mesh_40x", Icon20x20));
	Style->Set("IKinemaRigTool.ResetBoneSettings.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_resetbones_40x", Icon20x20));
	Style->Set("IKinemaRigTool.PlayAnimation.Small", new IMAGE_BRUSH("RigTool/icon_IKinemaRigTool_Play_40x", Icon20x20));
	Style->Set("IKinemaRigTool.ResetConstraint.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_resetasks_40x", Icon20x20));
	Style->Set("IKinemaRigTool.DeleteConstraint.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_deletetasks_40x", Icon20x20));

	Style->Set("IKinemaRigTool.ResetCoMConstraint.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_resetasks_40x", Icon20x20));
	Style->Set("IKinemaRigTool.ResetCoMConstraint", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_resetasks_40x", Icon40x40));

	Style->Set("IKinemaRigTool.MapBones.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_Mapping_Editor_512px", Icon20x20));
	Style->Set("IKinemaRigTool.MapBones", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_Mapping_Editor_512px", Icon40x40));

	Style->Set("IKinemaRigTool.Characterise.Small", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_Self_Pen_512px", Icon20x20));
	Style->Set("IKinemaRigTool.Characterise", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_Self_Pen_512px", Icon40x40));

	Style->Set("IKinemaRigTool.Details", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_details_tab_40x", Icon20x20));
	Style->Set("IKinemaRigTool.Hierarchy", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_Skeleton_Hirarcy_40px", Icon20x20));
	Style->Set("IKinemaRigTool.MocapSource", new IMAGE_BRUSH("RigTool/icon_ikinemarigtool_Mocap_source_40px", Icon20x20));



	return Style;
}

#undef IMAGE_BRUSH
#undef BOX_BRUSH
#undef BORDER_BRUSH
#undef TTF_FONT
#undef OTF_FONT

void FIKinemaRigToolStyle::ReloadTextures()
{
	FSlateApplication::Get().GetRenderer()->ReloadTextureResources();
}

const ISlateStyle& FIKinemaRigToolStyle::Get()
{
	return *StyleInstance;
}
