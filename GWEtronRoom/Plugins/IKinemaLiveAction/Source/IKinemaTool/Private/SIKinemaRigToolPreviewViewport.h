/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include "SEditorViewport.h"

class FIKinemaRigTool;

/*-----------------------------------------------------------------------------
   SIKinemaRigToolViewport
-----------------------------------------------------------------------------*/

class SIKinemaRigToolPreviewViewport : public SEditorViewport
{
public:
	SLATE_BEGIN_ARGS(SIKinemaRigToolPreviewViewport)
		{}

		SLATE_ARGUMENT(TWeakPtr<FIKinemaRigTool>, IKinemaRigTool)
	SLATE_END_ARGS()

	/** Destructor */
	~SIKinemaRigToolPreviewViewport();

	/** SCompoundWidget interface */
	void Construct(const FArguments& InArgs);

	/** Refreshes the viewport */
	void RefreshViewport();

	/** Returns true if the viewport is visible */
	bool IsVisible() const override;

	/** Accessors */
	TSharedPtr<FSceneViewport> GetViewport() const;
	TSharedPtr<FIKinemaRigToolEdPreviewViewportClient> GetViewportClient() const;
	TSharedPtr<SViewport> GetViewportWidget() const;
	
public:
	/** The parent tab where this viewport resides */
	TWeakPtr<SDockTab> ParentTab;

	void SetViewportType(ELevelViewportType ViewType);

protected:
	/** SEditorViewport interface */
	virtual TSharedRef<FEditorViewportClient> MakeEditorViewportClient() override;
	virtual TSharedPtr<SWidget> MakeViewportToolbar() override;

private:
	/** Pointer back to the PhysicsAsset editor tool that owns us */
	TWeakPtr<FIKinemaRigTool> IKinemaRigToolPtr;
	
	/** Level viewport client */
	TSharedPtr<FIKinemaRigToolEdPreviewViewportClient> EditorViewportClient;
};
