/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once


/*-----------------------------------------------------------------------------
   SIKinemaRigToolPreviewViewportToolBar
-----------------------------------------------------------------------------*/
#include "SViewportToolbar.h"
class SIKinemaRigToolPreviewViewportToolBar : public SViewportToolBar
{
public:
	SLATE_BEGIN_ARGS(SIKinemaRigToolPreviewViewportToolBar){}
		SLATE_ARGUMENT(TWeakPtr<FIKinemaRigTool>, IKinemaRigToolPtr)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, TSharedPtr<class SEditorViewport> InRealViewport);

private:

	/** Generates the toolbar perspective menu content */
	TSharedRef<SWidget> GeneratePerspectiveMenu() const;
	/** Generates the toolbar view menu content */
	TSharedRef<SWidget> GenerateViewMenu() const;

	/** Generates the toolbar modes menu content */
	TSharedRef<SWidget> GenerateModesMenu() const;


	FText GetCameraMenuLabel() const;

	const FSlateBrush* GetCameraMenuLabelIcon() const;

	TSharedRef<SWidget> GenerateOptionsMenu() const;
	TSharedRef<SWidget> GenerateFOVMenu() const;
	float OnGetFOVValue() const;
	void OnFOVValueChanged(float NewValue);

private:
	/** The viewport that we are in */
	TWeakPtr<FIKinemaRigTool> IKinemaRigToolPtr;
};
