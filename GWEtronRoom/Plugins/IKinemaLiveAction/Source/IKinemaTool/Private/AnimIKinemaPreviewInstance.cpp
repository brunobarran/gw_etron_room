/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/


#include "AnimIKinemaPreviewInstance.h"
#include "IKinemaRigToolModule.h"
#include "IKinemaRigToolEdSkeletalMeshComponent.h"
#include "AnimIKinemaPreviewInstance.h"


#define LOCTEXT_NAMESPACE "AnimIKinemaPreviewInstance"


void FAnimIKinemaPreviewInstanceProxy::Initialize(UAnimInstance* InAnimInstance)
{
	FAnimSingleNodeInstanceProxy::Initialize(InAnimInstance);
}


void FAnimIKinemaPreviewInstanceProxy::Update(float DeltaTimeX)
{
	FAnimSingleNodeInstanceProxy::Update(DeltaTimeX);
}


bool FAnimIKinemaPreviewInstanceProxy::Evaluate(FPoseContext& Output)
{
	FAnimSingleNodeInstanceProxy::Evaluate(Output);

	UIKinemaRigToolEdSkeletalMeshComponent* Component = Cast<UIKinemaRigToolEdSkeletalMeshComponent>(GetSkelMeshComponent());
	if (bEnableControllers)
	{
		if (Component && GetSkeleton())
		{
			// create bone controllers from 
			if (BoneControllers.Num() > 0)
			{
				FPoseContext PreController(Output), PostController(Output);
				// if set key is true, we should save pre controller local space transform 
				// so that we can calculate the delta correctly


				FComponentSpacePoseContext ComponentSpacePoseContext(Output.AnimInstanceProxy);
				ComponentSpacePoseContext.Pose.InitPose(Output.Pose);

				// and now apply bone controllers data
				// it is possible they can be overlapping, but then bone controllers will overwrite
				ApplyBoneControllers(BoneControllers, ComponentSpacePoseContext);

				// convert back to local
				ComponentSpacePoseContext.Pose.ConvertToLocalPoses(Output.Pose);
			}
		}
	}
	return true;
}

FAnimNode_ModifyBone* FAnimIKinemaPreviewInstanceProxy::FindModifiedBone(const FName& InBoneName)
{
	TArray<FAnimNode_ModifyBone>& Controllers = BoneControllers;

	return Controllers.FindByPredicate(
		[InBoneName](const FAnimNode_ModifyBone& InController) -> bool
	{
		return InController.BoneToModify.BoneName == InBoneName;
	}
	);
}

FAnimNode_ModifyBone& FAnimIKinemaPreviewInstanceProxy::ModifyBone(const FName& InBoneName)
{
	FAnimNode_ModifyBone* SingleBoneController = FindModifiedBone(InBoneName);
	TArray<FAnimNode_ModifyBone>& Controllers = BoneControllers;

	if (SingleBoneController == nullptr)
	{
		int32 NewIndex = Controllers.Add(FAnimNode_ModifyBone());
		SingleBoneController = &Controllers[NewIndex];
	}

	SingleBoneController->BoneToModify.BoneName = InBoneName;
	SingleBoneController->TranslationMode = BMM_Replace;
	SingleBoneController->TranslationSpace = BCS_BoneSpace;

	SingleBoneController->RotationMode = BMM_Replace;
	SingleBoneController->RotationSpace = BCS_BoneSpace;

	SingleBoneController->ScaleMode = BMM_Replace;
	SingleBoneController->ScaleSpace = BCS_BoneSpace;


	return *SingleBoneController;
}

void FAnimIKinemaPreviewInstanceProxy::ApplyBoneControllers(TArray<FAnimNode_ModifyBone> &InBoneControllers, FComponentSpacePoseContext& ComponentSpacePoseContext)
{
	if (USkeleton* LocalSkeleton = ComponentSpacePoseContext.AnimInstanceProxy->GetSkeleton())
	{
		for (auto& SingleBoneController : InBoneControllers)
		{
			TArray<FBoneTransform> BoneTransforms;
			FAnimationCacheBonesContext Proxy(this);
			SingleBoneController.CacheBones_AnyThread(Proxy);
			if (SingleBoneController.IsValidToEvaluate(LocalSkeleton, ComponentSpacePoseContext.Pose.GetPose().GetBoneContainer()))
			{
				SingleBoneController.EvaluateSkeletalControl_AnyThread(ComponentSpacePoseContext, BoneTransforms);
				if (BoneTransforms.Num() > 0)
				{
					ComponentSpacePoseContext.Pose.LocalBlendCSBoneTransforms(BoneTransforms, 1.0f);
				}
			}
		}
	}
}

void FAnimIKinemaPreviewInstanceProxy::RemoveBoneModification(const FName& InBoneName)
{
	TArray<FAnimNode_ModifyBone>& Controllers = BoneControllers;
	Controllers.RemoveAll(
		[InBoneName](const FAnimNode_ModifyBone& InController)
	{
		return InController.BoneToModify.BoneName == InBoneName;
	}
	);
}

void FAnimIKinemaPreviewInstanceProxy::ResetModifiedBone()
{
	TArray<FAnimNode_ModifyBone>& Controllers = BoneControllers;
	Controllers.Empty();
}

UAnimIKinemaPreviewInstance::UAnimIKinemaPreviewInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UAnimIKinemaPreviewInstance::NativeInitializeAnimation()
{
	FAnimIKinemaPreviewInstanceProxy& Proxy = GetProxyOnGameThread<FAnimIKinemaPreviewInstanceProxy>();

	// Cache our play state from the previous animation otherwise set to play
	bool bCachedIsPlaying = (CurrentAsset != nullptr) ? Proxy.IsPlaying() : true;
	Super::NativeInitializeAnimation();
	Proxy.SetPlaying(bCachedIsPlaying);

}

static FArchive& operator<<(FArchive& Ar, FAnimNode_ModifyBone& BoneControllers)
{
	FAnimNode_ModifyBone::StaticStruct()->SerializeItem(Ar, &BoneControllers, nullptr);
	return Ar;
}

void UAnimIKinemaPreviewInstance::Serialize(FArchive& Ar)
{
	Super::Serialize(Ar);

	if (Ar.IsTransacting())
	{
		FAnimIKinemaPreviewInstanceProxy& Proxy = GetProxyOnGameThread<FAnimIKinemaPreviewInstanceProxy>();
		Ar << Proxy.GetBoneControllers();
	}
}

UAnimSequence* UAnimIKinemaPreviewInstance::GetAnimSequence()
{
	return Cast<UAnimSequence>(CurrentAsset);
}


void UAnimIKinemaPreviewInstance::SetAnimationAsset(UAnimationAsset* NewAsset, bool bIsLooping, float InPlayRate)
{
	FAnimIKinemaPreviewInstanceProxy& Proxy = GetProxyOnGameThread<FAnimIKinemaPreviewInstanceProxy>();

	// make sure to turn that off before setting new asset
	Proxy.GetRequiredBones().SetUseSourceData(false);

	Super::SetAnimationAsset(NewAsset, bIsLooping, InPlayRate);
	RootMotionMode = Cast<UAnimMontage>(CurrentAsset) != nullptr ? ERootMotionMode::RootMotionFromMontagesOnly : ERootMotionMode::RootMotionFromEverything;
}

FAnimInstanceProxy* UAnimIKinemaPreviewInstance::CreateAnimInstanceProxy()
{
	return new FAnimIKinemaPreviewInstanceProxy(this);
}

FAnimNode_ModifyBone& UAnimIKinemaPreviewInstance::ModifyBone(const FName& InBoneName)
{
	return GetProxyOnGameThread<FAnimIKinemaPreviewInstanceProxy>().ModifyBone(InBoneName);
}



void UAnimIKinemaPreviewInstance::RemoveBoneModification(const FName& InBoneName)
{
	GetProxyOnGameThread<FAnimIKinemaPreviewInstanceProxy>().RemoveBoneModification(InBoneName);
}

void UAnimIKinemaPreviewInstance::ResetModifiedBone()
{
	GetProxyOnGameThread<FAnimIKinemaPreviewInstanceProxy>().ResetModifiedBone();
}
#undef LOCTEXT_NAMESPACE