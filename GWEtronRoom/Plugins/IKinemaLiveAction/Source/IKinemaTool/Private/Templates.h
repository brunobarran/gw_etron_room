/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "IKinemaRig.h"
class FTemplate
{
	FName TemplateName;
	FString FilePath;
	TMultiMap<FString, FString> Map;

	void WriteTemplate();
public:
	FTemplate(UIKinemaRig* IKinemaRig, const FName& InTemplateName);
	FTemplate(const FName& InTemplateName, const FString& InJSON);
	const FName& GetName();
	const TArray<FString> GetTargets(const FString& Source) const;
	bool SourceExists(const FString& Source) const;
	void SetFilePath(const FString& path);
	bool DeleteTemplate();
	static TMap<FName, FTemplate> LoadTemplates();
	static TMap<FName, FTemplate> LoadDefaults();
};