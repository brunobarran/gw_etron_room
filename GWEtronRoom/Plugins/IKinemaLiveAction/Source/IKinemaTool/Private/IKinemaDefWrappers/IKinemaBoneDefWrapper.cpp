/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaBoneDefWrapper.h"
#define LOCTEXT_NAMESPACE "IKinemaRigTool"

UIKinemaBoneDefWrapper::UIKinemaBoneDefWrapper(const FObjectInitializer& object)
:Super(object)
{
}


#if WITH_EDITOR



void UIKinemaBoneDefWrapper::PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent)
{
	Super::PostEditChangeChainProperty(PropertyChangedEvent);
	
	if (rig)
	{
		rig->Modify();
		rig->UpdateVersion++;
	}
	if (BonePtr)
	{
		BonePtr->Name = BoneDef.Name;
		BonePtr->ParentName = BoneDef.ParentName;
		BonePtr->TaskIndex = BoneDef.TaskIndex;

		if (PropertyChangedEvent.Property->GetNameCPP() == FString("Characterisation"))
		{
			BoneDef.SelfColision.bIsEnabled = BonePtr->SelfColision.bIsEnabled = true;
			if ((BoneDef.Characterisation == EELBOW) || (BoneDef.Characterisation == EHAND))
			{
				BoneDef.SelfColision.Task.bIsPenetrating = true;
				rig->HandleSelfPenetrationChange(BoneDef);
			}
		}
		//If the bone is enabled for penetration, add in bones as self penetration
		if (PropertyChangedEvent.Property->GetNameCPP() == FString("bIsPenetrating"))
		{
			rig->HandleSelfPenetrationChange(BoneDef);
		}

		BonePtr->CopyBoneDefPropertiesFrom(BoneDef);
		BonePtr->RestPose = BoneDef.RestPose;
	}
}
bool UIKinemaBoneDefWrapper::CanEditChange(const UProperty * InProperty) const
{
	{
		bool ParentVal = Super::CanEditChange(InProperty);

		//show the projectile type dropdown only if the delivery method is projecile
		if (InProperty->GetFName() == GET_MEMBER_NAME_CHECKED(FIKinemaShape, BoxHalfLengths))
		{
			ParentVal = ParentVal && (BoneDef.SelfColision.CollisionShape.Shape == EIKinemaCollisionShape::EIKBOX);
		}
		else if (InProperty->GetFName() == GET_MEMBER_NAME_CHECKED(FIKinemaShape, Radius))
		{
			ParentVal = ParentVal && (BoneDef.SelfColision.CollisionShape.Shape == EIKinemaCollisionShape::EIKCAPSULE || BoneDef.SelfColision.CollisionShape.Shape == EIKinemaCollisionShape::EIKSPHERE);
		}
		else if (InProperty->GetFName() == GET_MEMBER_NAME_CHECKED(FIKinemaShape, HalfLength))
		{
			ParentVal = ParentVal && (BoneDef.SelfColision.CollisionShape.Shape == EIKinemaCollisionShape::EIKCAPSULE);
		}
		return ParentVal;

	}
}

void UIKinemaBoneDefWrapper::HandleSelfPenetration()
{
	
}
#endif

#undef LOCTEXT_NAMESPACE
