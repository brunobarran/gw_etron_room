/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "SLicensePanel.h"


#define LOCTEXT_NAMESPACE "IKinemaRigTool"


void SIKinemaLicenseDialog::Construct(const FArguments& InArgs)
{
	bOkClicked = false;
	bChanged = false;
	FMargin ButtonMargin(50, 50, 50, 10);
	ChildSlot
		[
			SNew(SBorder)
			.Visibility(EVisibility::Visible)
		.BorderImage(FEditorStyle::GetBrush("ChildWindow.Background"))
		.Padding(4.0f)
		[
			SNew(SBox)
			.Visibility(EVisibility::Visible)
		[
			SNew(SBorder)
			.BorderImage(FEditorStyle::GetBrush("ToolPanel.GroupBorder"))
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.AutoHeight()
		.Padding(8.0f, 8.0f, 8.0f, 8.0f)
		[
			
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.FillWidth(1.0f)
		.HAlign(HAlign_Left)
		.VAlign(VAlign_Center)
		.Padding(8)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("TargetSkeleton", "License Key:"))
		.ToolTipText(LOCTEXT("KeyHint", "The license key can be found in your order form."))
		.ShadowOffset(FVector2D(1.0f, 1.0f))
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		.Padding(8)
		[
			SNew(SEditableTextBox)
			//.Text(LOCTEXT("SampleKey", "565d871fc36cc8.29389100"))
		.Text(this, &SIKinemaLicenseDialog::GetLicense)
		.ToolTipText(LOCTEXT("KeyHint", "The license key can be found in your order form."))
		.OnTextCommitted(this, &SIKinemaLicenseDialog::OnTextCommitted)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
		
	+ SVerticalBox::Slot()
		.AutoHeight()
		.Padding(8.0f, 8.0f, 8.0f, 8.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.FillWidth(1.0f)
		.HAlign(HAlign_Left)
		.VAlign(VAlign_Center)
		.Padding(8)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("Expire", "License Expiration Date:"))
		.ShadowOffset(FVector2D(1.0f, 1.0f))
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		.Padding(8)
		[
			SNew(STextBlock)
			//.Text(LOCTEXT("SampleKey", "565d871fc36cc8.29389100"))
		.Text(this, &SIKinemaLicenseDialog::GetExpiration)
		.ToolTipText(LOCTEXT("Expiration", "License expiry date."))
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]

	+ SVerticalBox::Slot()
		.AutoHeight()
		.Padding(8.0f, 8.0f, 8.0f, 8.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.FillWidth(1.0f)
		.HAlign(HAlign_Left)
		.VAlign(VAlign_Center)
		.Padding(8)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("Options", "License Options:"))
		.ShadowOffset(FVector2D(1.0f, 1.0f))
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		.Padding(8)
		[
			SNew(STextBlock)
			//.Text(LOCTEXT("SampleKey", "565d871fc36cc8.29389100"))
		.Text(this, &SIKinemaLicenseDialog::GetOptions)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
		
		

	+ SVerticalBox::Slot()
		.AutoHeight()
		.Padding(8.0f, 8.0f, 8.0f, 8.0f)
		[
			SNew(SUniformGridPanel)
			.SlotPadding(FEditorStyle::GetMargin("StandardDialog.SlotPadding"))
		.MinDesiredSlotWidth(FEditorStyle::GetFloat("StandardDialog.MinDesiredSlotWidth"))
		.MinDesiredSlotHeight(FEditorStyle::GetFloat("StandardDialog.MinDesiredSlotHeight"))
	
	+ SUniformGridPanel::Slot(1, 0)
		[
			SNew(SButton)
			.HAlign(HAlign_Center)
		.VAlign(VAlign_Center)
		.ContentPadding(FEditorStyle::GetMargin("StandardDialog.ContentPadding"))
		.OnClicked(this, &SIKinemaLicenseDialog::OkClicked)
		.Text(LOCTEXT("IKinemaLicenseOk", "OK"))
		]
	+ SUniformGridPanel::Slot(2, 0)
		[
			SNew(SButton)
			.HAlign(HAlign_Center)
		.VAlign(VAlign_Center)
		.ContentPadding(FEditorStyle::GetMargin("StandardDialog.ContentPadding"))
		.OnClicked(this, &SIKinemaLicenseDialog::CancelClicked)
		.Text(LOCTEXT("CreateIKinemaRigCancel", "Cancel"))
		]
		]
		]
		]
		]
		];
}

/** Sets properties for the supplied AnimBlueprintFactory */
bool SIKinemaLicenseDialog::ConfigureProperties()
{
	FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(FPaths::ConvertRelativePathToFull(FPaths::EngineDir() + TEXT("Binaries/ThirdParty/IKinema/license"))));
	auto keyFile = licenseRelativeFolder;
	keyFile += "/ikey";
	TArray<FString> Key;
	bool haveKey = FFileHelper::LoadANSITextFileToStrings(*keyFile, &IFileManager::Get(), Key);
	TSharedRef<SWindow> Window = SNew(SWindow)
		.Title(LOCTEXT("IKinemaLicenseKey", "IKinema License Key"))
		.ClientSize(FVector2D(500, 220))
		.SupportsMinimize(false).SupportsMaximize(false)
		[
			AsShared()
		];

	PickerWindow = Window;

	GEditor->EditorAddModalWindow(Window);

	return bOkClicked;
}


	/** Handler for when ok is clicked */
	FReply SIKinemaLicenseDialog::OkClicked()
	{
		if (LicenseKey.ToString() != "")
		{
			FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(FPaths::ConvertRelativePathToFull(FPaths::EngineDir() + TEXT("Binaries/ThirdParty/IKinema/license"))));
			auto keyFile = licenseRelativeFolder;
			keyFile += "/ikey";
			FFileHelper::SaveStringToFile(LicenseKey.ToString(), *(keyFile));
			auto AbsoluteFilePath = licenseRelativeFolder + "/a0.lic";
			FPlatformFileManager::Get().GetPlatformFile().DeleteFile(*AbsoluteFilePath);
		}
	

		FLADataStreamModule& module = FModuleManager::GetModuleChecked< FLADataStreamModule >("LADataStreamCore");
		module.UpdateLicense();
		CloseDialog(true);

		return FReply::Handled();
	}

	void SIKinemaLicenseDialog::CloseDialog(bool bWasPicked)
	{
		bOkClicked = bWasPicked;
		if (PickerWindow.IsValid())
		{
			PickerWindow.Pin()->RequestDestroyWindow();
		}
	}

	/** Handler for when cancel is clicked */
	FReply SIKinemaLicenseDialog::CancelClicked()
	{
		CloseDialog();
		return FReply::Handled();
	}

	FReply SIKinemaLicenseDialog::OnKeyDown(const FGeometry& MyGeometry, const FKeyEvent& InKeyEvent) 
	{
		if (InKeyEvent.GetKey() == EKeys::Escape)
		{
			CloseDialog();
			return FReply::Handled();
		}
		return SWidget::OnKeyDown(MyGeometry, InKeyEvent);
	}

	void SIKinemaLicenseDialog::OnTextCommitted(const FText& InputText, ETextCommit::Type CommitType)
	{
	
			LicenseKey = InputText;
			bChanged = true;
	}

	FText SIKinemaLicenseDialog::GetLicense() const
	{
		
		if (!bChanged)
		{
			FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(FPaths::ConvertRelativePathToFull(FPaths::EngineDir() + TEXT("Binaries/ThirdParty/IKinema/license"))));
			auto keyFile = licenseRelativeFolder;
			keyFile += "/ikey";
			TArray<FString> Key;
			bool haveKey = FFileHelper::LoadANSITextFileToStrings(*keyFile, &IFileManager::Get(), Key);
			if (haveKey) {
				LicenseKey.FromString(Key[0]);
				return FText(FText::FromString(Key[0]));
			}else{
				UE_LOG(LogIKinemaLicensing, Warning, TEXT("Cannot find ikey file"));
			}
		}

		return LicenseKey;
	}
	FText SIKinemaLicenseDialog::GetExpiration() const
	{
		FLADataStreamModule& module = FModuleManager::GetModuleChecked< FLADataStreamModule >("LADataStreamCore");
		ExpirationDate date = module.RLMGetLicenseExpiration();
		if (date.days_left == 0)
		{
			date.str = "Expired";
		}
		if (!module.licValid())
		{
			date.str = "IKinema LiveAction license is not found or expired. \nPlease contact support@ikinema.com with the \ncontents of the UE4 log.";
		}
		return FText(FText::FromString(date.str));
	}

	FText SIKinemaLicenseDialog::GetOptions() const
	{
		FLADataStreamModule& module = FModuleManager::GetModuleChecked< FLADataStreamModule >("LADataStreamCore");
		char* options = module.GetOptions();
		return FText(FText::FromString(options));
	}

#undef LOCTEXT_NAMESPACE
DEFINE_LOG_CATEGORY(LogIKinemaLicensing);