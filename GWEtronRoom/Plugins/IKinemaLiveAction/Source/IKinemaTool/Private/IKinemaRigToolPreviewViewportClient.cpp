/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "IKinemaRigToolPreviewViewportClient.h"
#include "IKinemaRigToolModule.h"
#include "IKinemaRigToolEdSkeletalMeshComponent.h"
#include "IKinemaRigTool.h"
#include "ScopedTransaction.h"
#include "IKinemaRigToolHitProxies.h"
#include "IKinemaRigToolSharedData.h"
#include "SIKinemaRigToolPreviewViewport.h"
#include "GameFramework/WorldSettings.h"
#include "CanvasTypes.h"
#include "Engine/Font.h"
#include "IKinemaDefWrappers/IKinemaSourceBoneWrapper.h"

FIKinemaRigToolEdPreviewViewportClient::FIKinemaRigToolEdPreviewViewportClient(TWeakPtr<FIKinemaRigTool> InIKinemaRigTool, TSharedPtr<FIKinemaRigToolSharedData> Data, const TSharedRef<SIKinemaRigToolPreviewViewport>& InIKinemaRigToolPreviewViewport)
	: FEditorViewportClient(nullptr, &Data->PreviewScene, StaticCastSharedRef<SEditorViewport>(InIKinemaRigToolPreviewViewport))
	, IKinemaRigToolPtr(InIKinemaRigTool)
	, SharedData(Data)
	, MinPrimSize(0.5f)
	, IKinemaRigTool_TranslateSpeed(0.25f)
	, IKinemaRigTool_RotateSpeed(1.0 * (PI / 180.0))
	, IKinemaRigTool_LightRotSpeed(0.22f)
{
	check(IKinemaRigToolPtr.IsValid());



	ModeTools->SetWidgetMode(FWidget::EWidgetMode::WM_Translate);
	ModeTools->SetCoordSystem(COORD_Local);

	bAllowedToMoveCamera = true;

	// Setup defaults for the common draw helper.
	DrawHelper.bDrawPivot = false;
	DrawHelper.bDrawWorldBox = false;
	DrawHelper.bDrawKillZ = false;
	DrawHelper.GridColorAxis = FColor(80, 80, 80);
	DrawHelper.GridColorMajor = FColor(72, 72, 72);
	DrawHelper.GridColorMinor = FColor(64, 64, 64);
	DrawHelper.PerspectiveGridSize = 32767;

	IKinemaRigToolFont = GEngine->GetSmallFont();
	check(IKinemaRigToolFont);

	EngineShowFlags.DisableAdvancedFeatures();
	EngineShowFlags.CompositeEditorPrimitives = true;

	// Get actors asset collision bounding box, and move actor so its not intersection the floor plane at Z = 0.
	FBox CollBox = SharedData->EditorSkelComp->CalcBounds(SharedData->EditorSkelComp->GetComponentTransform()).GetBox();// ->CalcAABB(SharedData->EditorSkelComp, SharedData->EditorSkelComp->ComponentToWorld);
	FVector SkelCompLocation = FVector(0, 0, -CollBox.Min.Z);

	SharedData->EditorSkelComp->SetAbsolute(true, true, true);
	SharedData->EditorSkelComp->SetRelativeLocation(SkelCompLocation);

	// Take into account internal mesh translation/rotation/scaling etc.
	FTransform LocalToWorld = SharedData->EditorSkelComp->GetComponentTransform();
	FSphere WorldSphere = SharedData->EditorSkelMesh->GetBounds().GetSphere().TransformBy(LocalToWorld);

	SetViewLocationForOrbiting(FVector::ZeroVector);

	FVector CollBoxExtent = CollBox.GetExtent();
	if (CollBoxExtent.X > CollBoxExtent.Y)
	{
		SetViewLocation(FVector(WorldSphere.Center.X, -(WorldSphere.Center.Y - WorldSphere.W * 0.90f), WorldSphere.Center.Z));
		SetViewRotation(EditorViewportDefs::DefaultPerspectiveViewRotation);
	}
	else
	{
		SetViewLocation(FVector(WorldSphere.Center.X - 1.5*WorldSphere.W, WorldSphere.Center.Y, WorldSphere.Center.Z));
		SetViewRotation(FRotator::ZeroRotator);
	}

	

	SetViewModes(VMI_Lit, VMI_Lit);
	FEditorViewportClient::SetCameraSpeedSetting(2);
	bUsingOrbitCamera = false;
	prevDrag = FVector(0, 0, 0);
	if (!FIKinemaRigTool::IsPIERunning())
	{
		SetRealtime(true);
	}
}

FIKinemaRigToolEdPreviewViewportClient::~FIKinemaRigToolEdPreviewViewportClient()
{
}

void FIKinemaRigToolEdPreviewViewportClient::DrawCanvas(FViewport& InViewport, FSceneView& View, FCanvas& Canvas)
{
	if (!IKinemaRigToolPtr.IsValid())
	{
		return;
	}

	// Turn on/off the ground box
	SharedData->EditorFloorComp->SetVisibility(SharedData->bDrawGround);

	float W, H;
	IKinemaRigToolFont->GetCharSize(TEXT('L'), W, H);

	const float XOffset = 200.0f;

	FCanvasTextItem TextItem(FVector2D::ZeroVector, FText::GetEmpty(), IKinemaRigToolFont, FLinearColor::White);


	int32 HalfX = (Viewport->GetSizeXY().X - XOffset) / 2;
	int32 HalfY = Viewport->GetSizeXY().Y / 2;

	if ((SharedData->bShowHierarchy /*&& SharedData->EditorSimOptions->bShowNamesInHierarchy*/))
	{
		// Iterate over each graphics bone.
		for (int32 i = 0; i < SharedData->EditorSkelComp->GetNumComponentSpaceTransforms(); ++i)
		{
			FVector BonePos = SharedData->EditorSkelComp->GetComponentTransform().TransformPosition(SharedData->EditorSkelComp->GetComponentSpaceTransforms()[i].GetLocation());

			FPlane proj = View.Project(BonePos);
			if (proj.W > 0.f) // This avoids drawing bone names that are behind us.
			{
				int32 XPos = HalfX + (HalfX * proj.X);
				int32 YPos = HalfY + (HalfY * (proj.Y * -1));

				FName BoneName = SharedData->EditorSkelMesh->RefSkeleton.GetBoneName(i);

				FColor BoneNameColor = FColor::White;
				//iterate through selected bones and see if any match
				for (int32 j = 0; j < SharedData->SelectedBodies.Num(); ++j)
				{
					int32 SelectedBodyIndex = SharedData->SelectedBodies[j].Index;

					FString Name = SharedData->IKinemaRig->SolverDef.Bones[SelectedBodyIndex].Name.ToString();
					Name.Split(":", nullptr, &Name);
					Name = Name.Replace(TEXT("FBXASC032"), TEXT("-"));
					auto SelectedBoneName = FName(*Name);
					if (SelectedBoneName == BoneName)
					{
						BoneNameColor = FColor::Green;
						break;
					}

				}

				if (Canvas.IsHitTesting())
				{
					Canvas.SetHitProxy(new HIKinemaRigToolEdBoneNameProxy(i));
				}

				TextItem.Text = FText::FromString(BoneName.ToString());
				TextItem.SetColor(BoneNameColor);
				Canvas.DrawItem(TextItem, XPos, YPos);

				if (Canvas.IsHitTesting())
				{
					Canvas.SetHitProxy(NULL);
				}
			}
		}
	}

}

void FIKinemaRigToolEdPreviewViewportClient::Draw(const FSceneView* View, FPrimitiveDrawInterface* PDI)
{
	FEditorViewportClient::Draw(View, PDI);

	FIKinemaRigToolSharedData::EIKinemaRigToolRenderMode MeshViewMode = SharedData->GetCurrentMeshViewMode();

	if (MeshViewMode == FIKinemaRigToolSharedData::PRM_None)
	{
		SharedData->EditorSkelComp->SetVisibility(false);
	}
	else
	{
		SharedData->EditorSkelComp->SetVisibility(true);
	}

	if (MeshViewMode == FIKinemaRigToolSharedData::PRM_Wireframe)
	{
		SharedData->EditorSkelComp->SetForceWireframe(true);
	}
	else
	{
		SharedData->EditorSkelComp->SetForceWireframe(false);
	}

	// Draw IKinemaRigTool skeletal component.
	if (PDI->IsHitTesting())
	{
		SharedData->EditorSkelComp->RenderHitTest(View, PDI);
	}
	else
	{
		SharedData->EditorSkelComp->Render(View, PDI);
	}
}

bool FIKinemaRigToolEdPreviewViewportClient::InputKey(FViewport* InViewport, int32 ControllerId, FKey Key, EInputEvent Event, float AmountDepressed, bool Gamepad)
{
	bool bHandled = false;
	if (!bHandled)
	{
		bHandled = FEditorViewportClient::InputKey(InViewport, ControllerId, Key, Event, AmountDepressed, Gamepad);
	}

	if (bHandled)
	{
		Invalidate();
	}

	return bHandled;
}

bool FIKinemaRigToolEdPreviewViewportClient::InputAxis(FViewport* InViewport, int32 ControllerId, FKey Key, float Delta, float DeltaTime, int32 NumSamples, bool bGamepad)
{
	bool bHandled = false;
	if (SharedData->bManipulating)
	{
		InViewport->KeyState(EKeys::LeftControl) || InViewport->KeyState(EKeys::RightControl);
	}
	if (!bHandled)
	{
		bHandled = FEditorViewportClient::InputAxis(InViewport, ControllerId, Key, Delta, DeltaTime, NumSamples, bGamepad);
	}

	InViewport->Invalidate();

	return bHandled;
}

void FIKinemaRigToolEdPreviewViewportClient::ProcessClick(class FSceneView& InViewport, class HHitProxy* HitProxy, FKey Key, EInputEvent Event, uint32 HitX, uint32 HitY)
{
	if (Key == EKeys::LeftMouseButton)
	{
		auto bMultiSelection = IsShiftPressed();
		if (HitProxy && HitProxy->IsA(HIKinemaRigToolEdBoneProxy::StaticGetType()) && !IsCtrlPressed())
		{
			HIKinemaRigToolEdBoneProxy* BoneProxy = (HIKinemaRigToolEdBoneProxy*)HitProxy;

			if (!bMultiSelection)
			{
				SharedData->SetSelectedSourceBone(INDEX_NONE);
			}
			SharedData->HitBone(BoneProxy->BodyIndex, bMultiSelection);
		}
		else if (HitProxy && HitProxy->IsA(HIKinemaRigToolEdConstraintProxy::StaticGetType()))
		{
			HIKinemaRigToolEdConstraintProxy* ConstraintProxy = (HIKinemaRigToolEdConstraintProxy*)HitProxy;

			SharedData->HitConstraint(ConstraintProxy->ConstraintIndex, bMultiSelection);
		}
		else if (HitProxy && HitProxy->IsA(HIKinemaRigToolEdCoMConstraintProxy::StaticGetType()))
		{
			SharedData->HitCoMConstraint(bMultiSelection);
		}
		else if (HitProxy && HitProxy->IsA(HIKinemaRigToolEdSourceBoneProxy::StaticGetType()) && IsCtrlPressed())
		{
			HIKinemaRigToolEdSourceBoneProxy* BoneProxy = (HIKinemaRigToolEdSourceBoneProxy*)HitProxy;

			if (!bMultiSelection)
			{
				SharedData->SetSelectedBody(0);
			}
			SharedData->HitSourceBone(BoneProxy->BodyIndex, bMultiSelection);
		}
		else
		{
			HitNothing();
		}
	}
	else if (Key == EKeys::RightMouseButton)
	{
		if (HitProxy)
		{
			if (HitProxy->IsA(HIKinemaRigToolEdBoneProxy::StaticGetType()) && SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
			{
				HIKinemaRigToolEdBoneProxy* BoneProxy = (HIKinemaRigToolEdBoneProxy*)HitProxy;

				bool bAlreadySelected = false;

				//find out if body is already selected
				for (int32 i = 0; i < SharedData->SelectedBodies.Num(); ++i)
				{
					if (SharedData->SelectedBodies[i].Index == BoneProxy->BodyIndex)
					{
						bAlreadySelected = true;
						break;
					}
				}

				// Select body under cursor if not already selected	(if ctrl is held down we only add, not remove)
				if (!bAlreadySelected)
				{
					FIKinemaRigToolSharedData::FSelection Selection(BoneProxy->BodyIndex);
					SharedData->SetSelectedBody(&Selection, IsCtrlPressed());
				}

				// Pop up menu, if we have a body selected.
				if (SharedData->GetSelectedBody())
				{
					OpenBodyMenu();
				}
			}
			else if (HitProxy->IsA(HIKinemaRigToolEdConstraintProxy::StaticGetType()) && SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_ConstraintEdit)
			{
				HIKinemaRigToolEdConstraintProxy* ConstraintProxy = (HIKinemaRigToolEdConstraintProxy*)HitProxy;

				bool bAlreadySelected = false;

				//find out if constraint is already selected
				for (int32 i = 0; i < SharedData->SelectedConstraints.Num(); ++i)
				{
					if (SharedData->SelectedConstraints[i].Index == ConstraintProxy->ConstraintIndex)
					{
						bAlreadySelected = true;
						break;
					}
				}

				// Select constraint under cursor if not already selected (if ctrl is held down we only add, not remove)
				if (!bAlreadySelected)
				{
					SharedData->SetSelectedConstraint(ConstraintProxy->ConstraintIndex, IsCtrlPressed());
				}

				// Pop up menu, if we have a constraint selected.
				if (SharedData->GetSelectedConstraint())
				{
					OpenConstraintMenu();
				}
			}
			else if (HitProxy->IsA(HIKinemaRigToolEdCoMConstraintProxy::StaticGetType()) && SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_ConstraintEdit)
			{
				if (SharedData->bIsCoMSelected)
				{
					OpenCoMMenu();
				}
			}
		}
	}
}

bool FIKinemaRigToolEdPreviewViewportClient::InputWidgetDelta(FViewport* InViewport, EAxisList::Type CurrentAxis, FVector& Drag, FRotator& Rot, FVector& Scale)
{

	bool bHandled = false;
	bool SourceBody = false;

	bool bCtrlDown = InViewport->KeyState(EKeys::LeftControl) || InViewport->KeyState(EKeys::RightControl);
	bool bShiftDown = InViewport->KeyState(EKeys::LeftShift) || InViewport->KeyState(EKeys::RightShift);

	if (bCtrlDown || bShiftDown)
	{
		bHandled = true;
		return bHandled;
	}

	TArray<FIKinemaRigToolSharedData::FSelection> & SelectedObjects = SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit ?
		(SharedData->SelectedSourceBodies.Num() > 0 ? SharedData->SelectedSourceBodies : SharedData->SelectedBodies) : SharedData->SelectedConstraints;
	if ((SharedData->SelectedSourceBodies.Num() > 0))
	{
		SourceBody = true;
	}

	for (int32 i = 0; i < SelectedObjects.Num(); ++i)
	{
		FIKinemaRigToolSharedData::FSelection & SelectedObject = SelectedObjects[i];
		if (SharedData->bManipulating)
		{
			if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit) /// BODY EDITING ///
			{
				if (!SourceBody)
				{
					int32 BoneIndex = SharedData->EditorSkelComp->GetBoneIndex(SharedData->IKinemaRig->SolverDef.Bones[SelectedObject.Index].Name);
					FAnimNode_ModifyBone* SkelControl = NULL;
					if (BoneIndex >= 0)
					{
						//Get the skeleton control manipulating this bone
						const FName BoneName = SharedData->EditorSkelComp->SkeletalMesh->RefSkeleton.GetBoneName(BoneIndex);
						SkelControl = &(SharedData->EditorSkelComp->PreviewInstance->ModifyBone(BoneName));
					}


					FTransform BoneTM = SharedData->EditorSkelComp->GetBoneTransform(BoneIndex)* SharedData->EditorSkelComp->GetComponentTransform();;
					if (SkelControl)
					{
						FTransform CurrentSkelControlTM(SkelControl->Rotation, SkelControl->Translation, SkelControl->Scale);
						// Remove SkelControl's orientation from BoneMatrix, as we need to translate/rotate in the non-SkelControlled space
						BoneTM = BoneTM.GetRelativeTransformReverse(CurrentSkelControlTM);


						auto WidgetMode = GetWidgetMode();

						const bool bDoRotation = WidgetMode == FWidget::WM_Rotate || WidgetMode == FWidget::WM_TranslateRotateZ;
						const bool bDoTranslation = WidgetMode == (FWidget::WM_Translate || WidgetMode == FWidget::WM_TranslateRotateZ) && (CanUseDragTool() || SelectedObject.Index == 0 && !SharedData->bShowCollisionShapes);
						const bool bDoTranslationShape = WidgetMode == (FWidget::WM_Translate || WidgetMode == FWidget::WM_TranslateRotateZ) && SharedData->bShowCollisionShapes;
						const bool bDoScale = WidgetMode == FWidget::WM_Scale;
						if (bDoRotation)
						{
							FVector RotAxis;
							float RotAngle;
							Rot.Quaternion().ToAxisAndAngle(RotAxis, RotAngle);

							FVector4 BoneSpaceAxis = BoneTM.TransformVector(RotAxis);

							//Calculate the new delta rotation
							FQuat DeltaQuat(BoneSpaceAxis, RotAngle);
							DeltaQuat.Normalize();

							if (!SharedData->bShowCollisionShapes)
							{
								FRotator NewRotation = (CurrentSkelControlTM * FTransform(DeltaQuat)).Rotator();
								SkelControl->Rotation = NewRotation;
							}
							else
							{
								FRotator NewRotation = (SharedData->IKinemaRig->SolverDef.Bones[SelectedObject.Index].SelfColision.CollisionShape.LocalOffset * FTransform(DeltaQuat)).Rotator();
								SharedData->IKinemaRig->SolverDef.Bones[SelectedObject.Index].SelfColision.CollisionShape.LocalOffset.SetRotation(NewRotation.Quaternion());
							}

						}
						if (bDoTranslation)
						{
							FVector4 BoneSpaceOffset = BoneTM.TransformVector(Drag);
							SkelControl->Translation += BoneSpaceOffset;


						}
						if (bDoTranslationShape)
						{
							auto transbone = SharedData->IKinemaRig->SolverDef.Bones[SelectedObject.Index];

							FVector4 speed = Drag;


							bool normal = Drag.Normalize();
							FVector4 move = Drag * FVector(FMath::Abs(speed.X), FMath::Abs(speed.Y), FMath::Abs(speed.Z));
							move = BoneTM.TransformVector(move);

							transbone.SelfColision.CollisionShape.LocalOffset.SetTranslation(transbone.SelfColision.CollisionShape.LocalOffset.GetTranslation() + move);
							SharedData->IKinemaRig->SolverDef.Bones[SelectedObject.Index] = transbone;

							prevDrag = Drag;


						}

						if (bDoScale)
						{
							auto transbone = SharedData->IKinemaRig->SolverDef.Bones[SelectedObject.Index];


							if (transbone.SelfColision.CollisionShape.Shape == EIKinemaCollisionShape::EIKCAPSULE)
							{
								transbone.SelfColision.CollisionShape.HalfLength += Scale.X;
								transbone.SelfColision.CollisionShape.Radius += Scale.Z;
								transbone.SelfColision.CollisionShape.Radius += Scale.Y;
							}
							else
							{
								if (transbone.SelfColision.CollisionShape.Shape == EIKinemaCollisionShape::EIKBOX)
								{
									transbone.SelfColision.CollisionShape.BoxHalfLengths.X += Scale.Y;
									transbone.SelfColision.CollisionShape.BoxHalfLengths.Y += Scale.Z;
									transbone.SelfColision.CollisionShape.BoxHalfLengths.Z += Scale.X;

								}
							}
							SharedData->IKinemaRig->SolverDef.Bones[SelectedObject.Index] = transbone;


						}


						//Store the Delta Transform in the MatchBose
						auto& bone = SharedData->IKinemaRig->SolverDef.Bones[SelectedObject.Index];
						bone.MatchPose.SetRotation(SkelControl->Rotation.Quaternion());
						bone.MatchPose.SetTranslation(SkelControl->Translation);

					}
					auto& target = SharedData->IKinemaRig->SolverDef.Bones[SelectedObject.Index];
					if (SharedData->IKinemaRig->Actor)
					{
						if (target.SourceName != NAME_None)
						{
							const FScopedTransaction Transaction(NSLOCTEXT("IKinemaRigTool", "UpdateLinks", "Update Bone offsets"));
							auto& Bone = SharedData->IKinemaRig->Actor->GetBone(target.SourceName);
							SharedData->LinkBones(target, Bone);


						}
						else if (SharedData->IKinemaRig->SolverDef.Bones.Num() > 0)
						{
							const FScopedTransaction Transaction(NSLOCTEXT("IKinemaRigTool", "UpdateTasks", "Update Task offsets"));
							//Update task offsets
							for (auto& task : SharedData->IKinemaRig->SolverDef.Tasks)
							{
								FTransform ComponentToWorld = SharedData->EditorSkelComp->GetComponentTransform();
								auto& bone = SharedData->IKinemaRig->SolverDef.Bones[task.BoneIndex];
								BoneIndex = SharedData->EditorSkelComp->GetBoneIndex(bone.Name);
								if (BoneIndex != INDEX_NONE && task.SourceIndex != INDEX_NONE)
								{
									auto& source = SharedData->IKinemaRig->Actor->GetBone(task.SourceIndex);
									FTransform targetWorld = SharedData->EditorSkelComp->GetComponentSpaceTransforms()[BoneIndex] * ComponentToWorld;
									FTransform sourceWorld = source.GlobalTransform *SharedData->IKinemaRig->Actor->SourceTransform;
									auto srcInv = (sourceWorld.GetRotation()).Inverse();
									FVector scaledSource = sourceWorld.GetTranslation()*SharedData->IKinemaRig->SolverDef.ImportScale *SharedData->IKinemaRig->SolverDef.SourceScale;
									FVector POffset = ((targetWorld.GetTranslation()) - scaledSource) / SharedData->IKinemaRig->SolverDef.SourceScale;
									POffset = srcInv * POffset;
									POffset.Y = -POffset.Y;
									task.TaskOffset.SetTranslation(POffset);

									//Rotation Offset
									FQuat ROffset = srcInv *  targetWorld.GetRotation();
									ROffset.X = -ROffset.X;
									ROffset.Z = -ROffset.Z;
									task.TaskOffset.SetRotation(ROffset);
								}
							}
						}
					}
					SharedData->IKinemaRig->Modify();
					SharedData->IKinemaRig->PostLoadOrImport();
					SharedData->IKinemaRig->UpdateVersion++;
					SelectedObject.WidgetTM = BoneTM;
				}
				else
				{
					auto* Bone = &SharedData->IKinemaRig->Actor->GetBone(SelectedObject.Index);
					FAnimNode_ModifyBone* SkelControl = NULL;

					FTransform BoneTM = Bone->GlobalTransform *SharedData->IKinemaRig->Actor->SourceTransform;
					FTransform CurrentSkelControlTM(Bone->LocalTransform);
					// Remove SkelControl's orientation from BoneMatrix, as we need to translate/rotate in the non-SkelControlled space
					BoneTM = BoneTM.GetRelativeTransformReverse(CurrentSkelControlTM);


					auto WidgetMode = GetWidgetMode();

					const bool bDoRotation = WidgetMode == FWidget::WM_Rotate || WidgetMode == FWidget::WM_TranslateRotateZ;
					const bool bDoTranslation = (WidgetMode == FWidget::WM_Translate || WidgetMode == FWidget::WM_TranslateRotateZ) && (CanUseDragTool() || Bone->ParentName.IsNone());

					const bool bDoScale = WidgetMode == FWidget::WM_Scale;

					if (bDoRotation)
					{
						FVector RotAxis;
						float RotAngle;
						Rot.Quaternion().ToAxisAndAngle(RotAxis, RotAngle);


						FVector4 BoneSpaceAxis = (BoneTM).TransformVector(RotAxis);
						//Calculate the new delta rotation
						FQuat DeltaQuat(BoneSpaceAxis, RotAngle);
						DeltaQuat.Normalize();

						FRotator NewRotation = (CurrentSkelControlTM * FTransform(DeltaQuat)).Rotator();
						Bone->LocalTransform.SetRotation(NewRotation.Quaternion());
						Bone->Dirty = true;
					}
					if (bDoTranslation)
					{
						FVector4 BoneSpaceOffset = BoneTM.TransformVector(Drag / SharedData->IKinemaRig->Actor->ImportScale) + Bone->LocalTransform.GetTranslation();
						Bone->LocalTransform.SetTranslation(BoneSpaceOffset);
					}
					if (bDoScale)
					{
						FVector4 BoneSpaceScaleOffset;

						if (ModeTools->GetCoordSystem() == COORD_World)
						{
							BoneSpaceScaleOffset = BoneTM.TransformVector(Scale);
						}
						else
						{
							BoneSpaceScaleOffset = Scale;
						}
						BoneSpaceScaleOffset += Bone->LocalTransform.GetScale3D();

						Bone->LocalTransform.SetScale3D(BoneSpaceScaleOffset);
					}
					SharedData->IKinemaRig->Actor->DoFK(SelectedObject.Index);

					UIKinemaSourceBoneWrapper*  BoneWrapper = FindObject<UIKinemaSourceBoneWrapper>(SharedData->IKinemaRig->Actor->GetOutermost(), *(Bone->Name.ToString()));

					if (BoneWrapper)
					{
						BoneWrapper->BoneDef = *Bone;
					}
					auto TargetIndex = SharedData->IKinemaRig->SolverDef.FindBoneIndex(Bone->TargetBone);
					if (TargetIndex != INDEX_NONE)
					{
						const FScopedTransaction Transaction(NSLOCTEXT("IKinemaRigTool", "UpdateSource", "Update Bone offsets"));
						auto& target = SharedData->IKinemaRig->SolverDef.Bones[TargetIndex];
						SharedData->LinkBones(target, *Bone);

						SharedData->IKinemaRig->Modify();
						SharedData->IKinemaRig->PostLoadOrImport();
						SharedData->IKinemaRig->UpdateVersion++;
					}
					SelectedObject.WidgetTM = Bone->GlobalTransform;

				}
			}

			bHandled = true;
		}
	}

	return bHandled;

}

void FIKinemaRigToolEdPreviewViewportClient::TrackingStarted(const struct FInputEventState& InInputState, bool bIsDraggingWidget, bool bNudge)
{
	// If releasing the mouse button, check we are done manipulating
	if (bIsDraggingWidget)
	{
		FSceneViewFamilyContext ViewFamily(FSceneViewFamily::ConstructionValues(Viewport, GetScene(), EngineShowFlags));
		FSceneView* View = CalcSceneView(&ViewFamily);

		const int32	HitX = InInputState.GetViewport()->GetMouseX();
		const int32	HitY = InInputState.GetViewport()->GetMouseY();

		StartManipulating(Widget->GetCurrentAxis(), FViewportClick(View, this, InInputState.GetKey(), InInputState.GetInputEvent(), HitX, HitY), View->ViewMatrices.GetViewMatrix());

		// If we are manipulating, don't move the camera as we drag now.
		if (SharedData->bManipulating)
		{
			bAllowedToMoveCamera = false;
		}

	}
}

void FIKinemaRigToolEdPreviewViewportClient::TrackingStopped()
{
	if (SharedData->bManipulating)
	{
		EndManipulating();
		bAllowedToMoveCamera = true;
	}
}

bool FIKinemaRigToolEdPreviewViewportClient::CanUseDragTool() const
{
	return false;// !ShouldOrbitCamera() && (GetCurrentWidgetAxis() == EAxisList::None) && ((ModeTools == nullptr) || ModeTools->AllowsViewportDragTool());
}

FWidget::EWidgetMode FIKinemaRigToolEdPreviewViewportClient::GetWidgetMode() const
{
	FWidget::EWidgetMode ReturnWidgetMode = FWidget::WM_None;

	FWidget::EWidgetMode WidgetMode = FEditorViewportClient::GetWidgetMode();
	if ((SharedData->GetSelectedBody() || SharedData->GetSelectedSourceBody()) && !SharedData->GetSelectedConstraint())
	{
		ReturnWidgetMode = WidgetMode;
	}
	return ReturnWidgetMode;

}

FVector FIKinemaRigToolEdPreviewViewportClient::GetWidgetLocation() const
{
	FVector retVector(0.f, 0.f, 0.f);
	if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
	{
		if (!SharedData->GetSelectedSourceBody())
		{
			if (!SharedData->GetSelectedBody())
			{
				return FVector::ZeroVector;
			}
			int32 BoneIndex = SharedData->EditorSkelComp->GetBoneIndex(SharedData->IKinemaRig->SolverDef.Bones[SharedData->GetSelectedBody()->Index].Name);
			if (BoneIndex != INDEX_NONE)
			{
				if (!SharedData->bShowCollisionShapes)
				{
					FTransform WorldTransforms = SharedData->EditorSkelComp->GetComponentSpaceTransforms()[BoneIndex] * SharedData->EditorSkelComp->GetComponentTransform();
					retVector = WorldTransforms.GetTranslation();
				}
				else
				{
					FTransform WorldTransforms = SharedData->IKinemaRig->SolverDef.Bones[SharedData->GetSelectedBody()->Index].SelfColision.CollisionShape.LocalOffset * SharedData->EditorSkelComp->GetComponentSpaceTransforms()[BoneIndex] * SharedData->EditorSkelComp->GetComponentTransform();
					retVector = WorldTransforms.GetTranslation();
				}
			}
		}
		else
		{
			auto& SourceBody = SharedData->IKinemaRig->Actor->GetBone(SharedData->GetSelectedSourceBody()->Index);
			auto transform = SourceBody.GlobalTransform *SharedData->IKinemaRig->Actor->SourceTransform;
			retVector = transform.GetTranslation() * SharedData->IKinemaRig->Actor->ImportScale;
		}
	}
	return retVector;
}

FMatrix FIKinemaRigToolEdPreviewViewportClient::GetWidgetCoordSystem() const
{

	if (GetWidgetCoordSystemSpace() == COORD_Local)
	{
		if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit) /// BODY EDITING ///
		{
			// Don't draw widget if nothing selected.
			if (SharedData->GetSelectedSourceBody())
			{
				const auto& bone = SharedData->IKinemaRig->Actor->GetBone(SharedData->GetSelectedSourceBody()->Index);
				FTransform LocalTransform = bone.GlobalTransform * SharedData->IKinemaRig->Actor->SourceTransform;
				return LocalTransform.ToMatrixNoScale().RemoveTranslation();
			}
			if (!SharedData->GetSelectedBody())
			{
				return FMatrix::Identity;
			}
			int32 BoneIndex = SharedData->EditorSkelComp->GetBoneIndex(SharedData->IKinemaRig->SolverDef.Bones[SharedData->GetSelectedBody()->Index].Name);
			if (BoneIndex != INDEX_NONE)
			{
				FTransform LocalTransform = SharedData->EditorSkelComp->GetComponentSpaceTransforms()[BoneIndex];

				return LocalTransform.ToMatrixNoScale().RemoveTranslation();
			}
			else
			{
				return FMatrix::Identity;
			}
		}
		else
		{
			if (!SharedData->GetSelectedConstraint())
			{
				return FMatrix::Identity;
			}
			auto& task = SharedData->IKinemaRig->SolverDef.Tasks[SharedData->GetSelectedConstraint()->Index].BoneIndex;
			FString Name = SharedData->IKinemaRig->SolverDef.Bones[task].Name.ToString();
			Name.Split(":", nullptr, &Name);
			Name = Name.Replace(TEXT("FBXASC032"), TEXT("-"));
			auto SelectedBoneName = FName(*Name);
			int32 BoneIndex = SharedData->EditorSkelComp->GetBoneIndex(SelectedBoneName);
			if (BoneIndex == INDEX_NONE)
			{
				return FMatrix::Identity;
			}
			FTransform LocalTransform = SharedData->EditorSkelComp->GetComponentSpaceTransforms()[BoneIndex];
			return LocalTransform.ToMatrixNoScale().RemoveTranslation();
		}
	}
	else
	{
		return FMatrix::Identity;
	}
}


ECoordSystem FIKinemaRigToolEdPreviewViewportClient::GetWidgetCoordSystemSpace() const
{
	//We always operate in World Space
	return ModeTools->GetCoordSystem();
}

void FIKinemaRigToolEdPreviewViewportClient::Tick(float DeltaSeconds)
{
	FEditorViewportClient::Tick(DeltaSeconds);
	UWorld* World = SharedData->PreviewScene.GetWorld();

	World->Tick(LEVELTICK_All, DeltaSeconds);
	if (SharedData->bRunningSimulation)
	{
		// check if PIE disabled the realtime viewport and quit sim if so
		if (!bIsRealtime)
		{
			SharedData->ToggleSimulation();
			Invalidate();
		}

		AWorldSettings* Setting = World->GetWorldSettings();
		Setting->WorldGravityZ = 0.0f;
		Setting->bWorldGravitySet = true;

		// We back up the transforms array now
		SharedData->EditorSkelComp->AnimationSpaceBases = SharedData->EditorSkelComp->GetComponentSpaceTransforms();
	}
	/*
	if (SharedData->Recorder.InRecording())
	{
	// make sure you don't allow switch SharedData->EditorSkelComp
	SharedData->Recorder.UpdateRecord(SharedData->EditorSkelComp, DeltaSeconds);
	}
	*/
}

FSceneInterface* FIKinemaRigToolEdPreviewViewportClient::GetScene() const
{
	return SharedData->PreviewScene.GetScene();
}

FLinearColor FIKinemaRigToolEdPreviewViewportClient::GetBackgroundColor() const
{
	return FColor(64, 64, 64);
}

void FIKinemaRigToolEdPreviewViewportClient::OpenBodyMenu()
{
	TSharedPtr<SWidget> MenuWidget = IKinemaRigToolPtr.Pin()->BuildMenuWidgetBody();
	TSharedPtr< SIKinemaRigToolPreviewViewport > ParentWidget = IKinemaRigToolPtr.Pin()->GetPreviewViewportWidget();

	if (MenuWidget.IsValid() && ParentWidget.IsValid())
	{
		const FVector2D MouseCursorLocation = FSlateApplication::Get().GetCursorPos();
		FSlateApplication::Get().PushMenu(
			ParentWidget.ToSharedRef(),
			FWidgetPath(),
			MenuWidget.ToSharedRef(),
			MouseCursorLocation,
			FPopupTransitionEffect(FPopupTransitionEffect::ContextMenu)
		);
	}

}

void FIKinemaRigToolEdPreviewViewportClient::OpenConstraintMenu()
{
	TSharedPtr<SWidget> MenuWidget = IKinemaRigToolPtr.Pin()->BuildMenuWidgetConstraint();
	TSharedPtr< SIKinemaRigToolPreviewViewport > ParentWidget = IKinemaRigToolPtr.Pin()->GetPreviewViewportWidget();

	if (MenuWidget.IsValid() && ParentWidget.IsValid())
	{
		const FVector2D MouseCursorLocation = FSlateApplication::Get().GetCursorPos();

		FSlateApplication::Get().PushMenu(
			ParentWidget.ToSharedRef(),
			FWidgetPath(),
			MenuWidget.ToSharedRef(),
			MouseCursorLocation,
			FPopupTransitionEffect(FPopupTransitionEffect::ContextMenu));
	}
}

void FIKinemaRigToolEdPreviewViewportClient::OpenCoMMenu()
{
	TSharedPtr<SWidget> MenuWidget = IKinemaRigToolPtr.Pin()->BuildMenuWidgetCoM();
	TSharedPtr< SIKinemaRigToolPreviewViewport > ParentWidget = IKinemaRigToolPtr.Pin()->GetPreviewViewportWidget();

	if (MenuWidget.IsValid() && ParentWidget.IsValid())
	{
		const FVector2D MouseCursorLocation = FSlateApplication::Get().GetCursorPos();

		FSlateApplication::Get().PushMenu(
			ParentWidget.ToSharedRef(),
			FWidgetPath(),
			MenuWidget.ToSharedRef(),
			MouseCursorLocation,
			FPopupTransitionEffect(FPopupTransitionEffect::ContextMenu));
	}
}


void FIKinemaRigToolEdPreviewViewportClient::StartManipulating(EAxisList::Type Axis, const FViewportClick& Click, const FMatrix& WorldToCamera)
{
	//Set selected object Widget and Manipulate TMs to identity, except for LookAt task.
	//Extend the target in the LookAt axis direction
	if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
	{

		if (SharedData->GetSelectedSourceBody())
		{
			if (GetWidgetMode() == FWidget::WM_Rotate)
			{
				GEditor->BeginTransaction(NSLOCTEXT("UnrealEd", "RotateSourceBone", "Rotate Source Bone"));
			}
			else
			{
				GEditor->BeginTransaction(NSLOCTEXT("UnrealEd", "TranslateSourceBone", "Translate Source Bone"));
			}
			for (int32 i = 0; i < SharedData->SelectedSourceBodies.Num(); ++i)
			{
				SharedData->IKinemaRig->Actor->GetBone(SharedData->SelectedSourceBodies[i].Index).Dirty = true;
			}
			SharedData->IKinemaRig->Actor->Modify();
		}
		else if (SharedData->GetSelectedBody())
		{
			if (GetWidgetMode() == FWidget::WM_Rotate)
			{
				GEditor->BeginTransaction(NSLOCTEXT("UnrealEd", "RotateBone", "Rotate Bone"));
			}
			else
			{
				GEditor->BeginTransaction(NSLOCTEXT("UnrealEd", "TranslateBone", "Translate Bone"));
			}

			SharedData->EditorSkelComp->PreviewInstance->SetFlags(RF_Transactional);	// Undo doesn't work without this!
			SharedData->EditorSkelComp->PreviewInstance->Modify();


			// now modify the bone array
			const FName BoneName = SharedData->EditorSkelComp->SkeletalMesh->RefSkeleton.GetBoneName(SharedData->GetSelectedBody()->Index);
			SharedData->EditorSkelComp->PreviewInstance->ModifyBone(BoneName);
			SharedData->EditorSkelComp->PreviewInstance->EnableControllers(true);
		}
		SharedData->bManipulating = true;
	}
}

void FIKinemaRigToolEdPreviewViewportClient::EndManipulating()
{
	if (SharedData->bManipulating)
	{
		prevDrag.Set(0, 0, 0);
		SharedData->bManipulating = false;
		GEditor->EndTransaction();
		Viewport->Invalidate();
	}
}


void FIKinemaRigToolEdPreviewViewportClient::HitNothing()
{
	if (!SharedData->bSelectionLock)
	{
		if (SharedData->EditingMode == FIKinemaRigToolSharedData::PEM_BodyEdit)
		{
			if (IsCtrlPressed() == false)	//we only want to deselect if Ctrl is not used
			{
				SharedData->SetSelectedBody(nullptr);
				SharedData->SetSelectedSourceBone(INDEX_NONE);

			}
		}
		else
		{
			if (IsCtrlPressed() == false)	//we only want to deselect if Ctrl is not used
			{
				SharedData->SetSelectedConstraint(INDEX_NONE);
			}
		}
	}

	if (!SharedData->bSelectionLock)
	{
		Viewport->Invalidate();
		IKinemaRigToolPtr.Pin()->RefreshHierachyTree();
		//for (auto& bone : SharedData->IKinemaRig->SolverDef.Bones)
		//{
		//	if (bone.SourceName != NAME_None)
		//	{
		//		auto& source = SharedData->IKinemaRig->Actor->GetBone(bone.SourceName);
		//		SharedData->LinkBones(bone, source);
		//	}
		//}

	}
}
