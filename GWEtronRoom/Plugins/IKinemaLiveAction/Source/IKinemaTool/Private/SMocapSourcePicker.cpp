/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "SMocapSourcePicker.h"
#include "IKinemaRigToolModule.h"
#include "Templates.h"
#include "LADataStreamModule.h"

#define LOCTEXT_NAMESPACE "SMocapSourcePicker"

void SMocapSourcePicker::Construct(const FArguments& InArgs)
{
	bIsYUp = false;
	bIsRigidBody = false;
	FLADataStreamModule& module = FModuleManager::GetModuleChecked< FLADataStreamModule >("LADataStreamCore");
	IsLicRB = EVisibility::Hidden;
	if (module.IsRigidBody())
	{
		IsLicRB = EVisibility::Visible;
	}

	ImportScale = 1.f;
	OnSourceScaleChanged = InArgs._OnSourceScaleChanged;

	if (InArgs._renderTag == RenderTag::LiveLinkMenu)
	{
		MakeLiveLinkWeidget();
	}
	else
	{
		MakeStandardWidget();
	}
}

bool SMocapSourcePicker::CanZeroRotations() const
{
	return (MocapServer == EMocapServer::NatNet) | (MocapServer == EMocapServer::XSens) | (MocapServer == EMocapServer::VRPN);
}



void SMocapSourcePicker::OnZeroRotations(ECheckBoxState InState)
{
	switch (InState)
	{
	case ECheckBoxState::Unchecked:
		bZeroRotations = false;
		break;
	case ECheckBoxState::Checked:
		bZeroRotations = true;
		break;
	default:
		bZeroRotations = false;
	}
}


void SMocapSourcePicker::SetYup(ECheckBoxState InState)
{
	switch (InState)
	{
	case ECheckBoxState::Unchecked:
		bIsYUp = false;
		break;
	case ECheckBoxState::Checked:
		bIsYUp = true;
		break;
	default:
		bIsYUp = false;
	}
}

void SMocapSourcePicker::SetRigidBodies(ECheckBoxState InState)
{
	switch (InState)
	{
	case ECheckBoxState::Unchecked:
		bIsRigidBody = false;
		break;
	case ECheckBoxState::Checked:
		bIsRigidBody = true;
		break;
	default:
		bIsRigidBody = false;
	}
}

void SMocapSourcePicker::OnServerAddressCommitted(const FText& InputText, ETextCommit::Type CommitType)
{
	if (InputText.ToString().ToLower() == "localhost") {
		UE_LOG(LogIKinemaRunTimeRigTool, Warning, TEXT("Localhost not recomended. Use format: x.x.x.x"));
		ServerAddressBox->SetText(FText::FromString("127.0.0.1"));
	}
	ServerIP = InputText.ToString();
}

void SMocapSourcePicker::OnServerPortCommitted(const FText& InputText, ETextCommit::Type CommitType)
{
	TDefaultNumericTypeInterface<int> Conversion;
	auto optional = Conversion.FromString(InputText.ToString(), 0);
	PortNumber = optional.Get(801);
}

void SMocapSourcePicker::OnImportScaleCommitted(const FText& InputText, ETextCommit::Type CommitType)
{
	TDefaultNumericTypeInterface<float> Conversion;
	auto optional = Conversion.FromString(InputText.ToString(), 0);
	ImportScale = optional.Get(1.f);

	if (OnSourceScaleChanged.IsBound())
	{
		OnSourceScaleChanged.Execute(ImportScale);
	}

}

void SMocapSourcePicker::OnSubjectNameCommitted(const FText& InputText, ETextCommit::Type CommitType)
{
	SubjectName = InputText.ToString();
}

const EMocapServer SMocapSourcePicker::GetMocapServer() const
{
	return MocapServer;
}

const FString & SMocapSourcePicker::GetServerIP() const
{
	return ServerIP;
}

const FString & SMocapSourcePicker::GetSubjectName() const
{
	return SubjectName;
}

const FString & SMocapSourcePicker::GetTemplateName() const
{
	return TemplateName;
}

const int SMocapSourcePicker::GetPortNumber() const
{
	return PortNumber;
}

const float SMocapSourcePicker::GetImportScale() const
{
	return ImportScale;
}

void SMocapSourcePicker::ChangeMocapSourceProvider(int32 ProviderIndex)
{
	UEnum* EnumPtr = nullptr;
	int index = UEnum::LookupEnumNameSlow(TEXT("VICON"), &EnumPtr);

	MocapServer = (EMocapServer)EnumPtr->GetValueByIndex(ProviderIndex);



	if (MocapServer == EMocapServer::LiveLink)
	{
		MakeLiveLinkWeidget();
	}
	else
	{
		MakeStandardWidget();
	}

	SetServerIP(ServerIP);
	SetSubjectName(SubjectName);
	SetPortNumber(PortNumber);
	SetMocapServer(MocapServer);
	SetImportScale(ImportScale);
	SetTemplateName(TemplateName);
}

void SMocapSourcePicker::ChangeTemplateName(int32 TemplateIndex)
{

	if (TemplateIndex == -1)
	{
		TemplateName = "None";
		return;
	}

	auto Templates = FTemplate::LoadTemplates();

	TArray<FName> temp;
	Templates.GenerateKeyArray(temp);

	TemplateName = temp[TemplateIndex].ToString();
}

TSharedRef<SWidget> SMocapSourcePicker::OnGetMenuContent() const
{
	FMenuBuilder MenuBuilder(true, NULL);

	// Get the provider names first so that we can sort them for the UI
	UEnum* EnumPtr = nullptr;
	int index = UEnum::LookupEnumNameSlow(TEXT("VICON"), &EnumPtr);

	TArray< TPair<FName, int32> > SortedProviderNames;
	const int NumProviders = EnumPtr->NumEnums() - 1;
	SortedProviderNames.Reserve(NumProviders);
	for (int ProviderIndex = 0; ProviderIndex < NumProviders; ++ProviderIndex)
	{
		const FName ProviderName = EnumPtr->GetNameByIndex(ProviderIndex);
		int32 ProviderSortKey = ProviderName == FName("None") ? -1 * ProviderIndex : ProviderIndex;
		SortedProviderNames.Add(TPairInitializer<FName, int32>(ProviderName, ProviderSortKey));
	}


	// Sort based on the provider index
	SortedProviderNames.Sort([](const TPair<FName, int32>& One, const TPair<FName, int32>& Two)
	{
		return One.Value < Two.Value;
	});

	for (auto SortedProviderNameIter = SortedProviderNames.CreateConstIterator(); SortedProviderNameIter; ++SortedProviderNameIter)
	{
		const FText ProviderText = GetProviderText(SortedProviderNameIter->Key);
		const int32  ProviderIndex = SortedProviderNameIter->Value;

		FFormatNamedArguments Arguments;
		Arguments.Add(TEXT("ProviderName"), ProviderText);
		MenuBuilder.AddMenuEntry(
			ProviderText,
			FText::Format(LOCTEXT("MocapSourceProvider_Tooltip", "Use {ProviderName} as Mocap source provider"), Arguments),
			FSlateIcon(),
			FUIAction(
				FExecuteAction::CreateSP(this, &SMocapSourcePicker::ChangeMocapSourceProvider, FMath::Abs(ProviderIndex)),
				FCanExecuteAction()
			)
		);
	}
	return MenuBuilder.MakeWidget();
}

FText SMocapSourcePicker::OnGetButtonText() const
{
	UEnum* EnumPtr = nullptr;
	int index = UEnum::LookupEnumNameSlow(TEXT("VICON"), &EnumPtr);
	return GetProviderText(EnumPtr->GetNameByValue(MocapServer));
}

FText SMocapSourcePicker::OnGetTemplateButtonText() const
{
	if (TemplateName.IsEmpty())
	{
		return LOCTEXT("NoTemplate", "None  (IKINEMA default templates will be used");
	}
	return FText::FromString(TemplateName);
}

FText SMocapSourcePicker::GetProviderText(const FName& InName) const
{
	if (InName == "None")
	{
		return LOCTEXT("NoProviderDescription", "None  (Mocap streaming disabled)");
	}

	return FText::FromName(InName);
}

TSharedRef<SWidget> SMocapSourcePicker::OnGetTemplateMenuContent() const
{
	FMenuBuilder MenuBuilder(true, NULL);

	// Get a list of all templates
	auto Templates = FTemplate::LoadTemplates();
	int32 index = -1;

	FFormatNamedArguments Arguments;
	Arguments.Add(TEXT("TemplateName"), FText::FromString("None"));
	MenuBuilder.AddMenuEntry(
		FText::FromString("None"),
		FText::Format(LOCTEXT("Template_Tooltip", "Use {TemplateName} as a match template"), Arguments),
		FSlateIcon(),
		FUIAction(
			FExecuteAction::CreateSP(this, &SMocapSourcePicker::ChangeTemplateName, index),
			FCanExecuteAction()
		)
	);

	for (auto temp : Templates)
	{
		index++;
		const FText TemplateNameText = FText::FromString(temp.Key.ToString());

		FFormatNamedArguments TempArguments;
		TempArguments.Add(TEXT("TemplateName"), TemplateNameText);
		MenuBuilder.AddMenuEntry(
			TemplateNameText,
			FText::Format(LOCTEXT("Template_Tooltip", "Use {TemplateName} as a match template"), TempArguments),
			FSlateIcon(),
			FUIAction(
				FExecuteAction::CreateSP(this, &SMocapSourcePicker::ChangeTemplateName, index),
				FCanExecuteAction()
			)
		);
	}

	return MenuBuilder.MakeWidget();
}

void SMocapSourcePicker::SetMocapServer(EMocapServer Type)
{
	MocapServer = Type;
}
void SMocapSourcePicker::SetServerIP(const FString& _ServerIP)
{
	ServerAddressBox->SetText(FText::FromString(_ServerIP));
	ServerIP = _ServerIP;

}

void SMocapSourcePicker::SetTemplateName(const FString& _TemplateName)
{
	TemplateName = _TemplateName;
}

void SMocapSourcePicker::SetSubjectName(const FString& _SubjectName)
{
	SubjectNameBox->SetText(FText::FromString(_SubjectName));
	SubjectName = _SubjectName;
}
void SMocapSourcePicker::SetPortNumber(const int portNumber)
{
	TDefaultNumericTypeInterface<int> Conversion;
	if (portNumber != 0)
	{
		auto optional = Conversion.ToString(portNumber);

		ServerPortBox->SetText(FText::FromString(optional));
		PortNumber = portNumber;
	}
}
void SMocapSourcePicker::SetImportScale(const float _ImportScale)
{
	TDefaultNumericTypeInterface<float> Conversion;
	auto optional = Conversion.ToString(_ImportScale);
	ImportScaleBox->SetText(FText::FromString(optional));
	ImportScale = _ImportScale;
}

void SMocapSourcePicker::MakeStandardWidget()
{
	ChildSlot
		[
			SNew(SBorder)
			.BorderImage(FEditorStyle::GetBrush("DetailsView.CategoryTop"))
		.Padding(FMargin(0.0f, 3.0f, 1.0f, 0.0f))
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("SourceLabel", "Source"))
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(SComboButton)
			.OnGetMenuContent(this, &SMocapSourcePicker::OnGetMenuContent)
		.ContentPadding(1)
		.ToolTipText(LOCTEXT("ChooseProvider", "Choose the Mocap source provider you want to use."))
		.ButtonContent()
		[
			SNew(STextBlock)
			.Text(this, &SMocapSourcePicker::OnGetButtonText)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
		]
		]
	+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("TemplateLabel", "Matching Template"))
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(SComboButton)
			.OnGetMenuContent(this, &SMocapSourcePicker::OnGetTemplateMenuContent)
		.ContentPadding(1)
		.ToolTipText(LOCTEXT("ChooseTemplate", "Choose the matching template you want to use."))
		.ButtonContent()
		[
			SNew(STextBlock)
			.Text(this, &SMocapSourcePicker::OnGetTemplateButtonText)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
		]
		]
	+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[

			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.VAlign(VAlign_Center)
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("ServerAddressLabel", "Server Address"))
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SAssignNew(ServerAddressBox, SEditableTextBox)
			.ToolTipText(LOCTEXT("ServerAddressTextToolTip", "The IP address for the Mocap server streaming machine"))
		.OnTextCommitted(this, &SMocapSourcePicker::OnServerAddressCommitted)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
		]
	+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.VAlign(VAlign_Center)
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("ServerPortLabel", "Server Port"))
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(2.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SAssignNew(ServerPortBox, SEditableTextBox)
			.ToolTipText(LOCTEXT("ServerAddressTextToolTip", "The port number for the Mocap streaming server"))
		.Text(LOCTEXT("ServertPortVal", ""))
		.OnTextCommitted(this, &SMocapSourcePicker::OnServerPortCommitted)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
		]
	+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.VAlign(VAlign_Center)
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("SubjectLabel", "Subject Name"))
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SAssignNew(SubjectNameBox, SEditableTextBox)
			.ToolTipText(LOCTEXT("SubjectNameTextToolTip", "The requested subject name"))
		.OnTextCommitted(this, &SMocapSourcePicker::OnSubjectNameCommitted)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
		]
	+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.VAlign(VAlign_Center)
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("ImportScaleLabel", "Import Scale"))
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SAssignNew(ImportScaleBox, SEditableTextBox)
			.ToolTipText(LOCTEXT("ImportScaleTextToolTip", "The scaling factor to convert from Mocap server units to UE4 units"))
		.Text(LOCTEXT("ImportScaleValue", "1"))
		.OnTextCommitted(this, &SMocapSourcePicker::OnImportScaleCommitted)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
		]
	+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.VAlign(VAlign_Center)
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("YUpLabel", "Streaming Y-up"))
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(SCheckBox)
			.ToolTipText(LOCTEXT("YUpLabelTextToolTip", "Enable if your Mocap streaming server uses Y-up"))
		.OnCheckStateChanged(this, &SMocapSourcePicker::SetYup)
		]
		]
		]
	+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.VAlign(VAlign_Center)
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("ZeroRotationLabel", "Zero Source rotation"))
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		.IsEnabled(this, &SMocapSourcePicker::CanZeroRotations)
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(SCheckBox)
			.ToolTipText(LOCTEXT("ZeroRotationLabelTextToolTip", "Zero all source rotations on import"))
		.IsEnabled(this, &SMocapSourcePicker::CanZeroRotations)
		.OnCheckStateChanged(this, &SMocapSourcePicker::OnZeroRotations)
		]
		]
		]
	+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.VAlign(VAlign_Center)
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("RigidBodyLabel", "Import Rigid Bodies"))
		.Visibility(IsLicRB)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(SCheckBox)
			.ToolTipText(LOCTEXT("RigidBodyTextToolTip", "Enable if you are streaming RigidBodies"))
		.Visibility(IsLicRB)
		.OnCheckStateChanged(this, &SMocapSourcePicker::SetRigidBodies)
		]
		]
		]


		]
		];
}
void SMocapSourcePicker::MakeLiveLinkWeidget()
{
	ChildSlot
		[
			SNew(SBorder)
			.BorderImage(FEditorStyle::GetBrush("DetailsView.CategoryTop"))
		.Padding(FMargin(0.0f, 3.0f, 1.0f, 0.0f))
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("SourceLabel", "Source"))
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(SComboButton)
			.OnGetMenuContent(this, &SMocapSourcePicker::OnGetMenuContent)
		.ContentPadding(1)
		.ToolTipText(LOCTEXT("ChooseProvider", "Choose the Mocap source provider you want to use."))
		.ButtonContent()
		[
			SNew(STextBlock)
			.Text(this, &SMocapSourcePicker::OnGetButtonText)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
		]
		]
	+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("TemplateLabel", "Matching Template"))
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(SComboButton)
			.OnGetMenuContent(this, &SMocapSourcePicker::OnGetTemplateMenuContent)
		.ContentPadding(1)
		.ToolTipText(LOCTEXT("ChooseTemplate", "Choose the matching template you want to use."))
		.ButtonContent()
		[
			SNew(STextBlock)
			.Text(this, &SMocapSourcePicker::OnGetTemplateButtonText)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
		]
		]

	+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.VAlign(VAlign_Center)
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("SubjectLabel", "Subject Name"))
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SAssignNew(SubjectNameBox, SEditableTextBox)
			.ToolTipText(LOCTEXT("SubjectNameTextToolTip", "The requested subject name"))
		.OnTextCommitted(this, &SMocapSourcePicker::OnSubjectNameCommitted)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
		]
	+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.VAlign(VAlign_Center)
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("ImportScaleLabel", "Import Scale"))
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SAssignNew(ImportScaleBox, SEditableTextBox)
			.ToolTipText(LOCTEXT("ImportScaleTextToolTip", "The scaling factor to convert from Mocap server units to UE4 units"))
		.Text(LOCTEXT("ImportScaleValue", "1"))
		.OnTextCommitted(this, &SMocapSourcePicker::OnImportScaleCommitted)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
		]
	+ SVerticalBox::Slot()
		.VAlign(VAlign_Center)
		.FillHeight(1.0f)
		.Padding(2.0f)
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
		.VAlign(VAlign_Center)
		.FillWidth(1.0f)
		.Padding(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(LOCTEXT("RigidBodyLabel", "Import Rigid Bodies"))
		.Visibility(IsLicRB)
		.Font(FEditorStyle::GetFontStyle(TEXT("SourceControl.LoginWindow.Font")))
		]
		]
	+ SHorizontalBox::Slot()
		.FillWidth(2.0f)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
		.FillHeight(1.0f)
		.Padding(2.0f)
		.VAlign(VAlign_Center)
		[
			SNew(SCheckBox)
			.ToolTipText(LOCTEXT("RigidBodyTextToolTip", "Enable if you are streaming RigidBodies"))
		.Visibility(IsLicRB)
		.OnCheckStateChanged(this, &SMocapSourcePicker::SetRigidBodies)
		]
		]
		]



		]
		];

}

#undef LOCTEXT_NAMESPACE


