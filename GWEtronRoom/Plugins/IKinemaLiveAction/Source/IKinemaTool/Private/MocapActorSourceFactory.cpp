/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

/*=============================================================================
	Implementing factory for an IKinema solver definition
=============================================================================*/

#include "MocapActorSourceFactory.h"
#include "IKinemaRigToolModule.h"
#include "MocapActorSource.h"
#include "Factories.h"
#include "BusyCursor.h"



#define LOC_NAMESPACE TEXT("MocapActorSourceFactory")
#define LOCTEXT_NAMESPACE "MocapActorSourceFactory"



UMocapActorSourceFactory::UMocapActorSourceFactory(const class FObjectInitializer& PCIP)
	: Super(PCIP)
{
	// Property initialization
	SupportedClass = UActorSkeleton::StaticClass();
	bCreateNew = true;
	bEditAfterNew = false;
	bEditorImport = false;
	bText = false;
	ImportPriority = 1;

}

// Factory implementation.



UObject* UMocapActorSourceFactory::FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn)
{
	UActorSkeleton* NewRig = NewObject<UActorSkeleton>(InParent, Class, Name, Flags | RF_Transactional);
	return NewRig;
}

#undef LOCTEXT_NAMESPACE