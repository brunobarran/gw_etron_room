﻿
using System;
using System.IO;
using UnrealBuildTool;
using System.Security.Cryptography;
using System.Text;

public class XSens : ModuleRules
{

    private string ModulePath
    {
        get { return ModuleDirectory; }
    }

    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "")); }
    }

    public XSens(ReadOnlyTargetRules Target) : base(Target)
    {
		Type = ModuleType.External;

		if (Target.Platform == UnrealTargetPlatform.Mac )
		{
			return;
		}

		//
		// Start of the configuration section
		//

        
        string TargetPlatform = "";
        string XSensLibName = "xstypes";
        switch(Target.Platform)
        {
            case UnrealTargetPlatform.Win32:
                TargetPlatform = "Win32/";
                XSensLibName = XSensLibName + "32";
                break;
            case UnrealTargetPlatform.Win64:
                TargetPlatform = "x64/";
                XSensLibName = XSensLibName + "64";
                break;
        }
		// Path to the XSens  root directory
        string XSensRootDir = Path.Combine(ThirdPartyPath, TargetPlatform);
        Console.WriteLine(XSensRootDir);
		//
		// End of the configuration section
		//

		// Convert \ to / to avoid escaping problems
        XSensRootDir = XSensRootDir.Replace('\\', '/');
        string XSensIncludeDir = XSensRootDir + "include/";
        string XSensLibDir = XSensRootDir + "lib/";

		// XSens include paths
        PublicIncludePaths.Add(XSensIncludeDir);

        // XSens library path
        PublicLibraryPaths.Add(XSensLibDir);

        // statically link with XSens lib
        PublicAdditionalLibraries.Add(XSensLibName + ".lib");
        PublicAdditionalLibraries.Add("streaming_protocol.lib");
        PublicDefinitions.Add("XSTYPES_STATIC_LIB");
        //Add public delay load DLL
        //PublicDelayLoadDLLs.Add(XSensLibName + ".dll");
       // string FileName = XSensLibDir + XSensLibName + ".dll";
       // RuntimeDependencies.Add(FileName, StagedFileType.NonUFS);



    }
}
