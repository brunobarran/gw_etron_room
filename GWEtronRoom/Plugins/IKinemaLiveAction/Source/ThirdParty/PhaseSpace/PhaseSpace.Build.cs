﻿
using System;
using System.IO;
using UnrealBuildTool;
using System.Security.Cryptography;
using System.Text;

public class PhaseSpace : ModuleRules
{

    private string ModulePath
    {
        get { return ModuleDirectory; }
    }

    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "")); }
    }

    public PhaseSpace(ReadOnlyTargetRules Target) : base(Target)
    {
		Type = ModuleType.External;

		if (Target.Platform == UnrealTargetPlatform.Win32 ||
			Target.Platform == UnrealTargetPlatform.Mac )
		{
			return;
		}

		//
		// Start of the configuration section
		//

		// Path to the PhaseSpace root directory
        string PhaseSpaceRootDir = Path.Combine(ThirdPartyPath, "PSClientSDK/");
		Console.WriteLine(PhaseSpaceRootDir);
		//
		// End of the configuration section
		//

		// Convert \ to / to avoid escaping problems
		PhaseSpaceRootDir = PhaseSpaceRootDir.Replace('\\', '/');

		string PhaseSpaceLibDir = PhaseSpaceRootDir + "lib/";


		// PhaseSpace library name.
		string PhaseSpaceLibName = "libowlsock";//.lib";


		// PhaseSpace defines
		PublicDefinitions.Add("WITH_PHASESPACE_LIBS=1");
		//Definitions.Add("_CRT_SECURE_NO_WARNINGS=1");
		PublicDefinitions.Add("PHASESPACE");

		// PhaseSpace include paths
		PublicIncludePaths.Add(PhaseSpaceRootDir + "include");

		// PhaseSpace library path
		PublicLibraryPaths.Add(PhaseSpaceLibDir);

		// statically link with PhaseSpace lib
		PublicAdditionalLibraries.Add(PhaseSpaceLibName+".lib");
		
		//Add public delay load DLL
        PublicDelayLoadDLLs.Add(PhaseSpaceLibName + ".dll");

        string FileName = PhaseSpaceLibDir + PhaseSpaceLibName + ".dll";
        RuntimeDependencies.Add(FileName, StagedFileType.NonUFS);
    }
}
