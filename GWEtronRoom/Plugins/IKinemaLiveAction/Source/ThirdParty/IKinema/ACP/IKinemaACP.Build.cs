﻿
using System;
using System.IO;
using UnrealBuildTool;
using System.Security.Cryptography;
using System.Text;

public class IKinemaACP : ModuleRules
{

    private string ModulePath
    {
        get { return ModuleDirectory; }
    }

    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "")); }
    }

    public IKinemaACP(ReadOnlyTargetRules Target) : base(Target)
    {
		Type = ModuleType.External;
       
		if (Target.Platform == UnrealTargetPlatform.Win32 )
		{
			return;
		}

		// Path to the IKinemaACP root directory
		string IKinemaACPRootDir = ThirdPartyPath + '/';
		
    
		//
		// End of the configuration section
		//

		// Convert \ to / to avoid escaping problems
		IKinemaACPRootDir = IKinemaACPRootDir.Replace('\\', '/');

		string IKinemaACPLibDir = IKinemaACPRootDir + "sdk/lib/";
        string IKinemaACPIncludeDir = IKinemaACPRootDir;
		string IKinemaACPPlatform = "";
		string UEPlatform = "";
        string IKinemaACPLibExtension = "";
		        if (Target.Platform == UnrealTargetPlatform.Win64)
		        {
		            IKinemaACPPlatform = "Win64";
		            UEPlatform = "Win64";
		            IKinemaACPLibExtension = ".lib";
		            IKinemaACPIncludeDir += "sdk/include";
		        }
		    

		// Final IKinemaACP lib path
		IKinemaACPLibDir += IKinemaACPPlatform;

		// IKinemaACP library name.
		string IKinemaACPLibName = "ACPLib";
		IKinemaACPLibName += IKinemaACPLibExtension;
       
		// IKinemaACP defines
		PublicDefinitions.Add("WITH_IKinemaACP_LIBS=1");
        PublicDefinitions.Add("FIK_SSE=1");
        PublicDefinitions.Add("MS_IKinemaACP_" + UEPlatform);

		// IKinemaACP include paths
        PublicIncludePaths.Add(IKinemaACPIncludeDir);
        
		// IKinemaACP library path
		PublicLibraryPaths.Add(IKinemaACPLibDir);
       
	   // statically link with IKinemaACP lib
        if (Target.Platform == UnrealTargetPlatform.IOS || Target.Platform == UnrealTargetPlatform.Android)
        {
            IKinemaACPLibName = Path.Combine(IKinemaACPLibDir, "lib" + IKinemaACPLibName);
            PublicAdditionalShadowFiles.Add(IKinemaACPLibName);
        }
        PublicAdditionalLibraries.Add(IKinemaACPLibName);

		string engDir = Directory.GetParent(Directory.GetCurrentDirectory()).FullName; //Get base directory compatible with System.IO
        string IKinemaACPBinariesDir = String.Format("$(EngineDir)/Binaries/ThirdParty/IKinema/License/");
        string IKinemaACPLicenseDir = IKinemaACPRootDir + "sdk/license";
        if (File.Exists(engDir+"/Binaries/ThirdParty/IKinema/License/a0.lic"))
        {
            RuntimeDependencies.Add(IKinemaACPBinariesDir + ("a0.lic"));
        }
        RuntimeDependencies.Add(IKinemaACPBinariesDir + ("ikey"));

        PublicDelayLoadDLLs.Add("ACPLib.dll");
        {
            string FileName = IKinemaACPLibDir + "/ACPLib.dll";
            RuntimeDependencies.Add(FileName, StagedFileType.NonUFS);
            
        }
        Console.WriteLine(IKinemaACPBinariesDir);
        Console.WriteLine(IKinemaACPLicenseDir);


    }
}
