/* Copyright 2009-2017 IKinema, Ltd. All Rights Reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema RunTime project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "IKMath.h"
#include "IKString.h"
#include <vector>

#ifdef ACP_DLL
#define DLL_EXPORT_ACP __declspec( dllexport )
#else
#ifndef ACP_STATIC
#define DLL_EXPORT_ACP __declspec( dllimport )
#else
#define DLL_EXPORT_ACP
#endif
#endif


namespace FIK
{
	struct DLL_EXPORT_ACP Bone
	{

		Bone();

		Bone(const char* name, const char* parent_name, const Vector& rest_translation, const Quaternion& rest_rotation);
		//name
		IKString Name;
		//parent name
		IKString ParentName;
		// rest translation
		Vector RestTranslation;
		// rest rotation
		Quaternion RestRotation;
	};

	struct DLL_EXPORT_ACP IKBone : Bone
	{

		//Most important IKinema bone attributes that we need for ACP
		bool	Active;

		bool	DofX;
		bool	DofY;
		bool	DofZ;

		bool	StretchX;
		bool	StretchY;
		bool	StretchZ;

		// One weight value for each bone axis.
		Vector	Weight;
		float	MaxVelocity;

		float RetargetingGain;

		IKBone();

		IKBone(const char* name, const char* parent_name, const Vector& rest_translation, const Quaternion& rest_rotation);

	};

	inline Bone::Bone()
	{
	}

	inline Bone::Bone(const char* name, const char* parent_name, const Vector& rest_translation, const Quaternion& rest_rotation)
		: Name(name), ParentName(parent_name), RestTranslation(rest_translation), RestRotation(rest_rotation)
	{
	}



	inline IKBone::IKBone() : Active(true), DofX(true), DofY(true), DofZ(true), StretchX(false), StretchY(false), StretchZ(false), Weight(1.f, 1.f, 1.f), MaxVelocity(0.03f), RetargetingGain(1.0f)
	{
	}

	inline IKBone::IKBone(const char* name, const char* parent_name, const Vector& rest_translation, const Quaternion& rest_rotation)
		: Bone(name, parent_name, rest_translation, rest_rotation), Active(true), DofX(true), DofY(true), DofZ(true), StretchX(false), StretchY(false), StretchZ(false), Weight(1.f, 1.f, 1.f), MaxVelocity(0.03f), RetargetingGain(1.0f)
	{
	}

}