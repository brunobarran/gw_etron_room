#pragma once
#include <IKMath.h>

namespace FIK
{
	namespace ACP
	{
		template <typename T>
		class Filter
		{
		public:
			Filter(): mAlpha(0), mBeta(0), mTau(0), mFreq(0)
			{
			};
			Filter(const T& initStat, float freq, float tau);;
			~Filter(void) {};
			
			T filter(T& dataPoint);

			void UpdateSmoothness(const float& tau);
		private:
			//Smoothing statistics of the Filters
			T mSp1;
			T mSp2;
			//Exponential decay rate of the y-intercept and slope
			float mAlpha;
			float mBeta; // beta = alpha*tau/(1-alpha). Stored here to avoid doing extra calculations
						 //Filter time step
			float mTau;
			//data sampling InFilterAlpha
			float mFreq;
		};

		template <typename T>
		Filter<T>::Filter(const T& initStat, float freq, float tau) : mSp1(initStat), mSp2(initStat), mTau(tau), mFreq(freq)
		{
			float dT = 1.f / mFreq; //Sampling period
			mAlpha = mTau;// exp(-T / mTau);
			mBeta = mAlpha * dT / (1 - mAlpha);
		}

		template <typename T>
		T Filter<T>::filter(T& dataPoint)
		{
			float oneMinAlpha = (1 - mAlpha);
			//Update statistics
			mSp1 = dataPoint * mAlpha + mSp1 * oneMinAlpha;
			mSp2 = mSp1 * mAlpha + mSp2 * oneMinAlpha;

			//Predictor function
			T ret = mSp1 * (2 + mBeta) - mSp2 * (1 + mBeta);
			return ret;
		}

		template <typename T>
		void Filter<T>::UpdateSmoothness(const float& tau)
		{
			mTau = tau;
			float dT = 1.f / mFreq; //Sampling period
			mAlpha = mTau;// exp(-T / mTau);
			mBeta = mAlpha * dT / (1 - mAlpha);
		}
		//Specialisation for Quaternion
		template<>
		Quaternion Filter<Quaternion>::filter(Quaternion& dataPoint);
		
	}
}
