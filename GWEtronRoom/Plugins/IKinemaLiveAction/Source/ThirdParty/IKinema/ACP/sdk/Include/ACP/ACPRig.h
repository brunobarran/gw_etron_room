/* Copyright 2009-2017 IKinema, Ltd. All Rights Reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema RunTime project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "IKBone.h"
#include "IKString.h"
#include <vector>

#ifdef ACP_DLL
#define DLL_EXPORT_ACP __declspec( dllexport )
#else
#ifndef ACP_STATIC
#define DLL_EXPORT_ACP __declspec( dllimport )
#else
#define DLL_EXPORT_ACP
#endif
#endif

//TODO: node construction from ACPRig&& and ACPRig&

namespace FIK {
	class IKSolver;
}

namespace FIK
{

	namespace ACP
	{

		//static Bone Empty;

		class DLL_EXPORT_ACP ACPRig
		{
		public:
			using BoneType = IKBone;
			ACPRig();
			~ACPRig();
			explicit ACPRig(IKSolver& Solver);

			void AddBone(IKBone& InBone);

			const IKBone& GetBone(IKString& BoneName) const;

			const IKBone& GetBone(int index) const;

			int GetBoneIndex(IKString& BoneName) const;

			int GetParentIndex(int index) const;
		private:
			std::vector<BoneType> RefSkeleton;
		public:
			auto begin() -> decltype(RefSkeleton.begin()) { return RefSkeleton.begin(); };
			auto end() -> decltype(RefSkeleton.end()) { return RefSkeleton.end(); };

			auto begin() const -> decltype(RefSkeleton.begin()) { return RefSkeleton.begin(); };
			auto end() const -> decltype(RefSkeleton.end()) { return RefSkeleton.end(); };
		
		};
	}
}
