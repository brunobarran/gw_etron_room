#pragma once

#include "AnimNode.h"
#include "ACPRig.h"

namespace FIK
{
	namespace ACP
	{

		class DLL_EXPORT_ACP FilterNode :
			public AnimNode<ACPRig>
		{
		public:

			/*
			* FeetSliding Node Factory
			* Assumes license is located in the current working directory
			*/
			static FilterNode* Create(const ACPRig& Rig, float InFrequency);

			/*
			* Constructor assumingthe license file is in the current application directory
			* The Sliding Tasks are initialised during construction
			*/
			static FilterNode* Create(const ACPRig& Rig, float InFrequency, const char* licensePath, char** errorMessage);

			static FilterNode* Create(const ACPRig& Rig);
			
			virtual ~FilterNode();

			//virtual AutoTune::Enum Solve(Transform* Pose, size_t size);

			virtual float GetFilterAlpha() const = 0;

			virtual void SetFilterAlpha(const float InFilterAlpha) = 0;
		protected:
			FilterNode(const ACPRig& Rig, float InFrequency);
			FilterNode(const ACPRig& Rig, float InFrequency, const char* licensePath, char** errorMessage);
			explicit FilterNode(const ACPRig& Rig);
		};
	}
}

