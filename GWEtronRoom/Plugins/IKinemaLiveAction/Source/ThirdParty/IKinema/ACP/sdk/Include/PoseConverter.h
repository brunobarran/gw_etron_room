/* Copyright 2009-2017 IKinema, Ltd. All Rights Reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema RunTime project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include <vector>
#include <IKMath.h>

namespace FIK 
{
	
		template<typename Rig>
		class PoseConverter
		{
		public:
			PoseConverter(const Rig& InRig);
			PoseConverter(Rig& InRig, std::vector<FIK::Transform>& LocalPose);
			PoseConverter(const Rig& InRig, std::vector<FIK::Transform>& LocalPose);
			PoseConverter(const Rig& InRig, FIK::Transform const* LocalPose, size_t Size);
			

			~PoseConverter();

			
			const FIK::Transform& GetGlobalTransform(size_t Index);
		private:
			const Rig& mRig;
			std::vector<FIK::Transform> Pose;
			std::vector<int> GlobalFlag;
			void CalculateGlobalTransform(size_t index);
		};

		template <typename Rig>
		PoseConverter<Rig>::PoseConverter(const Rig& InRig)
			:mRig(InRig)
		{
			for (auto& b : mRig)
			{
				FIK::Transform xForm(b.RestRotation, b.RestTranslation);
				Pose.push_back(xForm);
			}
			GlobalFlag.resize(Pose.size(), 0);
			GlobalFlag[0] = 1;
		}

		template <typename Rig>
		PoseConverter<Rig>::PoseConverter(Rig& InRig, std::vector<FIK::Transform>& LocalPose)
			:mRig(InRig), Pose(LocalPose), GlobalFlag(LocalPose.size(), 0)
		{
			GlobalFlag[0] = 1; // Root is always in global space for us
		}

		template <typename Rig>
		PoseConverter<Rig>::PoseConverter(const Rig& InRig, std::vector<FIK::Transform>& LocalPose)
			:mRig(InRig), Pose(LocalPose), GlobalFlag(LocalPose.size(), 0)
		{
			GlobalFlag[0] = 1; // Root is always in global space for us
		}

		template <typename Rig>
		PoseConverter<Rig>::PoseConverter(const Rig& InRig, FIK::Transform const* LocalPose, size_t Size)
			: mRig(InRig), Pose(LocalPose, LocalPose + Size), GlobalFlag(Size, 0)
		{
			GlobalFlag[0] = 1; // Root is always in global space for us
		}

		template <typename Rig>
		PoseConverter<Rig>::~PoseConverter()
		{
		}

		template <typename Rig>
		void PoseConverter<Rig>::CalculateGlobalTransform(size_t index)
		{
			auto ParentIndex = mRig.GetParentIndex(index);

			if (ParentIndex < GlobalFlag.size() && GlobalFlag[ParentIndex] == 0)
			{
				CalculateGlobalTransform(ParentIndex);
			}

			auto& Bone = Pose[index];
			auto& ParentBone = Pose[ParentIndex];

			Pose[index] = ParentBone * Bone;
			GlobalFlag[index] = 1;
		}

		template <typename Rig>
		const FIK::Transform& PoseConverter<Rig>::GetGlobalTransform(size_t Index)
		{
			if (GlobalFlag[Index] == 0)
			{
				CalculateGlobalTransform(Index);
			}
			return Pose[Index];
		}
}



