/* Copyright 2009-2017 IKinema, Ltd. All Rights Reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema RunTime project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "IKSolver.h"
#include "IKString.h"
#include "span.h"

#ifdef ACP_DLL
#define DLL_EXPORT_ACP __declspec( dllexport )
#else
#ifndef ACP_STATIC
#define DLL_EXPORT_ACP __declspec( dllimport )
#else
#define DLL_EXPORT_ACP
#endif
#endif


namespace FIK
{
		/*
		 * ACP Node base class.
		 * All ACP nodes inherits from this node.
		 */
		template <typename T>
		class DLL_EXPORT_ACP AnimNode
		{
			using BoneType = typename T::BoneType;
		public:
			virtual ~AnimNode();


			static void operator delete(void* ptr, std::size_t sz);

			static void operator delete[](void* ptr, std::size_t sz);

		protected:
			int NumOfIterations;
			float FrameRate;    
			IKSolver* Solver;
			const T  Rig;
			
			/*
			 * Pass the local FK transforms fromthe input to the solver as retargeting input
			 * input an std::vector<FIK::Transform>, with the local transforms
			 */
			void PassFKPose(const FIK::Transform* Pose, size_t Size);

			/*
			* Pass the local FK transforms from the solver to the std::vector
			* output an std::vector<FIK::Transform>, with the local transforms
			* return the AutoTune result of the FIK::IKSolver::Solve
			*/
			AutoTune::Enum UpdateAndSolvePose(FIK::Transform* Pose, size_t Size);

		public:
			
			/*
			 * AnimNode constructor. This assumes the license file is located in 
			 * the current directory of the application
			 */
			explicit AnimNode(const T& InRig);

			
			/*
			 * AnimNode constructor. The license path and the error message is 
			 * passed in as licensePath and errorMessage
			 * ACPRig, skeleton hierarchy and reference pose description
			 * if licensePath == empty string, the license file is allocated to the current directory
			 * errorMessage the returned errorMessage from the license manager
			 */
			AnimNode(const T& InRig, const char* licensePath, char** errorMessage);

			//TODO: Hide the std::vector, pass as array and size??
			/*
			 * Am abstract function to be implemented by the different ACP nodes
			 * input/output an std::vector<FIK::Transform>, with the local transforms
			 * of the skeleton. The order and length of the vector has to match the order and length ofthe ACPRig
			 */
			virtual AutoTune::Enum Solve(IKinema::span<Transform> InPose) = 0;


			
			/*
			 * Remove the task attached to the bone TaskName. 
			 * Abstract function to leave details of implementation to the specific AnimNode
			 */
			virtual void RemoveTask(const IKString& TaskName) = 0;

			/*
			 * Utility to reset the Solver
			 */
			void ResetNode();

			/*
			 * Utility to get the data frame rate
			 */
			float GetFrameRate() const;

			/*
			 * Utility to get the Node Solver number of Iterations
			 */
			const int& GetNumOfIterations() const;

			void SetPCoefficient(float pGain);

			/*
			 * Utility to set the Node Solver number of Iterations
			 */
			void SetNumOfIterations(const int num_of_iterations);

			/*
			 * Utility to set the data frame rate
			 */
			void SetFrameRate(const float frame_rate);

			const T& GetRig() const { return Rig; };

		private:

			/*
			 * Update the IKSegment settings with those taken from the ACPRig Bone object
			 * p_seg can not be nullptr, and Bone is from the ACPRig
			 */
			static void UpdateSolverBoneSettings(IKSegment* pSeg, const BoneType& bone);
		};
	
}


