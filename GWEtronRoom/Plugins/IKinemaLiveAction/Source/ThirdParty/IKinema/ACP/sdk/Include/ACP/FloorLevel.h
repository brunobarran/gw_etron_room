/* Copyright 2009-2017 IKinema, Ltd. All Rights Reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema RunTime project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include "AnimNode.h"
#include "ACPRig.h"

namespace FIK
{
	namespace ACP
	{

		class DLL_EXPORT_ACP FloorLevel :
			public AnimNode<ACPRig>
		{
		public:
			//TODO HipsOffset
			struct DLL_EXPORT_ACP FloorTask
			{
				IKString BoneName;
				float MeshOffset;
				bool bIsOffsetRoot;
				Vector PlaneNormal;
				Vector PlanePosition;

				explicit FloorTask(const IKString& InBoneName);
				FloorTask(const FloorTask& other);
				FloorTask(FloorTask&& other);
				FloorTask& operator=(const FloorTask& other);
				FloorTask& operator=(FloorTask&& other);
			};

			virtual void AddTask(const FloorTask& InTask) = 0;

			/*
			* FloorLevel Node constructor
			* Assumes license is located in the current working directory
			*/

			static FloorLevel* Create(const ACPRig& InRig);

			/*
			* Constructor assumingthe license file is in the current application directory
			* The Floor Tasks are initialised during construction
			*/
			static FloorLevel* Create(const ACPRig& InRig, const FloorTask* InTasks, size_t Size);


			/*
			* Constructor accepting license file and returning an error message when licensing fails
			*/
			static FloorLevel* Create(const ACPRig& InRig, const char* licensePath, char** errorMessage);

			/*
			* Constructor accepting license file and returning an error message when licensing fails
			* The Sliding Tasks are initialised during construction
			*/
			static FloorLevel* Create(const ACPRig& InRig, const char* licensePath, char** errorMessage, const FloorTask* InTasks, size_t Size);

			virtual void SetTaskMeshOffset(const IKString& TaskName, float MeshOffset) = 0;
			virtual void SetTaskPlaneNormal(const IKString& TaskName, const Vector& PlaneNormal) = 0;
			virtual void SetTaskPlanePosition(const IKString& TaskName, const Vector& PlanePosition) = 0;

			virtual ~FloorLevel();


		protected:
			/*
			* FloorLevel Node constructor
			* Assumes license is located in the current working directory
			*/

			explicit FloorLevel(const ACPRig& InRig);


			/*
			* Constructor accepting license file and returning an error message when licensing fails
			*/
			FloorLevel(const ACPRig& InRig, const char* licensePath, char** errorMessage);
			
		};
	}
}
