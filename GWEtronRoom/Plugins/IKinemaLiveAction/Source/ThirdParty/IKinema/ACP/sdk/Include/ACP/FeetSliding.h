/* Copyright 2009-2017 IKinema, Ltd. All Rights Reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema RunTime project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "AnimNode.h"
#include "ACPRig.h"

namespace FIK
{
	namespace ACP
	{
		class DLL_EXPORT_ACP FeetSliding :
			public AnimNode<ACPRig>
		{
		public:
			struct DLL_EXPORT_ACP SlidingTask
			{
				
				IKString BoneName;
				explicit SlidingTask(const IKString& InBoneName);
				SlidingTask(const SlidingTask& other);
				SlidingTask(SlidingTask&& other);
				SlidingTask& operator=(const SlidingTask& other);
				SlidingTask& operator=(SlidingTask&& other);

				bool bIsLocking;
				float MinVelThresh;
				float MaxVelThresh;
				int BlendFrames;
			};
	
			/*
			* FeetSliding Node constructor
			* Assumes license is located in the current working directory
			*/
			static FeetSliding* Create(const ACPRig& InRig);

			/*
			* Constructor assumingthe license file is in the current application directory
			* The Sliding Tasks are initialised during construction
			*/
			static FeetSliding* Create(const ACPRig& InRig, const SlidingTask* InTasks, size_t Size);

			/*
			* Constructor accepting license file and returning an error message when licensing fails
			*/
			static FeetSliding* Create(const ACPRig& InRig, const char* licensePath, char** errorMessage);

			/*
			* Constructor accepting license file and returning an error message when licensing fails
			* The Sliding Tasks are initialised during construction
			*/
			static FeetSliding* Create(const ACPRig& InRig, const char* licensePath, char** errorMessage, const SlidingTask* InTasks, size_t Size);
		
			/*
			 * Add a slidding task to the node
			 */
			virtual void AddTask(SlidingTask& InTask) = 0;


			
			virtual void SetTaskThresholds(IKString TaskName, float Min, float Max) = 0;
			virtual void SetTaskIsLocking(IKString TaskName, bool bIsLocking) = 0;
			virtual void SetTaskBlendFrames(IKString TaskName, int InBlendFrames) = 0;
			
			
			virtual ~FeetSliding();
		protected:
			/*
			* FeetSliding Node constructor
			* Assumes license is located in the current working directory
			*/
			explicit FeetSliding(const ACPRig& InRig);

			/*
			* Constructor assumingthe license file is in the current application directory
			* The Sliding Tasks are initialised during construction
			*/
			FeetSliding(const ACPRig& InRig, const SlidingTask* InTasks, size_t Size);

			/*
			* Constructor accepting license file and returning an error message when licensing fails
			*/
			FeetSliding(const ACPRig& InRig, const char* licensePath, char** errorMessage);

			/*
			* Constructor accepting license file and returning an error message when licensing fails
			* The Sliding Tasks are initialised during construction
			*/
			FeetSliding(const ACPRig& InRig, const char* licensePath, char** errorMessage, const SlidingTask* InTasks, size_t Size);
		
		};

	}
}
