/* Copyright (C) 2006-2011, IKinema Ltd. All rights reserved.
 *
 * IKinema API Library; SDK distribution
 *
 * This file is part of IKinema engine http://www.ikinema.com
 *
 * Your use and or redistribution of this software in source and / or binary form, with or without
 * modification, is subject to:
 * (i) your ongoing acceptance of and compliance with the terms and conditions of
 * the IKinema License Agreement; and
 *
 * (ii) your inclusion of this notice in any version of this software that you use
 * or redistribute.
 *
 *
 * A copy of the IKinema License Agreement is available by contacting
 * IKinema Ltd., http://www.ikinema.com, support@ikinema.com
 *
 */

#ifndef FIKSTRING_HEADER_INCLUDED
#define FIKSTRING_HEADER_INCLUDED

class IKString
{
public:
	IKString();
	IKString(const char *pStr);
	IKString(const IKString &ikStr);
	~IKString();

	IKString& operator=(const IKString& str);
	IKString& operator=(const char *pStr);
	bool operator==(const char *pStr) const;
	bool operator==(const IKString& str) const;
	const char* GetCharPtr()const { return m_pStr; }
	const char* c_str() const { return GetCharPtr(); }
	bool Empty();
	void SetString(const char *pStr);
private:
	char m_pStr[128];
	size_t m_uiBuffSize;
};

#include "IKString.inl"

#endif //FIK_STRING_HEADER_INCLUDED
