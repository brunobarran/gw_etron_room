/* Copyright (C) 2006-2011, IKinema Ltd. All rights reserved.
 *
 * IKinema API Library; SDK distribution
 * 
 * This file is part of IKinema engine http://www.ikinema.com
 *
 * Your use and or redistribution of this software in source and / or binary form, with or without
 * modification, is subject to: 
 * (i) your ongoing acceptance of and compliance with the terms and conditions of
 * the IKinema License Agreement; and 
 *
 * (ii) your inclusion of this notice in any version of this software that you use 
 * or redistribute.
 *
 *
 * A copy of the IKinema License Agreement is available by contacting
 * IKinema Ltd., http://www.ikinema.com, support@ikinema.com
 *
 */

#ifndef IKSEGMENT_HEADER_INCLUDED
#define IKSEGMENT_HEADER_INCLUDED

#include "IKMemory.h"
#include "IKMath.h"
#include "IKShape.h"

#ifdef BUILD_DLL
#if defined( __WIN32__ ) || defined( _WIN32 )
#define DLL_EXPORT __declspec( dllexport )
#else
#pragma GCC visibility push(default)
#define DLL_EXPORT
#endif
#else
#define DLL_EXPORT
#endif

namespace FIK {
	
	#pragma pack(push, 1)
	/** 
		\struct EulerLimits

		\brief Structure describing the rotational limits in terms of Max/Min Euler rotations.

		max/min Euler rotations (rotations around local bone axes in rest (or current) orientation in degrees
		values exceeding -359/359 indicate full range for bone rotation; normally min should be negative max should be positive;
		The limits could be same sign, indicating that the rest (or current) orientation is off-limits.

	*/

	struct EulerLimits
	{
		/** Array to hold the min max limits in degrees for each axis in the following format
			\n rot[<b>axis</b>][<b>limit</b>]
			
			axis can be 
				- 0 = x
				- 1 = y
				- 2 = z

			limit 
				- 0 = Min
				- 1 = Max
		*/
		float rot[3][2]; 
		/** local axis along  bone || local axis nearest to axis along bone || axis with least expected rotation ("roll") */
		unsigned int bone_axis; // 0 || 1 || 2
	};
	#pragma pack(pop)
	
	/**
		\class IKSegment

		\brief Class defining a Segment/Bone of the imported Skeleton into the solver

		This class will hold the Segment properties such as retargeting gains, limit values, stiffness ..etc.
		It is also used to perform the forward kinematics on the Skeleton to get to the final pose.

	*/
	class DLL_EXPORT IKSegment
	{
		public:
			/** Default destructor */
			virtual ~IKSegment(){}

			/*********************FOR ANIMATION/RETARGETING (CAN BE CHANGED EVERY FRAME)*******************/
								/////////////////INPUT///////////////////	
			/** @name Input/Output Methods
			  
			    These methods can be called at every frame. They are used to change the segment settigns in real-time.
			*/
			///@{

			/** Enables animation/mocap input to the solution for this bone.
			
				\param flag	Flag that enables/disables animation or mocap input to solution.
				\param switchToLimits This flag is used if first parameter is equal to "false". If this parameter 
									  is set to "true" it will switch the segment back to limits by default.
			
				\remark Enabling retargeting will disable shadowpose and sets the retargeting gain to the default value of 1

			*/
			virtual void enableRetargeting(bool flag,bool switchToLimits=true) = 0;
			
			/** Check if Solving for animation targets is enabled for this segment. */
			virtual bool haveRetargeting() = 0;

			/** 
				Enable/Disable ShadowPosing and tunes the segment retargeting gain.
				\param flag True to enable shadowPosing 
			*/
			virtual void enableShadowPosing(bool flag) = 0;

			/**	Check if shadowPosing is enabled */
			virtual bool haveShadowPosing() const = 0;

			/** Enables joint limits  for this bone.
				
				\param flag Flag that enables/disables joint limits input to solution.

			*/
			virtual void enableLimits(bool flag) = 0;

			/**	Returns if the segment has limits. */
			virtual bool haveLimits() = 0;

			/**
				Enables or disables enforced limits.

				Enforcing limits will recompute optimum orientation each time a segment violates the limits.
				Mainly, used for joints with asymmetrical limits.

				\param flag true to enforce optimum limit computation. False to correct based on rest pose. 
							Sufficient for joints with symmetrical limits.

			*/
			
			virtual void enforceLimits(bool flag) = 0;

			/**	Returns if enforced limits are enabled.*/
			virtual bool getEnforceLimits() const = 0;

			/**
				Enables stretch limits input to the solution for this bone.
				
				\param flag Flag that enables/disables stretch limits input to solution.			

			*/
			virtual void enableStretchLimits(bool flag) = 0;

			/**	
				Checks if Stretch limits are enabled for this segment.

				\remark Always returns false for the root segment.
			*/
			virtual bool getEnableStretchLimits()=0;

			/**
				Enables or disables enforced limits.
				
				\param flag True when stretching must never go beyond the limits.
							false to apply a constant pull towards the rest position.

			*/
				
			virtual void enforceStretchLimits(bool flag) = 0;

			/**	Returns if the segment has enforced stretch limits.*/
			virtual bool getEnforceStretchLimits()=0;

			/** removes any accumulated stretch, optionally switches off all stretch
			
				\param flag is set to "true" if stretch dofs (3-5) are to be disabled after reset
							Set this flag to "false" to keep the stretch enabled
			*/
			virtual void resetStretch(bool flag=false)=0;

			/** 
				\brief Enable/Disable specific degrees of freedom in the segment.

				This method can be used to set up custom hinge, swing, twist, elbow and null joints by 
				removing some or all dofs from solving.
	
				\param x,y,z New degrees of freedom for this bone.
				\param rotation Flag to indicate whether the rotational or the translational (stretching) dofs are meant
			*/
			virtual void setActiveDofs(bool x, bool y, bool z, bool rotation=true) = 0;

			/** Returns if this is a translational segment (covers both modes: translating (in parent space) and stretching (local space)) */
			virtual bool Translational() const = 0;

			/**
				Allows the joint to translate in its parent space.
			
				\param flag enable/disable translating mode
				\param reset_dofs if true,this will enable/disable all three translational dofs
								  if false,relevant translation dofs should be set with setActiveDofs(x,y,z, rotation=false)
			*/
			virtual void setTranslating(bool flag,bool reset_dofs=true) = 0;

			/** Returns if this is a translating segment? (covers one mode: translating in parent space)*/
			virtual bool getTranslating()const = 0;
			
			/**	
				Enable or disable updates of this segment.

				\param value Flag that enables/disables segment update.
			    \param resetDofs If this flag is set to "true" all segment dofs will be set to the same value.
								 Otherwise if the flag is "false", previously set dof flags will be kept (useful
								 if a segment is frequently switched off/on).
			*/
			virtual void setActive(bool value,bool resetDofs=true) = 0;

			/**	This method is used when we have retargeting from mocap or animation to set desired orientation for this bone.
				
				\param x,y,z,w:	The desired orientation for this bone.
			*/
			virtual void setTargetOrientation(Real x, Real y, Real z, Real w) = 0;

			/**	Returns desired orientation that was passed from mocap or animation. */
			virtual const Real* getTargetOrientation() = 0;
			
			/**	
				Updates the segment if the source bone has been modified bypassing the solver.
			*/
			virtual void setOrientation(Real x, Real y, Real z, Real w) = 0;

			/**
				Sets  translation relative the parent (m_start). Called normally  to update the solver
				about changing root bone translation.
				\param x,y,z New bone relative translation.
			*/
			virtual void setTranslation(Real x, Real y, Real z) = 0;

			/**	
				Translates segment in parent space by delta vector.
				\param x,y,z:	New bone delta translation.
			*/
			virtual void translateSegment(Real x, Real y, Real z) = 0;

			/**
				Set joint weights (per axis): this is "mobility" of the bone.
			
				\remark The Weight value is (1-stiffness). Setting a weight of 0 will disable the dof.
						It is better to remove the dof/segment from solving if weight is zero.

				\param axis This parameter represent the desired axis. Valid values are 0 for x, 1 for y, 2 for z,
							 3 for x translation, 4 for y translation, 5 for z translation
				\param weight Set 0 if there's no mobility (frozen). Value of 1 represents normal mobility. If the parameter
								is greater than 1 then the bone will have extra mobility. 
			*/
			virtual void setWeight(int axis, Real weight) = 0;
			//@}
								/////////////////OUTPUT///////////////////////

			/** @name Output Pose
			  
			    This section describes the methods used to get the solved pose and pass it to the rest of the Animation pipeline.
				\remark returned pointers can be stored as long as the segment is not destroyed.
			*/
			///@{
			
			/** Get current local orientation

				\returns FIK::Real pointer to Local orientation after solving, format is the same as FIK::Quaternion.
				
			
			*/
			virtual const Real* getQ()const = 0;

			/** Get the local delta rotation from rest pose.

				Returns the local delta rotation from the rest pose.
			
				\returns FIK::Real pointer to local orientation with rest orientation, the quaterion in the same order as FIK::Quaternion
			*/
			
			virtual const Real* getDeltaQ()const = 0;

			/** Get local translation after the solved. 
				
				\returns pointer to local translation vector in FIK::Vector format.

			*/

			virtual const Real* getPos()const = 0;
			//@}
			/**********************************SKELETON SETUP/ACCESS*************************************/
			/** @name Skeleton Setup
			  
			    Setup of the Imported skeleton hierarchy and rest pose.
			*/
			///@{


			/**	
				\brief Sets the parent of this segment.
				
				Changes the parent of the IKSegment, and removes it from the children list of its current parent

				\param parent Parent segment for this bone.
			*/
			virtual void setParent(IKSegment *parent) = 0;

			/** \brief Returns pointer to child segment of this bone.*/
			virtual IKSegment *Child() const = 0;

			/** \brief Returns pointer to sibling segment of this bone.*/
			virtual IKSegment *Sibling() const = 0;

			/** \brief Returns pointer to parent segment of this bone. NULL if this IKSegment is the root of the skeleton */
			virtual IKSegment *Parent() const = 0;

			/** \brief Sets rest orientation relative the parent.
			
				\param x,y,z,w New rest orientation for this segment relative to parent.

			*/
			virtual void setRestOrientation(Real x, Real y, Real z, Real w) = 0;
			
			/**	\brief Sets rest translation relative the parent origin
			
				\param x,y,z New rest translation for this segment relative to parent.
			*/
			virtual void setRestTranslation(Real x, Real y, Real z) = 0;

			/** \brief Returns number of degrees of freedom (includes active dofs only)*/
			virtual unsigned int NoDof() const = 0;

			/** \brief Returns index of first degree of freedom for this segment, based on position in IKSolver segments.*/
			virtual unsigned int Id() const = 0;

			/**	\brief Returns solver index handle for this segment (position in IKSolver segments vector).*/
			virtual unsigned int SegmentId() const = 0;

			/** 
				\brief Returns  pointer to the solver. 
				\deprecated To be removed in the next release
			*/
			virtual void* GetSolver() const = 0;

			/**	\brief Attaches user data to the IKSegment instance.
			
				\param data	Pointer to user data.
			*/
			virtual void setUserData(void* data) = 0;

			/**	\brief Returns the user data that is attached to the IKSegment instance.*/
			virtual void* getUserData() = 0;
			//@}
			/*******************************FORWARD KINEMATICS ACCESS********************************/
			/** 
				@name Forward Kinematic Access
			*/
			///@{
			
			/**
				\brief Perform FK calculations on the segment and its children.

				This function will update the  forward kinematics recursively, 
				applying transf to this segment and going further down the chain.
				Call this on the root segment with transf.mRot=Quaternion(0,0,0,1) 
				and transform.mTrans=Vector(0,0,0) or IKSolver::updateFIK() to make 
				sure that all global positions are valid (updated with the latest local results).
				
				\remark IKSolve::solve will perform the FK calculations automatically before returning.
						Only call this if local translation/orientation has changed by-passing the solver.

				\param transform Transformation matrix that will be passed to the segment as parent transform.
				\param useTargetOrient Flag that enables/disables the usage of target orientation.
			*/
			
			virtual void updateForward(Transform& transform,bool useTargetOrient=false) = 0;
			
			/** \brief Returns FIK::Vector with global position (skeleton space) of the base of the segment. */
			virtual const Vector& getStart() const = 0;
			
			/** \brief Returns FIK::Vector with global position (skeleton space) of the tip of the segment (tip is equal to base if no length vector is set).*/
			virtual const Vector& getEnd() const = 0;

			/** \brief Returns the global transformation (position+orientation) at the end of the segment. */
			virtual const Transform& getTransform() const = 0;

			/** \brief Returns global orientation quaternion after solving in FIK::Quaternion format.*/
			virtual const Real* getGlobalQ()const = 0;

			/** \brief  Returns global position after solving, equivalent to getStart().ptr().*/
			virtual const Real* getGlobalPos()const = 0;

			/** \brief Returns FIK::Vector with the centre of gravity position of augmented body for this segment. */
			virtual const Vector& getAugCenter()const = 0;

			/** \brief Returns FIK::RotMatrix with the global transform orientation matrix.*/
			virtual const RotMatrix& getGlobalMat()const = 0;		


			virtual void SetCollisionShape(IKShape* Shape) = 0;
			virtual IKShape* GetCollisionShape() const = 0;
			virtual IKShape* GetCollisionShape()  = 0;

			//@}
			/******************************LIMITS CONFIGURATION**************************************/
			/** @name Limits Configuration */
			///@{

			/** 
				\brief Set joint limits for checking distances to boundaries.


				When axis=0,1,2 (x,y,z) then lmin and lmax are dot products local_axis.dot(parent_axis), respective local_axis
				is defined with setProjectionAxis. Possible range -1 to 1.

				When axis=3 then lmin and lmax are delta roll values in degrees with respect to the rest orientation, 
				taken around the axis defined with setRotationAxis reasonable range -90 to 90, keep as small as possible, 
				otherwise projection limits are invalidated.

				\param axis The limits will be set over the specified axis with this parameter.
				\param lmin, lmax:	Minimum and maximum limits of the bone over the specified axis.
			*/
			virtual void setLimits(unsigned int axis, Real lmin, Real lmax) = 0;

			/**	
				\brief Return segment limits.

				\returns pointer to segment limits array[4][2] in the following format:\n
							limits[0]- min_x / max_x\n
							limits[1]- min_y / max_y\n
							limits[2] - min_z / max_z\n
							limits[3] - min_roll / max_roll \n
							using (radians, absolute values).

			*/
			virtual const Real* getLimits()const = 0;

			/** 
				\brief Sets jont limits in Euler limits format as defined by FIK::EulerLimits.
			
				\param limits Limits represented with EulerLimits struct.
				\param relativeRest	Set this parameter to false if current local axes are meant (while figure is manipulated through the solver).
			*/
			virtual void setEulerLimits(const EulerLimits& limits, bool relativeRest=true) = 0;

			/**	
				\brief Sets axis with respect to which roll limits are defined.

				Call after setting the delta roll limits values (axis 3) so that the correct absolute values for roll can be computed.
				Setting arbitrary axis (1,0,0),(0,1,0) or (0,0,1) is not efficient as this will involve an extra rotation at each check,
				use rotationAxis=0,1,2 instead. The arbitrary vector does not have to be normalised but should not be (0,0,0).
			
				\param rotationAxis Index of the desired rotation axis 0 = x, 1 = y, 2 = z, 3 = Arbitary rotation axis .
				\param vector Arbitrary rotation vector.
			*/
			virtual void setRotationAxis(unsigned int rotationAxis=2, const Real* vector=NULL) = 0;

			/**
				\brief Sets axis with respect to which the projections limits are defined (which local axis was projected by the user onto parent)
				
				Possible values: 0,1,2 (X,Y,Z), pick the nearest to your rotation axis, if in doubt.
			
				\param projectionAxis: Index of the desired projection axis.
			*/
			virtual void setProjectionAxis(unsigned int projectionAxis=2) = 0;

			/** \brief	Determines if the segment should always try to go to the rest orientation, without checking limits. This is the default
						condition before setting any limits.

				if flag is fallse and enforceLimits is set to true then return towards the limits boundary. 
				If enforceLimits is set to false then return towards rest orientation.

				\param flag If the flag is set to "true: it will discard limits information (if any given) and constantly go back to rest if possible.
						if the parameter is "false" the segment will use limits information (if any given) to check range violation and
			*/				
			virtual void setAlwaysToRest(bool flag) = 0;
			
			/** 
				\brief	The method produces a constant averaged target joint orientation from the limits data and the weight of the rest orientation.
				
				\param restWeight This value indicates the influence of the rest orientation on the target produced. 
						The target will not be adjusted in any way once it is set.
			*/
			virtual void setLimitQuat(Real restWeight=0.3333) = 0;

			/** 
				\brief set limits for stretch (translation) degrees of freedom in local frame of this bone
			
				\param axis 0 for x, 1 for y, 2 for z
				\param lmin minimum value of stretch/translation
				\param lmax	maximum value of stretch/translation
			*/
			virtual void setStretchLimits(unsigned int axis,Real lmin, Real lmax)=0;

			/** \brief	Returns segment stretch limits.
			
				\return Pointer to the stretch limit array of size [3][2] with the following format:\n
							limits[0]- min_x / max_x\n
							limits[1]- min_y / max_y\n
							limits[2] - min_z / max_z\n
							using (scene units in local bone space).
			*/
			virtual const Real* getStretchLimits() = 0;

			/**
				\brief Set individual gain for limits error (multiplies limits gain of solver)
			
				\param gain	value of gain to apply for this segment (0 will disable limits).
			*/
			virtual void setLimitsGain(Real gain)=0;

			/** \brief	return the limits gain currently set */
			virtual Real getLimitsGain()=0;

			/**	
				\brief Set IKSegment individual retargeting gain (multiplies retargeting gain of solver)

				\remark changing IKSegment retargeting gain will change the gain for the shadowposing as well. 
						If ShadowPosing is used do not call this function.


				\param gain value of gain to apply for this segment (0 will disable retargeting)
			*/
			virtual void setRetargetingGain(Real gain)=0;

			/**
			\brief Set IKSegment individual DoF retargeting gain (multiplies retargeting gain of solver)

			\remark changing IKSegment retargeting gain will change the gain for the shadowposing as well.
			If ShadowPosing is used do not call this function.


			\param gain value of gain to apply for this segment (0 will disable retargeting)
			*/
			virtual void setRetargetingGain(int axis, Real gain) = 0;

			/**	\brief Return the retargeting gain currently set */
			virtual Real getRetargetingGain()=0;

			/**	\brief Return the retargeting gain currently set  for the specified DoF*/
			virtual Real getRetargetingGain(int axis) = 0;

			/** 
				\brief Set individual gain for translation/stretch error
				
				\param gain	value of gain to apply for this segment (0 will disable "spring" action on stretch)
			*/
			virtual void setTranslationErrorGain(Real gain)=0;

			/**	\brief Return the translation error gain*/
			virtual Real getTranslationErrorGain()=0;
			//@}
			/*****************************VARIOUS UTILITIES**************************************/
			/** @name Utilities */
			///@{
			
			/** \brief	Returns the max distance of the end of this bone from the local origin. */
			virtual Real MaxExtension() const = 0;
			
			/** \brief Returns weight set for the respective degree of freedom. */
			virtual Real Weight(int dof) const = 0;
			
			/**
				\brief Scales the rest translation and local length vector.
				
				\remark This will lead to the mass and local centre of mass being reassigned with defaults.

				\param scale New scale for the segment.
			*/
			virtual void scaleSegment(Real scale) = 0;
			
			/** \brief Sets segment to rest position and rest orientation.*/
			virtual void resetSegment() = 0;	
			
			/** 
				\brief	Set name of this segment.
				
				\param bname Name of the segment.
			*/
			virtual void setName(const char* bname) = 0;

			/** \brief Returns local rest translation.*/
			virtual const Vector& getRestPos()const = 0;

			/** \brief Returns local rest orientation. */
			virtual const Quaternion& getRestQ()const = 0;

			/** 
				\brief Returns a local dof vector for the Jacobian.
				
				\param dof Index of the column vector.
			*/
			virtual Vector Axis(int dof) const = 0;

			/** \brief Returns whether animation/mocap retargeting is enabled for this segment.*/
			virtual bool haveMocap()const = 0;

			/** \brief Returns if segment is active.*/
			virtual bool Active()const = 0;

			/** 
				\brief	Returns if a dof of the segment is active.
			
				\param i Index of the degree of freedom to check.
			*/
			virtual bool ActiveDof(unsigned int i)const = 0;
			
			/** \brief	Returns if the global transform is valid.*/
			virtual bool Dirty() = 0;

			/** \brief	Returns the name of this segment. */
			virtual const char* getName()const = 0;

			/** \brief Returns the center of gravity weight of this segment. */
			virtual Real getCGWeight()const = 0;

			/** 
				\brief Sets the mass of the segment (concentrated in the middle) at any point during runtime.
			
				\param mass New mass value for the bone.
			*/
			virtual void setMass(Real mass) = 0;

			/** \brief	Returns segment mass. */
			virtual Real getMass() = 0;

            /**
				\brief Sets the maximum joint velocity for all DOF.
				
				\param max_dq New maximum joint velocity value for the bone.
			*/
            virtual void setMaxW(Real max_dq) = 0;
        
            /**	\brief Returns maximum joint velocity; same for all DOF */
            virtual Real getMaxW() = 0;        
        
			/** 
				\brief	Sets position of centre of mass for this segment in its local frame.
			
				\param x,y,z:	New position centre of mass for the bone.
			*/
			virtual void setCenter(Real x, Real y, Real z) = 0;

			/** 
				\brief Returns position of centre of mass for this segment in its local frame.
			*/
			virtual const Vector& getCenter()const = 0;

			/** \brief Returns depth to root value.*/
			virtual int getDepthToRoot() = 0;

			/** 
				\brief Returns equivalent scale calculated from StretchIK 
				
				\remark call IKSolver::deriveScale(parentScale) first.
				
				\deprecated To be removed in the next release.
			*/
			virtual Real getScale()=0;

			/** 
				\brief Returns equivalent accumulated global scale calculated from StretchIK 
				
				\remark call IKSolver::deriveScale(parentScale) first.

				\deprecated To be removed in the next release.
			*/
			virtual Real getGlobalScale()=0;


			//@}
		private:
			//no private memebers

		protected:
			//no protected members		
	};



}//namespace FIK

#endif // IKSEGMENT_HEADER_INCLUDED

