/* Copyright (C) 2006-2011, IKinema Ltd. All rights reserved.
 *
 * IKinema API Library; SDK distribution
 * 
 * This file is part of IKinema engine http://www.ikinema.com
 *
 * Your use and or redistribution of this software in source and / or binary form, with or without
 * modification, is subject to: 
 * (i) your ongoing acceptance of and compliance with the terms and conditions of
 * the IKinema License Agreement; and 
 *
 * (ii) your inclusion of this notice in any version of this software that you use 
 * or redistribute.
 *
 *
 * A copy of the IKinema License Agreement is available by contacting
 * IKinema Ltd., http://www.ikinema.com, support@ikinema.com
 *
 */

#ifndef IKMEMORY_HEADER_INCLUDED
#define IKMEMORY_HEADER_INCLUDED
 
#include <stddef.h>

#define FIKAlignment 128

#if defined BUILD_DLL
	#if defined( __WIN32__ ) || defined( _WIN32 )
		#define LIB_API __declspec( dllexport )
	#else
		#pragma GCC visibility push(default)
		#define LIB_API
	#endif
#elif defined USE_DLL
	#define LIB_API __declspec( dllimport )
#else
	#define LIB_API
#endif


namespace FIK {

//different implementations can ignore the 'alignment' parameter depending of the platform
typedef   void* (*FIKAllocCB)(unsigned long long size, unsigned long long alignment);
typedef   void (*FIKFreeCB)(void* memblock);

//default memory routines
//extern LIB_API FIKAllocCB	FIKAlloc;
LIB_API void* FIKAlloc(unsigned long long size, unsigned long long alignment);
extern LIB_API FIKFreeCB	FIKFree;

//client should not be able to change the alignment
LIB_API void SetMemoryRoutines(FIKAllocCB alloc, FIKFreeCB free, unsigned int alignment=128);


}//namespace FIK

#endif//IKMEMORY_HEADER_INCLUDED
