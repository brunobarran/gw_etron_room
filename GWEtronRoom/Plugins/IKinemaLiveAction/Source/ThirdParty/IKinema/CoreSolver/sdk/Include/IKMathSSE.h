/* Copyright (C) 2006-2011, IKinema Ltd. All rights reserved.
 *
 * IKinema API Library; SDK distribution
 * 
 * This file is part of IKinema engine http://www.ikinema.com
 *
 * Your use and or redistribution of this software in source and / or binary form, with or without
 * modification, is subject to: 
 * (i) your ongoing acceptance of and compliance with the terms and conditions of
 * the IKinema License Agreement; and 
 *
 * (ii) your inclusion of this notice in any version of this software that you use 
 * or redistribute.
 *
 *
 * A copy of the IKinema License Agreement is available by contacting
 * IKinema Ltd., http://www.ikinema.com, support@ikinema.com
 *
 */

#ifndef IKMATH_HEADER_INCLUDED
#define IKMATH_HEADER_INCLUDED

#include <math.h>
#include <float.h>

#include "IKMemory.h"
#include <intrin.h>
#include <xmmintrin.h>
#include <smmintrin.h>
#ifdef BUILD_DLL
#if defined( __WIN32__ ) || defined( _WIN32 )
	#define DLL_EXPORT __declspec( dllexport )
#else
	#pragma GCC visibility push(default)
#endif
#else
#define DLL_EXPORT
#endif

#define INLINE_MATH 1

#if INLINE_MATH
#define INLINE_FUNC inline
#else
#define INLINE_FUNC
#endif

namespace FIK {

typedef float Real;

#define FIK_PI (3.14159265358979323846f)
#define FIK_EPSILON (1.0e-9f)
#define RADS_PER_DEG (0.01745329251994329547f)

Real Radians(Real x); 
bool fuzzyZero(Real x);
Real Clamp(Real value, Real min, Real max);

//Union to guarantee 16 byte alignment of vector data
union sse4
{
	__m128 m;
	Real f[4];
};
class RotMatrix;
class Quaternion;
class DLL_EXPORT Vector
{
	public:
		Vector(){}
		Vector(const float* vec);
		Vector(const double* vec);
		Vector(Real x, Real y, Real z);
		Vector(const __m128& f);
		Real dot(const Vector& v)const;		
		Vector  cross(const Vector& v) const;
		Real length();
		Real lengthInverse() const;
		void normalize();//does not check if vector is zero!
		Real& operator[](int i);
		const Real& operator [] (int i )const;
		Vector operator+(const Vector& rvec)const;
		Vector operator-(const Vector& rvec)const;
		Vector& operator=(const Vector& rvec);
		Vector operator*(Real scalar);
		Vector& operator*=(Real scalar);
		Vector& operator +=(const Vector& rvec);
		inline Real* ptr(){return &arr.f[0];}
		inline const Real* cptr() const {return &arr.f[0];}
		static Vector ZERO() {return Vector(.0f, .0f, .0f);}
		friend RotMatrix;
		friend Quaternion;
	private:

		
		//Real arr[4];
		sse4 arr;
};

class DLL_EXPORT Quaternion
{
	public:
		Quaternion(){}
		Quaternion(Real x, Real y, Real z, Real w);
		Quaternion(const float* vec);
		Quaternion(const double* vec);
		Quaternion(const Vector& axis, Real angle);
		Quaternion(const __m128& vec);
		Real& operator[](int i);
		const Real& operator[](int i) const;
		Vector operator* (const Vector& v) const;
		Quaternion& operator=(const Quaternion& rquat);
		void normalize();
		Quaternion inverse()const;//unit inverse
		inline Real* ptr(){return &quat.f[0];}
		inline const Real* cptr() const {return &quat.f[0];}
		static Quaternion IDENTITY() {return Quaternion(0.0,0.0,0.0,1.0);}
		RotMatrix toMatrixTrans() const;
		friend RotMatrix;
		friend Quaternion operator*(const Quaternion& q1,const Quaternion& q2);
	private:
		//Real arr[4];
		sse4 quat;

};

class DLL_EXPORT RotMatrix
{
	public:
		RotMatrix(){}
		RotMatrix(Real xx, Real xy, Real xz, 
              Real yx, Real yy, Real yz, 
              Real zx, Real zy, Real zz);
		RotMatrix(const Quaternion& quat);
		RotMatrix(int axis, Real angle);
		Vector& operator[](int i);
		const Vector& operator[](int i) const;
		RotMatrix& operator=(const RotMatrix& rmat);
		RotMatrix& operator*=(const RotMatrix& rmat);
		Vector operator*(const Vector& vec)const;
		RotMatrix operator*(const RotMatrix& rmat)const;
		void setIdentity();
		Quaternion toQuat()const;
		Vector getColumn(int i)const;		
		RotMatrix transposed()const;
		RotMatrix(const __m128* row);
	private:
		
		sse4 m_row[4];
};


typedef struct
{
	RotMatrix mRot;
	Quaternion mRotQuat;
	Vector mTrans;
}Transform;

Quaternion operator*(const Quaternion& q1,const Quaternion& q2);
RotMatrix MultTransposeLeft(const RotMatrix& m1, const RotMatrix& m2);
RotMatrix MultTransposeRight(const RotMatrix& m1, const RotMatrix& m2);


}//namespace FIK

#include "IKMathSSE.inl"

#endif // IKMATH_HEADER_INCLUDED
