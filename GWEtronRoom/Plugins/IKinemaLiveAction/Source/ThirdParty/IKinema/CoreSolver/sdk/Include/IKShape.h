#pragma once
#include "IKMath.h"
namespace FIK
{

	enum ShapeType
	{
		IKBox = 0,
		IKSphere,
		IKCapsule,
		ShapeNone = 0xFF
	};


	//Shapes are by default centred in the bone location. Not at the centre of the bone length.
	//TODO: Fix that

	class DLL_EXPORT IKShape
	{
	public:
		IKShape() : localOffset()
		{};
		virtual ShapeType GetType() const { return ShapeNone; };
		virtual bool IsValid() const = 0;
		virtual ~IKShape(){};

		virtual bool RayCast(const Transform& ShapeTransform, const Vector& RayStart, const Vector& RayDir, Vector& NearestPoint, Vector& Normal)  = 0; // called by Segment to do the cast. 

		const Vector& GetContactPoint() const;
		const Vector& GetContactNormal() const;
		const Transform& GetLocalOffset() const;
		void SetLocalOffset(const Transform& InLocalOffset);
	protected:
		Vector ClosestPoint;
		Vector CPNormal;
		Transform localOffset;

	};


	class DLL_EXPORT IKCapsuleShape : public IKShape
	{
	public:
		IKCapsuleShape();
		IKCapsuleShape(Real InHalfHeight, Real InRadius);
		ShapeType GetType() const override { return IKCapsule; };
		bool IsValid() const override;

		~IKCapsuleShape() override {};


		bool RayCast(const Transform& ShapeTransform, const Vector& RayStart, const Vector& RayDir, Vector& NearestPoint, Vector& Normal) override;
		void GetLine(const Transform& ShapeTransform);
		
		const Real& GetHalfHeight() const;
		const Real& GetRadius() const;
		void SetHalfHeight(Real InHalfHeight);
		void SetRadius(Real InRadius);
	private:
	
		int IntersectRayCapsule(const Vector& RayStart, const Vector& RayDir, float s[2]);

		/*
		  Return the distance of point along the line defined by p1 - p0, 0 < t < 1
		  where t = 0, if point = p0, and t= 1 if point = p1
		*/
		Real DistanceAlongLine(const Vector& point);

		Real HalfHeight;
		Real Radius;
		Vector p0;
		Vector p1;
	};

	class DLL_EXPORT IKBoxShape : public IKShape
	{
	public:
		IKBoxShape();
		IKBoxShape(Real x, Real y, Real z);
		explicit IKBoxShape(const Vector& InHalfExtents);
		ShapeType GetType() const override{ return IKBox; };
		bool IsValid() const override;
		~IKBoxShape() override {};

		bool RayCast(const Transform& ShapeTransform, const Vector& RayStart, const Vector& RayDir, Vector& NearestPoint, Vector& Normal)  override;
		
		const Vector& GetHalfExtent() const;
		void SetHalfExtent(const Vector& InHalfExtent);

	private:
		static int RayBoxIntersection(const Vector& minimum, const Vector& maximum, const Vector& origin, const Vector& _dir, Vector& coord, Real & t);
		Vector HalfExtent;
	};

	class DLL_EXPORT IKSphereShape : public IKShape
	{
	public:
		IKSphereShape();
		explicit IKSphereShape(Real InRadius);
		ShapeType GetType() const override { return IKSphere; };
		bool IsValid() const override;
		~IKSphereShape() override {};

		bool RayCast(const Transform& ShapeTransform, const Vector& RayStart, const Vector& RayDir, Vector& NearestPoint, Vector& Normal) override;
		
		const Real& GetRadius() const;
		void SetRadius(Real InRadius);
	private:

		friend IKCapsuleShape;
		static bool intersectRaySphere(const Vector& origin, const Vector& dir, const Vector& center, Real radius, Real& dist, Vector& hit_pos);
		static bool intersectRaySphere(const Vector& origin, const Vector& dir, const Vector& center, Real radius, Real& dist, Vector&& hit_pos);

		Real Radius;
	};

	

}