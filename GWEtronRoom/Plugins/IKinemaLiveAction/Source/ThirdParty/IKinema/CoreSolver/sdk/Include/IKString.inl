
#include <string.h>
#include <stdexcept>
#include "IKString.h"

#define IKSTR_BASE_SIZE 128

inline IKString::IKString()
{
	m_uiBuffSize = IKSTR_BASE_SIZE;
	m_pStr[0] = '\0';
}

inline IKString::IKString(const char *pStr)
{
	m_uiBuffSize = IKSTR_BASE_SIZE;
	m_pStr[0] = '\0';
	SetString(pStr);
}

inline IKString::IKString(const IKString &ikStr)
{
	m_uiBuffSize = IKSTR_BASE_SIZE;
	m_pStr[0] = '\0';
	SetString(ikStr.GetCharPtr());
}

inline IKString::~IKString()
{
	
}

inline IKString& IKString::operator=(const IKString& str)
{
	SetString(str.GetCharPtr());
	return *this;
}

inline void IKString::SetString(const char *pStr)
{
	size_t size = strlen(pStr) + 1;
	if (size >= m_uiBuffSize)
	{
		throw std::runtime_error("String is larger than 128");
	}

	strcpy_s(m_pStr, size, pStr);
}

inline IKString& IKString::operator=(const char *pStr)
{
	SetString(pStr);
	return *this;
}

inline bool IKString::operator==(const char *pStr) const
{
	int res = strcmp(m_pStr, pStr);
	if (res == 0)
	{
		return true;
	}
	
	return false;
}

inline bool IKString::operator==(const IKString& str) const
{
	return this->operator==(str.GetCharPtr());
}



inline bool IKString::Empty()
{
	return (m_pStr && strlen(m_pStr) == 0);
}

