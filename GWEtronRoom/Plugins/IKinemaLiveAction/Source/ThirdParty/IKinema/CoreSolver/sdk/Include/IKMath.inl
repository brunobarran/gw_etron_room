/* Copyright (C) 2006-2011, IKinema Ltd. All rights reserved.
 *
 * IKinema API Library; SDK distribution
 * 
 * This file is part of IKinema engine http://www.ikinema.com
 *
 * Your use and or redistribution of this software in source and / or binary form, with or without
 * modification, is subject to: 
 * (i) your ongoing acceptance of and compliance with the terms and conditions of
 * the IKinema License Agreement; and 
 *
 * (ii) your inclusion of this notice in any version of this software that you use 
 * or redistribute.
 *
 *
 * A copy of the IKinema License Agreement is available by contacting
 * IKinema Ltd., http://www.ikinema.com, support@ikinema.com
 *
 */

#include <math.h>
#include <float.h>

#if !FIK_SSE

namespace FIK {

INLINE_FUNC Real Radians(Real x) { 
    return x * RADS_PER_DEG;
}
INLINE_FUNC float absf(float g)
{
unsigned int *gg;
gg=(unsigned int*)&g;
*(gg)&=2147483647u;
return g;
}

INLINE_FUNC bool      fuzzyZero(Real x) {

unsigned int *gg;
gg=(unsigned int*)&x;
*(gg)&=2147483647u;

return (x<FIK_EPSILON);

}

INLINE_FUNC Real Clamp(Real value, Real min, Real max)
{
	if(value< min) return min;
	if(value> max) return max;
	return value;
}


//Vector

INLINE_FUNC Vector::Vector()
{
	arr.f[0] = 0.f;
	arr.f[1] = 0.f;
	arr.f[2] = 0.f;
}

INLINE_FUNC Vector::Vector(const float* vec)
{
			arr.f[0]=Real(vec[0]);
			arr.f[1]=Real(vec[1]);
			arr.f[2]=Real(vec[2]);
}
INLINE_FUNC Vector::Vector(Real x, Real y, Real z)
{
			arr.f[0]=x;
			arr.f[1]=y;
			arr.f[2]=z;
}
INLINE_FUNC Vector::Vector(const double* vec)
{
			arr.f[0]=Real(vec[0]);
			arr.f[1]=Real(vec[1]);
			arr.f[2]=Real(vec[2]);
}
INLINE_FUNC Real Vector::dot(const Vector& v)const{
		return (arr.f[0] * v[0] + arr.f[1] * v[1] + arr.f[2] * v[2]);
}

INLINE_FUNC Vector Vector::cross(const Vector& v) const {
    return Vector(arr.f[1] * v[2] - arr.f[2] * v[1],
                      arr.f[2] * v[0] - arr.f[0] * v[2],
                      arr.f[0] * v[1] - arr.f[1] * v[0]);
}
INLINE_FUNC Real Vector::length() const
{
	return sqrtf(arr.f[0]*arr.f[0]+arr.f[1]*arr.f[1]+arr.f[2]*arr.f[2]);
}
INLINE_FUNC void Vector::normalize()
{
	Real ln=sqrtf((float)(arr.f[0]*arr.f[0]+arr.f[1]*arr.f[1]+arr.f[2]*arr.f[2]));
	arr.f[0] /= ln; arr.f[1]/=ln; arr.f[2]/=ln;
}

inline Real& Vector::operator [] (int i )
{
      return arr.f[i];
}
inline const Real& Vector::operator [] (int i )const
{
      return arr.f[i];
}
INLINE_FUNC Vector& Vector::operator = ( const Vector& rkVector )
{
    arr.f[0] = rkVector[0];
    arr.f[1] = rkVector[1];
    arr.f[2] = rkVector[2];

    return *this;
}

INLINE_FUNC Vector Vector::operator*(Real s) const {
    return Vector(arr.f[0] * s, arr.f[1] * s, arr.f[2] * s);
}

INLINE_FUNC Vector& Vector::operator*=(Real s) {
    arr.f[0] *= s; arr.f[1] *= s; arr.f[2] *= s;
    return *this;
}
INLINE_FUNC Vector& Vector::operator +=(const Vector& rvec){
	arr.f[0] +=rvec[0];
	arr.f[1] +=rvec[1];
	arr.f[2] +=rvec[2];
	return *this;
}

INLINE_FUNC Vector Vector::operator + ( const Vector& rkVector ) const
{
    return Vector(
        arr.f[0] + rkVector[0],
        arr.f[1] + rkVector[1],
        arr.f[2] + rkVector[2]);
}
INLINE_FUNC Vector Vector::operator - ( const Vector& rkVector ) const
{
    return Vector(
        arr.f[0] - rkVector[0],
        arr.f[1] - rkVector[1],
        arr.f[2] - rkVector[2]);
}

INLINE_FUNC bool Vector::operator== (const Vector& rvec) const
{
	return arr.f[0] == rvec[0] && arr.f[1] == rvec[1] && arr.f[2] == rvec[2];
}

//Quaternion
INLINE_FUNC Real* Quaternion::ptr() { return &arr.f[0]; }

INLINE_FUNC const Real* Quaternion::cptr() const { return &arr.f[0]; }

INLINE_FUNC Quaternion::Quaternion()
{
	arr.f[0] = arr.f[1] = arr.f[2] = 0.f; arr.f[3] = 1.f;
}

INLINE_FUNC Quaternion::Quaternion(const float* vec)
{
			arr.f[0]=Real(vec[0]);
			arr.f[1]=Real(vec[1]);
			arr.f[2]=Real(vec[2]);
			arr.f[3]=Real(vec[3]);
}
INLINE_FUNC Quaternion::Quaternion(Real x, Real y, Real z, Real w)
{
			arr.f[0]=x;
			arr.f[1]=y;
			arr.f[2]=z;
			arr.f[3]=w;
}

INLINE_FUNC Quaternion::Quaternion(const double* vec)
{
			arr.f[0]=Real(vec[0]);
			arr.f[1]=Real(vec[1]);
			arr.f[2]=Real(vec[2]);
			arr.f[3]=Real(vec[3]);
}

inline Real& Quaternion::operator [] (int i )
{
      return arr.f[i];
}

inline const Real& Quaternion::operator [] (int i )const
{
      return arr.f[i];
}

INLINE_FUNC Quaternion& Quaternion::operator = ( const Quaternion& rkQuaternion )
{
	arr.f[0] = rkQuaternion[0];
    arr.f[1] = rkQuaternion[1];
    arr.f[2] = rkQuaternion[2];
	arr.f[3] = rkQuaternion[3];

    return *this;
}

INLINE_FUNC RotMatrix Quaternion::toMatrixTrans() const 
{
	return RotMatrix (*this).transposed();
}

INLINE_FUNC Quaternion::Quaternion(const Vector& axis, Real angle) {
	if (axis.length() == 0.f)
	{
		arr.f[0] = arr.f[1] = arr.f[2] = 0.f;
		arr.f[3] = 1.f;
		return;
	}
	Real d = sqrtf(axis[0]*axis[0]+axis[1]*axis[1]+axis[2]*axis[2]);
    Real s = sinf(angle * Real(0.5)) / d;
    arr.f[0]=axis[0] * s;
	arr.f[1]=axis[1] * s;
	arr.f[2]=axis[2] * s; 
    arr.f[3]=cosf(angle * Real(0.5));
}

INLINE_FUNC Quaternion operator*(const Quaternion& q1, 
                                   const Quaternion& q2) {
    return Quaternion(q1[3] * q2[0] + q1[0] * q2[3] + q1[1] * q2[2] - q1[2] * q2[1],
                         q1[3] * q2[1] + q1[1] * q2[3] + q1[2] * q2[0] - q1[0] * q2[2],
                         q1[3] * q2[2] + q1[2] * q2[3] + q1[0] * q2[1] - q1[1] * q2[0],
                         q1[3] * q2[3] - q1[0] * q2[0] - q1[1] * q2[1] - q1[2] * q2[2]); 
}

INLINE_FUNC Vector Quaternion::operator* (const Vector& v) const
{
	// nVidia SDK implementation
	Vector uv, uuv;
	Vector qvec(&arr.f[0]);
	uv = qvec.cross(v);
	uuv = qvec.cross(uv);
	uv *= (2.0f * arr.f[3]);
	uuv *= 2.0f;

	return v + uv + uuv;
}

INLINE_FUNC Quaternion Quaternion::inverse ()const
{    
    return Quaternion(-arr.f[0],-arr.f[1],-arr.f[2],arr.f[3]);
}

INLINE_FUNC void Quaternion::normalize()
{
	Real d = sqrtf(arr.f[0]*arr.f[0]+arr.f[1]*arr.f[1]+arr.f[2]*arr.f[2]+arr.f[3]*arr.f[3]);
    arr.f[0]/=d;
	arr.f[1]/=d;
	arr.f[2]/=d; 
    arr.f[3]/=d;
}


//RotMatrix
INLINE_FUNC RotMatrix::RotMatrix(Real xx, Real xy, Real xz, 
              Real yx, Real yy, Real yz, 
              Real zx, Real zy, Real zz) {
    m_row[0].f[0] = xx; m_row[0].f[1] = xy; m_row[0].f[2] = xz;
    m_row[1].f[0] = yx; m_row[1].f[1] = yy; m_row[1].f[2] = yz;
    m_row[2].f[0] = zx; m_row[2].f[1] = zy; m_row[2].f[2] = zz;
}

INLINE_FUNC RotMatrix::RotMatrix(const Quaternion& q) {
	Real qx, qy, qz, qw, qx2, qy2, qz2, qxqx2, qyqy2, qzqz2, qxqy2, qyqz2, qzqw2, qxqz2, qyqw2, qxqw2;
    qx = q[0];
    qy = q[1];
    qz = q[2];
    qw = q[3];

    qx2 = ( qx + qx );
    qy2 = ( qy + qy );
    qz2 = ( qz + qz );
    qxqx2 = ( qx * qx2 );
    qxqy2 = ( qx * qy2 );
    qxqz2 = ( qx * qz2 );
    qxqw2 = ( qw * qx2 );
    qyqy2 = ( qy * qy2 );
    qyqz2 = ( qy * qz2 );
    qyqw2 = ( qw * qy2 );
    qzqz2 = ( qz * qz2 );
    qzqw2 = ( qw * qz2 );

	m_row[0].f[0] = ( ( 1.0f - qyqy2 ) - qzqz2 );	m_row[1].f[0] = ( qxqy2 + qzqw2 );			m_row[2].f[0] = ( qxqz2 - qyqw2 );
	m_row[0].f[1] = ( qxqy2 - qzqw2 );			m_row[1].f[1] = ( ( 1.0f - qxqx2 ) - qzqz2 );	m_row[2].f[1] = ( qyqz2 + qxqw2 );
	m_row[0].f[2] = ( qxqz2 + qyqw2 );			m_row[1].f[2] = ( qyqz2 - qxqw2 );			m_row[2].f[2] = ( ( 1.0f - qxqx2 ) - qyqy2 );
}

INLINE_FUNC RotMatrix::RotMatrix(int axis, Real angle)
{
	Real cosine=cosf(angle);
	Real sine=sinf(angle);
	if (axis == 0)
	{
		m_row[0].f[0] = 1.f; m_row[0].f[1] = 0.f; m_row[0].f[2] = 0.f;
		m_row[1].f[0] = 0.f; m_row[1].f[1] = cosine; m_row[1].f[2] = -sine;
		m_row[2].f[0] = 0.f; m_row[2].f[1] = sine; m_row[2].f[2] = cosine;
	}
	else if (axis == 1)
	{
		m_row[0].f[0] = cosine; m_row[0].f[1] = 0.0; m_row[0].f[2] = sine;
		m_row[1].f[0] = 0.0; m_row[1].f[1] = 1.0; m_row[1].f[2] = 0.0;
		m_row[2].f[0] = -sine; m_row[2].f[1] = 0.0; m_row[2].f[2] = cosine;
	}
	else if (axis == 2)
	{
		m_row[0].f[0] = cosine; m_row[0].f[1] = -sine; m_row[0].f[2] = 0.0f;
		m_row[1].f[0] = sine; m_row[1].f[1] = cosine; m_row[1].f[2] = 0.0f;
		m_row[2].f[0] = 0.0f; m_row[2].f[1] = 0.0f; m_row[2].f[2] = 1.0f;
	}
	else
	{
		m_row[0].f[0] = m_row[0].f[1] = m_row[0].f[2] = 0.f;
		m_row[1].f[0] = m_row[1].f[1] = m_row[1].f[2] = 0.f;
		m_row[2].f[0] = m_row[2].f[1] = m_row[2].f[2] = 0.f;
	}
}


INLINE_FUNC Quaternion RotMatrix::toQuat() const {
    int 	next[3] = { 1, 2, 0 };

    Quaternion result;
   
    Real trace = m_row[0].f[0] + m_row[1].f[1] + m_row[2].f[2];
    
    if (trace > 0.0) 
    {
        Real s = sqrtf(trace + Real(1.0));
        result[3] = s * Real(0.5);
        s = Real(0.5) / s;
        
        result[0] = (m_row[2].f[1] - m_row[1].f[2]) * s;
        result[1] = (m_row[0].f[2] - m_row[2].f[0]) * s;
        result[2] = (m_row[1].f[0] - m_row[0].f[1]) * s;
    } 
    else 
    {
        int i = 0;
        if (m_row[1].f[1] > m_row[0].f[0])
            i = 1;
        if (m_row[2].f[2] > m_row[i].f[i])
            i = 2;
        
        int j = next[i];  
        int k = next[j];
        
        Real s = sqrtf(m_row[i].f[i] - m_row[j].f[j] - m_row[k].f[k] + Real(1.0));
        
        result[i] = s * Real(0.5);
        
        s = Real(0.5) / s;
        
        result[3] = (m_row[k].f[j] - m_row[j].f[k]) * s;
        result[j] = (m_row[j].f[i] + m_row[i].f[j]) * s;
        result[k] = (m_row[k].f[i] + m_row[i].f[k]) * s;
    }
    return result;
}

INLINE_FUNC void RotMatrix::normalize()
{
	Quaternion r = toQuat();
	r.normalize();
	RotMatrix temp(r);
	m_row[0] = temp.m_row[0];
	m_row[1] = temp.m_row[1];
	m_row[2] = temp.m_row[2];
}
inline Vector&  RotMatrix::operator[](int i)       { return *(Vector*)(&m_row[i].f[0]); } // TODO check this
inline const Vector&  RotMatrix::operator[](int i)const       { return *(Vector*)(&m_row[i].f[0]); }
INLINE_FUNC RotMatrix& RotMatrix::operator=(const RotMatrix& rmat)
{
	m_row[0]=rmat.m_row[0];
	m_row[1]=rmat.m_row[1];
	m_row[2]=rmat.m_row[2];
	return *this;
}

INLINE_FUNC RotMatrix RotMatrix::operator* (const RotMatrix& rkMatrix)const
{
    RotMatrix kProd;
	
	for (int iRow = 0; iRow < 3; iRow++)
    {
            kProd[iRow][0] =
                m_row[iRow].f[0]*rkMatrix[0][0] +
                m_row[iRow].f[1]*rkMatrix[1][0] +
                m_row[iRow].f[2]*rkMatrix[2][0];

			kProd[iRow][1] =
                m_row[iRow].f[0]*rkMatrix[0][1] +
                m_row[iRow].f[1]*rkMatrix[1][1] +
                m_row[iRow].f[2]*rkMatrix[2][1];

			kProd[iRow][2] =
                m_row[iRow].f[0]*rkMatrix[0][2] +
                m_row[iRow].f[1]*rkMatrix[1][2] +
                m_row[iRow].f[2]*rkMatrix[2][2];
    }
    return kProd;
}

INLINE_FUNC RotMatrix& RotMatrix::operator*=(const RotMatrix& rkMatrix)
{
	for (int iRow = 0; iRow < 3; iRow++)
    {
        Vector temp(&m_row[iRow].f[0]);
            m_row[iRow].f[0] =
                temp[0]*rkMatrix[0][0] +
                temp[1]*rkMatrix[1][0] +
                temp[2]*rkMatrix[2][0];

			m_row[iRow].f[1] =
                temp[0]*rkMatrix[0][1] +
                temp[1]*rkMatrix[1][1] +
                temp[2]*rkMatrix[2][1];

			m_row[iRow].f[2] =
                temp[0]*rkMatrix[0][2] +
                temp[1]*rkMatrix[1][2] +
                temp[2]*rkMatrix[2][2];
    }

    return *this;
}


INLINE_FUNC Vector RotMatrix::operator*(const Vector& v)const {
    return Vector(operator[](0).dot(v),operator[](1).dot(v), operator[](2).dot(v) ); // Extract each row as a Vector then do dot product
}

INLINE_FUNC Vector RotMatrix::getColumn(int i) const {
	return Vector(m_row[0].f[i], m_row[1].f[i], m_row[2].f[i]);
}

INLINE_FUNC RotMatrix RotMatrix::transposed() const {
    return RotMatrix(m_row[0].f[0], m_row[1].f[0], m_row[2].f[0],
                        m_row[0].f[1], m_row[1].f[1], m_row[2].f[1],
                        m_row[0].f[2], m_row[1].f[2], m_row[2].f[2]);
}

INLINE_FUNC void RotMatrix::setIdentity()
{
	m_row[0].f[0]=Real(1.0);
	m_row[0].f[1]=Real(0.0);
	m_row[0].f[2]=Real(0.0);
    m_row[1].f[0]=Real(0.0);
	m_row[1].f[1]=Real(1.0);
	m_row[1].f[2]=Real(0.0);
    m_row[2].f[0]=Real(0.0);
	m_row[2].f[1]=Real(0.0);
	m_row[2].f[2]= Real(1.0);
}

INLINE_FUNC RotMatrix MultTransposeLeft(const RotMatrix& m1, const RotMatrix& m2) {
    return RotMatrix(
        m1[0][0] * m2[0][0] + m1[1][0] * m2[1][0] + m1[2][0] * m2[2][0],
        m1[0][0] * m2[0][1] + m1[1][0] * m2[1][1] + m1[2][0] * m2[2][1],
        m1[0][0] * m2[0][2] + m1[1][0] * m2[1][2] + m1[2][0] * m2[2][2],
        m1[0][1] * m2[0][0] + m1[1][1] * m2[1][0] + m1[2][1] * m2[2][0],
        m1[0][1] * m2[0][1] + m1[1][1] * m2[1][1] + m1[2][1] * m2[2][1],
        m1[0][1] * m2[0][2] + m1[1][1] * m2[1][2] + m1[2][1] * m2[2][2],
        m1[0][2] * m2[0][0] + m1[1][2] * m2[1][0] + m1[2][2] * m2[2][0],
        m1[0][2] * m2[0][1] + m1[1][2] * m2[1][1] + m1[2][2] * m2[2][1],
        m1[0][2] * m2[0][2] + m1[1][2] * m2[1][2] + m1[2][2] * m2[2][2]);
}

INLINE_FUNC RotMatrix MultTransposeRight(const RotMatrix& m1, const RotMatrix& m2) {
    return
        RotMatrix(m1[0].dot(m2[0]), m1[0].dot(m2[1]), m1[0].dot(m2[2]),
                     m1[1].dot(m2[0]), m1[1].dot(m2[1]), m1[1].dot(m2[2]),
                     m1[2].dot(m2[0]), m1[2].dot(m2[1]), m1[2].dot(m2[2]));
                     
}


}//namespace FIK

#else
#define SIMD_HALF _mm_set1_ps(0.5f)
#define SIMD_THREE _mm_set1_ps(3.0f)

namespace FIK {

INLINE_FUNC Real Radians(Real x) { 
    return x * RADS_PER_DEG;
}
//INLINE_FUNC bool      fuzzyZero(Real x) { return fabs(x) < FIK_EPSILON; }
INLINE_FUNC float absf(float g)
{
unsigned int *gg;
gg=(unsigned int*)&g;
*(gg)&=2147483647u;
return g;
}

INLINE_FUNC bool      fuzzyZero(Real x) {

unsigned int *gg;
gg=(unsigned int*)&x;
*(gg)&=2147483647u;

return (x<FIK_EPSILON);

}
INLINE_FUNC Real Clamp(Real value, Real min, Real max)
{
	return value < min ? min : value < max ? value : max;
}


//Vector
INLINE_FUNC Vector::Vector()
{
	arr.m = _mm_setzero_ps();
}
INLINE_FUNC Vector::Vector(const float* vec)
{
			
					
			arr.m = _mm_setzero_ps();
			arr.m = _mm_loadu_ps(vec);
			arr.m = _mm_castsi128_ps(_mm_and_si128(_mm_castps_si128(arr.m), _mm_set_epi32(0x0000000, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF)));
			

}
INLINE_FUNC Vector::Vector(Real x, Real y, Real z)
{
			arr.m = _mm_set_ps(0,z,y,x);
}
INLINE_FUNC Vector::Vector(const double* vec)
{
			
			arr.m = _mm_set_ps(0,(float)vec[2],(float)vec[1],(float)vec[0]);
}

INLINE_FUNC Vector::Vector(const __m128& data)
{
	arr.m = data;
	arr.m = _mm_castsi128_ps(_mm_and_si128(_mm_castps_si128(arr.m),_mm_set_epi32 (0x0000000,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF)));
}



INLINE_FUNC Real Vector::dot(const Vector& v)const{
	__m128 t0;
	t0 = _mm_mul_ps(arr.m, v.arr.m);
	t0 = _mm_hadd_ps(t0, t0);
	t0 = _mm_hadd_ps(t0, t0);
	return _mm_cvtss_f32(t0);
}

INLINE_FUNC Vector Vector::cross(const Vector& v) const {
   
	__m128 cross = _mm_sub_ps(
		_mm_mul_ps(_mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(arr.m), _MM_SHUFFLE(3, 0, 2, 1))), _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(v.arr.m), _MM_SHUFFLE(3, 1, 0, 2)))), 
	 _mm_mul_ps(_mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(arr.m), _MM_SHUFFLE(3, 1, 0, 2))), _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(v.arr.m), _MM_SHUFFLE(3, 0, 2, 1)))));
	
		return Vector(cross);
  

}
INLINE_FUNC Real Vector::length() const
{	
	__m128 t0;
	t0 = _mm_mul_ps(arr.m, arr.m);
	t0 = _mm_hadd_ps(t0, t0);
	t0 = _mm_hadd_ps(t0, t0);
	t0 = _mm_sqrt_ss(t0);
	return _mm_cvtss_f32(t0);
}

INLINE_FUNC Real Vector::lengthInverse() const
{
	DataType t0;
	t0.m = _mm_mul_ps(arr.m, arr.m);
	t0.m = _mm_hadd_ps(t0.m, t0.m);
	t0.m = _mm_hadd_ps(t0.m, t0.m);
	__m128 r = _mm_rsqrt_ps(t0.m);
	t0.m = _mm_mul_ps(_mm_mul_ps(SIMD_HALF,r),_mm_sub_ps(SIMD_THREE,_mm_mul_ps(_mm_mul_ps(t0.m,r),r)));
	return _mm_cvtss_f32(t0.m);

}

INLINE_FUNC void Vector::normalize()
{
	
	arr.m = (_mm_mul_ps(arr.m, _mm_set1_ps(lengthInverse())));
		
	
}

inline Real& Vector::operator [] (int i )
{
      return arr.f[i];
}
inline const Real& Vector::operator [] (int i )const
{
      return arr.f[i];
}
INLINE_FUNC Vector& Vector::operator = ( const Vector& rkVector )
{
	arr.m = rkVector.arr.m;
    return *this;
}

INLINE_FUNC Vector Vector::operator*(Real s) const 
{
	return Vector(_mm_mul_ps(arr.m,_mm_set1_ps(s )));
}

INLINE_FUNC Vector& Vector::operator*=(Real s) {
	arr.m = _mm_mul_ps(arr.m,_mm_set1_ps(s));
    return *this;
}
INLINE_FUNC Vector& Vector::operator +=(const Vector& rvec){

	arr.m = _mm_add_ps(arr.m,rvec.arr.m);
	return *this;
}

INLINE_FUNC Vector Vector::operator + ( const Vector& rkVector ) const
{
	return (_mm_add_ps(arr.m,rkVector.arr.m));
}
INLINE_FUNC Vector Vector::operator - ( const Vector& rkVector ) const
{
	return Vector(_mm_sub_ps(arr.m,rkVector.arr.m));

}

INLINE_FUNC bool Vector::operator== (const Vector& rvec) const
{
	return _mm_movemask_ps(_mm_cmpeq_ps(arr.m, rvec.arr.m)) == 0x0F;
}

//Quaternion
INLINE_FUNC Quaternion::Quaternion(const float* vec)
{
		arr.m = _mm_loadu_ps(vec);
}
INLINE_FUNC Quaternion::Quaternion(Real x, Real y, Real z, Real w)
{
			
			arr.m = _mm_set_ps(w,z,y,x);
}

INLINE_FUNC Quaternion::Quaternion(const double* vec)
{
		arr.m = _mm_set_ps((float)vec[3],(float)vec[2],(float)vec[1],(float)vec[0]);
}

INLINE_FUNC Quaternion::Quaternion(const __m128& data)
{
	arr.m = data;
}

inline Real& Quaternion::operator [] (int i )
{
      return arr.f[i];
}

inline const Real& Quaternion::operator [] (int i )const
{
      return arr.f[i];
}

INLINE_FUNC Quaternion& Quaternion::operator = ( const Quaternion& rkQuaternion )
{
	
	arr.m = rkQuaternion.arr.m;

    return *this;
}

INLINE_FUNC Quaternion::Quaternion() { arr.m = _mm_set_ps(1, 0, 0, 0); }

INLINE_FUNC Quaternion::Quaternion(const Vector& axis, Real angle) 
{
	float v = axis.length();
	if (v == 0)
	{
		arr.m = _mm_set_ps(1, 0, 0, 0);
		return;
	}
	__m128 d = _mm_set1_ps(1.f/v);
    
	float halfAngle = angle* 0.5f;
	__m128 s = _mm_mul_ps(_mm_set1_ps(sinf(halfAngle)),d);
		
    
	arr.m = _mm_mul_ps(axis.arr.m,s);
	arr.f[3] = cosf(halfAngle);

}

INLINE_FUNC Quaternion operator*(const Quaternion& q1, 
                                   const Quaternion& q2) 
{
	__m128 a1123 = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(q1.arr.m),_MM_SHUFFLE(1,1,2,3)));
    __m128 b1000 = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(q2.arr.m),_MM_SHUFFLE(1,0,0,0)));
	
	__m128 a2231 = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(q1.arr.m),_MM_SHUFFLE(2,2,3,1)));
    __m128 b2312 = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(q2.arr.m),_MM_SHUFFLE(2,3,1,2)));
    
	__m128 t1    = _mm_mul_ps(a1123, b1000);
	__m128 t2    = _mm_mul_ps(a2231, b2312);
		   t1    = _mm_xor_ps(t1,_mm_castsi128_ps(_mm_set_epi32 (0,0x80000000,0x00000000,0)));
   
	__m128 t12   = _mm_add_ps(t1, t2);
	__m128 a3212 = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(q1.arr.m),_MM_SHUFFLE(3,3,1,2)));
    __m128 t12m  = _mm_xor_ps(t12,_mm_castsi128_ps(_mm_set_epi32 (0x80000000,0,0,0x00000000)));
    __m128 b3331 = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(q2.arr.m),_MM_SHUFFLE(3,2,3,1)));

    __m128 a0000 = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(q1.arr.m),_MM_SHUFFLE(0,0,0,0)));
    __m128 t3    = _mm_mul_ps(a3212, b3331);
	t3  = _mm_xor_ps(t3,_mm_castsi128_ps(_mm_set_epi32 (0x00000000,0,0,0x80000000)));
    __m128 b0123 = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(q2.arr.m),_MM_SHUFFLE(0,1,2,3)));
	__m128 t0    = _mm_mul_ps(a0000, b0123);

		   t0    = _mm_xor_ps(t0,_mm_castsi128_ps(_mm_set_epi32 (0x80000000,0,0x80000000,0x00000000)));
    __m128 t03   = _mm_add_ps(t0, t3);
		
		
		return Quaternion(_mm_add_ps(t03, t12m));
}

INLINE_FUNC Vector Quaternion::operator* (const Vector& v) const
{
	// nVidia SDK implementation
	Vector uv, uuv;
	Vector qvec(arr.m);
	uv = qvec.cross(v);
	uuv = qvec.cross(uv);
	uv *= (2.0f * arr.f[3]);
	uuv *= 2.0f;

	return v + uv + uuv;
}


INLINE_FUNC bool Quaternion::operator== (const Quaternion& rvec) const
{
	return _mm_movemask_ps(_mm_cmpeq_ps(arr.m, rvec.arr.m)) == 0x0F;
}


INLINE_FUNC Quaternion Quaternion::inverse ()const
{    
	return Quaternion( _mm_xor_ps(arr.m,_mm_castsi128_ps(_mm_set_epi32 (0x00000000,0x80000000,0x80000000,0x80000000))));
 
}

INLINE_FUNC void Quaternion::normalize()
{	
	__m128 t0 = _mm_mul_ps(arr.m, arr.m);
	t0 = _mm_hadd_ps(t0, t0);
	t0 = _mm_hadd_ps(t0, t0);
	
	__m128 r = _mm_rsqrt_ps(t0);
	t0 = _mm_mul_ps(_mm_mul_ps(SIMD_HALF,r),_mm_sub_ps(SIMD_THREE,_mm_mul_ps(_mm_mul_ps(t0,r),r)));
	arr.m = _mm_mul_ps(arr.m,t0);
}


INLINE_FUNC RotMatrix Quaternion::toMatrixTrans() const
{
		
	DataType rows[4];
	__m128 quat2mat_x0 =_mm_castsi128_ps(_mm_set_epi32 (0x80000000,0x80000000,0x80000000,0x00000000)) ;
	__m128 quat2mat_x1 =_mm_castsi128_ps(_mm_set_epi32 (0x80000000,0x80000000,0,0x80000000));
	
	__m128 q2 = _mm_add_ps(arr.m,arr.m); //q2 = qx2 qy2 qz2 qw2

	//Multiplications
	__m128 q2shuffled = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(q2),_MM_SHUFFLE(2,2,1,1)));						//q2= qy2 qy2 qz2 qz2
	__m128 t1 = _mm_mul_ps( _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(arr.m),_MM_SHUFFLE(1,0,0,1))),q2shuffled);	//t1 = yy2 xy2 xz2 yz2
	__m128 t2  = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(arr.m),_MM_SHUFFLE(3,3,3,2)));							//t2 = z w w w
		   t2 = _mm_mul_ps(t2, _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(q2),_MM_SHUFFLE(0,1,2,2))));				//t2 = t2* (z2 z2 y2 x2) = zz2 wz2 wy2 wx2
	__m128 t3 = _mm_mul_ss(arr.m,q2);																						// t3 =  xx2 0 0 0

	// calculate the last elements of the third row 
	__m128 temp1 = _mm_sub_ss(_mm_sub_ss(_mm_set_ss(1),t3),t1);																// temp1 = 1-xx2 - yy2      

	// calcluate first row 
	__m128 temp2 = _mm_xor_ps(t1,quat2mat_x0);			//temp2 yy2,       -xy2,       -xz2,    -yz2 
	__m128 temp3 = _mm_xor_ps(t2,quat2mat_x1);			//temp3 -zz2,        wz2,       -wy2,    -wx2 
	temp3 = _mm_add_ss(temp3,_mm_set_ss(1));			//temp3 1-zz2       wz2,       -wy2,    -wx2		
	q2 = _mm_sub_ps(temp3,temp2);				        //q2  =  1-zz2-yy2,    xy2+wz2,    xz2-wy2, yz2-wx2 												           	 	
	rows[0].m = q2;
	rows[0].f[3] = 0;									//-yy2-zz2+1,    xy2+wz2,    xz2-wy2, 0

	// calculate second row 
	temp2 = _mm_move_ss(temp2,t3);																//temp2 = xx2,       -xy2,       -xz2,    -yz2 
	temp3 = _mm_xor_ps(temp3,quat2mat_x0);														//temp3 = -zz2+1,       -wz2,        wy2,     wx2 
	temp3 = _mm_sub_ps(temp3,temp2);															//temp3 = -xx2-zz2+1,    xy2-wz2,    xz2+wy2, yz2+wx2 
	t1 = _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(temp3),_MM_SHUFFLE(2,3,0,1)));		//t1 = xy2-wz2, -xx2-zz2+1,    yz2+wx2, xz2+wy2 
	rows[1].m = t1;
	rows[1].f[3] = 0;																			//xy2-wz2, -xx2-zz2+1,    yz2+wx2,     0

	// calculate third row
	temp2 = _mm_movehl_ps(q2,t1);					//temp2 yz2+wx2,    xz2+wy2,    xz2-wy2, yz2-wx2 
													//temp1 1 -xx2-yy2
												
	rows[2].m = _mm_shuffle_ps(temp2,temp1,_MM_SHUFFLE(1,0,3,1));
	rows[2].f[3] = 0;												//xz2+wy2,    yz2-wx2, -xx2-yy2+1,     0
	
	rows[3].m = _mm_setzero_ps();
	return RotMatrix(rows);
}

INLINE_FUNC Real* Quaternion::ptr() { return &arr.f[0]; }

INLINE_FUNC const Real* Quaternion::cptr() const { return &arr.f[0]; }
//RotMatrix
INLINE_FUNC RotMatrix::RotMatrix(Real xx, Real xy, Real xz, 
              Real yx, Real yy, Real yz, 
              Real zx, Real zy, Real zz) {
				  m_row[0].m = _mm_set_ps(0,xz,xy,xx);
				  m_row[1].m = _mm_set_ps(0,yz,yy,yx);
				  m_row[2].m = _mm_set_ps(0,zz,zy,zx);
				  m_row[3].m = _mm_set_ps(0,0,0,0);
}

INLINE_FUNC RotMatrix::RotMatrix(const Quaternion& q)
{
	
	(*this) = q.toMatrixTrans();
	_MM_TRANSPOSE4_PS(m_row[0].m,m_row[1].m,m_row[2].m,m_row[3].m);
}

INLINE_FUNC RotMatrix::RotMatrix(int axis, Real angle)
{
	Real cosine=cosf(angle);
	Real sine=sinf(angle);
	if (axis == 0)
	{
		m_row[0].m = _mm_set_ps(0, 0.f, 0.f, 1.0f);
		m_row[1].m = _mm_set_ps(0, -sine, cosine, 0.f);
		m_row[2].m = _mm_set_ps(0, cosine, sine, 0.f);
		m_row[3].m = _mm_set_ps(0, 0, 0, 0);
	}
	else if (axis == 1)
	{
		m_row[0].m = _mm_set_ps(0, sine, 0.f, cosine);
		m_row[1].m = _mm_set_ps(0, 0.f, 1.f, 0.f);
		m_row[2].m = _mm_set_ps(0, cosine, 0.f, -sine);
		m_row[3].m = _mm_set_ps(0, 0, 0, 0);
	}
	else if(axis == 2)
	{
		m_row[0].m = _mm_set_ps(0, 0.f, -sine, cosine);
		m_row[1].m = _mm_set_ps(0, 0.f, cosine, sine);
		m_row[2].m = _mm_set_ps(0, 1.f, 0.f, 0.f);
		m_row[3].m = _mm_set_ps(0, 0, 0, 0);
	}
	else
	{
		m_row[0].m = _mm_set_ps(0, 0, 0, 0);
		m_row[1].m = _mm_set_ps(0, 0, 0, 0);
		m_row[2].m = _mm_set_ps(0, 0, 0, 0);
		m_row[3].m = _mm_set_ps(0, 0, 0, 0);
	}
}

INLINE_FUNC RotMatrix::RotMatrix(const DataType* row)
{
	m_row[0].m = row[0].m;
	m_row[1].m = row[1].m;
	m_row[2].m = row[2].m;
	m_row[3].m = row[3].m;
}

//Done using scalar instructions because of the need
//to do a lot of shuffling (packing and unpacking which is expensive)
INLINE_FUNC Quaternion RotMatrix::toQuat() const {
    int 	next[3] = { 1, 2, 0 };

    Quaternion result;
   
    Real trace = m_row[0].f[0] + m_row[1].f[1] + m_row[2].f[2];
    
    if (trace > 0.0) 
    {
        Real s = sqrtf(trace + Real(1.f));
        result.arr.f[3] = s * Real(0.5f);
        s = Real(0.5) / s;
        
        result.arr.f[0] = (m_row[2].f[1] - m_row[1].f[2]) * s;
        result.arr.f[1] = (m_row[0].f[2] - m_row[2].f[0]) * s;
        result.arr.f[2] = (m_row[1].f[0] - m_row[0].f[1]) * s;
    } 
    else 
    {
        int i = 0;
        if (m_row[1].f[1] > m_row[0].f[0])
            i = 1;
        if (m_row[2].f[2] > m_row[i].f[i])
            i = 2;
        
        int j = next[i];  
        int k = next[j];
        
        Real s = sqrtf(m_row[i].f[i] - m_row[j].f[j] - m_row[k].f[k] + Real(1.f));
        
        result.arr.f[i] = s * Real(0.5);
        
        s = Real(0.5) / s;
        
        result.arr.f[3] = (m_row[k].f[j] - m_row[j].f[k]) * s;
        result.arr.f[j] = (m_row[j].f[i] + m_row[i].f[j]) * s;
        result.arr.f[k] = (m_row[k].f[i] + m_row[i].f[k]) * s;
    }
    return result;

}

INLINE_FUNC void RotMatrix::normalize()
{
	Quaternion r = toQuat();
	r.normalize();
	RotMatrix temp(r);
	m_row[0].m = temp.m_row[0].m;
	m_row[1].m = temp.m_row[1].m;
	m_row[2].m = temp.m_row[2].m;
}
inline Vector&  RotMatrix::operator[](int i)       { return *(Vector*)(&m_row[i].m); }
inline const Vector&  RotMatrix::operator[](int i)const       { return *(Vector*)(&m_row[i].m); }
INLINE_FUNC RotMatrix& RotMatrix::operator=(const RotMatrix& rmat)
{
	m_row[0].m=rmat.m_row[0].m;
	m_row[1].m=rmat.m_row[1].m;
	m_row[2].m=rmat.m_row[2].m;
	m_row[3].m=rmat.m_row[3].m;
	return *this;
}


INLINE_FUNC RotMatrix RotMatrix::operator* (const RotMatrix& rkMatrix)const
{
   RotMatrix kProd;

   __m128 t0 = _mm_load1_ps(&m_row[0].f[0]);
   //Row 0
   kProd.m_row[0].m = _mm_mul_ps(t0,rkMatrix.m_row[0].m);
   t0 = _mm_load1_ps(&m_row[0].f[1]);
   kProd.m_row[0].m = _mm_add_ps(kProd.m_row[0].m,_mm_mul_ps(t0,rkMatrix.m_row[1].m));
   t0 = _mm_load1_ps(&m_row[0].f[2]);
   kProd.m_row[0].m = _mm_add_ps(kProd.m_row[0].m,_mm_mul_ps(t0,rkMatrix.m_row[2].m));
   t0 = _mm_load1_ps(&m_row[0].f[3]);
   kProd.m_row[0].m = _mm_add_ps(kProd.m_row[0].m,_mm_mul_ps(t0,rkMatrix.m_row[3].m));
   //Row 1
   t0 = _mm_load1_ps(&m_row[1].f[0]);
   kProd.m_row[1].m = _mm_mul_ps(t0,rkMatrix.m_row[0].m);
   t0 = _mm_load1_ps(&m_row[1].f[1]);
   kProd.m_row[1].m = _mm_add_ps(kProd.m_row[1].m,_mm_mul_ps(t0,rkMatrix.m_row[1].m));
   t0 = _mm_load1_ps(&m_row[1].f[2]);
   kProd.m_row[1].m = _mm_add_ps(kProd.m_row[1].m,_mm_mul_ps(t0,rkMatrix.m_row[2].m));
   t0 = _mm_load1_ps(&m_row[1].f[3]);
   kProd.m_row[1].m = _mm_add_ps(kProd.m_row[1].m,_mm_mul_ps(t0,rkMatrix.m_row[3].m));

   //Row 2
   t0 = _mm_load1_ps(&m_row[2].f[0]);
   kProd.m_row[2].m = _mm_mul_ps(t0,rkMatrix.m_row[0].m);
   t0 = _mm_load1_ps(&m_row[2].f[1]);
   kProd.m_row[2].m = _mm_add_ps(kProd.m_row[2].m,_mm_mul_ps(t0,rkMatrix.m_row[1].m));
   t0 = _mm_load1_ps(&m_row[2].f[2]);
   kProd.m_row[2].m = _mm_add_ps(kProd.m_row[2].m,_mm_mul_ps(t0,rkMatrix.m_row[2].m));
   t0 = _mm_load1_ps(&m_row[2].f[3]);
   kProd.m_row[2].m = _mm_add_ps(kProd.m_row[2].m,_mm_mul_ps(t0,rkMatrix.m_row[3].m));
   //Row 3
   t0 = _mm_load1_ps(&m_row[3].f[0]);
   kProd.m_row[3].m = _mm_mul_ps(t0,rkMatrix.m_row[0].m);
   t0 = _mm_load1_ps(&m_row[3].f[1]);
   kProd.m_row[3].m = _mm_add_ps(kProd.m_row[3].m,_mm_mul_ps(t0,rkMatrix.m_row[1].m));
   t0 = _mm_load1_ps(&m_row[3].f[2]);
   kProd.m_row[3].m = _mm_add_ps(kProd.m_row[3].m,_mm_mul_ps(t0,rkMatrix.m_row[2].m));
   t0 = _mm_load1_ps(&m_row[3].f[3]);
   kProd.m_row[3].m = _mm_add_ps(kProd.m_row[3].m,_mm_mul_ps(t0,rkMatrix.m_row[3].m));

	/*for (int iRow = 0; iRow < 3; iRow++)
    {
            kProd[iRow][0] =
                m_row[iRow][0]*rkMatrix[0][0] +
                m_row[iRow][1]*rkMatrix[1][0] +
                m_row[iRow][2]*rkMatrix[2][0];

			kProd[iRow][1] =
                m_row[iRow][0]*rkMatrix[0][1] +
                m_row[iRow][1]*rkMatrix[1][1] +
                m_row[iRow][2]*rkMatrix[2][1];

			kProd[iRow][2] =
                m_row[iRow][0]*rkMatrix[0][2] +
                m_row[iRow][1]*rkMatrix[1][2] +
                m_row[iRow][2]*rkMatrix[2][2];
    }*/
	return kProd;
}


INLINE_FUNC RotMatrix& RotMatrix::operator*=(const RotMatrix& rkMatrix)
{
	DataType temp = m_row[0];
	DataType temp1 = m_row[1];
	DataType temp2 = m_row[2];
	DataType temp3 = m_row[3];

	m_row[0].m = _mm_mul_ps(_mm_set1_ps(temp.f[0]),rkMatrix.m_row[0].m);
  
	m_row[0].m = _mm_add_ps(m_row[0].m,_mm_mul_ps(_mm_set1_ps(temp.f[1]),rkMatrix.m_row[1].m));
    
	m_row[0].m = _mm_add_ps(m_row[0].m,_mm_mul_ps(_mm_set1_ps(temp.f[2]),rkMatrix.m_row[2].m));
     
	m_row[0].m = _mm_add_ps(m_row[0].m,_mm_mul_ps(_mm_set1_ps(temp.f[3]),rkMatrix.m_row[3].m));
   //Row 1
  
	m_row[1].m = _mm_mul_ps(_mm_set1_ps(temp1.f[0]),rkMatrix.m_row[0].m);
	m_row[1].m = _mm_add_ps(m_row[1].m,_mm_mul_ps(_mm_set1_ps(temp1.f[1]),rkMatrix.m_row[1].m));
	m_row[1].m = _mm_add_ps(m_row[1].m,_mm_mul_ps(_mm_set1_ps(temp1.f[2]),rkMatrix.m_row[2].m));
	m_row[1].m = _mm_add_ps(m_row[1].m,_mm_mul_ps(_mm_set1_ps(temp1.f[3]),rkMatrix.m_row[3].m));

   //Row 2
   m_row[2].m = _mm_mul_ps(_mm_set1_ps(temp2.f[0]),rkMatrix.m_row[0].m);
 
   m_row[2].m = _mm_add_ps(m_row[2].m,_mm_mul_ps(_mm_set1_ps(temp2.f[1]),rkMatrix.m_row[1].m));
 
   m_row[2].m = _mm_add_ps(m_row[2].m,_mm_mul_ps(_mm_set1_ps(temp2.f[2]),rkMatrix.m_row[2].m));

   m_row[2].m = _mm_add_ps(m_row[2].m,_mm_mul_ps(_mm_set1_ps(temp2.f[3]),rkMatrix.m_row[3].m));
   //Row 3
   //temp = m_row[3].m;
  m_row[3].m = _mm_mul_ps(_mm_set1_ps(temp3.f[0]),rkMatrix.m_row[0].m);
 m_row[3].m = _mm_add_ps(m_row[3].m,_mm_mul_ps(_mm_set1_ps(temp3.f[1]),rkMatrix.m_row[1].m));
   m_row[3].m = _mm_add_ps(m_row[3].m,_mm_mul_ps(_mm_set1_ps(temp3.f[2]),rkMatrix.m_row[2].m));
   m_row[3].m = _mm_add_ps(m_row[3].m,_mm_mul_ps(_mm_set1_ps(temp3.f[3]),rkMatrix.m_row[3].m));
   return *this;
}


INLINE_FUNC Vector RotMatrix::operator*(const Vector& v)const {
  //  return Vector(m_row[0].f.dot(v), m_row[1].dot(v), m_row[2].dot(v) );
	DataType t0,t1,t2,t3;
#if 1
	t0.m = _mm_mul_ps(m_row[0].m,v.arr.m); //t0 = a11*v1 a12*v2 a13*v3 a14*v4
	t0.m = _mm_hadd_ps(t0.m,t0.m);
	t0.m = _mm_hadd_ps(t0.m,t0.m);
	t1.m = _mm_mul_ps(m_row[1].m,v.arr.m);
	t1.m = _mm_hadd_ps(t1.m,t1.m);
	t1.m = _mm_hadd_ps(t1.m,t1.m);
	t2.m = _mm_mul_ps(m_row[2].m,v.arr.m);
	t2.m = _mm_hadd_ps(t2.m,t2.m);
	t2.m = _mm_hadd_ps(t2.m,t2.m);
	t3.m = _mm_mul_ps(m_row[3].m,v.arr.m);
	t3.m = _mm_hadd_ps(t3.m,t3.m);
	t3.m = _mm_hadd_ps(t3.m,t3.m);
#else
	t0.m = _mm_dp_ps(m_row[0].m,v.arr.m,0xFF);
	t1.m = _mm_dp_ps(m_row[1].m,v.arr.m,0xFF);
	t2.m = _mm_dp_ps(m_row[2].m,v.arr.m,0xFF);
	t3.m = _mm_dp_ps(m_row[3].m,v.arr.m,0xFF);
#endif

	return Vector(t0.f[0],t1.f[1], t2.f[2]);
}

INLINE_FUNC Vector RotMatrix::getColumn(int i) const {
	//return Vector(m_row[0][i], m_row[1][i], m_row[2][i]);
	return Vector(m_row[0].f[i],m_row[1].f[i],m_row[2].f[i]);
}

INLINE_FUNC RotMatrix RotMatrix::transposed() const {
  
	 
	DataType row[4];
	row[0] =m_row[0];
	row[1] =m_row[1];
	row[2] =m_row[2];
	row[3] =m_row[3];

	_MM_TRANSPOSE4_PS(row[0].m,row[1].m,row[2].m,row[3].m);

	return RotMatrix(row);
}

INLINE_FUNC void RotMatrix::setIdentity()
{
	m_row[0].m=_mm_setzero_ps();
	m_row[1].m=_mm_setzero_ps();
	m_row[2].m=_mm_setzero_ps();
	m_row[3].m=_mm_setzero_ps();
	m_row[0].m = _mm_set_ss(1);
	m_row[1].f[1] = 1;
	m_row[2].f[2] = 1;
}

//These two functions appear to be not used anywhere in the code

INLINE_FUNC RotMatrix MultTransposeLeft(const RotMatrix& m1, const RotMatrix& m2) {
	return m1.transposed()*m2;
}

INLINE_FUNC RotMatrix MultTransposeRight(const RotMatrix& m1, const RotMatrix& m2) {
	return m1*(m2.transposed());
}


}//namespace FIK




#endif

