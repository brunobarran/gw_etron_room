/* Copyright (C) 2006-2011, IKinema Ltd. All rights reserved.
 *
 * IKinema API Library; SDK distribution
 * 
 * This file is part of IKinema engine http://www.ikinema.com
 *
 * Your use and or redistribution of this software in source and / or binary form, with or without
 * modification, is subject to: 
 * (i) your ongoing acceptance of and compliance with the terms and conditions of
 * the IKinema License Agreement; and 
 *
 * (ii) your inclusion of this notice in any version of this software that you use 
 * or redistribute.
 *
 *
 * A copy of the IKinema License Agreement is available by contacting
 * IKinema Ltd., http://www.ikinema.com, support@ikinema.com
 *
 */

#ifndef IKSOLVER_HEADER_INCLUDED
#define IKSOLVER_HEADER_INCLUDED

#include "IKMath.h"
#include "IKMemory.h"
#include "IKSegment.h"
#include "IKTask.h"


#ifdef __CELLOS_LV2__
#include <cell/spurs.h>
#endif//__CELLOS_LV2__

#ifdef BUILD_DLL
#if defined( __WIN32__ ) || defined( _WIN32 )
	#define DLL_EXPORT __declspec( dllexport )
#else
	#pragma GCC visibility push(default)
#endif
#else
#define DLL_EXPORT
#endif

namespace FIK {

	/** \brief Structure holding a skeletal bone
		
		\struct <ImportBone> 
		ImportBone Structure is used to define a Bone/Segment to be imported into the Solver
	*/
	#pragma pack(push, 1)
	struct DLL_EXPORT ImportBone
	{		
		const char* name; /**< C-Style string holding the name of the bone */
		const char* parentname; /**< C-Style string holding the name of the parent bone in the skeletal heirarchy. If no parent (Root) set to nullptr */
		void* user_data;	/**< Void pointer to hold any user specific data */
		unsigned int projection_axis; /**< Segment projection axis used for limits calculation. It can take values 0 = x, 1 = y, 2 = z */ 
		unsigned int rotation_axis; /**< Limits rotation axis definition. It can take values 0=x, 1=y, 2=z */
		bool enforce_limit; /**< True or false. To enforce the limits on the imported bone.\n Solver will try to pull towards centre of the limits range */
		float rest_offset[3]; /**< Rest pose translation in parent (local) space. format: vector{x,y,z} */	
		float rest_orientation[4]; /**< Rest pose rotation in parent (local) space. format: quaternion {w,x,y,z}
								   \warning The accepted format here is different from FIK::Quaternion used in rest of the API.
									
									\remark This will be changed in a future release */
		float rotation_axis_vector[3]; /**< Unit vector to represent the rotation axis if not a prinple axis. \remark This will be removed for the public interface in future versions */
		float limits[4][2];/**< Rotation min - max limits range in x, y, z axes, and Roll. Values are given in degrees. */
		
		/** @name Constructors
		*  Different ImportBone constructors. 
		*/
		///@{
		/**	No specific settings are used for the bone. Only used for vector/array initialisation.*/
		ImportBone();
		/**	Copies Bone settings from other.*/
		ImportBone(const ImportBone& other);
		///@}
		/**
			Assignment operator.
		*/
		ImportBone& operator=(const ImportBone& other);
		
	};
	#pragma pack(pop)

	/**
		\brief Output value of the solver autotune algorithm

		\enum <AutoTune>
		Return value discribing the result of the Solver autotune algorithm
	
	*/
	namespace  AutoTune {
		enum Enum {
			SOLVED = 0, /*!< Solver is stable, no autotune performed, current solver settings can be used in production. */
			TUNED,		/*!< Solver P-Gain has been tuned. */
			FAILED		/*!< Solver tuning has failed. */
		};
	}

	class Solver;
	/** \class IKSolver
		
		\brief The IKinema full body solver class.

		This class is the main class in using the IKinema Solver. 
		It will be used to import the skeletal hierarchy of the figure to be solved and create the different
		IKTasks needed for the rig. 

		It containts the main methods need to use IKinema. General use case will be to first  create the solver,
		import the skeleton  and create the IKTasks using

		\code	
				char* error;
				IKSolver* solver = new IKSolver(nullptr, error);
				//Import the skeleton
				solver->ImportBone(&importbones[0], numberOfBones)
				IKPositionTask* posTask = solver->addPositionTask(nameofbone);
		\endcode
		Then at every frame you need to
			
		\code	
				posTask->setTarget(target);
				solver->solve(30);
		\endcode
		Pass the resulting pose to the rest of your animation pipeline using, by looping through all segments 
		and calling IKSegment::getQ() and IKSegment::getP()

		
		\remark The solver class is responsible for all memory allocation and deallocation of the IKTasks and IKSegments
		associated with it.

	*/
	class DLL_EXPORT IKSolver
	{
		public:
			/**
				\brief Default constructor of the IKinema solver.

				In case of licenseing error, ensure you delete the constructed solver as it is left in an undefined state.
				\param	licensePath Folder path that is used by the licensing system to search for valid license.
						To search current folder pass nullptr.
				\param	errorMessage String pointer to report license status to user code. 
						In case of licensing failure this parameter will point to string
						describing the error. If the licensing process is successful, the parameter will
						have NULL value.
			*/
										
			IKSolver(const char* licensePath=NULL, char **errorMessage=NULL );

			/** 
				\brief	Default destructor for IKinema solver class.

				The descructor will cleanly delete all created IKTask and IKSegment objects as part of deleting the IKSolver.

			*/
			~IKSolver();
			
			
			/** @name Input/Output
			*	Inputs and outputs of the IKSolver class.
			*/
			///@{

			/**
			\brief	Accessor to the IKSegment that represents the skeleton bone with the specified name.

			\param	name	C-Style string, Name of the skeleton bone.
			\returns	Pointer to IKSegment.
			*/

			IKSegment* getSegment(const char* name);

			/** 
				\brief	Imports a skeleton into IKSolver using array of ImportBone.
				
				\param	bones pointer to the first bone in the ImportBone array.
				\param	bonesCount	Number of bones that are stored in the ImportBone array.
			*/
			void importBones(const ImportBone* bones, unsigned int bonesCount);



			/**
				\brief	Accessor to the IKSegment that represents the skeleton bone with the specified index handle.
				
				Returns pointer to the IKSegment with the specified index handle. Note that the user code
				is responsible to provide valied index for this request to prevent memory issues, because 
				the method doesn't compare theprovided index value with the total number of segments for 
				performance reasons.

				\param handle	Index of the skeleton segment/bone.
				\returns	Pointer to IKSegment

			*/
			
			IKSegment* getSegmentByHandle(unsigned int handle);

			/**
				\brief	Accessor to skeleton root segment.

				Returns the root segment of the imported skeleton hierarchy.
				\remark	The root segment is detected after IKSolver::ImportBones is executed

				\returns	IKSegment pointer to the root Segment
			*/
			
			IKSegment* getRootSegment();

			/**
				Returns total number of imported segments in the skeleton.
				\returns unsigned int with the total number of segments in teh solver
			*/
			
			unsigned int numSegments();

			/**
				\brief	Creates a new IKPositionTask constraint for the specified bone name.

				Allocates new IKPositonTask for the specified bone name and adds it to the list of tasks.
				
				\param boneName C-style string. The position task will be assigned to the skeleton bone with the name specified by this parameter.
				\deprecated \param priority Set the priority of the task. Primary (high) priority is set by value = 0, 
											secondary (low) priority is set by value = 1.

				\returns IKPositionTask pointer to the newly created task or NULL if there's
				no segmentin the skeleton matching the bone name.
			*/

			IKPositionTask* addPositionTask(const char* boneName, unsigned int priority=0);


			/**
				\brief	Creates a new IKOrientationTask constraint for the specified bone name.

				Allocates new IKOrientationTask for the specified bone name and adds it to the list of tasks.
				
				\param boneName C-style string. The orientation task will be assigned to the skeleton bone with the name specified by this parameter.
				 \deprecated \param priority Set the priority of the task. Primary (high) priority is set by value = 0, 
											secondary (low) priority is set by value = 1.

				\returns IKOrientationTask pointer to the newly created task or NULL if there's
				no segment in the skeleton matching the bone name..
			*/
			
			IKOrientationTask* addOrientationTask(const char* boneName,unsigned int priority=0);
			
			/**
				\brief	Creates a new IKBalanceTask constraint for the specified bone name.

				Allocates new IKBalanceTask - only one possible for IKSolver instance. Subsequent calls to this
				method will change the properties of the balance task based on provided parameters.
				
				\param globalUp The world up axis for balance computation. To set axis X/Y/Z as "up" use respectively 0/1/2.
				\param withMoments Flag that sets the use of momentums in balance computation.
				\param withCgPosition Flag that sets balance computations with center of gravity.
				\deprecated \param priority Set the priority of the task. Primary (high) priority is set by value = 0, 
					   secondary (low) priority is set by value = 1.

				\remark IKBalance tasks can be in one state only, withMoments or withCgPosition.

				\returns IKBalanceTask pointer to the newly created task 
			*/
			
			IKBalanceTask* addBalanceTask(unsigned int globalUp, bool withMoments=true, bool withCgPosition=false,unsigned int priority=0);

			/**
				\brief Add/remove dependency between two tasks

				Creates a dependency between two task. 

				\remark This method is mainly used to create a dependency link between an orientation task and 
						a penetration task on the same bone. This will ensure that the bone is oriented so it matches
						the penetration plane orientation.

				\param task Pointer to IKTask (The dependent task)
				\param flag Add/remove dependency
				\param dependentOn Pointer to the Penetration IKPositionTask

			*/
			void setTaskDependent(IKTask* task, bool flag, IKTask* dependentOn=NULL);
		
			/**	Returns pointer the IKBalanceTask. If the task dosn't exists the return value is NULL.*/
			IKBalanceTask* getBalanceTask();

			/**
				\brief Delete the specific task from the solver.


				Removes the specified task from the tasks list and deallocates memory. 
				\remark you have to use	FIK::IKSolver::removeBalanceTask to remove the balance task.
			
				\param pTask	Pointer to the task that will be removed from IKSolver instance.
			*/
			void removeTask(IKTask* pTask); 

			/** Removes the balance task from IKSolver instance. */
			void removeBalanceTask();

			/** Returns total number of tasks that were created for the solver. */
			unsigned int numTasks();

			/**
				\brief Attaches user data to the IKSolver instance.

				This functions just holds a pointer to the memory location of specific user data.
				The data is not used or modified by the IKSolver in anyway.

				\param data	Pointer to the data that will be attached to the IKSolver instance.
			*/
			void setUserData(void* data);

			/**	Returns the user data that is attached to the IKSolver instance. */
			void* getUserData();

			/**
				\brief Determines error in finding the solution and exit strategy for solver.

				Small values result in better tracking of the IK constraint targets. 
				\remark You should set this value to 10e-5
			
				/param tolerance Specifies the tolerance error.
			*/
			void setSolutionTolerance(Real tolerance);

			/**	Returns the error that is used in finding the solution and exit strategy for solver. */
			Real getSolutionTolerance();
			///@}
			/**************************************SOLVING / SETUP*************************************/
			/** @name Solving/Setup
			*	Methods to setup the solver and perform the inverse kinematics solve.
			*/
			///@{
			/**
				\brief Solves the Inverse Kinematics givens the IKTasks constraint

				Solves inverse kinematics. Returns true if converged, false if maximum number
				of iterations was used. Also false return value can mean that a conflict in the
				joint space could not be reconciled. The primary tasks should always be satisfied
				with maxIterations >= 30, regardless of return value.\n Instability in the solver will
				result in a false return value

				\param maxIterations Maximum number of iterations that will be used to find solution for the inverse kinematics.
									 For real-time application (games) use 30.\n For Virtual Production applications use 150.

				\returns True if converged, false if maxIteration was reached.
			*/
			bool solve( unsigned int maxIterations=30);

			/**
				\brief Solves the Inverse Kinematics givens the IKTasks constraint and autotunes the solver when
					   changing the weights of the IKTasks

				Solves inverse kinematics. Returns true if converged, false if maximum number
				of iterations was used. Also false return value can mean that a conflict in the
				joint space could not be reconciled. The primary tasks should always be satisfied
				with maxIterations >= 30, regardless of return value.\n Instability in the solver will
				result in a false return value.

				This function should be called when during the setup stages where the number of tasks and their weights are being changed.
				Once, an IKinema Rig has been tuned, you can call  solve(unsigned int).

				\note Calling this function is recommended whenever a change to the number of tasks or task weights is done. 
					  This function will automatically tune the internal states of the solver to achieve a stable solve.
					  It is not recommended to call this function in Real Time applications. Only call it during solver setup stages.

				\param maxIterations Maximum number of iterations that will be used to find solution for the inverse kinematics.
						For real-time application (games) use 30.\n For Virtual Production applications use 150.

				\param Result The result of the autotune algorithm.

				

				\returns True if converged, false if maxIteration was reached, or AutoTuned has failed.
			*/

			bool solve(unsigned int maxIterations, AutoTune::Enum& Result);

			/**
				\brief Initialise IKSolver internal memory and states.

				Resize memory and reevaluate segment/task ids. The methodis called automatically 
				when there is a change in number of tasks, or segments.

				\returns True of setup successful, or false if no segments could be found.
			*/
			bool setup();
			
			/** 
				\brief This method will put the figure in rest pose and reset solver memory.
				
				Use this when deterministic results are required.
				\remark The solve will require more iterations to converge after calling this method.
			*/
			void resetSolver();

			/**	Force re-evaluation of parameters depending on figure scale.*/
			void recomputeScale();
			///@}
			/***********************************TRANSLATION SOLVING********************************/
			/** @name Translation solving
			*	These methods setup the solver to allow for root segment translation
			*/
			///@{
			/**
				\brief Enables/disables solving for root translation. 
				
				If root translation is disabled, the root segment will not change it's position to satisfy
				desired solver tasks.
				
				\param flag Flag that sets root translation enabled/disabled.
			*/
			void translateRoot(bool flag);

			/**	Returns whether the root translation is enabled/disabled.*/
			bool isTranslateRoot() const;

			/**	
				\brief Returns correct root segment translation in its parent's space, with any solver offsets removed
				
				\deprecated Use IKSegment:getGloablPos()  on the root segment instead.
			*/
			const FIK::Real* getTranslation()const;

			/**
				\brief Sets mobility value for root translation (for all three axes)
				
				this also effectively increases/decreases the distance the root translation can cover for a single solve.
			
				\param value Root translation gain.
			
			*/
			void setTranslationWeight(Real value);

			/**	Returns the root translation weight.*/
			Real getTranslationWeight() const;

			/**	
				Enables/disables solving for secondary-priority target for root position.
			
				\param flag	Root will tend to the target position set (offset from world origin in rest pose by default)
							if the passed value is "true". Root will ranslate to minimise position errors on the effectors
							if the passed value is "false".

				\remark Typically not used in real-time (Game) application
			*/
			void enableTargetTranslation(bool flag);

			/**	Returns whether solving for secondary-priority target is enabled/disabled for root position.*/
			bool isEnabledTargetTranslation() const;

			/**	
				Sets a secondary-priority target for root position (target must be enabled to have effect).
			
				\param x position along x-axis for the secondary position target of the root.
				\param y position along y-axis for the secondary position target of the root.
				\param z position along z-axis for the secondary position target of the root.
			*/
			void setTargetTranslation(Real x, Real y, Real z);

			/**	Returns vector representing secondary position target of the root bone. */
			const Vector& getTargetTranslation() const;

			///@}
			/********************************CG/BALANCE SOLVING************************************/
			/** @name center of Mass Solving
			*	These methods setup the solver for Center of Mass and balance solving.
			*/
			///@{
			/**
				\brief	Sets the total mass of the figure

				Changes the total mass of the figure (80 kg by default) and recomputes individual segment
				masses in proportion to their m_center (mass=2*mass/total_length*m_center.length()).
				Therefore, change segment center of mass positions after you have changed the total mass.
				
				\param mass new figure Mass
			*/
			
			void setFigureMass(Real mass);

			/**	Returns figure mass. */
			Real getFigureMass() const;

			/**
				\brief Sets the desired zero moment point (ZMP)


				point about which the figure is rotationally balanced with respect to	
				external forces. This automtically switches from using the default ZMP ( a target
				of a position task+support site). Call useDefaultZMP(true) to switch back.

				\param x position along x-axis for the desired zero moment point.
				\param y position along y-axis for the desired zero moment point.
				\param z position along z-axis for the desired zero moment point.
			*/
			void setZMP(Real x, Real y, Real z);

			/** Returns the zero moment point position of the skeleton.*/
			Vector getZMP() const;

			/**
				\brief Enables/disables searching for a position task that is a support site to define ZMP.

				Changing the value of the ZMP using setZMP(x,y,z) will automatically disable the use of the default ZMP.
				You need to call useDefaultZMP(true) to switch back.

				\param flag If the flag i set to false, user code must use FIK::IKSolver::setZMP. If the flag is
							set to true, the parent value will be always used.
			*/

			void useDefaultZMP(bool flag);

			/**
				Returns whether the solver has enabled/disabled searching for a position task that
				is support site to define ZMP.
			*/
			bool getUseDefaultZMP() const;
			///@}
			/******************************SOLVER CONFIGURATION***********************************/
			/** @name Solver Configuration
			*	General configuration for the IKinema IKSolver.
			*/
			///@{
			
			/** 
				\brief Sets the general precision/damping for all tasks.

				This will upgrade/downgrade the precision of all tasks. larger absolute values give less damping.

				\remark The precision value should be negative
				
				\param value New value for precision/dampting for tasks.
			*/
			void setPrecision(Real value);

			/**	Returns the value for precision/damping for tasks. */
			Real getPrecision() const;

			/**
				\brief Changes the effect of task weights.

				When enabled, the tasks weight values are rescaled and they are taken relative to each other.
				To weight each task
				on each own (e.g. to boost some but keep the others unaffected) set this flag to "false".
				\deprecated To be removed in the next release of the SDK.
				\param flag True to weight tasks relative to each other, false to weight them individually.
			*/
			
			void rescaleTasks(bool flag);

			/**	Returns if tasks are weighted on each onw or relative to each other.*/
			bool getRescaleTasks() const;

			/**
				\brief Scales influence of limits on the solution, usable range should be about 0.0-2.5.
			
				\remark When passing animation data to the solver (game applications), do not use limits.

				\param value New scale value for limits influence on solution.
			*/
			void setLimitsGain(Real value);

			/**	Returns the limits influence on solution.*/
			Real getLimitsGain() const;

			/**
				\brief Scales influence of target animation pose on the solution. 
			
				Usable range should be between 0.0-30.0. 
				When the character has high number of IK constraints, smaller values will give better response.
			
				\param value New scale value for target pose influence on solution.
			*/
			void setRetargetingGain(Real value);

			/** Returns the target pose influence on solution.*/
			Real getRetargetingGain() const;

			/** 
				\brief Enable/Disable ShadowPosing functionality.

				ShadowPosing is the ability to use the rest pose as animation target during the solve.
				This funcationality is used in Virtual Production applications, when doing markers or rigid body solving. It replaces the need to use limits.

				\param bFlag set to true to enable shadow posing, false will disable ShadowPosing, retargeting and sets the retargeting gain to the default value.

				\remark Calling IKSolver::EnableRetargeting() or IKSolver::setRetargetingGain() will overwrite ShadowPosing settings.

			*/

			void enableShadowPosing(bool bFlag);

			/**
				\brief Weights all secondary tasks against primary tasks.
				
				Global weight value for the second priority tasks. range about 0.0-5.0.
				\deprecated Will be removed when the task priority system is removed from the Solver in the next release.
				
				\param value New weight for secondary tasks against primary tasks.
			
			*/
			void setSecondaryTaskWeight(Real value);

			/**	Returns weight value for secondary tasks against primary tasks. */
			Real getSecondaryTaskWeight() const;

			/**
				\brief Weights all secondary tasks against limits
				
				range 0.0-1.5, precision 0.0 gives	worst case for a given secondary task weight.
				\deprecated Will be removed when the task priority system is removed from the Solver in the next release.
				
				\param value New weight for secondary tasks against limits.
			*/
			void setSecondaryTaskPrecision(Real value);

			/** Returns weight value for secondary tasks against limits.*/
			Real getSecondaryTaskPrecision() const;
			
			/**	
				\brief Set the balance task weight.
				
				Increases/decreases the weight of the balance of moments subtask, range about 0.0-100.0
			
				\param factor New weight for balance of moments subtask.
			*/
			void setMomentsWeight(Real factor);

			/** Returns weight value for balance of moments subtask.*/
			Real getMomentsWeight() const;

			/**
				Increases/decreases the precision of the moments balance task (less precision adds more damping).

				\param factor New value for precision of the moments balance task. */
			void setMomentsPrecision(Real factor);

			/**	Returns precision value of the moments balance task. */
			Real getMomentsPrecision()const;

			/**
				\brief changes the Pull gain for all tasks.

				If the solver results in an unstable solve, reduce the P value.
				
				\remark  Values larger than one will lead to NaN and INF in the solution.

				\param value New value for task pull.
			*/
			void setP(Real value);

			/** Returns the amount of task pull. */
			Real getP()const;

			/**
				\brief Enable the use of limits and retargeting together.

				Controls if retargeting and limits errors should be added together. 
				It is disabled by default and very rarely enabled.
	
				\param bFlag To add retargeting and limits errors set "true", otherwise set "false".
			*/
			void setAddErrors( bool bFlag );

			/** Returns the flag that controls if limits and target errors re added together. */
			bool getAddErrors()const;
			//@}
			/**************************************Various utilities********************************************/
			/** @name Various utility functions.
			*	General utility functions for the IKinema IKSolver.
			*/
			///@{
			
			/** 
				\brief Applies scale  to all positions and translations in segments and tasks.

				This is used when the skeleton imported during IKSolver initialisation has different 
				scale to the output skeleton in the rest of the pipeline. 
			
				\param scale New scale vaue for the figure.
			
			*/
			void scaleFigure(Real scale);

			/** 
				sets m_scale (solver parameter) (from upper layer IKinema)
				
				\warning Changing this will lead to instability in the solver. Do not Use.

				\deprecated To be removed in the next release.

			*/
			void setScale(float scale);

			/** 
				sets null motion (joint angles) targets from several motions up to index mocap_no 

				\deprecated To be removed in the next release. Use IKSegment::setTargetOrientation() instead. 
							User code blends the different animations and pass in the final pose

			*/
			void setQuatHat(Real*** quat_hat, int mocap_no);
			
			/** 
				sets null motion (joint angles) targets from several motions up to index mocap_no 

				\deprecated To be removed in the next release. Use IKSegment::setTranslation() instead. 
			*/
			void setXHat(Real** x_hat);

			/** 
				\brief Set blend weight of different animation source.

				sets Blend weights from values up to index no (for upper layer IKinema).

				\deprecated Animation blending is done in user code and only final result in passed here

			*/
			void setBlendingFactors(Real* values,int no);

			/**	
				\brief Update forward kinematics/apply last increment
				
				Call this function if you have by passed the solver in FK mode and used IKSegment::setOrientation or IKSegment::setTranslation.
				It requires root segment pointer to be initialised.
			*/
			void updateFK();
			
			/**
				\brief Calculate equivalent scale for each segment resulting from stretch offsets 
			
				\warning Changing this will lead to instability in the solver. Do not Use.

				\deprecated To be removed in the next release.

				\param rootParentScale Scale of the root segment/bone in the solver.
			*/
			void deriveSegmentScale(Real rootParentScale);
			
			/** 
				\brief Checks if the solver has modified the segments and FK needs updating.
			
				Checks if the solver has modified the segments and FK needs updating
				this does not consider any changes done directly on the segments bypassing the solver

				\returns True if the solver has performed the FK update after the last solve.
			*/
			bool doneFK();
			
			/**	Returns pointer to the center of gravity position vector. */
			const Real* getCenter();

			/**	Returns the zero moment point (reference point for all global positions when compiled for solving balance) */
			const Vector& getZMP();

			/**	Forces a call to setup() at the next call to solve().*/
			void needSetup(bool flag);

			/**	
				Forces recomputation of internal scale parameters based on total extension of all bones. 

				\remark only use when AutoTuning the solver. Normal use cased you do not need to call this function.
			
			*/
			void computeScale();
			
			/**	
				Returns scale (solver parameter,not linked to ratio of current size to normal size) 
			*/
			Real getScale();

			/**	Is solved after last solve call.*/
			bool isSolved();

			/** 
				\brief Print the solver settings to file.

				This is useful when experiencing issues in solver setup. 
				Please generate a settings dump and sent it to IKinema support 

				\param fileName string containing path to file to use for settings dump.
			*/
			void dumpSettings(const char* fileName);

			/**
				\brief Solver state debug dump. 

				This will record the internal IKinema solver state every time to file using a JSON format until stopDebugDump() is called.
				Recorded data includes
					* Solver settings
					* Input Animation data
					* Current solved pose
					* Task demands
				The output file can be sent to IKinema Support team for investigation

				\remark This function uses the standard I/O streams and it is only compiled in the solver for PC debug builds.

				\param fileName string containing path to file to use for settings dump
			*/
			
			void debugDump(const char* fileName);

			/**
				\brief Stop the solver dump

				This function should be called after collecting enough data where the problems are visible. 
			*/
			void stopDebugDump();
			
			/** 
				Compare pSolver to current IKSolver object 
			
				\param pSolver Pointer to Internal Solver implementation

			*/
			bool Equals(Solver *pSolver) const;
			//@}
		private:
			/** Internal solver implementation */
			Solver *m_pImpl;

		protected:
			//no protected members
	};

	/**
	  \ingroup PS3 Port
	  \brief Initialise the SPU solver and solve

	  \returns true if solved.
	*/
		
	bool DLL_EXPORT IKinemaInit(const char * StudioName, const char * ProjectName, const char * LicensePath, const char * hash);
}

#endif //IKSOLVER_HEADER_INCLUDED
