/* Copyright (C) 2006-2017, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema engine http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

/**
\brief Macro to enable SSE4.2 optimised maths in User code
*/

#ifndef IKMATH_HEADER_INCLUDED
#define IKMATH_HEADER_INCLUDED
#ifndef FIK_SSE
#define FIK_SSE 0
#endif
#if FIK_SSE
#include <pmmintrin.h>
#endif

#ifdef BUILD_DLL
#if defined( __WIN32__ ) || defined( _WIN32 )
#define DLL_EXPORT __declspec( dllexport )
#else
#pragma GCC visibility push(default)
#endif
#else
#define DLL_EXPORT
#endif

#define INLINE_MATH 1

#if INLINE_MATH
#define INLINE_FUNC inline
#else
#define INLINE_FUNC
#endif

namespace FIK {

	typedef float Real;

	#define FIK_PI (3.14159265358979323846f) /**< \brief Constant value of PI */
	#define RADS_PER_DEG (0.01745329251994329547f) /**< \brief Conversion factor from Degrees to Radians used in FIK::Radians() */

	#define FIK_EPSILON (1.0e-9f) /**< \brief Small number used for zero checking */

#if FIK_SSE
	/**
	\brief Union to guarantee 16 byte alignment of vector data

	*/
	union DataType
	{
		/** \brief 128bit float vector representing the SSE registers*/
		__m128 m;
		/** \brief float[4] array for direct access to the vector memory defined by m */
		Real f[4];
	};
#else
	struct DataType
	{
		Real f[4];
	};
#endif


	/**
	\brief Convert angle from Degrees to Radians

	\param x angle value in Degrees

	\returns the angle in radians, range from [-2*PI, 2*PI]
	*/
	Real Radians(Real x);

	/**
	\brief if x is zero.

	\param x value to check

	\returns true if x is less than FIK_EPSILON, false otherwise.
	*/
	bool fuzzyZero(Real x);

	/**
	\brief Clamp value between min and max

	\param value value to Clamp
	\param min, max minimum and maximum value in the range
	*/
	Real Clamp(Real value, Real min, Real max);



	class RotMatrix;
	class Quaternion;
	/**
	\brief  Optimised 3D Vector class

	A direction in 3D space represented as distances along the 3 orthogonal axes. Normally used to represent segment positions and target demands.

	This class is optimised for the SSE4 instruction set.
	*/
	class Vector
	{
	public:
		/**	\brief Default constructor, Vector not initialised. */
		Vector();
		/**
		\brief Construct from float[3] array
		\param vec pointer to a float[3] array, holding the vector values
		*/
		Vector(const float* vec);
		/**
		\brief Construct from double[3] array.

		Construct the vector from a double[3] array, explicily casting to FIK::Real type.

		\param vec pointer to a double[3] array, holding the vector values
		*/
		Vector(const double* vec);
		/**
		\brief construct using values x,y,z

		\param x,y,z distances along the 3 orthogonal axes.
		*/
		Vector(Real x, Real y, Real z);
#if FIK_SSE
		/**
		\brief Construct from SSE register.

		\param vec SSE register with the vector value.
		*/
		Vector(const __m128& vec);
#endif
		/**
		\brief perform the dot product

		Perform the dot/scalar product (This vector dot v).

		\param v the other vector of the dot product operator

		\returns FIK::Real representing the scalar results of the dot product
		*/
		Real dot(const Vector& v)const;

		/**
		\brief perform the cross product

		Perform the cross product (This vector cross v).

		\param v the other vector of the dot product operator

		\returns FIK::Vector perpendicular to the plane defined by both vectors,
		pointing in the direction defined by the right hand rule.
		*/

		Vector  cross(const Vector& v) const;
		/** \brief Returns the length of the vector.*/
		Real length() const;
		/** \brief Returns the 1/length of the vector */
		Real lengthInverse() const;
		/** \brief Normalizes the vector, so that it has a length of 1.

		\warning This function does not check if the vector is of zero length.
		*/
		void normalize();
		/**
		\brief Accessor for the ith vector elements

		\param i index of vector elements. Range 0 - 2

		\warning No checks are done internally to ensure that i is withing the correct range
		*/
		Real& operator[](int i);
		/**
		\brief Accessor for the ith vector elements

		\param i index of vector elements. Range 0 - 2
		\returns const reference to for the ith value
		\warning No checks are done internally to ensure that i is withing the correct range
		*/
		const Real& operator [] (int i)const;
		/**
		\brief Perform vector addition

		\param rvec vector to add to this vector

		\returns FIK::Vector equal to the sum of this vector and rvec
		*/
		Vector operator+(const Vector& rvec)const;
		/**
		\brief Perform vector substraction

		\param rvec vector to substract from this vector

		\returns FIK::Vector equal to the difference between this vector and rvec
		*/
		Vector operator-(const Vector& rvec)const;
		/**
		\brief Copies rvec into this vector.

		\param rvec vector to copy tothis vector
		*/
		Vector& operator=(const Vector& rvec);
		/**
		\brief Scales the vector by scalar factor.

		\param scalar  scalar representing the scaling factor
		\returns scaled copy of this vector.
		*/
		Vector operator*(Real scalar) const;
		/**
		\brief Scales the vector by scalar factor in place.

		\param scalar  scalar representing the scaling factor
		\returns reference to the scaled vector.
		*/
		Vector& operator*=(Real scalar);

		/**
		\brief Perform in place vector addition

		\param rvec vector to add to this vector

		\returns reference to this vector after the addition.
		*/
		Vector& operator +=(const Vector& rvec);

		/**
		\brief Compare two vectors for equality
		*/
		bool operator== (const Vector& rvec) const;
		/** \brief Pointer accessor for direct copying.*/
		inline Real* ptr() { return &arr.f[0]; }
		/** \brief Pointer accessor for direct copying.*/
		inline const Real* cptr() const { return &arr.f[0]; }
		/** \brief Create zero vector */
		static Vector ZERO() { return Vector(.0f, .0f, .0f); }
		friend RotMatrix;
		friend Quaternion;
	private:
		/** Vector data members */
		DataType arr;
	};

	/**
	\brief  Optimised quaternion implementation

	A unit quaternion represents a 3D rotation around an arbitary axis. The order of the quaternion elements is x,y,z, w
	\remark This order is different to FIK::importBone order.
	This class is optimised for the SSE4 instruction set.
	*/
	class Quaternion
	{
	public:
		/** \brief default constructor. Quaternion is not initialised and does not represent a rotation in this state. */
		Quaternion();
		/**
		\brief Construct the quaternion directly from the x,y,z and w of other quaternion.

		\param x,y,z vector elements of the Quaternion
		\param w scalar element to represent the rotation
		*/
		Quaternion(Real x, Real y, Real z, Real w);
		/**
		\brief Construct from float[4] array
		\param q pointer to a float[4] array, holding the quaternion values in x,y,z,w order
		*/
		Quaternion(const float* q);
		/**
		\brief Construct from double[4] array

		Construct the vector from a double[3] array, explicily casting to FIK::Real type.

		\param q pointer to a double[4] array, holding the quaternion values in x,y,z,w order
		*/
		Quaternion(const double* q);
		/**
		\brief Construct a quaternion from axis/angle values.

		\param axis Unit axis of rotation
		\param angle Angle of rotation in Radians.
		\remark You can use FIK::Radians() to convert degress to radians
		*/
		Quaternion(const Vector& axis, Real angle);
#if FIK_SSE
		/**
		\brief Construct from SSE register.

		\param vec SSE register with the quaternion value.
		*/
		Quaternion(const __m128& vec);
#endif
		/**
		\brief Accessor for the ith quaternion elements

		\param i index of quaternion elements. Range 0 - 3

		\warning No checks are done internally to ensure that i is withing the correct range
		*/
		Real& operator[](int i);
		/**
		\brief Accessor for the ith vector elements

		\param i index of vector elements. Range 0 - 2
		\returns const reference to for the ith value
		\warning No checks are done internally to ensure that i is withing the correct range
		*/
		const Real& operator[](int i) const;
		/**
		\brief Rotation of vector by a Quaternion
		\param v FIK::Vector to be rotated
		\returns Rotated vector.
		*/
		Vector operator* (const Vector& v) const;
		/**
		\brief Copy the quaternion.

		\param rquat quaternion to be copied to this quaternion.
		*/
		Quaternion& operator=(const Quaternion& rquat);
		
		/**
		\brief Compare two Quaternions for equality
		*/
		bool operator== (const Quaternion& rvec) const;
		/**
		\brief Normalise a quaternion.

		Ensures the quaternion has a unit length and it represents a valid rotation.
		*/
		void normalize();
		/**
		\brief Calculate the inverse rotation of this quaternion.

		\remark The quaternion must be normalised and representing a valid rotation before it can be inverted
		*/
		Quaternion inverse()const;//unit inverse
								  /** \brief Pointer accessor for direct copying.*/
		Real* ptr();
		/** \brief Pointer accessor for direct copying.*/
		const Real* cptr() const;
		/** \brief Create a quaternion representing identity rotation. */
		static Quaternion IDENTITY() { return Quaternion(0.0, 0.0, 0.0, 1.0); }
		/** \brief Convert the quaternion to the transpose of the equivelent rotation matrix.*/
		RotMatrix toMatrixTrans() const;
		friend RotMatrix;
		/**
		\brief Multiply two quaternions.

		The resulting quaternion will represent the accumulated rotation of both.
		\param q1 first rotation Quaternion.
		\param q2 second rotation quaternion.
		\returns FIK::Quaternion representing the result of q1*q2.
		*/
		friend Quaternion operator*(const Quaternion& q1, const Quaternion& q2);
	private:
		/** Vector data members */
		DataType arr;

	};

	/**
	\brief  Optimised implementation of 3x3 matrix which can represent a rotation around an axis. implementation

	This class is optimised for the SSE4 instruction set.
	*/
	class RotMatrix
	{
	public:
		/** \brief Default constructor. Matrix is not initialised */
		RotMatrix() {}
		/**
		\brief Construct the matrix from its individual elements
		\param xx,xy,xz first row of the matrix.
		\param yx,yy,yz second row of the matrix.
		\param zx,zy,zz third row of the matrix.
		*/
		RotMatrix(Real xx, Real xy, Real xz,
			Real yx, Real yy, Real yz,
			Real zx, Real zy, Real zz);
		/**
		\brief Construct the matrix from a quaternion
		\param quat FIK::Quaternion representing a valid rotation.
		*/
		RotMatrix(const Quaternion& quat);
		/**
		\brief Construct the matrix from an axis and angle of rotation
		\param axis The axis of rotation, can take values 0,1,2 for x,y,z respectively.
		\param angle Rotation angle in radians.
		*/
		RotMatrix(int axis, Real angle);
		/**
		\brief Accessor for the ith row of the matrix.

		\param i index of quaternion elements. Range 0 - 2
		\returns FIK::Vector with the ith row elements.
		\warning No checks are done internally to ensure that i is withing the correct range
		*/
		Vector& operator[](int i);
		/**
		\brief Accessor for the ith row of the matrix.

		\param i index of quaternion elements. Range 0 - 2
		\returns FIK::Vector with the ith row elements.
		\warning No checks are done internally to ensure that i is withing the correct range
		*/
		const Vector& operator[](int i) const;
		/**
		\brief Assignment operator.

		\param rmat matrix to copy to this matrix
		*/
		RotMatrix& operator=(const RotMatrix& rmat);
		/**
		\brief In place matrix multiplications

		\param rmat matrix to multiply this matrix with.
		\returns Reference to this matrix holding the result of this*rmat.
		*/
		RotMatrix& operator*=(const RotMatrix& rmat);
		/**
		\brief Multiply vector by matrix

		\param vec To multiply by this matrix.
		\returns FIK::Vector of the result.
		*/
		Vector operator*(const Vector& vec)const;
		/**
		\brief Matrix multiplications

		\param rmat matrix to multiply this matrix with.
		\returns Matrix holding the result of this*rmat.
		*/
		RotMatrix operator*(const RotMatrix& rmat)const;
		/**
		\brief Normalise the rotation matrix to avoid accumulated errors due to Floating point precision

		*/
		void normalize();

		/**
		\brief Set this matrix to identity, to represent 0 rotations
		*/
		void setIdentity();
		/** \brief convert this matrix to the equavilent quaternion */
		Quaternion toQuat()const;
		/**
		\brief Access the matrix column
		\param i index of the ith column.
		\returns FIK::Vector with the elements of the ith column.
		\warning No checks are done internally to ensure that i is withing the correct range
		*/
		Vector getColumn(int i)const;
		/**
		\brief Return the transpose of this matrix. Representing the inverse rotation */
		RotMatrix transposed()const;
		/**
		\brief Construct from SSE registers.

		\param row pointer to the SSE registers array.
		*/
		RotMatrix(const DataType* row);
	private:

		DataType m_row[4];
	};


	/**
	\brief Transformation matrix

	This represents a uniform transformation containing translation and rotation information.
	Rotation is represented by mRot and mRotQuat.
	*/
	class Transform
	{
	public:

		Transform(const Quaternion& Rot, const Vector& Trans) :mRotQuat(Rot), mTrans(Trans) {};

		Transform(const Quaternion& Rot) :mRotQuat(Rot), mTrans(0, 0, 0) {};

		Transform(const Vector& Trans) :mRotQuat(0, 0, 0, 1), mTrans(Trans) {};

		Transform() :mRotQuat(0, 0, 0, 1), mTrans(0, 0, 0) {};



		//Transform vector by Transfomr
		Vector operator*(const Vector& vec)
		{
			return (mRotQuat*vec + mTrans);
		}


		Transform operator*(const Transform& other) const
		{
			return mult_Transform(*this, other);
		}

		Transform operator*=(const Transform& other)
		{
			Transform result = mult_Transform(*this, other);
			return (*this);
		}

		bool operator==(const Transform& other) const
		{
			
			return (mTrans == other.mTrans) && (mRotQuat == other.mRotQuat);
		}
#if !FIK_SSE
		RotMatrix GetRotMatrix() const { return mRotQuat.toMatrixTrans().transposed(); }
		void SetRotMatrix(const RotMatrix& mat) { mRotQuat = mat.toQuat(); }
#else
		const RotMatrix& GetRotMatrix() const { return mRot; }
		void SetRotMatrix(const RotMatrix& mat) { mRot = mat; }
#endif
		const Quaternion& GetRotation() const { return mRotQuat; }

		const Vector& GetTranslation() const { return mTrans; }

		

		void SetRotation(const Quaternion& quat) { mRotQuat = quat; }

		void SetTranslation(const Vector& trans) { mTrans = trans; }

		void Inverse()
		{
			auto t = Inverse(*this);
			mTrans = t.mTrans;
			mRotQuat = t.mRotQuat;
		}

		Transform GetInverse() const
		{
			return Inverse(*this);
		}

	private:
		Transform Inverse(const Transform& p_in) const
		{
			Transform res;
			res.mRotQuat = p_in.mRotQuat.inverse();
			res.mTrans = res.mRotQuat * p_in.mTrans * -1;
			return res;
		}


		Transform mult_Transform(const Transform& lhs, const Transform& rhs) const
		{
			FIK::Transform result;
			result.mTrans = lhs.mTrans + lhs.mRotQuat * rhs.mTrans;
			result.mRotQuat = lhs.mRotQuat * rhs.mRotQuat;
			return result;
		}

	private:
		/** \brief Rotational part of the transformation matrix*/
		RotMatrix mRot;
		/** \brief Quaternion representation of the rotational part of the transformation matrix */
		Quaternion mRotQuat;
		/** \brief Translational part of the transformation matrix */
		Vector mTrans;



	};

	/**
	\brief Multiply the two quatenions together.
	\param q1, q2 quaternions to multiply.
	\returns FIK::Quaternion representing the combined rotation of q1 and q2.
	*/
	Quaternion operator*(const Quaternion& q1, const Quaternion& q2);
	/**
	\brief Multiply m1 transposed with m2.
	\param m1, m2 matrices to multiply.
	*/
	RotMatrix MultTransposeLeft(const RotMatrix& m1, const RotMatrix& m2);
	/**
	\brief Multiply m1  with m2 transposed.
	\param m1, m2 matrices to multiply.
	*/
	RotMatrix MultTransposeRight(const RotMatrix& m1, const RotMatrix& m2);


}//namespace FIK

#include "IKMath.inl"

 // IKMATH_HEADER_INCLUDED
#endif