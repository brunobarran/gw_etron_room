/* Copyright (C) 2006-2011, IKinema Ltd. All rights reserved.
 *
 * IKinema API Library; SDK distribution
 * 
 * This file is part of IKinema engine http://www.ikinema.com
 *
 * Your use and or redistribution of this software in source and / or binary form, with or without
 * modification, is subject to: 
 * (i) your ongoing acceptance of and compliance with the terms and conditions of
 * the IKinema License Agreement; and 
 *
 * (ii) your inclusion of this notice in any version of this software that you use 
 * or redistribute.
 *
 *
 * A copy of the IKinema License Agreement is available by contacting
 * IKinema Ltd., http://www.ikinema.com, support@ikinema.com
 *
 */

#ifndef IKTASK_HEADER_INCLUDED
#define IKTASK_HEADER_INCLUDED

#include "IKMath.h"
#include "IKMemory.h"
#include "IKSegment.h"

#ifdef BUILD_DLL
#if defined( __WIN32__ ) || defined( _WIN32 )
	#define DLL_EXPORT __declspec( dllexport )
#else
	#pragma GCC visibility push(default)
#endif
#else
#define DLL_EXPORT
#endif

namespace FIK {

	/** \class IKTask 
		
		\brief Generic IK constraint interface. All other task types inherit from this interface.
	*/
	class DLL_EXPORT IKTask
	{
		public:
			virtual ~IKTask() {};

			/** \brief Returns index of the first degree of freedom of this task based on position in the tasks list. */
			virtual unsigned int Id() const = 0;

			/** \brief	Returns handle index of the task. Its position in the IKSolver task list */
			virtual unsigned int TaskId() const = 0;

			/** \brief Returns the total number of DoFs for this tasks (includes active and inactive dofs). */
			virtual unsigned int Size() const = 0;
			
			/** \brief Returns number of active dofs of the target. */
			virtual short NoDofs()const = 0;
			
			/** 
				\brief Checks if Task is active. 
				
				Only active tasks influence the solution.
			*/
			virtual bool Active() const = 0;
			
			/**	
				\brief Returns weight of the task				
				
				The returned value is set with IKTask::setWeight, might not be the same as individual dof weights.
			*/
			virtual Real Weight() const	= 0;

			/** 
				\brief Set the depth of influence of the task.

				Defines the chain length towards the root. 
			
				\param length New lenght value towards root. A value of -1 will set the length all the way down to the root.

			*/
			virtual void setLength(unsigned int length) = 0;
			
			/**
				\brief Returns chain length/depth towards root
			*/
			virtual unsigned int getLength() const = 0;
			
			/** 
				\brief Set weights for all DoF, range about 0-100.
			
				\param weight: New weight for all task dofs.
			*/
			virtual void setWeight(Real weight) = 0;

			/** 
				\brief Sets weight of a task DoF, range about 0-100.
			
				\param axis The axis over which the dof will be modified. Valid values are 0/1/2 respectively for X/Y/Z.
				\param value New value for the axis degree of freedom weight.
			*/
			virtual void setDofWeight(unsigned int axis, Real value) = 0;

			/**
				\brief Returns the degree of freedom over specified axis.
			
				\param axis takenhe axis id for which the degree of freedom will be returned. Valid values are 0/1/2 respectively for X/Y/Z.
			*/
			virtual Real getDofWeight(unsigned int axis) const = 0;
			
			/** 
				\brief Set damping for all DoF,  less damping/greater precision for larger values.
			
				\param value New tolerance value for all task degrees of freedom.
			*/
			virtual void setPrecision(Real value) = 0;

			/**
				\brief Sets precision of a task dof
				
				\param axis	The axis over which the precision of a task dof will be modified. Valued values are 0/1/2 respectively for X/Y/Z.
				\param value New value for the axis precision of a task dof.
			*/
			virtual void setDofPrecision(unsigned int axis,Real value) = 0;

			/**
				\brief Returns the precision of task over specified axis.
			
				\param axis	The axis id for which the precision of task will be returned. Valid values are 0/1/2 respectively for X/Y/Z.
			*/
			virtual Real getDofPrecision(unsigned int axis) const = 0;

			/** 
				\brief Sets all dof priorities to the same value.
				
				\deprecated to be removed in the next release.
				
				\param priority	The new priority value for the task (0=primary, 1=secondary).
			*/
			virtual void setPriority(unsigned int priority) = 0;

			/**
				\brief Sets different priorities on each dof.

				\deprecated to be removed in the next release.

				\param x,y,z New priority values for every degree of freedom (0=primary, 1=secondary).
			*/
			virtual void setDofPriorities(unsigned int x, unsigned int y, unsigned int z) = 0;

			/**
				\brief Returns the dof priority of task over specified axis.
				
				\deprecated to be removed in the next release.

				\param axis Thes axis id for wich the dof priority will be returned. Valid values are 0/1/2 respectively for X/Y/Z.
			*/
			virtual unsigned int getDofPriority(unsigned int axis) const = 0;

			/** 
				\brief Returns number of DoFs at second priority 

				\deprecated to be removed in the next release.
			*/
			virtual unsigned int NoPriorities() const = 0;

			/**
				\brief Set task target from the passed array.

				The size of the array is determined by the concrete Task type. 
				The target is a 3 values vector for a position tasks, and 4 values
				quaternion in FIK::Quaternion order for an orientation task.

				\param arr Pointer to the array of the desired target.
			*/
			virtual void setTarget(const Real* arr) = 0;

			/**
				\brief Scale position target.
			
				\param scale New scale value for the task target.
			*/
			virtual void scaleTarget(Real scale) = 0;

			/** 
				\brief Disables/enables the task

				\param value To enable the task set this parameter "true", otherwise set as "false".
				\param resetDofs All task dofs will be set to the same value if this parameter is "true".
								 Set to false to keep previous settings. This is useful if task is frequently switched off/on
			*/
			virtual void setActive(bool value,bool resetDofs=true) = 0;

			/**
				\brief  Activate the specific DoF in global (Model) space.

				\param x,y,z New values for every degree of freedom for the task.

			*/
			virtual void setDofs(bool x, bool y, bool z) = 0;	

			/**
				\brief Returns if degree of freedom is enabled over specified axis.
			
				\param dof The degree of freedom enabled/disabled value will be returned for
						   the axis specified with this parameter.
			*/
			virtual bool isDofEnabled(unsigned int dof) const = 0;

			/** \brief	Returns true if the task is positional. */
			virtual bool Positional() const = 0;

			/**	Returns true if the task is orientational.*/
			virtual bool Orientational() const = 0;

			/**	
				\brief Checks if task has a force attached to it.

			*/
			virtual bool Kinetic() const = 0;

			/**	
				\brief Checks if task is at a support site
			*/
			virtual bool Supporting() const = 0;

			/** 
				\brief Checks if the task is a CoM task
			*/
			virtual bool Gravitational() const = 0;

			/**
				\brief Enables/disables using the end segment current global transform as task target.
			
				\deprecated To be removed in the next release.

				\param value If the parameter is "true", the method will serve to preserve the global transform of the bone
								in question while solving other tasks. Use if you are sure that all target orientations set on 
								segments will produce a reasonable target for the end in global frame.
				\param depth Value that sets depth towards root to which segments input should be switched between limits/pose.
			*/
			virtual void solveAnimation(bool value, int depth=0) = 0;

			/** 
				\brief Returns if the task is solving animation.

				\deprecated To be removed in the next release.
			*/
			virtual bool AnimationTask() const = 0;

			/**
				\brief Acces to the segment to which the task is attached.

				\returns IKSegment* to the segment affected by the tasks.
			*/
			virtual const IKSegment* getSegment() = 0;
			
			/**
				\brief Acces to the Solver implementation

				\deprecated To be removed in the next release.
			*/
			virtual void* GetSolver() const = 0;

			/**	
				\brief Attaches user data to the IKTask instance.
			
				\param data Pointer to the data that will be attached to the IKTask instance.
			*/
			virtual void setUserData(void* data) = 0;

			/**
				\brief Returns the user data that is attached to the IKTask instance.
			*/
			virtual void* getUserData() = 0;
			
			/**
				\brief returns if the task is being activated by another task

				This is mainly used for penetration tasks to align the bone along the surface. 
				For example. orientation task that is dependent on a position task with penetration option.
			*/
			virtual bool isDependent()const = 0;

			/** \brief returns dependent task of this (main) task, if any, otherwise NULL.*/
			virtual const IKTask* getDependentTask() const = 0;

			/** \brief returns dependent on task of this (dependent) task, if any, otherwise NULL. */
			virtual const IKTask* getDependentOnTask() const = 0;

			private:
				//no private memebers
			protected:
				//no protected members
	
	};
	/**
		\class IKPositionTask

		\brief Class defining a positional constraint.
		
		\relates IKBalanceTask
		It defines an IK position constraint in global (Model) space on a specific bone in the skeleton.
		The methods in this function can be called at every frame updating the parameters for the task.

	*/
	class DLL_EXPORT IKPositionTask : public IKTask
	{
		public:	
			virtual ~IKPositionTask(){}

			/** 
				\brief  Set target position for the task.
			
				\param vec New target position as FIK::Vector.
			*/
			virtual void setTarget(const Vector& vec) = 0;

			/** 
				\brief Set target position for the task.
				
				\param arr New target position assuming source is FIK::Real[3]={x,y,z}.
			*/
			virtual void setTarget(const Real* arr) = 0;

			/**
				\brief Set target position for the task.
				
				\param x,y,z New values for the target position.
			*/
			virtual void setTarget(Real x, Real y, Real z) = 0;

			/**
				\brief Returns the target of the task
				
				\remark useful to check on animation targets.

				\return Target position as FIK::Vector
			*/
			virtual const Vector& getTarget() const = 0;

			/**
				\brief Add an offset from the base of the end bone to its "tip". The offset is in end bone's local space.
			
				\param x,y,z New position for the tip offset.
			*/
			virtual void setTipOffset(Real x, Real y, Real z) = 0;
			
			/** 
				\brief Returns the tip offset for this task.
			*/
			virtual const Vector& getTipOffset() const = 0;
			
			/**
				\brief Sets force acting on the end effector (can be in addition to position target).
			
				\remark Useful when solving for Moments balance.

				\param x,y,z Force vector for this task.
			*/
			virtual void setForce(Real x, Real y, Real z) = 0;

			/** \brief Returns the force vector for this task.*/
			virtual const Vector& getForce() const = 0;
			
			/** 
				\brief	Enable/disable position target when solving balance.
				
				\remark When disabled the position demands will not be solved for. The solver will place the end effector in an optimal position minimising moments.

				\param flag Flag that enables/disables position target.
			*/
			virtual void enablePosition(bool flag) = 0;
			

			virtual void AdjustNormalFilter(float alpha) = 0;
			/** 
				\brief	Enable/disable external force influence when solving balance.
			
				\param flag Flag that enables/disables force for this task.
				\param supportSite Flag that signals that the force should be automatically computed to ensure static force balance on the figure.
			*/
			virtual void enableForce(bool flag, bool supportSite=false) = 0;

			/** 
				\brief	Sets desired weight ration at this suport site when solving balance.
				
				\param value New value for support site weight.
			*/
			virtual void setSupportRatio(Real value) = 0;

			/** \brief	Returns support site wight for this task.*/
			virtual Real getSupportRatio() const = 0;

			/** 
				\brief	Set by BalanceTask when it goes active/inactive.
				
				\param value New value for torque derivatives.
			*/
			virtual void computeTorqueDerivatives(bool value) = 0;

			/** \brief	Returns remaining position error after solving.*/
			virtual const Real* getDeltaPos() = 0;

			/** 
				\brief	Scales the target of this task.
				
				\param scale New scale value fro the task target.
			*/
			virtual void scaleTarget(Real scale) = 0;	

			/** \brief	Returns if the task is positional.*/
			virtual bool Positional() const = 0;

			/** \brief	Returns if theres force for this task.*/
			virtual bool Kinetic() const = 0;

			/** \brief	Returns if the task has supporting site enabled.*/
			virtual bool Supporting() const = 0;

			/** 
				\brief	Returns negated torque due to force at the tip segment.

				\return FIK::Vector with the negated segment torque
			*/
			virtual Vector getNTorque() = 0;
			
			/** 
				\brief  Set the task as pole vector task.
				
				if true, the task will only consider movement that is 
				out of the plane of this limb. The limb plane is defined by 
				(segment_base-parent_base) x (child_base - segment_base). This means the
				task segment needs to have a parent and a child.
			
				\param flag true enables this configuration, false switches back to normal task.
			*/
			virtual void setAsPoleTask(bool flag)= 0;

			/** \brief returns if the task is currently in pole task mode.*/
			virtual bool getAsPoleTask()const = 0;
			
			/** 
				\brief Sets the task as a penetration task only
				
				The position task is only active when the penetration plane is crossed. 
				
				The penetration plane position is set with IKPositionTask::setPPlanePosition(),
				and its normal is set with IKPositionTask::setPPlaneNormal().
			
				\param flag true to enable, false to disable.
			*/
			virtual void setAsPenetrationTask(bool flag) = 0;

			/** \brief getter for the "as penetration task" attribute*/
			virtual bool getAsPenetrationTask()const = 0;

			/** 
				\brief Sets a position and penetration task.
				
				The position task acts as normal task but prevents moving across
				the penetration plane.

				The penetration plane position is set with IKPositionTask::setPPlanePosition(),
				and its normal is set with IKPositionTask::setPPlaneNormal().
			
				\param flag true to enable, false to disable.
			*/
			virtual void setWithPenetrationTask(bool flag) = 0;

			/** \brief getter for the "with penetration task" attribute.*/
			virtual bool getWithPenetrationTask()const = 0;

			/** 
				\brief Set the penetration plane normal.
				
				The plane orientation is defined by giving its "up" normal
				in skeleton space.

				\param x,y,z elements of the normal vector (does not have to be normalized)
			*/
			virtual void setPPlaneNormal(Real x, Real y, Real z) = 0;

			/** \brief getter for the penetration plane normal that has been set.*/
			virtual const Vector& getPPlaneNormal() const = 0;

			/** 
				\brief defines the position of the plane in skeleton space..

				\param x,y,z elements of the position vector
			*/
			virtual void setPPlanePosition(Real x, Real y, Real z) = 0;

			/** \brief getter for the penetration plane position that has been set.*/
			virtual const Vector& getPPlanePosition() const = 0;

			virtual void addCollisionSegment(IKSegment* segment) = 0;

			virtual void SetMeshOffset(Real Offset) = 0;

			private:
				//no private memebers
			protected:
				//no protected members
	};

		/**
		\class IKOrientationTask

		\brief Class defining an orientation constraint.
		
		It defines an IK orientation constraint in global (Model) space on a specific bone in the skeleton.
		The methods in this function can be called at every frame updating the parameters for the task.

	*/
	class DLL_EXPORT IKOrientationTask : public IKTask
	{
		public:
			virtual ~IKOrientationTask(){}

			/** 
				\brief	Set target orientation for the task.
			
				\param quat New target orientation as FIK::Quaternion.
			*/
			virtual void setTarget(const Quaternion& quat)= 0;

			/** 
				\brief	Set target orientation for the task.
				
				\param arr New target orientation assuming source is FIK::Real[4]={x,y,z,w}.
			*/
			virtual void setTarget(const Real* arr)= 0;

			/** 
				\brief	Set target orientation for the task.
				
				\param x,y,z,w New values for the target orientation.
			*/
			virtual void setTarget(Real x, Real y, Real z, Real w)= 0;

			/** 
				\brief	Set target orientation for the task.
			
				\param mat New target orientation as FIK::RotMatrix.
			*/
			virtual void setTarget(const RotMatrix& mat)= 0;

			/** \brief	Returns if the task is orientational.*/
			virtual bool Orientational() const = 0;

			/** \brief	Returns the target orientation as FIK::Quaternion of this task.*/
			virtual const Quaternion& getTarget() const = 0;

			/** 
				\brief	Define local-space axis to be pointed at the look-at target.
			
				\param x,y,z New look-at axis.
			*/
			virtual void setLookAtAxis(Real x, Real y, Real z) = 0;
			
			/** \brief	Returns the look-at axis for this task.*/
			virtual const Vector& getLookAtAxis() const = 0;

			/** 
				\brief Sets the look-at target for the task

				Set global (Model) space direction in which the look-at axis is to be pointed.
				This will  update the orientation target if the task is in look-at mode.
			
				\param x,y,z New look-at direction.
			*/
			virtual void setLookAtDirection(Real x, Real y, Real z) = 0;
			
			/** \brief	Returns the current look-at direction for this task.*/
			virtual const Vector& getLookAtDirection() const = 0;

			/** 
				\brief	Changes the look-at status of the task.			
			
				\param flag start/stop look-at mode
			*/
			virtual void setLookAt(bool flag) = 0;
			
			/** \brief	Returns if the task is in look-at mode.*/
			virtual bool isLookAt() const = 0;
		
			private:
				//no private memebers
			protected:
				//no protected members

	};

			/**
		\class IKBalanceTask

		\brief Class defining the balance task.
		
		\relates IKPositionTask
		\relates IKSegment

		The balance task can be either:
			- Balance of Moment:
				In this case the solver will result in a pose that ensures 
				the balance of the state forces acting on the skeleton.
				These forces include the weight (support) forces acting at
				the IKPositionTasks defined as supporting. The weight ratio 
				between the different support tasks is set using IKPositionTask::setSupportRatio().
				Any other external forces are set using IKPositionTask::setForce()
		
			- Center of Mass Task:
				In this case the solver will result in a pose that satisfies the
				demanded position for the CoM. The position can be set using IKBalanceTask::setTarget()

		By default the solver will assume a total figure mass of 80Kg, and this mass is distributed along
		the different segments/bones based on the length of the bone. The individual segment masses can be 
		changed using IKSegment::setMass().

	*/
	class DLL_EXPORT IKBalanceTask : public IKTask
	{
		public:
			virtual ~IKBalanceTask(){}

			/** 
				\brief	Sets target position for the centre of mass.
				
				\param x,y,z New values for the target position for center of mass.*/
			virtual void setTarget(Real x, Real y, Real z) = 0;

			/** 
				\brief	Set target position for the centre of mass.
			
				\param vec New target position assuming source is FIK::Real[3]={x,y,z}.*/
			virtual void setTarget(const Real* vec) = 0;

			/**
				\brief	Add a user offset to an internally generated animation target. Vectors are added in global space.
			
				\brief x,y,z New values for user offset.
			*/
			virtual void addToTarget(Real x, Real y, Real z) = 0;
			
			/** \brief	Returns the target of the balance task (useful to check on animation targets).*/
			virtual const Vector& getTarget() const = 0;
			
			/** 
				\brief	Returs the total net torque acting on the figure
				
				Calculates the current torque on the figure from the weight force + external forces acting on IKPositionTask force tasks 
				
				\return FIK::Vector with the torque value in global (model) space.
			*/
			virtual Vector getTorque() = 0;

			/** 
				\brief	Enables solving for CoG target, will set task active for value "true".
			
				\param flag Flag that enables or disables solving for center of gravity.
			*/
			virtual void enableCGPosition(bool flag) = 0;

			/** 
				\brief	Enables relative center of gravity computation.
			
				\param flag The target is taken relative the current root position if the flag is "true".
							The target is taken relative global origin if the flag is "false".
			*/
			virtual void setCGRelativeRoot(bool flag) = 0;

			/** 
				\brief	Enables solving for static equilibrium of moments, will set task active for value "true".
			
				\param flag Flag that enables/disables solving for static equilibrium moments.
			*/
			virtual void enableMoments(bool flag) = 0;
			
			/** \brief	Returns if the task is computing center of gravity.*/
			virtual bool Gravitational() const = 0;

			/** 
				\brief	Disables/enables balance task.
			
				\param value To enable the task set this parameter "true", otherwise set as "false".
				\param resetDofs All task dofs will be set to the same value if this parameter is "true". Previously
								 set dof flags will be kept (useful if a task is frequently switched off/on) if the
			 					 parameter is "false".
			*/
			virtual void setActive(bool value,bool resetDofs=true) = 0;

			/** 
				\brief	Sets priority for all dofs of the moments balance subtask.
			
				\param pr New priority value for degrees of freedom.
			*/
			virtual void setMomentsPriority(unsigned int pr) = 0;

			/** \brief	Returns moments priority value.*/
			virtual unsigned int getMomentsPriority() const = 0;	
			
			/** 
				\brief	Sets the global up vector as a unit vector along the specified axis.
			
				\param axis New global up axis for balance computation. Valid values are 0/1/2.
			*/
			virtual void setGlobalUp(unsigned int axis) = 0;

			/** 
				\brief	Sets the global up vector transformed into solver space, should not be a zero vector (will be normalized)
			
				\param x,y,z New values for global up vector into solver space.
			*/
			virtual void setGlobalUp(Real x, Real y, Real z) = 0;

			/** \brief	Returns global up vector.*/
			virtual const Vector& getGlobalUp() const = 0;

			/** \brief	Returns if momentums are enabled in balance computations.*/
			virtual bool getMomentsEnabled() const = 0;

			/** \brief	Returns if center of gravity is enabled in balance computations.*/
			virtual bool getCoGEnabled() const = 0;

			/** \brief	Returns if the task is solving animation.*/
			virtual bool AnimationTask() const = 0;
		

			
			private:
				//no private memebers
			protected:
				//no protected members
	};

}//namespace FIK

#endif // IKTASK_HEADER_INCLUDED
