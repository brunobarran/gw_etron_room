﻿
using System;
using System.IO;
using UnrealBuildTool;
using System.Security.Cryptography;
using System.Text;

public class IKinema : ModuleRules
{

    private string ModulePath
    {
        get { return ModuleDirectory; }
    }

    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "")); }
    }

    public IKinema(ReadOnlyTargetRules Target) : base(Target)
    {
		Type = ModuleType.External;
       
		if (Target.Platform == UnrealTargetPlatform.Win32 )
		{
			return;
		}

		// Path to the IKinema root directory
		string IKinemaRootDir = ThirdPartyPath + '/';
		
    
		//
		// End of the configuration section
		//

		// Convert \ to / to avoid escaping problems
		IKinemaRootDir = IKinemaRootDir.Replace('\\', '/');

		string IKinemaLibDir = IKinemaRootDir + "sdk/lib/";
        string IKinemaIncludeDir = IKinemaRootDir;
		string IKinemaPlatform = "";
		string UEPlatform = "";
        string IKinemaLibExtension = "";
		switch (Target.Platform)
		{
			case UnrealTargetPlatform.Win64:
                
				IKinemaPlatform = "Win64";
				UEPlatform = "Win64";
                IKinemaLibExtension = ".lib";
                IKinemaIncludeDir += "sdk/include";
				break;
		}

		// Final IKinema lib path
		IKinemaLibDir += IKinemaPlatform;

		// IKinema library name.
		string IKinemaLibName = "IKinema";
		IKinemaLibName += IKinemaLibExtension;
       
		// IKinema defines
		PublicDefinitions.Add("WITH_IKINEMA_LIBS=1");
        PublicDefinitions.Add("FIK_SSE=1");
        PublicDefinitions.Add("MS_IKINEMA_" + UEPlatform);

		// IKinema include paths
        PublicIncludePaths.Add(IKinemaIncludeDir);
        
		// IKinema library path
		PublicLibraryPaths.Add(IKinemaLibDir);
       
	   // statically link with IKinema lib
        if (Target.Platform == UnrealTargetPlatform.IOS || Target.Platform == UnrealTargetPlatform.Android)
        {
            IKinemaLibName = Path.Combine(IKinemaLibDir, "lib" + IKinemaLibName);
            PublicAdditionalShadowFiles.Add(IKinemaLibName);
        }
        PublicAdditionalLibraries.Add(IKinemaLibName);

        string engDir = Directory.GetParent(Directory.GetCurrentDirectory()).FullName; //Get base directory compatible with System.IO
        string IKinemaBinariesDir = String.Format("$(EngineDir)/Binaries/ThirdParty/IKinema/License/");
        string IKinemaLicenseDir = IKinemaRootDir + "sdk/license";
		if (File.Exists(engDir+"/Binaries/ThirdParty/IKinema/License/a0.lic"))
        {
            RuntimeDependencies.Add(IKinemaBinariesDir + ("a0.lic"));
        }
        RuntimeDependencies.Add(IKinemaBinariesDir + ("ikey"));

        PublicDelayLoadDLLs.Add("IKinema.dll");
        {
            string FileName = IKinemaLibDir + "/IKinema.dll";
            RuntimeDependencies.Add(FileName, StagedFileType.NonUFS);
            
        }
        Console.WriteLine(IKinemaBinariesDir);
        Console.WriteLine(IKinemaLicenseDir);


    }
}
