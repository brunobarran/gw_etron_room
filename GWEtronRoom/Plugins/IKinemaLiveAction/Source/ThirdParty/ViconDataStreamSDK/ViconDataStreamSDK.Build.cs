
using System;
using System.IO;
using UnrealBuildTool;
using System.Security.Cryptography;
using System.Text;

public class ViconDataStreamSDK : ModuleRules
{

    private string ModulePath
    {
        get { return ModuleDirectory; }
    }

    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "")); }
    }

    public ViconDataStreamSDK(ReadOnlyTargetRules Target) : base(Target)
    {
        Type = ModuleType.External;
        if ((Target.Platform == UnrealTargetPlatform.Win64))
        {
   
            
            string LibrariesPath = Path.Combine(ThirdPartyPath, "DataStreamSDK/");
            PublicLibraryPaths.Add(LibrariesPath);
            Console.WriteLine(LibrariesPath);
            PublicAdditionalLibraries.Add("ViconDataStreamSDK_CPP.lib");
           // Include path
            PublicIncludePaths.Add(Path.Combine(ThirdPartyPath, "DataStreamSDK"));
            PublicDelayLoadDLLs.Add("ViconDataStreamSDK_CPP.dll");
            {
                string FileName = LibrariesPath + "ViconDataStreamSDK_CPP.dll";
                RuntimeDependencies.Add(FileName, StagedFileType.NonUFS);
                RuntimeDependencies.Add(LibrariesPath + "boost_thread-vc90-mt-1_48.dll", StagedFileType.NonUFS);
            }
        }
    }
}