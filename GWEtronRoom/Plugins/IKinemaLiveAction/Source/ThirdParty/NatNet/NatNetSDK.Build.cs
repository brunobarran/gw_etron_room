
using System;
using System.IO;
using UnrealBuildTool;
using System.Security.Cryptography;
using System.Text;

public class NatNetSDK : ModuleRules
{

    private string ModulePath
    {
        get { return ModuleDirectory; }
    }

    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "")); }
    }

    public NatNetSDK(ReadOnlyTargetRules Target) : base(Target)
    {
        Type = ModuleType.External;
        if ((Target.Platform == UnrealTargetPlatform.Win64))
        {
            string LibrariesPath = Path.Combine(ThirdPartyPath, "NatNetSDK/lib/x64/");
            PublicLibraryPaths.Add(LibrariesPath);
            Console.WriteLine(LibrariesPath);
            PublicAdditionalLibraries.Add("NatNetLib.lib");
           // Include path
            PublicIncludePaths.Add(Path.Combine(ThirdPartyPath, "NatNetSDK/include"));

            PublicDelayLoadDLLs.Add("NatNetLib.dll");
            {
                string FileName = LibrariesPath + "NatNetLib.dll";
                RuntimeDependencies.Add(FileName, StagedFileType.NonUFS);
                
            }


        }
    }
}