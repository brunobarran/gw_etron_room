/* Copyright 2009-2017 IKinema, Ltd. All Rights Reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema RunTime project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/
#include "IKinemaLAAnimInstance.h"
#include "LADataStreamCorePrivatePCH.h"

FIKinemaLAAnimInstanceProxy::FIKinemaLAAnimInstanceProxy()
	:FAnimInstanceProxy()

{}

FIKinemaLAAnimInstanceProxy::FIKinemaLAAnimInstanceProxy(UAnimInstance* Instance)
	: FAnimInstanceProxy(Instance)
{
	
}

void FIKinemaLAAnimInstanceProxy::SetTimeFromPose(const FLAGenericTimeCode& InTimeCode)
{
	savedTimeProxy = InTimeCode;
}

void FIKinemaLAAnimInstanceProxy::GetTimeFromPose(FLABlueprintTimeCode& timestep)
{
	timestep.minutes = (int)savedTimeProxy.minutes;
	timestep.hours = (int)savedTimeProxy.hours;
	timestep.seconds = (int)savedTimeProxy.seconds;
	timestep.frame = (int)savedTimeProxy.frame;
}


FAnimInstanceProxy* UIKinemaLAAnimInstance::CreateAnimInstanceProxy()
{
	return new FIKinemaLAAnimInstanceProxy(this);
}


FLABlueprintTimeCode UIKinemaLAAnimInstance::GetStreamedTimeCode()
{
	GetProxyOnAnyThread<FIKinemaLAAnimInstanceProxy>().GetTimeFromPose(savedTime);

	return savedTime;
}