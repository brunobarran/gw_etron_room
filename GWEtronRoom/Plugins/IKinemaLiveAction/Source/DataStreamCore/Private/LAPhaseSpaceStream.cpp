/* Copyright (C) 2006-2017, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "LAPhaseSpaceStream.h"
#include "LADataStreamCorePrivatePCH.h"

TMap<FString, ULAPhaseSpaceStream*> ULAPhaseSpaceStream::Streams;

ULAPhaseSpaceStream::ULAPhaseSpaceStream(const FObjectInitializer& PCIP)
	: Super(PCIP), isStarted(false), connected(false), markers(nullptr)
{
	portNumber = 5555;
}


ELAResult ULAPhaseSpaceStream::Connect(FString server_ip, int _portNumber)
{

	if (IsValidLowLevel())
	{
		if (isConnected())
		{
			owlDone();
		}
		sServerIP = server_ip;
		portNumber = _portNumber;
		TStringConversion<TStringConvert <TCHAR, ANSICHAR>> a(*server_ip);

		FString server = sServerIP + ":" + FString::FromInt(portNumber);
		int ret = owlInit(TCHAR_TO_ANSI(*server), OWL_SLAVE);
		if (ret < 0)
		{
			connected = false;
			return ELAResult::ELAError;
		}

		int tracker = 0;
		owlTrackeri(tracker, OWL_CREATE, OWL_POINT_TRACKER);

		// set markers
		for (int i = 0; i < MarkersNumber; i++)
			owlMarkeri(MARKER(tracker, i), OWL_SET_LED, i);

		// activate tracker
		owlTracker(tracker, OWL_ENABLE);

		// flush requests and check for errors
		if (!owlGetStatus())
		{
			int error = owlGetError();
			UE_LOG(LogLADataStreamCore, Error, TEXT("error in point tracker setup %i"), error);
		}

		// set default frequency
		owlSetFloat(OWL_FREQUENCY, OWL_MAX_FREQUENCY);

		//Create memory for markers,
		if (markers == nullptr)
			markers = new OWLMarker[MarkersNumber];

		FMarkerData v;//(0,0,0);
		v.blendFactor = 0.f;
		v.isMissing = false;
		AMarkers.Init(v, MarkersNumber);

		connected = true;
		return ELAResult::ELASuccess;
	}
	return ELAResult::ELAError;
}


ULAPhaseSpaceStream* ULAPhaseSpaceStream::Get(const FString serverIP, int MarkersNumber)
{
	auto stream = Streams.Find(serverIP);
	if (stream)
	{
		if ((*stream)->IsValidLowLevel())
		{
			(*stream)->RefCount++;
			return *stream;
		}
		else
		{
			Streams.Remove(serverIP);
		}
	}
	auto newStream = NewObject<ULAPhaseSpaceStream>();
	newStream->MarkersNumber = MarkersNumber;
	newStream->RemoveFromRoot();
	newStream->RefCount = 1;
	Streams.Add(serverIP);
	Streams[serverIP] = newStream;

	return newStream;

}


ELAResult ULAPhaseSpaceStream::Disconnect()
{
	if (isConnected())
	{
		owlDone();
	}
	return ELAResult::ELASuccess;
}


// Disconnects the DS-SDK and sets the object to be Garbage-Collected if RefCount <= 0
void ULAPhaseSpaceStream::Destory()
{
	if (this->IsValidLowLevel())
	{
		RefCount--;
		if (RefCount <= 0)
		{
			Disconnect();
			if (markers)
			{
				delete[](markers);
				markers = nullptr;
			}
			ConditionalBeginDestroy(); //instantly clears UObject out of memory
			RemoveFromRoot();
		}

	}
}


void ULAPhaseSpaceStream::BeginDestroy()
{
	RefCount--;
	if (RefCount <= 0)
	{
		Streams.Remove(sServerIP);
		Disconnect();
		Super::BeginDestroy();
	}
	Super::BeginDestroy();
}

void ULAPhaseSpaceStream::FinishDestroy()
{
	if (RefCount <= 0)
	{
		Super::FinishDestroy();
	}
	Super::FinishDestroy();
}


float ULAPhaseSpaceStream::calculateAlpha(float freq)
{
	float rc = 1 / float(freq * 2 * PI);
	float DeltaTime = GWorld->DeltaTimeSeconds;

	if (DeltaTime == 0)
		return 0.5;
	else
		return DeltaTime / (rc + DeltaTime);
}

ELAResult ULAPhaseSpaceStream::GetFrame()
{


	//Start streaming if this is the first call to Evaluate

	if (!isStarted)
	{
		owlSetInteger(OWL_STREAMING, OWL_ENABLE);
		if ((owlGetError()) == OWL_INVALID_OPERATION)
		{
			Connect(sServerIP, portNumber);
			connected = true;
			return ELAResult::ELAError;
		}

		isStarted = true;
	}


	int readMarkers = owlGetMarkers(markers, MarkersNumber);
	//Check for any errors
	if ((owlGetError()) != OWL_NO_ERROR)
	{
		//Report Error
		UE_LOG(LogLADataStreamCore, Error, TEXT("PhaseSpace reading errors"));
		connected = false;
		owlDone();
		return ELAResult::ELAError;
	}

	// no data yet
	if (readMarkers == 0)
	{
		return ELAResult::ELAError;
	}
	FQuat r(FVector(1, 0, 0), -HALF_PI);

	float alpha = calculateAlpha(25); //Pass in cut off frequency
	//Copy marker translation to Output.Pose
	if (readMarkers > 0)
	{
		for (int i = 0; i < readMarkers; i++)
		{
			FTransform xform;
			if (markers[i].cond > 0)
			{

				//Might need to transform differently
				FVector x(markers[i].x, -markers[i].y, markers[i].z);
				x = r * x;
				x = AMarkers[i].trans + alpha * (x - AMarkers[i].trans);
				xform.SetTranslation(x);
				xform.SetScale3D(FVector(1, 1, 1));
				//Tranform to LH coordinate system
				//ConvertTransformToFromMaya(xform);
				//Rotate by 90 around X to bring to Z up from Y up of OWL
				AMarkers[i].vel.Add((xform.GetTranslation() - AMarkers[i].trans) / 60.f);
				if (AMarkers[i].vel.Num() > 3)
				{
					AMarkers[i].vel.RemoveAt(0);
				}
				if (AMarkers[i].isMissing)
				{
					AMarkers[i].blendFactor += 0.1f;
					FVector trans = (1.f - AMarkers[i].blendFactor)*AMarkers[i].trans + (AMarkers[i].blendFactor)*xform.GetTranslation();

					xform.SetScale3D(FVector(3, 3, 3));
					if (AMarkers[i].blendFactor > 1.f)
					{
						AMarkers[i].blendFactor = 1.f;
						AMarkers[i].isMissing = false;
					}
					xform.SetTranslation(trans);
				}

			}
			else
			{
				//Calculate average vel over last 3 frames
				FVector vel(0, 0, 0);
				for (int j = 0; j < AMarkers[i].vel.Num(); j++)
				{
					vel += AMarkers[i].vel[j];
				}
				vel /= AMarkers[i].vel.Num();
				FVector trans = AMarkers[i].trans + vel;
				xform.SetTranslation(trans);
				xform.SetScale3D(FVector(2, 2, 2));
				AMarkers[i].isMissing = true;
				AMarkers[i].blendFactor = 0.f;
			}
			AMarkers[i].trans = xform.GetTranslation();
		}
	}
	return ELAResult::ELASuccess;

}
