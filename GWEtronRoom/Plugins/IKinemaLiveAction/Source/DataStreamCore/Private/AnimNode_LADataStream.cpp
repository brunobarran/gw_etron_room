/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "AnimNode_LADataStream.h"
#include "LADataStreamCorePrivatePCH.h"
#include "LADataStreamModule.h"
#include "AnimNode_LADataStream.h"
#include "IKinemaLAAnimInstance.h"




DECLARE_STATS_GROUP(TEXT("Mocap"), STATGROUP_Mocap, STATCAT_Advanced);

FAnimNode_LADataStream::FAnimNode_LADataStream()
	: AutoScale(false), IsStreamYUp(true), IsRetargeting(true), ImportScale(1.f), useRigidBody(false), DebugDrawRBs(false), ViconStream(nullptr), numberOfAttempts(0), ReallyReconnect(false), DidWarn(false)
{
	FLADataStreamModule& module = FModuleManager::GetModuleChecked< FLADataStreamModule >("LADataStreamCore");
	if (!module.licValid())
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("License not found or expired."));
	}
	IsTrailLicense = module.IsTrail();
	if (!module.licValid())
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("License not found or expired."));
		//TODO: @AE show a message box to user 
		IsLicInvalid = true;

		return;
	}

	if (IsTrailLicense)
	{
		TimeSinceStart = FPlatformTime::Seconds();
	}

	if (Actor)
	{
		SetAsset();
	}
	ServerName = NAME_None;
	SubjectName = NAME_None;
}

FAnimNode_LADataStream::~FAnimNode_LADataStream()
{
	if (ViconStream)
	{
		serverSocket = StrServerName + FString::FromInt(PortNumber);
		switch (MocapServer)
		{
		case EMocapServer::VICON:
			ULAViconStream::Remove(StrServerName);
			break;
		case EMocapServer::NatNet:
			ULAOptiTrackStream::Remove(StrServerName);
			break;
		case EMocapServer::XSens:
			ULAXSensStream::Remove(serverSocket);
			break;
		case EMocapServer::VRPN:
			ULAVRPNStream::Remove(StrServerName);
			break;
		}
	}
}


void FAnimNode_LADataStream::BindSkeleton(FAnimInstanceProxy* AnimInstanceProxy)
{

	USkeleton* skeleton = AnimInstanceProxy->GetSkeleton();
	if (skeleton != nullptr && StrSubjectName != "None")
	{
		mSkeletonBinding = FLADataStreamToSkeletonBinding();
		mSkeletonBinding.bIsRetargeting = IsRetargeting;
		ReallyReconnect = mSkeletonBinding.BindToSkeleton(ViconStream, StrSubjectName, skeleton, AnimInstanceProxy->GetSkelMeshComponent());
		ReallyReconnect = ReallyReconnect && Reconnect;
	}
}

void FAnimNode_LADataStream::IntializeStreamer(FAnimInstanceProxy* AnimInstanceProxy)
{
	if (StrServerName != "None" && (!ViconStream || ViconStream->GetServerIP() != StrServerName))
	{
		if (ViconStream)
		{
			ViconStream->Remove(ViconStream);
			ViconStream = nullptr;
		}
		serverSocket = StrServerName + FString::FromInt(PortNumber);
		switch (MocapServer)
		{
		case EMocapServer::VICON:
			ViconStream = ULAViconStream::Get(StrServerName);
			break;
		case EMocapServer::NatNet:
			ViconStream = ULAOptiTrackStream::Get(StrServerName);
			break;
		case EMocapServer::XSens:
			ViconStream = ULAXSensStream::Get(serverSocket);
			break;
		case EMocapServer::VRPN:
			ViconStream = ULAVRPNStream::Get(StrServerName);
		}
	}
	if (ViconStream)
	{
		//TODO: port number changed at runtime	
		if (Reconnect && (!ViconStream->isConnected() || MocapServer == EMocapServer::NatNet))
		{
			if (ViconStream->Connect(StrServerName, PortNumber) == ELAResult::ELASuccess)
			{
				numberOfAttempts = 0;
				BindSkeleton(AnimInstanceProxy);
				ReallyReconnect = false;
			}
		}
		else if (!mSkeletonBinding.IsBound() && !useRigidBody)
		{
			BindSkeleton(AnimInstanceProxy);
		}
	}
}

void FAnimNode_LADataStream::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	FAnimNode_Base::Initialize_AnyThread(Context);
	InPose.Initialize(Context);
	// Forward to the incoming pose link.
	check(Context.AnimInstanceProxy != nullptr);
	if (IsLicInvalid)
	{
		return;
	}
	SetAsset();
	StrServerName = ServerName.ToString();
	StrSubjectName = SubjectName.ToString();
	auto AnimInstanceProxy = Context.AnimInstanceProxy;
	IntializeStreamer(AnimInstanceProxy);
	previousPort = PortNumber;

	if (IsTrailLicense)
	{
		TimeSinceStart = FPlatformTime::Seconds();
	}

}

#define NO_MESSAGE_DELAY 5
#define TRIAL_PLAY_TIME 120

void FAnimNode_LADataStream::PreUpdate(const UAnimInstance* InAnimInstance)
{
	if (IsTrailLicense)
	{
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, TEXT("This is a time limited trial version of IKINEMA LiveAction!\nPlease contact store@ikinema.con to obtain a full license"));
			if (!GEngine->bEnableOnScreenDebugMessages)
			{
				FPlatformProcess::Sleep(NO_MESSAGE_DELAY);
			}
		}
		float realtimeSeconds = FPlatformTime::Seconds();
		if ((realtimeSeconds - TimeSinceStart > TRIAL_PLAY_TIME))
		{
			//
			if (!DidWarn)
			{
				FMessageDialog::Open(EAppMsgType::Ok, FText::FromString(TEXT("This is a time limited trail version of IKINEMA LiveAction!\nPlease contact store@ikinema.con to obtain a full license")));
				DidWarn = true;
			}

		}
	}
}

void FAnimNode_LADataStream::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	DECLARE_SCOPE_CYCLE_COUNTER(TEXT("Update"), STAT_StatsMocapUpdate, STATGROUP_Mocap);

	InPose.Update(Context);
	EvaluateGraphExposedInputs.Execute(Context);

	StrServerName = ServerName.ToString();
	StrSubjectName = SubjectName.ToString();

	if (!IsLicInvalid)
	{
		if (!ViconStream || ViconStream->GetServerIP() != StrServerName)
		{
			IntializeStreamer(Context.AnimInstanceProxy);

		}
		if (ViconStream && ReallyReconnect && !ViconStream->isConnected())
		{
			ViconStream->Connect(StrServerName, PortNumber);
		}

		if (previousPort != PortNumber)
		{
			ViconStream->Disconnect();
			ViconStream->Connect(StrServerName, PortNumber);
			previousPort = PortNumber;
			BindSkeleton(Context.AnimInstanceProxy);
		}
		if (!mSkeletonBinding.IsBound() && !useRigidBody && ReallyReconnect || ((SubjectName != "None") && mSkeletonBinding.GetSubjectName() != StrSubjectName))
		{
			BindSkeleton(Context.AnimInstanceProxy);
		}
	}


	return;
}
void FAnimNode_LADataStream::Evaluate_AnyThread(FPoseContext& Output)
{
	check(Output.AnimInstanceProxy->GetSkeleton() != nullptr);
	TArray<FTransform> Bones;

	if (!ViconStream || IsLicInvalid || !ViconStream->isConnected() || (IsTrailLicense && DidWarn))
	{
		Output.Pose.ResetToRefPose();
		UE_LOG(LogLADataStreamCore, Warning, TEXT("LADataStream::Evaluate() Not connected to Mocap server."));
		return;
	}

	if (useRigidBody)
	{
		DECLARE_SCOPE_CYCLE_COUNTER(TEXT("GetFrame"), STAT_StatsGetFrame, STATGROUP_Mocap);
		FTransform b;
		if (RigidBodies.Num() > 0)
		{
			auto world = Output.AnimInstanceProxy->GetSkelMeshComponent()->GetWorld();

			for (const auto& rb : RigidBodies)
			{
				ViconStream->GetRigidBodyTransform(rb, b);
				b.NormalizeRotation();
				if (IsStreamYUp)
				{
					FQuat q(FVector(1, 0, 0), -HALF_PI);
					b = b * (q);
				}
				b.SetTranslation(b.GetTranslation() * ImportScale);
				Bones.Push(b);

				if (DebugDrawRBs)
				{
					// VRPN needs conversion m -> cm
					auto target = MocapServer == VRPN ? b.GetLocation() * 100.0 : b.GetLocation();
					FFunctionGraphTask::CreateAndDispatchWhenReady([world, target]() {
						::DrawDebugSphere(world, target, 5.f, 12, FColor::Magenta);
					}, TStatId(), nullptr, ENamedThreads::GameThread);
				}
			}
		}
		else
		{
			ViconStream->GetRigidBodyTransform(SubjectName.ToString(), b);
			Bones.Init(FTransform::Identity, Output.Pose.GetNumBones());
			if (IsStreamYUp)
			{
				FQuat q(FVector(1, 0, 0), -HALF_PI);
				b = b * (q);
			}
			b.SetTranslation(b.GetTranslation() * ImportScale);
			Bones[0] = (b);
		}
		Output.Pose.InitBones(Bones.Num());
		Output.Pose.CopyBonesFrom(Bones);
		auto AnimInstance = Output.AnimInstanceProxy->GetAnimInstanceObject();
		if (AnimInstance->IsA<UIKinemaLAAnimInstance>())
		{
			auto proxy = static_cast<FIKinemaLAAnimInstanceProxy*>(Output.AnimInstanceProxy);
			if (proxy)
			{
				proxy->SetTimeFromPose(ViconStream->GetTimestamp());
			}
		}
		return;

	}

	int numberOfBones = mSkeletonBinding.GetNumberOfBones();
	if (numberOfBones == 0)
	{
		Bones.Init(FTransform::Identity, 0);
		Output.Pose.ResetToRefPose();
		UE_LOG(LogLADataStreamCore, Warning, TEXT("LADataStream::Evaluate() Couldn't get a pose from Mocap source."));
		return;
	}

	//We want twice as much transforms, to hold globals as well as local.
	//The first n transforms will hold local the remaining n will be globals
	Bones.SetNum(2 * numberOfBones);
	{
		DECLARE_SCOPE_CYCLE_COUNTER(TEXT("GetFrame"), STAT_StatsGetFrame, STATGROUP_Mocap);
		if (!mSkeletonBinding.UpdatePose(Bones) || Bones.Num() == 0)
		{
			Bones = Output.AnimInstanceProxy->GetSkeleton()->GetRefLocalPoses();
			Output.Pose.CopyBonesFrom(Bones);
			return;
		}
	}
	if (!FMath::IsNearlyZero(Bones[0].GetTranslation().SizeSquared()))
	{
		if (Actor && AutoScale)
		{
			Scale = Actor->UpdateScale(Bones);
		}
	}
	//if ImportScale has changed, apply that effect
	if (ImportScale != 1.f) {
		for (int i = 0; i < Bones.Num(); i++)
		{
			Bones[i].SetTranslation(Bones[i].GetTranslation() * ImportScale);
		}
	}
	if (IsStreamYUp)
	{
		DECLARE_SCOPE_CYCLE_COUNTER(TEXT("ZUp"), STAT_StatsMocapZup, STATGROUP_Mocap);
		FQuat q(FVector(1, 0, 0), -HALF_PI);
		Bones[0] = Bones[0] * (q);
		for (int i = numberOfBones; i < Bones.Num(); i++)
		{
			Bones[i] = Bones[i] * q;
		}
	}

	if (useRigidBody)
	{
		while (Bones.Num() > Output.Pose.GetNumBones())
		{
			Bones.Pop();
		}
		Bones[0].SetTranslation(Bones[0].GetTranslation() * ImportScale);
		Output.Pose.CopyBonesFrom(Bones);
		return;
	}

	Output.Pose.InitBones(2 * numberOfBones);
	Output.Pose.CopyBonesFrom(Bones);

	auto AnimInstance = Output.AnimInstanceProxy->GetAnimInstanceObject();
	if (AnimInstance->IsA<UIKinemaLAAnimInstance>())
	{
		auto proxy = static_cast<FIKinemaLAAnimInstanceProxy*>(Output.AnimInstanceProxy);
		if (proxy)
		{
			proxy->SetTimeFromPose(ViconStream->GetTimestamp());
		}
	}

	return;
}

void FAnimNode_LADataStream::CacheBones_AnyThread(const FAnimationCacheBonesContext & Context)
{
	//InitializeBoneReferences(Context.AnimInstance->RequiredBones);
	InPose.CacheBones(Context);
}

void FAnimNode_LADataStream::SetAsset()
{
	if (!Actor)
	{
		return;
	}

	useRigidBody = Actor->IsRigidBody;
	for (const auto& rb : Actor->Bones)
	{
		RigidBodies.AddUnique(rb.Name.ToString());
	}

}

