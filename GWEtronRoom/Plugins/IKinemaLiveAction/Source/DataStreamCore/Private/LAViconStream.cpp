/* Copyright (C) 2006-2017, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/
#include "LAViconStream.h"
#include "LADataStreamCorePrivatePCH.h"

TMap<FString, ULAViconStream*> ULAViconStream::ViconStreams;
// Default constructor.
FLADataStreamToSkeletonBinding::FLADataStreamToSkeletonBinding()
: mViconStream(nullptr), mBound(false)
{
}

// Bind the given Strean to the given skeleton and store the result.
bool FLADataStreamToSkeletonBinding::BindToSkeleton(IDataStreamInterface* ViconStream,const FString& subject, USkeleton* skeleton, USkeletalMeshComponent* component)
{
	if (ViconStream == nullptr)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Invalid Mocap Stream for skeleton binding"));
		return false;
	}
	mViconStream = ViconStream;
	mSkeleton = skeleton;
	mComponent = component;
	mSubjectName = subject;
	int32 numBoneDefs = INDEX_NONE;
	if (ViconStream->isConnected())
	{
		ViconStream->GetFrame();
		ViconStream->GetSegmentCountForSubject(subject, numBoneDefs);
		if (numBoneDefs == INDEX_NONE || numBoneDefs == 0)
		{
			return false;
		}
		mSegmentNames.Empty(numBoneDefs);
		UE4BoneIndices.Empty(numBoneDefs);
		for (int32 i = 0; i < numBoneDefs; ++i)
		{
			int32 ue4BoneIndex = INDEX_NONE;
			FString Name;
			FString ParentName;
			ViconStream->GetSegmentNameForSubject(subject, i, Name);
			ViconStream->GetSegmentParentNameForSubject(subject, Name, ParentName);

			
			if (bIsRetargeting)
			{
				mSegmentNames.Add(Name);
			}
			else 
			{
				ue4BoneIndex = component->GetBoneIndex(FName(*Name));
				UE4BoneIndices.Add(ue4BoneIndex);
				mSegmentNames.Add(Name);
			}
		}
		mBound = true;
	}
	else
	{
		mBound = false;
	}


	return mBound;
}

bool FLADataStreamToSkeletonBinding::UpdatePose(TArray<FTransform>& outPose)
{
	if (mViconStream == nullptr)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("ViconStream object is not valid"));
		return false;
	}

	if (!mBound)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Skeleton not bound to a valid stream"));
		return false;
	}

	if (!mViconStream->isConnected())
		return false;
	
	return  (mViconStream->GetPoseForSubject(this, mSubjectName, outPose) && mViconStream->GetGlobalPoseForSubject(this, mSubjectName, outPose));

}
// Access the UE4 bone index given the bone def index.
int32 FLADataStreamToSkeletonBinding::GetUE4BoneIndex(int32 boneDefIndex) const
{
	if (UE4BoneIndices.IsValidIndex(boneDefIndex))
		return UE4BoneIndices[boneDefIndex];
	return -1;
}

FString FLADataStreamToSkeletonBinding::GetViconSegmentName(int32 SegmentIndex) const
{
	if (mSegmentNames.IsValidIndex(SegmentIndex))
		return mSegmentNames[SegmentIndex];
	return "";
}

int FLADataStreamToSkeletonBinding::GetNumberOfBones() const
{
	return mSegmentNames.Num();
}

bool FLADataStreamToSkeletonBinding::IsBound()
{ 
	return mBound; 
}

const IDataStreamInterface* FLADataStreamToSkeletonBinding::GetViconStream() const
{
	
	return mViconStream;
}

const USkeleton* FLADataStreamToSkeletonBinding::GetSkeleton() const
{
	return mSkeleton;
}

const USkeletalMeshComponent* FLADataStreamToSkeletonBinding::GetSkeletalMeshComponent() const
{
	return mComponent;
}

ULAViconStream::ULAViconStream(const FObjectInitializer& PCIP)
	: Super(PCIP), Thread(nullptr)
{

}




ELAResult ULAViconStream::Connect(FString server_ip,int portNumber)
{

	FLADataStreamModule& module = FModuleManager::GetModuleChecked< FLADataStreamModule >("LADataStreamCore");
	if (!module.licValid())
	{
		return ELAResult::ELAError;
	}
	if (IsValidLowLevel())
	{
		_server_ip = server_ip;
		server_ip = server_ip + ":";
		server_ip.AppendInt(portNumber);
		TStringConversion<TStringConvert <TCHAR, ANSICHAR>> a(*server_ip);

		DSSDK::String serverIp(a.Get());

		//_server_ip = server_ip;
		DSSDK::Output_Connect&& oc = _client.Connect(serverIp);
		if (oc.Result == DSSDK::Result::Success || oc.Result == DSSDK::Result::ClientAlreadyConnected)
		{
			_client.EnableSegmentData();
			_client.EnableMarkerData();
			if (_client.SetStreamMode(DSSDK::StreamMode::ServerPush).Result == DSSDK::Result::Success)
			{
				return ELAResult::ELASuccess;
			}
		}
	}
	return ELAResult::ELAError;
}

ELAResult ULAViconStream::UseMulticast(FString multicast_ip)
{

	return ELAResult::ELAError;
}


FString ULAViconStream::ToString()
{ 
	return FString("Vicon Streaming node, connected to  server") + _server_ip;
};

void ULAViconStream::Stop()
{ 
	KillThread = true; 
}

ELAResult ULAViconStream::Disconnect()
{
	
	if (_client.IsConnected().Connected)
	{
		Stop();
		if (Thread)
		{
			Thread->Kill(true);
			Thread = nullptr;
		}
		_client.Disconnect();
	}
	return ELAResult::ELASuccess;
}

ELAResult ULAViconStream::GetSubjectCount(int& count)
{
	DSSDK::Output_GetSubjectCount&& og = _client.GetSubjectCount();
	if (og.Result == DSSDK::Result::NoFrame)
	{
		if (_client.GetFrame().Result == DSSDK::Result::Success)
			og = _client.GetSubjectCount();
	}

	if (og.Result == DSSDK::Result::Success)
	{
		count = og.SubjectCount;
		return ELAResult::ELASuccess;
	}

	return ELAResult::ELAError;
}

ELAResult ULAViconStream::GetSubjectName(int index, FString& name)
{
	DSSDK::Output_GetSubjectName&& ogs = _client.GetSubjectName(index);
	if (ogs.Result == DSSDK::Result::NoFrame)
	{
		if (_client.GetFrame().Result == DSSDK::Result::Success)
			ogs = _client.GetSubjectName(index);
	}

	if (ogs.Result == DSSDK::Result::Success)
	{
		std::string t (ogs.SubjectName);
		name = FString(t.c_str());
		return ELAResult::ELASuccess;
	}
	return ELAResult::ELAError;
}
ELAResult ULAViconStream::GetSegmentCountForSubject(const FString& subjectName, int& count)
{
	TStringConversion<TStringConvert <TCHAR, ANSICHAR>> a(*subjectName);
	DSSDK::String name(a.Get());
	count = 0;
	DSSDK::Output_GetSegmentCount&& ogc = _client.GetSegmentCount(name);

	if (ogc.Result == DSSDK::Result::NoFrame)
	{
		if (_client.GetFrame().Result == DSSDK::Result::Success)
			ogc = _client.GetSegmentCount(name);
	}

	if (ogc.Result == DSSDK::Result::InvalidSubjectName)
		UE_LOG(LogLADataStreamCore, Warning, TEXT("Invalid Subject name"));

	if (ogc.Result != DSSDK::Result::Success)
	{
		UE_LOG(LogLADataStreamCore, Warning, TEXT("Failed to Get Segment Count"));
		return ELAResult::ELAError;
	}
	count = ogc.SegmentCount;
	return ELAResult::ELASuccess;
}

ELAResult ULAViconStream::GetSegmentNameForSubject(const FString& subjectName, int index, FString& segName)
{
	TStringConversion<TStringConvert <TCHAR, ANSICHAR>> a(*subjectName);
	DSSDK::String name(a.Get());

	DSSDK::Output_GetSegmentName && ogn = _client.GetSegmentName(name,index);

	if (ogn.Result == DSSDK::Result::InvalidSubjectName)
		UE_LOG(LogLADataStreamCore, Warning, TEXT("Invalid Subject name"));

	if (ogn.Result != DSSDK::Result::Success)
		return ELAResult::ELAError;
	std::string t(ogn.SegmentName);
	segName = FString(t.c_str());
	return ELAResult::ELASuccess;
}

ELAResult ULAViconStream::GetSegmentParentNameForSubject(const FString& subjectName, FString segName, FString& parentSegName)
{
	TStringConversion<TStringConvert <TCHAR, ANSICHAR>> a(*subjectName);
	DSSDK::String name(a.Get());
	TStringConversion<TStringConvert <TCHAR, ANSICHAR>> b(*segName);
	DSSDK::String s_name(b.Get());
	DSSDK::Output_GetSegmentParentName && ogn = _client.GetSegmentParentName(name, s_name);

	if (ogn.Result == DSSDK::Result::InvalidSubjectName)
		UE_LOG(LogLADataStreamCore, Warning, TEXT("Invalid Subject name"));

	if (ogn.Result != DSSDK::Result::Success)
		return ELAResult::ELAError;
	std::string t(ogn.SegmentName);

	parentSegName = FString(t.c_str());
	return ELAResult::ELASuccess;
}

DECLARE_STATS_GROUP(TEXT("Vicon"), STATGROUP_Vicon, STATCAT_Advanced);
bool ULAViconStream::GetPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding,FString& subjectName, TArray<FTransform>& outPose)
{
	if (outPose.Num() == 0)
	{
		return false;
	}
	{
		if (!Thread)
		{
			FString name("Streamer");
			KillThread = false;
			Thread = FRunnableThread::Create(this, *name, 0, EThreadPriority::TPri_Normal, FPlatformAffinity::GetNoAffinityMask());
		}
	}
	
	TStringConversion<TStringConvert <TCHAR, ANSICHAR>> a(*subjectName);
	DSSDK::String name(a.Get());
	DSSDK::Output_GetSegmentCount&& ogsc = _client.GetSegmentCount(name);
	if (ogsc.Result != DSSDK::Result::Success)
		return false;
	{
		//FScopeLock lock(&section);
		for (unsigned int j = 0; j < ogsc.SegmentCount; ++j)
		{


			TStringConversion<TStringConvert <TCHAR, ANSICHAR>> b(*(skeletonBinding->GetViconSegmentName(j)));
			DSSDK::String SegmentName(b.Get());

			int32 UE4Index = skeletonBinding->GetUE4BoneIndex(j);
			/*if (UE4Index == INDEX_NONE)
			{
			UE_LOG(LogLADataStreamCore, Error, TEXT("Invalid Bone Mapping from DataStream to UE4"));
			return false;
			}*/
			FTransform trans;// = outPose[UE4Index];
			DSSDK::Output_GetSegmentLocalTranslation&& ogsn = _client.GetSegmentLocalTranslation(name, SegmentName);
			if (ogsn.Result != DSSDK::Result::Success || ogsn.Occluded)
			{
				trans.SetTranslation(FVector(0, 0, 0));
			}
			else
			{
				trans.SetTranslation(FVector(ogsn.Translation[0], ogsn.Translation[1], ogsn.Translation[2]));
			}

			DSSDK::Output_GetSegmentLocalRotationQuaternion&& ogsq = _client.GetSegmentLocalRotationQuaternion(name, SegmentName);
			if (ogsq.Result != DSSDK::Result::Success || ogsq.Occluded)
			{
				//trans.SetRotation(FQuat(0, 0, 0, 0));
			}
			else
			{
				FQuat q((ogsq.Rotation[0]), (ogsq.Rotation[1]), (ogsq.Rotation[2]), ogsq.Rotation[3]);

				trans.SetRotation(q);// , ogsq.Rotation[3]));
			}
			ConvertTransformToFromMaya(trans);
			outPose[j] = trans;
		}
	}
	return true;
}


uint32 ULAViconStream::Run()
{
	while (!KillThread && isConnected())
	{
		GetFrame();
		FPlatformProcess::Sleep(0.0f);
	}
	return 0;
}

bool ULAViconStream::GetGlobalPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose)
{

	TStringConversion<TStringConvert <TCHAR, ANSICHAR>> a(*subjectName);
	DSSDK::String name(a.Get());
	DSSDK::Output_GetSegmentCount&& ogsc = _client.GetSegmentCount(name);
	if (ogsc.Result != DSSDK::Result::Success)
		return false;
	{
		//FScopeLock lock(&section);
		for (unsigned int j = 0; j < ogsc.SegmentCount; ++j)
		{


			TStringConversion<TStringConvert <TCHAR, ANSICHAR>> b(*(skeletonBinding->GetViconSegmentName(j)));
			DSSDK::String SegmentName(b.Get());

			int32 UE4Index = skeletonBinding->GetUE4BoneIndex(j);

			FTransform trans;
			DSSDK::Output_GetSegmentGlobalTranslation&& ogsn = _client.GetSegmentGlobalTranslation(name, SegmentName);
			if (ogsn.Result != DSSDK::Result::Success || ogsn.Occluded)
			{
				trans.SetTranslation(FVector(0, 0, 0));
			}
			else
			{
				trans.SetTranslation(FVector(ogsn.Translation[0], ogsn.Translation[1], ogsn.Translation[2]));
			}

			DSSDK::Output_GetSegmentGlobalRotationQuaternion&& ogsq = _client.GetSegmentGlobalRotationQuaternion(name, SegmentName);

			if (ogsq.Result != DSSDK::Result::Success || ogsq.Occluded)
			{

			}
			else
			{
				FQuat q((ogsq.Rotation[0]), (ogsq.Rotation[1]), (ogsq.Rotation[2]), ogsq.Rotation[3]);

				trans.SetRotation(q);
			}


			ConvertTransformToFromMaya(trans);

			outPose[j + ogsc.SegmentCount] = trans;
		}
	}
	
	return true;
}

ULAViconStream* ULAViconStream::Get(const FString& serverIP)
{
	auto stream = ViconStreams.Find(serverIP);
	if (stream)
	{
		if ((*stream)->IsValidLowLevel())
		{
			(*stream)->RefCount++;
			return *stream;
		}
		else
		{
			ViconStreams.Remove(serverIP);
		}
	}
	auto newStream = NewObject<ULAViconStream>();
	newStream->AddToRoot();
	newStream->RefCount = 1;
	ViconStreams.Add(serverIP);
	ViconStreams[serverIP] = newStream;
	
	return newStream;

}

void ULAViconStream::Remove(const FString serverIP)
{
	auto stream = ViconStreams.Find(serverIP);
	if (stream)
	{
		if ((*stream)->IsValidLowLevel())
		{
			(*stream)->RefCount--;

			if ((*stream)->RefCount <= 0)
			{
				(*stream)->Stop();
				(*stream)->Disconnect();
				ViconStreams.Remove(serverIP);
			}
		}
	}
}

void ULAViconStream::Destory()
{
	if (this->IsValidLowLevel())
	{
		RefCount--;
		if (RefCount <= 0)
		{
			Stop();
			Disconnect();
			
			ConditionalBeginDestroy(); //instantly clears UObject out of memory
			RemoveFromRoot();
		}
		
	}
}


ULAViconStream::~ULAViconStream()
{
}

void ULAViconStream::BeginDestroy()
{
	RefCount = 0;
	//if (RefCount <= 0)
	{
		ViconStreams.Remove(_server_ip);
		Stop();
		Disconnect();
	}
	Super::BeginDestroy();
}

void ULAViconStream::FinishDestroy()
{
	if (RefCount <= 0)
	{
		Super::FinishDestroy();
	}
	Super::FinishDestroy();
}

bool ULAViconStream::isConnected()
{
	DSSDK::Output_IsConnected res = _client.IsConnected();
	return res.Connected;
}

ELAResult ULAViconStream::GetFrame()
{
	//if (this->IsValidLowLevelFast())
	if (KillThread && !isConnected())
	{
		return ELASuccess;
	}
	{
		
		{
			DECLARE_SCOPE_CYCLE_COUNTER(TEXT("GetDSFrame"), STAT_StatsViconGetDSFrame, STATGROUP_Vicon);
			//FScopeLock lock(&section);
			
			DSSDK::Output_GetFrame gf = _client.GetFrame();
		

		if (gf.Result == DSSDK::Result::Success)
		{
			return ELASuccess;
		}
		if (gf.Result == DSSDK::Result::NotConnected)
			return Connect(_server_ip);
		}
	}

	return ELAResult::ELAError;
}

ELAResult ULAViconStream::GetRigidBodyTransform(const FString& subjectName, FTransform& trans)
{ 
	if (!Thread)
	{
		FString name("Streamer");
		KillThread = false;
		Thread = FRunnableThread::Create(this, *name, 0, EThreadPriority::TPri_Normal, FPlatformAffinity::GetNoAffinityMask());
	}

	TStringConversion<TStringConvert <TCHAR, ANSICHAR>> a(*subjectName);
	DSSDK::String name(a.Get());
	DSSDK::Output_GetSegmentCount&& ogsc = _client.GetSegmentCount(name);
	if (ogsc.Result != DSSDK::Result::Success)
		return  ELAResult::ELAError;

	{
		FString jointName;
		GetSegmentNameForSubject(subjectName, 0, jointName);
		TStringConversion<TStringConvert <TCHAR, ANSICHAR>> b(*(jointName));
		DSSDK::String SegmentName(b.Get());

		DSSDK::Output_GetSegmentGlobalTranslation&& ogsn = _client.GetSegmentGlobalTranslation(name, SegmentName);
		if (ogsn.Result != DSSDK::Result::Success || ogsn.Occluded)
		{
			trans.SetTranslation(FVector(0, 0, 0));
		}
		else
		{
			trans.SetTranslation(FVector(ogsn.Translation[0], ogsn.Translation[1], ogsn.Translation[2]));
		}

		DSSDK::Output_GetSegmentGlobalRotationQuaternion&& ogsq = _client.GetSegmentGlobalRotationQuaternion(name, SegmentName);

		if (ogsq.Result != DSSDK::Result::Success || ogsq.Occluded)
		{

		}
		else
		{
			FQuat q((ogsq.Rotation[0]), (ogsq.Rotation[1]), (ogsq.Rotation[2]), ogsq.Rotation[3]);

			trans.SetRotation(q);
		}
	}


	ConvertTransformToFromMaya(trans);
	return ELAResult::ELASuccess; 
}