/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#
#include "AnimNode_IKinemaPhaseSpace.h"
#include "LADataStreamCorePrivatePCH.h"

FAnimNode_IKinemaPhaseSpace::FAnimNode_IKinemaPhaseSpace():
	stream(nullptr)

{
}

FAnimNode_IKinemaPhaseSpace::~FAnimNode_IKinemaPhaseSpace()
{
}

void FAnimNode_IKinemaPhaseSpace::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	// Forward to the incoming pose link.
	check(Context.AnimInstanceProxy != nullptr);
	stream = ULAPhaseSpaceStream::Get(ServerName, MarkersNumber);


	check(stream != nullptr)
	if (stream)
	{
		if (!stream->isConnected())
		{
			stream->Connect(ServerName, PortNumber);
		}
	}
	InPose.Initialize(Context);

	FMarkerData v;//(0,0,0);
	v.blendFactor = 0.f;
	v.isMissing = false;
	AMarkers.Init(v, MarkersNumber);
	
}

void FAnimNode_IKinemaPhaseSpace::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	if (!stream)
	{
		stream = ULAPhaseSpaceStream::Get(ServerName,MarkersNumber);
		stream->Connect(ServerName, PortNumber);
	}
	
	ELAResult ret = stream->GetFrame();
	if (ret != ELAResult::ELASuccess)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Can not get frame from PhaseSpace server"));

	}
	// Forward to the incoming pose link.
	InPose.Update(Context);

	// Handle non-pose pins.
	EvaluateGraphExposedInputs.Execute(Context);
}


void FAnimNode_IKinemaPhaseSpace::Evaluate_AnyThread(FPoseContext& Output)
{
	TArray<FTransform> Bones;
	
	Bones.Init(FTransform::Identity,MarkersNumber);
	for(int i = 0; i < MarkersNumber; i++)
	{
		Bones[i].SetTranslation( AMarkers[i].trans);
	}
	
	for (int i = 0; i < MarkersNumber; i++)
	{
		FTransform xform(FTransform::Identity);
		if (stream->AMarkers.IsValidIndex(i))
		{
			xform.SetTranslation(stream->AMarkers[i].trans);
			Bones[i] = (xform);
		}
	}
	Output.Pose.CopyBonesFrom(Bones);	

}


