/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

//#include "Engine.h"

//Removing monolithic headers (Engine.h) replacing with IWYU
#include "Modules/ModuleManager.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Misc/MessageDialog.h"
#include "Misc/FileHelper.h"
#include "HAL/FileManager.h"
#include "HAL/PlatformFilemanager.h"
#include "HAL/PlatformAffinity.h"
#include "HAL/Runnable.h"
#include "DrawDebugHelpers.h"
#include "Widgets/Input/NumericTypeInterface.h"

#include "LADataStreamCoreClasses.h"

char* RlmCheckPoint(const char* licensePath);
void RlmStopFloatingLicensing();
char* RlmGetOptions();
extern const char* RlmExpirationDateString();
extern int RlmDaysRemaining();
#include "LADataStreamModule.h"
DECLARE_LOG_CATEGORY_CLASS(LogLADataStreamCore, Warning, All);
