/* Copyright (C) 2006-2017, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/


#include "LAVRPNStream.h"
#include "LADataStreamCorePrivatePCH.h"

#include <vrpn_Configure.h>  // for VRPN_CALLBACK
#include <vrpn_Shared.h>     // for timeval
//#include "Json.h"
#include "JsonObjectConverter.h"
TMap<FString, ULAVRPNStream*> ULAVRPNStream::Streams;

//
//static void ConvertTransformToFromMaya(FTransform& Transform, bool convertTranslation = true)
//{
//	FVector Translation = Transform.GetTranslation();
//	FQuat Rotation = Transform.GetRotation();
//
//	Rotation.X = -Rotation.X;
//	Rotation.Z = -Rotation.Z;
//	Transform.SetRotation(Rotation);
//
//	if (convertTranslation)
//	{
//		Translation.Y = -Translation.Y;
//		Transform.SetTranslation(Translation);
//	}
//}


void VRPN_CALLBACK
handle_tracker_pos_quat(void *userdata, const vrpn_TRACKERCB t)
{
	FVRPNChar *character = static_cast<FVRPNChar *>(userdata);

	FVector pos(t.pos[0], t.pos[1], t.pos[2]);
	FQuat quat(t.quat[0], t.quat[1], t.quat[2], t.quat[3]);
	quat.Normalize();

	if (character && character->Initialized)
	{
		FScopeLock(&character->characterSection);
		FTransform trans(quat, pos);
		ConvertTransformToFromMaya(trans);

		auto bone = character->Bones.FindByPredicate([t](FVRPNBones& bone) {return bone.ID == t.sensor;});
		if (bone) {
			bone->LocalPose = trans;
			return; // no need to go trough the RBs
		}

		auto rb = character->RigidBodies.FindByPredicate([t](FVRPNRigidBody& body) {return body.ID == t.sensor; });
		if (rb)
			rb->GlobalPose = trans;
	}
}

void VRPN_CALLBACK handle_text(void *userdata, const vrpn_TEXTCB t)
{
	FVRPNChar *character = static_cast<FVRPNChar *>(userdata);
	if (strcmp("request_metadata", t.message) == 0)
		return;
	char SegId = t.message[0];
	char Total = t.message[1];
	FScopeLock(&character->characterSection);
	character->CreateHierarchy(SegId, Total, &t.message[2]);
}

int VRPN_CALLBACK msg_handler(void * Streamerptr, vrpn_HANDLERPARAM p)
{
	if (!Streamerptr)
	{
		return -1;
	}
	ULAVRPNStream* streamer = (ULAVRPNStream*)Streamerptr;
	const char *sender_name = streamer->GetSocket()->sender_name(p.sender);
	const char *type_name = streamer->GetSocket()->message_type_name(p.type);
	if (sender_name != nullptr)
	{
		streamer->InitialiseCharacter(p.sender, FString(sender_name));
	}

	return -1; // Do not log the message
}



void FVRPNChar::CreateHierarchy(char SegId, char Total, const char* message)
{
	
	if (Initialized){
		return; // no need to handle metadata a second time
	}
	if ((SegId)-1 != (LastSegment + 1))
	{
		if (textSender)
		{
			textSender->send_message("request_metadata");

		}
	}
	LastSegment = SegId - 1;
	if (TotalSegments == -1)
	{
		TotalSegments = Total;
	}
	JsonString = JsonString + FString(message);

	if (SegId == TotalSegments)
	{
		FVRPNChar temp(Name);
		FJsonObjectConverter::JsonObjectStringToUStruct<FVRPNChar>(JsonString, &temp, 0, 0);
		Bones = temp.Bones;
		RigidBodies = temp.RigidBodies;
		InitRigidBodies();

		Initialized = true;
	}

}


FVRPNChar::FVRPNChar()
	:TotalSegments(-1), LastSegment(-1), Initialized(false), Name(NAME_None)
{

}

FVRPNChar::FVRPNChar(const FVRPNChar& other)
{
	Name = other.Name;
	TStringConversion<TStringConvert <TCHAR, ANSICHAR>> a(*(other.Name.ToString()));

	tracker = (new vrpn_Tracker_Remote(a.Get(), other.tracker->connectionPtr()));
	text = (new vrpn_Text_Receiver(a.Get(), other.text->connectionPtr()));
	textSender = (new vrpn_Text_Sender(a.Get(), other.textSender->connectionPtr()));
	Bones = other.Bones;
	RigidBodies = other.RigidBodies;
	tracker->register_change_handler(this, handle_tracker_pos_quat);
	text->register_message_handler(this, handle_text);

}

FVRPNChar FVRPNChar::operator=(const FVRPNChar& other)
{
	FVRPNChar newChar(other);
	return newChar;
}

FVRPNChar::~FVRPNChar()
{
	if (tracker)
	{
		tracker->unregister_change_handler(this, handle_tracker_pos_quat);
		tracker->connectionPtr()->removeReference();
	}
	if (text)
	{
		text->unregister_message_handler(this, handle_text);
		text->connectionPtr()->removeReference();
	}
	if (textSender)
	{

		textSender->connectionPtr()->removeReference();
	}
}

void FVRPNChar::DoFK()
{
	for (auto& bone : Bones)
	{
		if (bone.PID != INDEX_NONE)
		{
			bone.GlobalPose = bone.LocalPose * Bones[bone.PID].GlobalPose;
		}
		else
		{
			bone.GlobalPose = bone.LocalPose;
		}
	}
}

void FVRPNChar::InitRigidBodies()
{
	for (auto& rb : RigidBodies) {
		rb.GlobalPose = FTransform::Identity;
		m_rb_names.Add(rb.Name.GetPlainNameString());
		m_rb_indices.Add(m_rb_names.Last(), m_rb_names.Num() - 1);
	}
}

const FTransform& FVRPNChar::RigidBodyTransform(const FString& name) const
{
	if (!m_rb_indices.Contains(name)) {
		UE_LOG(LogLADataStreamCore, Error, TEXT("Wrong RigidBody name"));
		return FTransform::Identity;
	}

	auto rb_index =  m_rb_indices[name];
	return RigidBodies[rb_index].GlobalPose; // needs external locking
}

ULAVRPNStream::ULAVRPNStream(const FObjectInitializer& PCIP)
	: Super(PCIP)
{
	iDataPort = 3883;
	connected_client = false;
	is_realtime = true;
}


ELAResult ULAVRPNStream::Connect(FString server_ip, int portNumber)
{

	FLADataStreamModule& module = FModuleManager::GetModuleChecked< FLADataStreamModule >("LADataStreamCore");
	if (!module.licValid())
	{
		return ELAResult::ELAError;
	}

	if (IsValidLowLevel())
	{
		if (isConnected())
		{
			Disconnect();
		}
		else
		{
			if (Thread)
			{
				KillThread = true;
				Stop();
				Thread = nullptr;
			}
			characters.Empty();
			AvatarNameMap.Empty();
		}
		//TODO: Connect
		_server_ip = server_ip;
		m_port = portNumber;
		m_socket.Reset();
		FString client = server_ip + ":" + FString::FromInt(m_port);
		TStringConversion<TStringConvert <TCHAR, ANSICHAR>> a(*client);
		m_socket = TSharedPtr<vrpn_Connection>(vrpn_get_connection_by_name(a.Get(), "t"));

		if (m_socket.IsValid())
		{
			m_socket->register_log_filter(msg_handler, this);
			m_socket->mainloop();
			m_socket->setAutoDeleteStatus(false);
			//TODO: start parsing datagrams thread call startRead in a thread somehow :)
			if (!Thread)
			{
				static int ThreadIndex = 0;
				FString ThreadName = FString::Printf(TEXT("Streamer_%d"), ThreadIndex);
				ThreadIndex++;
				KillThread = false;
				Thread = FRunnableThread::Create(this, *ThreadName, 0, EThreadPriority::TPri_Normal, FPlatformAffinity::GetNoAffinityMask());
			}

		}
	}
	return ELAResult::ELASuccess;
}

uint32 ULAVRPNStream::Run()
{
	while (!KillThread && isConnected())
	{

		{
			FScopeLock lock(&section);
			
			if (m_socket.IsValid())
			{
				m_socket->mainloop();
				for (auto& character : characters)
				{
					character.Value->textSender->mainloop();
					character.Value->tracker->mainloop();
					character.Value->DoFK();
					character.Value->text->mainloop();
				}
			}
		}
		FPlatformProcess::Sleep(0.0f);
	}
	if (KillThread)
	{
		Stop();
	}
	return 0;
}

int ULAVRPNStream::startRead()
{
	//Start reading data.
	return -1;
}

bool ULAVRPNStream::InitialiseCharacter(int senderId, const FString& senderName)
{

	if (characters.Contains(senderId))
	{
		return true;
	}
	TSharedPtr<FVRPNChar> character = TSharedPtr<FVRPNChar>(new FVRPNChar(*(senderName)));



	FString client = _server_ip + ":" + FString::FromInt(m_port);
	FString text = senderName + "@" + client;

	TStringConversion<TStringConvert <TCHAR, ANSICHAR>> a(*senderName);
	character->tracker = (new vrpn_Tracker_Remote(a.Get(), m_socket.Get()));
	character->tracker->connectionPtr()->setAutoDeleteStatus(false);
	character->text = (new vrpn_Text_Receiver(a.Get(), m_socket.Get()));
	character->text->connectionPtr()->setAutoDeleteStatus(false);
	TStringConversion<TStringConvert <TCHAR, ANSICHAR>> b(*text);
	character->textSender = (new vrpn_Text_Sender(a.Get(), m_socket.Get()));
	character->textSender->connectionPtr()->setAutoDeleteStatus(false);
	if (character->textSender)
	{
		character->textSender->send_message("request_metadata");

	}
	character->tracker->register_change_handler(character.Get(), handle_tracker_pos_quat);
	character->text->register_message_handler(character.Get(), handle_text);

	characters.Add(senderId, character);
	AvatarNameMap.Add(senderId, senderName);
	return true;
}

ELAResult ULAVRPNStream::UseMulticast(FString multicast_ip)
{
	return ELAResult::ELAError;
}

ELAResult ULAVRPNStream::Disconnect()
{

	if (m_socket.IsValid())
	{
		KillThread = true;
		if (Thread)
		{
			Thread->Kill(true);
			delete Thread;
			Thread = nullptr;
		}


		{
			//Release reference to the XsSocket to close it.		
			FScopeLock lock(&section);
			m_socket->removeReference();
			m_socket.Reset();
			FPlatformFileManager::Get().GetPlatformFile().DeleteFile(*FString("t"));
		}


		AvatarNameMap.Empty();
		characters.Empty();
	}
	return ELAResult::ELASuccess;
}

ELAResult ULAVRPNStream::GetSubjectCount(int& count)
{
	count = characters.Num();
	return ELAResult::ELASuccess;
}

ELAResult ULAVRPNStream::GetRigidBodyTransform(const FString& name, FTransform& trans)
{
	if (characters.Num() == 0 || !characters[1]->Initialized)
		return ELAError;
	
	{
		FScopeLock lock(&section);
		trans = characters[1]->RigidBodyTransform(name);
	}
	return ELASuccess;
}

const TArray<FString>& ULAVRPNStream::GetRigidBodyNames() const
{
	if (characters.Num() == 0 || !characters[1]->Initialized)
		return temp;

	return characters[1]->RigidBodyNames();
}

ELAResult ULAVRPNStream::GetSubjectName(int index, FString& name)
{
	name = AvatarNameMap[index];
	name.ReplaceInline(ANSI_TO_TCHAR("-"), ANSI_TO_TCHAR("_"));
	return ELAResult::ELASuccess;
}

ELAResult ULAVRPNStream::GetSegmentCountForSubject(const FString& subjectName, int& count)
{
	unsigned int ind = GetSubjectIndex(subjectName);
	count = -1;
	if (ind == -1)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Invalid Subject name"));
		return ELAResult::ELAError;
	}

	count = characters[ind]->Bones.Num();
	return ELAResult::ELASuccess;
}

ELAResult ULAVRPNStream::GetSegmentNameForSubject(const FString& subjectName, int index, FString& segName)
{
	//TODO: SegmentNames.
	int ind = GetSubjectIndex(subjectName);
	if (ind == -1)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Invalid Subject name"));
		return ELAResult::ELAError;
	}
	segName = characters[ind]->Bones[index].Name.ToString();
	segName.ReplaceInline(ANSI_TO_TCHAR("-"), ANSI_TO_TCHAR("_"));
	return ELAResult::ELASuccess;

}

ELAResult ULAVRPNStream::GetSegmentParentNameForSubject(const FString& subjectName, FString segName, FString& parentSegName)
{
	int SubIndex, SegIndex, parentID;
	SubIndex = GetSubjectIndex(subjectName);
	if (SubIndex == -1)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Invalid Subject name"));
		return ELAResult::ELAError;
	}

	SegIndex = GetSegmentIndex(SubIndex, segName);
	if (SegIndex == -1 || (!characters.Contains(SubIndex)))
	{
		parentSegName = "";
		UE_LOG(LogLADataStreamCore, Error, TEXT("Invalid Subject name"));
		return ELAResult::ELAError;
	}
	parentID = characters[SubIndex]->Bones[SegIndex].PID;
	if (characters[SubIndex]->Bones.IsValidIndex(parentID))
	{
		parentSegName = characters[SubIndex]->Bones[parentID].Name.ToString();
		return ELAResult::ELASuccess;

	}
	return ELAResult::ELAError;
}

ELAResult ULAVRPNStream::GetSegmentParentIndexForSubject(FString& subjectName, FString segName, int& parentSegName)
{

	int SegIndex;
	int SubIndex = GetSubjectIndex(subjectName);
	SegIndex = GetSegmentIndex(SubIndex, segName);
	if (SegIndex == -1 || (!characters.Contains(SubIndex)))
	{

		return ELAResult::ELAError;
	}

	parentSegName = characters[SubIndex]->Bones[SegIndex].PID;
	return ELAResult::ELASuccess;

}

int ULAVRPNStream::GetSegmentIndex(int SubjectIndex, FString SegmentName)
{
	if (SubjectIndex != INDEX_NONE && characters.Contains(SubjectIndex))
	{
		int index = characters[SubjectIndex]->Bones.IndexOfByPredicate([&SegmentName](const FVRPNBones& bone) {return SegmentName == bone.Name.ToString(); });
		return index;
	}
	return INDEX_NONE;
}

int ULAVRPNStream::GetSubjectIndex(const FString& SubjectName)
{
	if (SubjectName.IsEmpty() || (!AvatarNameMap.FindKey(SubjectName)))
		return -1;
	int id = *AvatarNameMap.FindKey(SubjectName);
	return id;
}

bool ULAVRPNStream::GetPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose)
{
	if (outPose.Num() == 0)
	{
		return false;
	}
	
	DECLARE_SCOPE_CYCLE_COUNTER(TEXT("LocalPose"), STAT_StatsOptitrackLocalPose, STATGROUP_OptiTrack);
	int segCount;
	GetSegmentCountForSubject(subjectName, segCount);
	int SubIndex = GetSubjectIndex(subjectName);
	if (SubIndex == INDEX_NONE)
	{
		return false;
	}
	if (characters[SubIndex]->Bones.Num() < 0)
		return false;
	{
		FScopeLock(&characters[SubIndex]->characterSection);
		const auto& localSeg = characters[SubIndex]->Bones;
		for (int j = 0; j < segCount; ++j)
		{
			outPose[j] = localSeg[j].LocalPose;
		}
	}
	return true;
}


ELAResult ULAVRPNStream::GetSegmentLocalRotationQuaternion(FString SubjectName, FString SegmentName, FQuat& t)
{

	int SegIndex;
	int SubIndex = GetSubjectIndex(SubjectName);
	if ((!characters.Contains(SubIndex)))
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Wrong Subject name"));
		return ELAResult::ELAError;
	}

	SegIndex = GetSegmentIndex(SubIndex, SegmentName);
	if (SegIndex == -1)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Wrong Segment name"));
		return ELAResult::ELAError;
	}


	return ELAResult::ELAError;
}

ELAResult ULAVRPNStream::GetSegmentLocalTranslation(FString SubjectName, FString SegmentName, FVector& t)
{

	int SegIndex;
	int SubIndex = GetSubjectIndex(SubjectName);
	if ((!characters.Contains(SubIndex)))
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Wrong Subject name"));
		return ELAResult::ELAError;
	}

	SegIndex = GetSegmentIndex(SubIndex, SegmentName);
	if (SegIndex == -1)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Wrong Segment name"));
		return ELAResult::ELAError;
	}



	return ELAResult::ELAError;
}

bool ULAVRPNStream::GetGlobalPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose)
{
	DECLARE_SCOPE_CYCLE_COUNTER(TEXT("GlobalPose"), STAT_StatsOptitrackGlobalPose, STATGROUP_OptiTrack);
	if (outPose.Num() == 0)
	{
		return false;
	}

	int segCount;
	int SubIndex = GetSubjectIndex(subjectName);
	if (characters[SubIndex]->Bones.Num() < 0)
		return false;


	GetSegmentCountForSubject(subjectName, segCount);
	{
		FScopeLock(&characters[SubIndex]->characterSection);
		const auto& globalSeg = characters[SubIndex]->Bones;
		for (int j = 0; j < segCount; ++j)
		{
			outPose[j + segCount] = globalSeg[j].GlobalPose;
		}
	}
	return true;
}


ULAVRPNStream* ULAVRPNStream::Get(const FString& serverIP)
{
	auto stream = Streams.Find(serverIP);
	if (stream)
	{
		if ((*stream)->IsValidLowLevel())
		{
			(*stream)->RefCount++;
			return *stream;
		}
		else
		{
			Streams.Remove(serverIP);
		}
	}
	auto newStream = NewObject<ULAVRPNStream>();
	newStream->AddToRoot();
	newStream->RefCount = 1;
	Streams.Add(serverIP);
	Streams[serverIP] = newStream;

	return newStream;

}

void ULAVRPNStream::Remove(const FString serverIP)
{
	auto stream = Streams.Find(serverIP);
	if (stream)
	{
		if ((*stream)->IsValidLowLevel())
		{
			(*stream)->RefCount--;

			if ((*stream)->RefCount <= 0)
			{
				(*stream)->Disconnect();
				Streams.Remove(serverIP);
			}
		}
	}
}

void ULAVRPNStream::Destory()
{
	if (this->IsValidLowLevel())
	{
		RefCount--;
		if (RefCount <= 0)
		{
			Disconnect();
			if (Thread)
			{
				delete Thread;
				Thread = nullptr;
			}
			ConditionalBeginDestroy(); //instantly clears UObject out of memory
			RemoveFromRoot();
		}

	}
}

void ULAVRPNStream::BeginDestroy()
{
	RefCount--;
	if (RefCount <= 0)
	{
		Streams.Remove(_server_ip);
		Disconnect();
		Super::BeginDestroy();
	}
	Super::BeginDestroy();
}

void ULAVRPNStream::FinishDestroy()
{
	if (RefCount <= 0)
	{
		Super::FinishDestroy();
	}
	Super::FinishDestroy();
}

bool ULAVRPNStream::isConnected()
{
	//TODO:

	//FScopeLock lock(&section);
	if (!KillThread && m_socket.IsValid())
		return m_socket->connected() && m_socket->doing_okay();
	return false;
}

ELAResult ULAVRPNStream::GetFrame()
{
	Datagram* currentDatagram = nullptr;
	if (is_realtime)
	{
		if (!bIsInitialized)
		{
			bIsInitialized = true;
			for (auto character : characters)
			{
				bIsInitialized &= character.Value->Initialized;
			}
			//bIsInitialized = InitialiseCharacter();
		}
		/*{
			FScopeLock lock(&section);
			while (!frameStore.IsEmpty() && frameStore.Peek(currentDatagram))
			{

				frameStore.Dequeue(currentDatagram);
				if (currentDatagram)
				{

					int avatarId = currentDatagram->avatarId();
					if (Characters.Contains(avatarId))
					{
						Characters[avatarId].Deserialize(currentDatagram);
						delete currentDatagram;
					}

				}
			}
		}*/
	}


	//TODO
	return ELAResult::ELASuccess;
}
