/* Copyright (C) 2006-2017, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "LAXSensStream.h"
#include "LADataStreamCorePrivatePCH.h"
#include <xsens/xsmath.h>
TMap<FString, ULAXSensStream*> ULAXSensStream::Streams;


namespace XS {

	XSensChar::XSensChar(int id, FString name)
		:Id(id), Name(name)
	{
		
	}

	void XSensChar::Deserialize(Datagram* data)
	{
		if (!data)
		{
			//print log message
			return;
		}

		FrameTime = data->frameTime();

		StreamingProtocol type = static_cast<StreamingProtocol>(data->messageType());
		switch (type)
		{
		case StreamingProtocol::SPPoseQuaternion://1
		{
			QuaternionDatagram* Qdg = static_cast<QuaternionDatagram*>(data);
			if (Qdg)
			{
				parseQuaternion(Qdg);
			}
			break;
		}
		case StreamingProtocol::SPTimeCode://2
		{
			TimeCodeDatagram* TCdg = static_cast<TimeCodeDatagram*>(data);
			if (TCdg)
			{
				parseTimeCode(TCdg);
			}
			break;
		}
		case StreamingProtocol::SPMetaMoreMeta://3
		{
			MetaDatagram* Metadg = static_cast<MetaDatagram*>(data);
			if (Metadg)
			{
				parseMeta(Metadg);
			}
			break;
		}
		case StreamingProtocol::SPJointAngles://3
		{
			JointAnglesDatagram* JAdg = static_cast<JointAnglesDatagram*>(data);
			if (JAdg)
			{
				parseJointAngle(JAdg);
			}
			break;
		}
		}

	}

	void XSensChar::parseQuaternion(QuaternionDatagram* datagram)
	{
		QuaternionDatagram::Kinematics kin;
		const int size = GlobalPose.Num();
		if (size <= 0)
		{
			GlobalPose.SetNum(datagram->dataCount());
			LocalPose.SetNum(datagram->dataCount());
		}
		else
		{
			for (int i = 0; i < GlobalPose.Num(); i++)
			{
				kin = datagram->getSegmentData(i);
				FTransform tran;
				tran.SetTranslation(FVector(kin.sensorPos[0], kin.sensorPos[1], kin.sensorPos[2]));
				FQuat rot(XsMath_deg2rad(kin.quatRotation[1]),XsMath_deg2rad(kin.quatRotation[2]), XsMath_deg2rad(kin.quatRotation[3]), XsMath_deg2rad(kin.quatRotation[0]));
				rot.Normalize();
				tran.SetRotation(rot);
				ConvertTransformToFromMaya(tran);
				GlobalPose[kin.segmentId-1] = tran;
			}
		}
		BuildParentHierarchy(LocalPose.Num());
		//Get local transforms
		for (int i = 0; i < LocalPose.Num(); i++)
		{
			if (ParentId[i] != -1)
			{
				LocalPose[i] = GlobalPose[i] * GlobalPose[ParentId[i]].Inverse();
			}
			else
			{
				LocalPose[i] = GlobalPose[i];
			}
		}
	}
	void XSensChar::BuildParentHierarchy(int boneCount) {
		int NumOfProps = 0;
		if (boneCount <= 27) {
			NumOfProps = boneCount - 23;
		}
		else/*we have fingers*/{
			NumOfProps = boneCount - 63;
		}

		
		int leftCarpusBaseID = 23;
		int rightCarpusBaseID = 43;

		ParentId.SetNum(63 + NumOfProps);
		ParentId[0] = -1; //Pelvis
		ParentId[1] = 0; //L5
		ParentId[2] = 1; //L3
		ParentId[3] = 2; //T12
		ParentId[4] = 3; //Sternum (T8)
		ParentId[5] = 4; //Neck
		ParentId[6] = 5; //Head

		ParentId[7] = 4; //RightShoulder
		ParentId[8] = 7; //RightUpperArm
		ParentId[9] = 8; //RightForeArm
		ParentId[10] = 9; //RightHand

		ParentId[11] = 4; //LeftShoulder
		ParentId[12] = 11; //LeftUpperArm
		ParentId[13] = 12; //LeftForeArm
		ParentId[14] = 13; //LeftHand

		ParentId[15] = 0; //RightUpperLeg
		ParentId[16] = 15;//RightLowerLeg
		ParentId[17] = 16;//RightFoot
		ParentId[18] = 17;//RightToe

		ParentId[19] = 0;//LeftUpperLeg
		ParentId[20] = 19;//LeftLowerLeg
		ParentId[21] = 20;//LeftFoot
		ParentId[22] = 21;//LeftToe

		//parent all props to root (in case the figure is moved in the Maya scene)
		if (NumOfProps != 0) {
			for (int i = 0; i < NumOfProps; i++) {
				ParentId[22 + i + 1] = -1;
			}
		}

		//Fingers (Thumb does not have a middle phalange)
		//Left
		ParentId[leftCarpusBaseID + 0 + NumOfProps] = 14;//Carpus (23 if no props)
			//Thumb
		ParentId[leftCarpusBaseID + 1 + NumOfProps] = leftCarpusBaseID + 0 + NumOfProps;//First Metacarpal
		ParentId[leftCarpusBaseID + 2 + NumOfProps] = leftCarpusBaseID + 1 + NumOfProps;//First Proximal Phalange
		ParentId[leftCarpusBaseID + 3 + NumOfProps] = leftCarpusBaseID + 2 + NumOfProps;//First Distal Phalange
			//Rest of fingers
		for (int i = 0; i <= 15; i++) {
			if (i % 4 == 0) {//Then it's a metacarpal, parent to carpus
				ParentId[leftCarpusBaseID + i + 4 + NumOfProps] = leftCarpusBaseID + 0 + NumOfProps;
			}
			else {//else parent it to the prev bone
				ParentId[leftCarpusBaseID + i + 4 + NumOfProps] = leftCarpusBaseID + (i + 3) + NumOfProps;
			}
		}
		//Right
		ParentId[rightCarpusBaseID + 0 + NumOfProps] = 10;//Carpus (43 if no props)
			//Thumb
		ParentId[rightCarpusBaseID + 1 + NumOfProps] = rightCarpusBaseID + 0 + NumOfProps;//First Metacarpal
		ParentId[rightCarpusBaseID + 2 + NumOfProps] = rightCarpusBaseID + 1 + NumOfProps;//First Proximal Phalange
		ParentId[rightCarpusBaseID + 3 + NumOfProps] = rightCarpusBaseID + 2 + NumOfProps;//First Distal Phalange
			//Rest of fingers
		for (int i = 0; i <= 15; i++) {
			if (i % 4 == 0) {//Then it's a metacarpal, parent to carpus
				ParentId[rightCarpusBaseID + i + 4 + NumOfProps] = rightCarpusBaseID + 0 + NumOfProps;
			}
			else {//else parent it to the prev bone
				ParentId[rightCarpusBaseID + i + 4 + NumOfProps] = rightCarpusBaseID + (i + 3) + NumOfProps;
			}
		}
	}
	void XSensChar::parseJointAngle(JointAnglesDatagram* datagram)
	{
		int count = 0;
		JointAnglesDatagram::Joint* joints = datagram->getData(count);

		for (int i = 0; i < count; i++)
		{
			GlobalPose.Push(FTransform::Identity);
			LocalPose.Push(FTransform::Identity);

			ParentId.Push(joints[i].parent);
		}

	}

	void XSensChar::parseTimeCode(TimeCodeDatagram* datagram)
	{
		TimeCode.m_nano = datagram->getNano();
		TimeCode.m_hour = datagram->getHour();
		TimeCode.m_minute = datagram->getMinute();
		TimeCode.m_second = datagram->getSecond();
	}

	void XSensChar::parseMeta(const MetaDatagram * datagram)
	{
		FString item("name");
		TStringConversion<TStringConvert <TCHAR, ANSICHAR>> b(*item);
		if (datagram->hasItem(b.Get()))
		{
			Name = datagram->itemData(b.Get()).c_str();
		}
	}


}


ULAXSensStream::ULAXSensStream(const FObjectInitializer& PCIP)
: Super(PCIP)
{
	iDataPort = 1511;
	connected_client = false;
	is_realtime = true;

	//Populate the Segment Names, and parent IDs.

	SegmentNameById.Push("PELVIS");
	SegmentNameById.Push("L5");
	SegmentNameById.Push("L3");				//!< Segment id of the L3 segment
	SegmentNameById.Push("T12");
	SegmentNameById.Push("T8");
	SegmentNameById.Push("NECK");
	SegmentNameById.Push("HEAD");
	SegmentNameById.Push("RIGHT_SHOULDER");
	SegmentNameById.Push("RIGHT_UPPER_ARM");
	SegmentNameById.Push("RIGHT_FORE_ARM");
	SegmentNameById.Push("RIGHT_HAND");



	SegmentNameById.Push("LEFT_SHOULDER");
	SegmentNameById.Push("LEFT_UPPER_ARM");
	SegmentNameById.Push("LEFT_FORE_ARM");
	SegmentNameById.Push("LEFT_HAND");



	SegmentNameById.Push("RIGHT_UPPER_LEG");
	SegmentNameById.Push("RIGHT_LOWER_LEG");
	SegmentNameById.Push("RIGHT_FOOT");
	SegmentNameById.Push("RIGHT_TOE");
	SegmentNameById.Push("LEFT_UPPER_LEG");
	SegmentNameById.Push("LEFT_LOWER_LEG");
	SegmentNameById.Push("LEFT_FOOT");
	SegmentNameById.Push("LEFT_TOE");
	if (NumOfProps != 0) {
		for (int i = 0; i < NumOfProps; i++) {
			SegmentNameById.Push("PROP");
		}
	}
	//SegmentNameById.Push("PROP0");
	//SegmentNameById.Push("PROP1");
	//SegmentNameById.Push("PROP2");
	//SegmentNameById.Push("PROP3");
	SegmentNameById.Push("LEFT_Carpus");
	SegmentNameById.Push("LEFT_First_Metacarpal");
	SegmentNameById.Push("LEFT_First_Proximal_Phalange");
	SegmentNameById.Push("LEFT_First_Distal_Phalange");
	SegmentNameById.Push("LEFT_Second_metacarpal");
	SegmentNameById.Push("LEFT_second_Proximal_Phalange");
	SegmentNameById.Push("LEFT_Second_middle_phalange");
	SegmentNameById.Push("LEFT_Second_distal_phalange");
	SegmentNameById.Push("LEFT_Third_metacarpal");
	SegmentNameById.Push("LEFT_Third_Proximal_phalange");
	SegmentNameById.Push("LEFT_Third_Middle_phalange");
	SegmentNameById.Push("LEFT_Third_distal_phalange");
	SegmentNameById.Push("LEFT_Fourth_metacarpal");
	SegmentNameById.Push("LEFT_Fourth_proximal_phalange");
	SegmentNameById.Push("LEFT_Fourth_Middle_Phalange");
	SegmentNameById.Push("LEFT_Fourth_Distal_phalange");
	SegmentNameById.Push("LEFT_Fifth_Metacarpal");
	SegmentNameById.Push("LEFT_Fifth_Proximal_Phalange");
	SegmentNameById.Push("LEFT_Fifth_Middle_Phalange");
	SegmentNameById.Push("LEFT_Fifth_Distal_Phalange");

	SegmentNameById.Push("RIGHT_Carpus");
	SegmentNameById.Push("RIGHT_First_Metacarpal");
	SegmentNameById.Push("RIGHT_First_Proximal_Phalange");
	SegmentNameById.Push("RIGHT_First_Distal_Phalange");
	SegmentNameById.Push("RIGHT_Second_metacarpal");
	SegmentNameById.Push("RIGHT_second_Proximal_Phalange");
	SegmentNameById.Push("RIGHT_Second_middle_phalange");
	SegmentNameById.Push("RIGHT_Second_distal_phalange");
	SegmentNameById.Push("RIGHT_Third_metacarpal");
	SegmentNameById.Push("RIGHT_Third_Proximal_phalange");
	SegmentNameById.Push("RIGHT_Third_Middle_phalange");
	SegmentNameById.Push("RIGHT_Third_distal_phalange");
	SegmentNameById.Push("RIGHT_Fourth_metacarpal");
	SegmentNameById.Push("RIGHT_Fourth_proximal_phalange");
	SegmentNameById.Push("RIGHT_Fourth_Middle_Phalange");
	SegmentNameById.Push("RIGHT_Fourth_Distal_phalange");
	SegmentNameById.Push("RIGHT_Fifth_Metacarpal");
	SegmentNameById.Push("RIGHT_Fifth_Proximal_Phalange");
	SegmentNameById.Push("RIGHT_Fifth_Middle_Phalange");
	SegmentNameById.Push("RIGHT_Fifth_Distal_Phalange");

}


ELAResult ULAXSensStream::Connect(FString server_ip, int portNumber)
{

	FLADataStreamModule& module = FModuleManager::GetModuleChecked< FLADataStreamModule >("LADataStreamCore");
	if (!module.licValid())
	{
		return ELAResult::ELAError;
	}

	if (IsValidLowLevel())
	{
		if (isConnected())
		{
			Disconnect();
		}
		//TODO: Connect
		_server_ip = server_ip;
		m_port = portNumber;
		m_socket.Reset();
		m_socket = TUniquePtr<XsSocket>(new XsSocket(IpProtocol::IP_UDP, NetworkLayerProtocol::NLP_IPV4));

		XsResultValue res = m_socket->bind(*_server_ip, m_port);
		if (res == XRV_OK)
		{
			startRead(500);
			bIsInitialized = InitialiseCharacter();
			//TODO: start parsing datagrams thread call startRead in a thread somehow :)
			if (!Thread)
			{
				static int ThreadIndex = 0;
				FString ThreadName = FString::Printf(TEXT("Streamer_%d"), ThreadIndex);
				ThreadIndex++;
				KillThread = false;
				Thread = FRunnableThread::Create(this, *ThreadName, 0, EThreadPriority::TPri_Normal, FPlatformAffinity::GetNoAffinityMask());
			}
		
		}
	}
	return ELAResult::ELASuccess;
}

uint32 ULAXSensStream::Run()
{
	while (!KillThread && isConnected())
	{
		startRead();
		FPlatformProcess::Sleep(0.0f);
	}
	if (KillThread)
	{
		Stop();
	}
	return 0;
}
Datagram* ULAXSensStream::createDgram(StreamingProtocol proto)
{
	switch (proto)
	{
		case SPPoseQuaternion:	return new QuaternionDatagram;
		case SPMetaMoreMeta:	return new MetaDatagram;
		case SPJointAngles:		return new JointAnglesDatagram;
		case SPTimeCode:		return new TimeCodeDatagram;
		default:				return nullptr;
	}
}

void ULAXSensStream::startRead(int TimeOut)
{
	//Start reading data.
	XsByteArray buffer;
	{
		{
			FScopeLock lock(&section);
			if(m_socket.IsValid())
				m_socket->read(buffer, TimeOut);
		}
		if (buffer.size() > 0)
		{
			//Buffer the datagram;
			StreamingProtocol type = static_cast<StreamingProtocol>(Datagram::messageType(buffer));
			Datagram *datagram = createDgram(type);

			if (datagram != nullptr)
			{
				datagram->deserialize(buffer);

				int avatarId = datagram->avatarId();
				if (bIsInitialized && Characters.Contains(avatarId))
				{
					Characters[avatarId].Deserialize(datagram);
					genericTime.hours = Characters[avatarId].TimeCode.m_hour;
					genericTime.minutes = Characters[avatarId].TimeCode.m_minute;
					genericTime.seconds = Characters[avatarId].TimeCode.m_second;
					genericTime.frame = Characters[avatarId].FrameTime;
					delete datagram;
				}
				else
				{
					//Queue for initialization
					frameStore.Enqueue(datagram);
				}

			}

		}
		buffer.clear();
	}
}

bool ULAXSensStream::InitialiseCharacter(void)
{
	Datagram* currentDatagram;
	bool init = false;
	int iterations = 0;
	static int numberofpackets = 0;
	while (!init)
	{
		iterations++;
		if (iterations > 10)
		{
			break;
		}
		while (!frameStore.IsEmpty() && frameStore.Dequeue(currentDatagram))
		{
			numberofpackets++;
			//TODO: exit after some time
			if (currentDatagram)
			{
				if (currentDatagram->messageType() == StreamingProtocol::SPMetaMoreMeta && !AvatarNameMap.Contains(currentDatagram->avatarId()))
				{
					if (!Characters.Contains(currentDatagram->avatarId()))
					{
						XS::XSensChar avatar((int)currentDatagram->avatarId(), "");
						avatar.Deserialize(currentDatagram);
						Characters.Add(currentDatagram->avatarId(), avatar);
					}
					Characters[currentDatagram->avatarId()].Deserialize(currentDatagram);
					AvatarNameMap.Add(currentDatagram->avatarId(), Characters[currentDatagram->avatarId()].Name);
					init = false;
				}
				else
				{
					if (!Characters.Contains(currentDatagram->avatarId()))
					{
						XS::XSensChar avatar((int)currentDatagram->avatarId(), "");
						avatar.Deserialize(currentDatagram);
						Characters.Add(currentDatagram->avatarId(), avatar);
						init = false;
					}
					else
					{
						Characters[currentDatagram->avatarId()].Deserialize(currentDatagram);
						init = true;
					}

				}
			}
		}
	}
	return init;
}

ELAResult ULAXSensStream::UseMulticast(FString multicast_ip)
{
	return ELAResult::ELAError;
}

ELAResult ULAXSensStream::Disconnect()
{
	
	if (m_socket.IsValid())
	{
		{
			//Release reference to the XsSocket to close it.		
			FScopeLock lock(&section);
			m_socket.Reset();
		}
		
		//TODO: Disconnect
		KillThread = true;
		if (Thread)
		{
			Thread->Kill(true);
			delete Thread;
			Thread = nullptr;
		}
	}
	return ELAResult::ELASuccess;
}

ELAResult ULAXSensStream::GetSubjectCount(int& count)
{
	count = Characters.Num();
	return ELAResult::ELASuccess;
}

ELAResult ULAXSensStream::GetRigidBodyTransform(const FString& name, FTransform& trans)
{
	//TODO: Extract Prop from 
	FString LeftString, RightString;
	if (name.Split("Prop", &LeftString, &RightString))
	{
		int32 PropIndex = -1;
		LexFromString(PropIndex, *RightString);
		int charIndex = PropIndex / 4;
		auto XSensChar = Characters.Find(charIndex);
		if (XSensChar)
		{
			FString xsensname("Prop");
			int i = PropIndex % 4;
			xsensname += FString::FromInt(PropIndex % 4);
			int pr = GetSegmentIndex(xsensname);
			trans = XSensChar->GlobalPose[pr];
			return ELAResult::ELASuccess;
		}

	}
	return ELAResult::ELAError;
}


ELAResult ULAXSensStream::GetSubjectName(int index, FString& name)
{
	name = AvatarNameMap[index];
	name.ReplaceInline(ANSI_TO_TCHAR("-"), ANSI_TO_TCHAR("_"));
	return ELAResult::ELASuccess;
}

ELAResult ULAXSensStream::GetSegmentCountForSubject(const FString& subjectName, int& count)
{
	int ind = GetSubjectIndex(subjectName);
	count = -1;

	auto XSensChar = Characters.Find(ind);
	if (XSensChar)
	{
		count = XSensChar->LocalPose.Num();
		return ELAResult::ELASuccess;
	}
	UE_LOG(LogLADataStreamCore, Warning, TEXT("Invalid Subject name or Character ID"));
	return ELAResult::ELAError;
}

ELAResult ULAXSensStream::GetSegmentNameForSubject(const FString& subjectName, int index, FString& segName)
{
	//TODO: SegmentNames.
	int ind = GetSubjectIndex(subjectName);
	if (ind == -1)
	{
		UE_LOG(LogLADataStreamCore, Warning, TEXT("Invalid Subject name"));
		return ELAResult::ELAError;
	}
	segName = SegmentNameById[index];
	segName.ReplaceInline(ANSI_TO_TCHAR("-"), ANSI_TO_TCHAR("_"));
	return ELAResult::ELASuccess;
	
}

ELAResult ULAXSensStream::GetSegmentParentNameForSubject(const FString& subjectName, FString segName, FString& parentSegName)
{
	int SubIndex, SegIndex, parentID;
	SubIndex = GetSubjectIndex(subjectName);
	if (SubIndex == -1)
	{
		UE_LOG(LogLADataStreamCore, Warning, TEXT("Invalid Subject name"));
		return ELAResult::ELAError;
	}

	SegIndex = GetSegmentIndex(segName);
	if (SegIndex == -1 || (!Characters.Contains(SubIndex)))
	{
		parentSegName = "";
		UE_LOG(LogLADataStreamCore, Warning, TEXT("Invalid Subject name"));
		return ELAResult::ELAError;
	}
	parentID = Characters[SubIndex].ParentId[SegIndex];
	if (SegmentNameById.IsValidIndex(parentID))
	{
		parentSegName = SegmentNameById[parentID];
		return ELAResult::ELASuccess;

	}
	return ELAResult::ELAError;
}

ELAResult ULAXSensStream::GetSegmentParentIndexForSubject(FString& subjectName, FString segName, int& parentSegName)
{

	int SegIndex;
	int SubIndex = GetSubjectIndex(subjectName);
	SegIndex = GetSegmentIndex(segName);
	if (SegIndex == -1 || (!Characters.Contains(SubIndex)))
	{
		
		return ELAResult::ELAError;
	}

	parentSegName = Characters[SubIndex].ParentId[SegIndex];
	return ELAResult::ELASuccess;

}

int ULAXSensStream::GetSegmentIndex( FString SegmentName)
{
	
	int index = SegmentNameById.IndexOfByPredicate([&SegmentName](const FString &name){return SegmentName == name; });
	return index;
}

int ULAXSensStream::GetSubjectIndex(const FString& SubjectName)
{
	if (SubjectName.IsEmpty())
		return -1;
	
	if (AvatarNameMap.FindKey(SubjectName))
	{
		return *AvatarNameMap.FindKey(SubjectName);
	}
	
	TDefaultNumericTypeInterface<int> Conversion;
	auto optional = Conversion.FromString(SubjectName, 0);
	int id = optional.Get(-1) - 1;
	
	return id;
}

bool ULAXSensStream::GetPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose)
{
	if (outPose.Num() == 0)
	{
		return false;
	}
	{
		//DECLARE_SCOPE_CYCLE_COUNTER(TEXT("GetNatNetFrame"), STAT_StatsOptitrackNatNetFrame, STATGROUP_OptiTrack);
		if (GetFrame() != ELAResult::ELASuccess) return false;
	}
	DECLARE_SCOPE_CYCLE_COUNTER(TEXT("LocalPose"), STAT_StatsOptitrackLocalPose, STATGROUP_OptiTrack);
	int segCount;
	GetSegmentCountForSubject(subjectName, segCount);
	int SubIndex = GetSubjectIndex(subjectName);
	if (Characters[SubIndex].LocalPose.Num() < 0)
		return false;
	{
		FScopeLock lock(&section);
		TArray<FTransform>& localSeg = Characters[SubIndex].LocalPose;
		for (int j = 0; j < segCount; ++j)
		{
			outPose[j] = localSeg[j];
		}
	}
	return true;
}


ELAResult ULAXSensStream::GetSegmentLocalRotationQuaternion(FString SubjectName, FString SegmentName, FQuat& t)
{

	int SegIndex;
	int SubIndex = GetSubjectIndex(SubjectName);
	if ((!Characters.Contains(SubIndex)))
	{
		UE_LOG(LogLADataStreamCore, Warning, TEXT("Wrong Subject name"));
		return ELAResult::ELAError;
	}

	SegIndex = GetSegmentIndex(SegmentName);
	if (SegIndex == -1)
	{
		UE_LOG(LogLADataStreamCore, Warning, TEXT("Wrong Segment name"));
		return ELAResult::ELAError;
	}
	
	
	return ELAResult::ELAError;
}

ELAResult ULAXSensStream::GetSegmentLocalTranslation(FString SubjectName, FString SegmentName, FVector& t)
{
	
	int SegIndex;
	int SubIndex = GetSubjectIndex(SubjectName);
	if ((!Characters.Contains(SubIndex)))
	{
		UE_LOG(LogLADataStreamCore, Warning, TEXT("Wrong Subject name"));
		return ELAResult::ELAError;
	}

	SegIndex = GetSegmentIndex(SegmentName);
	if (SegIndex == -1)
	{
		UE_LOG(LogLADataStreamCore, Warning, TEXT("Wrong Segment name"));
		return ELAResult::ELAError;
	}



	return ELAResult::ELAError;
}

bool ULAXSensStream::GetGlobalPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose)
{
	DECLARE_SCOPE_CYCLE_COUNTER(TEXT("GlobalPose"), STAT_StatsOptitrackGlobalPose, STATGROUP_OptiTrack);
	if (outPose.Num() == 0)
	{
		return false;
	}
	
	int segCount;
	int SubIndex = GetSubjectIndex(subjectName);
	if (Characters[SubIndex].GlobalPose.Num() < 0)
		return false;
	
	
	GetSegmentCountForSubject(subjectName,segCount);
	{
		FScopeLock lock(&section);
		TArray<FTransform>& globalSeg = Characters[SubIndex].GlobalPose;
		for (int j = 0; j < segCount; ++j)
		{
			outPose[j + segCount] = globalSeg[j];
		}
	}
	return true;
}


ULAXSensStream* ULAXSensStream::Get(const FString& serverIP)
{
	auto stream = Streams.Find(serverIP);
	if (stream)
	{
		if ((*stream)->IsValidLowLevel())
		{
			(*stream)->RefCount++;
			return *stream;
		}
		else
		{
			Streams.Remove(serverIP);
		}
	}
	auto newStream = NewObject<ULAXSensStream>();
	newStream->AddToRoot();
	newStream->RefCount = 1;
	Streams.Add(serverIP);
	Streams[serverIP] = newStream;

	return newStream;

}

void ULAXSensStream::Remove(const FString serverIP)
{
	auto stream = Streams.Find(serverIP);
	if (stream)
	{
		if ((*stream)->IsValidLowLevel())
		{
			(*stream)->RefCount--;

			if ((*stream)->RefCount <= 0)
			{
				(*stream)->Stop();
				(*stream)->Disconnect();
				(*stream)->RemoveFromRoot();
				Streams.Remove(serverIP);
			}
		}
	}
}

void ULAXSensStream::Destory()
{
	if (this->IsValidLowLevel())
	{
		RefCount--;
		if (RefCount <= 0)
		{
			Disconnect();
			if (Thread)
			{
				delete Thread;
				Thread = nullptr;
			}
			ConditionalBeginDestroy(); //instantly clears UObject out of memory
			RemoveFromRoot();
		}
		
	}
}

void ULAXSensStream::BeginDestroy()
{
	RefCount--;
	if (RefCount <= 0)
	{
		Disconnect();
		Super::BeginDestroy();
	}
	Super::BeginDestroy();
}

void ULAXSensStream::FinishDestroy()
{
	if (RefCount <= 0)
	{
		Super::FinishDestroy();
	}
	Super::FinishDestroy();
}

bool ULAXSensStream::isConnected()
{
	//TODO:

	FScopeLock lock(&section);
	if (!KillThread && m_socket.IsValid())
		return m_socket->isUsable();
	return false;
}

ELAResult ULAXSensStream::GetFrame()
{
	Datagram* currentDatagram = nullptr;
	if (is_realtime)
	{
		if (!bIsInitialized)
		{
			bIsInitialized = InitialiseCharacter();
		}
		/*{
			FScopeLock lock(&section);
			while (!frameStore.IsEmpty() && frameStore.Peek(currentDatagram))
			{

				frameStore.Dequeue(currentDatagram);
				if (currentDatagram)
				{

					int avatarId = currentDatagram->avatarId();
					if (Characters.Contains(avatarId))
					{
						Characters[avatarId].Deserialize(currentDatagram);
						delete currentDatagram;
					}

				}
			}
		}*/
	}
	
	
	//TODO
	return ELAResult::ELASuccess;
}
