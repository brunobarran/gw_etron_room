/* Copyright (C) 2006-2017, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/


#include "MocapActorSource.h"
#include "LADataStreamCorePrivatePCH.h"


UActorSkeleton::UActorSkeleton(class FObjectInitializer const & ObjectInitializer)
	:Super(ObjectInitializer)
{

}

UActorSkeleton::UActorSkeleton(int NumOFBones)
{
	Bones.Reserve(NumOFBones);
	NameIndexMap.Reserve(NumOFBones);
}



void UActorSkeleton::AddBone(const FName& Name,const FName& ParentName, const FTransform& transform /*= FTransform::Identity*/ )
{
	FMocapBone bone;
	bone.Name = Name;
	bone.ParentName = ParentName;
	bone.Dirty = false;
	bone.bIsSelected = false;
	bone.LocalTransform = bone.GlobalTransform = transform;
	Bones.Push(bone);

	NameIndexMap.Add(Name, Bones.Num() - 1);
}

const FMocapBone UActorSkeleton::GetBone(int index) const
{
	if (!Bones.IsValidIndex(index))
	{
		return FMocapBone();
	}
	check(Bones.Num() > 0)
	return Bones[index];
}
const FMocapBone UActorSkeleton::GetBone(FName Name) const
{
	if (Bones.Num() < 0 || !NameIndexMap.Contains(Name))
	{
		return FMocapBone();
	}
	check(NameIndexMap.Contains(Name))
	check(Bones.Num() > 0)
	return Bones[NameIndexMap[Name]];
}

FMocapBone& UActorSkeleton::GetBone(int index)
{
	check(index != INDEX_NONE)
	return Bones[index];
}
FMocapBone& UActorSkeleton::GetBone(const FName& Name)
{
	check(NameIndexMap.Contains(Name))
	check(Bones.Num() > 0)
	return Bones[NameIndexMap[Name]];
}

const TArray<FMocapBone>& UActorSkeleton::GetBones() const
{
	return Bones;
}

int UActorSkeleton::GetBoneIndex(const FName& Name)
{
	if ( !NameIndexMap.Contains(Name))
	{
		return INDEX_NONE;
	}
	check(NameIndexMap.Contains(Name))
	return NameIndexMap[Name];
}

void UActorSkeleton::DoFK(const int Index)
{
	for (int32 BoneIndex = Index;  BoneIndex  < Bones.Num(); ++BoneIndex)
	{
		// root is already verified, so root should not come here
		// check AllocateLocalPoses
		const int32 ParentIndex = GetParentBoneIndex(BoneIndex);//BoneContainer->GetParentBoneIndex(BoneIndex);
		auto& bone = Bones[BoneIndex];
		// convert back 
		if (ParentIndex != INDEX_NONE)
		{
			auto& parentBone = Bones[ParentIndex];
			bone.GlobalTransform = bone.LocalTransform * parentBone.GlobalTransform ;
		}
		else
		{
			bone.GlobalTransform = bone.LocalTransform;// *SourceTransform;
		}
	}
}

void UActorSkeleton::PopulateTransforms(const TArray<FTransform>& Pose, bool ZeroRotations /* = false*/)
{
	//TODO: arrays are empty
	if (Bones.Num() > 0)
	{
		for (int i = 0; i < Bones.Num(); i++)
		{
			Bones[i].LocalTransform = Pose[i];
			Bones[i].GlobalTransform = Pose[Bones.Num() + i];// * SourceTransform;
			if (ZeroRotations)
			{
				Bones[i].LocalTransform.SetRotation(FQuat::Identity);
				Bones[i].GlobalTransform.SetRotation(FQuat::Identity);
			}
		}
		Bones[0].LocalTransform = Bones[0].LocalTransform;// *  SourceTransform;
	}
	else
	{
		return;
	}
}

float UActorSkeleton::UpdateScale(TArray<FTransform>& Pose)
{
	//calculate leg lenghts
	float CurrentLegLength = 0.f;
	if (LegBones.Num() > 0)
	{
		for (const auto& boneName : LegBones)
		{
			auto boneIndex = GetBoneIndex(boneName);
			CurrentLegLength += Pose[boneIndex].GetTranslation().Size();
		}
	}

	auto delta = ((CurrentLegLength)/LegLength);
	//return 1.f / delta;
	float scale = 1.f / delta;
	if (FMath::Abs(1.f - delta) > 0.01f)
	{
		
		for (auto& bone : Pose)
		{
			auto vec = bone.GetTranslation();
			vec *= scale;
			bone.SetTranslation(vec);
		}
	}

	return scale;
}

int UActorSkeleton::GetParentBoneIndex(int index)
{
	auto parentName = Bones[index].ParentName;
	if (parentName == NAME_None)
	{
		return INDEX_NONE;
	}
	return NameIndexMap[parentName];
}


int UActorSkeleton::GetParentBoneIndex(FName name)
{
	return GetParentBoneIndex(NameIndexMap[name]);
}

int UActorSkeleton::NumOfBones()
{
	return Bones.Num();
}
