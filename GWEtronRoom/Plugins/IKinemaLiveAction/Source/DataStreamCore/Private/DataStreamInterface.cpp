/* Copyright (C) 2006-2017, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/



#include "DataStreamInterface.h"
#include "LADataStreamCorePrivatePCH.h"
//////////////////////////////////////////////////////////////////////////
// ToStringInterface

UDataStreamInterface::UDataStreamInterface(const FObjectInitializer& PCIP)
: Super(PCIP)
{

}

IDataStreamInterface * IDataStreamInterface::Get(const FString & ServerIP, EMocapServer ServerType)
{
	switch (ServerType)
	{
	case EMocapServer::VICON:
		return (ULAViconStream::Get(ServerIP));
		break;
	case EMocapServer::NatNet:
		return (ULAOptiTrackStream::Get(ServerIP));
		break;
	case EMocapServer::XSens:
		return (ULAXSensStream::Get(ServerIP));
		break;
	case EMocapServer::VRPN:
		return (ULAVRPNStream::Get(ServerIP));
	case None:
	default:
		break;
	}

	return nullptr;
}

void IDataStreamInterface::Remove(IDataStreamInterface* Stream)
{
	auto ServerType = Stream->GetType();
	FString ServerIP = Stream->GetServerIP();
	if (ServerIP == "")
		return;
	switch (ServerType)
	{
	case EMocapServer::VICON:
		(ULAViconStream::Remove(ServerIP));
		break;
	case EMocapServer::NatNet:
		(ULAOptiTrackStream::Remove(ServerIP));
		break;
	case EMocapServer::XSens:
		(ULAXSensStream::Remove(ServerIP));
		break;
	case VRPN:
		ULAVRPNStream::Remove(ServerIP);
		break;
	case None: break;
	default: break;
	}
}
