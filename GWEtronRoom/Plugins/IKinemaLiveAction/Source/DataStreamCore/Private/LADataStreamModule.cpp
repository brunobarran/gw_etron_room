/* Copyright (C) 2006-2017, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "LADataStreamModule.h"
#include "LADataStreamCorePrivatePCH.h"
#include "Interfaces/IPluginManager.h"



#define RB_OPTION_STRING "RigidBody"

HMODULE ViconDLL = nullptr;
HMODULE XSensTypeDLL = nullptr;
HMODULE PhaseSpaceDLL = nullptr;
HMODULE NatNetDLL = nullptr;

bool LoadStreamingDLLs()
{
	FString path;
	FString PluginsPath = IPluginManager::Get().FindPlugin("IKinemaLiveAction")->GetBaseDir();
	
	
	if (ViconDLL == NULL)
	{
		path = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(PluginsPath / TEXT("Source/ThirdParty/ViconDataStreamSDK/DataStreamSDK/boost_thread-vc90-mt-1_48.dll")));
		ViconDLL = LoadLibraryW(*(path));
		path = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*( PluginsPath / TEXT("Source/ThirdParty/ViconDataStreamSDK/DataStreamSDK/ViconDataStreamSDK_CPP.dll")));
		ViconDLL = LoadLibraryW(*(path));
		if (!ViconDLL)
		{
			UE_LOG(LogLADataStreamCore, Fatal, TEXT("Failed to load Vicon DataStream DLLs from %s. %i"), *path, GetLastError());
			return false;
		}
	}


	if (XSensTypeDLL == NULL)
	{
		path = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(PluginsPath / TEXT("Source/ThirdParty/IKinema/CoreSolver/sdk/lib/Win64/IKinema.dll")));
		XSensTypeDLL = LoadLibraryW(*(path));
		if (!XSensTypeDLL)
		{
			UE_LOG(LogLADataStreamCore, Fatal, TEXT("Failed to load Xsens Type Dlls."));
			return false;
		}
	}

	path = ("../../IKinemaLiveAction/Source/ThirdParty/PhaseSpace/PSClientSDK/lib/libowlsock.dll");

	PhaseSpaceDLL = LoadLibraryW(*(path));
	if (PhaseSpaceDLL == NULL)
	{
		path = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(PluginsPath / TEXT("Source/ThirdParty/PhaseSpace/PSClientSDK/lib/libowlsock.dll")));
		PhaseSpaceDLL = LoadLibraryW(*(path));
		if (!PhaseSpaceDLL)
		{
			UE_LOG(LogLADataStreamCore, Fatal, TEXT("Failed to load PhaseSpace DLLs."));
			return false;
		}
	}

	path = ("../../IKinemaLiveAction/Source/ThirdParty/NatNet/NatNetSDK/lib/x64/NatNetLib.dll");
	NatNetDLL = LoadLibraryW(*(path));
	if (NatNetDLL == NULL)
	{
		path = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(PluginsPath / TEXT("Source/ThirdParty/NatNet/NatNetSDK/lib/x64/NatNetLib.dll")));
		NatNetDLL = LoadLibraryW(*(path));
		if (!NatNetDLL)
		{
			UE_LOG(LogLADataStreamCore, Fatal, TEXT("Failed to load NatNet DLLs."));
			return false;
		}
	}


	return true;

}



void FLADataStreamModule::StartupModule()
{

	if (!LoadStreamingDLLs())
	{
		//UE_LOG(LogLADataStreamCore, Fatal, TEXT("Failed to load DLLs."));
	}
	//FString licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(TEXT("../../Binaries/ThirdParty/IKinema/license"));
	licenseRelativeFolder = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*(FPaths::ConvertRelativePathToFull(FPaths::EngineDir() + TEXT("Binaries/ThirdParty/IKinema/license"))));
	
	isTrail = true;
	char* errorMessage = nullptr;
	errorMessage = RlmCheckPoint(TCHAR_TO_ANSI(*licenseRelativeFolder));
	if (errorMessage)
	{
		isValid = false;
		UE_LOG(LogLADataStreamCore, Error, TEXT("DataStream startup error. License not found or expired.  %s"), ANSI_TO_TCHAR(errorMessage));
		FMessageDialog::Open(EAppMsgType::Ok, NSLOCTEXT("UnrealEd", "Error_IKinemaLicense","IKINEMA LiveAction license is not found or expired. Please contact support@ikinema.com with contents of the UE4 log"));
	}
	else
	{

		//Create Directory.
		auto keyFile = licenseRelativeFolder;
		keyFile += "/ikey";
		TArray<FString> Key;
		FString TrailKey("4d077e63e999f6.20544082");
		bool haveKey = FFileHelper::LoadANSITextFileToStrings(*keyFile, &IFileManager::Get(), Key);
		if (haveKey)
		{
			for (auto key : Key)
			{
				if (key == TrailKey)
				{
					isTrail = true;
				}
				else
				{
					isTrail = false;
				}
			}
		}
		//Get the options
		isValid = true;
		char* options = RlmGetOptions();
		FString OptionsString(options);
		if (OptionsString.Contains(RB_OPTION_STRING))
		{
			bRigidBodyLic = true;
		}
		RlmStopFloatingLicensing();
	}
	
	
}

bool FLADataStreamModule::IsTrail() const
{
	return isTrail;
}
void FLADataStreamModule::ShutdownModule()
{
	if (ViconDLL)
	{
		FreeLibrary(ViconDLL);
	}
	if (XSensTypeDLL)
	{
		FreeLibrary(XSensTypeDLL);
	}

	if (PhaseSpaceDLL)
	{
		FreeLibrary(PhaseSpaceDLL);
	}

	if (NatNetDLL)
	{
		FreeLibrary(NatNetDLL);
	}

	RlmStopFloatingLicensing();
}

bool FLADataStreamModule::licValid() const 
{

	return isValid;
}

bool FLADataStreamModule::IsRigidBody() const
{

	return bRigidBodyLic;
}

ExpirationDate FLADataStreamModule::RLMGetLicenseExpiration() const
{

	ExpirationDate date;

	date.days_left = RlmDaysRemaining();
	date.str = RlmExpirationDateString();

	return date;
}

char* FLADataStreamModule::GetOptions() const
{

	char* options = RlmGetOptions();

	return options;

}

void FLADataStreamModule::UpdateLicense()
{
	char* errorMessage = nullptr;
	RlmStopFloatingLicensing();
	errorMessage = RlmCheckPoint(TCHAR_TO_ANSI(*licenseRelativeFolder));
	if (errorMessage == nullptr)
	{
		isValid = true;
	}
	else
	{
		isValid = false;
		UE_LOG(LogLADataStreamCore, Error, TEXT("DataStream License update error. License not found or expired.  %s"), ANSI_TO_TCHAR(errorMessage));
	}

	
}



IMPLEMENT_MODULE(FLADataStreamModule, LADataStreamCore);