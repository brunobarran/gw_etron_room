/* Copyright (C) 2006-2017, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#include "LAOptiTrackStream.h"
#include "DataStreamInterface.h"
#include "LADataStreamCorePrivatePCH.h"
#include "LAOptiTrackStream.h"
#include "NatNetCAPI.h"

TMap<FString, ULAOptiTrackStream*> ULAOptiTrackStream::Streams;

static TAutoConsoleVariable<int32> CVarUseUniCast(
	TEXT("a.UseUniCast"),
	0,
	TEXT("Should we use Multicast (default) or Unicast?.\n")
	TEXT("0: Disable, 1: Enable"),
	ECVF_Cheat);



void __cdecl DataHandler(sFrameOfMocapData *data, void *pUserData)
{
	DECLARE_SCOPE_CYCLE_COUNTER(TEXT("Process"), STAT_StatsOptitrackProcess, STATGROUP_OptiTrack);
	ULAOptiTrackStream *natClient = (ULAOptiTrackStream*)pUserData;
	NP::pFrameOfMocapData frame(data);
	natClient->SetCurrentFrame(frame);
}


static void ConvertTransformToFromMaya(FTransform& Transform, bool convertTranslation = true)
{
	FVector Translation = Transform.GetTranslation();
	FQuat Rotation = Transform.GetRotation();

	Rotation.X = -Rotation.X;
	Rotation.Z = -Rotation.Z;

	Transform.SetRotation(Rotation);

	if (convertTranslation)
	{
		Translation.Y = -Translation.Y;

		Transform.SetTranslation(Translation);
	}
}

namespace NP {

	pSkeletonData::pSkeletonData(sSkeletonData* sk)
	{
		nRigidBodies = sk->nRigidBodies;
		skeletonID = sk->skeletonID;
		for (int i = 0; i < nRigidBodies; ++i)
			RigidBodyData.Push(pRigidBodyData(&(sk->RigidBodyData[i])));

	}

	pSkeletonData::~pSkeletonData()
	{
		RigidBodyData.Empty();
	}

	pFrameOfMocapData::pFrameOfMocapData(sFrameOfMocapData* fr)
	{
		int i;
		MocapData.Empty();
		RigidBodies.Empty();
		Skeletons.Empty();

		iFrame = fr->iFrame;
		Timecode = fr->Timecode;
		TimecodeSubframe = fr->TimecodeSubframe;
		//fLatency = softwareLatencyMillisec;
		for (i = 0; i < fr->nMarkerSets; ++i)
			MocapData.Push(pMarkerSetData(&(fr->MocapData[i])));
		for (i = 0; i < fr->nRigidBodies; ++i)
			RigidBodies.Push(pRigidBodyData(&(fr->RigidBodies[i])));
		for (i = 0; i < fr->nSkeletons; ++i)
			Skeletons.Push(pSkeletonData(&(fr->Skeletons[i])));
	}

	pFrameOfMocapData::pFrameOfMocapData()
	{
		MocapData.Empty();
		RigidBodies.Empty();
		Skeletons.Empty();
		iFrame = 0;
		Timecode = 0;
		TimecodeSubframe = 0;
		fLatency = 0.0f;
	}

	pFrameOfMocapData::~pFrameOfMocapData()
	{
		MocapData.Empty();
		RigidBodies.Empty();
		Skeletons.Empty();
	}

	pMarkerSetData::pMarkerSetData(sMarkerSetData* ms)
	{
		Markers.Empty();
		Markers.SetNum(ms->nMarkers);
		name = FString(ms->szName);
		name.ReplaceInline(ANSI_TO_TCHAR("-"), ANSI_TO_TCHAR("_"));
		for (int i = 0; i < ms->nMarkers; i++)
		{
			FVector m = FVector(ms->Markers[i][0], ms->Markers[i][1], ms->Markers[i][2]);
			Markers.Push(m);
		}
		nMarkers = ms->nMarkers;
	}

	pMarkerSetData::~pMarkerSetData()
	{
		Markers.Empty();
	}

	pRigidBodyData::pRigidBodyData(sRigidBodyData *rb)
	{
		quat = FQuat(rb->qx, rb->qy, rb->qz, rb->qw);
		trans = FVector(rb->x, rb->y, rb->z);
		ID = rb->ID;
	}

	pRigidBodyData::~pRigidBodyData() {}

}


ULAOptiTrackStream::ULAOptiTrackStream(const FObjectInitializer& PCIP)
	: Super(PCIP), HadFrame(false)
{
	iDataPort = 1511;
	iCmdPort = 1510;
	prConnType = ConnectionType::ConnectionType_Multicast;
	dataDescription = NULL;
	connected_client = false;
	is_motive = true;
	is_200 = true;
	is_realtime = true;


}

void ULAOptiTrackStream::GetDescriptions()
{
	sDataDescriptions *t = nullptr;
	TMap<int, FString> *tSegmentNameByID;
	tSegmentNameByID = new TMap<int, FString>;

	sServerDescription s;
	pClient->GetServerDescription(&s);
	int motiveVersion = s.HostAppVersion[0] * 100 + s.HostAppVersion[1];
	if (strcmp("Motive", s.szHostApp) == 0 && motiveVersion >= 105) {
		is_motive = true;
		if (motiveVersion < 200)
		{
			is_200 = false;
		}
		else
		{
			is_200 = true;
		}
	}
	else {
		is_motive = false;
	}

	while (!t)
	{
		pClient->GetDataDescriptionList(&t);
	}

	dataDescription = t;

	int32 i;
	BodyDescriptions.Empty();
	SubjectDescriptions.Empty();
	for (i = 0; i < (t->nDataDescriptions); i++)
	{
		if (t->arrDataDescriptions[i].type == Descriptor_Skeleton)
		{
			SubjectDescriptions.Push(*(t->arrDataDescriptions[i].Data.SkeletonDescription));

		}
		else if (t->arrDataDescriptions[i].type == Descriptor_RigidBody)
			BodyDescriptions.Push(*(t->arrDataDescriptions[i].Data.RigidBodyDescription));
	}
	//Get Subject Description
	globalsInt.Empty();
	localsInt.Empty();
	SegmentNameById.Empty();
	for (i = 0; i < (SubjectDescriptions.Num()); i++)
	{
		FString subName;
		GetSubjectName(i, subName);
		SubjectIdByName.Add(subName, i);

		for (int j = 0; j < (SubjectDescriptions[i].nRigidBodies); j++)
		{
			sRigidBodyDescription r = SubjectDescriptions[i].RigidBodies[j];
			FString name(r.szName);

			name.ReplaceInline(ANSI_TO_TCHAR("-"), ANSI_TO_TCHAR("_"));
			SegmentIdByName.Add(name, r.ID);
			tSegmentNameByID->Add(r.ID, name);
		}
		TArray<FTransform> temp;
		temp.SetNum(SubjectDescriptions[i].nRigidBodies);
		//locals.Add(subName, temp);
		//globals.Add(subName, temp);
		globalsInt.Add(temp);
		localsInt.Add(temp);
		SegmentNameById.Push(*tSegmentNameByID);
	}

	//Get RigidBod Description
	RigidBodyIndices.Empty();
	for (i = 0; i < BodyDescriptions.Num(); i++)
	{
		sRigidBodyDescription* r = &(BodyDescriptions[i]);
		FString name(r->szName);
		name.ReplaceInline(ANSI_TO_TCHAR("-"), ANSI_TO_TCHAR("_"));
		RigidBodyNames.AddUnique(name);
		RigidBodyIndices.Add(name, i);
	}
	if (RigidBodyIndices.Num() != rigidBodyGlobal.Num())
	{
		rigidBodyGlobal.Empty();
		rigidBodyGlobal.Init(FTransform::Identity, RigidBodyIndices.Num());
	}

	pClient->SetFrameReceivedCallback(DataHandler, this);

	//pClient->SetVerbosityLevel(NatNetVerbosity_Info);


	// set up indices maps
	FString name;


	this->SegmentIndices.SetNum(this->SubjectDescriptions.Num());
	for (i = 0; i < SubjectDescriptions.Num(); ++i)
	{
		name = SubjectDescriptions[i].szName;
		name.ReplaceInline(ANSI_TO_TCHAR("-"), ANSI_TO_TCHAR("_"));
		SubjectIndices.Add(name, i);

		for (int j = 0; j < SubjectDescriptions[i].nRigidBodies; ++j)
		{
			name = SubjectDescriptions[i].RigidBodies[j].szName;
			name.ReplaceInline(ANSI_TO_TCHAR("-"), ANSI_TO_TCHAR("_"));
			SegmentIndices[i].Add(name, j);
		}
	}
}


ELAResult ULAOptiTrackStream::Connect(FString server_ip, int portNumber)
{

	FLADataStreamModule& module = FModuleManager::GetModuleChecked< FLADataStreamModule >("LADataStreamCore");
	if (!module.licValid())
	{
		return ELAResult::ELAError;
	}

	if (IsValidLowLevel())
	{
		if (isConnected())
		{
			Disconnect();
			if (pClient.IsValid())
			{
				pClient->Disconnect();
				pClient.Reset();
			}
		}
		sServerIP = server_ip;
		TStringConversion<TStringConvert <TCHAR, ANSICHAR>> a(*server_ip);

		// create NatNet client
		prConnType = CVarUseUniCast.GetValueOnAnyThread() == 1 ? ConnectionType_Unicast : ConnectionType_Multicast;


		pClient = TSharedPtr<NatNetClient>(new NatNetClient());

		ANSICHAR ip[128] = "";
		FMemory::Memcpy(ip, a.Get(), a.Length());

		sNatNetClientConnectParams connect;
		connect.localAddress = "0.0.0.0";
		connect.serverAddress = ip;
		connect.serverCommandPort = 1510;
		connect.serverDataPort = portNumber;
		connect.connectionType = prConnType;
		int ret = pClient->Connect(connect);
		if (ret == ErrorCode_OK)

		{
			sServerDescription ServerDescription;
			FMemory::Memset(&ServerDescription, 0, sizeof(ServerDescription));

			if (pClient->GetServerDescription(&ServerDescription) == ErrorCode_OK)
			{
				if (ServerDescription.HostPresent)
				{

					connected_client = true;
					GetDescriptions();
					return  ELAResult::ELASuccess;
				}
			}
		}
	}

	if (pClient.IsValid())
	{
		pClient->Disconnect();
		pClient.Reset();
	}
	return ELAResult::ELAError;
}

ELAResult ULAOptiTrackStream::UseMulticast(FString multicast_ip)
{
	return ELAResult::ELAError;
}

ELAResult ULAOptiTrackStream::Disconnect()
{
	if (pClient.IsValid())
	{
		pClient->Disconnect();
		//connected_client = false;
	}
	return ELAResult::ELASuccess;
}

ELAResult ULAOptiTrackStream::GetSubjectCount(int& count)
{
	count = SubjectDescriptions.Num();
	return ELAResult::ELASuccess;
}

ELAResult ULAOptiTrackStream::GetRigidBodyTransform(const FString& name, FTransform& trans)
{
	int rbIndex;
	rbIndex = (RigidBodyIndices.Contains(name)) ? RigidBodyIndices[name] : -1;

	if (rbIndex == INDEX_NONE)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Wrong RigidBody name"));
		return ELAResult::ELAError;
	}
	{
		FScopeLock lock(&section);
		trans = rigidBodyGlobal.IsValidIndex(rbIndex) ? rigidBodyGlobal[rbIndex] : FTransform::Identity;
	}
	return ELAResult::ELASuccess;
}


const TArray<FString>&  ULAOptiTrackStream::GetRigidBodyNames() const
{
	return RigidBodyNames;
}

ELAResult ULAOptiTrackStream::GetSubjectName(int index, FString& name)
{
	name = SubjectDescriptions[index].szName;
	name.ReplaceInline(ANSI_TO_TCHAR("-"), ANSI_TO_TCHAR("_"));
	return ELAResult::ELASuccess;
}

ELAResult ULAOptiTrackStream::GetSegmentCountForSubject(const FString& subjectName, int& count)
{
	unsigned int ind = GetSubjectIndex(subjectName);
	if (ind == -1 || !SubjectDescriptions.IsValidIndex(ind))
	{
		count = 0;
		return ELAResult::ELAError;
	}

	count = SubjectDescriptions[ind].nRigidBodies;
	return ELAResult::ELASuccess;
}

int ULAOptiTrackStream::GetSegmentCountForSubject(const int& subjectIndex)
{
	if (subjectIndex == -1 || !SubjectDescriptions.IsValidIndex(subjectIndex))
	{
		return 0;
	}

	return SubjectDescriptions[subjectIndex].nRigidBodies;
}

ELAResult ULAOptiTrackStream::GetSegmentNameForSubject(const FString& subjectName, int index, FString& segName)
{

	int ind = GetSubjectIndex(subjectName);
	if (ind == -1)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Invalid Subject name"));
		return ELAResult::ELAError;
	}
	segName = SubjectDescriptions[ind].RigidBodies[index].szName;
	segName.ReplaceInline(ANSI_TO_TCHAR("-"), ANSI_TO_TCHAR("_"));
	return ELAResult::ELASuccess;

}

ELAResult ULAOptiTrackStream::GetSegmentParentNameForSubject(const FString& subjectName, FString segName, FString& parentSegName)
{
	int SubIndex, SegIndex, parentID;
	SubIndex = GetSubjectIndex(subjectName);
	if (SubIndex == -1)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Invalid Subject name"));
		return ELAResult::ELAError;
	}

	SegIndex = GetSegmentIndex(SubIndex, segName);
	if (SegIndex == -1)
	{
		parentSegName = "";
		return ELAResult::ELAError;
	}
	parentID = this->SubjectDescriptions[SubIndex].RigidBodies[SegIndex].parentID;
	if (SegmentNameById[SubIndex].Contains(parentID))
	{
		parentSegName = SegmentNameById[SubIndex][parentID];
		return ELAResult::ELASuccess;

	}
	return ELAResult::ELAError;
}

ELAResult ULAOptiTrackStream::GetSegmentParentIndexForSubject(FString& subjectName, FString segName, int& parentSegName)
{

	int SubIndex, SegIndex;
	SubIndex = GetSubjectIndex(subjectName);
	if (SubIndex == -1)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Invalid Subject name"));
		return ELAResult::ELAError;
	}

	SegIndex = GetSegmentIndex(SubIndex, segName);
	if (SegIndex == -1)
	{

		return ELAResult::ELAError;
	}

	parentSegName = this->SubjectDescriptions[SubIndex].RigidBodies[SegIndex].parentID;
	if (SegmentNameById[SubIndex].Contains(parentSegName))
		parentSegName = GetSegmentIndex(SubIndex, SegmentNameById[SubIndex][parentSegName]);
	else
		parentSegName = -1;
	return ELAResult::ELASuccess;

}


int ULAOptiTrackStream::GetSegmentParentIndexForSubject(int subjectIndex, int SegIndex)
{

	auto ID = SubjectDescriptions[subjectIndex].RigidBodies[SegIndex].parentID;

	if (SegmentNameById[subjectIndex].Contains(ID))
		return GetSegmentIndex(subjectIndex, SegmentNameById[subjectIndex][ID]);
	else
		return -1;
}

int ULAOptiTrackStream::GetSegmentIndex(int SubjectIndex, FString SegmentName)
{
	if (SegmentName.IsEmpty() ||
		!(SegmentIndices[SubjectIndex].Contains(SegmentName)))
		return -1;

	return this->SegmentIndices[SubjectIndex][SegmentName];
}



int ULAOptiTrackStream::GetSubjectIndex(const FString& SubjectName)
{
	if (SubjectName.IsEmpty() || (!SubjectIndices.Contains(SubjectName)))
		return -1;
	return SubjectIndices[SubjectName];
}

bool ULAOptiTrackStream::GetPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose)
{
	if (outPose.Num() == 0)
	{
		return false;
	}

	DECLARE_SCOPE_CYCLE_COUNTER(TEXT("LocalPose"), STAT_StatsOptitrackLocalPose, STATGROUP_OptiTrack);


	int SubIndex = GetSubjectIndex(subjectName);
	int segCount = GetSegmentCountForSubject(SubIndex);
	if (localsInt.Num() < 0)
		return false;

	{
		FScopeLock lock(&section);
		const TArray<FTransform>& localSeg = localsInt[SubIndex];
		for (int j = 0; j < segCount; ++j)
		{
			outPose[j] = localSeg[j];
		}
	}
	return true;
}

ELAResult ULAOptiTrackStream::GetSegmentLocalRotationQuaternion(FString SubjectName, FString SegmentName, FQuat& t)
{

	int SubIndex, SubID, SegIndex;
	SubIndex = GetSubjectIndex(SubjectName);
	if (SubIndex == -1)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Wrong Subject name"));
		return ELAResult::ELAError;
	}

	SegIndex = GetSegmentIndex(SubIndex, SegmentName);
	if (SegIndex == -1)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Wrong Segment name"));
		return ELAResult::ELAError;
	}

	SubID = SubjectDescriptions[SubIndex].skeletonID;


	for (int i = 0; i < currentFrame.Skeletons.Num(); i++)
	{
		if (currentFrame.Skeletons[i].skeletonID == SubID)
		{
			if (currentFrame.Skeletons[i].RigidBodyData.IsValidIndex(SegIndex))
			{
				t = currentFrame.Skeletons[i].RigidBodyData[SegIndex].quat;
				if (!is_motive) {
					t.W = -t.W;
				}
			}
			return ELAResult::ELASuccess;
		}
	}
	return ELAResult::ELAError;
}

ELAResult ULAOptiTrackStream::GetSegmentLocalTranslation(FString SubjectName, FString SegmentName, FVector& t)
{

	int SubIndex, SubID, SegIndex;
	SubIndex = GetSubjectIndex(SubjectName);
	if (SubIndex == -1)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Wrong Subject name"));
		return ELAResult::ELAError;
	}

	SegIndex = GetSegmentIndex(SubIndex, SegmentName);
	if (SegIndex == -1)
	{
		UE_LOG(LogLADataStreamCore, Error, TEXT("Wrong Segment name"));
		return ELAResult::ELAError;
	}

	SubID = SubjectDescriptions[SubIndex].skeletonID;

	if (SegIndex != 0)
	{
		auto rb = SubjectDescriptions[SubIndex].RigidBodies[SegIndex];
		t = FVector(-rb.offsetx, rb.offsety, rb.offsetz);
		return ELAResult::ELASuccess;
	}

	for (int i = 0; i < currentFrame.Skeletons.Num(); i++)
	{
		if (currentFrame.Skeletons[i].skeletonID == SubID)
		{
			if (currentFrame.Skeletons[i].RigidBodyData.IsValidIndex(SegIndex))
			{
				t = currentFrame.Skeletons[i].RigidBodyData[SegIndex].trans;
			}
			return ELAResult::ELASuccess;
		}
	}

	return ELAResult::ELAError;
}


ELAResult ULAOptiTrackStream::GetSegmentLocalRotationQuaternion(int SubjectIndex, int  SegmentIndex, FQuat& t)
{

	int SubID;

	SubID = SubjectDescriptions[SubjectIndex].skeletonID;


	for (int i = 0; i < currentFrame.Skeletons.Num(); i++)
	{
		if (currentFrame.Skeletons[i].skeletonID == SubID)
		{
			if (currentFrame.Skeletons[i].RigidBodyData.IsValidIndex(SegmentIndex))
			{
				t = currentFrame.Skeletons[i].RigidBodyData[SegmentIndex].quat;
				if (!is_motive) {
					t.W = -t.W;
				}
			}
			return ELAResult::ELASuccess;
		}
	}
	return ELAResult::ELAError;
}


ELAResult ULAOptiTrackStream::GetSegmentLocalTranslation(int SubjectIndex, int  SegmentIndex, FVector& t)
{

	int SubID = SubjectDescriptions[SubjectIndex].skeletonID;

	if (SegmentIndex != 0)
	{
		auto rb = SubjectDescriptions[SubjectIndex].RigidBodies[SegmentIndex];

		t = FVector(-rb.offsetx, rb.offsety, rb.offsetz);

		if (is_200)
		{
			t.X = -t.X;
		}

		return ELAResult::ELASuccess;
	}

	for (int i = 0; i < currentFrame.Skeletons.Num(); i++)
	{
		if (currentFrame.Skeletons[i].skeletonID == SubID)
		{
			if (currentFrame.Skeletons[i].RigidBodyData.IsValidIndex(SegmentIndex))
			{
				t = currentFrame.Skeletons[i].RigidBodyData[SegmentIndex].trans;
			}
			return ELAResult::ELASuccess;
		}
	}

	return ELAResult::ELAError;
}


bool ULAOptiTrackStream::GetGlobalPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose)
{
	DECLARE_SCOPE_CYCLE_COUNTER(TEXT("GlobalPose"), STAT_StatsOptitrackGlobalPose, STATGROUP_OptiTrack);
	if (outPose.Num() == 0)
	{
		return false;
	}

	const int SubIndex = GetSubjectIndex(subjectName);
	const int segCount = GetSegmentCountForSubject(SubIndex);

	if (globalsInt.Num() < 0)
		return false;

	{
		FScopeLock lock(&section);
		const TArray<FTransform>& globalSeg = globalsInt[SubIndex];
		for (int j = 0; j < segCount; ++j)
		{
			outPose[j + segCount] = globalSeg[j];
		}
	}
	return true;
}

ULAOptiTrackStream* ULAOptiTrackStream::Get(const FString& serverIP)
{
	auto stream = Streams.Find(serverIP);
	auto cast = CVarUseUniCast.GetValueOnAnyThread() == 1 ? ConnectionType_Unicast : ConnectionType_Multicast;

	if (stream)
	{
		if ((*stream)->IsValidLowLevel())
		{
			FScopeLock(&(*stream)->StreamerSection);
			if (cast == (*stream)->prConnType)
			{
				(*stream)->RefCount++;
				return *stream;
			}
			else
			{
				Streams.Remove(serverIP);
			}
		}
		else
		{
			Streams.Remove(serverIP);
		}
	}
	auto newStream = NewObject<ULAOptiTrackStream>();
	newStream->AddToRoot();
	newStream->RefCount = 1;
	Streams.Add(serverIP);
	Streams[serverIP] = newStream;

	return newStream;

}

void ULAOptiTrackStream::Remove(const FString serverIP)
{
	auto stream = Streams.Find(serverIP);
	if (stream)
	{
		if ((*stream)->IsValidLowLevel())
		{
			//TODO: acquire lock
			FScopeLock(&(*stream)->StreamerSection);
			(*stream)->RefCount--;

			if ((*stream)->RefCount <= 0)
			{
				(*stream)->Disconnect();
				(*stream)->RemoveFromRoot();
				Streams.Remove(serverIP);

			}
		}
	}
}

void ULAOptiTrackStream::Destory()
{
	if (this->IsValidLowLevel())
	{
		RefCount--;
		if (RefCount <= 0)
		{
			Disconnect();
			ConditionalBeginDestroy(); //instantly clears UObject out of memory
			AddToRoot();
		}

	}
}

void ULAOptiTrackStream::BeginDestroy()
{
	RefCount--;
	if (RefCount <= 0)
	{
		Streams.Remove(_server_ip);
		Disconnect();
	}
	Super::BeginDestroy();
}

bool ULAOptiTrackStream::isConnected()
{
	sServerDescription sd;
	if (pClient.IsValid())
	{
		pClient->GetServerDescription(&sd);
		if (sd.HostPresent && connected_client)
		{
			return true;
		}
	}
	return false;
}

ELAResult ULAOptiTrackStream::GetFrame()
{
	if (HadFrame)
	{
		return ELAResult::ELASuccess;
	}
	else
	{
		return ELAResult::ELAError;
	}
}

void ULAOptiTrackStream::SetCurrentFrame(NP::pFrameOfMocapData & frame)
{
	currentFrame = frame;
	HadFrame = true;
	int subCount;
	GetSubjectCount(subCount);
	FTransform trans;
	FVector t(0, 0, 0);
	FQuat q(0, 0, 0, 1);
	{
		DECLARE_SCOPE_CYCLE_COUNTER(TEXT("Parenting"), STAT_StatsOptitrackParenting, STATGROUP_OptiTrack);
		for (int i = 0; i < subCount; i++)
		{
			int segCount = GetSegmentCountForSubject(i);
			TArray<FTransform> globalSeg;
			TArray<FTransform> localSeg;
			localSeg.SetNum(segCount);
			globalSeg.SetNum(segCount);

			for (int j = 0; j < segCount; ++j)
			{
				int32 parentIndex = GetSegmentParentIndexForSubject(i, j);
				GetSegmentLocalTranslation(i, j, t);


				trans.SetTranslation(t);
				GetSegmentLocalRotationQuaternion(i, j, q);

				trans.SetRotation(q);
				ConvertTransformToFromMaya(trans);
				localSeg[j] = trans;
				//We will always do the parent before the child.
				if (parentIndex == -1)
				{
					globalSeg[j] = trans;
				}
				else
				{
					globalSeg[j] = trans*globalSeg[parentIndex];
				}
			}
			{
				FScopeLock lock(&section);
				globalsInt[i] = globalSeg;
				localsInt[i] = localSeg;
			}
		}
				
		NatNet_DecodeTimecode(frame.Timecode, frame.TimecodeSubframe, (int*)&timecode.hours, (int*)&timecode.minutes, (int*)&timecode.seconds, (int*)&timecode.frame, (int*)&timecode.subframe);
	}


	if (RigidBodyIndices.Num())
	{
		//Update RB transforms
		FTransform RBtrans = FTransform::Identity;
		TArray<FTransform> rb;

		for (int rbindex = 0; rbindex < RigidBodyIndices.Num(); rbindex++)
		{
			int rbID = BodyDescriptions[rbindex].ID;
			for (int i = 0; i < currentFrame.RigidBodies.Num(); i++)
			{
				if (currentFrame.RigidBodies[i].ID == rbID)
				{
					RBtrans.SetTranslation(currentFrame.RigidBodies[i].trans);
					RBtrans.SetRotation(currentFrame.RigidBodies[i].quat);
					ConvertTransformToFromMaya(RBtrans);
					rb.Push(RBtrans);
					break;
				}
			}

		}
		{
			FScopeLock lock(&section);
			rigidBodyGlobal = rb;
		}
	}
}
