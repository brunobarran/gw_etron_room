using UnrealBuildTool;
using System;
using System.IO;
public class LADataStreamCore : ModuleRules
{
	public LADataStreamCore(ReadOnlyTargetRules Target) : base(Target)
    {
        //we are going to be building without PCH's 
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
        PrivateDependencyModuleNames.AddRange(new string[] { "Projects", "Json", "JsonUtilities" });
        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "PhaseSpace", "IKinema", "ViconDataStreamSDK", "NatNetSDK",  "XSens", "VRPN" });
        PrivateIncludePathModuleNames.Add("IKinemaCore");

    }
}
