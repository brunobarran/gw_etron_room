/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include <owl.h>
#include "DataStreamInterface.h"
#include "LAPhaseSpaceStream.generated.h"

//TODO: @AE & @RT Need to be re-implemented. Current implementation is not streaming in Real-Time


USTRUCT()
struct FMarkerData
{
	GENERATED_USTRUCT_BODY()
	FVector trans;
	
	TArray<FVector> vel;
	bool isMissing;
	float blendFactor;
	FMarkerData()
		:trans(0, 0, 0)
	{
		vel.Add(FVector(0, 0, 0));
	}
};

UCLASS()
class ULAPhaseSpaceStream : public UObject
{
	GENERATED_UCLASS_BODY()

	
	int MarkersNumber;
private:
	
	UPROPERTY()
	FString sServerIP;

	static TMap<FString, ULAPhaseSpaceStream*> Streams;
	int32 RefCount;

	int portNumber;

	OWLMarker* markers;
	void init(void);
	bool connected;
	bool isStarted;
public:
	static ULAPhaseSpaceStream* Get(const FString serverIP, int MarkerNumber);
	TArray<FMarkerData> AMarkers;
	~ULAPhaseSpaceStream()
	{
		owlDone();
	};
	
	bool isConnected()
	{ 
		return connected; 
	};

	ELAResult GetFrame();
	ELAResult Connect(FString server, int portNumber = 5555) ;
	ELAResult Disconnect();
	// Disconnects the DS-SDK and sets the object to be Garbage-Collected if RefCount <= 0
	virtual void Destory();
	virtual void BeginDestroy() override;
	virtual void FinishDestroy() override;

	float calculateAlpha(float freq);
};

