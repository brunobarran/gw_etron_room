/* Copyright 2009-2017 IKinema, Ltd. All Rights Reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema RunTime project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "Animation/AnimInstance.h"
#include "Animation/AnimInstanceProxy.h"
#include "IKinemaLAAnimInstance.generated.h"


USTRUCT(BlueprintType)
struct FLABlueprintTimeCode
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TimeCode")
		int hours;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TimeCode")
		int minutes;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TimeCode")
		int seconds;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TimeCode")
		int frame;

};



USTRUCT()
struct FIKinemaLAAnimInstanceProxy : public FAnimInstanceProxy
{
	GENERATED_BODY()

public:
	FIKinemaLAAnimInstanceProxy();
	FIKinemaLAAnimInstanceProxy(UAnimInstance* Instance);

	void GetTimeFromPose(FLABlueprintTimeCode& timestep);
	void SetTimeFromPose(const FLAGenericTimeCode& InTimeCode);
private:
	FLAGenericTimeCode savedTimeProxy;
};

/**
*
*/
UCLASS(Transient, Blueprintable)
class LADATASTREAMCORE_API UIKinemaLAAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

private:
	virtual FAnimInstanceProxy* CreateAnimInstanceProxy() override;

public:


	UFUNCTION(BlueprintCallable, Category = IKinema)
		FLABlueprintTimeCode GetStreamedTimeCode();


	UPROPERTY(BlueprintReadOnly, Category = IKinema)
		FLABlueprintTimeCode savedTime;



private:
	friend struct FIKinemaLAAnimInstanceProxy;


};

UCLASS(ClassGroup = (IKinema), meta = (BlueprintSpawnableComponent))
class LADATASTREAMCORE_API UTimestampComponent : public USceneComponent
{
	GENERATED_BODY()

public:

	// Sets default values for this component's properties
	UTimestampComponent() {
		PrimaryComponentTick.bCanEverTick = true;



	}

protected:

public:
	// Called every frame



	UPROPERTY(Category = IKinema, EditAnywhere, Interp)
		float frames;

	UPROPERTY(Category = IKinema, EditAnywhere, Interp)
		float hours;

	UPROPERTY(Category = IKinema, EditAnywhere, Interp)
		float minutes;
	UPROPERTY(Category = IKinema, EditAnywhere, Interp)
		float seconds;



	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override
	{

		auto parent = GetOwner();
		auto comp = parent->FindComponentByClass<USkeletalMeshComponent>();
		if (comp != nullptr)
		{
			auto anim = Cast<UIKinemaLAAnimInstance>(comp->GetAnimInstance());
			if (anim != nullptr)
			{
				FLABlueprintTimeCode TimeStep = anim->savedTime;

				frames = TimeStep.frame;
				seconds = TimeStep.seconds;
				minutes = TimeStep.minutes;
				hours = TimeStep.hours;

			}
		}




	}


private:


};
