#pragma once
#include "Windows/AllowWindowsPlatformTypes.h" 
#include <vrpn_Connection.h> // for vrpn_HANDLERPARAM, etc
#include <vrpn_Text.h>           // for vrpn_Text_Receiver, etc
#include <vrpn_Tracker.h>        // for vrpn_TRACKERACCCB, etc
#include "Windows/HideWindowsPlatformTypes.h"
#include "Runtime/Core/Public/Async/AsyncWork.h"
#include "DataStreamInterface.h"
#include "LAVRPNStream.generated.h"

DECLARE_STATS_GROUP(TEXT("VRPN"), STATGROUP_VRPN, STATCAT_Advanced);
	
USTRUCT()
struct FVRPNBones
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FName Name;

	UPROPERTY()
	FTransform Rest;

	UPROPERTY()
	FTransform LocalPose;

	UPROPERTY()
	FTransform GlobalPose;

	UPROPERTY()
	int32 PID;

	UPROPERTY()
	int32 ID;


};

USTRUCT()
struct FVRPNRigidBody
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FName Name;

	UPROPERTY()
	FTransform GlobalPose;

	UPROPERTY()
	int32 ID;


};


USTRUCT()
struct FVRPNChar 
{
	GENERATED_USTRUCT_BODY()
	FVRPNChar();
	FVRPNChar(const FName& name) :Name(name) { };
	FVRPNChar(const FVRPNChar& other);
	FVRPNChar operator=(const FVRPNChar& other);

	~FVRPNChar();

	FCriticalSection characterSection;

	void CreateHierarchy(char SegId, char Total, const char* message);

	int TotalSegments = -1;
	int LastSegment = -1;
	bool Initialized = false;
	FString JsonString = "";
	FName Name;

	// rigid body streaming support
	TMap<FString, int> m_rb_indices;
	TArray<FString> m_rb_names;

	UPROPERTY()
	TArray<FVRPNBones> Bones;

	UPROPERTY()
	TArray<FVRPNRigidBody> RigidBodies;

	void DoFK();
	void InitRigidBodies();
	const TArray<FString>& RigidBodyNames() const { return m_rb_names; }
	const FTransform& RigidBodyTransform(const FString& name) const;

	vrpn_Tracker_Remote* tracker = nullptr;
	vrpn_Text_Receiver*  text = nullptr;
	vrpn_Text_Sender* textSender = nullptr;

};


UCLASS()
class ULAVRPNStream : public UObject, public IDataStreamInterface, public FRunnable
{
	GENERATED_UCLASS_BODY()

private:
	UPROPERTY()
	FString _server_ip;
	

	TSharedPtr<vrpn_Connection> m_socket;

	TMap<int, TSharedPtr<FVRPNChar>> characters;

	uint16_t m_port;
	
	FRunnableThread* Thread;
	bool KillThread;
	int startRead();
	uint32 Run() override;
	static TMap<FString, ULAVRPNStream*> Streams;
	int32 RefCount;

	int iDataPort;

	TArray<FString> temp;
		
	bool connected_client;
	bool has_timecode;
	double curr_time;
	bool is_realtime;
	bool bIsInitialized;
	virtual FLAGenericTimeCode GetTimestamp() override {
		return FLAGenericTimeCode{ 0,0,0,0 };
	}
	TMap<int, FString> AvatarNameMap;
	int GetSegmentIndex(int SubjectIndex, FString SegmentName);
	int GetSubjectIndex(const FString& SubjectName);

	ELAResult GetSegmentLocalRotationQuaternion(FString SubjectName, FString SegmentName, FQuat& quat);
	ELAResult GetSegmentLocalTranslation(FString SubjectName, FString SegmentName, FVector& pos);
	ELAResult GetSegmentParentIndexForSubject(FString& subjectName, FString segName, int& parentSegName);
	FCriticalSection section;
public:

	~ULAVRPNStream() override
	{
		Disconnect();
	};

	//Initialize the Socket and parset
	bool InitialiseCharacter(int senderId, const FString& senderName);

	EMocapServer GetType() override { return EMocapServer::VRPN; };
	FString& GetServerIP() override { return _server_ip; };

	TSharedPtr<vrpn_Connection>& GetSocket()
	{
		return m_socket;
	}

	/* Begin: UObject Interface*/
	void BeginDestroy() override;
	void FinishDestroy() override;
	/*End: UObject Interface*/
	bool isConnected() override;
	FString ToString() override{ return "VRPN"; };
	ELAResult GetFrame() override;

	// Disconnects the DS-SDK and sets the object to be Garbage-Collected if RefCount <= 0
	void Destory() override;

	bool GetPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose) override;
	bool GetGlobalPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose) override;

	LADATASTREAMCORE_API static  ULAVRPNStream* Get(const FString& serverIP);
	LADATASTREAMCORE_API static  void Remove(const FString serverIP);
	ELAResult Connect(FString server, int portNumber = 1510) override;
	ELAResult UseMulticast(FString multiCastIP) override;
	ELAResult Disconnect() override;

	ELAResult GetSubjectCount(int& count) override;
	ELAResult GetSubjectName(int index, FString& name) override;
	ELAResult GetSegmentCountForSubject(const FString& name, int& count) override;
	ELAResult GetSegmentNameForSubject(const FString& subjectName, int index, FString& segName) override;
	ELAResult GetSegmentParentNameForSubject(const FString& subjectName, FString segName, FString& parentSegName) override;

	ELAResult GetRigidBodyTransform(const FString& name, FTransform& trans) override;
	const TArray<FString>&  GetRigidBodyNames() const override;
};

