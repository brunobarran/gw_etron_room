/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
#include "Animation/AnimNodeBase.h"
#include "LAPhaseSpaceStream.h"
#include "AnimNode_IKinemaPhaseSpace.generated.h"

USTRUCT(BlueprintType)
struct LADATASTREAMCORE_API FAnimNode_IKinemaPhaseSpace : public FAnimNode_Base
{
	GENERATED_USTRUCT_BODY()

	
	FPoseLink InPose;

	UPROPERTY(EditAnywhere, Category=Server, meta=(PinShownByDefault))
	FString ServerName;

	UPROPERTY(EditAnywhere, Category=Server,meta=(PinShownByDefault))
	int32 PortNumber;

	UPROPERTY(EditAnywhere, Category=Markers,meta=(PinShownByDefault))
	int32 MarkersNumber;



protected:
	
	UPROPERTY()
	TArray<FMarkerData> AMarkers;

	UPROPERTY()
	ULAPhaseSpaceStream* stream;

public:	

	FAnimNode_IKinemaPhaseSpace();
	~FAnimNode_IKinemaPhaseSpace();

	// FAnimNode_Base interface
	virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	virtual void Update_AnyThread(const FAnimationUpdateContext& Context) override;
	virtual void Evaluate_AnyThread(FPoseContext& Output) override;
	// End of FAnimNode_Base interface
};
