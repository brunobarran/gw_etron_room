#pragma once
#include "DataStreamInterface.h"
#include "MocapActorSource.generated.h"


USTRUCT()
struct FMocapBone
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(VisibleAnywhere, Category = Hierarchy)
	FName Name;

	UPROPERTY(VisibleAnywhere, Category = Hierarchy)
	FName ParentName;

	UPROPERTY(EditAnywhere, Category = Transform)
	FTransform LocalTransform;

	UPROPERTY(EditAnywhere, Category = Transform)
	FTransform GlobalTransform;

	UPROPERTY(EditAnywhere, Category = Transform)
	FName TargetBone;

	bool Dirty;
	bool bIsSelected;
};

UCLASS()
class LADATASTREAMCORE_API UActorSkeleton : public UObject
{
	GENERATED_UCLASS_BODY()

	UPROPERTY()
	TMap<FName, int32> NameIndexMap;

	UPROPERTY(VisibleAnywhere, Category = IKDefinition)
	TArray<FName> LegBones;
	
	//Cache the leglength during first import to save us from doing it every frame with AutoScaling
	UPROPERTY()
	float LegLength; 

	UPROPERTY(EditFixedSize, EditAnywhere, Category = IKDefinition)
	TArray<FMocapBone> Bones;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
	float ImportScale = 1.0f;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
	FTransform SourceTransform;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
	FString SubjectName;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
	FString ServerIP;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
	FString TemplateName;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
	int32 ServerPort;

	UPROPERTY(EditAnywhere, Category = IKDefinition)
	TEnumAsByte<EMocapServer> ServerType;

	UPROPERTY(VisibleAnywhere, Category = IKDefinition)
	bool IsRigidBody;

	UActorSkeleton(int NumOfBones);

	void AddBone(const FName& Name, const FName& ParentName, const FTransform& transform = FTransform::Identity);

	const FMocapBone GetBone(int index) const;
	const FMocapBone GetBone(FName Name) const;

	FMocapBone& GetBone(int index);
	FMocapBone& GetBone(const FName& Name);
	const TArray<FMocapBone>& GetBones() const;
	int GetBoneIndex(const FName& Name);
	int GetParentBoneIndex(int index);
	int GetParentBoneIndex(FName Name);
	int NumOfBones();
	void DoFK(const int BoneIndex);

	void PopulateTransforms(const TArray<FTransform>& Pose, bool ZeroRotations = false);

	float UpdateScale(TArray<FTransform>& Bones);

};
