/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include <NatNetTypes.h>
#include <NatNetClient.h>
#include "DataStreamInterface.h"
#include "HAL/ThreadSafeBool.h"
#include "Containers/Queue.h"
#include "LAOptiTrackStream.generated.h"

DECLARE_STATS_GROUP(TEXT("Optitrack"), STATGROUP_OptiTrack, STATCAT_Advanced);
namespace NP {

	class pRigidBodyData
	{
	public:
		FVector trans;
		FQuat quat;
		int ID;
		pRigidBodyData(sRigidBodyData *rb);
		pRigidBodyData() {};
		~pRigidBodyData();
	};

	class pMarkerSetData {
	public:
		FString name;
		int nMarkers;
		TArray<FVector> Markers;

		pMarkerSetData(sMarkerSetData* ms);
		pMarkerSetData() {};
		~pMarkerSetData();
	};

	class pSkeletonData {
	public:
		int skeletonID, nRigidBodies;
		TArray<pRigidBodyData> RigidBodyData;
		pSkeletonData(sSkeletonData* sk);
		pSkeletonData() {};
		~pSkeletonData();
	};

	class pFrameOfMocapData {
	public:
		int iFrame;
		unsigned int Timecode, TimecodeSubframe;
		float fLatency;
		TArray<pMarkerSetData> MocapData;
		TArray<pRigidBodyData> RigidBodies;
		TArray<pSkeletonData> Skeletons;

		pFrameOfMocapData(sFrameOfMocapData* fr);
		pFrameOfMocapData();

		~pFrameOfMocapData();
	};

} // end of namespace

UCLASS()
class  ULAOptiTrackStream : public UObject, public IDataStreamInterface
{
	GENERATED_UCLASS_BODY()

private:
	UPROPERTY()
	FString _server_ip;
	UPROPERTY()
	FString _multicast_ip;

	TSharedPtr<NatNetClient> pClient;

	//Atomic Bool to make sure we had at least one frame
	FThreadSafeBool HadFrame;

	static TMap<FString, ULAOptiTrackStream*> Streams;
	int32 RefCount;

	FString sLocalIP;
	FString sServerIP;
	TArray<TMap<int, FString>> SegmentNameById;
	TArray<TMap<FString, int>> MarkerIdByName;
	TMap<FString, int> SegmentIdByName;
	TMap<FString, int> SubjectIdByName;
	TArray<FString> RigidBodyNames;
	TArray<sSkeletonDescription> SubjectDescriptions;
	TArray<sRigidBodyDescription> BodyDescriptions;


	//TMap<FString,TArray<FTransform>> globals;
	//TMap<FString, TArray<FTransform>> locals;

	TArray<TArray<FTransform>> globalsInt;
	TArray<TArray<FTransform>> localsInt;

	TArray<FTransform> rigidBodyGlobal;

	 //0(1) complexity index resolution for segments from name
	TMap<FString, FString> nameEscape;
	TMap<FString, int> SubjectIndices;
	TArray<TMap<FString, int> > SegmentIndices;
	TMap<FString, int> RigidBodyIndices;


	int iDataPort;
	int iCmdPort;
	ConnectionType prConnType;
	void init(void);
	void GetDescriptions();
	NP::pFrameOfMocapData currentFrame;
	bool connected_client;
	bool has_timecode;
	double curr_time;
	bool is_realtime;
	bool is_motive;
	bool is_200;
	FLAGenericTimeCode timecode;

	int GetSegmentParentIndexForSubject(int subjectIndex, int SegIndex);

	int GetSegmentIndex(int SubjectIndex, FString SegmentName);
	int GetSubjectIndex(const FString& SubjectName);

	ELAResult GetSegmentLocalRotationQuaternion(FString SubjectName, FString SegmentName, FQuat& quat);
	ELAResult GetSegmentLocalTranslation(FString SubjectName, FString SegmentName, FVector& pos);
	ELAResult GetSegmentLocalRotationQuaternion(int SubjectIndex, int SegmentIndex, FQuat & t);
	ELAResult GetSegmentLocalTranslation(int SubjectIndex, int SegmentIndex, FVector & t);
	ELAResult GetSegmentParentIndexForSubject(FString& subjectName, FString segName, int& parentSegName);
	FCriticalSection StreamerSection; //Critical section for deleting/creating streamer
	FCriticalSection section; //Critical section for update frame data
public:
	TQueue<NP::pFrameOfMocapData > frameStore;
	sDataDescriptions *dataDescription;
	virtual ~ULAOptiTrackStream()
	{
		Disconnect();
	};
	virtual EMocapServer GetType() override { return EMocapServer::NatNet; };
	virtual FString& GetServerIP() override { return sServerIP; };
	virtual FLAGenericTimeCode GetTimestamp() override {
		return timecode;
	}
	/* Begin: UObject Interface*/
	virtual void BeginDestroy() override;
	/*End: UObject Interface*/
	virtual bool isConnected() override;

	virtual FString ToString() override{ return "test"; };
	ELAResult GetFrame();
	void SetCurrentFrame(NP::pFrameOfMocapData& frame);

	// Disconnects the DS-SDK and sets the object to be Garbage-Collected if RefCount <= 0
	virtual void Destory() override;

	virtual bool GetPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose) override;
	virtual bool GetGlobalPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose) override;

	static  ULAOptiTrackStream* Get(const FString& serverIP);
	static  void Remove(const FString serverIP);
	virtual ELAResult Connect(FString server, int portNumber = 1510) override;
	virtual ELAResult UseMulticast(FString multiCastIP) override;
	virtual ELAResult Disconnect() override;

	virtual ELAResult GetSubjectCount(int& count) override;
	virtual ELAResult GetSubjectName(int index, FString& name) override;
	virtual ELAResult GetSegmentCountForSubject(const FString& name, int& count) override;
	int GetSegmentCountForSubject(const int& subjectIndex);
	virtual ELAResult GetSegmentNameForSubject(const FString& subjectName, int index, FString& segName) override;
	virtual ELAResult GetSegmentParentNameForSubject(const FString& subjectName, FString segName, FString& parentSegName) override;


	virtual ELAResult GetRigidBodyTransform(const FString& name, FTransform& trans) override;
	virtual const TArray<FString>&  GetRigidBodyNames() const override;


};
