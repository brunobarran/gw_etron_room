/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include "Animation/AnimNodeBase.h"
#include "LAViconStream.h"
#include "LAOptiTrackStream.h"
#include "LAXSensStream.h"
#include "MocapActorSource.h"
#include "AnimNode_LADataStream.generated.h"


USTRUCT(BlueprintType)
struct  LADATASTREAMCORE_API FAnimNode_LADataStream : public FAnimNode_Base
{
	GENERATED_USTRUCT_BODY()

	// The input pose is segmented into two:
	// - The FK input pose that serves as a bias for the solver.
	// - The task targets appended at the end.
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Links)
	FPoseLink InPose;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MoCapServer)
	TEnumAsByte<EMocapServer> MocapServer;
	
	UPROPERTY(EditAnywhere, Category=Server, meta=(PinShownByDefault))
	FName ServerName;

	UPROPERTY(EditAnywhere, Category=Server,meta=(PinShownByDefault))
	int32 PortNumber;

	UPROPERTY(EditAnywhere, Category = Server, meta = (PinShownByDefault))
	FName SubjectName;

	UPROPERTY(EditAnywhere, Category = StreamingType, meta = (PinHiddenByDefault, ToolTip = "Prop streaming"))
	TArray<FString> RigidBodies;

	UPROPERTY(EditAnywhere, Category = Server, meta = (PinShownByDefault))
	bool AutoScale;

	UPROPERTY(EditAnywhere, Category = Server, meta = (PinShownByDefault))
	UActorSkeleton* Actor = nullptr;

	UPROPERTY(EditAnywhere, Category = Server, meta = (PinHiddenByDefault))
	float Scale = 1;

	//True if the stream is Yup (default in Vicon)
	UPROPERTY(EditAnywhere, Category = Server, meta = (PinShownByDefault))
	bool IsStreamYUp;

	UPROPERTY(EditAnywhere, Category = Server, meta = (PinShownByDefault))
	bool IsRetargeting;

	UPROPERTY(EditAnywhere, Category = Server, meta = (PinShownByDefault,ToolTip = "Attempt to Reconnect?"))
	bool Reconnect;

	/**Unit conversion from Mocap system (Meters by default) to UE4 (cm) */
	UPROPERTY(EditAnywhere, Category = ImportScale,  meta = (PinHiddenByDefault, Default=1.0f, ToolTip = UnitConversion))
	float ImportScale;

	UPROPERTY(EditAnywhere, Category = StreamingType, meta = (PinHiddenByDefault, ToolTip = "Prop streaming"))
	bool useRigidBody;

	UPROPERTY(EditAnywhere, Category = StreamingType, meta = (PinHiddenByDefault, ToolTip = "Debug draw rigid bodies"))
	bool DebugDrawRBs;

	FString StrServerName;
	FString StrSubjectName;

public:	

	FAnimNode_LADataStream();
	~FAnimNode_LADataStream();
	void BindSkeleton(FAnimInstanceProxy* AnimInstanceProxy);
	void IntializeStreamer(FAnimInstanceProxy* AnimInstanceProxy);
	// Keep the binding here.
	FLADataStreamToSkeletonBinding mSkeletonBinding;
	int timestep;

	
	// FAnimNode_Base interface
	void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	void Update_AnyThread(const FAnimationUpdateContext& Context) override;
	void Evaluate_AnyThread(FPoseContext& Output) override;
	void CacheBones_AnyThread(const FAnimationCacheBonesContext & Context) override;

	bool HasPreUpdate() const override { return true; };
	void PreUpdate(const UAnimInstance* InAnimInstance) override;
	// End of FAnimNode_Base interface

private:
	void SetAsset();
	IDataStreamInterface* ViconStream; //!< MocapStream
	bool IsLicInvalid;
	bool IsTrailLicense;
	int numberOfAttempts;
	bool ReallyReconnect;
	bool DidWarn;
	float TimeSinceStart;

	int32 previousPort;
	FString serverSocket;
	
};

