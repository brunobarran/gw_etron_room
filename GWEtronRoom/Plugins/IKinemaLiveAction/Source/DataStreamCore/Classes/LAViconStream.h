/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once


/**
 Work around the name collision between the namespace CPP defined in ViconDataStreamSDK, and the #define CPP
*/
#ifdef CPP
#if CPP
#define __REDEF_CPP 1
#else
#define __REDEF_CPP 0
#endif // #if CPP
#undef CPP
#endif // #ifdef CPP

#include <Client.h>
namespace DSSDK = ViconDataStreamSDK::CPP;

#ifdef __REDEF_CPP
#define CPP __REDEF_CPP
#endif // #ifdef __REDEF_CPP


#include "Runtime/Core/Public/Async/AsyncWork.h"
#include "HAL/Runnable.h"
#include "HAL/RunnableThread.h"
#include "DataStreamInterface.h"
#include "LAViconStream.generated.h"


class LADATASTREAMCORE_API FLADataStreamToSkeletonBinding;

UCLASS()
class  ULAViconStream : public UObject, public IDataStreamInterface, public FRunnable
{
	GENERATED_UCLASS_BODY()

private:

	FString _server_ip;
	FString _multicast_ip;

	TArray<FString> RigidBodyNames;

	DSSDK::Client _client;
	static TMap<FString, ULAViconStream*> ViconStreams;
	int32 RefCount;
	
	FRunnableThread* Thread;
	FCriticalSection section;
	bool KillThread;
public:
	virtual EMocapServer GetType() { return EMocapServer::VICON; };
	virtual FString& GetServerIP() override { return _server_ip; };
	virtual ~ULAViconStream();
	
	/* Begin: UObject Interface*/
	virtual void BeginDestroy() override;
	virtual void FinishDestroy() override;
	/*End: UObject Interface*/
	virtual bool isConnected() override;

	virtual FString ToString() override;
	ELAResult GetFrame();
	
	// Disconnects the DS-SDK and sets the object to be Garbage-Collected if RefCount <= 0
	virtual void Destory() override;

	virtual bool GetPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose) override;
	virtual bool GetGlobalPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose) override;

	static  ULAViconStream* Get(const FString& serverIP);
	static  void Remove(const FString serverIP);
	virtual ELAResult Connect(FString server, int portNumber=802) override;
	virtual ELAResult UseMulticast(FString multiCastIP) override;
	virtual ELAResult Disconnect() override;

	virtual ELAResult GetSubjectCount(int& count) override;
	virtual ELAResult GetSubjectName(int index, FString& name) override;
	virtual ELAResult GetSegmentCountForSubject(const FString& name, int& count) override;
	virtual ELAResult GetSegmentNameForSubject(const FString& subjectName, int index, FString& segName) override;
	virtual ELAResult GetSegmentParentNameForSubject(const FString& subjectName, FString segName, FString& parentSegName) override;
	virtual ELAResult GetRigidBodyTransform(const FString& name, FTransform& trans) override;
	virtual const TArray<FString>&  GetRigidBodyNames() const override
	{
		return	RigidBodyNames;
	}

	virtual uint32 Run();
	virtual void Stop() override;
	virtual FLAGenericTimeCode GetTimestamp() override {
		auto TimeCode = _client.GetTimecode();
		return FLAGenericTimeCode{TimeCode.Hours, TimeCode.Minutes, TimeCode.Seconds, TimeCode.Frames,TimeCode.SubFrame};
	}
};

// Represents a binding of an ViconStream subject to a specific skeleton.
// Resolves UE4 bone indices and stores them.

class LADATASTREAMCORE_API FLADataStreamToSkeletonBinding
{
public:

	// Default constructor.
	FLADataStreamToSkeletonBinding();
	~FLADataStreamToSkeletonBinding()
	{
		mSegmentNames.Empty();
		UE4BoneIndices.Empty();
		mBound = false;

	}

	// Bind the given ViconStreamSubject to the given skeleton and store the result.
	// Since the member properties are not stored on file, this should be called
	// from runtime each time, to set up the members.
	bool BindToSkeleton(IDataStreamInterface* ViconStream,const FString& subject, class USkeleton* skeleton = nullptr, class USkeletalMeshComponent* component = nullptr);

	bool UpdatePose(TArray<FTransform>& outPose);

	bool IsBound();

	const FString& GetSubjectName() { return mSubjectName; };

	// Access the UE4 bone index given the segment index.
	int32 GetUE4BoneIndex(int32 SegmentIndex) const;

	// Acces the Vicon segment name given the segment index.

	FString GetViconSegmentName(int32 SegmentIndex) const;

	int32 GetNumberOfBones() const;

	// Access the IKinema rig.
	const IDataStreamInterface* GetViconStream() const;

	const USkeleton* GetSkeleton() const;

	const USkeletalMeshComponent* GetSkeletalMeshComponent() const;

	bool bIsRetargeting;

	int frameNumber;

private:

	// Remember the rig.
	
	IDataStreamInterface* mViconStream;

	USkeleton* mSkeleton;


	USkeletalMeshComponent* mComponent;

	FString mSubjectName;

	// Cache the UE4 bone indices corresponding to IKinema segments.
	TArray<int32> UE4BoneIndices;
	// Cache Vicon Subject bone names
	TArray<FString> mSegmentNames;



	//Flag to make sure we are bound to a valid stream
	bool mBound;
};

