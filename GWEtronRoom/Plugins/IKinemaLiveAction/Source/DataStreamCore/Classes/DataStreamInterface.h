/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once
//#include "CoreUObject.h"
#include "DataStreamInterface.generated.h"


class LADATASTREAMCORE_API FLADataStreamToSkeletonBinding;

UENUM()
enum ELAResult {
	ELASuccess,
	ELAError,
};

/** Enum for controlling which reference frame a controller is applied in. */
UENUM()
enum EMocapServer
{
	None UMETA(DisplayName = "Please chose one"),
	VICON UMETA(DisplayName = "VICON Datastream SDK"),
	NatNet UMETA(DisplayName = "OptiTrack NatNet"),
	XSens UMETA(DisplayName = "XSens MVN Studio - Quaternion"),
	VRPN UMETA(DisplayName = "VRPN Streaming"),
	LiveLink UMETA(DisplayName = " ")
};

USTRUCT(BlueprintType)
struct FLAGenericTimeCode
{
	GENERATED_BODY()


public:
	unsigned int hours;
	unsigned int minutes;
	unsigned int seconds;
	unsigned int frame;
	unsigned int subframe;

};

/** Class needed to support InterfaceCast(Object) */
UINTERFACE(MinimalAPI)
class UDataStreamInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class  LADATASTREAMCORE_API IDataStreamInterface
{
	GENERATED_IINTERFACE_BODY()

public:

	static IDataStreamInterface* Get(const FString& ServerIP, EMocapServer ServerType);
	static void Remove(IDataStreamInterface* Stream);

	virtual bool isConnected()=0;

	virtual FString ToString() =0;
	virtual ELAResult GetFrame()=0;

	// Disconnects the DS-SDK and sets the object to be Garbage-Collected if RefCount <= 0
	virtual void Destory()=0;

	virtual bool GetPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose)=0;
	virtual bool GetGlobalPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose)=0;



	virtual ELAResult Connect(FString server,int portNumber)=0;
	virtual ELAResult UseMulticast(FString multiCastIP)=0;
	virtual ELAResult Disconnect()=0;

	virtual ELAResult GetSubjectCount(int& count)=0;
	virtual ELAResult GetSubjectName(int index, FString& name)=0;
	virtual ELAResult GetSegmentCountForSubject(const FString& name, int& count)=0;
	virtual ELAResult GetSegmentNameForSubject(const FString& subjectName, int index, FString& segName)=0;
	virtual ELAResult GetSegmentParentNameForSubject(const FString& subjectName, FString segName, FString& parentSegName)=0;

	virtual EMocapServer GetType() { return EMocapServer::None; };

	virtual FString& GetServerIP() = 0;
	virtual FLAGenericTimeCode GetTimestamp()=0;

	virtual const TArray<FString>&  GetRigidBodyNames() const = 0;
	virtual ELAResult GetRigidBodyTransform(const FString& name, FTransform& trans)  { return ELAResult::ELASuccess; };
	//virtual ~IDataStreamInterface() {};
};