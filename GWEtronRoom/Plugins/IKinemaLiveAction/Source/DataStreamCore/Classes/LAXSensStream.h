/* Copyright (C) 2006-2016, IKinema Ltd. All rights reserved.
*
* IKinema API Library; SDK distribution
*
* This file is part of IKinema LiveAction project http://www.ikinema.com
*
* Your use and or redistribution of this software in source and / or binary form, with or without
* modification, is subject to:
* (i) your ongoing acceptance of and compliance with the terms and conditions of
* the IKinema License Agreement; and
*
* (ii) your inclusion of this notice in any version of this software that you use
* or redistribute.
*
*
* A copy of the IKinema License Agreement is available by contacting
* IKinema Ltd., http://www.ikinema.com, support@ikinema.com
*
*/

#pragma once

#include <StreamingProtocol/streamer.h>
#include "StreamingProtocol/parsermanager.h"
#include "StreamingProtocol/quaterniondatagram.h"
#include "StreamingProtocol/metadatagram.h"
#include "StreamingProtocol/timecodedatagram.h"
#include "StreamingProtocol/jointanglesdatagram.h"
#include "HAL/Runnable.h"
#include <xsens/xssocket.h>
#include "Runtime/Core/Public/Async/AsyncWork.h"
#include "DataStreamInterface.h"
#include "LAXSensStream.generated.h"

DECLARE_STATS_GROUP(TEXT("XSens"), STATGROUP_XSens, STATCAT_Advanced);

//TODO: extend data grams to get us UE4 data.
namespace XS {

	struct TimeCodeStruct
	{
		long int	m_nano;
		signed char		m_hour;
		signed char		m_minute;
		signed char		m_second;
	};
	class XSensChar {
	public:
		XSensChar(int id, FString name);
		int Id;
		FString Name; //Suite Name;
		TArray<FTransform> GlobalPose;
		TArray<FTransform> LocalPose;
		TArray<int> ParentId; //Segment parent id
		TimeCodeStruct TimeCode;

		int32 FrameTime;

		//Interpret Datagram based on its type
		//It could be either, MetaData to get name, Quaternion to get pose, or TimeCode
		void Deserialize(Datagram* data);

	private:
		void parseQuaternion(QuaternionDatagram* datagram);
		void parseMeta(const MetaDatagram* datagram);
		void parseJointAngle(JointAnglesDatagram* datagram);
		void parseTimeCode(TimeCodeDatagram* datagram);
		void BuildParentHierarchy(int boneCount);
	};

} // end of namespace

UCLASS()
class ULAXSensStream : public UObject, public IDataStreamInterface, public FRunnable
{
	GENERATED_UCLASS_BODY()

private:
	UPROPERTY()
	FString _server_ip;
	

	TUniquePtr<XsSocket> m_socket;
	uint16_t m_port;
	
	FRunnableThread* Thread;
	bool KillThread;
	void startRead(int TimeOut = 0);
	uint32 Run();
	static TMap<FString, ULAXSensStream*> Streams;
	int32 RefCount;

	int iDataPort;

	//Initialize the Socket and parset
	bool InitialiseCharacter(void);
	Datagram* createDgram(StreamingProtocol proto);
	
	bool connected_client;
	bool has_timecode;
	double curr_time;
	bool is_realtime;
	bool bIsInitialized;
	FLAGenericTimeCode genericTime;
	TArray<FString> SegmentNameById;
	TMap<int, XS::XSensChar> Characters;
	TMap<int, FString> AvatarNameMap;
	int GetSegmentIndex(FString SegmentName);
	int GetSubjectIndex(const FString& SubjectName);

	ELAResult GetSegmentLocalRotationQuaternion(FString SubjectName, FString SegmentName, FQuat& quat);
	ELAResult GetSegmentLocalTranslation(FString SubjectName, FString SegmentName, FVector& pos);
	ELAResult GetSegmentParentIndexForSubject(FString& subjectName, FString segName, int& parentSegName);
	FCriticalSection section;
public:
	TQueue<Datagram*> frameStore;

	virtual ~ULAXSensStream()
	{
		Disconnect();
	};
	virtual EMocapServer GetType() { return EMocapServer::XSens; };
	virtual FString& GetServerIP() override { return _server_ip; };
	virtual FLAGenericTimeCode GetTimestamp() override {
		return genericTime;
	}
	virtual const TArray<FString>&  GetRigidBodyNames() const override
	{
		return	SegmentNameById;
	}

	/* Begin: UObject Interface*/
	virtual void BeginDestroy() override;
	virtual void FinishDestroy() override;
	/*End: UObject Interface*/
	virtual bool isConnected() override;
	virtual FString ToString() override{ return "XSens"; };
	ELAResult GetFrame();

	// Disconnects the DS-SDK and sets the object to be Garbage-Collected if RefCount <= 0
	virtual void Destory() override;

	virtual bool GetPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose) override;
	virtual bool GetGlobalPoseForSubject(FLADataStreamToSkeletonBinding* skeletonBinding, FString& subjectName, TArray<FTransform>& outPose) override;

	LADATASTREAMCORE_API static  ULAXSensStream* Get(const FString& serverIP);
	LADATASTREAMCORE_API static  void Remove(const FString serverIP);
	virtual ELAResult Connect(FString server, int portNumber = 1510) override;
	virtual ELAResult UseMulticast(FString multiCastIP) override;
	virtual ELAResult Disconnect() override;

	virtual ELAResult GetSubjectCount(int& count) override;
	virtual ELAResult GetSubjectName(int index, FString& name) override;
	virtual ELAResult GetSegmentCountForSubject(const FString& name, int& count) override;
	virtual ELAResult GetSegmentNameForSubject(const FString& subjectName, int index, FString& segName) override;
	virtual ELAResult GetSegmentParentNameForSubject(const FString& subjectName, FString segName, FString& parentSegName) override;


	ELAResult GetRigidBodyTransform(const FString& name, FTransform& trans);
	
	int NumOfProps = 0;
};

