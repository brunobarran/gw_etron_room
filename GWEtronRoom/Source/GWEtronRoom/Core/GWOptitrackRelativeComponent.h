// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OptitrackRigidBodyComponent.h"
#include "GWOptitrackRelativeComponent.generated.h"

UENUM(BlueprintType)
namespace GWOptitrackComponent
{
	enum ERigidBody
	{
		PELVIS 		UMETA(DisplayName = "Pelvis"),
		LEFT_FOOT 	UMETA(DisplayName = "Left foot"),
		RIGHT_FOOT	UMETA(DisplayName = "Right foot"),
		LEFT_HAND 	UMETA(DisplayName = "Left hand"),
		RIGHT_HAND 	UMETA(DisplayName = "Right hand"),
		HMD		UMETA(DisplayName = "HMD")
	};
}

/**
 * 
 */
UCLASS(Blueprintable, ClassGroup = "GW", meta = (BlueprintSpawnableComponent), hidecategories = ("Optitrack"))
class GWETRONROOM_API UGWOptitrackRelativeComponent : public UOptitrackRigidBodyComponent
{
	GENERATED_BODY()


protected:
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	FTransform PrevRbState;
	class ACharacter* Owner;

public:
	void SetPlayerId(int playerId);

public:

	UPROPERTY(EditDefaultsOnly, Category = GW)
		TEnumAsByte<GWOptitrackComponent::ERigidBody> RigidBody;

	UPROPERTY(EditDefaultsOnly, Category = GW)
		int PlayerId = 0;

	UPROPERTY()
		FTransform DeltaTransform;
	
};
