// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GWLandingGameMode.generated.h"

/**
 * 
 */
UCLASS(Config = Game)
class GWETRONROOM_API AGWLandingGameMode : public AGameModeBase
{
	GENERATED_BODY()
		virtual FString InitNewPlayer
		(
			APlayerController * NewPlayerController,
			const FUniqueNetIdRepl & UniqueId,
			const FString & Options,
			const FString & Portal
		) override;

	UPROPERTY(Config)
		bool IsServer;

	UPROPERTY(Config)
		FString ServerIpAddress;

	UPROPERTY(Config)
		bool IsVR;

	UPROPERTY(Config)
		FString TypeOfPlayer;

	UPROPERTY(Config)
		FString LandingLevel;

public:


	UFUNCTION()
		FString GetURL();

	UFUNCTION()
		FString	GetURLOptions();	
};
