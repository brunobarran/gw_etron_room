// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GWPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class GWETRONROOM_API AGWPlayerController : public APlayerController
{
	GENERATED_BODY()
public:

	virtual void PawnLeavingGame() override;
};
