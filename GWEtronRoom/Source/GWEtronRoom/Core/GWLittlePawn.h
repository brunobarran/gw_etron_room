// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GWLittlePawn.generated.h"

UCLASS()
class GWETRONROOM_API AGWLittlePawn : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AGWLittlePawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UGWOptitrackRelativeComponent* OptitrackComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* PawnRoot;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* TranslatorComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* RotatorComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* CameraComponent;

public:
	UPROPERTY(EditDefaultsOnly, Category = GW)
		int PlayerId;
public:
	UPROPERTY()
		class AOptitrackClientOrigin* TrackingOrigin;

	UPROPERTY()
		class UGWGuybrushAnimInstance* AnimInstance;

};
