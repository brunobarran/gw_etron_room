// Fill out your copyright notice in the Description page of Project Settings.

#include "GWWhiteRoomGameMode.h"
#include "GWPlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "EngineUtils.h"

FString AGWWhiteRoomGameMode::InitNewPlayer(APlayerController * NewPlayerController, const FUniqueNetIdRepl & UniqueId, const FString & Options, const FString & Portal)
{


	FString err = Super::InitNewPlayer(NewPlayerController, UniqueId, Options, Portal);

	FString IsServer = UGameplayStatics::ParseOption(Options, TEXT("IsServer"));
	if (IsServer.ToBool()) return err;

	FString TypeOfPlayer = UGameplayStatics::ParseOption(Options, TEXT("TypeOfPlayer"));
	if (TypeOfPlayer.Len() != 0) {
		AGWPlayerState* PlayerState = Cast<AGWPlayerState>(NewPlayerController->PlayerState);
		if (!PlayerState) {
		}
		if (NewPlayerController && PlayerState) {
			PlayerState->TypeOfPlayer = TypeOfPlayer;
			for (TActorIterator<APawn> ActorItr(GetWorld()); ActorItr; ++ActorItr)
			{
				// Same as with the Object Iterator, access the subclass instance with the * or -> operators.
				APawn *Pawn = *ActorItr;

				if (Pawn->ActorHasTag(*TypeOfPlayer)) {
					UE_LOG(LogTemp, Warning, TEXT("InitNewPlayer Possess"));
					NewPlayerController->Possess(Pawn);
					break;
				}
			}

		}
	}
	return err;

}