// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "Animation/AnimInstanceProxy.h"
#include "GWGuybrushAnimInstance.generated.h"

/**
 * 
 */

namespace GWGuybrushAnimInstance
{
	enum ERigidBody
	{
		PELVIS 		UMETA(DisplayName = "Pelvis"),
		LEFT_FOOT 	UMETA(DisplayName = "Left foot"),
		RIGHT_FOOT	UMETA(DisplayName = "Right foot"),
		LEFT_HAND 	UMETA(DisplayName = "Left hand"),
		RIGHT_HAND 	UMETA(DisplayName = "Right hand"),
		HMD		UMETA(DisplayName = "HMD")
	};
}

USTRUCT()
struct FGWGuybrushAnimInstanceProxy : public FAnimInstanceProxy
{
	GENERATED_BODY()

public:
	FGWGuybrushAnimInstanceProxy() {}
	FGWGuybrushAnimInstanceProxy(UAnimInstance* Instance);
	virtual void Initialize(UAnimInstance * InAnimInstance);
	virtual void Update(float DeltaSeconds) override;

protected:

	UPROPERTY()
		TArray<FTransform> RB_Transform;

	UPROPERTY()
		TArray<bool> RB_Tracked;

protected:
	bool IsRigidBodyTracked(int RigidBodyId, int PlayerId, FTransform& OutTransform, bool PrintDebug = false);

private:
	UPROPERTY()
		class UGWGuybrushAnimInstance* AnimInstance;

	int NUM_RIGID_BODIES = 6;
	int NUM_UPPER_BODY_RB = 3;
	int NUM_LOWER_BODY_RB = 3;

};

UCLASS()
class GWETRONROOM_API UGWGuybrushAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

private:
	virtual FAnimInstanceProxy* CreateAnimInstanceProxy() override;


private:

	friend struct FGWGuybrushAnimInstanceProxy;

public:
	UGWGuybrushAnimInstance();
	virtual void NativeInitializeAnimation() override;
	virtual void NativeUpdateAnimation(float DeltaTimeX) override;

public:
	UPROPERTY(BlueprintReadOnly, Category = GW)
		float UpperBodyWeight;

	UPROPERTY(BlueprintReadOnly, Category = GW)
		float LowerBodyWeight;


public:

	UPROPERTY(EditDefaultsOnly, Category = GW)
		float MaxUpperBodyBlend;

	UPROPERTY(EditDefaultsOnly, Category = GW)
		float MaxLowerBodyBlend;

	UPROPERTY(EditDefaultsOnly, Category = GW)
		float InterpolationFactor;

	UPROPERTY(EditDefaultsOnly, Category = GW)
		int AllowUntrackedRigidBodies;

public:

	UPROPERTY()
		class AGWLittlePawn* Owner;

	UPROPERTY()
		bool UpperBodyTracked;

	UPROPERTY()
		bool LowerBodyTracked;

	UPROPERTY()
		class AOptitrackClientOrigin* TrackingOrigin;

	UPROPERTY()
		int PlayerId;

};
