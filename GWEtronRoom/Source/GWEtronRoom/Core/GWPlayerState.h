// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "Net/UnrealNetwork.h"
#include "GWPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class GWETRONROOM_API AGWPlayerState : public APlayerState
{
	GENERATED_BODY()
public:
	UPROPERTY(Replicated)
		FString TypeOfPlayer;
};
