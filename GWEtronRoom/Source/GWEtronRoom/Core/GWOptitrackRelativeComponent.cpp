// Fill out your copyright notice in the Description page of Project Settings.

#include "GWOptitrackRelativeComponent.h"
#include "OptitrackClientOrigin.h"
#include "GameFramework/Character.h"
#include "Components/CapsuleComponent.h"

void UGWOptitrackRelativeComponent::BeginPlay()
{
	Super::BeginPlay();


	Owner = Cast<ACharacter>(GetOwner());

	PrevRbState = FTransform(GetComponentRotation(), GetComponentLocation(), FVector::ZeroVector);
	DeltaTransform = FTransform::Identity;
}

void UGWOptitrackRelativeComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!TrackingOrigin) return;
	if (!Owner) return;
	if (PlayerId == 0) return;

	FOptitrackRigidBodyState rbState;
	if (TrackingOrigin->GetLatestRigidBodyState(PlayerId * 10 + RigidBody, rbState))
	{
		FVector DeltaPosition = rbState.Position - PrevRbState.GetLocation();
		FQuat DeltaRotation = rbState.Orientation * PrevRbState.GetRotation().Inverse();
		PrevRbState = FTransform(rbState.Orientation, rbState.Position, FVector::ZeroVector);

		auto rot = Owner->GetCapsuleComponent()->GetComponentRotation();

		float YawToRadians = (FMath::Fmod(rot.Yaw + 360, 360) * 3.1415926) / 180;

		//2D Vector rotation maths (as the pivot point is now always under the character)
		FVector axisChanged;
		axisChanged.X = DeltaPosition.X * FMath::Cos(YawToRadians) - DeltaPosition.Y * FMath::Sin(YawToRadians);
		axisChanged.Y = DeltaPosition.X * FMath::Sin(YawToRadians) + DeltaPosition.Y * FMath::Cos(YawToRadians);
		axisChanged.Z = DeltaPosition.Z;


		AddWorldOffset(axisChanged);
		SetRelativeRotation(rbState.Orientation);

		DeltaTransform = FTransform(DeltaRotation, axisChanged, FVector::ZeroVector);


	}
	else {
		DeltaTransform = FTransform::Identity;
	}
}

void UGWOptitrackRelativeComponent::SetPlayerId(int playerId)
{
	PlayerId = playerId;
}
