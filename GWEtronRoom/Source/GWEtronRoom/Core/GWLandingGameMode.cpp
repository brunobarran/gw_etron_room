// Fill out your copyright notice in the Description page of Project Settings.

#include "GWLandingGameMode.h"
#include "Kismet/GameplayStatics.h"



FString AGWLandingGameMode::InitNewPlayer(APlayerController * NewPlayerController, const FUniqueNetIdRepl & UniqueId, const FString & Options, const FString & Portal)
{
	FString ret = Super::InitNewPlayer(NewPlayerController, UniqueId, Options, Portal);

	if (IsServer) {
		UGameplayStatics::OpenLevel(GetWorld(), FName(*LandingLevel), true, "listen" + GetURLOptions());
	}
	else {
		NewPlayerController->ClientTravel(GetURL(), ETravelType::TRAVEL_Absolute);
	}
	return ret;
}

FString AGWLandingGameMode::GetURL()
{
	return ServerIpAddress + GetURLOptions();
}

FString AGWLandingGameMode::GetURLOptions()
{
	return "?vr=" + FString::FromInt(IsVR) + "?TypeOfPlayer=" + TypeOfPlayer + "?IsServer" + FString::FromInt(IsServer);
}
