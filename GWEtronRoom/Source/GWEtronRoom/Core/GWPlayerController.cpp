// Fill out your copyright notice in the Description page of Project Settings.

#include "GWPlayerController.h"

void AGWPlayerController::PawnLeavingGame()
{
	if (GetPawn() != NULL)
	{
		UnPossess();
		SetPawn(NULL);
	}
}