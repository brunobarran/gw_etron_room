// Fill out your copyright notice in the Description page of Project Settings.

#include "GWGuybrushAnimInstance.h"
#include "OptitrackClientOrigin.h"
#include "GWLittlePawn.h"

FGWGuybrushAnimInstanceProxy::FGWGuybrushAnimInstanceProxy(UAnimInstance* Instance)
{
	UE_LOG(LogTemp, Warning, TEXT("Anim Proxy Constructor"));


}

void FGWGuybrushAnimInstanceProxy::Initialize(UAnimInstance * InAnimInstance)
{
	Super::Initialize(InAnimInstance);
	UE_LOG(LogTemp, Warning, TEXT("Anim Proxy Initialize"));

	UGWGuybrushAnimInstance* AI = Cast<UGWGuybrushAnimInstance>(InAnimInstance);
	if (!AI) return;

	AnimInstance = AI;

	RB_Transform.Init(FTransform::Identity, NUM_RIGID_BODIES);
	RB_Tracked.Init(false, NUM_RIGID_BODIES);

}

void FGWGuybrushAnimInstanceProxy::Update(float DeltaSeconds)
{
	Super::Update(DeltaSeconds);
	if (!AnimInstance) return;
	if (AnimInstance->TrackingOrigin == nullptr) return;
	if (AnimInstance->PlayerId == 0)return;

	int NumTracked = 0;


	for (int i = 0; i < NUM_RIGID_BODIES; i++) {
		bool Tracked = IsRigidBodyTracked(i, AnimInstance->PlayerId, RB_Transform[i], false);
		RB_Tracked[i] = Tracked;
		if (Tracked) {
			NumTracked++;
		}
	}

	int NumUpperBodyTracked = int(RB_Tracked[GWGuybrushAnimInstance::LEFT_HAND]) + int(RB_Tracked[GWGuybrushAnimInstance::RIGHT_HAND]) + int(RB_Tracked[GWGuybrushAnimInstance::HMD]);
	int NumLowerBodyTracked = int(RB_Tracked[GWGuybrushAnimInstance::LEFT_FOOT]) + int(RB_Tracked[GWGuybrushAnimInstance::RIGHT_FOOT]) + int(RB_Tracked[GWGuybrushAnimInstance::PELVIS]);

	AnimInstance->UpperBodyTracked = (AnimInstance->AllowUntrackedRigidBodies >= (NUM_UPPER_BODY_RB - NumUpperBodyTracked));
	AnimInstance->LowerBodyTracked = (AnimInstance->AllowUntrackedRigidBodies >= (NUM_LOWER_BODY_RB - NumLowerBodyTracked));

	AnimInstance->UpperBodyWeight = AnimInstance->UpperBodyTracked ? FMath::Lerp(AnimInstance->UpperBodyWeight, 0.0f, AnimInstance->InterpolationFactor) : FMath::Lerp(AnimInstance->UpperBodyWeight, AnimInstance->MaxUpperBodyBlend, AnimInstance->InterpolationFactor);
	AnimInstance->LowerBodyWeight = AnimInstance->LowerBodyTracked ? FMath::Lerp(AnimInstance->LowerBodyWeight, 0.0f, AnimInstance->InterpolationFactor) : FMath::Lerp(AnimInstance->LowerBodyWeight, AnimInstance->MaxLowerBodyBlend, AnimInstance->InterpolationFactor);
}


bool FGWGuybrushAnimInstanceProxy::IsRigidBodyTracked(int RigidBodyId, int PlayerId, FTransform& OutTransform, bool PrintDebug)
{
	if (!AnimInstance) return false;
	if (AnimInstance->TrackingOrigin == nullptr) return false;

	FTransform DummyTransform;
	FOptitrackRigidBodyState rbState;
	bool rbTracked = AnimInstance->TrackingOrigin->GetLatestRigidBodyState(PlayerId * 10 + RigidBodyId, rbState);
	DummyTransform.SetLocation(rbState.Position);
	DummyTransform.SetRotation(rbState.Orientation);
	bool Tracked = (rbTracked && !DummyTransform.Equals(FTransform::Identity));

	Tracked = Tracked && !DummyTransform.Equals(RB_Transform[RigidBodyId]);

	if (Tracked) {
		OutTransform = DummyTransform;
	}

	if (PrintDebug) {
		if (!Tracked) { UE_LOG(LogTemp, Warning, TEXT("Tracking error for RB=%d"), (PlayerId * 10 + RigidBodyId)); }
		else { UE_LOG(LogTemp, Warning, TEXT("Tracking succesful for RB=%d"), (PlayerId * 10 + RigidBodyId)); }
	}

	return Tracked;
}



/*************************************************************************************************************/

FAnimInstanceProxy* UGWGuybrushAnimInstance::CreateAnimInstanceProxy()
{
	return new FGWGuybrushAnimInstanceProxy(this);
}

UGWGuybrushAnimInstance::UGWGuybrushAnimInstance()
{
	InterpolationFactor = 0.1f;
	AllowUntrackedRigidBodies = 0;
	MaxLowerBodyBlend = MaxUpperBodyBlend = 1.0f;
	UpperBodyTracked = LowerBodyTracked = false;
	PlayerId = 0;
}

void UGWGuybrushAnimInstance::NativeInitializeAnimation()
{
	UE_LOG(LogTemp, Warning, TEXT("Anim Instance Initialize"));
	Super::NativeInitializeAnimation();
	AGWLittlePawn* Pawn = Cast<AGWLittlePawn>(TryGetPawnOwner());
	if (Pawn) {
		Owner = Pawn;
		PlayerId = Owner->PlayerId;
	}
}

void UGWGuybrushAnimInstance::NativeUpdateAnimation(float DeltaTimeX)
{
	Super::NativeUpdateAnimation(DeltaTimeX);
	if (!Owner)return;

}
