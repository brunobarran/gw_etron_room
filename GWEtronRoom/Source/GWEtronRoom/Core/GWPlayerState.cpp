// Fill out your copyright notice in the Description page of Project Settings.

#include "GWPlayerState.h"

void AGWPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	// Here we list the variables we want to replicate + a condition if wanted
	DOREPLIFETIME(AGWPlayerState, TypeOfPlayer);
}
