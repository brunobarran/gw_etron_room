// Fill out your copyright notice in the Description page of Project Settings.

#include "GWLittlePawn.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GWOptitrackRelativeComponent.h"
#include "GWGuybrushAnimInstance.h"
#include "OptitrackClientOrigin.h"
#include "Camera/CameraComponent.h"

// Sets default values
AGWLittlePawn::AGWLittlePawn()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PlayerId = 0;

	TranslatorComponent = CreateDefaultSubobject<USceneComponent>(TEXT("TranslatorComponent"));
	TranslatorComponent->SetupAttachment(GetCapsuleComponent());

	GetMesh()->SetupAttachment(TranslatorComponent);

	OptitrackComponent = CreateDefaultSubobject<UGWOptitrackRelativeComponent>(TEXT("OptitrackComponent"));
	OptitrackComponent->SetupAttachment(TranslatorComponent);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(OptitrackComponent);

}

// Called when the game starts or when spawned
void AGWLittlePawn::BeginPlay()
{
	Super::BeginPlay();

	AnimInstance = Cast<UGWGuybrushAnimInstance>(GetMesh()->GetAnimInstance());


	if (AnimInstance && TrackingOrigin == nullptr)
	{
		TrackingOrigin = AOptitrackClientOrigin::FindDefaultClientOrigin(GetWorld());
		AnimInstance->TrackingOrigin = TrackingOrigin;
	}

	if (OptitrackComponent) {
		OptitrackComponent->SetPlayerId(PlayerId);
	}
	
}

// Called every frame
void AGWLittlePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!OptitrackComponent->DeltaTransform.Equals(FTransform::Identity)) {
		FVector pos = OptitrackComponent->DeltaTransform.GetLocation();
		pos.Z = 0.0f;

		GetCapsuleComponent()->AddWorldOffset(pos);
		TranslatorComponent->AddWorldOffset(-1.0f*pos);
	}
}

// Called to bind functionality to input
void AGWLittlePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

